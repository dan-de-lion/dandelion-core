import { resolve } from 'path';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import dts from 'vite-plugin-dts';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: {
        compilerOptions: {
          isCustomElement: (tag) => ['ion-icon'].includes(tag),
        },
      },
    }),
    dts({
      insertTypesEntry: false,
    }),
  ],
  resolve: {
    alias: {
      'vue': !Boolean(process.env.BUILDING) ? 'vue/dist/vue.esm-bundler.js' : 'vue',
      '@': resolve(__dirname, 'src'),
    },
  },
  build: {
    sourcemap: true,
    minify: false,
    lib: {
      entry: {
        'dandelion-core': resolve(__dirname, 'src/index.ts'),
        'dandelion-core-storages': resolve(__dirname, 'src/module-storages.ts'),
      },
      formats: ['es', 'cjs'],
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue', 'vue/dist/vue.mjs'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
});

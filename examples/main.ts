import 'reflect-metadata';
import '@fontsource/outfit/100.css';
import '@fontsource/outfit/200.css';
import '@fontsource/outfit/300.css';
import '@fontsource/outfit/400.css';
import '@fontsource/outfit/500.css';
import '@fontsource/outfit/600.css';
import '@fontsource/outfit/700.css';
import '@fontsource/outfit/800.css';
import '@fontsource/outfit/900.css';
import 'vue3-toastify/dist/index.css';
import App from './App.vue';
import { Component, createApp, h } from 'vue';
import Comuni from './ComuniItaliani.vue';
import { ContainerTypes, getContainer, getOwnershipForAllContainers } from '@/ContainersConfig';
import { ActivationPeriodsForCrfStorage } from '../src/storages/ActivationPeriodsForCrfStorage';
import { InMemoryCacheStorageForTests } from '../test/mock-repositories/InMemoryCacheStorageForTests';
import { CustomExportStorage } from '../test/mock-repositories/CustomExportStorage';
import { ReminderStorage } from '../test/mock-repositories/ReminderStorage';
import { ReminderService } from '../test/mock-services/ReminderService';
import { UserService } from '../test/mock-services/UserService';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { CaseListService, ReadonlyCaseListElement, setupDandelion } from '@/index';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import AfterChangedVariableHookComponent from '@/hooks/AfterChangedVariableHookComponent.vue';
import ListChangesHookComponent from '@/hooks/ListChangesHookComponent.vue';
import ValueFakerHookComponent from '@/hooks/ValueFakerHookComponent.vue';
import CaseReevaluationService from '@/services/CaseReevaluationService';
import { CRFVersionSelectionStrategy } from '@/services/CRFVersionSelectionStrategy';
import CurrentContextService from '@/services/CurrentContextService';
import type { DataSynchService } from '@/services/DataSynchService';
import { FreezeStrategy } from '@/services/FreezeStrategy';
import { ConsoleLogger } from '@/services/Logger';
import { SchedulerService } from '@/services/SchedulerService';
import { Language } from '@/services/TranslationService';
import { CrfServiceFromFolder } from '@/storages/CrfServiceFromFolder';
import { LocalCaseStorage } from '@/storages/LocalCaseStorage';
import { LocalStorageForEvents } from '@/storages/LocalStorageForEvents';
import { LocalDataSynchStorage } from '@/storages/LocalDataSynchStorage';
import { LocalSynchEventsStorage } from '@/storages/LocalSynchEventsStorage';
import { LocalKeyStorage } from '@/storages/LocalKeyStorage';
import { RESTCaseStorage } from '@/storages/RESTCaseStorage';
import { RESTKeyStorage } from '@/storages/RESTKeyStorage';
import { RESTStorageForEvents } from '@/storages/RESTStorageForEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { TypesOfDisplayHooks } from '@/types/TypesOfHooks';

const app = createApp({
  render: () => h(App),
});

const caseListColumns = [
  'Admission.textVariable',
  'Admission.mainDcInstance.singlechoiceVariable',
  'Admission.nome',
  'Admission.admissionDate',
  'Admission.vivoMorto',
];

setupDandelion({
  coreConfiguration: new CoreConfiguration({
    baseUrlForXslt: '/xslt/',
    baseUrlForCases: 'http://localhost:4679/Rows/',
    baseUrlForEvents: 'http://localhost:4679/Events/',
    baseUrlForKeys: 'http://localhost:4690/Keys/',
    baseUrlForAuth: 'http://localhost:5555/Auth/',
    encrypt: false,
    debug: true,
    statisticianMode: false,
    defaultLanguage: Language.it,
    additionalTranslations: {},
    marginForScroll: [10, 1000],
    accessLevelAndCrfConfigurations: [
      { accessLevel: '*', crfName: 'Admission' },
      // { accessLevel: 'OnlyAdmin', crfName: 'Admission' },
      // { accessLevel: 'public', crfName: 'TestSet' },
    ],
    listOfCaseTypes: [
      new CaseConfiguration({
        caseName: 'Admission',
        // childrenCRFs: [{ crfName: 'TestSet' }],
        referenceDateVariableName: 'Admission.admissionDate',
        cachedVariables: caseListColumns,
      }),
    ],

    maxCasesLoadedPerRequest: 20,
    baseUrlForOverrides: '/xslt-overrides/',
    defaultRequiredForStatus: { general: 3, errors: 2, warnings: 2 },
    showDifferencesOnSave: true,
    pluggableDataTypes: {
      comuni: Comuni,
    },
    // actionHooks: new Map<TypesOfActionHooks, ActionHookInterface>()
    //   .set(TypesOfActionHooks.BeforeSaving, {
    //     run: () => Promise.resolve(true),
    //   })
    //   .set(TypesOfActionHooks.BeforeCreating, {
    //     run: () => Promise.resolve(true),
    //   }),
    displayHooks: new Map<TypesOfDisplayHooks, Component[]>()
      .set(TypesOfDisplayHooks.AfterChangedVariable, [AfterChangedVariableHookComponent])
      .set(TypesOfDisplayHooks.AfterVariable, [ValueFakerHookComponent, ListChangesHookComponent]),
  }),
  sharedServices: [
    { name: NamedBindings.CustomExportStorage, class: CustomExportStorage },
    { name: NamedBindings.Logger, class: ConsoleLogger },
    { name: NamedBindings.CRFService, class: CrfServiceFromFolder },
    { name: NamedBindings.UserService, class: UserService },
  ],
  services: [
    {
      name: NamedBindings.ActivationPeriodForCentreCodeStorage,
      class: ActivationPeriodsForCrfStorage,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CRFVersionSelectionStrategy,
      class: CRFVersionSelectionStrategy,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.FreezeStrategy,
      class: FreezeStrategy,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.VariablesCacheStorage,
      class: InMemoryCacheStorageForTests,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.StorageForEvents,
      class: LocalStorageForEvents,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.KeyStorage,
      class: LocalKeyStorage,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CaseStorage,
      class: LocalCaseStorage,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    // {
    //   name: NamedBindings.StorageForEvents,
    //   class: RESTStorageForEvents,
    //   containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    // },
    // {
    //   name: NamedBindings.KeyStorage,
    //   class: RESTKeyStorage,
    //   containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    // },
    // {
    //   name: NamedBindings.CaseStorage,
    //   class: RESTCaseStorage,
    //   containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    // },
    {
      name: NamedBindings.CaseReevaluationService,
      class: CaseReevaluationService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.DataSynchStorage,
      class: LocalDataSynchStorage,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.StorageForSynchEvents,
      class: LocalSynchEventsStorage,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: 'ReminderService',
      class: ReminderService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: 'ReminderStorage',
      class: ReminderStorage,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
  ],
});

const container = getContainer();
const caseService = getContainer(Contexts.DEFAULT).get(NamedBindings.CaseService) as CaseServiceInterface;
const currentContextService = getContainer(Contexts.DEFAULT).get(
  NamedBindings.CurrentContextService
) as CurrentContextService;
const freezeStrategy = new FreezeStrategy(caseService, currentContextService); //TODO Need to be done for all case types
freezeStrategy.activate();
(container.get(NamedBindings.DataSynchService) as DataSynchService).startSynchCycle(30);
getOwnershipForAllContainers();
// (getContainer(Contexts.SHARED).get(NamedBindings.Scheduler) as SchedulerService).startPeriodicReevaluation();
const caseListService = container.get(NamedBindings.CaseListService) as CaseListService;
caseListService.setFilters([
  {
    apply(value: ReadonlyCaseListElement): boolean {
      if (value.cache.get('Admission.textVariable')?.startsWith('')) {
        return true;
      }
      return false;
    },
    isActive(): boolean {
      return true;
    },
  },
]);
app.mount('#app');

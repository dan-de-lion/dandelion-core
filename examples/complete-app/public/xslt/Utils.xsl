<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="createCleanedRoot">
        <xsl:choose>
            <xsl:when test="matches($root, '^model.')">
                <xsl:value-of select="substring-after($root, 'model.')" />
            </xsl:when>
            <xsl:when test="$root = 'model'">
                <xsl:value-of select="''" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!-- ######## STRING MANIPULATION ######## -->

    <xsl:template name="translateString">
        <xsl:param name="string" />

        <xsl:variable name="escapedString">
            <xsl:call-template name="escape">
                <xsl:with-param name="string" select="$string" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="translation" select="concat('translationService.t(', $apos, $escapedString, $apos, ')')" />
        <span>
            <xsl:attribute name="v-html">
                <xsl:value-of select="normalize-space($translation)" disable-output-escaping="yes" />
            </xsl:attribute>
        </span>
    </xsl:template>

    <xsl:template name="escape">
        <xsl:param name="string" />

        <!-- Escape the backslashes first... -->
        <xsl:variable name="firstEscape" select="replace($string, '\', '\\')" />

        <!-- ... then escapes the apostrophes -->
        <xsl:value-of select="replace($firstEscape, $apos, concat('\', $apos))" disable-output-escaping="yes" />
    </xsl:template>

    <xsl:template name="addModelBeforeFullName">
        <xsl:param name="fullName" />
        <xsl:value-of select="concat('model.', $fullName)" />
    </xsl:template>

    <xsl:template name="protectFullName">
        <xsl:param name="fullName" />
        <xsl:value-of select="replace($fullName, '\.', '?.')" />
    </xsl:template>

    <xsl:template name="createElementName">
        <xsl:param name="fullName" />
        <xsl:param name="setLevel" />
        <xsl:value-of select="concat($fullName, '[', $apos, ' + index', $setLevel, ' + ', $apos , ']')" />
    </xsl:template>

    <xsl:template name="selectFullName">
        <xsl:param name="fullName" />

        <xsl:choose>
            <xsl:when test="not($fullName)">
                <xsl:value-of select="$cleanedRoot" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fullName" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="selectParent">
        <xsl:param name="parent" />

        <xsl:choose>
            <xsl:when test="not($parent) and $cleanedRoot != ''">
                <xsl:value-of select="concat($cleanedRoot, '.')" />
            </xsl:when>
            <xsl:when test="not($parent) and $cleanedRoot = ''">
                <xsl:value-of select="''" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($parent, '.')" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../Utils.xsl" />
    <xsl:import href="../VariableAttributes.xsl" />
    <xsl:import href="../VariableStructure.xsl" />
    <xsl:import href="../Types/AccessLevels/AccessLevelsBaseTypes.xsl" />
    <xsl:import href="../Types/AccessLevels/AccessLevelsDataClassTypes.xsl" />

    <xsl:variable name="apos">'</xsl:variable>

    <xsl:param name="root" />
    <xsl:param name="crfName" />
    <xsl:param name="crfVersion" />

    <xsl:variable name="cleanedRoot">
        <xsl:call-template name="createCleanedRoot" />
    </xsl:variable>

    <xsl:output method="html" indent="yes" />

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../Types/ItemsDataClass/GetAllItemsDataClass.xsl" />
    <xsl:import href="../Utils.xsl" />
    <xsl:output method="text" indent="yes" />

    <xsl:param name="root" />

    <xsl:variable name="cleanedRoot">
        <xsl:call-template name="createCleanedRoot" />
    </xsl:variable>
</xsl:stylesheet>

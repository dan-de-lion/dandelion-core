<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->

    <xsl:template match="dataClass">
        <xsl:param name="parent" />

        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:apply-templates select="variable[(@accessLevel or .//variable[@accessLevel]) and not(@dataClass)]">
            <xsl:with-param name="parent" select="$selectedParent" />
        </xsl:apply-templates>

        <xsl:apply-templates select="variable[@dataClass]" mode="filterDataClasses">
            <xsl:with-param name="parent" select="$selectedParent" />
        </xsl:apply-templates>

    </xsl:template>

    <xsl:template match="variable [ @dataType= 'group' and (@dataClass = '' or not(@dataClass))] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        "<xsl:value-of select="@name" />" : {
        <xsl:if test="@accessLevel">"accessLevel": "<xsl:value-of select="@accessLevel" />",
        </xsl:if>
        <xsl:apply-templates select="./variable [ .//@accessLevel ]">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>
        },
    </xsl:template>

    <xsl:template match="variable [ @dataType= 'group' and @dataClass != ''] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="dataClass" select="@dataClass" />
            "<xsl:value-of select="@name" />" : {
                <xsl:if test="@accessLevel">"accessLevel": "<xsl:value-of select="@accessLevel" />",
                </xsl:if>

                <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                    <xsl:with-param name="parent" select="$fullName" />
                </xsl:apply-templates>
            },
    </xsl:template>


<!--    Search for Access Level -->
    <xsl:template match="dataClass" mode="searchForAccessLevel">
        <xsl:apply-templates select="variable" mode="searchForAccessLevel" />
    </xsl:template>

    <xsl:template match="variable [ @dataType= 'group' and @dataClass != ''] " mode="searchForAccessLevel">
        <xsl:variable name="dataClass" select="@dataClass" />

        <xsl:choose>
            <xsl:when test="@accessLevel">true</xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="//dataClass[ @name = $dataClass ]" mode="searchForAccessLevel" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'group' ]" mode="searchForAccessLevel">
            <xsl:if test="@accessLevel">true</xsl:if>
    </xsl:template>

    <!-- Filters for nested DataClass -->
    <xsl:template match="variable" mode="filterDataClasses">
        <xsl:variable name="stringHasChildrenWithAccessLevel">
            <xsl:apply-templates select="." mode="searchForAccessLevel" />
        </xsl:variable>
        <xsl:variable name="hasChildrenWithAccessLevel" select="normalize-space($stringHasChildrenWithAccessLevel)" />

        <xsl:if test="$hasChildrenWithAccessLevel != ''">
            <xsl:apply-templates select="." />
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>

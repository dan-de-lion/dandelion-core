<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="variable [ @dataType= 'group' and @dataClass = ''] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        {
            "fullName" : "<xsl:value-of select="concat($root, '.', $fullName)" />",
            "type": "<xsl:value-of select="@dataType" />"
        },
        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>

        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="fullName" select="concat($fullName, '.')" />
        </xsl:apply-templates>

    </xsl:template>

    <xsl:template match="variable [ @dataType= 'group' and @dataClass != ''] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="dataClass" select="@dataClass" />
        {
        "fullName" : "<xsl:value-of select="concat($root, '.', $fullName)" />",
        "type": "<xsl:value-of select="@dataType" />",
        "dataClass": "<xsl:value-of select="$dataClass" />"
        },
        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>

        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="fullName" select="concat($fullName, '.')" />
        </xsl:apply-templates>

        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>

    </xsl:template>

    <xsl:template match="variable [ @dataType= 'set' ] ">
        <xsl:param name="parent" />

        <xsl:variable name="dataClass" select="@itemsDataClass" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>
        {
            "fullName" : "<xsl:value-of select="concat($root, '.', $fullName)" />",
            "type" : "set",
            "itemsDataClass" : "<xsl:value-of select="$dataClass" />"
        },
    </xsl:template>
</xsl:stylesheet>

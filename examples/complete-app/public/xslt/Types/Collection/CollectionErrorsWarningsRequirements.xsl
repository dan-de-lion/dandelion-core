<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## ERRORS and WARNINGS ########## -->

    <xsl:template match="error">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:call-template name="createError">
            <xsl:with-param name="errorName" select="concat($parentName, 'ERROR_', @name)" />
            <xsl:with-param name="errorMessage" select="." />
            <xsl:with-param name="ifCondition" select="@if" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="warning">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:variable name="warningName" select="concat($parentName, 'WARNING_', @name, '_CONFIRMED')" />

        <div
            v-if="{ @if } &amp;&amp;
            { concat('userService.canRead(', $apos,  $warningName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')') }"
            class="warning"
        >

            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $warningName, $apos, ',', $apos, $crfName, $apos,',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="concat( $withReferenceID, '.WARNING_REF_', @name, '-wrapper')" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="concat($warningName, '-wrapper')" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>

            <input type="checkbox" v-custom-model-for-warnings="'{$warningName}'">
                <xsl:attribute
                    name=":disabled"
                    select="concat('!userService.canWrite(', $apos, $warningName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                />
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="concat( $withReferenceID, '.WARNING_REF_', @name)" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$warningName" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>
            </input>
            <label>
                <xsl:attribute name=":for" select="concat('getContextForID + ', $apos, $warningName, $apos)" />
                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">I confirm the inserted data</xsl:with-param>
                </xsl:call-template>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="requirement">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:variable name="requirementName" select="concat( $parentName, 'REQUIREMENT_', @name)" />

        <div
            class="requirement"
            v-if="!({ @satisfiedIf })"
            v-custom-model-for-requirements="'{$requirementName}'"
            init-value="null"
        >
            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $requirementName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="concat( $withReferenceID, '.REQUIREMENT_REF_', @name)" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$requirementName" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="description">
        <p class="variableDescription">
            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
        </p>
    </xsl:template>

</xsl:stylesheet>

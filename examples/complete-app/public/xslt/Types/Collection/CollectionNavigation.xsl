<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ###### NAVIGATION FOR PAGES ##### -->
    <!-- construct buttons for page navigation, for each child variable -->

    <xsl:template match="variable" mode="nav">
        <xsl:param name="parent" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <button type="button">
            <xsl:attribute
                name="v-on:click.stop"
                select="concat('currentPageService.set(getContextForID + ', $apos, $fullName, $apos, ')')"
            />
            <xsl:attribute
                name=":class"
                select="concat('{selected: ', concat('listOfActivePages.includes(getContextForID + ', $apos, $fullName, $apos, ')'), '}')"
            />
            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
            <xsl:call-template name="getTranslatedLabel" />
        </button>
    </xsl:template>

</xsl:stylesheet>

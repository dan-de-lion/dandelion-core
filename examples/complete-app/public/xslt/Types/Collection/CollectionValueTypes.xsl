<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="value" mode="checkbox">

        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="myValue">
            <xsl:choose>
                <xsl:when test="$valuesSetName != ''">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '-', $myValue)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="concat($variableName, '-', $myValue)" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="groupName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="$withReferenceID" />                
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="$variableName" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="name" select="@name" />

        <xsl:variable name="arrayOfExcludes">

            <xsl:apply-templates select="./excludes" />

            <xsl:for-each select="../value[@name!=$name and ./excludes='*']/@name">
                <xsl:value-of select="concat(',',.)">
                </xsl:value-of>
            </xsl:for-each>

            <!-- TODO: transform in IF or template on ./excludes/*: we already are on ../value[@name=$name] node ;-) -->
            <xsl:for-each select="../value[@name=$name and ./excludes='*']">

                <xsl:variable select="@name" name="x" />
                <xsl:for-each select="../value[@name!=$x]/@name">

                    <xsl:value-of select="concat(',',.)">
                    </xsl:value-of>
                </xsl:for-each>
            </xsl:for-each>

            <xsl:for-each select="../value[@name!=$name and ./excludes=$name]/@name">

                <xsl:value-of select="concat(',',.)">
                </xsl:value-of>
            </xsl:for-each>

        </xsl:variable>

        <li>
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <input type="checkbox" value="{ $myValue }" excludes="{$arrayOfExcludes}">
                <xsl:attribute name=":name" select="concat('getContextForID + ', $apos , $groupName, $apos)" />

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="$computedFormula and $computedFormula != ''">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', $computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:if test="@label">
                    <xsl:attribute name="my-label" select="@label" />
                </xsl:if>

                <xsl:if test="@numericalValue">
                    <xsl:attribute name="my-numerical-value" select="@numericalValue" />
                </xsl:if>
            </input>

            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="separatedTooltip" select="'true'" />
                <xsl:with-param name="cssClass" select="'valueLabel'" />
            </xsl:call-template>

        </li>

    </xsl:template>


    <xsl:template match="value" mode="select">

        <xsl:param name="valuesSetName" />

        <xsl:variable name="myValue">
            <xsl:choose>
                <xsl:when test="$valuesSetName != ''">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <option value="{ $myValue }">

            <xsl:call-template name="getTranslatedLabel" />

            <xsl:if test="@label">
                <xsl:attribute name="my-label" select="@label" />
            </xsl:if>

            <xsl:if test="@numericalValue">
                <xsl:attribute name="my-numerical-value" select="@numericalValue" />
            </xsl:if>

        </option>

    </xsl:template>


    <xsl:template match="value" mode="radio">

        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="myValue">
            <xsl:choose>
                <xsl:when test="$valuesSetName != ''">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '-', $myValue)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="concat($variableName, '-', $myValue)" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="groupName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="$withReferenceID" />
            </xsl:if>
           <xsl:if test="$withReferenceID = ''">
                 <xsl:value-of select="$variableName" />
            </xsl:if>
        </xsl:variable>

        <li>
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <input type="radio" value="{ $myValue }">
                <xsl:attribute name=":name" select="concat('getContextForID + ', $apos , $groupName, $apos)" />

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="$computedFormula and $computedFormula != ''">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', $computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:if test="@label">
                    <xsl:attribute name="my-label" select="@label" />
                </xsl:if>

                <xsl:if test="@numericalValue">
                    <xsl:attribute name="my-numerical-value" select="@numericalValue" />
                </xsl:if>

            </input>

            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="separatedTooltip" select="'true'" />
                <xsl:with-param name="cssClass" select="'valueLabel'" />
            </xsl:call-template>

        </li>

    </xsl:template>

    <xsl:template match="valuesGroup" mode="radio">

        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="groupValuesSetName" select="concat($valuesSetName, '.', @name)" />

        <h4>
            <xsl:call-template name="getTranslatedLabel" />
        </h4>

            <xsl:apply-templates select="value" mode="radio">

                    <xsl:with-param name="valuesSetName" select="$groupValuesSetName" />

                    <xsl:with-param name="variableName" select="$variableName" />

                    <xsl:with-param name="computedFormula" select="$computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

            </xsl:apply-templates>

    </xsl:template>

    <xsl:template match="valuesGroup" mode="checkbox">

        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="groupValuesSetName" select="concat($valuesSetName, '.', @name)" />

        <h4>
            <xsl:call-template name="getTranslatedLabel" />
        </h4>

            <xsl:apply-templates select="value" mode="checkbox">

                    <xsl:with-param name="valuesSetName" select="$groupValuesSetName" />

                    <xsl:with-param name="variableName" select="$variableName" />

                    <xsl:with-param name="computedFormula" select="$computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

            </xsl:apply-templates>

    </xsl:template>

    <xsl:template match="valuesGroup" mode="select">

        <xsl:param name="valuesSetName" />

        <xsl:variable name="groupValuesSetName" select="concat($valuesSetName, '.', @name)" />

        <optgroup>

            <xsl:attribute name="label">
                <xsl:choose>
                    <xsl:when test="@label and @label != ''">
                        <xsl:value-of select="@label" />
                    </xsl:when>
                    <xsl:when test="@name">
                        <xsl:value-of select="@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'-- this variable has no label or name! --'" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>

            <xsl:apply-templates select="value" mode="select">

                    <xsl:with-param name="valuesSetName" select="$groupValuesSetName" />

            </xsl:apply-templates>

        </optgroup>


    </xsl:template>

</xsl:stylesheet>

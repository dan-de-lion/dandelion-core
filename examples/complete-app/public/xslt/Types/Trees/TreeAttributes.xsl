<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template name="applyVisibilityForTree">
        <xsl:param name="fullName" />
        <xsl:choose>
            <xsl:when test="@visibleIf and @visibleIf != '' ">
                <xsl:attribute name="v-if">
                    <xsl:value-of
                        select="concat('userService?.canRead(', $apos, $fullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,') &amp;&amp; (', @visibleIf, ')')"
                    />
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="v-if">
                    <xsl:value-of
                        select="concat('userService?.canRead(', $apos, $fullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')')"
                    />
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'output' ]">
        <xsl:if test="not (@computedFormula)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('Computed Formula missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
            </li>
        </xsl:if>

        <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
            <li class="developerError">
                <xsl:value-of
                    select="concat('requiredForStatus should be specified and set to ', $apos, 'false', $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
            </li>
        </xsl:if>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'support'] ">
        <xsl:if test="not (@computedFormula)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('Computed Formula missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
            </li>
        </xsl:if>

        <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
            <li class="developerError">
                <xsl:value-of
                    select="concat('requiredForStatus should be specified and set to ', $apos, 'false', $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
            </li>
        </xsl:if>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'singlechoice']">
        <xsl:if test="not(@valuesSet) and not(./valuesGroup) and not(./value)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('ValuesSet and Value missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
            </li>
        </xsl:if>

        <xsl:variable name="valuesSetName" select="@valuesSet" />

        <xsl:if test="//valuesSet[ @name = $valuesSetName ]/value/excludes">
            <li class="developerError">
                <xsl:value-of
                    select="concat('Value in valuesSet ', $apos, @valuesSet, $apos, ' in variable ', $apos, @name, $apos, ' shouldn', $apos, 't have excludes tag. Do you intend multiplechoice instaed?')"
                />
            </li>
        </xsl:if>

        <xsl:if test="./valuesGroup and ./value">
            <li class="developerError">
                <xsl:value-of
                    select="concat('There is a valuesGroup and a value at the same level inside variable ', $apos, @name , $apos, ' that is a type ', @dataType )"
                />
            </li>
        </xsl:if>

        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'multiplechoice']">
        <xsl:if test="not(@valuesSet) and not(./valuesGroup) and not(./value)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('ValuesSet and Value missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
            </li>
        </xsl:if>

        <xsl:if test="./valuesGroup and ./value">
            <li class="developerError">
                <xsl:value-of
                    select="concat('There is a valuesGroup and a value at the same level inside variable ', $apos, @name , $apos, ' that is a type ', @dataType )"
                />
            </li>
        </xsl:if>

        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template match="valuesSet">
        <xsl:apply-templates select="value" />
        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template match="valuesSet[ @accessLevel ]">
        <li class="developerError">
            <xsl:value-of select="concat('ValuesSet ', $apos, @name, $apos, '  cannot contain accessLevel')" />
        </li>
    </xsl:template>

    <xsl:template match="valuesSet[contains(@name, '.')]">
        <li class="developerError">
            <xsl:value-of select="'Name in a ValuesSet should not contain dots'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="valuesSet[./valuesGroup and ./value]">
        <li class="developerError">
            <xsl:value-of
                select="concat('There is a valuesGroup and a value at the same level inside valuesSet ', $apos, @name , $apos )"
            />
        </li>
    </xsl:template>

    <xsl:template match="valuesGroup">
        <xsl:if test="not(@name)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('There is a valuesGroup without a name inside variable ', $apos, ../@name , $apos )"
                />
            </li>
        </xsl:if>

        <xsl:if test="not(./value)">
            <li class="developerError">
                <xsl:value-of select="concat('No values found inside valuesGroup ', $apos, @name, $apos)" />
            </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'set' ]">

        <xsl:if test="not (@itemsDataClass)">
            <li class="developerError">
                <xsl:value-of
                    select="concat($apos, 'ItemsDataClass' , $apos, ' missing in variable ', $apos, @name, $apos, ' that is a type ', $apos, 'set' , $apos)"
                />
            </li>
        </xsl:if>

        <xsl:if test="@itemsDataClass">

            <xsl:variable name="dataType" select="@itemsDataClass" />

            <xsl:if test="@separateInPages and not(//dataClass[ @name = $dataType ]/variable/@itemLabel)">
                <li class="developerError">
                    <xsl:value-of
                        select="concat($apos, 'ItemLabel', $apos, ' missing in dataClass ', $apos, @itemsDataClass, $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', $apos, 'set', $apos)"
                    />
                </li>
            </xsl:if>

            <xsl:if test="@computedItems and not(//dataClass[ @name = $dataType ]/variable/@itemKey)">
                <li class="developerError">
                    <xsl:value-of
                        select="concat($apos, 'ItemKey', $apos, ' missing in dataClass ', $apos, @itemsDataClass, $apos, ' in variable ', $apos, @name, $apos, 'that is a type ', $apos, 'set', $apos)"
                    />
                </li>
            </xsl:if>

        </xsl:if>

    </xsl:template>

    <xsl:template match="variable[@itemKey and @itemKey != 'true']">
        <li class="developerError">
            <xsl:value-of select="'itemKey should always be true'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[not (@name)]">
        <li class="developerError">
            <xsl:value-of select="concat('Name missing in a variable that is a type ', @dataType)" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@name = '']">
        <li class="developerError">
            <xsl:value-of select="concat('Name in a variable that is a type ', @dataType, ' should not be empty')" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@visibleIf = '']">
        <li class="developerError">
            <xsl:value-of select="'VisibleIf in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@computedFormula = '']">
        <li class="developerError">
            <xsl:value-of select="'ComputedFormula in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType = '']">
        <li class="developerError">
            <xsl:value-of select="'DataType in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@valuesSet = '']">
        <li class="developerError">
            <xsl:value-of select="'ValuesSet in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@itemsDataClass = '']">
        <li class="developerError">
          <xsl:value-of select="'ItemsDataClass in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[not (@dataType)]">
        <li class="developerError">
            <xsl:value-of select="concat('DataType missing in variable ', $apos, @name, $apos)" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'number' and (@max or @min or @precision or @measureUnit)] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Number Attributes should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'text' and  @interface = 'textArea'] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Interface ', $apos, 'textArea', $apos, ' should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'singlechoice' and @dataType != 'multiplechoice' and  @valuesSet] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('ValuesSet should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'date' and  (@minDate or @maxDate)] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Date attributes should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'group' and @dataType != 'set' and  @separateInPages] ">
        <!--Add
        variable instance of dataClass-->
        <li class="developerError">
            <xsl:value-of
                select="concat('SeparateInPages should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'set' and  @itemsDataClass] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('ItemsDataClass should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'group' and ./variable] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('There shouldn', $apos, 't be other variables as children of variable ', $apos, @name, $apos, ' that is a type ', @dataType, ' and not a type ', $apos, 'group.', $apos, ' Maybe tag variable is not closed?')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'text' and @pattern] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Pattern attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @pattern and (@pattern = '' or normalize-space(@pattern) = ' ')] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Pattern attribute should not be empty in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'plugin']">
        <xsl:variable name="regexPattern">^[a-z0-9_]+$</xsl:variable>
        <xsl:choose>
            <xsl:when test="matches(@interface, $regexPattern)">
                <!-- The string respect the regex -->
            </xsl:when>
            <xsl:otherwise>
                <!-- The string respect the regex, generate a message of error -->
                <li class="developerError">
                    <xsl:value-of
                        select="concat('Error: the interface value does not respect regex ', $apos, $regexPattern, $apos, '. The interface should not have upper case or special characters')"
                    />
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="variable[ @ignoreCRFErrors = 'true' ]">
    </xsl:template>

    <xsl:template match="variable[contains(@name, '.')]">
        <li class="developerError">
            <xsl:value-of
                select="concat('Name in a variable that is a type', $apos, @name, $apos, ' should not contain dots')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeRadioNotDefined" mode="radio" />
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeSelectNotDefined" mode="select" />
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeCheckboxNotDefined" mode="checkbox" />

</xsl:stylesheet>

﻿<?xml version="1.0" encoding="utf-8"?>

<xs:schema id="XMLSchema"
           elementFormDefault="qualified"
           xmlns:xs="http://www.w3.org/2001/XMLSchema">

    <xs:element name="root" type="rootType"/>

    <xs:simpleType name="baseDataType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="label"/>
            <xs:enumeration value="text"/>
            <xs:enumeration value="number"/>
            <xs:enumeration value="email"/>
            <xs:enumeration value="date"/>
            <xs:enumeration value="boolean"/>
            <xs:enumeration value="singlechoice"/>
            <xs:enumeration value="multiplechoice"/>
            <xs:enumeration value="set"/>
            <xs:enumeration value="group"/>
            <xs:enumeration value="relationships"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="complexDataType">
        <xs:restriction base="xs:string"/>
    </xs:simpleType>

    <xs:simpleType name="BaseDateType">
        <xs:restriction base="xs:date"/>
    </xs:simpleType>

    <xs:simpleType name="complexDateType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="today"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="BaseIntType">
        <xs:restriction base="xs:int"/>
    </xs:simpleType>

    <xs:simpleType name="complexIntType">
        <xs:restriction base="xs:string">
        </xs:restriction>
    </xs:simpleType>

    <!--Check name based on JS naming convention. -->
    <xs:attribute name="name">
        <xs:simpleType>
            <xs:restriction base="xs:string">
                <xs:pattern value="[a-z]([a-zA-Z0-9])*"/>
            </xs:restriction>
        </xs:simpleType>
    </xs:attribute>

    <xs:attributeGroup name="dateGroup">
        <xs:attribute name="maxDate">
            <xs:simpleType>
                <xs:union memberTypes="BaseDateType complexDateType"/>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="minDate">
            <xs:simpleType>
                <xs:union memberTypes="BaseDateType complexDateType"/>
            </xs:simpleType>
        </xs:attribute>
    </xs:attributeGroup>

    <xs:attributeGroup name="numberGroup">
        <xs:attribute name="min">
            <xs:simpleType>
                <xs:union memberTypes="BaseIntType complexIntType"/>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="max">
            <xs:simpleType>
                <xs:union memberTypes="BaseIntType complexIntType"/>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="precision">
            <xs:simpleType>
                <xs:union memberTypes="BaseIntType complexIntType"/>
            </xs:simpleType>
        </xs:attribute>
    </xs:attributeGroup>

    <xs:attributeGroup name="valuesSetGroup">
        <xs:attribute type="xs:string" name="valuesSet"/>
    </xs:attributeGroup>

    <xs:attributeGroup name="indexGroup">
        <xs:attribute type="xs:string" name="indexName"/>
        <xs:attribute type="xs:string" name="indexClass"/>
    </xs:attributeGroup>

    <xs:complexType name="variableType" mixed="true">
        <xs:choice maxOccurs="unbounded">
            <xs:element type="variableType" name="variable" maxOccurs="unbounded" minOccurs="0"/>
            <xs:element type="errorOrWarningType" name="error" maxOccurs="unbounded" minOccurs="0"/>
            <xs:element type="errorOrWarningType" name="warning" maxOccurs="unbounded" minOccurs="0"/>
        </xs:choice>
        <xs:attribute type="xs:string" name="name" use="required"/>
        <xs:attribute name="dataType" use="required">
            <xs:simpleType>
                <xs:union memberTypes="baseDataType complexDataType"/>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute type="xs:string" name="label"/>
        <xs:attribute type="xs:string" name="tooltip"/>
        <xs:attribute type="xs:string" name="required"/>
        <xs:attribute type="xs:string" name="visibleIf"/>
        <xs:attribute type="xs:string" name="hideMode"/>
        <xs:attribute type="xs:string" name="requiredForStatus"/>
        <xs:attribute type="xs:string" name="computedFormula"/>
        <xs:attribute type="xs:string" name="itemsDataClass"/>
        <xs:attribute type="xs:string" name="relationsElements"/>
        <xs:attribute type="xs:string" name="elementsDataType"/>
        <!--In XSD 1.0 "co-occurrence constraints" cannot be done, so all group are present at the same time -->
        <xs:attributeGroup ref="dateGroup"/>
        <xs:attributeGroup ref="numberGroup"/>
        <xs:attributeGroup ref="valuesSetGroup"/>
        <xs:attributeGroup ref="indexGroup"/>
        <xs:attribute name="interface">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="checkBox"/>
                    <xs:enumeration value="radio"/>
                    <xs:enumeration value="dropdown"/>
                    <xs:enumeration value="autocomplete"/>
                    <xs:enumeration value="textArea"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="separateInPages">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="true"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="itemKey">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="true"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="itemLabel">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="true"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="itemColor">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="true"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
        <xs:attribute name="labelFromKey">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="true"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="dataClassType">
        <xs:choice maxOccurs="unbounded">
            <xs:element type="variableType" name="variable" maxOccurs="unbounded" minOccurs="0"/>
            <xs:element type="errorOrWarningType" name="error" maxOccurs="unbounded" minOccurs="0"/>
            <xs:element type="errorOrWarningType" name="warning" maxOccurs="unbounded" minOccurs="0"/>
            <xs:element type="requirementType" name="requirement" maxOccurs="unbounded" minOccurs="0"/>
        </xs:choice>
        <xs:attribute type="xs:string" name="name" use="required"/>
    </xs:complexType>

    <xs:complexType name="valueType" mixed="true">
        <xs:sequence>
            <xs:element type="xs:string" name="excludes" maxOccurs="unbounded" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute type="xs:string" name="name" use="required"/>
        <xs:attribute type="xs:string" name="label"/>
        <xs:attribute type="xs:string" name="tooltip"/>
        <xs:attribute type="xs:string" name="visibleIf"/>
    </xs:complexType>

    <xs:complexType name="valuesSetType">
        <xs:sequence>
            <xs:element type="valueType" name="value" maxOccurs="unbounded" minOccurs="0"/>
            <xs:element type="xs:string" name="class" minOccurs="0"/>
            <xs:element type="xs:string" name="name" minOccurs="0"/>
            <xs:element type="xs:string" name="table" minOccurs="0"/>
            <xs:element type="xs:string" name="column" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute type="xs:string" name="name" use="required"/>
        <xs:attribute name="sourceType">
            <xs:simpleType>
                <xs:restriction base="xs:string">
                    <xs:enumeration value="table"/>
                    <xs:enumeration value="index"/>
                </xs:restriction>
            </xs:simpleType>
        </xs:attribute>
    </xs:complexType>

    <xs:complexType name="errorOrWarningType" mixed="true">
        <xs:attribute type="xs:string" name="if" use="required"/>
        <xs:attribute type="xs:string" name="name" use="required"/>
        <xs:attribute type="xs:string" name="requiredForStatus"/>
    </xs:complexType>

    <xs:complexType name="requirementType" mixed="true">
        <xs:attribute type="xs:string" name="satisfiedIf" use="required"/>
        <xs:attribute type="xs:string" name="name" use="required"/>
        <xs:attribute type="xs:string" name="requiredForStatus" use="required"/>
    </xs:complexType>

    <xs:complexType name="rootType">
        <xs:sequence>
            <xs:element type="dataClassType" name="dataClass" maxOccurs="unbounded"/>
            <xs:element type="valuesSetType" name="valuesSet" maxOccurs="unbounded" minOccurs="0"/>
        </xs:sequence>
    </xs:complexType>

</xs:schema>

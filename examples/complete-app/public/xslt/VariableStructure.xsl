<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## VARIABLES COMPACT ATTRIBUTES ######## -->

    <xsl:template name="wrapperAttributes">
        <xsl:param name="fullName" />
        <xsl:param name="separatedTooltip" select="'true'" />
        <xsl:param name="element" select="'label'" />
        <xsl:param name="cssLabelClass" select="'variableLabel'" />
        <xsl:param name="withReferenceID" />

        <span>
            <xsl:attribute name="v-if">hooksExist(TypesOfHooks.BeforeVariable)</xsl:attribute>
            <xsl:attribute
                name="v-html"
                select="concat('runHooks(', $apos, $fullName, $apos, ', TypesOfHooks.BeforeVariable)')"
            />
        </span>

        <xsl:attribute name="v-if">
            <xsl:value-of
                select="concat('userService.canRead(', $apos,  $fullName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
            />
        </xsl:attribute>

        <xsl:call-template name="applyVisibility">
            <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>

        <xsl:call-template name="addWrapperID">
            <xsl:with-param name="fullName">
                <xsl:if test="$withReferenceID != ''">
                    <xsl:value-of select="$withReferenceID" />
                </xsl:if>
                <xsl:if test="$withReferenceID = ''">
                    <xsl:value-of select="$fullName" />
                </xsl:if>
            </xsl:with-param>
        </xsl:call-template>

        <xsl:call-template name="addVariableLabel">
            <xsl:with-param name="fullName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
            <xsl:with-param name="separatedTooltip" select="$separatedTooltip" />
            <xsl:with-param name="cssClass" select="$cssLabelClass" />
            <xsl:with-param name="element" select="$element" />
        </xsl:call-template>

        <xsl:if test="$statisticianMode = 'true'">
            <span class="statistician" v-html="'{$fullName}'" />
        </xsl:if>
    </xsl:template>

    <xsl:template name="inputAttributes">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:apply-templates select="@computedFormula" />

        <xsl:if test="@requiredForStatus">
            <xsl:attribute name="required-for-status">
                <xsl:value-of select="@requiredForStatus" />
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
            <xsl:attribute name="required-for-status">
                <xsl:value-of select="$requiredFromParent" />
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="@initValue">
            <xsl:attribute name="init-value">
                <xsl:value-of select="@initValue" />
            </xsl:attribute>
        </xsl:if>

        <xsl:attribute name="v-custom-model">
            <xsl:value-of select="concat($apos, $fullName, $apos)" />
        </xsl:attribute>

        <xsl:attribute name=":disabled">
            <xsl:choose>
                <xsl:when test="@computedFormula and @computedFormula != '' ">
                    <xsl:value-of
                        select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of
                        select="concat('!userService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                    />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>

        <xsl:call-template name="addID">
            <xsl:with-param name="fullName">
                <xsl:if test="$withReferenceID != ''">
                    <xsl:value-of select="$withReferenceID" />
                </xsl:if>
                <xsl:if test="$withReferenceID = ''">
                    <xsl:value-of select="$fullName" />
                </xsl:if>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="afterVariable">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />

        <span>
            <xsl:attribute name="v-if">hooksExist(TypesOfHooks.AfterVariable)</xsl:attribute>
            <xsl:attribute
                name="v-html"
                select="concat('runHooks(', $apos, $fullName, $apos, ', TypesOfHooks.AfterVariable)')"
            />
        </span>

        <xsl:apply-templates select="./error">
            <xsl:with-param name="parentName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
        <xsl:apply-templates select="./warning">
            <xsl:with-param name="parentName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="parentName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
    </xsl:template>


    <xsl:template name="createError">
        <xsl:param name="errorName" />
        <xsl:param name="errorMessage" />
        <xsl:param name="ifCondition" />
        <xsl:param name="withReferenceID" />

        <div
            class="error"
            v-custom-model-for-errors="'{$errorName}'"
            init-value="null"
            v-if="{ $ifCondition } &amp;&amp; { concat('userService.canRead(', $apos,  $errorName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')') }"
        >

            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $errorName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="concat($withReferenceID, '.ERROR_REF_', @name)" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$errorName" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>
            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="$errorMessage" />
                </xsl:with-param>
            </xsl:call-template>
        </div>
    </xsl:template>
</xsl:stylesheet>

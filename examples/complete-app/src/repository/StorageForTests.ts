import { injectable } from 'inversify'
import { PartialStoredCase } from '@dandelion/core'

import type { CaseStorageInterface } from '@dandelion/core'
import { CaseMetaData } from '@dandelion/core'
import { HistoryPartialStoredCase } from '@dandelion/core'
import { Stringifier } from '../fix-in-core/SerDes'
import _ = require('lodash')

@injectable()
export class StorageForTests implements CaseStorageInterface {
  getCaseHistory(caseID: string, lastK?: number | undefined): Promise<HistoryPartialStoredCase[]> {
    throw new Error('Method not implemented.')
  }
  lock(uuid: string): Promise<Date> {
    throw new Error('Method not implemented.')
  }
  unlock(uuid: string): Promise<void> {
    throw new Error('Method not implemented.')
  }
  private localStorage = window.localStorage

  delete(uuid: string): Promise<PartialStoredCase> {
    if (this.localStorage.getItem(uuid) !== null) {
      const xCase = this.localStorage.getItem(uuid)
      this.localStorage.setItem(`del-${uuid}`, xCase ?? '')
      this.localStorage.removeItem(uuid)
      const result = new Stringifier<PartialStoredCase>().deserialize(xCase!)
      return Promise.resolve(result)
    }
    return Promise.resolve(
      new PartialStoredCase(
        '',
        '',
        new CaseMetaData('', true, new Map(), new Map(), new Date(), new Date(), ''),
        ''
      )
    )
  }

  permanentlyDelete(uuid: string): Promise<string> {
    const myItem = this.localStorage.getItem(uuid)
    if (!myItem) {
      return Promise.resolve('')
    }
    this.localStorage.removeItem(uuid)
    return Promise.resolve(uuid)
  }

  restore(uuid: string): Promise<PartialStoredCase> {
    if (this.localStorage.getItem(`del-${uuid}`) !== null) {
      const xCase = this.localStorage.getItem(`del-${uuid}`)
      this.localStorage.setItem(uuid, xCase ?? '')
      this.localStorage.removeItem(`del-${uuid}`)
      const result = new Stringifier<PartialStoredCase>().deserialize(xCase!)
      return Promise.resolve(result)
    }
    return Promise.resolve(
      new PartialStoredCase(
        '',
        '',
        new CaseMetaData('', true, new Map(), new Map(), new Date(), new Date(), ''),
        ''
      )
    )
  }

  getPartials(caseID: string): Promise<Array<PartialStoredCase>> {
    const partialStoredCases = new Stringifier<PartialStoredCase[]>().deserialize(
      this.localStorage.getItem(`partials-${caseID}`)!
    )

    const resultArray: Array<PartialStoredCase> = []
    partialStoredCases.forEach((partial: PartialStoredCase) => {
      let partialMetaData = partial.metaData

      partialMetaData = { ...partialMetaData, lastUpdate: new Date(partial.metaData.lastUpdate) }
      partialMetaData = {
        ...partialMetaData,
        creationDate: new Date(partial.metaData.creationDate)
      }
      // eslint-disable-next-line no-param-reassign
      partial = { ...partial, metaData: partialMetaData }

      resultArray.push(partial)
    })

    return Promise.resolve(resultArray)
  }

  saveAllPartials(partialStoredCases: Array<PartialStoredCase>): Promise<void> {
    this.localStorage.setItem(
      `partials-${partialStoredCases[0].caseID}`,
      new Stringifier().serialize(_.cloneDeep(partialStoredCases))
    )
    return Promise.resolve()
  }
}

import './assets/main.css'
import {
  NamedBindings,
  setupContainers,
  CoreConfiguration,
  CaseConfiguration,
  CaseListService,
  CaseVariablesCacheService,
  Language
} from '@dandelion/core'
import { CrfServiceFromFolder } from './services/CrfServiceFromFolder'
import { StorageForTests } from './repository/StorageForTests'
import { LocalStorageForEvents } from './repository/LocalStorageForEvents'
import { UserServiceWithWriteRights } from './services/UserServiceWithWriteRights'
import { createApp } from 'vue'
import App from './components/CompleteApp.vue'
import router from './router'
import '../../../public/dandelion-core.css'
import { CRFVersionSelectionStrategy } from '@dandelion/core'
import { CacheStorage } from './repository/CacheStorage'
import { ConsoleLogger } from '../../../src/services/Logger'

const app = createApp(App)

app.use(router)

const container = setupContainers(
  new CoreConfiguration({
    baseURL: '/xslt/',
    defaultLanguage: Language.it,
    marginForScroll: [10, 0],
    listOfCaseTypes: [
      new CaseConfiguration({
        caseName: 'Admission',
        referenceDateVariableName: 'date'
      })
    ],
    debug: true
  }),
  { name: NamedBindings.CRFService, class: CrfServiceFromFolder },
  { name: NamedBindings.CaseStorage, class: StorageForTests },
  { name: NamedBindings.StorageForEvents, class: LocalStorageForEvents },
  { name: NamedBindings.UserService, class: UserServiceWithWriteRights },
  { name: NamedBindings.ListService, class: CaseListService },
  { name: NamedBindings.CRFVersionSelectionStrategy, class: CRFVersionSelectionStrategy },
  { name: NamedBindings.VariablesCacheStorage, class: CacheStorage },
  { name: NamedBindings.Logger, class: ConsoleLogger }
)

;(
  container.get(NamedBindings.CaseVariablesCacheService) as CaseVariablesCacheService
).addNamesOfVariablesToBeCached(['Admission.text1', 'Admission.text2'])

app.mount('#app')

import { createRouter, createWebHistory } from 'vue-router'
import Home from '../components/Home.vue'
import { Contexts } from '@dandelion/core'
import SingleCaseView from '../views/SingleCaseView.vue'
import NewCaseView from '../views/NewCaseView.vue'

const router = createRouter({
  history: createWebHistory('/'),
  routes: [
    {
      path: '/',
      component: Home
    },
    {
      path: '/case/:id',
      component: SingleCaseView
    },
    {
      path: '/new-case',
      component: NewCaseView,
      props: {
        crfName: 'Admission',
        startingDataclass: 'NewCase',
        context: Contexts.DEFAULT
      }
    }
  ]
})

export default router

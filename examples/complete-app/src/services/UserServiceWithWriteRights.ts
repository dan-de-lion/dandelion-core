import type { AccessRights, UserServiceInterface } from '@dandelion/core'
import { injectable } from 'inversify'

@injectable()
export class UserServiceWithWriteRights implements UserServiceInterface {
  getCurrentUser() {
    return 'admin'
  }
  getCurrentCentreCode(): string {
    return 'abc10'
  }
  getHeaders() {}
  hasReadAccessRights(accessLevel: any): boolean {
    return true
  }
  hasWriteAccessRights(accessLevel: any): boolean {
    return true
  }
  canRead(variable: string): boolean {
    return true
  }

  canWrite(variable: string): boolean {
    return true
  }

  getAccessLevelsRights(): Map<string, AccessRights> {
    return new Map()
  }

  getCurrentRoles(): Array<string> {
    return []
  }
}

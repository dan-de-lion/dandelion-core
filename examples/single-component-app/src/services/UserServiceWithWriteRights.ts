import type { AccessRights, UserServiceInterface } from '@dandelion/core';
import { injectable } from 'inversify';

@injectable()
export class UserServiceWithWriteRights implements UserServiceInterface {
  canRead(variable: string): boolean {
    return true;
  }

  canWrite(variable: string): boolean {
    return true;
  }

  getAccessLevelsRights(): Map<string, AccessRights> {
    return new Map().set('public', {
      read_roles: ['myRole'],
      write_roles: ['myRole'],
    });
  }

  getCurrentRoles(): Array<string> {
    return ['myRole'];
  }
}

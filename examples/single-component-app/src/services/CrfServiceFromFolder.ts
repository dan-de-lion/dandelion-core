import { injectable } from 'inversify';
import { CRF, type CRFServiceInterface, CRFVersionValidity, type Version } from '@dandelion/core';

@injectable()
export class CrfServiceFromFolder implements CRFServiceInterface {
  constructor(private pathToFolder: string = import.meta.env.VITE_APP_CRFS_PATH_TO_FOLDER) {
    this.pathToFolder = pathToFolder;
  }

  getCRFActiveVersions(crfName: string): CRFVersionValidity[] {
    return [{ startDate: new Date(1900, 1), version: '0.0.1' }];
  }

  async getCRF(crfName: string, crfVersion: Version = '0.0.1'): Promise<CRF> {
    const pathString = `${this.pathToFolder}/${crfName}/${crfVersion}`;
    const toXml = `${pathString}/${crfName}_${crfVersion}.xml`;
    const toJs = `${pathString}/${crfName}_functions_${crfVersion}.js`;
    const toCss = `${pathString}/${crfName}_style_${crfVersion}.css`;
    const toTranslation = `${pathString}/${crfName}_translations_${crfVersion}.json`;

    return Promise.all([
      fetch(toXml),
      fetch(toJs),
      fetch(toCss, {
        headers: {
          Accept: 'text/css',
        },
      }),
      fetch(toTranslation),
    ])
      .then(async ([xml, js, css, translation]) => {
        if (!xml.ok) {
          throw new Error('XML files is required');
        }

        return new CRF(
          crfName,
          crfVersion,
          xml.ok ? await xml.text() : '',
          js.ok ? await js.text() : '',
          css.ok ? await css.text() : '',
          translation.ok ? await translation.json() : ''
        );
      })
      .catch((error) => Promise.reject(error));
  }

  getLatestVersion(crfName: string): Version {
    return '0.0.1';
  }

  getAccessLevelForCRF(crfName: string, _crfVersion: Version): any {
    return crfName === 'Admission'
      ? {
          Admission: {
            Group: {
              Group2: {
                accessLevel: 'OnlyAdmin',
              },
            },
          },
        }
      : {};
  }
}

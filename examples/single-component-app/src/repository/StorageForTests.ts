import { injectable } from 'inversify';
import { PartialStoredCase, type CaseStorageInterface, CaseMetaData } from '@dandelion/core';
import _ from 'lodash';
import Stringifier from '../fix-in-core/SerDes';

@injectable()
export class StorageForTests implements CaseStorageInterface {
  private localStorage = window.localStorage;

  delete(_uuid: string): Promise<PartialStoredCase | undefined> {
    return Promise.resolve(undefined);
  }

  permanentlyDelete(uuid: string): Promise<string | undefined> {
    return Promise.resolve(uuid);
  }

  restore(_uuid: string): Promise<PartialStoredCase | undefined> {
    return Promise.resolve(undefined);
  }

  getPartials(caseID: string): Promise<Array<PartialStoredCase>> {
    const partialStoredCases = new Stringifier<PartialStoredCase[]>().deserialize(
      this.localStorage.getItem(`partials-${caseID}`)!
    );
    if (!partialStoredCases) {
      return Promise.resolve([
        new PartialStoredCase(
          {},
          caseID,
          new CaseMetaData(caseID, true, new Map(), new Map(), new Date(), new Date()),
          'public'
        ),
      ]);
    }
    const resultArray: Array<PartialStoredCase> = [];
    partialStoredCases.forEach((partial: PartialStoredCase) => {
      let partialMetaData = partial.metaData;

      partialMetaData = { ...partialMetaData, lastUpdate: new Date(partial.metaData.lastUpdate) };
      partialMetaData = {
        ...partialMetaData,
        creationDate: new Date(partial.metaData.creationDate),
      };
      // eslint-disable-next-line no-param-reassign
      partial = { ...partial, metaData: partialMetaData };

      resultArray.push(partial);
    });

    return Promise.resolve(resultArray);
  }

  saveAllPartials(partialStoredCases: Array<PartialStoredCase>): Promise<boolean> {
    this.localStorage.setItem(
      `partials-${partialStoredCases[0].caseID}`,
      new Stringifier().serialize(_.cloneDeep(partialStoredCases))
    );
    return Promise.resolve(true);
  }
}

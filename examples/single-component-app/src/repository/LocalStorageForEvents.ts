import { injectable } from 'inversify';
import { CreateCaseEvent, Event, EventsStorageInterface } from '@dandelion/core';

@injectable()
export class LocalStorageForEvents implements EventsStorageInterface {
  private caseID = 'id-prova';

  constructor() {}

  loadAllEvents(): Promise<Array<Event>> {
    const resultArray = [new CreateCaseEvent(this.caseID)];

    return Promise.resolve(resultArray);
  }

  saveEvent(): Promise<string> {
    return Promise.resolve(this.caseID);
  }
}

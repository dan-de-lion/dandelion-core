import { ValuesSetValue } from '@dandelion/core';

type Class<T> = new (...args: any[]) => T;
type DandelionSerializedType<ST> = { __dandelionType__: string; __value__: ST };

export interface SerDes<InType, OutType> {
  serialize(object: InType): OutType;
  deserialize(serialized: OutType): InType;
}

abstract class SerDesBrick<T, ST> {
  constructor(private myClass: Class<T>) {}

  protected abstract replacer(object: T): ST;
  protected abstract reviver(object: ST): T;

  tryReplace(object: any): DandelionSerializedType<ST> {
    return this.shouldReplace(object)
      ? {
          __value__: this.replacer(object),
          __dandelionType__: this.myClass.name,
        }
      : object;
  }

  tryRevive(object: any): T {
    return this.shouldRevive(object) ? this.reviver(object.__value__) : object;
  }

  private shouldReplace(object: any): boolean {
    return object instanceof this.myClass;
  }

  private shouldRevive(object: any): boolean {
    return (
      object !== undefined &&
      object !== null &&
      Object.prototype.hasOwnProperty.call(object, '__dandelionType__') &&
      object.__dandelionType__ === this.myClass.name
    );
  }
}

type SerializedValuesSetvalue = { value: string; label: string; numericalValue: number };
class SerializerFoValuesSetValue extends SerDesBrick<ValuesSetValue, SerializedValuesSetvalue> {
  constructor() {
    super(ValuesSetValue);
  }

  protected replacer(o: ValuesSetValue) {
    return { label: o.getLabel(), value: o.toString(), numericalValue: o.getNumericalValue() };
  }

  protected reviver(o: any) {
    return new ValuesSetValue(o.value, o.label, o.numericalValue);
  }
}

type SerializedMap<K, V> = [K, V][];
class SerializerForMap<K, V> extends SerDesBrick<Map<K, V>, SerializedMap<K, V>> {
  constructor() {
    super(Map);
  }

  protected replacer(o: Map<K, V>) {
    return Array.from(o.entries());
  }

  protected reviver(o: any) {
    return new Map<K, V>(o);
  }
}

export default class Stringifier<T> implements SerDes<T, string> {
  private listOfSerDes: SerDesBrick<any, any>[] = [
    new SerializerFoValuesSetValue(),
    new SerializerForMap(),
  ];

  serialize(object: T): string {
    return JSON.stringify(object, (_key, value) =>
      this.listOfSerDes.reduce((prev, s) => s.tryReplace(prev), value)
    );
  }

  deserialize(serialized: string): T {
    return JSON.parse(serialized, (_key, value) =>
      this.listOfSerDes.reduce((prev, s) => s.tryRevive(prev), value)
    );
  }
}

import {
  NamedBindings,
  setupContainers,
  CoreConfiguration,
  CaseConfiguration,
  CaseListService,
  CaseDataExporterService,
  Language,
  CurrentPageService,
} from '@dandelion/core';
import { CrfServiceFromFolder } from './services/CrfServiceFromFolder';
import { StorageForTests } from './repository/StorageForTests';
import { LocalStorageForEvents } from './repository/LocalStorageForEvents';
import { UserServiceWithWriteRights } from './services/UserServiceWithWriteRights';
import { createApp } from 'vue';
import App from './SingleComponentApp.vue';
import '../../../public/dandelion-core.css';

const app = createApp(App);

const container = setupContainers(
  new CoreConfiguration({
    baseURL: '/xslt/',
    defaultLanguage: Language.it,
    marginForScroll: [10, 0],
    listOfCaseTypes: [
      new CaseConfiguration({
        caseName: 'Admission',
      }),
    ],
    debug: true,
    selectorVariableName: '',
  }),
  { name: NamedBindings.CRFService, class: CrfServiceFromFolder },
  { name: NamedBindings.CaseStorage, class: StorageForTests },
  { name: NamedBindings.StorageForEvents, class: LocalStorageForEvents },
  { name: NamedBindings.UserService, class: UserServiceWithWriteRights },
  { name: NamedBindings.CaseListService, class: CaseListService },
  { name: NamedBindings.CaseDataExporterService, class: CaseDataExporterService }
);

const currentPageService = container.get(NamedBindings.CurrentPageService) as CurrentPageService;
/*currentPageService.set("Admission")*/

app.mount('#app');

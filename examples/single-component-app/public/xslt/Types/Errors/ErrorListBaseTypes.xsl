<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

  <xsl:template match="variable [ @dataType= 'group' ] ">
    <xsl:param name="parent" />

    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

    <xsl:apply-templates select="error">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="variable [ @dataType= 'set' ] ">
    <xsl:param name="parent" />
    <xsl:param name="setLevel" />

    <xsl:variable name="dataClass" select="@itemsDataClass" />

    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$fullName" />
    </xsl:apply-templates>

    <xsl:variable name="elementName">
      <xsl:call-template name="createElementName">
        <xsl:with-param name="fullName" select="$fullName" />
        <xsl:with-param name="setLevel" select="$setLevel" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="elementNameWithModel">
      <xsl:call-template name="addModelBeforeFullName">
        <xsl:with-param name="fullName" select="$elementName" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="escapedElementNameWithModel">
      <xsl:call-template name="escapeFullName">
        <xsl:with-param name="fullName" select="$elementNameWithModel" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="escapedFullNameWithModel">
      <xsl:call-template name="addModelBeforeFullName">
        <xsl:with-param name="fullName">
          <xsl:call-template name="escapeFullName">
            <xsl:with-param name="fullName" select="$fullName" />
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>

    <SetComponent>
      <xsl:attribute name=":full-name" select="concat($apos, $escapedFullNameWithModel , $apos)" />
      <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
      <xsl:attribute name=":this-set" select="$escapedFullNameWithModel" />
      <xsl:attribute name=":this-object">thisObject</xsl:attribute>
      <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
      <xsl:attribute name=":model" select="$root" />

      <xsl:element name="my-custom-template">
        <xsl:attribute
                    name="v-for"
                    select="concat('(childElement, index', $setLevel, ') in ', $escapedFullNameWithModel)"
                />

        <SetItemComponent>
          <xsl:attribute name=":full-name" select="concat($apos, $escapedElementNameWithModel , $apos)" />
          <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
          <xsl:attribute name=":this-set">thisSet</xsl:attribute>
          <xsl:attribute name=":this-object" select="$elementNameWithModel" />
          <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
          <xsl:attribute name=":model" select="$root" />

          <xsl:element name="my-custom-template">
            <xsl:attribute name="v-slot:default" select="concat('{ thisSet, thisObject, thisIndex }')" />

            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
              <xsl:with-param name="setLevel" select="$setLevel + 1" />

              <xsl:with-param name="parent" select="$elementName" />

            </xsl:apply-templates>

          </xsl:element>
        </SetItemComponent>
      </xsl:element>
    </SetComponent>

    <xsl:apply-templates select="error">
      <xsl:with-param name="fullName" select="$fullName" />
    </xsl:apply-templates>
  </xsl:template>
</xsl:stylesheet>

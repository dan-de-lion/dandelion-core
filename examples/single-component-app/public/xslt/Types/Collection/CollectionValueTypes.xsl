<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="value" mode="checkbox">

        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="myValue" select="concat($valuesSetName, '.', @name)" />

        <xsl:variable name="fullName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '-', $myValue)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="concat($variableName, '-', $myValue)" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="escapedGroupName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$variableName" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="name" select="@name" />

        <xsl:variable name="arrayOfExcludes">

            <xsl:apply-templates select="./excludes" />

            <xsl:for-each select="../value[@name!=$name and ./excludes='*']/@name">
                <xsl:value-of select="concat(',',.)">
                </xsl:value-of>
            </xsl:for-each>

            <!-- TODO: transform in IF or template on ./excludes/*: we already are on ../value[@name=$name] node ;-) -->
            <xsl:for-each select="../value[@name=$name and ./excludes='*']">

                <xsl:variable select="@name" name="x" />
                <xsl:for-each select="../value[@name!=$x]/@name">

                    <xsl:value-of select="concat(',',.)">
                    </xsl:value-of>
                </xsl:for-each>
            </xsl:for-each>

            <xsl:for-each select="../value[@name!=$name and ./excludes=$name]/@name">

                <xsl:value-of select="concat(',',.)">
                </xsl:value-of>
            </xsl:for-each>

        </xsl:variable>

        <li>
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <input type="checkbox" value="{ $myValue }" excludes="{$arrayOfExcludes}">
                <xsl:attribute
                    name=":name"
                    select="concat('getContextForID + ', $apos , $escapedGroupName, $apos)"
                />

                <xsl:variable name="escapedVariableName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$variableName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="$computedFormula and $computedFormula != ''">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedVariableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', $computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedVariableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:if test="@label">
                    <xsl:attribute name="my-label" select="@label" />
                </xsl:if>

                <xsl:if test="@numericalValue">
                    <xsl:attribute name="my-numerical-value" select="@numericalValue" />
                </xsl:if>
            </input>

            <xsl:call-template name="addDescription">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="separatedTooltip" select="'true'" />
                <xsl:with-param name="cssclass" select="'valueDescription'" />
            </xsl:call-template>

        </li>

    </xsl:template>


    <xsl:template match="value" mode="select">

        <xsl:param name="valuesSetName" />

        <option value="{ concat($valuesSetName, '.', @name) }">

            <xsl:call-template name="getTranslatedLabel" />

            <xsl:if test="@label">
                <xsl:attribute name="my-label" select="@label" />
            </xsl:if>

            <xsl:if test="@numericalValue">
                <xsl:attribute name="my-numerical-value" select="@numericalValue" />
            </xsl:if>

        </option>

    </xsl:template>


    <xsl:template match="value" mode="radio">

        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="myValue" select="concat($valuesSetName, '.', @name)" />

        <xsl:variable name="fullName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '-', $myValue)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="concat($variableName, '-', $myValue)" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="escapedGroupName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$variableName" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>

        <li>
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

                 <xsl:variable name="escapedVariableName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$variableName" />
                    </xsl:call-template>
                </xsl:variable>

            <input type="radio" value="{ $myValue }">
                <xsl:attribute
                    name=":name"
                    select="concat('getContextForID + ', $apos , $escapedGroupName, $apos)"
                />

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="$computedFormula and $computedFormula != ''">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedVariableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', $computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedVariableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:if test="@label">
                    <xsl:attribute name="my-label" select="@label" />
                </xsl:if>

                <xsl:if test="@numericalValue">
                    <xsl:attribute name="my-numerical-value" select="@numericalValue" />
                </xsl:if>

            </input>


            <xsl:call-template name="addDescription">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="separatedTooltip" select="'true'" />
                <xsl:with-param name="cssclass">valueDescription</xsl:with-param>
            </xsl:call-template>

        </li>

    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:sxl="http://www.w3.org/1999/XSL/Transform"
>

    <xsl:template
        match="variable[ @dataType = 'reference' and (not(@interface) or @interface != 'pagelink') ]"
    >
        <xsl:param name="parent" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable
            name="cleanedRoot"
            select="replace(replace($root, 'model.', ''), 'model', '')"
        />

        <xsl:variable name="referenceToBePassed">
            <xsl:if test="$cleanedRoot = ''">
                <xsl:value-of select="@fullname" />
            </xsl:if>
            <xsl:if test="$cleanedRoot != ''">
                <xsl:value-of select="replace(@fullname, concat($cleanedRoot, '.'), '')" />
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$startingDataClass != ''">
            <xsl:apply-templates
                select="/root/dataClass[@name = $startingDataClass]"
                mode="referenceResolution"
            >
                <xsl:with-param name="variableReference" select="$referenceToBePassed" />
                <xsl:with-param name="parent" select="$cleanedRoot" />
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$startingDataClass = ''">
            <xsl:apply-templates select="/root/dataClass[1]" mode="referenceResolution">
                <xsl:with-param name="variableReference" select="$referenceToBePassed" />
                <xsl:with-param name="parent" select="$cleanedRoot" />
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:apply-templates>
        </xsl:if>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'reference' and @interface = 'pagelink' ]">
        <a class="page-link">
            <!--        TODO: remove 'model.' -->
            <xsl:attribute
                name="v-on:click"
                select="concat('currentPageService.set(', $apos, @fullname, $apos, ')')"
            />
            <xsl:apply-templates select="/root/dataClass[1]" mode="labelResolution">
                <xsl:with-param name="variableReference" select="substring-after(@fullname, '.')" />
                <xsl:with-param name="parent" select="$root" />
            </xsl:apply-templates>
        </a>
    </xsl:template>

    <xsl:template match="variable" mode="referenceResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="parent" />
        <xsl:param name="fullName" />

        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="parentToBePassed">
            <xsl:if test="$parent = ''">
                <xsl:value-of select="$parent" />
            </xsl:if>
            <xsl:if test="$parent != ''">
                <xsl:value-of select="concat($parent, '.')" />
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$rest = ''">

            <xsl:apply-templates select="./variable[@name = $variableToFind]">
                <xsl:with-param name="parent" select="$parentToBePassed" />
                <xsl:with-param name="withReferenceID" select="$fullName" />
            </xsl:apply-templates>

            <xsl:variable name="dataType" select="@dataType" />

            <xsl:apply-templates
                select="//dataClass[@name = $dataType]/variable[@name=$variableToFind]"
            >
                <xsl:with-param name="parent" select="$parentToBePassed" />
                <xsl:with-param name="withReferenceID" select="$fullName" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$rest != ''">
            <xsl:apply-templates
                select="./variable[@name = $variableToFind]"
                mode="referenceResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="parent" select="concat($parentToBePassed, $variableToFind)" />
                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:apply-templates>

            <xsl:variable name="dataType" select="@dataType" />

            <xsl:apply-templates select="//dataClass[@name = $dataType]" mode="referenceResolution">
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="parent" select="$parent" />
                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template match="dataClass" mode="referenceResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="parent" />
        <xsl:param name="fullName" />

        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="parentToBePassed">
            <xsl:if test="$parent = ''">
                <xsl:value-of select="$parent" />
            </xsl:if>
            <xsl:if test="$parent != ''">
                <xsl:value-of select="concat($parent, '.')" />
            </xsl:if>
        </xsl:variable>

        <xsl:if test="$rest = ''">
            <xsl:apply-templates select="./variable[@name = $variableToFind]">
                <xsl:with-param name="parent" select="$parentToBePassed" />
                <xsl:with-param name="withReferenceID" select="$fullName" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$rest != ''">
            <xsl:apply-templates
                select="./variable[@name = $variableToFind]"
                mode="referenceResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="parent" select="concat($parentToBePassed, $variableToFind)" />
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable | dataClass" mode="labelResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="parent" />


        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="$variableToFind = ''">
            <xsl:call-template name="applyVisibility" />
            <xsl:call-template name="getTranslatedLabel" />
        </xsl:if>
        <xsl:if test="$variableToFind != ''">
            <xsl:apply-templates
                select="./variable[@name = $variableToFind]"
                mode="labelResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="parent" select="concat($parent, '.', $variableToFind)" />
            </xsl:apply-templates>

            <xsl:variable name="dataType" select="@dataType" />

            <xsl:apply-templates
                select="//dataClass[@name = $dataType]/variable[@name = $variableToFind]"
                mode="labelResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="parent" select="$parent" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>

    <xsl:template name="firstChunk">
        <xsl:param name="fullNameToTrim" />

        <xsl:choose>
            <xsl:when test="contains($fullNameToTrim, '.')">
                <xsl:value-of select="substring-before($fullNameToTrim, '.')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$fullNameToTrim" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="restChunk">
        <xsl:param name="fullNameToTrim" />

        <xsl:choose>
            <xsl:when test="contains($fullNameToTrim, '.')">
                <xsl:value-of select="substring-after($fullNameToTrim, '.')" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="''" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template mode="referenceLabel" match="*">
        <xsl:call-template name="getTranslatedLabel" />
    </xsl:template>

</xsl:stylesheet>

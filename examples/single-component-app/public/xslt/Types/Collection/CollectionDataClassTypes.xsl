<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## GROUP ######## -->
    <!-- container for other variables -->

    <xsl:template match="variable [ @dataType = 'group' ] ">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="meIsAPageFromParent" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="meIsAPage">
            <xsl:if test="@interface = 'pagelink' or $meIsAPageFromParent = 'true'">
                <xsl:value-of select="true()" />
            </xsl:if>
            <xsl:if test="(not(@interface) or @interface != 'pagelink') and $meIsAPageFromParent = 'false'">
                <xsl:value-of select="false()" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <span v-if="hooksExist(TypesOfHooks.BeforeVariable)">
            <xsl:attribute
                name="v-html"
                select="concat('runHooks(', $apos, $escapedFullName, $apos, ', TypesOfHooks.BeforeVariable)')"
            />
        </span>

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$escapedFullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="@interface = 'pagelink'">

            <a class="page-link">
                <xsl:attribute
                    name="v-on:click"
                    select="concat('currentPageService.set(', $apos, $escapedFullName, $apos, ')')"
                />

                <xsl:call-template name="getTranslatedLabel" />
            </a>
        </xsl:if>

        <fieldset v-custom-model="'{$escapedFullNameWithModel}'">

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="$requiredFromParent" />
                </xsl:attribute>
            </xsl:if>

            <xsl:attribute name="class" select="@name" />

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
            <xsl:call-template name="addDescription">
                <xsl:with-param name="element">legend</xsl:with-param>
                <xsl:with-param name="cssclass">groupDescription</xsl:with-param>
            </xsl:call-template>

            <xsl:if test="$statisticianMode = 'true'">
                <span class="statistician">
                    <xsl:value-of>FullName: </xsl:value-of>
                    <xsl:call-template name="addModelBeforeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </span>
            </xsl:if>

            <xsl:if test="$meIsAPage = true()">
                <xsl:attribute name="v-show" select="concat('currentPageService.get(', $apos, $fullName, $apos, ')')" />
            </xsl:if>

            <xsl:if test="@separateInPages">
                <nav>
                        <xsl:choose>
                            <xsl:when test="@requiredForStatus">
                                <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                                <xsl:apply-templates select="./variable" mode="nav">
                                    <xsl:with-param name="parent" select="$fullName" />

                                    <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                                </xsl:apply-templates>
                            </xsl:when>
                            <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                                <xsl:apply-templates select="./variable" mode="nav">
                                    <xsl:with-param name="parent" select="$fullName" />

                                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                                </xsl:apply-templates>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates select="./variable" mode="nav">
                                    <xsl:with-param name="parent" select="$fullName" />
                                </xsl:apply-templates>
                            </xsl:otherwise>
                        </xsl:choose>
                </nav>
            </xsl:if>

            <xsl:variable name="nextParent" select="$fullName" />

            <div>
                <!--        <xsl:call-template name="inputAttributes">-->
                <!--          <xsl:with-param name="fullName" select="$fullName" />-->
                <!--        </xsl:call-template>-->
                <xsl:variable name="separateChildrenInPages">
                    <xsl:choose>
                        <xsl:when test="@separateInPages">
                            <xsl:value-of select="true()" />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="false()" />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:choose>
                    <xsl:when test="@requiredForStatus">
                        <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                        <xsl:apply-templates select="./variable">
                            <xsl:with-param name="parent" select="concat($nextParent, '.')" />
                            <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                            <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                        <xsl:apply-templates select="./variable">
                            <xsl:with-param name="parent" select="concat($nextParent, '.')" />
                            <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="./variable">
                            <xsl:with-param name="parent" select="concat($nextParent, '.')" />
                            <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                        </xsl:apply-templates>
                    </xsl:otherwise>
                </xsl:choose>
            </div>

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </fieldset>
    </xsl:template>

    <!-- ######## DATACLASS ######## -->
    <!-- same as a "group" of variables, but can be inserted in different points of the data collection -->
    <xsl:template match="dataClass">
        <xsl:param name="setLevel">0</xsl:param>

        <!-- (use this for $arrayOfExcludes) -->
        <!-- to identify itself (use this for $arrayOfExcludes) -->
        <xsl:param name="parent" />
        <xsl:param name="separateChildrenInPages" />
        <xsl:param name="arrayOfExcludes" />
        <xsl:param name="requiredFromParent" select="''" />

        <section>
            <xsl:if test="@activateIf">
                <xsl:attribute name="v-if" select="@activateIf" />
            </xsl:if>

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="$requiredFromParent" />
                </xsl:attribute>
            </xsl:if>

            <xsl:variable name="cleanedRoot" select="replace(replace($root, 'model.', ''), 'model', '')" />

            <xsl:variable name="selectedParent">
                <xsl:choose>
                    <xsl:when test="not($parent) and $cleanedRoot != ''">
                        <xsl:value-of select="concat($cleanedRoot, '.')" />
                    </xsl:when>
                    <xsl:when test="not($parent) and $cleanedRoot = ''">
                        <xsl:value-of select="$cleanedRoot" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="concat($parent, '.')" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:attribute name="class">
                <xsl:value-of select="@name" />
            </xsl:attribute>

            <xsl:choose>
                <xsl:when test="@requiredForStatus">
                    <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                    <xsl:apply-templates select="./variable">
                        <xsl:with-param name="setLevel" select="$setLevel" />
                        <xsl:with-param name="parent" select="$selectedParent" />
                        <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                        <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                        <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:apply-templates select="./variable">
                        <xsl:with-param name="setLevel" select="$setLevel" />
                        <xsl:with-param name="parent" select="$selectedParent" />
                        <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                        <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                        <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="./variable">
                        <xsl:with-param name="setLevel" select="$setLevel" />
                        <xsl:with-param name="parent" select="$selectedParent" />
                        <xsl:with-param name="meIsAPageFromParent" select="$separateChildrenInPages" />
                        <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>

            <xsl:apply-templates select="./error">
                <xsl:with-param name="fullName" select="$selectedParent" />
            </xsl:apply-templates>
            <xsl:apply-templates select="./warning">
                <xsl:with-param name="fullName" select="$selectedParent" />
            </xsl:apply-templates>
            <xsl:apply-templates select="./requirement">
                <xsl:with-param name="fullName" select="$selectedParent" />
            </xsl:apply-templates>
        </section>

        <xsl:if test="@activateIf">
            <section>
                <xsl:attribute name="v-if" select="concat('!', @activateIf)" />
                <h3>
                    <xsl:call-template name="translateString">
                        <xsl:with-param name="string">
                            <xsl:value-of select="concat(@name, 'not active')" />
                        </xsl:with-param>
                    </xsl:call-template>
                </h3>
                <p>
                    <xsl:call-template name="translateString">
                        <xsl:with-param name="string">
                            <xsl:value-of select="@howToActivate" />
                        </xsl:with-param>
                    </xsl:call-template>
                </p>
            </section>
        </xsl:if>
    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPageFromParent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="meIsAPage">
            <xsl:if test="@interface = 'pagelink' or $meIsAPageFromParent = 'true'">
                <xsl:value-of select="true()" />
            </xsl:if>
            <xsl:if test="(not(@interface) or @interface != 'pagelink') and $meIsAPageFromParent = 'false'">
                <xsl:value-of select="false()" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="dataType" select="@dataType" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$escapedFullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="@interface = 'pagelink'">

            <a class="page-link">
                <xsl:attribute
                    name="v-on:click"
                    select="concat('currentPageService.set(', $apos, $escapedFullName, $apos, ')')"
                />

                <xsl:call-template name="getTranslatedLabel" />
            </a>
        </xsl:if>

        <span v-if="hooksExist(TypesOfHooks.BeforeVariable)">
            <xsl:attribute
                name="v-html"
                select="concat('runHooks(', $apos, $escapedFullName, $apos, ', TypesOfHooks.BeforeVariable)')"
            />
        </span>

        <fieldset
            v-if="userService.canRead('{$escapedFullName}','{$crfName}','{$crfVersion}')"
            v-custom-model="'{$escapedFullNameWithModel}'"
        >
            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="$requiredFromParent" />
                </xsl:attribute>
            </xsl:if>

            <xsl:attribute name="class" select="@dataType" />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <xsl:call-template name="addDescription">
                <xsl:with-param name="element">legend</xsl:with-param>
                <xsl:with-param name="cssclass">groupDescription</xsl:with-param>
            </xsl:call-template>

            <xsl:if test="$meIsAPage = true()">
                <xsl:attribute name="v-show" select="concat('currentPageService.get(', $apos, $fullName, $apos,')')" />
            </xsl:if>

            <xsl:if test="@separateInPages">
                <nav>
                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                            <xsl:apply-templates select="//dataClass[ @name = $dataType ]/variable" mode="nav">
                                <xsl:with-param name="parent" select="$fullName" />

                                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:apply-templates select="//dataClass[ @name = $dataType ]/variable" mode="nav">
                                <xsl:with-param name="parent" select="$fullName" />

                                <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="//dataClass[ @name = $dataType ]/variable" mode="nav">
                                <xsl:with-param name="parent" select="$fullName" />

                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                </nav>
            </xsl:if>

            <xsl:variable name="nextParent" select="$fullName" />

            <DataclassItemComponent>

                <xsl:variable name="fullNameWithModel">
                    <xsl:call-template name="addModelBeforeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute name=":full-name" select="concat($apos, $fullNameWithModel , $apos)" />
                <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
                <xsl:attribute name=":model" select="$root" />

                <xsl:element name="my-custom-template">
                    <xsl:attribute name="v-slot:default">{ thisSet, thisObject, thisIndex }
                    </xsl:attribute>

                    <xsl:call-template name="inputAttributes">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>

                    <xsl:variable name="separateChildrenInPages">
                        <xsl:choose>
                            <xsl:when test="@separateInPages">
                                <xsl:value-of select="true()" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="false()" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>

                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                            <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
                                <xsl:with-param name="parent" select="$nextParent" />
                                <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages" />
                                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
                                <xsl:with-param name="parent" select="$nextParent" />
                                <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages" />
                                <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
                                <xsl:with-param name="parent" select="$nextParent" />
                                <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages" />
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </DataclassItemComponent>

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </fieldset>
    </xsl:template>

    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeNavNotDefined" mode="nav" />

</xsl:stylesheet>

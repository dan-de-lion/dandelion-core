<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'number' ] ">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>

            </xsl:call-template>

            <input type="{ @dataType }" init-value="NaN">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

                <xsl:apply-templates select="@precision" />

                <xsl:if test="@min and @min != ''">

                    <xsl:attribute name="min" select="@min" />

                </xsl:if>

                <xsl:if test="@max and @max != ''">

                    <xsl:attribute name="max" select="@max" />

                </xsl:if>

            </input>

            <xsl:apply-templates select="./description" />

            <xsl:if test="@unitOfMeasure">
                <span class="unitOfMeasure">
                    <xsl:call-template name="translateString">
                        <xsl:with-param name="string">
                            <xsl:value-of select="@unitOfMeasure" />
                        </xsl:with-param>
                    </xsl:call-template>
                </span>
            </xsl:if>

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'text' or @dataType = 'email' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:call-template>

            <input type="{ @dataType }">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName" select="$fullName" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

                <!-- text input maxlength -->

                <xsl:if test="@max">

                    <xsl:attribute name="maxlength" select="@max" />

                </xsl:if>

                <xsl:if test="@pattern">
                    <xsl:variable name="acceptedValues">
                        <xsl:choose>
                            <xsl:when test="@pattern = 'alphabetical'">^[A-Za-z]*$</xsl:when>
                            <xsl:when test="@pattern = 'numerical'">^[0-9]*$</xsl:when>
                            <xsl:when test="@pattern = 'non-numerical'">^[^0-9]*$</xsl:when>
                            <xsl:when test="@pattern = 'alphanumeric'">^[A-Za-z0-9]*$</xsl:when>
                            <xsl:when test="@pattern = 'ten-digits-numerical'">^[0-9]{10}$</xsl:when>
                            <xsl:when test="@pattern = 'email'">
                                ^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$</xsl:when>
                            <xsl:when test="@pattern = 'url'">
                                ^(https?:\/\/)?([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5})(:[0-9]{1,5})?(\/.*)?$</xsl:when>
                            <xsl:otherwise test="@pattern">
                                <xsl:value-of select="@pattern" />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                </xsl:if>
            </input>

            <xsl:if test="@pattern">
                <div class="error">
                    <xsl:attribute name="v-if">
                        <xsl:value-of
                            select="concat('checkValidity(', $apos,  $fullName , $apos, ', model, ', $apos, $acceptedValues, $apos, ')')"
                        />
                    </xsl:attribute>

                    <xsl:variable name="message">
                        <xsl:choose>
                            <xsl:when
                                test="@pattern = 'alphabetical'"
                            >Only alphabetical characters are allowed. Example: "HelloWorld"</xsl:when>
                            <xsl:when
                                test="@pattern = 'numerical'"
                            >Only numerical characters are allowed. Example: "12345"</xsl:when>
                            <xsl:when
                                test="@pattern = 'non-numerical'"
                            >Non-numerical characters are allowed. Example: "Hello@#$%!"</xsl:when>
                            <xsl:when
                                test="@pattern = 'alphanumeric'"
                            >Only alphanumeric characters are allowed. Example: "Hello123"</xsl:when>
                            <xsl:when
                                test="@pattern = 'ten-digits-numerical'"
                            >Please enter exactly ten numerical digits. Example: "0123456789"</xsl:when>
                            <xsl:when
                                test="@pattern = 'email'"
                            >Please enter a valid email address. Example: "example@example.com"</xsl:when>
                            <xsl:when
                                test="@pattern = 'url'"
                            >Please enter a valid URL. Example: "https://www.example.com"</xsl:when>
                            <xsl:otherwise>Please enter a valid value</xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:attribute name="v-html">
                        <xsl:value-of select="concat('translationService.t(''', $message, ''')')" />
                    </xsl:attribute>
                </div>
            </xsl:if>


            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'output' ]">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:call-template>

            <output required-for-status="false">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>

            </output>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'color' ]">

        <xsl:param name="parent" />
        <xsl:param name="labelFromRepeater" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />


        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:call-template>

            <input type="{ @dataType }">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'support'] ">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div style="display: none">

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>

            </xsl:call-template>

            <input type="{ @dataType }" required-for-status="false">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'text' and @interface = 'textArea' ] ">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>

            </xsl:call-template>

            <textarea>

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

            </textarea>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'time' ] ">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>

            </xsl:call-template>

            <input type="time">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'date' ]">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName">
                    <xsl:if test="$withReferenceID != ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                    <xsl:if test="$withReferenceID = ''">
                        <xsl:value-of select="$fullName" />
                    </xsl:if>
                </xsl:with-param>

            </xsl:call-template>

            <input type="date">

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

                <xsl:if test="@minDate">

                    <xsl:attribute name=":min" select="@minDate" />

                </xsl:if>

                <xsl:if test="@maxDate and @maxDate != 'today'">

                    <xsl:attribute name=":max" select="@maxDate" />

                </xsl:if>

                <xsl:if test="@maxDate = 'today'">
                    <xsl:attribute name=":max" select="concat('new Date().toISOString().split(`T`)[0]','')" />
                </xsl:if>

            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'boolean' ]">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div class="boolean-variable">
            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:call-template>

            <xsl:variable name="escapedFullNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName">
                        <xsl:call-template name="escapeFullName">
                            <xsl:with-param name="fullName" select="$fullName" />
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="escapedFullName">
                <xsl:call-template name="escapeFullName">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
            </xsl:variable>

            <input type="checkbox" init-value="false" v-custom-model-for-boolean="'{$escapedFullNameWithModel}'">
                <xsl:attribute name=":disabled">
                    <xsl:choose>
                <xsl:when test="@computedFormula and @computedFormula != '' ">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:apply-templates select="@computedFormula" />

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'singlechoice' and ( not (@interface) or @interface = 'radio' ) ]">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

                <xsl:with-param name="csslabelclass">radioDescription</xsl:with-param>

                <xsl:with-param name="element">span</xsl:with-param>

            </xsl:call-template>

            <xsl:variable name="escapedFullNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName">
                        <xsl:call-template name="escapeFullName">
                            <xsl:with-param name="fullName" select="$fullName" />
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>

            <ul class="radiolist" v-custom-model-for-singlechoice="'{$escapedFullNameWithModel}'">

                <xsl:if test="@initValue">
                    <xsl:attribute name="init-value">
                        <xsl:value-of select="@initValue" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:apply-templates select="@computedFormula" />

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

                <xsl:variable name="valuesSetName" select="@valuesSet" />

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="radio">

                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                    
                    <xsl:with-param name="variableName" select="$fullName" />

                    <xsl:with-param name="computedFormula" select="@computedFormula" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:apply-templates>

                <xsl:apply-templates select="value" mode="radio">

                    <xsl:with-param name="valuesSetName" select="@name" />

                    <xsl:with-param name="variableName" select="$fullName" />

                    <xsl:with-param name="computedFormula" select="@computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

                </xsl:apply-templates>

            </ul>

            <button class="variable-reset" type="button">

                <xsl:variable name="escapedFullNameWithModel">
                    <xsl:call-template name="addModelBeforeFullName">
                        <xsl:with-param name="fullName">
                            <xsl:call-template name="escapeFullName">
                                <xsl:with-param name="fullName" select="$fullName" />
                            </xsl:call-template>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="escapedFullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                <xsl:when test="@computedFormula and @computedFormula != '' ">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('resetRadioButton(', $apos, $escapedFullNameWithModel, $apos, ')')"
                />
                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">Reset</xsl:with-param>
                </xsl:call-template>
            </button>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>


    </xsl:template>


    <xsl:template match="variable[ @dataType = 'singlechoice' and @interface = 'dropdown' ]">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:call-template>

            <select>

                <xsl:call-template name="inputAttributes">

                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:call-template>

                <xsl:variable name="escapedFullNameWithModel">
                    <xsl:call-template name="addModelBeforeFullName">
                        <xsl:with-param name="fullName">
                            <xsl:call-template name="escapeFullName">
                                <xsl:with-param name="fullName" select="$fullName" />
                            </xsl:call-template>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="escapedFullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                <xsl:when test="@computedFormula and @computedFormula != '' ">
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:attribute
                    name="v-custom-model-for-select"
                    select="concat($apos, $escapedFullNameWithModel, $apos)"
                />
                <option value="" my-label="none" />

                <xsl:variable name="valuesSetName" select="@valuesSet" />

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">

                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />

                    <xsl:with-param name="computedFormula" select="@computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:apply-templates>

                <xsl:apply-templates select="value" mode="select">

                    <xsl:with-param name="valuesSetName" select="@name" />

                    <xsl:with-param name="computedFormula" select="@computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:apply-templates>

            </select>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'multiplechoice' and ( not (@interface) or @interface = 'checkbox' ) ]">

        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

                <xsl:with-param name="csslabelclass">checkBoxDescription</xsl:with-param>

                <xsl:with-param name="element">span</xsl:with-param>

            </xsl:call-template>

            <xsl:variable name="escapedFullNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName">
                        <xsl:call-template name="escapeFullName">
                            <xsl:with-param name="fullName" select="$fullName" />
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>

            <ul class="checkboxlist" v-custom-model-for-checkboxes="'{$escapedFullNameWithModel}'" init-value="[]">

                <xsl:if test="@initValue">
                    <xsl:attribute name="init-value">
                        <xsl:value-of select="@initValue" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:apply-templates select="@computedFormula" />

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID != ''">
                            <xsl:value-of select="$withReferenceID" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID = ''">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

                <xsl:variable name="valuesSetName" select="@valuesSet" />

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="checkbox">

                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />

                    <xsl:with-param name="variableName" select="$fullName" />

                    <xsl:with-param name="computedFormula" select="@computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

                </xsl:apply-templates>

                <xsl:apply-templates select="value" mode="checkbox">

                    <xsl:with-param name="valuesSetName" select="@name" />

                    <xsl:with-param name="variableName" select="$fullName" />

                    <xsl:with-param name="computedFormula" select="@computedFormula" />

                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                    <xsl:with-param name="withReferenceID" select="$withReferenceID" />

                </xsl:apply-templates>

            </ul>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>


    <xsl:template match="variable[ @dataType = 'multiplechoice' and @interface = 'autocomplete' ] ">

        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <div>

            <xsl:call-template name="wrapperAttributes">

                <xsl:with-param name="fullName" select="$fullName" />

            </xsl:call-template>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

        </div>

    </xsl:template>

    <!--   #### PLUGGABLE DATA TYPE #### -->
    <xsl:template match="variable[ @dataType = 'plugin' and @interface ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <!-- add canRead -->
        <Suspense>
            <xsl:element name="{@interface}">

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>


                <xsl:call-template name="applyVisibility">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
                <xsl:apply-templates select="./customAttribute" />
                <xsl:attribute name=":context">context</xsl:attribute>
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName">
                        <xsl:if test="$withReferenceID = 'true'">
                            <xsl:value-of select="concat($fullName, '-reference')" />
                        </xsl:if>
                        <xsl:if test="$withReferenceID != 'true'">
                            <xsl:value-of select="$fullName" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

                <xsl:variable name="escapedFullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:variable name="escapedFullNameWithModel">
                    <xsl:call-template name="addModelBeforeFullName">
                        <xsl:with-param name="fullName" select="$escapedFullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute name=":full-name" select="concat($apos, $escapedFullNameWithModel, $apos)" />
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$escapedFullName" />
                </xsl:call-template>
            </xsl:element>
        </Suspense>
    </xsl:template>

    <!--  #### CUSTOM ATTRIBUTE FOR PLUGGABLE DATA TYPE #### -->
    <xsl:template match="customAttribute">
        <xsl:attribute name="{@name}">
            <xsl:value-of select="." />
        </xsl:attribute>
    </xsl:template>
</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'set' ]">
        <xsl:param name="parent" />
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="meIsAPageFromParent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="dataClass" select="@itemsDataClass" />
        <xsl:variable name="computedItems" select="@computedItems" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <span v-if="hooksExist(TypesOfHooks.BeforeVariable)">
            <xsl:attribute
                name="v-html"
                select="concat('runHooks(', $apos, $escapedFullName, $apos, ', TypesOfHooks.BeforeVariable)')"
            />
        </span>

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="@separateInPages">
                <xsl:variable name="separateChildrenInPages" select="true()" />
                <xsl:variable
                    name="addElementClickAction"
                    select="concat('addElementToSet(thisSet); currentPageService.set(', $apos, $escapedFullName, '[', $apos, ' + (thisSet.length - 1) + ', $apos, ']', $apos, ')')"
                />
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="separateChildrenInPages" select="false()" />
                <xsl:variable name="addElementClickAction" select="'addElementToSet(thisSet)'" />
            </xsl:otherwise>
        </xsl:choose>

        <xsl:variable name="elementName">
            <xsl:call-template name="createElementName">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedElementName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$elementName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="elementNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$elementName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedElementNameWithModel">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$elementNameWithModel" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$escapedFullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="protectedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName">
                    <xsl:call-template name="protectFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>

        <SetComponent>
            <xsl:attribute name=":full-name" select="concat($apos, $escapedFullNameWithModel , $apos)" />
            <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
            <xsl:attribute name=":this-set" select="$protectedFullNameWithModel" />
            <xsl:attribute name=":this-object">thisObject</xsl:attribute>
            <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
            <xsl:attribute name=":model" select="$root" />

            <xsl:element name="my-custom-template">
                <xsl:attribute name="v-slot:default">{ thisSet, thisObject, thisIndex }</xsl:attribute>

                <fieldset v-custom-model-for-sets="'{$escapedFullNameWithModel}'" init-value="[]">

                    <xsl:if test="@initValue">
                        <xsl:attribute name="init-value">
                            <xsl:value-of select="@initValue" />
                        </xsl:attribute>
                    </xsl:if>


                    <xsl:if test="@requiredForStatus">
                        <xsl:attribute name="required-for-status">
                            <xsl:value-of select="@requiredForStatus" />
                        </xsl:attribute>
                    </xsl:if>

                    <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                        <xsl:attribute name="required-for-status">
                            <xsl:value-of select="$requiredFromParent" />
                        </xsl:attribute>
                    </xsl:if>

                    <xsl:attribute name=":disabled">
                        <xsl:choose>
                        <xsl:when test="@computedFormula and @computedFormula != '' ">
                            <xsl:value-of
                                    select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                                />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                    select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                                />
                        </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:apply-templates select="@computedFormula" />

                    <xsl:call-template name="addID">
                        <xsl:with-param name="fullName">
                            <xsl:if test="$withReferenceID != ''">
                                <xsl:value-of select="$withReferenceID" />
                            </xsl:if>
                            <xsl:if test="$withReferenceID = ''">
                                <xsl:value-of select="$fullName" />
                            </xsl:if>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:attribute name="class" select="@name" />
                    <xsl:if test="$meIsAPageFromParent = true()">

                        <xsl:attribute
                            name="v-show"
                            select="concat('currentPageService.get(', $apos, $fullName, $apos, ')')"
                        />

                    </xsl:if>


                    <xsl:call-template name="applyVisibility">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                    <xsl:call-template name="addDescription">
                        <xsl:with-param name="element">legend</xsl:with-param>
                        <xsl:with-param name="cssclass">setDescription</xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="setComputedItems" />



                    <xsl:apply-templates select="./description" />

                    <xsl:if test="@separateInPages">

                        <section class="itemSelector">

                            <button type="button" class="add-item">
                                <xsl:attribute name="v-on:click.stop" select="$addElementClickAction" />
                                <xsl:attribute
                                    name=":disabled"
                                    select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                                />
                                <xsl:call-template name="translateString">
                                    <xsl:with-param name="string">
                                        <xsl:value-of select="concat('Add ', $dataClass)" />
                                    </xsl:with-param>
                                </xsl:call-template>
                            </button>

                            <nav>
                                <xsl:variable
                                    name="itemLabelVariable"
                                    select="//dataClass[ @name = $dataClass ]/variable[ @itemLabel ]/@name"
                                />

                                <button
                                    type="button"
                                    class="page-navigation"
                                    v-for="{ concat('(childElement, index', $setLevel, ') in ', $protectedFullNameWithModel) }"
                                >

                                    <xsl:attribute
                                        name="v-on:click.stop"
                                        select="concat('currentPageService.set(', $apos, $escapedElementName, $apos, ')')"
                                    />

                                    <xsl:attribute
                                        name=":class"
                                        select="concat('{selected: ', concat('currentPageService.get(', $apos, $escapedElementName, $apos, ')'), '}')"
                                    />

                                    <xsl:attribute name="v-html">

                         <xsl:if test="$itemLabelVariable">
                        <xsl:value-of
                                                select="concat('childElement.', $itemLabelVariable, ' || translationService.t(', $apos, '-- new ', $dataClass, ' --', $apos, ')')"
                                            />
                    </xsl:if>

                    <xsl:if test="not($itemLabelVariable)">
                       <xsl:value-of
                                                select="concat('translationService.t(', $apos, '-- new ', $dataClass, ' --', $apos, ')')"
                                            />
                    </xsl:if>

                                    </xsl:attribute>

                                    <!--translationService.t()
                                    and xsl:call-template name="translateString don't work, find a
                                    solution" >-->
                                </button>

                            </nav>

                        </section>

                    </xsl:if>
                    <div>
                        <xsl:value-of select="$meIsAPageFromParent" />
                    </div>
                    <ul>
                        <xsl:call-template name="setItem">
                            <xsl:with-param name="protectedFullNameWithModel" select="$protectedFullNameWithModel" />
                            <xsl:with-param name="elementName" select="$elementName" />
                            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                            <xsl:with-param name="elementNameWithModel" select="$elementNameWithModel" />
                            <xsl:with-param name="escapedElementNameWithModel" select="$escapedElementNameWithModel" />
                            <xsl:with-param name="setLevel" select="$setLevel" />
                            <xsl:with-param name="dataClass" select="$dataClass" />
                            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                        </xsl:call-template>
                    </ul>


                    <xsl:if test="not(@separateInPages) and not(@computedItems)">

                        <button type="button" class="add-item">

                            <xsl:attribute name="v-on:click.stop">addElementToSet(thisSet)</xsl:attribute>
                            <xsl:call-template name="translateString">
                                <xsl:with-param name="string">
                                    <xsl:value-of select="concat('Add ', $dataClass)" />
                                </xsl:with-param>
                            </xsl:call-template>
                        </button>

                    </xsl:if>


                    <xsl:call-template name="afterVariable">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </fieldset>
            </xsl:element>
        </SetComponent>

    </xsl:template>

    <xsl:template name="setComputedItems">
        <xsl:variable name="itemsDataClass" select="@itemsDataClass" />
        <xsl:attribute name="v-computed-items" select="@computedItems" />
        <xsl:attribute name="item-key" select="//dataClass[ @name = itemsDataClass ]/variable[ @itemKey ]/@name" />
    </xsl:template>

    <xsl:template name="setItem">
        <xsl:param name="protectedFullNameWithModel" />
        <xsl:param name="elementName" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="elementNameWithModel" />
        <xsl:param name="escapedElementNameWithModel" />
        <xsl:param name="setLevel" />
        <xsl:param name="dataClass" />
        <xsl:param name="requiredFromParent" />
        <li
            v-for="{ concat('(childElement, index', $setLevel, ') in ', $protectedFullNameWithModel) }"
            track-by="childelement.guid"
        >

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$elementName" />
            </xsl:call-template>

            <xsl:if test="@separateInPages">
                <xsl:attribute
                    name="v-show"
                    select="concat('currentPageService.get(', $apos, $escapedElementName, $apos, ')')"
                />
            </xsl:if>

            <SetItemComponent>
                <xsl:attribute name=":full-name" select="concat($apos, $escapedElementName, $apos)" />
                <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
                <xsl:attribute name=":this-set">thisSet</xsl:attribute>
                <xsl:attribute name=":this-object" select="$elementNameWithModel" />
                <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
                <xsl:attribute name=":model" select="$root" />
                <xsl:element name="my-custom-template">
                    <xsl:attribute name="v-slot:default">{ thisSet, thisObject, thisIndex }</xsl:attribute>

                    <xsl:choose>
                        <xsl:when test="@requiredForStatus">
                            <xsl:variable name="requiredForStatus" select="@requiredForStatus" />
                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                <xsl:with-param name="setLevel" select="$setLevel + 1" />

                                <xsl:with-param name="parent" select="$elementName" />
                                <xsl:with-param name="requiredFromParent" select="$requiredForStatus" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:when test="not(@requiredForStatus) and $requiredFromParent != ''">
                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                <xsl:with-param name="setLevel" select="$setLevel + 1" />

                                <xsl:with-param name="parent" select="$elementName" />
                                <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                <xsl:with-param name="setLevel" select="$setLevel + 1" />

                                <xsl:with-param name="parent" select="$elementName" />
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="not(@computedItems)">
                        <button type="button" class="remove-item">
                            <xsl:attribute
                                name="v-on:click.stop"
                                select="concat('removeElementFromSet(thisSet, index', $setLevel, ')')"
                            />
                            <xsl:attribute
                                name=":disabled"
                                select="concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />

                            <xsl:call-template name="translateString">
                                <xsl:with-param name="string">
                                    <xsl:value-of select="concat('Remove this ', $dataClass)" />
                                </xsl:with-param>
                            </xsl:call-template>
                        </button>
                    </xsl:if>

                </xsl:element>
            </SetItemComponent>
        </li>
    </xsl:template>

</xsl:stylesheet>

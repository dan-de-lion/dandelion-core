<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->

    <xsl:template match="dataClass">
        <xsl:param name="parent" />
        <xsl:param name="valueLabel" />
        <xsl:param name="depthLevel">0</xsl:param>
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="meIsAPage" />

        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages or $meIsAPage = true()">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

            <xsl:variable name="selectedParent">
                <xsl:call-template name="selectParent">
                    <xsl:with-param name="parent" select="$parent" />
                </xsl:call-template>
            </xsl:variable>

        <ul class="{@name}">
            <xsl:apply-templates select="./variable">
                <xsl:with-param name="parent" select="$selectedParent" />
                <xsl:with-param name="valueLabel" select="$valueLabel" />
                <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
                <xsl:with-param name="depthLevel" select="$depthLevel" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:apply-templates>
        </ul>
    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPage" />
        <xsl:param name="depthLevel" />

        <xsl:variable name="dataType" select="@dataType" />

        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when
                    test="@separateInPages or //dataClass[ @name = $dataType ]/@separateInPages"
                >
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:variable name="dataClassExist" select="//dataClass[@name = $dataType]/@name" />

        <xsl:if
            test="($showDataclassInstancesInTree = true() or $meIsAPage = true()) and $dataClassExist and $depthLevel &lt;= $maxDepth"
        >
            <li>
                <xsl:attribute name="class" select="$fullName" />
                <xsl:apply-templates select="@visible" mode="tree" />

                <xsl:variable name="escapedFullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $escapedFullName, $apos), ')')"
                />

                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">
                        <xsl:if test="@label and @label != ''">
                            <xsl:value-of select="@label" />
                        </xsl:if>
                        <xsl:if test="not(@label)">
                            <xsl:value-of select="@name" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

                <xsl:choose>
                    <xsl:when
                        test="($separateChildrenInPages = true() or $showDataclassInstancesInTree = true()) and $depthLevel &lt;= $maxDepth"
                    >
                        <xsl:apply-templates select="//dataClass[@name = $dataType]">
                            <xsl:with-param name="parent" select="concat($fullName, '.')" />
                            <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
                            <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                        </xsl:apply-templates>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="//dataClass[@name = $dataType]">
                            <xsl:with-param name="parent" select="concat($fullName, '.')" />
                            <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
                            <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                        </xsl:apply-templates>
                    </xsl:otherwise>
                </xsl:choose>
            </li>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>

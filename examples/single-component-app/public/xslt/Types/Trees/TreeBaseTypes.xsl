<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:template match="variable [ @dataType= 'group' ] ">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPage" />
        <xsl:param name="depthLevel" />
        <xsl:param name="setLevel">0</xsl:param>

        <!-- TODO: unify with dataclass instance in DataClassTypes.xsl -->
        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:if
            test="($showGroupsInTree = true() or $meIsAPage = true()) and $depthLevel &lt;= $maxDepth"
        >
            <li>
                <xsl:apply-templates select="@visible" mode="tree" />

                <xsl:variable name="escapedFullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $escapedFullName, $apos), ')')"
                />

                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">
                        <xsl:if test="@label and @label != ''">
                            <xsl:value-of select="@label" />
                        </xsl:if>
                        <xsl:if test="not(@label)">
                            <xsl:value-of select="@name" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

                <ul>
                    <xsl:choose>
                        <xsl:when
                            test="($separateChildrenInPages = true() or $showGroupsInTree = true()) and $depthLevel &lt;= $maxDepth"
                        >
                            <xsl:apply-templates select="./variable">
                                <xsl:with-param name="parent" select="concat($fullName, '.')" />
                                <xsl:with-param
                                    name="meIsAPage"
                                    select="$separateChildrenInPages"
                                />
                                <!--                        TODO: check this +1, before the refactoring was +2, why???-->
                                <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                                <xsl:with-param name="setLevel" select="$setLevel" />
                            </xsl:apply-templates>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="./variable">
                                <xsl:with-param name="parent" select="concat($fullName, '.')" />
                                <xsl:with-param
                                    name="meIsAPage"
                                    select="$separateChildrenInPages"
                                />
                                <xsl:with-param name="depthLevel" select="$depthLevel + 1" />
                                <xsl:with-param name="setLevel" select="$setLevel" />
                            </xsl:apply-templates>
                        </xsl:otherwise>
                    </xsl:choose>
                </ul>
            </li>
        </xsl:if>

    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:template match="@visible" mode="tree">
        <xsl:attribute name="v-if" select="." />
    </xsl:template>
</xsl:stylesheet>

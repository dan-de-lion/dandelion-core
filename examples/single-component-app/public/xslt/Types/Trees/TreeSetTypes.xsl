<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:template match="variable [ @dataType= 'set' ] ">
        <xsl:param name="parent" />
        <xsl:param name="meIsAPage" />
        <xsl:param name="depthLevel" />
        <xsl:param name="setLevel">0</xsl:param>

        <!-- TODO: unify with dataclass instance in DataClassTypes.xsl -->
        <xsl:variable name="separateChildrenInPages">
            <xsl:choose>
                <xsl:when test="@separateInPages">
                    <xsl:value-of select="true()" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="false()" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="dataClass" select="@itemsDataClass" />

        <xsl:if test="($showSetsInTree or $meIsAPage) and $setLevel &lt;= $maxDepth">

            <xsl:variable name="escapedFullName">
                <xsl:call-template name="escapeFullName">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="escapedFullNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName" select="$escapedFullName" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="elementName">
                <xsl:call-template name="createElementName">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="setLevel" select="$setLevel" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="escapedElementName">
                <xsl:call-template name="escapeFullName">
                    <xsl:with-param name="fullName" select="$elementName" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="escapedElementNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName" select="$escapedElementName" />
                </xsl:call-template>
            </xsl:variable>

            <xsl:variable name="protectedFullNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName">
                        <xsl:call-template name="protectFullName">
                            <xsl:with-param name="fullName" select="$fullName" />
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>

            <li>
                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">
                        <xsl:if test="@label and @label != ''">
                            <xsl:value-of select="@label" />
                        </xsl:if>
                        <xsl:if test="not(@label)">
                            <xsl:value-of select="@name" />
                        </xsl:if>
                    </xsl:with-param>
                </xsl:call-template>

                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $escapedFullName, $apos), ')')"
                />

                <ul class="{@name}">

                    <li
                        v-for="{ concat('(childElement, index', $setLevel, ') in ', $protectedFullNameWithModel) }"
                        track-by="childelement.guid"
                    >

                    <xsl:attribute
                            name="v-on:click.stop"
                            select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $escapedElementName, $apos), ')')"
                        />

                    <xsl:apply-templates select="@visible" mode="tree" />

                   <xsl:variable
                            name="itemLabelVariable"
                            select="//dataClass[ @name = $dataClass ]/variable[ @itemLabel ]/@name"
                        />

                    <xsl:attribute name="v-html">

                    <xsl:if test="$itemLabelVariable">
                        <xsl:value-of
                                    select="concat('childElement.', $itemLabelVariable, ' || translationService.t(', $apos, '-- new ', $dataClass, ' --', $apos, ')')"
                                />
                    </xsl:if>

                    <xsl:if test="not($itemLabelVariable)">
                       <xsl:value-of
                                    select="concat('translationService.t(', $apos, '-- new ', $dataClass, ' --', $apos, ')')"
                                />
                    </xsl:if>

                    </xsl:attribute>

                        <xsl:choose>
                            <xsl:when
                                test="(boolean($separateChildrenInPages) or $showSetsInTree) and $depthLevel &lt;= $maxDepth"
                            >
                                <xsl:variable name="depthLevelToChild" select="$depthLevel + 1" />
                                <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                    <xsl:with-param name="setLevel" select="$setLevel + 1" />
                                    <xsl:with-param name="parent" select="concat($elementName, '.')" />
                                    <xsl:with-param name="depthLevel" select="$depthLevelToChild" />
                                </xsl:apply-templates>
                            </xsl:when>
                            <!--TODO: check this +1, before the refactoring was +2, why???-->
                            <xsl:otherwise>
                                <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                                    <xsl:with-param name="setLevel" select="$setLevel + 1" />
                                    <xsl:with-param name="parent" select="concat($elementName, '.')" />
                                </xsl:apply-templates>
                            </xsl:otherwise>
                        </xsl:choose>
                    </li>
                </ul>
            </li>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>

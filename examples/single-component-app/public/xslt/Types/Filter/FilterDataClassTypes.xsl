<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:v-on="vue.vue"
>

  <xsl:template match="dataClass">
    <xsl:param name="parent" />
    <xsl:param name="valueLabel" />

    <xsl:variable name="currentDataClassName" select="@name" />

    <!-- TODO
    <button type="button" class="top-chevron" data-ng-click="{ concat('showMore = !showMore') }">
      <xsl:attribute name="data-ng-class" select="concat('{''icon-chevron-up'': showMore, ''icon-chevron-down'': !showMore}')" />
    </button>
    -->

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$root" />
      <xsl:with-param name="parentDataClass" select="$currentDataClassName" />
      <xsl:with-param name="valueLabel" select="$valueLabel" />
    </xsl:apply-templates>

    <!-- REVERSE -->
    <xsl:for-each select="//dataClass/variable[@dataType = $currentDataClassName]">
      <xsl:variable name="childDataClassName" select="(..)/@name" />
      <xsl:apply-templates select=".">
        <xsl:with-param name="parent" select="$root" />
        <xsl:with-param name="parentDataClass" select="$currentDataClassName" />
        <xsl:with-param name="currentDataClassFromParent" select="$childDataClassName" />
      </xsl:apply-templates>
    </xsl:for-each>
    <!-- END REVERSE -->

    <!-- TODO
    <button type="button" class="show-more" data-ng-click="{ concat('showMore = !showMore') }">
      <xsl:attribute name="data-ng-class" select="concat('{''icon-chevron-up'': showMore, ''icon-dots-three-horizontal'': !showMore}')" />
    </button>
    -->

    <!-- OR -->
<!--    <div>-->
<!--      <span>-->
<!--&lt;!&ndash;        <li>&ndash;&gt;-->
<!--&lt;!&ndash;          <xsl:call-template name="ic3DataClass">&ndash;&gt;-->
<!--&lt;!&ndash;            <xsl:with-param name="init" select="'newCrfs = {};'"/>&ndash;&gt;-->
<!--&lt;!&ndash;            <xsl:with-param name="visibility" select="'orEnabled'"/>&ndash;&gt;-->
<!--&lt;!&ndash;            <xsl:with-param name="parent" select="concat('newCrfs.', $currentDataClassName)"/>&ndash;&gt;-->
<!--&lt;!&ndash;            <xsl:with-param name="ic3DataType" select="concat('''', $currentDataClassName, '''')"/>&ndash;&gt;-->
<!--&lt;!&ndash;          </xsl:call-template>&ndash;&gt;-->
<!--&lt;!&ndash;        </li>&ndash;&gt;-->
<!--      </span>-->
<!--    </div>-->

  </xsl:template>

  <xsl:template match="variable">
    <xsl:param name="parent" />
    <xsl:param name="parentDataClass" />
    <xsl:param name="currentDataClassFromParent" />

<!--    <xsl:choose>-->
<!--      <xsl:when test="$currentDataClassFromParent">-->
<!--        <xsl:variable name="currentDataClass" select="$currentDataClassFromParent"/>-->
<!--        <xsl:variable name="fullName" select="concat($parent, '.instanceOf_', $currentDataClassFromParent)" />-->
<!--      </xsl:when>-->
<!--      <xsl:otherwise>-->
<!--        <xsl:variable name="currentDataClass" select="@dataType"/>-->
<!--        <xsl:variable name="fullName" select="concat($parent, '.instanceOf_', @dataType)" />-->
<!--      </xsl:otherwise>-->
<!--    </xsl:choose>-->
      <xsl:variable name="fullName" select="concat($parent, @name)" />


<!--      <xsl:variable name="fromParentToClassLabel" select="concat('from_', $parentDataClass, '_to_', $currentDataClass)"/>-->
    <span>
    <!-- TODO, data-ng-init in vue -->
<!--      <xsl:attribute name="init-value">-->
<!--        <xsl:value-of select="concat('(', $fullName, ' === undefined)? ', $fullName, ' = []: ', $fullName, '=', $fullName )"/>-->
<!--      </xsl:attribute>-->

<!--      <xsl:attribute name="v-for" select="concat('instance in ', $fullName, '')"/>-->
<!--      <xsl:attribute name="track-by" select="'$index'"/>-->
<!--        <span class="or-label">-->
<!--          <xsl:attribute name="v-show" select="concat('$index', '&gt; 0')"></xsl:attribute>-->
<!--          &lt;!&ndash; LABEL BELOW SHOULD BE TRANSLATED&ndash;&gt;-->
<!--          oppure-->
<!--        </span>-->

        <!-- TODO, data-ng-init in vue -->
<!--          <xsl:attribute name="init-value">-->
<!--            <xsl:value-of select="concat('instance.show = ', $root, '.visibilityConfig.', $parentDataClass, '.', @name)" />-->
<!--          </xsl:attribute>-->
          <xsl:call-template name="applyVisibility" />
      <!-- LABEL BELOW SHOULD BE TRANSLATED WITH PROPER TEMPLATE-->
          <label>
            <xsl:call-template name="translateString">
              <xsl:with-param name="string">
                <xsl:value-of select="$fromParentToClassLabel" />
              </xsl:with-param>
            </xsl:call-template>
          </label>
      <!-- Add / remove buttons -->
          <button type="button" class="icon-cross">
            <xsl:attribute name="v-on:click.stop">
                <xsl:call-template name="translateString">
              <xsl:with-param name="string">
                <xsl:value-of select="concat($fullName, '.splice($index, 1)')" />
              </xsl:with-param>
            </xsl:call-template>
            </xsl:attribute>
          </button>

          <xsl:variable
                name="dataTypeForClass"
                select="concat('instance.dataType = ''', $currentDataClass, '''')"
            />
          <span>
        <!-- TODO, data-ng-init in vue -->
        <!--            <xsl:call-template name="ic3DataClass">-->
        <!--              <xsl:with-param name="init" select="concat('showMore = false; ', $dataTypeForClass, ';', 'instance.onKey = ''', @name, '''')"/>-->
        <!--              <xsl:with-param name="visibility" select="'instance.show = true'"/>-->
        <!--              <xsl:with-param name="parent" select="'instance.variables'"/>-->
        <!--              <xsl:with-param name="ic3DataType" select="concat('''', $currentDataClass, '''')"/>-->
        <!--            </xsl:call-template>-->
          </span>
    </span>
<!--    <button type="button" class="dataclass-link">-->
<!--      <xsl:attribute name="v-on:click" select="concat('newCrfs = {};', $fullName, '.push(newCrfs); instance.show = true')"/>-->
<!--      <xsl:value-of select="$translatedFromParentToClassLabel"/>-->
<!--    </button>-->
  </xsl:template>

</xsl:stylesheet>

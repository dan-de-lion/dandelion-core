<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->

    <xsl:template match="dataClass">
        <xsl:param name="setLevel">0</xsl:param>
        <xsl:param name="parent" />

        <xsl:variable name="selectedParent">
            <xsl:choose>
                <xsl:when test="not($parent)">
                    <xsl:value-of select="''" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat($parent, '.')" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="setLevel" select="$setLevel" />
            <xsl:with-param name="parent" select="$selectedParent" />
        </xsl:apply-templates>

        <xsl:apply-templates select="./warning">
            <xsl:with-param name="fullName" select="$selectedParent" />
        </xsl:apply-templates>

    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable">
        <xsl:param name="parent" />

        <xsl:variable name="dataType" select="@dataType" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="dataClassExists" select="//dataClass[@name = $dataType]/@name" />

        <xsl:if test="$dataClassExists and @dataType != 'reference'">
            <DataclassItemComponent>

                <xsl:variable name="fullNameWithModel">
                    <xsl:call-template name="addModelBeforeFullName">
                        <xsl:with-param name="fullName" select="$fullName" />
                    </xsl:call-template>
                </xsl:variable>

                <xsl:attribute name=":full-name" select="concat($apos, $fullNameWithModel , $apos)" />
                <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
                <xsl:attribute name=":model" select="$root" />

                <xsl:element name="my-custom-template">
                    <xsl:attribute name="v-slot:default">{ thisSet, thisObject, thisIndex }</xsl:attribute>
                    <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
                        <xsl:with-param name="parent" select="$fullName" />
                    </xsl:apply-templates>
                </xsl:element>
            </DataclassItemComponent>
        </xsl:if>

        <xsl:apply-templates select="./warning">
            <xsl:with-param name="fullName" select="$fullName" />
        </xsl:apply-templates>

    </xsl:template>

</xsl:stylesheet>

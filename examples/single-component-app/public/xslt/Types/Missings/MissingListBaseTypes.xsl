<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable [ @dataType= 'group' ] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="fullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

          <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

    <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
      <li
                v-show="!{$fullNameWithModel} &amp;&amp; caseStatusService.isVariableMissingForMissingList('{ $fullNameWithModel }', null) &amp;&amp; userService?.canRead('{$escapedFullName}, {$crfName}, {$crfVersion}')"
            >
                <xsl:attribute
                    name=":class"
                    select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos, ')'), '}')"
                />
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullNameWithModel, $apos), ')')"
                />
                <xsl:call-template name="applyVisibility">
                    <xsl:with-param name="fullName" select="$fullNameWithModel" />
                </xsl:call-template>
            </li>
        </xsl:if>

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>

        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="fullName" select="$fullName" />
        </xsl:apply-templates>

    </xsl:template>

    <xsl:template match="variable [ @dataType= 'set' ] ">
        <xsl:param name="parent" />
        <xsl:param name="setLevel" />

        <xsl:variable name="dataClass" select="@itemsDataClass" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="fullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>

        <xsl:variable name="protectedFullName" select="replace($fullName, '\.', '?.')" />

        <xsl:variable name="elementNameWithModel">
            <xsl:call-template name="createElementName">
                <xsl:with-param name="fullName" select="$fullNameWithModel" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="elementName">
            <xsl:call-template name="createElementName">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="setLevel" select="$setLevel" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedElementName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$elementNameWithModel" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullNameWithModel" />
            </xsl:call-template>
        </xsl:variable>

        <SetComponent>
            <xsl:attribute name=":full-name" select="concat($apos, $escapedFullName , $apos)" />
            <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
            <xsl:attribute name=":this-set" select="$protectedFullName" />
            <xsl:attribute name=":this-object">thisObject</xsl:attribute>
            <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
            <xsl:attribute name=":model" select="$root" />

            <xsl:element name="my-custom-template">
                <xsl:attribute
                    name="v-for"
                    select="concat('(childElement, index', $setLevel, ') in ', $protectedFullName)"
                />

                <SetItemComponent>
                    <xsl:attribute name=":full-name" select="concat($apos, $escapedElementName , $apos)" />
                    <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
                    <xsl:attribute name=":this-set">thisSet</xsl:attribute>
                    <xsl:attribute name=":this-object" select="$elementNameWithModel" />
                    <xsl:attribute name=":this-index" select="concat('index', $setLevel)" />
                    <xsl:attribute name=":model" select="$root" />

                    <xsl:element name="my-custom-template">
                        <xsl:attribute name="v-slot:default">{ thisSet, thisObject, thisIndex }</xsl:attribute>

                        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]">
                            <xsl:with-param name="setLevel" select="$setLevel + 1" />
                            <xsl:with-param name="parent" select="$elementName" />
                        </xsl:apply-templates>
                    </xsl:element>
                </SetItemComponent>
            </xsl:element>
        </SetComponent>

        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="fullName" select="$fullName" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="requirement">
        <xsl:param name="fullName" />

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="requirementName" select="concat( $fullName, '.REQUIREMENT_', @name)" />

    <li
            class="requirement"
            v-show="!{ @satisfiedIf } &amp;&amp; userService?.canRead('{$escapedFullName}, {$crfName}, {$crfVersion}')"
        >
        <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />

        <xsl:attribute
                name="v-on:click.stop"
                select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $fullName, $apos), ')')"
            />
        <xsl:value-of select="." />
    </li>
  </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'output' ]">
        <xsl:if test="not (@computedFormula)">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('Computed Formula missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
          </li>
        </xsl:if>

        <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('requiredForStatus should be specified and set to ', $apos, 'false', $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
          </li>
        </xsl:if>
    <xsl:next-match />
  </xsl:template>

    <xsl:template match="variable[ @dataType = 'support'] ">
        <xsl:if test="not (@computedFormula)">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('Computed Formula missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
          </li>
        </xsl:if>

     <xsl:if test="not(@requiredForStatus) or @requiredForStatus != 'false'">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('requiredForStatus should be specified and set to ', $apos, 'false', $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
          </li>
        </xsl:if>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'singlechoice']">
        <xsl:if test="not (@valuesSet) and not (./value)">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('ValuesSet and Value missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
          </li>
        </xsl:if>

        <xsl:variable name="valuesSetName" select="@valuesSet" />
        <xsl:if test="//valuesSet[ @name = $valuesSetName ]/value/excludes">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('Value in valuesSet ', $apos, @valuesSet, $apos, ' in variable ', $apos, @name, $apos, ' shouldn', $apos, 't have excludes tag. Do you intend multiplechoice instaed?')"
                />
          </li>
        </xsl:if>
  </xsl:template>

  <xsl:template match="variable[ @dataType = 'multiplechoice']">

        <xsl:if test="not (@valuesSet) and not (./value)">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat('ValuesSet and Value missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
          </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'set' ]">

    <xsl:param name="parent" />
    <xsl:param name="setLevel" />

        <xsl:if test="not (@itemsDataClass)">
          <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                    select="concat($apos, 'ItemsDataClass' , $apos, ' missing in variable ', $apos, @name, $apos, ' that is a type ', $apos, 'set' , $apos)"
                />
          </li>
        </xsl:if>

    <xsl:if test="@itemsDataClass">

          <xsl:variable name="dataClass" select="@itemsDataClass" />

      <xsl:if test="@separateInPages and not(//dataClass[ @name = $dataClass ]/variable/@itemLabel)">
            <li>
              <xsl:attribute name="class" select="'developerError'" />
              <xsl:value-of
                        select="concat($apos, 'ItemLabel', $apos, ' missing in dataClass ', $apos, @itemsDataClass, $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', $apos, 'set', $apos)"
                    />
            </li>
          </xsl:if>

      <xsl:if test="@computedItems and not(//dataClass[ @name = $dataClass ]/variable/@itemKey)">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                        select="concat($apos, 'ItemKey', $apos, ' missing in dataClass ', $apos, @itemsDataClass, $apos, ' in variable ', $apos, @name, $apos, 'that is a type ', $apos, 'set', $apos)"
                    />
        </li>
      </xsl:if>

    </xsl:if>

  </xsl:template>

  <xsl:template match="variable[@itemKey and @itemKey != 'true']">
    <li>
      <xsl:attribute name="class" select="'developerError'" />
      <xsl:value-of select="concat('itemKey should always be true')" />
    </li>
    <xsl:next-match />
  </xsl:template>

    <xsl:template match="variable[not (@name)]">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="concat('Name missing in a variable that is a type ', @dataType)" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@name = '']">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
      <xsl:value-of select="concat('Name in a variable that is a type ', @dataType, ' should not be empty')" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@visible = '']">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="'Visible in a variable should not be empty'" />
        </li>
    <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@computedFormula = '']">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="'ComputedFormula in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType = '']">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="'DataType in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@valuesSet = '']">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="'ValuesSet in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@itemsDataClass = '']">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="'ItemsDataClass in a variable should not be empty'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[not (@dataType)]">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of select="concat('DataType missing in variable ', $apos, @name, $apos)" />
        </li>
        <xsl:next-match />
    </xsl:template>

  <xsl:template match="variable [ @dataType != 'number' and (@max or @min or @precision or @measureUnit)] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('Number Attributes should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'text' and  @interface = 'textArea'] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('Interface ', $apos, 'textArea', $apos, ' should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

  <xsl:template match="variable [ @dataType != 'singlechoice' and @dataType != 'multiplechoice' and  @valuesSet] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('ValuesSet should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'date' and  (@minDate or @maxDate)] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('Date attributes should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

  <xsl:template match="variable [ @dataType != 'group' and @dataType != 'set' and  @separateInPages] ">
        <!--Add
        variable instance of dataClass-->
         <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('SeparateInPages should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'set' and  @itemsDataClass] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('ItemsDataClass should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'group' and ./variable] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('There shouldn', $apos, 't be other variables as children of variable ', $apos, @name, $apos, ' that is a type ', @dataType, ' and not a type ', $apos, 'group.', $apos, ' Maybe tag variable is not closed?')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'text' and @pattern] ">
        <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('Pattern attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

  <xsl:template match="variable [ @pattern and (@pattern = '' or normalize-space(@pattern) = ' ')] ">
   <li>
          <xsl:attribute name="class" select="'developerError'" />
          <xsl:value-of
                select="concat('Pattern attribute should not be empty in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
    <xsl:next-match />
  </xsl:template>

  <xsl:template match="variable [ @dataType = 'plugin']">
    <xsl:variable name="regexPattern">^[a-z0-9_]+$</xsl:variable>
    <xsl:choose>
      <xsl:when test="matches(@interface, $regexPattern)">
        <!-- La stringa rispetta la regex -->
      </xsl:when>
      <xsl:otherwise>
        <!-- La stringa non rispetta la regex, genera un messaggio di errore -->
           <li>
            <xsl:attribute name="class" select="'developerError'" />
            <xsl:value-of
                        select="concat('Error: the interface value does not respect regex ', $apos, $regexPattern, $apos, '. The interface should not have upper case or special characters')"
                    />
          </li>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

   <xsl:template match="variable[ @ignoreCRFErrors = 'true' ]">
  </xsl:template>

  <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeRadioNotDefined" mode="radio" />
  <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeSelectNotDefined" mode="select" />
  <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeCheckboxNotDefined" mode="checkbox" />

</xsl:stylesheet>

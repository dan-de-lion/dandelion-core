<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:v-on="vue.vue"
>

  <xsl:template match="root">
    <xsl:apply-templates select="./dataClass" />
  </xsl:template>

  <!-- ######## GROUP ######## -->
  <!-- container for other variables -->

  <xsl:template match="variable [ @dataType = 'group' ] ">
    <xsl:param name="parent" />
    <xsl:param name="meIsAPage" />

    <xsl:variable name="dataType" select="@dataType" />
    <xsl:choose>
      <xsl:when test="@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="@initValue">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of
                    select="concat('Init value attribute is not in variable: ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
      </li>
    </xsl:if>

    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <xsl:if test="not (./variable)">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of
                    select="concat('There are no childrens in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
      </li>
    </xsl:if>

    <xsl:variable name="nextParent" select="$fullName" />

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="parent" select="$nextParent" />
      <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
    </xsl:apply-templates>

  </xsl:template>

  <!-- ######## DATACLASS ######## -->
  <!-- same as a "group" of variables, but can be inserted in different points of the data collection -->

  <xsl:template match="dataClass">
    <xsl:param name="setLevel">0</xsl:param>

    <!-- for storing the name of the variable with @valuesSetKey -->
    <!-- (use this for $arrayOfExcludes) -->
    <xsl:param name="containerName" />
    <!-- myValue represents the valuesSetName.@name string that will allow the future variable -->
    <!-- to identify itself (use this for $arrayOfExcludes) -->
    <xsl:param name="myValue" />
    <xsl:param name="parent" />
    <xsl:param name="labelFromRepeater" />
    <xsl:param name="separateChildrenInPages" />
    <xsl:param name="arrayOfExcludes" />

    <xsl:if test="@name = 'NewCase'">
      <!--TODO-->
    </xsl:if>

    <xsl:if test="@activateIf and not (position() = 1)">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of
                    select="concat('ActivateIf in dataClass ', $apos, @name, $apos, ' should be only in first dataClass')"
                />
      </li>
    </xsl:if>

    <xsl:if test="@initValue">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of
                    select="concat('Init value attribute is not in variable: ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
      </li>
    </xsl:if>

    <xsl:if test="@name = 'NewCase' and not (position() = last())">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of select="'New Case should be the last dataClass'" />
      </li>
    </xsl:if>

    <xsl:if test="not (./variable)">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of select="concat('There are no childrens in dataClass ', $apos, @name, $apos)" />
      </li>
    </xsl:if>

    <xsl:choose>
      <xsl:when test="not($parent)">
        <xsl:variable name="selectedParent" select="$root" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="selectedParent" select="$parent" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:apply-templates select="./variable">
      <xsl:with-param name="setLevel" select="$setLevel" />
      <xsl:with-param name="parent" select="$selectedParent" />
      <xsl:with-param name="myValue" select="$myValue" />
      <xsl:with-param name="containerName" select="$containerName" />
      <xsl:with-param name="labelFromRepeater" select="$labelFromRepeater" />
      <xsl:with-param name="meIsAPage" select="$separateChildrenInPages" />
      <xsl:with-param name="arrayOfExcludes" select="$arrayOfExcludes" />
    </xsl:apply-templates>

  </xsl:template>

  <!-- ######## INSTANCE OF DATACLASS ######## -->
  <!-- variable with datatype not in BaseTypes -->
  <!-- searches for a dataclass that has the same name of the datatype -->

  <xsl:template match="variable">

    <xsl:param name="parent" />
    <xsl:param name="meIsAPage" />

    <xsl:variable name="dataClassExists" select="//dataClass[@name = $dataType]/@name" />

    <!--<xsl:if test="$dataClassExists">
    <xsl:apply-templates select = "." mode="dataClassInstance">
      <xsl:with-param name="parent" select = "$parent"/>
      <xsl:with-param name="meIsAPage" select = "$meIsAPage"/>
    </xsl:apply-templates>
  </xsl:if>
  <xsl:if test="not ($dataClassExists)">
   <xsl:next-match />
  </xsl:if>-->
  </xsl:template>


  <xsl:template match="variable" mode="dataClassInstance">
    <xsl:param name="parent" />
    <xsl:param name="meIsAPage" />
    <xsl:variable name="dataType" select="@dataType" />

    <xsl:choose>
      <xsl:when test="@separateInPages or //dataClass[ @name = $dataType ]/@separateInPages">
        <xsl:variable name="separateChildrenInPages" select="true()" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="separateChildrenInPages" select="false()" />
      </xsl:otherwise>
    </xsl:choose>

    <xsl:if test="@initValue">
      <li>
        <xsl:attribute name="class" select="'developerError'" />
        <xsl:value-of
                    select="concat('Init value attribute is not in variable: ', $apos, @name, $apos, ' that is a type ', @dataType)"
                />
      </li>
    </xsl:if>

    <xsl:variable name="fullName" select="concat($parent, @name)" />

    <xsl:variable name="nextParent" select="$fullName" />

    <DataclassItemComponent>

      <xsl:variable name="fullNameWithModel">
        <xsl:call-template name="addModelBeforeFullName">
          <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
      </xsl:variable>

      <xsl:attribute name=":full-name" select="concat($apos, $fullNameWithModel , $apos)" />
      <xsl:attribute name=":root-as-string" select="concat($apos, $root, $apos)" />
      <xsl:attribute name=":model" select="$root" />

      <xsl:element name="my-custom-template">
        <xsl:attribute name="v-slot:default" select="concat('{ thisSet, thisObject, thisIndex }')" />

        <xsl:apply-templates select="//dataClass[ @name = $dataType ]">
          <xsl:with-param name="parent" select="$nextParent" />
          <xsl:with-param name="separateChildrenInPages" select="$separateChildrenInPages" />
        </xsl:apply-templates>
      </xsl:element>
    </DataclassItemComponent>

  </xsl:template>

</xsl:stylesheet>

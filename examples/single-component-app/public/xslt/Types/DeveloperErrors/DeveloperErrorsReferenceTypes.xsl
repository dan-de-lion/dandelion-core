<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'reference' ]">
        <xsl:apply-templates select="/root/dataClass[1]" mode="referenceResolution">
            <xsl:with-param name="variableReference" select="@fullname" />
            <xsl:with-param name="parent" select="$root" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="variable | dataClass" mode="referenceResolution">
        <xsl:param name="variableReference" />
        <xsl:param name="parent" />

        <xsl:variable name="variableToFind">
            <xsl:call-template name="firstChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="rest">
            <xsl:call-template name="restChunk">
                <xsl:with-param name="fullNameToTrim" select="$variableReference" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:if test="$variableToFind = ''">
            <xsl:apply-templates select="//variable[@name = $rest]">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="$variableToFind != ''">
            <xsl:apply-templates
                select="//variable[@name = $variableToFind]"
                mode="referenceResolution"
            >
                <xsl:with-param name="variableReference" select="$rest" />
                <xsl:with-param name="parent" select="concat($parent, '.', $variableToFind)" />
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>


    <xsl:template name="firstChunk">
        <xsl:param name="fullNameToTrim" />

        <xsl:value-of select="substring-before(substring-after($fullNameToTrim, '.'), '.')" />
    </xsl:template>

    <xsl:template name="restChunk">
        <xsl:param name="fullNameToTrim" />

        <xsl:value-of select="substring-after($fullNameToTrim,'.')" />
    </xsl:template>

</xsl:stylesheet>

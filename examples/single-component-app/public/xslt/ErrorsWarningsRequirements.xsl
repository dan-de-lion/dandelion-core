<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## ERRORS and WARNINGS ########## -->

    <xsl:template match="error">
        <xsl:param name="fullName" />

        <xsl:variable name="errorName" select="concat( $fullName, 'ERROR_', @name)" />

        <xsl:variable name="escapedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$errorName" />
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <div
            class="error"
            v-custom-model-for-errors="'{$escapedFullNameWithModel}'"
            init-value="null"
            v-if="{ @if } &amp;&amp; { concat('userService.canRead(', $apos,  $escapedFullName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')') }"
        >

            <xsl:attribute
                name=":disabled"
                select="concat('!userService.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
            />

            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />


            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$errorName" />
            </xsl:call-template>
               <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="warning">
        <xsl:param name="fullName" />

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="warningName" select="concat( $fullName, 'WARNING_', @name, '_CONFIRMED')" />

        <div
            v-if="{ @if } &amp;&amp; { concat('userService.canRead(', $apos,  $escapedFullName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')') }"
        >

            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos,',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="concat($warningName, '-wrapper')" />
            </xsl:call-template>

            <xsl:variable name="escapedWarningNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName">
                        <xsl:call-template name="protectFullName">
                            <xsl:with-param name="fullName" select="$warningName" />
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>

            <xsl:attribute name=":class">
                <xsl:value-of
                    select="concat('{''warning'': !', $escapedWarningNameWithModel, ', ''solvedWarning'': ', $escapedWarningNameWithModel, '}')"
                />
            </xsl:attribute>
            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
            <xsl:variable name="escapedFullNameWithModel">
                <xsl:call-template name="addModelBeforeFullName">
                    <xsl:with-param name="fullName">
                        <xsl:call-template name="escapeFullName">
                            <xsl:with-param name="fullName" select="$warningName" />
                        </xsl:call-template>
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:variable>
            <xsl:variable name="escapedFullName">
                <xsl:call-template name="escapeFullName">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
            </xsl:variable>

            <input type="checkbox" v-custom-model-for-warnings="'{$escapedFullNameWithModel}'">
                <xsl:attribute
                    name=":disabled"
                    select="concat('!userService.canWrite(', $apos, $escapedFullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                />
                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$warningName" />
                </xsl:call-template>
            </input>
            <label>
                <xsl:attribute name=":for" select="concat($apos, $warningName, $apos)" />

                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">I confirm the inserted data</xsl:with-param>
                </xsl:call-template>
            </label>
        </div>
    </xsl:template>

    <xsl:template match="requirement">
        <xsl:param name="fullName" />

        <xsl:variable name="requirementName" select="concat( $fullName, '.REQUIREMENT_', @name)" />

        <xsl:variable name="escapedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param name="fullName" select="$requirementName" />
                    </xsl:call-template>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <div
            class="requirement"
            v-if="!({ @satisfiedIf })"
            v-custom-model-for-requirements="'{$escapedFullNameWithModel}'"
            init-value="null"
        >

            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!userService?.canWrite(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$requirementName" />
            </xsl:call-template>

              <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="description">
        <p class="variableLabel">
              <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
        </p>
    </xsl:template>

</xsl:stylesheet>

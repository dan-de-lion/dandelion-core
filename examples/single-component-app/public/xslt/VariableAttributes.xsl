<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template name="applyVisibility">
        <xsl:param name="fullName" />
        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable> 
        <xsl:variable name="escapedFullNameWithModel">
            <xsl:call-template name="addModelBeforeFullName">
                <xsl:with-param name="fullName" select="$escapedFullName" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="@visible and @visible != '' ">
                <xsl:attribute name="v-if">
                    <xsl:value-of
                        select="concat('userService?.canRead(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,') &amp;&amp; (', @visible, ')')"
                    />
                </xsl:attribute>
                <xsl:attribute name="v-custom-if">
                    <xsl:value-of select="concat($apos, $escapedFullNameWithModel, $apos)" />
                </xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="v-if">
                    <xsl:value-of
                        select="concat('userService?.canRead(', $apos, $escapedFullName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')')"
                    />
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- ######## DESCRIPTION AND TOOLTIP ######## -->

    <xsl:template name="addDescription">
        <xsl:param name="fullName" />
        <xsl:param name="separatedTooltip">true</xsl:param>
        <xsl:param name="element">label</xsl:param>
        <xsl:param name="cssclass">variableLabel</xsl:param>

        <xsl:element name="{ $element }">
            <xsl:attribute name="class" select="$cssclass" />
            <xsl:if test="$fullName and ($fullName != '') and ($element = 'label')">
                <xsl:attribute name=":for">
                    <xsl:call-template name="escapeFullName">
                        <xsl:with-param
                            name="fullName"
                            select="concat('getContextForID + ', $apos, $fullName, $apos)"
                        />
                    </xsl:call-template>
                </xsl:attribute>
            </xsl:if>
            <xsl:if test="$separatedTooltip and $separatedTooltip = 'false'">
                <xsl:apply-templates select="@tooltip" />
            </xsl:if>
            <!-- moved it inside of element so as to keep it on same line as label -->
            <xsl:if test="$separatedTooltip and $separatedTooltip = 'true'">
                <xsl:call-template name="addSeparatedTooltip" />
            </xsl:if>

            <xsl:call-template name="getTranslatedLabel" />

        </xsl:element>

        <xsl:attribute name="v-on:mouseover.stop" select="concat('onHover(', $apos, $escapedFullName, $apos, ')')">
        </xsl:attribute>

    </xsl:template>

    <xsl:template name="addSeparatedTooltip">
        <xsl:if test="@tooltip and @tooltip != ''">
            <xsl:variable name="escapedString">
                <xsl:apply-templates select="@tooltip" mode="escape" />
            </xsl:variable>
            <!-- Translation is taken care of inside the directive -->
            <xsl:variable name="tooltipText" select="concat($apos, $escapedString, $apos)" />
            <span>
                <xsl:attribute name="tooltip">
                    <xsl:call-template name="translateString">
                        <xsl:with-param name="string">
                            <xsl:value-of select="$tooltipText" disable-output-escaping="yes" />
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:attribute>
                ? </span>
        </xsl:if>
    </xsl:template>

    <xsl:template match="@tooltip">
        <xsl:attribute name="title">
            <xsl:call-template name="translateString">
                <xsl:with-param name="string" select="." />
            </xsl:call-template>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="getTranslatedLabel">

        <xsl:variable name="text">
            <xsl:choose>
                <xsl:when test="@label and @label != ''">
                    <xsl:value-of select="@label" />
                </xsl:when>
                <xsl:when test="@name">
                    <xsl:value-of select="@name" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="'-- this variable has no label or name! --'" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:call-template name="translateString">
            <xsl:with-param name="string" select="$text" />
        </xsl:call-template>
    </xsl:template>


    <!-- ######## SIMPLE ATTRIBUTES ######## -->

    <xsl:template match="@precision">
        <xsl:variable name="prec" select="." />
        <xsl:attribute name="step">
            <xsl:if test="$prec != '' and $prec != 0">
                <xsl:value-of select="'0.'" />
            </xsl:if>
            <xsl:for-each select="1 to . -1">
                <xsl:value-of select="0" />
            </xsl:for-each>
            <xsl:value-of select="1" />
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="@computedFormula">
        <xsl:call-template name="setComputedFormula">
            <xsl:with-param name="computedFormula" select="." />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="setComputedFormula">
        <xsl:param name="computedFormula" />

        <xsl:attribute name="v-short-circuit" select="$computedFormula" />
    </xsl:template>

    <xsl:template match="excludes">
        <xsl:call-template name="translateString">
            <xsl:with-param name="string">
                <xsl:value-of select="." />
            </xsl:with-param>
        </xsl:call-template>

        <xsl:if test="position() != last()">
            ,
        </xsl:if>
    </xsl:template>

    <!-- ######## ID ######## -->

    <xsl:template name="addID">
        <xsl:param name="fullName" />

        <xsl:call-template name="addGenericID">
            <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="addWrapperID">
        <xsl:param name="fullName" />

        <xsl:call-template name="addGenericID">
            <xsl:with-param name="fullName" select="$fullName" />
            <xsl:with-param name="suffix" select="'-wrapper'" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="addGenericID">
        <xsl:param name="fullName" />
        <xsl:param name="suffix" select="''" />

        <xsl:variable name="escapedFullName">
            <xsl:call-template name="escapeFullName">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>
        </xsl:variable>

        <xsl:attribute name=":id" select="concat('getContextForID ', '+ ''', $escapedFullName, $suffix, $apos)" />
    </xsl:template>

    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeEscapeNotDefined" mode="escape" />

</xsl:stylesheet>

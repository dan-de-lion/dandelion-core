<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../Utils.xsl" />
    <xsl:import href="../VariableAttributes.xsl" />
    <xsl:import href="../VariableStructure.xsl" />
    <xsl:import href="../Types/Collection/CollectionBaseTypes.xsl" />
    <xsl:import href="../Types/Collection/CollectionDataClassTypes.xsl" />
    <xsl:import href="../Types/Collection/CollectionReferenceTypes.xsl" />
    <xsl:import href="../Types/Collection/CollectionSetTypes.xsl" />
    <xsl:import href="../Types/Collection/CollectionValueTypes.xsl" />
    <xsl:import href="../Types/Collection/CollectionNavigation.xsl" />
    <xsl:import href="../ErrorsWarningsRequirements.xsl" />

  <xsl:variable name="apos">'</xsl:variable>

  <xsl:param name="root" />
  <xsl:param name="statisticianMode" />
  <xsl:param name="crfName" />
  <xsl:param name="crfVersion" />
    <xsl:param name="startingDataClass" />

    <xsl:param name="DEBUG" select="false()" />
    <xsl:output method="html" indent="yes" />

</xsl:stylesheet>

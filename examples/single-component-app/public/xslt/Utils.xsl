<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## STRING MANIPULATION ######## -->

    <xsl:template name="translateString">
        <xsl:param name="string" />

        <xsl:variable name="escapedString">
            <xsl:call-template name="escape">
                <xsl:with-param name="string" select="$string" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable
            name="translation"
            select="concat('translationService.t(', $apos, $escapedString, $apos, ')')"
        />
        <span>
            <xsl:attribute name="v-html">
                <xsl:value-of select="$translation" disable-output-escaping="yes" />
            </xsl:attribute>
        </span>
    </xsl:template>

    <xsl:template name="escape">
        <xsl:param name="string" />

        <!-- Escape the backslashes first... -->
        <xsl:variable name="firstEscape" select="replace($string, '\', '\\')" />

        <!-- ... then escapes the apostrophes -->
        <xsl:value-of select="replace($firstEscape, $apos, concat('\', $apos))" />
    </xsl:template>

    <xsl:template name="escapeFullName">
        <xsl:param name="fullName" />
        <xsl:variable name="support" select="replace($fullName, ']', '+ '']')" />
        <xsl:value-of select="replace($support, '\[', '['' +')" />
    </xsl:template>

    <xsl:template name="addModelBeforeFullName">
        <xsl:param name="fullName" />
        <xsl:value-of select="concat('model.', $fullName)" />
    </xsl:template>

    <xsl:template name="protectFullName">
        <xsl:param name="fullName" />
        <xsl:value-of select="replace($fullName, '\.', '?.')" />
    </xsl:template>

    <xsl:template name="createElementName">
        <xsl:param name="fullName" />
        <xsl:param name="setLevel" />
        <xsl:value-of select="concat($fullName, '[ index', $setLevel, ' ]')" />
    </xsl:template>

    <xsl:template name="selectParent">
        <xsl:param name="parent" />
        <xsl:variable
            name="cleanedRoot"
            select="replace(replace($root, 'model.', ''), 'model', '')"
        />

        <xsl:choose>
            <xsl:when test="not($parent) and $cleanedRoot != ''">
                <xsl:value-of select="concat($cleanedRoot, '.')" />
            </xsl:when>
            <xsl:when test="not($parent) and $cleanedRoot = ''">
                <xsl:value-of select="$cleanedRoot" />
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat($parent, '.')" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--    <ic3-autocomplete page="1" limit="10" type="{ @dataType }" parent="{ $fullName }">-->
    <!--      <xsl:apply-templates select="@computedFormula" />-->
    <!--      <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]">-->
    <!--        <xsl:with-param name="valuesSetName" select="$valuesSetName" />-->
    <!--      </xsl:apply-templates>-->
    <!--      <xsl:call-template name="addID">-->
    <!--        <xsl:with-param name="fullName" select="$fullName" />-->
    <!--      </xsl:call-template>-->
    <!--      <xsl:attribute name="v-custom-model" select="concat($fullName, '.filter' )"/>-->
    <!--    </ic3-autocomplete>-->
    <!--  </xsl:template>-->

    <!--  <xsl:template name="ic3DataClass">-->
    <!--    <xsl:param name="visibility"/>-->
    <!--    <xsl:param name="parent"/>-->
    <!--    <xsl:param name="ic3DataType"/>-->
    <!--    <div v-if="{ $visibility }">-->
    <!--      <ic3-data-class-->
    <!--        xml="xml"-->
    <!--        processor="processor"-->
    <!--        parent="{ $parent }"-->
    <!--        loaded="loaded"-->
    <!--        ic3-data-class="{ $ic3DataType }"-->
    <!--      >-->
    <!--      </ic3-data-class>-->
    <!--    </div>-->
    <!--  </xsl:template>-->

    <xsl:template match="valuesSet[ @sourceType = 'index' ]">
        <xsl:param name="valuesSetName" />

        <xsl:attribute name="class-name" select="//valuesSet[ @name = $valuesSetName ]/class" />
        <xsl:attribute name="index-name" select="//valuesSet[ @name = $valuesSetName ]/name" />
    </xsl:template>

    <xsl:template match="valuesSet[ @sourceType = 'table' ]">
        <xsl:param name="valuesSetName" />

        <xsl:attribute name="table-name" select="//valuesSet[ @name = $valuesSetName ]/table" />
        <xsl:attribute name="column-name" select="//valuesSet[ @name = $valuesSetName ]/column" />
    </xsl:template>

</xsl:stylesheet>

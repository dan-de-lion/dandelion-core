export function isActive(
  container,
  aliveOrDead,
  dimissionDate,
  reminderDate,
  daysBeforeExpiration
) {
  if (aliveOrDead == 'VivoMorto.vivo' && !dimissionDate) {
    container
      .get('ReminderService')
      .addReminderToQueue('3', '1', '1', dimissionDate, reminderDate, daysBeforeExpiration);
    return true;
  }
  if (aliveOrDead == 'VivoMorto.vivo' && dimissionDate) {
    container
      .get('ReminderService')
      .addReminderToQueue('3', '1', '1', dimissionDate, reminderDate, daysBeforeExpiration);
    return true;
  }
  if (aliveOrDead == 'VivoMorto.morto') {
    container.get('ReminderService').removeReminder('3');
    return false;
  }
  return false;
}

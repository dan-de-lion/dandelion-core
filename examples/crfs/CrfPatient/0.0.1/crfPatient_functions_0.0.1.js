export function calculateBMI(thisObject) {
  const h = thisObject?.SchedaDemografica?.altezza;
  return Math.round(((thisObject?.SchedaDemografica?.peso * 10000) / (h * h)) * 100.0) / 100.0;
}

export function PazEscluso(model) {
  if (
    !model?.crfPatient?.screening?.CI?.firmaCI ||
    model?.crfPatient?.screening?.CI?.firmaCI == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci1 ||
    model?.crfPatient?.screening?.CInclusione?.ci1 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci2 ||
    model?.crfPatient?.screening?.CInclusione?.ci2 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci3 ||
    model?.crfPatient?.screening?.CInclusione?.ci3 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci4 ||
    model?.crfPatient?.screening?.CInclusione?.ci4 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci5 ||
    model?.crfPatient?.screening?.CInclusione?.ci5 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci6 ||
    model?.crfPatient?.screening?.CInclusione?.ci6 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci7 ||
    model?.crfPatient?.screening?.CInclusione?.ci7 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci8 ||
    model?.crfPatient?.screening?.CInclusione?.ci8 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci9 ||
    model?.crfPatient?.screening?.CInclusione?.ci9 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci10 ||
    model?.crfPatient?.screening?.CInclusione?.ci10 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CInclusione?.ci11 ||
    model?.crfPatient?.screening?.CInclusione?.ci11 == 'siNo.0' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce1 ||
    model?.crfPatient?.screening?.CEsclusione?.ce1 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce2 ||
    model?.crfPatient?.screening?.CEsclusione?.ce2 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce3 ||
    model?.crfPatient?.screening?.CEsclusione?.ce3 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce4 ||
    model?.crfPatient?.screening?.CEsclusione?.ce4 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce5 ||
    model?.crfPatient?.screening?.CEsclusione?.ce5 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce6 ||
    model?.crfPatient?.screening?.CEsclusione?.ce6 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce7 ||
    model?.crfPatient?.screening?.CEsclusione?.ce7 == 'siNo.1' ||
    !model?.crfPatient?.screening?.CEsclusione?.ce8 ||
    model?.crfPatient?.screening?.CEsclusione?.ce8 == 'siNo.1'
  ) {
    return 'escluso';
  }
  return 'incluso';
}

export function checkDomiciliation(thisSet, thisIndex) {
  if (!thisSet || !thisIndex || thisIndex < 0) {
    return false;
  }
  return thisSet[thisIndex - 1]?.pazDom == 'siNo.1' ?? true;
}

export function convertDateToData(date) {
  return date.toISOString().slice(0, 10);
}

export function convertDataToDate(data) {
  const date = new Date(data);
  return date;
}

export function todayDate() {
  const date = new Date();
  return convertDateToData(date);
}

export function checkSchedeGiornaliere(model) {
  if (!model?.crfPatient?.screening?.dtScreening) {
    return true;
  }

  let date = new Date(model?.crfPatient?.screening?.dtScreening);
  let err = false;
  let count = 0;

  model?.crfPatient?.giornaliera?.forEach((schedaGiorn) => {
    if (!schedaGiorn.dtGiornaliera) {
      err = true;
      return;
    }
    if (count == 0) {
      date = convertDataToDate(schedaGiorn.dtGiornaliera);
    } else {
      if (schedaGiorn.dtGiornaliera != '' && schedaGiorn.dtGiornaliera > convertDateToData(date)) {
        console.log(`La scheda del ${schedaGiorn.dtGiornaliera} dovrebbe essere del ${convertDateToData(date)}`);
        err = true;
      }
    }
    date.setDate(date.getDate() + 1);
    count++;
  });
  return err == true;
}

export function diagnosiEaIncompatibile(model, thisObject) {
  if (model?.crfPatient?.riOsp?.pazRiOsp == 'siNo.1') {
    const diagnosi = thisObject?.diagnosi;

    if (diagnosi && diagnosi.value != '') {
      const causaRiosp = model?.crfPatient?.riOsp?.CriteriIngresso;
      let incompatFlag = true;

      causaRiosp.forEach((causa) => {
        switch (diagnosi.value) {
          case 'diagnosiSet.1' /* nausea */:
            break;

          case 'diagnosiSet.2' /* vomito */:
            break;

          case 'diagnosiSet.3' /* mucosite */:
            if (['criteriIngressoSet.4', 'criteriIngressoSet.5'].includes(causa)) {
              incompatFlag = false;
            }
            break;

          case 'diagnosiSet.4' /* diarrea */:
            break;

          case 'diagnosiSet.5' /* febbre */:
            if (['criteriIngressoSet.1', 'criteriIngressoSet.3'].includes(causa)) {
              incompatFlag = false;
            }
            break;

          case 'diagnosiSet.6' /* altro */:
            break;

          default:
            break;
        }
      });
      return incompatFlag;
    }
  }
  return false;
}

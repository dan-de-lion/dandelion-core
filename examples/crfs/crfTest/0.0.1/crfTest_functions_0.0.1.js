export function MyFun(model) {
  if (
    !model?.crfTest?.ci1 ||
    model?.crfTest?.ci1 == 'yesNo.0' ||
    !model?.crfTest?.ci2 ||
    model?.crfTest?.ci2 == 'yesNo.0' ||
    !model?.crfTest?.ci3 ||
    model?.crfTest?.ci3 == 'yesNo.0' ||
    !model?.crfTest?.ci4 ||
    model?.crfTest?.ci4 == 'yesNo.0'
  ) {
    return 'NotAllYes';
  } else {
    return 'AllYes';
  }
}

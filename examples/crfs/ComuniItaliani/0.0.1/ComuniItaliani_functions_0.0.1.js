export function test() {
  console.log('function for test');
}

export function calculateBMI(thisObject) {
  const h = thisObject?.Demografica?.altezza;
  return Math.round(((thisObject?.Demografica?.peso * 10000) / (h * h)) * 100.0) / 100.0;
}

export function PazEscluso(model) {
  if (
    model.crfPatient?.preScreening?.CInclusione?.ci1 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci2 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci3 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci4 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci5 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci6 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci7 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci8 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci9 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci10 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CInclusione?.ci11 == 'siNo.0' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce1 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce2 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce3 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce4 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce5 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce6 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce7 == 'siNo.1' ||
    model.crfPatient?.preScreening?.CEsclusione?.ce8 == 'siNo.1'
  ) {
    return 'escluso';
  }
  return 'incluso';
}

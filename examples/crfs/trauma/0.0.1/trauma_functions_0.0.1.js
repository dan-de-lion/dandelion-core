function test() {
  console.log('function for test');
}

export function calculateAppropriatezza(thisObject) {
  if (thisObject.modalitaDiAccesso != 'ModalitaDiAccesso._118IntrodottoDaAREU') {
    return 'Appropriatezza.appropriato';
  }

  return null;
}

export function calculateShockIndex(model) {
  const shockIndex =
    model?.trauma?.Compilazione118?.hrPreospedaliero / model?.trauma?.Compilazione118?.sbpPreospedaliero;
  return isNaN(shockIndex) || !isFinite(shockIndex) ? '-' : Math.round(shockIndex * 100) / 100;
}

export function caseIsCreatable(model) {
  /* no more used */
  const buttons = document.querySelectorAll('.primary');

  if (
    model.trauma?.CasoClinico?.modalitaDiAccesso.toString() === '' ||
    model.trauma?.CasoClinico?.modalitaDiAccesso.toString() === 'ModalitaDiAccesso.autopresentato'
  ) {
    buttons[0].disabled = true;
    return true;
  }
  if (model.trauma?.CasoClinico?.nome && model.trauma?.CasoClinico?.cognome && model.trauma?.CasoClinico?.dataEvento) {
    buttons[0].disabled = false;
    return false;
  }
}

export function isTraumaKnown(model) {
  /*todo check if analysis changed */
  return false;
  if (
    model.trauma?.Compilazione118?.tipoTrauma?.toString() === 'TipoTrauma.sconosciuto' ||
    model.trauma?.Compilazione118?.tipoTrauma?.toString() === ''
  ) {
    return true;
  }
  return false;
}

export function calculateLos(model) {
  const exitDataAsString = model.trauma?.CompilazioneOspedalieraCoreMinimo?.dataDimissione_Decesso;
  const exitData = new Date(exitDataAsString);
  const exitDataInMinutes = Math.floor(exitData.getTime() / 60000);
  let exitTimeSplitted = '';
  let exitTime = 0;
  const exitTimeAsString = model.trauma?.CompilazioneOspedalieraCoreMinimo?.oraDimissione_Decesso;

  if (exitTimeAsString) {
    exitTimeSplitted = exitTimeAsString.split(':');
    exitTime = +exitTimeSplitted[0] * 60 + +exitTimeSplitted[1];
  }

  const totalExitTime = exitDataInMinutes + exitTime;

  const enterDataAsString = model.trauma?.CompilazioneOspedalieraCoreMinimo?.dataArrivoInPS;
  const enterData = new Date(enterDataAsString);
  const enterDataInMinutes = Math.floor(enterData.getTime() / 60000);

  let enterTimeSplitted = '';
  let enterTime = 0;
  const enterTimeAsString = model.trauma?.CompilazioneOspedalieraCoreMinimo?.oraArrivoInPS;

  if (enterTimeAsString) {
    enterTimeSplitted = enterTimeAsString.split(':');
    enterTime = +enterTimeSplitted[0] * 60 + +enterTimeSplitted[1];
  }

  const totalEnterTime = enterDataInMinutes + enterTime;

  if (totalEnterTime > totalExitTime) {
    return '-';
  }

  const losMinutes = totalExitTime - totalEnterTime;

  const losDays = Math.floor(losMinutes / 60 / 24);
  if (isNaN(losDays)) {
    return '-';
  }

  const losMinutesRemaining = losMinutes - losDays * 60 * 24;

  const losHours = Math.round(losMinutesRemaining / 60);

  return isNaN(losHours) || losHours < 0 ? '-' : losDays + ' giorni, ' + losHours + ' ore';
}

/*
export function modalitaDiAccessoIsDisabled(model) {
  setTimeout(() => {
    if (
      model.trauma?.CasoClinico?.modalitaDiAccesso.toString() ===
      "ModalitaDiAccesso._118IntrodottoDaAREU"
    ) {
      let elements = document.querySelectorAll(
        'ul li input[name="model.trauma.CasoClinico.modalitaDiAccesso"]'
      );
      elements.forEach(function (element) {
        element.setAttribute("disabled", true);
      });
      let buttons = document.querySelectorAll(
        'button[name="model.trauma.CasoClinico.modalitaDiAccesso"]'
      );
      buttons.forEach(function (button) {
        button.setAttribute("disabled", true);
      });
    }
  }, 1);
}
*/

/*
export function aerialWaysNotCheckable(model) {
  setTimeout(() => {
    let elements = document.querySelectorAll(
      'ul li input[name="model.trauma.Logistica118.priorita"]'
    );
    if (
      model.trauma?.Compilazione118?.intubazionePreospedaliera?.toString() ===
      "IntubazionePreospedaliera.vieAeree"
    ) {
      elements.forEach(function (element) {
        if (element.id != "trauma.Logistica118.priorita-Priorita.p0") {
          element.setAttribute("disabled", true);
        }
      });
    }
    if (
      model.trauma?.Compilazione118?.intubazionePreospedaliera?.toString() !==
        "IntubazionePreospedaliera.vieAeree" &&
      model.trauma?.Compilazione118?.intubazionePreospedaliera?.toString() !==
        ""
    ) {
      elements.forEach(function (element) {
        element.setAttribute("disabled", null);
      });
    }
  }, 1000);
}
*/

export function calculateAge(model) {
  const previousValue = model.trauma?.CasoClinico?.eta;
  const birthDateAsString = model.trauma?.CasoClinico?.dataNascita;
  if (!birthDateAsString || birthDateAsString === '') {
    return previousValue ?? '-- inserire la data di nascita --';
  }

  const birthDate = new Date(birthDateAsString);
  const today = new Date();
  const oneOrZero =
    today.getMonth() < birthDate.getMonth() ||
    (today.getMonth() === birthDate.getMonth() && today.getDate() < birthDate.getDate());

  const yearDifference = today.getFullYear() - birthDate.getFullYear();
  const dateDifferenceInMs = today.getTime() - birthDate.getTime();

  const dateDifferenceInDays = Math.floor(dateDifferenceInMs / 3600000);
  const dateDifferenceInMonths = Math.floor(dateDifferenceInDays / 730);

  if (yearDifference - oneOrZero < 2) {
    let ageInMonths = dateDifferenceInMonths;
    return ageInMonths + ' mesi';
  } else {
    let age = yearDifference - oneOrZero;
    return age + ' anni';
  }
}

export function calculateGcs(model) {
  const gcsM = model.trauma?.Compilazione118?.gcsMPreospedaliero;
  const gcsV = model.trauma?.Compilazione118?.gcsVPreospedaliero;
  const gcsE = model.trauma?.Compilazione118?.gcsEPreospedaliero;
  const gcsMNonNoto = model.trauma?.Compilazione118?.gcsMPreospedalieroNonNoto;
  const gcsVNonNoto = model.trauma?.Compilazione118?.gcsVPreospedalieroNonNoto;
  const gcsENonNoto = model.trauma?.Compilazione118?.gcsEPreospedalieroNonNoto;

  let gcs = '';

  if (gcsMNonNoto || gcsVNonNoto || gcsENonNoto) {
    gcs = 'GCS non calcolabile';
    return gcs;
  } else if (
    gcsM == '' ||
    gcsV == '' ||
    gcsE == '' ||
    gcsM == null ||
    gcsV == null ||
    gcsE == null ||
    gcsM == undefined ||
    gcsV == undefined ||
    gcsE == undefined
  ) {
    gcs = 'GCS non calcolabile';
    return gcs;
  } else {
    gcs = gcsM + gcsV + gcsE;
    if (isNaN(gcs)) {
      return 'GCS non calcolabile';
    } else {
      return gcs;
    }
  }
}

export function calculateGcsH(model) {
  const gcsMH = model.trauma?.CompilazioneOspedalieraDatiClinici?.gcsMH;
  const gcsVH = model.trauma?.CompilazioneOspedalieraDatiClinici?.gcsVH;
  const gcsEH = model.trauma?.CompilazioneOspedalieraDatiClinici?.gcsEH;
  const gcsMHNonNoto = model.trauma?.CompilazioneOspedalieraDatiClinici?.gcsMHNonNoto;
  const gcsEHNonNoto = model.trauma?.CompilazioneOspedalieraDatiClinici?.gcsEHNonNoto;
  const gcsVHNonNoto = model.trauma?.CompilazioneOspedalieraDatiClinici?.gcsVHNonNoto;
  let gcs = '';
  if (gcsMHNonNoto || gcsEHNonNoto || gcsVHNonNoto) {
    gcs = 'GCS non calcolabile';
    return gcs;
  } else if (
    gcsMH == '' ||
    gcsVH == '' ||
    gcsEH == '' ||
    gcsMH == null ||
    gcsVH == null ||
    gcsEH == null ||
    gcsMH == undefined ||
    gcsVH == undefined ||
    gcsEH == undefined
  ) {
    gcs = 'GCS non calcolabile';
    return gcs;
  } else {
    gcs = gcsMH + gcsVH + gcsEH;
    if (isNaN(gcs)) {
      return 'GCS non calcolabile';
    } else {
      return gcs;
    }
  }
}

export function esitoCases(model) {
  if (
    model.trauma?.CompilazioneOspedalieraCoreMinimo?.esitoDiProntoSoccorso == 'EsitoProntoSoccorso.trasferitoAdAltroH'
  ) {
    return 'Esito.vivo';
  }

  if (model.trauma?.CompilazioneOspedalieraCoreMinimo?.esitoDiProntoSoccorso == 'EsitoProntoSoccorso.decedutoInPS') {
    return 'Esito.deceduto';
  }

  return null;
}

export function esitoPSCases(model) {
  if (
    [
      'ModalitaDiAccesso.autopresentato',
      'ModalitaDiAccesso.trasferitoDaUnAltroOspedale',
      'ModalitaDiAccesso._118IntrodottoDaOspedale',
    ].includes(model.trauma?.CasoClinico?.modalitaDiAccesso)
  ) {
    return 'EsitoProntoSoccorso.ricoverato';
  }
  return null;
}

export function creatingScheda() {
  /* REMOVED FORM CRF: this should be a good form of "init", but core not supporting it rn */

  let schedaFirstTwoDigits = '';
  const currentYear = new Date().getFullYear().toString().substring(2);
  schedaFirstTwoDigits = currentYear;
  return schedaFirstTwoDigits;
}

export function thirdNumberOnSchedaMissioneIsNotSoreu(model) {
  const schedaMissioneToBeChecked = model.trauma?.Logistica118?.schedaMissione118;

  if (
    !schedaMissioneToBeChecked ||
    !schedaMissioneToBeChecked.length ||
    schedaMissioneToBeChecked.length < 3 ||
    schedaMissioneToBeChecked[2] === '1' ||
    schedaMissioneToBeChecked[2] === '3' ||
    schedaMissioneToBeChecked[2] === '5' ||
    schedaMissioneToBeChecked[2] === '7'
  ) {
    return false;
  }
  return true;
}

export function thirdNumberOnSchedaPazienteIsNotSoreu(model) {
  const schedaPazienteToBeChecked = model.trauma?.Logistica118?.schedaPaziente118;

  if (
    !schedaPazienteToBeChecked ||
    !schedaPazienteToBeChecked.length ||
    schedaPazienteToBeChecked.length < 3 ||
    schedaPazienteToBeChecked[2] === '1' ||
    schedaPazienteToBeChecked[2] === '3' ||
    schedaPazienteToBeChecked[2] === '5' ||
    schedaPazienteToBeChecked[2] === '7'
  ) {
    return false;
  }
  return true;
}

export function checkSoreuOnSchedasCodes(model) {
  /* REACTIVATE?
  const schedaMissioneToBeChecked = model.trauma?.Logistica118?.schedaMissione118;
  const schedaPazienteToBeChecked = model.trauma?.Logistica118?.schedaPaziente118;

  if (schedaPazienteToBeChecked[2] === schedaMissioneToBeChecked[2] && schedaPazienteToBeChecked[2] !== undefined && schedaMissioneToBeChecked[2] !== undefined) {
    return false;
  }
  return true;
  */
  return false;
}

export function checkSoreuFromSchedas(model) {
  const checkIfSoreusAreSame = checkSoreuOnSchedasCodes(model);

  if (checkIfSoreusAreSame === true) {
    /* Why? */
    return null;
  }

  if (
    !model.trauma?.Logistica118?.schedaMissione118 ||
    !model.trauma?.Logistica118?.schedaMissione118.length ||
    model.trauma?.Logistica118?.schedaMissione118.length < 3
  ) {
    return null;
  }

  let soreuNumber = model.trauma?.Logistica118?.schedaMissione118[2];

  if (soreuNumber === '1') {
    return 'Soreu.soreuAlpina';
  } else if (soreuNumber === '3') {
    return 'Soreu.soreuLaghi';
  } else if (soreuNumber === '5') {
    return 'Soreu.soreuMetropolitana';
  } else if (soreuNumber === '7') {
    return 'Soreu.soreuPianura';
  } else {
    return null;
  }
}

export function getHospitalFromUser(container, model) {
  if (
    model.trauma?.CompilazioneOspedalieraCoreMinimo?.ospedale &&
    model.trauma?.CompilazioneOspedalieraCoreMinimo?.ospedale != ''
  ) {
    return model.trauma?.CompilazioneOspedalieraCoreMinimo?.ospedale;
  }
  if (container.get('UserService').getCurrentRoles()[0] === 'AreuSupervisor') {
    return null;
  }
  if (container.get('UserService').getCurrentRoles()[0] === 'Areu') {
    return model.trauma?.CompilazioneOspedalieraCoreMinimo?.ospedale;
  }
  if (container.get('UserService').getCurrentRoles()[0] === 'CT') {
    return model.trauma?.CompilazioneOspedalieraCoreMinimo?.ospedale;
  }
  if (container.get('UserService').getCurrentRoles()[0] === 'Clinician') {
    return 'Ospedale.' + container.get('UserService').getCurrentCentreCode();
  }
}

export function checkDuplicatedSP118(container, model) {
  try {
    if (!model.trauma?.Logistica118?.schedaPaziente118) {
      return false;
    }
    const currentCase = container.get('CaseService').currentCase;
    return container
      .get('ListService')
      .caseList.some(
        (e) =>
          e.cache.get('trauma.Logistica118.schedaPaziente118') == model.trauma?.Logistica118?.schedaPaziente118 &&
          currentCase != e.caseMetaData?.caseID
      );
  } catch {
    return false;
  }
}

export function checkDuplicatedSDO(container, model) {
  try {
    if (!model.trauma?.CompilazioneOspedalieraCoreMinimo?.sdoOspedale) {
      return false;
    }
    const currentCase =
      container.get('CurrentCaseService').currentCase; /*TODO: expose method, change to currentCaseService when 4.0 */
    const listOfCases = container.get('ListService').caseList; /*TODO: expose method for entire caseList, not filtered*/
    return listOfCases.some(
      (e) =>
        e.cache.get('trauma.CompilazioneOspedalieraCoreMinimo.sdoOspedale') ==
          model.trauma?.CompilazioneOspedalieraCoreMinimo?.sdoOspedale && currentCase != e.caseMetaData?.caseID
    );
  } catch {
    return false;
  }
}

export function getISODateFromItalianDateTime(italianDateTime) {
  if (!italianDateTime) {
    return null;
  }

  const [datePart, _timePart] = italianDateTime.split(' ');
  if (!datePart) {
    return null;
  }

  const [date, month, year] = datePart.split('/');

  if (
    !date ||
    !month ||
    !year ||
    !date.length ||
    !month.length ||
    !year.length ||
    date.length > 2 ||
    month.length > 2 ||
    year.length != 4
  ) {
    return null;
  }

  return `${year}-${month.length > 1 ? month : '0' + month}-${date.length > 1 ? date : '0' + date}`;
}

export function getISOTimeFromItalianDateTime(italianDateTime) {
  if (!italianDateTime) {
    return null;
  }

  const [_datePart, timePart] = italianDateTime.split(' ');
  if (!timePart || !timePart.length || timePart.length != 5) {
    return null;
  }

  return timePart;
}

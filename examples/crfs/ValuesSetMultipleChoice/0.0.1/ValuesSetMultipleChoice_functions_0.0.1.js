export function getNumericalValueOfFirst(model) {
  const values = model.ValuesSetMultipleChoice?.test;
  return values && values.length > 0 ? values[0].getNumericalValue() : -1;
}

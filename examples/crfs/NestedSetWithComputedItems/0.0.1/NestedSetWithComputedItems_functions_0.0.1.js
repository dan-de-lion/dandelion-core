export function isProceduraWithDataFine(thisObject) {
  console.log(thisObject.procedure.getLabel());
  if (
    thisObject.procedure.getLabel() != 'Vacuum Therapy (Terapia del vuoto)' &&
    thisObject.procedure.getLabel() != 'Defibrillazione' &&
    thisObject.procedure.getLabel() != 'Rianimazione cardio-polmonare (CPR)' &&
    thisObject.procedure.getLabel() != 'Pressione intraaddominale' &&
    thisObject.procedure.getLabel() != 'Tecniche di clearance epatica' &&
    thisObject.procedure.getLabel() != 'Trasfusione di sangue massiva' &&
    thisObject.procedure.getLabel() != 'SDD (Topica, Topica e Sistemica)' &&
    thisObject.procedure.getLabel() != 'Contenzione del paziente' &&
    thisObject.procedure.getLabel() != 'Cardioversione elettrica'
  ) {
    return true;
  } else {
    return false;
  }
}

export function isProceduraWithDataInizio(thisObject) {
  /* if (thisObject.procedure.getLabel() != 'Vacuum Therapy (Terapia del vuoto)'
     && thisObject.procedure.getLabel() != 'Pressione intraaddominale'
     && thisObject.procedure.getLabel() != 'Tecniche di clearance epatica'
     && thisObject.procedure.getLabel() != 'SDD (Topica, Topica e Sistemica)'
     && thisObject.procedure.getLabel() != 'Contenzione del paziente') {
     return true
   }
   else {
     return false
   }*/
  return true;
}

export function isProceduraWithPresenteInIngresso(thisObject) {
  if (
    thisObject.procedure.getLabel() != 'Vacuum Therapy (Terapia del vuoto)' &&
    thisObject.procedure.getLabel() != 'Defibrillazione' &&
    thisObject.procedure.getLabel() != 'Rianimazione cardio-polmonare (CPR)' &&
    thisObject.procedure.getLabel() != 'Pressione intraaddominale' &&
    thisObject.procedure.getLabel() != 'Tecniche di clearance epatica' &&
    thisObject.procedure.getLabel() != 'Trasfusione di sangue massiva' &&
    thisObject.procedure.getLabel() != 'SDD (Topica, Topica e Sistemica)' &&
    thisObject.procedure.getLabel() != 'Contenzione del paziente' &&
    thisObject.procedure.getLabel() != 'Cardioversione elettrica'
  ) {
    return true;
  } else {
    return false;
  }
}

export function isProceduraWithDimissioneConpresidioOTrattamentoPresente(thisObject) {
  if (
    thisObject.procedure.getLabel() != 'Vacuum Therapy (Terapia del vuoto)' &&
    thisObject.procedure.getLabel() != 'Defibrillazione' &&
    thisObject.procedure.getLabel() != 'Rianimazione cardio-polmonare (CPR)' &&
    thisObject.procedure.getLabel() != 'Pressione intraaddominale' &&
    thisObject.procedure.getLabel() != 'Tecniche di clearance epatica' &&
    thisObject.procedure.getLabel() != 'Trasfusione di sangue massiva' &&
    thisObject.procedure.getLabel() != 'SDD (Topica, Topica e Sistemica)' &&
    thisObject.procedure.getLabel() != 'Contenzione del paziente' &&
    thisObject.procedure.getLabel() != 'Cardioversione elettrica'
  ) {
    return true;
  } else {
    return false;
  }
}

export function isProceduraTracheostomia(thisObject) {
  if (thisObject.procedure.getLabel() == 'Tracheostomia') return true;
}

export function isProceduraMonitoraggioInvasivoGittata(thisObject) {
  if (thisObject.procedure.getLabel() == 'Monitoraggio invasivo gittata') return true;
}

export function isProceduraSDD(thisObject) {
  if (thisObject.procedure.getLabel() == 'SDD (Topica, Topica e Sistemica)') return true;
}

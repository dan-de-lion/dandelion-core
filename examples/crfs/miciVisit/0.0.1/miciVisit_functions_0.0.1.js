/*
  Calcolo dell'indice di Harvey-Bradshaw (HBI)

  Per definizione, vedi
    https://www.igibdscores.it/it/info-hbi.html
*/
export function calcolaValoreHBI(model) {
  let hbi = 0;

  if (model.miciVisit?.recente?.benessereGenerale == 'livelloBenessere.bene') hbi += 1;
  if (model.miciVisit?.recente?.benessereGenerale == 'livelloBenessere.male') hbi += 2;
  if (model.miciVisit?.recente?.benessereGenerale == 'livelloBenessere.moltoMale') hbi += 3;
  if (model.miciVisit?.recente?.benessereGenerale == 'livelloBenessere.terribile') hbi += 4;

  if (model.miciVisit?.recente?.doloreAddominale == 'livelloDoloreAddominale.lieve') hbi += 1;
  if (model.miciVisit?.recente?.doloreAddominale == 'livelloDoloreAddominale.moderato') hbi += 2;
  if (model.miciVisit?.recente?.doloreAddominale == 'livelloDoloreAddominale.grave') hbi += 3;

  if (model.miciVisit?.recente?.massaAddominale == 'tipoMassaAddominale.dubbia') hbi += 1;
  if (model.miciVisit?.recente?.massaAddominale == 'tipoMassaAddominale.definita') hbi += 2;
  if (model.miciVisit?.recente?.massaAddominale == 'tipoMassaAddominale.definitaPalpabile') hbi += 3;

  if (['tipoFeci.liquide', 'tipoFeci.morbide'].includes(model.miciVisit?.recente?.caratteristicaFeci)) {
    hbi += model?.miciVisit?.recente?.volteInBagnoHB;
  }

  if (model.miciVisit?.recente?.manifestazioniExtraintestinali?.toString().includes('manifestazioniExtra.artralgia'))
    hbi++;
  if (model.miciVisit?.recente?.manifestazioniExtraintestinali?.toString().includes('manifestazioniExtra.uveite'))
    hbi++;
  if (model.miciVisit?.recente?.manifestazioniExtraintestinali?.toString().includes('manifestazioniExtra.eritema'))
    hbi++;
  if (model.miciVisit?.recente?.manifestazioniExtraintestinali?.toString().includes('manifestazioniExtra.ulcera'))
    hbi++;
  if (model.miciVisit?.recente?.manifestazioniExtraintestinali?.toString().includes('manifestazioniExtra.pioderma'))
    hbi++;

  if (model.miciVisit?.recente?.versantePerianale?.toString().includes('versantePeri.ragadi')) hbi++;
  if (model.miciVisit?.recente?.versantePerianale?.toString().includes('versantePeri.fistola')) hbi++;
  if (model.miciVisit?.recente?.versantePerianale?.toString().includes('versantePeri.ascesso')) hbi++;

  return hbi;
}

export function calcolaValorePCDAI(model) {
  const valorePCDAI =
    (model.miciVisit?.recente?.valutazionePcdai?.secrezioni
      ? model.miciVisit?.recente?.valutazionePcdai?.secrezioni?.getNumericalValue()
      : NaN) +
    (model.miciVisit?.recente?.valutazionePcdai?.limitazioneAttivita
      ? model.miciVisit?.recente?.valutazionePcdai?.limitazioneAttivita?.getNumericalValue()
      : NaN) +
    (model.miciVisit?.recente?.valutazionePcdai?.limitazioniAttivitaSex
      ? model.miciVisit?.recente?.valutazionePcdai?.limitazioniAttivitaSex?.getNumericalValue()
      : NaN) +
    (model.miciVisit?.recente?.valutazionePcdai?.malattiaPerianale
      ? model.miciVisit?.recente?.valutazionePcdai?.malattiaPerianale?.getNumericalValue()
      : NaN) +
    (model.miciVisit?.recente?.valutazionePcdai?.gradoIndurimento
      ? model.miciVisit?.recente?.valutazionePcdai?.gradoIndurimento?.getNumericalValue()
      : NaN);
  return isNaN(valorePCDAI) ? '(selezionare tutti i valori)' : valorePCDAI;
}

export function calcolaValutazioneHBI(model, hbi) {
  var valutazione;

  if (hbi < 5) valutazione = 'in remissione';
  else if (hbi <= 7) valutazione = 'attività lieve';
  else if (hbi <= 16) valutazione = 'attività moderata';
  else valutazione = 'attività grave';

  return hbi + ': ' + valutazione;
}

/*
  Calcolo del 'Mayo Score parziale'

  Per definizione, vedi
    https://www.igibdscores.it/it/info-mayo-partial.html
*/
export function calcolaValoreMayo(model) {
  const norma = model?.miciVisit?.anamnGen?.evaquazioni24h ?? 1;
  let mayo = 0;

  if (model?.miciVisit?.recente?.volteInBagnoMayo <= norma) mayo += 0;
  else if (model?.miciVisit?.recente?.volteInBagnoMayo <= norma + 2) mayo += 1;
  else if (model?.miciVisit?.recente?.volteInBagnoMayo <= norma + 4) mayo += 2;
  else mayo += 3;

  /*
                 Quantità di sangue:
                        Solo sangue ------------+
                       Ben evidente ---------+  |
                          In tracce ------+  |  |
                                          |  |  |
       Quanto spesso vede il sangue:      v  v  v
                                Mai --->  0  0  0
              In alcune evacuazioni --->  1  1' 3
          In metà delle evacuazioni --->  1' 2  3
            In tutte le evacuazioni --->  1' 2  3

        NOTA:  le combinazione corrispondenti ai valori contrassegnati con un apice
               non sono sono previste dalla definizione ufficiale del Mayo Score.
               I valori impostati sono stati decisi dai prof. Gionchetti e Rizzello.
    */

  if (model.miciVisit?.recente?.frequenzaSangueNelleFeci != 'frequenzaSangue.mai') {
    if (model.miciVisit?.recente?.quantitaSangueNelleFeci == 'quantitaSangue.inTracce') mayo += 1;
    else {
      if (model.miciVisit?.recente?.quantitaSangueNelleFeci == 'quantitaSangue.benEvidente') {
        if (model.miciVisit?.recente?.frequenzaSangueNelleFeci == 'frequenzaSangue.inAlcune') mayo += 1;
        else mayo += 2;
      } else {
        if (model.miciVisit?.recente?.quantitaSangueNelleFeci == 'quantitaSangue.soloSangue') mayo += 3;
      }
    }
  }

  if (model.miciVisit?.recente?.giudizioDelMedico == 'giudizioMedico.patologiaLieve') mayo += 1;
  if (model.miciVisit?.recente?.giudizioDelMedico == 'giudizioMedico.patologiaModerata') mayo += 2;
  if (model.miciVisit?.recente?.giudizioDelMedico == 'giudizioMedico.patologiaGrave') mayo += 3;

  return mayo;
}

export function calcolaValutazioneMayo(model, mayo) {
  var valutazione;

  if (mayo < 2) valutazione = 'in remissione';
  else if (mayo <= 4) valutazione = 'attività lieve';
  else if (mayo <= 7) valutazione = 'attività moderata';
  else valutazione = 'attività grave';

  return mayo + ': ' + valutazione;
}

export function isActive(container, aliveOrDead, dimissionDate) {
  let reminderDate = null;
  let reminderDefinition = { reminderId: '3', title: '1', text: '1', daysBeforeExpiration: 9 };
  if (dimissionDate) {
    const parsedDate = new Date(Date.parse(dimissionDate));
    reminderDate = new Date(parsedDate.setDate(parsedDate.getDate() + 30));
  }

  if (aliveOrDead == 'VivoMorto.vivo') {
    container.get('ReminderService').addReminderToQueue(reminderDefinition, reminderDate);
    return true;
  }
  if (aliveOrDead == 'VivoMorto.morto') {
    container.get('ReminderService').removeReminder('3');
    return false;
  }
  return false;
}

export function calculateBMI(model) {
  const p = model.miciVisit?.recente?.peso;
  const h = model.miciVisit?.recente?.altezza;
  console.log('Peso ', p, '   Altezza ', h);
  return Math.round(((p * 10000) / (h * h)) * 100.0) / 100.0;
}

export function initSetFistole(model) {
  const num = model.miciVisit?.recente?.numFistole;
  const set = [];
  for (let i = 0; i < num; i++) {
    set.push(i + 1);
  }
  return set;
}

/*
export function campoJson(filepath, keyName, keyVal, field) {
  const fileContent = fetch(filepath);
  const data = JSON.parse(fileContent);
  const recordTrovato = data.find((record) => record[keyName] === keyVal);

  if (recordTrovato) {
    return recordTrovato[field];
  } else {
    return null;
  }
  return "12.34";
}
*/

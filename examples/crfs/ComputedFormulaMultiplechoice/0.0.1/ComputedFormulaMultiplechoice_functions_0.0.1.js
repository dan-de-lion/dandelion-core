export function computedFormulaTest(model) {
  if (!model.ComputedFormulaMultiplechoice?.riscontro) {
    return null;
  } else if (model.ComputedFormulaMultiplechoice?.riscontro[0] == 'Appropriatezza.appropriato') {
    return ['ModalitaDiAccesso.autopresentato'];
  }
  return null;
}

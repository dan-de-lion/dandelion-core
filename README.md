# Dandelion Core

## Installation

For using Dandelion Core you need a Vue project.

1. Run `npm install @dandelion/core`.
2. You can import the CRFRenderComponent (the core) with <br /> `import { CRFRenderComponent } from '@dandelion/core'`.
3. You can install the Plugin globally with <br /> `import [ dandelionCore ] from '@dandelion/core'`

## AVVIO DEL DOCKER

$mountedPath = (Convert-Path $(pwd)).Replace("\", '/').Replace('C:/', '//c/').Replace('D:/', '//d/')
$targets = @{

    "init"       = {
        ##COSTRUISCE L'IMMAGINE
        docker build -t dandelion-core -f Dockerfile.win . 
    }

    "up"         = {
        ## RUN DEL SERVER
        docker run -it --rm --name dandelion-core -p 5173:5173 -p 4173:4173 -v "${mountedPath}:/app" -w /app dandelion-core npm run dev
    }

    "npminstall" = {
        ##AVVIO DEL CONTAINER E LANCIO DI NPM INSTALL
        docker run -it --rm --name dandelion-core -p 5173:5173 -v "${mountedPath}:/app" -w /app dandelion-core npm install
    }

    "down"       = {
        ## STOP DEL SERVER
        docker stop dandelion-core
    }

    "shell"      = {
        ## APRE UNA SHELL ALL'INTERNO DEL CONTAINER
        docker exec -it dandelion-core sh
    }

    "testvi"     = {
        ## TEST VITES
        docker exec -it dandelion-core npx vitest run $(TEST)
    }
}

## VERIFICA SE IL COMANDO ESISTE E LO LANCIA
if ($targets.ContainsKey($args[0])) {
    & $targets[$args[0]]
}
else {
    Write-Host "Comando non valido."
}
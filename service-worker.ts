// import { getContainer } from './inversify.config';
// import type { CoreConfigurationInterface } from './src/interfaces/CoreConfiguration';
// import type { CRFServiceInterface } from './src/interfaces/CRFServiceInterface';
// import { NamedBindings } from './src/types/NamedBindings';
// import { Contexts } from './src/types/Contexts';
//
// //@ts-expect-error
// self.addEventListener('fetch', (event: FetchEvent) => {
//   const coreConfiguration: CoreConfigurationInterface = getContainer(Contexts.DEFAULT).get(
//     NamedBindings.CoreConfiguration
//   );
//   const crfService: CRFServiceInterface = getContainer(Contexts.DEFAULT).get(
//     NamedBindings.CRFService
//   );
//
//   const requestURL = new URL(event.request.url);
//
//   if (
//     (requestURL.pathname.endsWith('.xsl') &&
//       requestURL.pathname.includes(coreConfiguration.baseURLForOverrides ?? 'xslt-overrides')) ||
//     crfService.isCRFPath(requestURL.pathname)
//   ) {
//     event.respondWith(
//       fetch(event.request)
//         .then((response) => {
//           if (response.status === 404) {
//             return new Response(null, { status: 202 });
//           }
//           return response;
//         })
//         .catch(() => {
//           return new Response(null, { status: 202 });
//         })
//     );
//   }
// });

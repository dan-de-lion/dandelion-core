FROM node:16-alpine
WORKDIR '/app'

## Make user match with host user
## (from BEST PRACTICES: https://github.com/nodejs/docker-node/blob/main/docs/BestPractices.md#non-root-user)
ARG GROUP_ID
ARG USER_ID

RUN deluser --remove-home node \
  && addgroup -S node -g $GROUP_ID \
  && adduser -S -G node -u $USER_ID node

USER node

EXPOSE 5173
EXPOSE 4173

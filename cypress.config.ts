import { defineConfig } from 'cypress';
import { VueLoaderPlugin } from 'vue-loader';
import webpack from '@cypress/webpack-preprocessor';

const webpackOptions = {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        options: { appendTsSuffixTo: [/\.vue$/] },
      },
    ],
  },
  plugins: [new VueLoaderPlugin()],
  resolve: {
    extensions: ['.ts', '.js', '.json', '.vue'],
  },
};

const options = {
  // send in the options from your webpack.config.js, so it works the same
  // as your app's code
  webpackOptions,
  watchOptions: {},
};

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:5173/',
    chromeWebSecurity: false,
    video: false,
    screenshotOnRunFailure: false,

    specPattern: 'test/cypress/e2e/*.cy.{js,jsx,ts,tsx}',
    fixturesFolder: 'test/cypress/fixtures',
    screenshotsFolder: 'test/cypress/screenshots',
    videosFolder: 'test/cypress/videos',
    downloadsFolder: 'test/cypress/downloads',
    supportFile: 'test/cypress/support/e2e.ts',

    setupNodeEvents(on, config) {
      // implement node event listeners here
      on('file:preprocessor', webpack(options));
    },
  },
});

import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { readonly, type Ref, ref } from 'vue';
import { DandelionService } from './DandelionService';
import { ContainerTypes } from '@/ContainersConfig';
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type CurrentContextService from '@/services/CurrentContextService';
import { NamedBindings } from '@/types/NamedBindings';
import { useDebounceFn } from '@vueuse/core';
import { UserServiceInterface } from '@/interfaces/UserServiceInterface';

export type CrfStatus = Map<string, number>;

@injectable()
export default class CaseStatusService extends DandelionService implements CaseStatusServiceInterface {
  protected listOfVariablesByAccessLevelAndStatus: Ref<Map<string, Map<number, Set<string>>>> = ref(new Map());
  protected listOfStatusRequiredByAccessLevelAndVariable: Map<string, Map<string, number>> = new Map();
  protected isActive = false;
  protected isMounting = false;
  protected status = ref(0);
  protected errorCounter = ref(0);
  protected warningCounter = ref(0);
  protected confirmedWarningCounter = ref(0);
  public requirementCounter = ref(0);
  protected errorSet: Set<string> = new Set();
  protected warningSet: Set<string> = new Set();
  protected confirmedWarningSet: Set<string> = new Set();
  protected requirementSet = new Set();
  protected statusMap: Ref<Map<string, CrfStatus>> = ref(new Map());
  protected highestPossibleStatusMap: Map<string, CrfStatus> = new Map();
  protected debouncedUpdateStatus;

  constructor(
    @inject(NamedBindings.CurrentContextService)
    protected currentContext: CurrentContextService,
    @inject(NamedBindings.CoreConfiguration)
    protected coreConfiguration: CoreConfigurationInterface,
    @inject(NamedBindings.UserService)
    protected userService: UserServiceInterface
  ) {
    super();
    this.debouncedUpdateStatus = useDebounceFn(() => {
      this.updateStatusFn();
    }, this.coreConfiguration.debounceForUpdatingStatus);
  }

  public updateStatusFn() {
    this.statusMap.value.forEach((crfStatus, accessLevel) => {
      crfStatus.forEach((_status, crfName, map2) => {
        /* if crfName is *, you have to select all the variables, so the prefix is empty string */
        const prefix = crfName === '*' ? '' : crfName;
        map2.set(crfName, this.calculateStatus(prefix, accessLevel));
      });
    });
    /* the default status have to take all the variables (empty prefix) and all the accessLevels (*) */
    this.status.value = this.calculateStatus('', '*');
  }

  public updateStatus() {
    this.debouncedUpdateStatus();
  }

  public finishedMounting() {
    this.isMounting = false;
    this.updateStatus();
  }

  /** Calculates the status based on the given prefix and access level.
   * @param {string} prefix - The prefix to filter variables.
   * @param {string} accessLevel - The access level to consider.
   * @returns {number} - The calculated status, or -1 if the highest possible status could not be determined.
   */
  public calculateStatus(prefix: string, accessLevel: string): number {
    const listOfVariablesByStatus = new Map<number, Set<string>>();
    if (accessLevel === '*') {
      this.listOfVariablesByAccessLevelAndStatus.value.forEach((variablesByStatus) => {
        variablesByStatus.forEach((variables, status) => {
          if (!listOfVariablesByStatus.has(status)) {
            listOfVariablesByStatus.set(status, new Set());
          }
          variables.forEach((variable) => {
            if (variable.startsWith(prefix)) {
              listOfVariablesByStatus.get(status)?.add(variable);
            }
          });
        });
      });
    } else {
      const accessLevelVariables = this.listOfVariablesByAccessLevelAndStatus.value.get(accessLevel);
      if (accessLevelVariables) {
        accessLevelVariables.forEach((variables, status) => {
          listOfVariablesByStatus.set(status, new Set(variables));
        });
      }
    }
    const statuses = [...listOfVariablesByStatus.keys()].sort((a, b) => (a > b ? 1 : -1));
    // foreach status in order check if there are missing variables
    for (const [index, status] of statuses.entries()) {
      const filteredVariables = Array.from(listOfVariablesByStatus.get(status)!.keys()).filter((variable) =>
        variable.startsWith(prefix)
      );
      if (filteredVariables.length > 0) {
        // return the previous status in the list or -1 if there is no previous status
        return statuses[index - 1] ?? 0;
      }
    }
    if (statuses.length === 0) {
      return 0;
    }
    let highestPossibleStatus = 0;
    if (accessLevel === '*') {
      this.highestPossibleStatusMap.forEach((crfStatus) => {
        crfStatus.forEach((status, crfName) => {
          if (crfName.startsWith(prefix) && status > highestPossibleStatus) {
            highestPossibleStatus = status;
          }
        });
      });
    } else {
      highestPossibleStatus = this.highestPossibleStatusMap.get(accessLevel)?.get(prefix === '' ? '*' : prefix) ?? 0;
    }
    if (!highestPossibleStatus && this.currentContext.containerType !== ContainerTypes.NEW_CASE) {
      console.error(`Highest possible status could not be determined for ${prefix || "''"}, returning -1`);
      return -1;
    }
    return highestPossibleStatus;
  }

  startStatusService(availableCrfs: string[]) {
    this.reset();
    this.isActive = true;
    this.isMounting = true;
    const availableConfigurations = this.coreConfiguration.accessLevelAndCrfConfigurations!.filter(
      ({ accessLevel, crfName }) =>
        (crfName === '*' || availableCrfs.includes(crfName)) &&
        (accessLevel === '*' || this.userService.hasReadAccessRights(accessLevel))
    );
    availableConfigurations.forEach(({ accessLevel, crfName }) => {
      const crfMap = this.statusMap.value.get(accessLevel) ?? new Map();
      crfMap.set(crfName, 0);
      this.statusMap.value.set(accessLevel, crfMap);
      const highestPossibleStatusMapForAccessLevel = this.highestPossibleStatusMap.get(accessLevel) ?? new Map();
      highestPossibleStatusMapForAccessLevel.set(crfName, 0);
      highestPossibleStatusMapForAccessLevel.set('*', 0);
      this.highestPossibleStatusMap.set(accessLevel, highestPossibleStatusMapForAccessLevel);
    });
  }

  public getStatusMap() {
    return this.statusMap;
  }

  registerVariable(variableName: string, accessLevel: string, statusLevel: number) {
    const listOfVariablesByStatus = this.listOfVariablesByAccessLevelAndStatus.value.get(accessLevel) ?? new Map();
    const listOfStatusByVariable = this.listOfStatusRequiredByAccessLevelAndVariable.get(accessLevel) ?? new Map();
    for (let k = statusLevel; k > 0; k--) {
      listOfVariablesByStatus.set(k, listOfVariablesByStatus.get(k) ?? new Set());
      listOfStatusByVariable.set(variableName, listOfStatusByVariable.get(variableName) ?? k);
    }
    listOfVariablesByStatus.get(statusLevel)!.add(variableName);
    this.listOfVariablesByAccessLevelAndStatus.value.set(accessLevel, listOfVariablesByStatus);
    this.listOfStatusRequiredByAccessLevelAndVariable.set(accessLevel, listOfStatusByVariable);
    const crfName = variableName.split('.')[0];
    // Check if we should update highest possible status for this accessLevl and both this and generic crfName
    const highestPossibleStatusMapForAccessLevel = this.highestPossibleStatusMap.get(accessLevel) ?? new Map();
    if ((highestPossibleStatusMapForAccessLevel.get(crfName) ?? 0) < statusLevel) {
      highestPossibleStatusMapForAccessLevel.set(crfName, statusLevel);
    }
    if ((highestPossibleStatusMapForAccessLevel.get('*') ?? 0) < statusLevel) {
      highestPossibleStatusMapForAccessLevel.set('*', statusLevel);
    }
    this.highestPossibleStatusMap.set(accessLevel, highestPossibleStatusMapForAccessLevel);
    // Do the same check also for '*' accessLevel
    const highestPossibleStatusMapForAllAccessLevels = this.highestPossibleStatusMap.get('*') ?? new Map();
    if ((highestPossibleStatusMapForAllAccessLevels.get(crfName) ?? 0) < statusLevel) {
      highestPossibleStatusMapForAllAccessLevels.set(crfName, statusLevel);
    }
    if ((highestPossibleStatusMapForAllAccessLevels.get('*') ?? 0) < statusLevel) {
      highestPossibleStatusMapForAllAccessLevels.set('*', statusLevel);
    }
    this.highestPossibleStatusMap.set('*', highestPossibleStatusMapForAllAccessLevels);
    // Prevent to continuously updating the status when the component is mounting
    if (!this.isMounting) {
      this.updateStatus();
    }
  }

  isVariableMissing(value: any): boolean {
    if (value === 0) {
      return false;
    }
    if (!value || value instanceof Error) {
      return true;
    }
    return typeof value.length !== 'undefined' && value.length === 0;
  }

  updateValue(variableName: string, value: any, accessLevel: string) {
    if (!this.isActive) {
      return;
    }
    const requiredForStatusPresent = this.listOfStatusRequiredByAccessLevelAndVariable
      .get(accessLevel)
      ?.get(variableName);
    if (!requiredForStatusPresent) {
      return;
    }
    const variablesByStatus = this.listOfVariablesByAccessLevelAndStatus.value
      .get(accessLevel)
      ?.get(requiredForStatusPresent);
    const isMissing = this.isVariableMissing(value);
    const hasVariable = variablesByStatus?.has(variableName);
    if (isMissing && !hasVariable) {
      variablesByStatus?.add(variableName);
      this.updateStatus();
    } else if (!isMissing && hasVariable) {
      variablesByStatus!.delete(variableName);
      this.updateStatus();
    }
  }

  setHiddenVisibility(variable: string, accessLevel: string) {
    if (!this.isActive) {
      return;
    }
    const requiredForStatusPresent = this.listOfStatusRequiredByAccessLevelAndVariable.get(accessLevel)?.get(variable);
    if (!requiredForStatusPresent) {
      return;
    }
    this.listOfVariablesByAccessLevelAndStatus.value.get(accessLevel)?.get(requiredForStatusPresent)?.delete(variable);
    this.updateStatus();
  }

  getStatus() {
    return readonly(this.status);
  }

  getListOfVariablesByAccessLevelAndStatus(): Readonly<
    Ref<ReadonlyMap<string, ReadonlyMap<number, ReadonlySet<string>>>>
  > {
    return readonly(this.listOfVariablesByAccessLevelAndStatus);
  }

  public addError(variable: string) {
    this.errorSet.add(variable);
    this.errorCounter.value = this.errorSet.size;
  }

  public removeError(variable: string) {
    this.errorSet.delete(variable);
    this.errorCounter.value = this.errorSet.size;
  }

  public getErrorCounter() {
    return readonly(this.errorCounter);
  }

  public addWarning(variable: string) {
    this.warningSet.add(variable);
    this.warningCounter.value = this.warningSet.size;
  }

  public removeWarning(variable: string) {
    this.warningSet.delete(variable);
    this.warningCounter.value = this.warningSet.size;
  }

  public getWarningCounter() {
    return readonly(this.warningCounter);
  }

  public addConfirmedWarning(variable: string) {
    this.confirmedWarningSet.add(variable);
    this.confirmedWarningCounter.value = this.confirmedWarningSet.size;
  }

  public removeConfirmedWarning(variable: string) {
    this.confirmedWarningSet.delete(variable);
    this.confirmedWarningCounter.value = this.confirmedWarningSet.size;
  }

  public getConfirmedWarningCounter() {
    return readonly(this.confirmedWarningCounter);
  }

  public addRequirement(variable: string) {
    this.requirementSet.add(variable);
    this.requirementCounter.value = this.requirementSet.size;
  }

  public removeRequirement(variable: string) {
    this.requirementSet.delete(variable);
    this.requirementCounter.value = this.requirementSet.size;
  }

  public getRequirementCounter() {
    return readonly(this.requirementSet);
  }

  public reset() {
    this.isActive = false;
    this.listOfVariablesByAccessLevelAndStatus.value.clear();
    this.listOfStatusRequiredByAccessLevelAndVariable.clear();
    this.statusMap.value.clear();
    this.status.value = 0;
    this.warningCounter.value = 0;
    this.errorCounter.value = 0;
    this.requirementCounter.value = 0;
    this.warningSet.clear();
    this.confirmedWarningSet.clear();
    this.errorSet.clear();
    this.requirementSet.clear();
  }

  getWarnings(): Set<string> {
    return new Set(this.warningSet);
  }

  getConfirmedWarnings(): Set<string> {
    return new Set(this.confirmedWarningSet);
  }

  getErrors(): Set<string> {
    return new Set(this.errorSet);
  }

  getMissingList(accessLevels: Array<string>, statuses: Array<number>, prefixes: Array<string>): Array<string> {
    const missingsForAccessLevel: Map<string, Array<string>> = new Map();
    const allAccessLevels: Array<string> = Array.from(this.listOfVariablesByAccessLevelAndStatus.value.keys());
    const filteredAccessLevels: Array<string> =
      accessLevels.length === 0 /* if none are passed */
        ? allAccessLevels
        : allAccessLevels.filter((al) => accessLevels.includes(al));
    filteredAccessLevels.forEach((accessLevel) => {
      const listOfVariablesByStatusFilteredByAccessLevel =
        this.listOfVariablesByAccessLevelAndStatus.value.get(accessLevel);
      if (!listOfVariablesByStatusFilteredByAccessLevel) {
        throw new Error();
      }
      // get all the access levels with status under the missingListStatusLevel
      const missingStatusLevels: Array<number> =
        statuses.length !== 0
          ? statuses
          : /* allStatuses */ Array.from(listOfVariablesByStatusFilteredByAccessLevel.keys());
      // get all the variables with status under the missingListStatusLevel and name equal to requested variable
      missingsForAccessLevel.set(
        accessLevel,
        missingStatusLevels.flatMap((status) => {
          const listOfVars = this.listOfVariablesByAccessLevelAndStatus.value.get(accessLevel)?.get(status);
          if (!listOfVars) {
            return [];
          }
          return Array.from(listOfVars)
            .filter(
              (v) => !this.getErrors().has(v) && !this.getWarnings().has(v) && !this.getConfirmedWarnings().has(v)
            )
            .filter((v) => !v.endsWith('.ACTIVE') && prefixes.some((prefix) => v.startsWith(prefix)));
        })
      );
    });
    return Array.from(missingsForAccessLevel.values()).flat();
  }
}

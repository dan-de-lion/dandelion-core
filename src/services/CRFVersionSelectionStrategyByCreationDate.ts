import { inject, injectable } from 'inversify';
import { ActivationPeriodsForCrfStorage } from '../storages/ActivationPeriodsForCrfStorage';
import { CRFVersionSelectionStrategy } from './CRFVersionSelectionStrategy';
import { CaseMetaData } from '@/entities/CaseMetaData';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export class CRFVersionSelectionStrategyByCreationDate extends CRFVersionSelectionStrategy {
  constructor(
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfiguration,
    @inject(NamedBindings.CRFService) protected crfService: CRFServiceInterface,
    @inject(NamedBindings.CurrentContextService) protected currentContextService: CurrentContextService,
    @inject(NamedBindings.ActivationPeriodForCentreCodeStorage)
    protected activationPeriodsForCrfStorage: ActivationPeriodsForCrfStorage,
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface
  ) {
    super(coreConfiguration, crfService, currentContextService, activationPeriodsForCrfStorage, userService);
  }

  public getReferenceDateIfChanged(_oldModel: any, _newModel: any, _caseMetaData: CaseMetaData): Date | false {
    // The creation date is immutable, ensuring that the reference date remains unchanged.
    return false;
  }

  protected getReferenceDate(_variableName: string, _model: any, caseMetaData: CaseMetaData): Date {
    return this.checkReferenceDate(caseMetaData.creationDate);
  }
}

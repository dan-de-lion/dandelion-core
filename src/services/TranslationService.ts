import i18n, { type Resource } from 'i18next';
import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import { DandelionService } from './DandelionService';
import defaultTranslations from '../../public/defaultTranslationsForCore.json';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import { NamedBindings } from '@/types/NamedBindings';

export enum Language {
  it = 'it',
  en = 'en',
}

@injectable()
export default class TranslationService extends DandelionService {
  protected defaultResources: Resource = defaultTranslations;
  protected translatorMode;
  protected language;

  constructor(
    @inject(NamedBindings.CoreConfiguration)
    protected coreConfiguration: CoreConfiguration
  ) {
    super();
    this.translatorMode = this.coreConfiguration.debug;
    this.language = this.coreConfiguration.defaultLanguage ?? Language.en;

    i18n.init({
      lng: this.language,
      debug: false,
      resources: this.defaultResources,
    });
    this.addTranslations(this.coreConfiguration.additionalTranslations ?? {});
  }

  public t(key: string, interpolationValues?: Record<string, string>) {
    return this.translatorMode && !i18n.exists(key)
      ? `<strong style="color: red;">${key}</strong>`
      : i18n.t(key, interpolationValues ?? {});
  }

  public setTranslatorMode(boolean: boolean) {
    this.translatorMode = boolean;
  }

  public async setLanguage(language: Language) {
    this.language = language;
    try {
      await i18n.changeLanguage(language);
    } catch (e) {
      if (this.coreConfiguration.debug) {
        return console.log('something went wrong loading');
      }
    }
  }

  public addTranslations(translations: Resource = this.defaultResources) {
    // necessary
    // eslint-disable-next-line guard-for-in
    for (const lang in translations) {
      const translationsToBeAdd = translations[lang];
      // necessary
      // eslint-disable-next-line guard-for-in
      for (const valuesToAdd in translationsToBeAdd) {
        const keysToBeAdd = translationsToBeAdd[valuesToAdd];
        i18n.addResourceBundle(lang, 'translation', keysToBeAdd, true, true);
      }
    }
  }
}

import { inject, injectable } from 'inversify';
import { nextTick, readonly, type Ref, ref } from 'vue';
import { DandelionService } from './DandelionService';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CurrentPageServiceInterface } from '@/interfaces/CurrentPageServiceInterface';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export default class CurrentPageService extends DandelionService implements CurrentPageServiceInterface {
  constructor(
    @inject(NamedBindings.CoreConfiguration)
    protected coreConfiguration: CoreConfigurationInterface,
    @inject(NamedBindings.Logger)
    protected logger: LoggerInterface,
    protected showParents: boolean = true
  ) {
    super();
  }

  protected listOfPages: Ref<Array<string>> = ref([]);

  public get listOfActivePages() {
    return readonly(this.listOfPages);
  }

  public get(id: string) {
    return this.listOfPages.value.includes(id);
  }

  public set(id: string, showParents?: boolean) {
    this.listOfPages.value = [];
    this.listOfPages.value.push(id);

    if ((showParents === undefined && !this.showParents) || showParents === false) {
      return;
    }

    const components = id.split('.');
    this.listOfPages.value.push(...components.map((_, i) => components.slice(0, i + 1).join('.')));
  }

  public async scrollToVariable(fullNameForID: string, e?: Event) {
    e?.stopPropagation();

    if (!this.get(fullNameForID)) {
      this.set(fullNameForID);
    }

    await nextTick();

    const element = document.getElementById(fullNameForID) as HTMLElement;

    const height = window.innerHeight;
    const width = window.innerWidth;

    const vh = height * 0.01;
    const vw = width * 0.01;

    try {
      window.scrollTo({
        top: element.offsetTop - this.coreConfiguration.marginForScroll![0] * vh,
        left: element.offsetLeft - this.coreConfiguration.marginForScroll![1] + vw,
        behavior: 'smooth',
      });
    } catch {
      if (this.coreConfiguration.debug) {
        this.logger.logError(`Scrolling to variable ${fullNameForID} failed`);
      }
    }
  }
}

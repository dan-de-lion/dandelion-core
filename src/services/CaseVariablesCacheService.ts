import _ from 'lodash';
import { inject, injectable, preDestroy } from 'inversify';
import { type UnwrapNestedRefs, reactive, computed, watch, type Ref } from 'vue';
import { DandelionService } from './DandelionService';
import { CaseUpdated } from '@/application-events/CaseUpdated';
import { getContext } from '@/ContainersConfig';
import { CaseListElement } from '@/entities/CaseListElement';
import { Case, CaseForUpdate } from '@/entities/Cases';
import { CaseMetaData } from '@/entities/CaseMetaData';
import type { User } from '@/entities/User';
import { CaseVariablesCacheServiceInterface } from '@/interfaces/CaseVariablesCacheServiceInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { VariablesCacheStorageInterface } from '@/interfaces/VariablesCacheStorageInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import Helpers from '@/utils/Helpers';

export declare type CaseID = string;
export declare type CaseCacheDTO = {
  caseMetaData: CaseMetaData;
  variables: Map<string, any>;
  lastUpdate: Date;
};

@injectable()
export default class CaseVariablesCacheService extends DandelionService implements CaseVariablesCacheServiceInterface {
  protected readonly cachedCases: UnwrapNestedRefs<Map<CaseID, CaseCacheDTO>>;
  protected cachedVariables: Array<string> = new Array<string>();
  protected readonly caseUpdatedToken: string;
  protected user: Ref<User | null>;

  constructor(
    @inject(NamedBindings.VariablesCacheStorage)
    protected cacheStorage: VariablesCacheStorageInterface,
    @inject(NamedBindings.CoreConfiguration)
    protected coreConfiguration: CoreConfigurationInterface,
    @inject(NamedBindings.UserService)
    protected userService: UserServiceInterface
  ) {
    super();
    this.cachedCases = reactive(this.cacheStorage.getCachedCases());
    this.user = this.userService.getCurrentUser();
    this.caseUpdatedToken = getContext(Contexts.SHARED).addEventListener(
      ApplicationEvents.CaseUpdated,
      this.caseSavedHandler.bind(this)
    );
    this.cachedVariables = [];
    this.coreConfiguration.listOfCaseTypes.forEach((caseType) => {
      // We need to check if the cachedVariables is defined, because it is optional
      if (caseType.cachedVariables !== undefined) {
        this.cachedVariables = this.cachedVariables.concat(caseType.cachedVariables);
      }
    });

    watch(this.user, () => {
      this.updateCache();
    });
  }

  @preDestroy()
  public onDestroy() {
    getContext(Contexts.SHARED).removeEventListenerByToken(this.caseUpdatedToken);
  }

  getCaseListElementFromCache(caseID: string): CaseListElement | undefined {
    const cachedCase = computed(() => this.cachedCases.get(caseID));
    if (!cachedCase.value) {
      return undefined;
    }
    const caseMetaData = computed(() => cachedCase.value!.caseMetaData);
    const variables = computed(() => cachedCase.value!.variables);
    return new CaseListElement(caseMetaData, variables, cachedCase.value!.lastUpdate);
  }

  public invalidateCacheFor(caseID: string) {
    this.cachedCases.delete(caseID);
    this.cacheStorage.overwriteCache(this.cachedCases);
  }

  public updateCache() {
    this.cachedCases.clear();
    Array.from(this.cacheStorage.getCachedCases()).forEach(([key, value]) => {
      this.cachedCases.set(key, value);
    });
  }

  public createCacheOfSpecificCase(defaultCase: Case) {
    const copiedModel = _.cloneDeep(defaultCase.data);
    const cachedCaseVariables = new Map();
    this.cachedVariables.forEach((variableToCache) => {
      const variable = Helpers.getObjByPath(variableToCache, copiedModel);
      if (variable !== undefined) {
        cachedCaseVariables.set(variableToCache, variable);
      }
    });
    this.cachedCases.set(defaultCase.caseID, {
      caseMetaData: defaultCase.caseMetaData,
      variables: cachedCaseVariables,
      lastUpdate: defaultCase.caseMetaData.lastUpdate,
    });
    this.cacheStorage.overwriteCache(this.cachedCases);
  }

  public updateCacheOfSpecificCase(caseForUpdate: CaseForUpdate) {
    const copiedModel = _.cloneDeep(caseForUpdate.data);
    const cachedCaseVariables = new Map();
    this.cachedVariables.forEach((variableToCache) => {
      const variable = Helpers.getObjByPath(variableToCache, copiedModel);
      if (variable !== undefined) {
        cachedCaseVariables.set(variableToCache, variable);
      }
    });
    // Update of the cache case, we have to add the new caseMetaData
    if (this.cachedCases.has(caseForUpdate.caseID)) {
      const cachedCase = this.cachedCases.get(caseForUpdate.caseID)!;
      this.cachedCases.set(caseForUpdate.caseID, {
        caseMetaData: {
          ...cachedCase.caseMetaData,
          ...caseForUpdate.saveMetaData,
        },
        variables: cachedCaseVariables,
        lastUpdate: caseForUpdate.saveMetaData.lastUpdate,
      });
    }
    this.cacheStorage.overwriteCache(this.cachedCases);
  }

  protected caseSavedHandler(_message: string, event: Event) {
    if (!(event instanceof CaseUpdated)) {
      return;
    }
    const caseForUpdate = event.caseForUpdate;
    this.updateCacheOfSpecificCase(caseForUpdate);
  }

  public invalidateForAll() {
    this.cachedCases.clear();
  }
}

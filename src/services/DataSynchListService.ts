import { inject } from 'inversify';
import type { Ref } from 'vue';
import { getContext } from '@/ContainersConfig';
import { DandelionService } from './DandelionService';
import { SynchListElement } from '@/entities/SynchListElement';
import type { User } from '@/entities/User';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { CreateCaseEvent } from '@/source-events/list/CreateCaseEvent';
import { DeleteCaseEvent } from '@/source-events/list/DeleteCaseEvent';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { PermanentlyDeleteCaseEvent } from '@/source-events/list/PermanentlyDeleteCaseEvent';
import { RestoreCase } from '@/source-events/list/RestoreCase';
import { UpdateCaseEvent } from '@/source-events/list/UpdateCaseEvent';
import type CurrentContextService from '@/services/CurrentContextService';
import { NamedBindings } from '@/types/NamedBindings';
import { MagicStrings } from '@/types/MagicStrings';

export class DataSynchListService extends DandelionService {
  protected synchList: Array<SynchListElement> = new Array<SynchListElement>();
  protected deletedSynchList: Array<SynchListElement> = new Array<SynchListElement>();
  protected permanentlyDeletedSynchList: Array<string> = new Array<string>();
  protected stackOfEvents: Array<SourceEventForList> = [];
  protected readonly caseType: string = '';
  protected readonly user: Ref<User | null>;

  constructor(
    @inject(NamedBindings.StorageForSynchEvents)
    protected stackOfEventsStorage: EventsStorageInterface,
    @inject(NamedBindings.CurrentContextService)
    protected currentContextService: CurrentContextService,
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface
  ) {
    super();
    this.caseType = this.currentContextService.getCaseConfiguration().caseName;
    this.user = this.userService.getCurrentUser();
  }

  public async getCaseList(): Promise<Array<SynchListElement>> {
    this.synchList = [];
    this.deletedSynchList = [];
    this.permanentlyDeletedSynchList = [];

    await this.updateListWithRemoteEvents();

    for (const event of this.stackOfEvents) {
      // necessary
      // @ts-ignore
      await Reflect.apply(this[event.handler], this, [event]);
    }
    return this.synchList;
  }

  protected async updateListWithRemoteEvents() {
    const remoteStackOfEvents =
      ((await this.stackOfEventsStorage.loadAllEvents(
        this.user.value?.centre ?? '',
        this.caseType
      )) as Array<SourceEventForList>) ?? [];

    const toBeAdded = remoteStackOfEvents.filter(
      (remoteEvent: SourceEventForList) => !this.stackOfEvents.find((event) => event.id === remoteEvent.id)
    );

    this.stackOfEvents.push(...toBeAdded);
  }

  public async getDeletedCaseList(): Promise<Array<SynchListElement>> {
    return this.deletedSynchList;
  }

  public async getPermanentlyDeletedList(): Promise<Array<string>> {
    return this.permanentlyDeletedSynchList;
  }

  public async addCase(caseID: string, timestamp: Date) {
    const event = new CreateCaseEvent(caseID, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    this.stackOfEvents.push(event);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    if (
      (
        getContext(this.currentContextService.context).container.get(
          NamedBindings.CoreConfiguration
        ) as CoreConfiguration
      ).debug
    ) {
      console.log('Case added: ', event.id);
    }
  }

  public async updateCase(id: string, timestamp: Date) {
    const event = new UpdateCaseEvent(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    this.stackOfEvents.push(event);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    if (
      (
        getContext(this.currentContextService.context).container.get(
          NamedBindings.CoreConfiguration
        ) as CoreConfiguration
      ).debug
    ) {
      console.log('Case updated: ', event.id);
    }
  }

  public async removeCase(id: string, timestamp: Date) {
    const event = new DeleteCaseEvent(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    this.stackOfEvents.push(event);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    if (
      (
        getContext(this.currentContextService.context).container.get(
          NamedBindings.CoreConfiguration
        ) as CoreConfiguration
      ).debug
    ) {
      console.log('Case deleted: ', event.id);
    }
  }

  public async restoreCase(id: string, timestamp: Date) {
    const event = new RestoreCase(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    this.stackOfEvents.push(event);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    if (
      (
        getContext(this.currentContextService.context).container.get(
          NamedBindings.CoreConfiguration
        ) as CoreConfiguration
      ).debug
    ) {
      console.log('Case Restored: ', event.id);
    }
  }

  public async permanentlyDeleteCase(id: string, motivation: string, timestamp: Date) {
    const event = new PermanentlyDeleteCaseEvent(
      id,
      motivation,
      this.user.value?.username ?? MagicStrings.EMPTY_USERNAME,
      timestamp
    );
    this.stackOfEvents.push(event);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    if (
      (
        getContext(this.currentContextService.context).container.get(
          NamedBindings.CoreConfiguration
        ) as CoreConfiguration
      ).debug
    ) {
      console.log('Case permanently deleted: ', event.id);
    }
  }

  protected applyCreateCase(event: CreateCaseEvent) {
    const caseListElement = new SynchListElement(event.caseID, event.timestamp);
    this.synchList.push(caseListElement);
  }

  protected applyUpdateCase(event: UpdateCaseEvent) {
    const caseIndex = this.findIndexOfAList(this.synchList, event.caseID);
    if (caseIndex !== -1) {
      this.synchList[caseIndex] = { ...this.synchList[caseIndex], caseID: event.caseID };
      this.synchList[caseIndex] = { ...this.synchList[caseIndex], lastUpdate: event.timestamp };
    }
  }

  protected applyDeleteCase(event: DeleteCaseEvent) {
    DataSynchListService.moveCaseFromAListToAnother(event.caseID, this.synchList, this.deletedSynchList);
  }

  protected applyRestoreCase(event: RestoreCase) {
    if (!this.deletedSynchList.find((x: SynchListElement) => x.caseID === event.caseID)) {
      const createEvent: CreateCaseEvent = new CreateCaseEvent(
        event.caseID,
        this.user.value?.username ?? '',
        new Date()
      );
      this.applyCreateCase(createEvent);
    }
    DataSynchListService.moveCaseFromAListToAnother(event.caseID, this.deletedSynchList, this.synchList);
  }

  protected applyPermanentlyDeleteCase(event: PermanentlyDeleteCaseEvent) {
    if (this.synchList.findIndex((x) => x.caseID === event.caseID) !== -1) {
      this.moveCaseToPermanentlyDeleteList(event.caseID, this.synchList);
    } else if (this.deletedSynchList.findIndex((x) => x.caseID === event.caseID) !== -1) {
      this.moveCaseToPermanentlyDeleteList(event.caseID, this.deletedSynchList);
    }
  }

  protected findIndexOfAList(list: Array<any>, id: string) {
    return list.findIndex((x: SynchListElement) => x.caseID === id);
  }

  protected static moveCaseFromAListToAnother(
    caseID: string,
    listToBeRemovedFrom: Array<SynchListElement>,
    listToBeAddedTo: Array<SynchListElement>
  ) {
    const caseToBeMoved = listToBeRemovedFrom.find((xCase) => xCase.caseID === caseID);
    if (caseToBeMoved) {
      DataSynchListService.removeCaseFromList(listToBeRemovedFrom, caseID);
      listToBeAddedTo.push(caseToBeMoved);
    }
  }

  protected moveCaseToPermanentlyDeleteList(caseID: string, listToBeRemovedFrom: Array<SynchListElement>) {
    const caseToBeMoved = listToBeRemovedFrom.find((xCase) => xCase.caseID === caseID);
    if (caseToBeMoved) {
      DataSynchListService.removeCaseFromList(listToBeRemovedFrom, caseID);
      this.permanentlyDeletedSynchList.push(caseToBeMoved.caseID);
    }
  }

  protected static removeCaseFromList(list: Array<SynchListElement>, caseID: string): void {
    const caseToDeleteIndex = list.findIndex((caseItem) => caseItem.caseID === caseID);

    if (caseToDeleteIndex > -1) {
      list.splice(caseToDeleteIndex, 1);
    }
  }
}

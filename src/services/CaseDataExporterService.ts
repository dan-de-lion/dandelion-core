import { inject, injectable } from 'inversify';
import Papa from 'papaparse';
import { DandelionService } from './DandelionService';
import type { CRFVariableInterface } from '@/entities/CRFEntities';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import type { CsvFile } from '@/entities/exportStrategy/CSVFile';
import { ToCSVStrategyFactory } from '@/entities/exportStrategy/ToCSVStrategyFactory';
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { OptionsForExportInterface } from '@/interfaces/OptionsForExportInterface';
import type { ToCSVStrategy } from '@/interfaces/ToCSVStrategyInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import type { CaseService } from '@/services/CaseService';
import type CurrentContextService from '@/services/CurrentContextService';
import { NamedBindings } from '@/types/NamedBindings';
import Helpers from '@/utils/Helpers';

export type ExportConfiguration = {
  variableList?: Array<ExportableVariable>;
  excludedVariables?: Array<string>;
  crfList?: Array<string>;
  excludedCrfs?: Array<string>;
  options?: OptionsForExportInterface;
};

@injectable()
export class CaseDataExporterService extends DandelionService {
  protected defaultExportConfig: ExportConfiguration;
  constructor(
    @inject(NamedBindings.CurrentContextService)
    protected currentContextService: CurrentContextService,
    @inject(NamedBindings.CaseListService)
    protected caseListService: CaseListServiceInterface,
    @inject(NamedBindings.CRFService)
    protected crfService: CRFServiceInterface,
    @inject(NamedBindings.CaseService)
    protected caseService: CaseService,
    @inject(NamedBindings.UserService)
    protected userService: UserServiceInterface,
    @inject(NamedBindings.CRFVersionSelectionStrategy)
    protected crfSelector: CRFVersionSelectionStrategyInterface
  ) {
    super();
    this.defaultExportConfig = {
      variableList: [],
      excludedVariables: [],
      crfList: [],
      excludedCrfs: [],
      options: {
        exportLabelForValues: false,
        exportLabelForVariableNames: false,
        csvSeparator: ';',
        splitColumnForSingleChoiceValues: false,
        splitColumnForMultipleChoiceValues: false,
        splitColumnForGroups: true,
        removeEmptyRows: false,
        removeEmptyColumns: false,
        removeCsvFilesWithoutRows: false,
        removeCsvFilesWithoutColumns: false,
        exportOnlyFilteredCases: false,
      },
    };
  }

  protected constructHierarchicalList(allVariables: Array<CRFVariableInterface>) {
    const obj = {};
    // sort the variables by name, in order to construct correctly the hierarchical list
    // first will be initialized the parent variables, then the children
    const sorted = allVariables.sort((a, b) => {
      if (a.fullName < b.fullName) {
        return -1;
      }
      if (a.fullName > b.fullName) {
        return 1;
      }
      return 0;
    });
    sorted.forEach((variable) => {
      const path = variable.fullName;
      Helpers.setObjByPath(path, obj, { type: variable.type });
    });
    return obj;
  }

  /**
   * This function returns the list of all the variables in the xml, that can be exported
   * If there is a set variable, all the children of that variable are ignored, since the set strategy will take care of them
   *  If the variable on allVariables isn't a base type, it is ignored, since it could be a Dataclass or a Group, which are wrapper of other base variables
   * @param allVariables
   */
  protected getAllExportableVariables(allVariables: Array<CRFVariableInterface>) {
    const obj: any = this.constructHierarchicalList(allVariables);
    const baseTypes = ['number', 'text', 'email', 'color', 'time', 'date', 'singlechoice', 'multiplechoice', 'plugin'];
    const leafs: Array<string> = [];

    const dfs = (obj: any, path: string) => {
      if (typeof obj === 'object') {
        // if the object is a base type, add it to the leafs, since we need to export it
        if (obj.type && baseTypes.includes(obj.type)) {
          leafs.push(path);
        } else if (obj.type === 'set') {
          // in this case we don't want to add the children of the set variable
          // it will be added by the set strategy
          leafs.push(path);
        } else {
          for (const key in obj) {
            if (Object.hasOwn(obj, key)) {
              dfs(obj[key], `${path}.${key}`);
            }
          }
        }
      }
    };
    // foreach crf in crfs, call dfs
    for (const key in obj) {
      if (Object.hasOwn(obj, key)) {
        dfs(obj[key], key);
      }
    }
    return leafs;
  }

  /**
   *  - exportLabelForValues : boolean parameter for insert the label of the variables into the matrix
   *  - exportLabelForVariableNames : boolean parameter for insert as column name the label of the variables
   *  - separator : the separator of every value in the csv
   *  - separateColumnForSingleChoiceValues : boolean parameter for expand the single choice variable into multiple columns
   *  - separateColumnForMultipleChoiceValue : boolean parameter for expand the multiple choice variable into multiple columns
   * @param exportConfig
   */
  public async toCSV(
    exportConfig = this.defaultExportConfig
  ): Promise<Array<{ info: { name: string }; returnArray: string }>> {
    // we need only the caseID in order to get the data
    const list: Array<string> = exportConfig.options?.exportOnlyFilteredCases
      ? this.caseListService.filteredCases.map((element) => element.caseMetaData.caseID)
      : this.caseListService.allCases.map((element) => element.caseID);
    const crfs = await this.crfSelector.getCRFConfigurationsByCentreCode();
    const filteredCrfs = crfs
      .filter((crf) => {
        if (!exportConfig.crfList) {
          return true;
        }
        return exportConfig.crfList.length === 0 || exportConfig.crfList.includes(crf.crfName);
      })
      .filter((crf) => {
        if (!exportConfig.excludedCrfs) {
          return true;
        }
        return !exportConfig.excludedCrfs.includes(crf.crfName);
      });
    const { allValues, allVariables, allDataClasses } = await this.crfService.getMultipleXmlData(filteredCrfs);
    const variablesToExport =
      exportConfig.variableList && exportConfig.variableList.length > 0
        ? exportConfig.variableList
        : this.getAllExportableVariables(allVariables).map(
            (variableName) => new ExportableVariable(variableName, variableName)
          );

    //first screening of variables to exclude
    const filteredVariablesToExport = variablesToExport.filter((exportableVariabile) => {
      if (!exportConfig.excludedVariables) {
        return true;
      }
      return !exportConfig.excludedVariables.includes(exportableVariabile.variableName);
    });
    // filter the variables to export, in order to have only the ones that the users can read
    const arrayOfExportables = filteredVariablesToExport
      .filter(async (variable) => {
        // the first part of the variable is the crfName
        const crfName = variable.variableName.split('.')[0];
        return this.userService.canRead(
          variable.variableName,
          crfName,
          await this.crfService.getLatestVersion(crfName)
        );
      })
      .map((exportableVariable) => {
        // get exportables with the correct strategy
        try {
          return ToCSVStrategyFactory.getStrategy(
            exportableVariable,
            exportConfig,
            allValues,
            allVariables,
            allDataClasses,
            0
          );
        } catch (e) {
          console.log(e);
        }
      });
    //get the first row of the csv
    const headers = arrayOfExportables.flatMap((exportable) => (!exportable ? [] : exportable.getHeader()));
    headers.unshift('CASE_GUID');
    const promises = list.map(async (caseID) => {
      const { data, caseMetaData } = await this.caseService.getDataFor(caseID);
      const singleBody = arrayOfExportables.flatMap((exportable) =>
        !exportable ? [] : exportable.getBody([], data, caseMetaData)
      );
      singleBody.unshift(caseID);
      return singleBody;
    });
    const bodies = await Promise.all(promises);
    bodies.unshift(headers);
    // Main csv returning object, not yet parsed
    const mainCSV: CsvFile = { info: { name: 'MAIN' }, csv: bodies };
    // additional csvs returning array, not yet parsed
    const filteredArray: ToCSVStrategy[] = arrayOfExportables.filter(
      (item): item is ToCSVStrategy => item !== undefined
    );
    const additionalCSVs = this.getAdditionalCSVs(filteredArray);
    // returning array of CSV, not yet parsed
    const removeEmptyRows = (csvFile: CsvFile) => ({
      ...csvFile,
      csv: csvFile.csv.filter((row) => row.every((c) => c.toString() === '')),
    });

    const removeEmptyColumns = (csvFile: CsvFile) => {
      const mapOfEmptyColumns = csvFile.csv[0].map((_, index) =>
        csvFile.csv.every((row) => row[index].toString() === '')
      );
      return {
        ...csvFile,
        csv: csvFile.csv.map((row) => row.filter((_, index) => !mapOfEmptyColumns[index])),
      };
    };
    const filterByRowCount = (csvFile: CsvFile) => csvFile.csv.length > 1;
    const filterByColumnCount = (csvFile: CsvFile) => csvFile.csv[0].length > 1;
    const arrayOfCsvFiles = [mainCSV, ...additionalCSVs];
    const filteredArrayOfCsvFiles = exportConfig.options
      ? arrayOfCsvFiles
          .map((csvFile) => (exportConfig.options!.removeEmptyRows ? removeEmptyRows(csvFile) : csvFile))
          .map((csvFile) => (exportConfig.options!.removeEmptyColumns ? removeEmptyColumns(csvFile) : csvFile))
          .filter((csvFile) => (exportConfig.options!.removeCsvFilesWithoutRows ? filterByRowCount(csvFile) : true))
          .filter((csvFile) =>
            exportConfig.options!.removeCsvFilesWithoutColumns ? filterByColumnCount(csvFile) : true
          )
      : arrayOfCsvFiles;

    //parsing and creating the resulting array
    const resultArray = filteredArrayOfCsvFiles.map((csvFile) => ({
      info: csvFile.info,
      returnArray: Papa.unparse(csvFile.csv, {
        delimiter: exportConfig.options && exportConfig.options.csvSeparator ? exportConfig.options.csvSeparator : ';',
      }),
    }));
    return resultArray;
  }

  getAdditionalCSVs(arrayOfExportables: Array<ToCSVStrategy>) {
    return arrayOfExportables.flatMap((exportable) => exportable.getAdditionalCSVs());
  }
}

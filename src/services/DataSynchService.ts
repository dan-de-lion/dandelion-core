import _ from 'lodash';
import { inject, injectable } from 'inversify';
import { DandelionService } from './DandelionService';
import { getContext } from '@/ContainersConfig';
import type { CaseListElement, ReadonlyCaseList, ReadonlyCaseListElement } from '@/entities/CaseListElement';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import type { SynchListElement } from '@/entities/SynchListElement';
import { CaseError, CaseErrorType } from '@/errors/CaseError';
import type { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import type { DataSynchStorageInterface } from '@/interfaces/DataSynchStorageInterface';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type { CaseListService } from '@/services/CaseListService';
import type CurrentContextService from '@/services/CurrentContextService';
import type { DataSynchListService } from '@/services/DataSynchListService';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export class DataSynchService extends DandelionService {
  protected model: any; // Necessary for eval

  constructor(
    @inject(NamedBindings.CaseStorage)
    protected originalStorage: CaseStorageInterface,
    @inject(NamedBindings.CaseListService)
    protected listService: CaseListService,
    @inject(NamedBindings.DataSynchListService)
    protected dataSynchListService: DataSynchListService,
    @inject(NamedBindings.DataSynchStorage)
    protected dataSynchStorage: DataSynchStorageInterface,
    @inject(NamedBindings.CurrentContextService)
    protected currentContextService: CurrentContextService,
    @inject(NamedBindings.UserService)
    protected userService: UserServiceInterface,
    protected secretVariables: string[] = []
  ) {
    super();
  }

  public startSynchCycle(synchCycleInSeconds = 30) {
    const { debug } = getContext(this.currentContextService.context).container.get(
      NamedBindings.CoreConfiguration
    ) as CoreConfiguration;
    setInterval(() => {
      this.synchronize();
    }, (debug ? 1000 : synchCycleInSeconds) * 1000);
  }

  public async synchronize(): Promise<any> {
    const context = getContext(this.currentContextService.context);
    const { debug } = context.container.get(NamedBindings.CoreConfiguration) as CoreConfiguration;

    if (debug) {
      console.log('==> Running DataSynch!');
    }

    try {
      // Fetch and update case lists concurrently
      const updateCaseListPromise = this.listService.updateCaseList();
      const getCaseListPromise = this.dataSynchListService.getCaseList();

      await Promise.all([updateCaseListPromise, getCaseListPromise]);

      // Fetch all required lists concurrently
      const [
        caseList = [],
        deletedCaseList = [],
        permanentlyDeletedCaseList = [],
        dataSynchList = [],
        deletedSynchList = [],
        permanentlyDeletedSynchList = [],
      ] = await Promise.all([
        this.listService.slicedCases,
        this.listService.slicedDeletedCases,
        this.listService.getPermanentlyDeletedList(),
        getCaseListPromise, // Reusing the already awaited promise
        this.dataSynchListService.getDeletedCaseList(),
        this.dataSynchListService.getPermanentlyDeletedList(),
      ]);

      await this.pickFirstToSend(
        caseList,
        deletedCaseList,
        permanentlyDeletedCaseList,
        dataSynchList,
        deletedSynchList,
        permanentlyDeletedSynchList
      );
    } catch (e) {
      console.error(e);
    }
  }

  protected async pickFirstToSend(
    caseList: ReadonlyCaseList,
    deletedCaseList: ReadonlyCaseList,
    permanentlyDeletedCaseList: Array<string>,
    dataSynchList: Array<SynchListElement>,
    deletedSynchList: Array<SynchListElement>,
    permanentlyDeletedSynchList: Array<string>
  ): Promise<void> {
    const { debug } = getContext(this.currentContextService.context).container.get(
      NamedBindings.CoreConfiguration
    ) as CoreConfiguration;

    for (let i = 0; i < permanentlyDeletedCaseList.length; i++) {
      if (permanentlyDeletedSynchList.find((x) => x === permanentlyDeletedCaseList[i])) {
        //  DO NOTHING
      } else {
        const id = permanentlyDeletedCaseList[i];
        //TODO: motivation
        await this.dataSynchListService.permanentlyDeleteCase(id, '', new Date());
        // necessary
        await this.dataSynchStorage.permanentlyDelete(id);
        if (debug) {
          console.log(`==> Permanently deleted case Synched ${id}`);
        }
        return;
      }
    }

    const toBeUpdated = new Array<ReadonlyCaseListElement>();
    const toBeUpdatedFromDeleted = new Array<ReadonlyCaseListElement>();
    const toBeCreated = new Array<ReadonlyCaseListElement>();
    const toBeRestored = new Array<ReadonlyCaseListElement>();
    const toBeDeleted = new Array<ReadonlyCaseListElement>();

    this.checkCaseList(
      caseList,
      toBeUpdated,
      toBeCreated,
      toBeRestored,
      dataSynchList,
      deletedSynchList,
      permanentlyDeletedSynchList
    );

    this.checkDeletedCaseList(
      deletedCaseList,
      permanentlyDeletedCaseList,
      toBeUpdated,
      toBeDeleted,
      dataSynchList,
      deletedSynchList,
      permanentlyDeletedSynchList
    );

    if (toBeUpdated.length !== 0) {
      const older = this.findOlder(toBeUpdated);
      if (older) {
        const id = older.caseMetaData.caseID;
        const timestamp = new Date();
        await this.trySaveFilteredData(id);
        await this.dataSynchListService.updateCase(id, timestamp);
        if (debug) {
          console.log(`==> Update case Synched ${id}`);
        }
        return;
      }
    }

    if (toBeUpdatedFromDeleted.length !== 0) {
      const older = this.findOlder(toBeUpdatedFromDeleted);
      if (older) {
        const id = older.caseMetaData.caseID;
        const timestamp = new Date();
        await this.trySaveFilteredData(id);
        await this.dataSynchListService.updateCase(id, timestamp);
        if (debug) {
          console.log(`==> Update case Synched ${id}`);
        }
        return;
      }
    }

    if (toBeCreated.length !== 0) {
      const older = this.findOlder(toBeCreated);
      if (older) {
        const id = older.caseMetaData.caseID;
        const timestamp = new Date();
        await this.trySaveFilteredData(id);
        await this.dataSynchListService.addCase(id, timestamp);
        if (debug) {
          console.log(`==> Add case Synched ${id}`);
        }
        return;
      }
    }

    if (toBeRestored.length !== 0) {
      const older = this.findOlder(toBeRestored);
      if (older) {
        const id = older.caseMetaData.caseID;
        const timestamp = new Date();
        await this.trySaveFilteredData(id);
        await this.dataSynchListService.restoreCase(id, timestamp);
        if (debug) {
          console.log(`==> Restore case Synched ${id}`);
        }
        return;
      }
    }

    if (toBeDeleted.length !== 0) {
      const older = this.findOlder(toBeDeleted);
      if (older) {
        const id = older.caseMetaData.caseID;
        const timestamp = new Date();
        await this.trySaveFilteredData(id);
        await this.dataSynchListService.removeCase(id, timestamp);
        if (debug) {
          console.log(`==> Delete case Synched ${id}`);
        }
      }
    }

    return;
  }

  protected checkDeletedCaseList(
    deletedCaseList: ReadonlyCaseList,
    permanentlyDeletedCaseList: Array<string>,
    toBeUpdated: Array<ReadonlyCaseListElement>,
    toBeDeleted: Array<ReadonlyCaseListElement>,
    dataSynchList: Array<SynchListElement>,
    deletedSynchList: Array<SynchListElement>,
    permanentlyDeletedSynchList: Array<string>
  ) {
    for (let i = 0; i < deletedCaseList.length; i++) {
      if (permanentlyDeletedSynchList.find((x) => x === deletedCaseList[i].caseMetaData.caseID)) {
        //  DO NOTHING
      } else if (deletedSynchList.find((x) => x.caseID === deletedCaseList[i].caseMetaData.caseID)) {
        if (this.caseMustBeUpdated(deletedCaseList[i], deletedSynchList)) {
          //@ts-expect-error
          toBeUpdated.push(_.cloneDeep<CaseListElement>(deletedCaseList[i]));
        }
      } else if (
        !deletedSynchList.find((x) => x.caseID === deletedCaseList[i].caseMetaData.caseID) &&
        !permanentlyDeletedCaseList.find((x) => x === deletedCaseList[i].caseMetaData.caseID) &&
        dataSynchList.find((x) => x.caseID === deletedCaseList[i].caseMetaData.caseID)
      ) {
        //@ts-expect-error
        toBeDeleted.push(_.cloneDeep<CaseListElement>(deletedCaseList[i]));
      }
    }
  }

  protected checkCaseList(
    caseList: ReadonlyCaseList,
    toBeUpdated: Array<ReadonlyCaseListElement>,
    toBeCreated: Array<ReadonlyCaseListElement>,
    toBeRestored: Array<ReadonlyCaseListElement>,
    dataSynchList: Array<SynchListElement>,
    deletedSynchList: Array<SynchListElement>,
    permanentlyDeletedSynchList: Array<string>
  ) {
    for (let i = 0; i < caseList.length; i++) {
      if (permanentlyDeletedSynchList.find((x) => x === caseList[i].caseMetaData.caseID)) {
        //  DO NOTHING
      } else if (dataSynchList.find((x) => x.caseID === caseList[i].caseMetaData.caseID)) {
        if (this.caseMustBeUpdated(caseList[i], dataSynchList)) {
          toBeUpdated.push(_.cloneDeep<ReadonlyCaseListElement>(caseList[i]));
        }
      } else {
        if (
          !deletedSynchList.find((x) => x.caseID === caseList[i].caseMetaData.caseID) &&
          !permanentlyDeletedSynchList.find((x) => x === caseList[i].caseMetaData.caseID)
        ) {
          toBeCreated.push(_.cloneDeep<ReadonlyCaseListElement>(caseList[i]));
        }
        if (deletedSynchList.find((x) => x.caseID === caseList[i].caseMetaData.caseID)) {
          toBeRestored.push(_.cloneDeep<ReadonlyCaseListElement>(caseList[i]));
        }
      }
    }
  }

  protected findOlder(list: ReadonlyCaseList) {
    let olderDate = new Date();
    let older = null;
    for (const p of list) {
      if (new Date(p.caseMetaData.lastUpdate) <= olderDate) {
        olderDate = new Date(p.caseMetaData.lastUpdate);
        older = p;
      }
    }
    return older;
  }

  protected async trySaveFilteredData(id: string) {
    try {
      const partialStoredCases = await this.originalStorage.getPartials(id);
      const caseMetaData = await this.originalStorage.getCaseMetaData(id);
      const filteredData = this.filterData(partialStoredCases);
      filteredData.__guid__ = id;
      if (partialStoredCases !== null) {
        partialStoredCases.forEach((_partial: PartialStoredCase) => {
          filteredData.__status__ = caseMetaData.status;
        });
      }
      filteredData.__centrecode__ = this.userService.getCurrentUser().value?.centre;
      await this.dataSynchStorage.saveSynch(filteredData, caseMetaData);
    } catch (e) {
      throw new CaseError(`Save filtered data for case ${id} failed in DataSynchService`, CaseErrorType.SYNCH_ERROR, e);
    }
  }

  protected caseMustBeUpdated(p: ReadonlyCaseListElement, list: Array<SynchListElement>): boolean {
    return !list.find(
      (x) => x.caseID === p.caseMetaData.caseID && new Date(x.lastUpdate) >= new Date(p.caseMetaData.lastUpdate)
    );
  }

  protected filterData(originalData: Array<PartialStoredCase>) {
    if (!originalData) {
      return {};
    }
    this.model = originalData;
    for (const variable of this.secretVariables) {
      eval(`delete this.${variable}`);
    }
    const result = this.model;
    this.model = null;
    return result;
  }
}

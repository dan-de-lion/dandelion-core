import { injectable } from 'inversify';
import { DandelionService } from './DandelionService';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import type { CaseConfiguration } from '@/interfaces/CoreConfiguration';
import { ContainerTypes } from '@/ContainersConfig';

@injectable()
export default class CurrentContextService extends DandelionService {
  constructor(
    readonly context: string,
    protected readonly caseConfiguration: CaseConfiguration | null = null,
    readonly containerType: ContainerTypes = ContainerTypes.DEFAULT
  ) {
    super();
  }

  getCaseConfiguration(): CaseConfiguration {
    if (!this.caseConfiguration) {
      throw new ApplicationError(
        'CaseConfiguration not found, check if you are getting it on the SHARED context',
        AppErrorType.CASE_CONFIGURATION_NOT_FOUND
      );
    }
    return this.caseConfiguration;
  }
}

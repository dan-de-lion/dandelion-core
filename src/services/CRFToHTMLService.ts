import { inject, injectable } from 'inversify';
import { DandelionService } from './DandelionService';
import type { Version } from '@/entities/CRF';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import type { XSLT2Processor } from '@/types/XSLT2Processor';

@injectable()
export class CRFToHTMLService extends DandelionService {
  constructor(
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfiguration,
    protected cachedCRFS: Map<string, any> = new Map<string, any>(),
    protected processorsCache: Map<string, XSLT2Processor> = new Map<string, XSLT2Processor>()
  ) {
    super();
  }

  public async getHTMLfromXML(
    xml: string,
    crfName: string,
    crfVersion: Version,
    path: string,
    showGroupsInTree = true,
    showDataclassInstancesInTree = true,
    showSetsInTree = true,
    maxDepth = MagicNumbers.maxDepthForCRFComponentAndTree,
    startingDataClass = '',
    crfRoot = 'model', //TODO try to understand if we can derive from crf
    prefixForID = '',
    isDeveloperErrorsComponent = false
  ): Promise<string> {
    try {
      const keyFromParams = CRFToHTMLService.getKeyFromParams(...arguments);
      const cachedValue = this.cachedCRFS.get(keyFromParams);

      if (cachedValue) {
        return cachedValue;
      }

      let processor: XSLT2Processor;
      const cachedProcessor = this.processorsCache.get(path);

      if (!cachedProcessor) {
        //necessary
        //@ts-expect-error
        processor = new XSLT2Processor();

        try {
          await processor.importStylesheetURI(path);
        } catch (e) {
          throw new CoreError(
            `processor.importStylesheetURI failed for XSLT: ${path}`,
            CoreErrorType.XSLT_NOT_FOUND,
            e
          );
        }

        this.processorsCache.set(path, processor);
      } else {
        processor = cachedProcessor;
      }

      processor.setParameter(null, 'root', crfRoot);
      processor.setParameter(null, 'crfName', crfName);
      processor.setParameter(null, 'crfVersion', crfVersion);
      if (showGroupsInTree) {
        processor.setParameter(null, 'showGroupsInTree', 'true');
      }
      if (showDataclassInstancesInTree) {
        processor.setParameter(null, 'showDataclassInstancesInTree', 'true');
      }
      if (showSetsInTree) {
        processor.setParameter(null, 'showSetsInTree', 'true');
      }
      processor.setParameter(null, 'maxDepth', maxDepth.toString());
      processor.setParameter(null, 'startingDataClass', startingDataClass);
      processor.setParameter(null, 'statisticianMode', this.coreConfiguration.statisticianMode ? 'true' : 'false');
      processor.setParameter(null, 'prefixForID', prefixForID);

      const parser = new DOMParser();
      let xmlDoc;
      let objectToTransform;
      let fragment;

      try {
        xmlDoc = parser.parseFromString(xml, 'text/xml');
      } catch (e) {
        throw new CRFError(
          `parser.parseFromString failed for CRF: ${crfName} v${crfVersion}`,
          CRFErrorType.XML_PARSING,
          e
        );
      }

      if (!isDeveloperErrorsComponent) {
        objectToTransform =
          startingDataClass === ''
            ? xmlDoc.getElementsByTagName('dataClass')
            : xmlDoc.querySelectorAll(`dataClass[name='${startingDataClass}']`);

        if (objectToTransform.length < 1 || !objectToTransform[0]) {
          throw new CRFError(
            `Starting Dataclass ${startingDataClass || '[0]'} absent in CRF ${crfName} v${crfVersion}?`,
            CRFErrorType.STARTING_DATACLASS_NOT_FOUND
          );
        }

        try {
          fragment = processor.transformToFragment(objectToTransform[0], document);
        } catch (e) {
          throw new CoreError(
            `processor.transformToFragment failed for CRF: ${crfName} v${crfVersion} using schema ${path}`,
            CoreErrorType.XSL_TRANSFORMATION,
            e
          );
        }
      } else {
        const xmlDoc = parser.parseFromString(xml, 'text/xml');
        fragment = processor.transformToFragment(xmlDoc.firstElementChild!, document);
      }

      let resultHTML = '';

      if (!fragment.firstChild) {
        return resultHTML;
      }

      for (const i in fragment.childNodes) {
        // necessary
        // eslint-disable-next-line no-prototype-builtins
        if (fragment.childNodes.hasOwnProperty(i)) {
          resultHTML += (fragment.childNodes[i] as HTMLElement).outerHTML
            .replace(/&amp;/g, '&')
            .replace(/my-custom-template/g, 'template');
        }
      }

      this.cachedCRFS.set(keyFromParams, resultHTML);
      return resultHTML;
    } catch (e) {
      throw new ApplicationError('Generic error in getHTMLfromXML', AppErrorType.GENERIC_ERROR, e);
    }
  }

  protected static getKeyFromParams(...args: Array<any>): string {
    return args.reduce((result, arg) => result + arg);
  }
}

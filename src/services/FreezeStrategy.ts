import _ from 'lodash';
import { inject, injectable, preDestroy } from 'inversify';
import CurrentContextService from './CurrentContextService';
import { DandelionService } from './DandelionService';
import { getContext } from '@/ContainersConfig';
import { CaseFreezed } from '@/application-events/CaseFreezed';
import { CaseLocked } from '@/application-events/CaseLocked';
import { CaseUnfreezed } from '@/application-events/CaseUnfreezed';
import { CaseUnlocked } from '@/application-events/CaseUnlocked';
import { CaseUpdated } from '@/application-events/CaseUpdated';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export class FreezeStrategy extends DandelionService {
  protected caseUpdatedToken?: string;

  constructor(
    @inject(NamedBindings.CaseService) protected caseService: CaseServiceInterface,
    @inject(NamedBindings.CurrentContextService) protected currentContextService: CurrentContextService
  ) {
    super();
  }

  async activate() {
    this.deactivate();
    this.caseUpdatedToken = getContext(this.currentContextService.context).addEventListener(
      ApplicationEvents.CaseUpdated,
      async (m, event) => {
        if (
          !(event instanceof CaseFreezed) &&
          !(event instanceof CaseUnfreezed) &&
          !(event instanceof CaseLocked) &&
          !(event instanceof CaseUnlocked)
        ) {
          await this.updateFreezeStatusOnCompleteness(m, event as CaseUpdated);
        }
      }
    );
  }

  async deactivate() {
    if (this.caseUpdatedToken) {
      getContext(this.currentContextService.context).removeEventListenerByToken(this.caseUpdatedToken);
      this.caseUpdatedToken = undefined;
    }
  }

  @preDestroy()
  public onDestroy() {
    this.deactivate();
  }

  async updateFreezeStatusOnCompleteness(_m: string, event: CaseUpdated): Promise<void> {
    let isFrozen = false;
    let caseCompleted = true;
    const frozenAccessLevels = (await this.caseService.getCaseMetaDataFor(event.caseForUpdate.caseID))
      .frozenAccessLevels;
    frozenAccessLevels.forEach((freezeValue) => {
      if (freezeValue) {
        isFrozen = freezeValue;
        return;
      }
    });

    if (event.caseForUpdate.saveMetaData.status instanceof Map && event.caseForUpdate.saveMetaData.status.has('*')) {
      const nestedMap = event.caseForUpdate.saveMetaData.status.get('*');
      if (nestedMap instanceof Map) {
        nestedMap.forEach((value) => {
          if (value < 3) {
            caseCompleted = false;
          }
        });
      }
    }

    if (caseCompleted && !isFrozen) {
      this.caseService.freeze(event.caseForUpdate.caseID);
    } else if (!caseCompleted && isFrozen) {
      this.caseService.unfreeze(event.caseForUpdate.caseID);
    }
  }
}

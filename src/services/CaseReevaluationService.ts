import { Mutex } from 'async-mutex';
import { inject, injectable } from 'inversify';
import _ from 'lodash';
import { toRaw } from 'vue';
import { DandelionService } from './DandelionService';
import { ContainerTypes, getContainer } from '@/ContainersConfig';
import { CaseMetaData, SaveMetaData } from '@/entities/CaseMetaData';
import { Case, CaseForUpdate } from '@/entities/Cases';
import { needToBeReevaluated, type Version } from '@/entities/CRF';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { CachedCaseListElement } from '@/services/CaseListService';
import type { CurrentCaseServiceForReevaluation } from '@/services/CurrentCaseServiceForReevaluation';
import type CurrentContextService from '@/services/CurrentContextService';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';
import mountComponentForReevaluation from '@/utils/MountComponentForReevaluation';

@injectable()
export default class CaseReevaluationService extends DandelionService {
  protected mutex: Mutex = new Mutex();
  protected currentCaseService: CurrentCaseServiceForReevaluation;

  constructor(
    @inject(NamedBindings.CRFService)
    protected crfService: CRFServiceInterface,
    @inject(NamedBindings.CaseService)
    protected caseService: CaseServiceInterface,
    @inject(NamedBindings.CRFVersionSelectionStrategy)
    protected crfVersionSelectionStrategy: CRFVersionSelectionStrategyInterface,
    @inject(NamedBindings.CurrentContextService)
    protected currentContextService: CurrentContextService,
    @inject(NamedBindings.CoreConfiguration)
    protected coreConfiguration: CoreConfigurationInterface
  ) {
    super();
    this.currentCaseService = getContainer(currentContextService.context, ContainerTypes.REEVALUATION).get(
      NamedBindings.CurrentCaseServiceForReevaluation
    );
  }

  /**
   *
   * @param caseList list of cached case list elements
   */
  public async run(caseList: Array<CachedCaseListElement>) {
    const caseConfiguration = this.currentContextService.getCaseConfiguration();
    const allCaseMetaData = (await this.caseService.getAllCaseMetaDataFor(caseConfiguration.caseName)).map(
      (caseMetaData): [string, CaseMetaData] => [caseMetaData.caseID, caseMetaData]
    );
    const mapOfAllCaseMetaData = new Map(allCaseMetaData);
    const mapOfAllCrfVersions = new Map(
      await Promise.all(
        caseList.map(async ({ caseID, referenceDate }): Promise<[string, Map<string, Version>]> => {
          const versions = this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByDate(referenceDate);
          return [caseID, await versions];
        })
      )
    );

    const listOfPromises = caseList.map(async ({ caseID }) => {
      const caseMetaData = mapOfAllCaseMetaData.get(caseID);
      if (!caseMetaData) {
        //TODO: log problem
        return;
      }

      const crfThatShouldHave = mapOfAllCrfVersions.get(caseID);
      if (!crfThatShouldHave) {
        //TODO: log problem
        return;
      }

      const isAnyCrfNotUpToDate = Array.from(crfThatShouldHave).some(([crfName, crfVersion]) =>
        needToBeReevaluated(crfVersion, caseMetaData.crfVersions.get(crfName))
      );
      const someCrfShouldNotBeThere = Array.from(caseMetaData.crfVersions.keys()).some(
        (crfName: string) => !crfThatShouldHave.has(crfName)
      );

      if (isAnyCrfNotUpToDate || someCrfShouldNotBeThere) {
        const defaultCase = await this.caseService.getDataFor(caseID);
        console.log(`############### REEVALUATING ${caseID} #################`);
        const reevaluatedCase = await this.reevaluateCase(
          new Case(defaultCase.caseID, defaultCase.data, defaultCase.caseMetaData)
        );
        return this.caseService.update(reevaluatedCase);
      }
    });

    await Promise.all(listOfPromises);
  }

  //we pass shouldLock false in currentCaseServiceForNewCase
  public async reevaluateCase(caseToBeReevaluated: Case, shouldLock = true): Promise<CaseForUpdate> {
    const release = await this.mutex.acquire();
    const currentContext = this.currentContextService.context;
    const { caseID, data, caseMetaData } = caseToBeReevaluated;
    const myCaseData = _.cloneDeep(data);
    await this.currentCaseService.open(caseID, myCaseData, caseMetaData, shouldLock);

    const crfNames = Array.from(
      (await this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase(data, caseMetaData)).keys()
    );

    const baseUrlForXslt = this.coreConfiguration.listOfOverridesSchemas?.includes(PathToXsl.SCHEMA_TO_COLLECTION_PAGE)
      ? this.coreConfiguration.baseUrlForOverrides
      : this.coreConfiguration.baseUrlForXslt;

    const component = await mountComponentForReevaluation(
      this.crfService!,
      this.crfVersionSelectionStrategy,
      caseMetaData,
      crfNames,
      baseUrlForXslt + PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
      false,
      false,
      false,
      MagicNumbers.maxDepthForCRFComponentAndTree,
      '',
      currentContext,
      this.currentCaseService.getModel()
    );

    const caseStatusService: CaseStatusServiceInterface = getContainer(currentContext, ContainerTypes.REEVALUATION).get(
      NamedBindings.CaseStatusService
    );

    // forces the update of the MapForStatus
    caseStatusService.updateStatusFn();
    const statusMap = _.cloneDeep(toRaw(caseStatusService.getStatusMap().value));
    const newCaseData = _.cloneDeep(this.currentCaseService.getModel());
    const newSaveMetadata: SaveMetaData = {
      status: new Map(statusMap.entries()),
      crfVersions: await this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase(myCaseData, caseMetaData),
      lastUpdate: new Date(),
    };
    component.unmount();
    await this.currentCaseService.terminateEditing(shouldLock);
    release();
    return new CaseForUpdate(caseID, newCaseData, newSaveMetadata);
  }
}

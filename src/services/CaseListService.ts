import 'reflect-metadata';
import { Mutex } from 'async-mutex';
import { inject, injectable } from 'inversify';
import { type DeepReadonly, reactive, readonly, ref, type Ref, ComputedRef, computed } from 'vue';
import { DandelionService } from './DandelionService';
import { CaseListUpdated } from '@/application-events/CaseListUpdated';
import { getContext } from '@/ContainersConfig';
import { CaseListElement, ReadonlyCaseList, ReadonlyCaseListElement } from '@/entities/CaseListElement';
import { CaseListSnapshot } from '@/entities/SnapshotAndEvents';
import type { User } from '@/entities/User';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import type { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import type { CaseVariablesCacheServiceInterface } from '@/interfaces/CaseVariablesCacheServiceInterface';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import type { FilterInterface } from '@/interfaces/FilterInterface';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import { ChangedReferenceDateEvent } from '@/source-events/list/ChangedReferenceDateEvent';
import { CreateCaseEvent } from '@/source-events/list/CreateCaseEvent';
import { DeleteCaseEvent } from '@/source-events/list/DeleteCaseEvent';
import { PermanentlyDeleteCaseEvent } from '@/source-events/list/PermanentlyDeleteCaseEvent';
import { RestoreCase } from '@/source-events/list/RestoreCase';
import { UpdateCaseEvent } from '@/source-events/list/UpdateCaseEvent';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { MagicStrings } from '@/types/MagicStrings';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { Case } from '@/entities/Cases';

export declare type CachedCaseListElement = {
  readonly caseID: string;
  lastUpdate: Date;
  referenceDate: Date;
};

@injectable()
export class CaseListService extends DandelionService implements CaseListServiceInterface {
  protected cachedCaseList = reactive<Array<CachedCaseListElement>>([]);
  protected caseList = reactive<Array<CaseListElement>>([]);
  protected filteredCaseList = reactive<Array<CaseListElement>>([]);
  protected cachedDeletedCaseList = reactive<Array<CachedCaseListElement>>([]);
  protected deletedCaseList = reactive<Array<CaseListElement>>([]);
  protected filteredDeletedCaseList = reactive<Array<CaseListElement>>([]);
  // TODO: reactive
  protected readonly permanentlyDeletedCaseList: Array<string> = new Array<string>();

  /** All cases in the list, no filters, no slices, no cached variables / metadata */
  public readonly allCases: DeepReadonly<CachedCaseListElement[]> = readonly(this.cachedCaseList);
  /** All filtered cases, no slices, available only if some filter is active */
  public readonly filteredCases: ReadonlyCaseList = readonly(this.filteredCaseList);
  /** All cases, filtered (if filters are active) and sliced: this is what the CaseListComponent should be showing */
  public readonly slicedCases: ReadonlyCaseList = readonly(this.caseList);
  /** All deleted cases in the list, no filters, no slices, no cached variables / metadata */
  public readonly allDeletedCases: DeepReadonly<CachedCaseListElement[]> = readonly(this.cachedDeletedCaseList);
  /** All deleted filtered cases, no slices, available only if some filter is active */
  public readonly filteredDeletedCases: ReadonlyCaseList = readonly(this.filteredDeletedCaseList);
  /** All deleted cases, filtered (if filters are active) and sliced: this is what the DeletedCaseListComponent should be showing */
  public readonly slicedDeletedCases: ReadonlyCaseList = readonly(this.deletedCaseList);

  /** Start of the slice / page, inclusive, i.e. if startList = 10, first element index is 10 */
  public readonly startList = ref(0);
  /** End of the slice / page, exclusive, i.e. if endList = 20, last element index is 19 */
  public readonly endList = ref(30);
  /** Start of the slice / page for deleted cases, inclusive, i.e. if startList = 10, first element index is 10 */
  public readonly startDeletedList = ref(0);
  /** End of the slice / page for deleted cases, exclusive, i.e. if endList = 20, last element index is 19 */
  public readonly endDeletedList = ref(30);

  protected readonly isUpdating = ref(false);

  // We need this variable, not possible to have a `computed` as the `isActive` methods in FilterInterface is not reactive
  public readonly areFiltersActive: Ref<boolean> = ref(false);
  protected arrayOfFilters = reactive<Array<FilterInterface>>([]);

  protected readonly mutex: Mutex = new Mutex();
  protected readonly numberOfAllCases = ref<number>(0);
  protected readonly numberOfFilteredCases = ref(0);
  protected readonly numberOfDeletedCases = ref(0);
  protected readonly numberOfFilteredDeletedCases = ref(0);

  protected readonly user: Ref<User | null>;
  protected readonly caseType;

  constructor(
    @inject(NamedBindings.StorageForEvents)
    protected stackOfEventsStorage: EventsStorageInterface,
    @inject(NamedBindings.CaseService)
    protected caseService: CaseServiceInterface,
    @inject(NamedBindings.CaseVariablesCacheService)
    protected caseVariablesCacheService: CaseVariablesCacheServiceInterface,
    @inject(NamedBindings.CurrentContextService)
    protected currentContextService: CurrentContextService,
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.Logger) protected logger: LoggerInterface,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface
  ) {
    super();
    this.arrayOfFilters = [];

    this.caseType = this.currentContextService.getCaseConfiguration().caseName;
    this.user = this.userService.getCurrentUser();
  }

  public async addCase(id: string, timestamp: Date): Promise<void> {
    const event = new CreateCaseEvent(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    await this.updateCaseList();
  }

  public async updateCase(id: string, timestamp: Date): Promise<void> {
    const event = new UpdateCaseEvent(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    await this.updateCaseList();
  }

  public async removeCase(id: string, timestamp: Date): Promise<void> {
    const event = new DeleteCaseEvent(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    await this.updateCaseList();
  }

  public async permanentlyDeleteCase(id: string, motivation: string, timestamp: Date): Promise<void> {
    const event = new PermanentlyDeleteCaseEvent(
      id,
      motivation,
      this.user.value?.username ?? MagicStrings.EMPTY_USERNAME,
      timestamp
    );
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    await this.updateCaseList();
  }

  public async restoreCase(id: string, timestamp: Date): Promise<void> {
    const event = new RestoreCase(id, this.user.value?.username ?? MagicStrings.EMPTY_USERNAME, timestamp);
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    await this.updateCaseList();
  }

  public async changedReferenceDate(id: string, changedReferenceDate: Date, timestamp: Date): Promise<void> {
    const event = new ChangedReferenceDateEvent(
      id,
      changedReferenceDate,
      this.user.value?.username ?? MagicStrings.EMPTY_USERNAME,
      timestamp
    );
    await this.stackOfEventsStorage.saveEvent(
      event,
      this.user.value?.centre ?? MagicStrings.EMPTY_CENTRE_CODE,
      this.caseType
    );
    await this.updateCaseList();
  }

  protected applyCreateCase(event: CreateCaseEvent): void {
    const caseIndex = this.cachedCaseList.findIndex((c) => c.caseID === event.caseID);
    if (caseIndex !== -1) {
      // TODO: log some info about this case that shouldn't happen
      this.applyUpdateCase(new UpdateCaseEvent(event.caseID, event.username, event.timestamp));
    } else {
      this.cachedCaseList.push({
        caseID: event.caseID,
        lastUpdate: event.timestamp,
        referenceDate: event.timestamp,
      });
    }
  }

  protected applyChangedReferenceDate(event: ChangedReferenceDateEvent): void {
    const cachedCase = this.cachedCaseList.find((c) => c.caseID === event.caseID);
    if (!cachedCase) {
      throw new ApplicationError(
        `Cached case for caseID ${event.caseID} not found`,
        AppErrorType.CACHED_CASE_NOT_FOUND
      );
    }
    cachedCase.referenceDate = event.referenceDate;
    cachedCase.lastUpdate = event.timestamp;
  }

  protected applyUpdateCase(event: UpdateCaseEvent): void {
    const cachedCase = this.cachedCaseList.find((c) => c.caseID === event.caseID);
    if (!cachedCase) {
      throw new ApplicationError(
        `Cached case for caseID ${event.caseID} not found`,
        AppErrorType.CACHED_CASE_NOT_FOUND
      );
    }
    cachedCase.lastUpdate = event.timestamp;
  }

  protected applyDeleteCase(event: DeleteCaseEvent): void {
    CaseListService.moveCaseFromAListToAnother(event.caseID, this.cachedCaseList, this.cachedDeletedCaseList);
  }

  protected applyPermanentlyDeleteCase(event: PermanentlyDeleteCaseEvent): void {
    if (this.cachedCaseList.find((x) => x.caseID === event.caseID)) {
      this.moveCaseToPermanentlyDeleteList(event.caseID, this.cachedCaseList);
    } else if (this.cachedDeletedCaseList.find((x) => x.caseID === event.caseID)) {
      this.moveCaseToPermanentlyDeleteList(event.caseID, this.cachedDeletedCaseList);
    }
  }

  protected applyRestoreCase(event: RestoreCase): void {
    CaseListService.moveCaseFromAListToAnother(event.caseID, this.cachedDeletedCaseList, this.cachedCaseList);
  }

  public async updateCaseList(): Promise<void> {
    const release = await this.mutex.acquire();
    this.isUpdating.value = true;

    await this.updateCachedCaseList();

    if (this.arrayOfFilters.some((filter) => filter.isActive())) {
      this.areFiltersActive.value = true;
      await this.updateListsWithFilters();
    } else {
      this.areFiltersActive.value = false;
      await this.updateListsWithoutFilters();
    }

    getContext(this.currentContextService.context).dispatchEvent(
      ApplicationEvents.CaseListUpdated,
      new CaseListUpdated(this.currentContextService.containerType)
    );

    this.isUpdating.value = false;
    release();
  }

  protected async updateCachedCaseList() {
    // Reset case list w/o loosing reactivity
    this.cachedDeletedCaseList.splice(0);
    this.permanentlyDeletedCaseList.splice(0);
    this.cachedCaseList.splice(0);

    const { snapshot, events } = await this.stackOfEventsStorage.getLastSnapshotAndEvents(
      this.user.value?.centre ?? '',
      this.caseType
    );
    this.cachedCaseList.push(...snapshot.cachedCaseList);

    // Apply remaining events that are not in snapshot
    for (const event of events) {
      try {
        // @ts-ignore necessary because we are doing some "magic" here
        await Reflect.apply(this[event.handler], this, [event]);
      } catch (e: any) {
        this.logger.logError(e.message, false);
      }
    }

    this.numberOfAllCases.value = this.cachedCaseList.length;
    this.numberOfDeletedCases.value = this.cachedDeletedCaseList.length;

    // Create a new snapshot if necessary
    if (events.length > MagicNumbers.eventsForNewSnapshot) {
      const date = new Date();
      const numberOfEvents = snapshot.numberOfEvents + events.length;
      const snapshotToBeSaved = new CaseListSnapshot(this.cachedCaseList, date, numberOfEvents);
      await this.stackOfEventsStorage.saveSnapshot(snapshotToBeSaved, this.user.value?.centre ?? '', this.caseType);
    }

    // Sort the cached case list by reference date DESC (max date is first)
    this.cachedCaseList.sort((a: CachedCaseListElement, b: CachedCaseListElement) => {
      if (a.referenceDate > b.referenceDate) {
        return -1;
      }
      if (a.referenceDate === b.referenceDate) {
        return 0;
      }
      return 1;
    });
  }

  // Despite "feature envy" it cannot be moved to CaseVariablesCacheService because caseService is needed
  public async retrieveCaseListElement(cachedCase: CachedCaseListElement): Promise<CaseListElement> {
    const { caseID, lastUpdate } = cachedCase;
    let caseListElement = this.caseVariablesCacheService.getCaseListElementFromCache(caseID);
    if (!caseListElement || caseListElement.lastUpdate < lastUpdate) {
      const defaultCase = await this.caseService.getDataFor(caseID);
      this.caseVariablesCacheService.createCacheOfSpecificCase(defaultCase);
      caseListElement = this.caseVariablesCacheService.getCaseListElementFromCache(caseID);
      if (!caseListElement) {
        throw new CoreError(
          `CaseVariableCacheService hasn\'t been able to create cache for ${caseID}`,
          CoreErrorType.CACHE_CREATION_FAILED
        );
      }
    }
    return caseListElement;
  }

  // Despite "feature envy" it cannot be moved to CaseVariablesCacheService because caseService is needed
  public async retrieveCaseListElements(cases: Array<CachedCaseListElement>): Promise<Array<CaseListElement>> {
    const maybeCaseListElements = cases?.map(({ caseID, lastUpdate }) => ({
      element: this.caseVariablesCacheService.getCaseListElementFromCache(caseID),
      lastUpdate,
      caseID,
    }));

    const alreadyCachedCases = maybeCaseListElements.filter(
      ({ element, lastUpdate }) => element && lastUpdate <= element.lastUpdate
    );

    const casesToBeCached = maybeCaseListElements.filter(
      ({ element, lastUpdate }) => !element || element.lastUpdate < lastUpdate
    );

    if (casesToBeCached.length <= 0) {
      return alreadyCachedCases.map(({ element }) => element!);
    }

    // Download and put in cache cases not already there
    const toBeCached: Array<Case> = await this.caseService.getMultipleDataFor(
      casesToBeCached.map(({ caseID }) => caseID)
    );
    toBeCached.forEach((singleCase) => this.caseVariablesCacheService.createCacheOfSpecificCase(singleCase));
    const results = casesToBeCached.map(({ caseID }) =>
      this.caseVariablesCacheService.getCaseListElementFromCache(caseID)
    );

    // We must start again from maybeCaseListElements to keep the order of the list
    return maybeCaseListElements
      .map(({ element, caseID }) => element ?? results.find((c) => c?.caseMetaData?.value?.caseID === caseID))
      .filter((x): x is CaseListElement => x !== undefined && x instanceof CaseListElement);
  }

  protected async updateListsWithFilters() {
    // Divide the list on chunks of items to avoid blocking the UI and making too big calls to backend
    const chunkedList = this.cachedCaseList.reduce(
      (accumulator: Array<Array<CachedCaseListElement>>, currentValue, index) => {
        if (index % (this.coreConfiguration.maxCasesLoadedPerRequest ?? 20) === 0) {
          accumulator.push([]);
        }
        accumulator[accumulator.length - 1].push(currentValue);
        return accumulator;
      },
      []
    );

    this.filteredCaseList.splice(0);
    let currentChunkIndex = 0;
    do {
      const start = this.startList.value;
      const end = this.endList.value;
      // Get the case list with cached variables, the filters require the variables
      const caseListChunk = reactive(await this.retrieveCaseListElements(chunkedList[currentChunkIndex]));
      const filteredCaseList = caseListChunk.filter((value: ReadonlyCaseListElement) =>
        this.arrayOfFilters.filter((f) => f.isActive()).every((singleFilter) => singleFilter.apply(readonly(value)))
      );
      this.filteredCaseList.push(...filteredCaseList);
      this.caseList.splice(0);
      this.caseList.push(...this.filteredCaseList.slice(start, end));
      // Update the (temporary) number of cases
      this.numberOfFilteredCases.value = filteredCaseList.length;

      currentChunkIndex++;
    } while (currentChunkIndex < chunkedList.length /* TODO: find a logic for stopping wisely */);

    // TODO: chunking? Maybe overkill, but can be uniformed with code for non-deleted
    const startDeleted = this.startDeletedList.value;
    const endDeleted = this.endDeletedList.value;

    const deletedCaseList = reactive(await this.retrieveCaseListElements(this.cachedDeletedCaseList));
    const filteredDeletedCaseList = deletedCaseList.filter((value: ReadonlyCaseListElement) =>
      this.arrayOfFilters.filter((f) => f.isActive()).every((singleFilter) => singleFilter.apply(readonly(value)))
    );
    this.filteredDeletedCaseList.splice(0);
    this.filteredDeletedCaseList.push(...filteredDeletedCaseList);
    this.deletedCaseList.splice(0);
    this.deletedCaseList.push(...this.filteredDeletedCaseList.slice(startDeleted, endDeleted));
    // Update the number of cases
    this.numberOfFilteredDeletedCases.value = filteredDeletedCaseList.length;
  }

  protected async updateListsWithoutFilters() {
    this.caseList.splice(0);

    let currentChunkIndex = 0;
    const numberOfRequestedElements = this.endList.value - this.startList.value;
    const chunkSize = this.coreConfiguration.maxCasesLoadedPerRequest ?? 20;
    do {
      const chunkStart = this.startList.value + chunkSize * currentChunkIndex;
      const chunkEnd = chunkStart + Math.min(this.endList.value - chunkStart, chunkSize);
      const currentChunk = this.cachedCaseList.slice(chunkStart, chunkEnd);
      if (currentChunk.length <= 0 || chunkStart > this.cachedCaseList.length) {
        // Something went wrong, e.g. some case has not loaded correctly
        break;
      }
      const caseListChunk = reactive(await this.retrieveCaseListElements(currentChunk));
      this.caseList.push(...caseListChunk);

      currentChunkIndex++;
    } while (
      this.caseList.length < numberOfRequestedElements &&
      this.caseList.length + this.startList.value < this.cachedCaseList.length
    );

    // TODO: chunking? Maybe overkill, but can be uniformed with code for non-deleted
    this.deletedCaseList.splice(0);
    this.deletedCaseList.push(...reactive(await this.retrieveCaseListElements(this.cachedDeletedCaseList)));
    this.numberOfDeletedCases.value = this.cachedDeletedCaseList.length;
  }

  public async setFilters(arrayOfFilter: Array<FilterInterface>): Promise<void> {
    this.arrayOfFilters.splice(0);
    this.arrayOfFilters.push(...arrayOfFilter);
    await this.updateCaseList();
  }

  // TODO: return a readonly, uniform with other methods
  public getPermanentlyDeletedList(): Array<string> {
    return this.permanentlyDeletedCaseList;
  }

  protected static moveCaseFromAListToAnother(
    caseID: string,
    listToBeRemovedFrom: CachedCaseListElement[],
    listToBeAddedTo: Array<CachedCaseListElement>
  ) {
    const caseToBeMoved = listToBeRemovedFrom.find((xCase) => xCase.caseID === caseID);
    if (caseToBeMoved) {
      CaseListService.removeCaseFromList(listToBeRemovedFrom, caseID);
      listToBeAddedTo.push(caseToBeMoved);
    }
  }

  // TODO: why not use moveCaseFromAListToAnother?
  protected moveCaseToPermanentlyDeleteList(caseID: string, listToBeRemovedFrom: Array<CachedCaseListElement>) {
    const caseToBeMoved = listToBeRemovedFrom.find((xCase) => xCase.caseID === caseID);
    if (caseToBeMoved) {
      CaseListService.removeCaseFromList(listToBeRemovedFrom, caseID);
      this.permanentlyDeletedCaseList.push(caseToBeMoved.caseID);
    }
  }

  public isUpdatingCaseList(): DeepReadonly<Ref<boolean>> {
    return readonly(this.isUpdating);
  }

  protected static removeCaseFromList(list: Array<CachedCaseListElement>, caseID: string): void {
    const caseToDeleteIndex = list.findIndex((caseItem) => caseItem.caseID === caseID);

    if (caseToDeleteIndex > -1) {
      list.splice(caseToDeleteIndex, 1);
    }
  }
}

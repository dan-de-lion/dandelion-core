import { inject, injectable } from 'inversify';
import _ from 'lodash';
import { DandelionService } from './DandelionService';
import { getContext } from '@/ContainersConfig';
import { CaseCreated } from '@/application-events/CaseCreated';
import { CaseCreationFailed } from '@/application-events/CaseCreationFailed';
import { CaseDeleted } from '@/application-events/CaseDeleted';
import { CaseDeletionFailed } from '@/application-events/CaseDeletionFailed';
import { CasePermanentlyDeleted } from '@/application-events/CasePermanentlyDeleted';
import { CasePermanentlyDeletionFailed } from '@/application-events/CasePermanentlyDeletionFailed';
import { CaseFreezed } from '@/application-events/CaseFreezed';
import { CaseLocked } from '@/application-events/CaseLocked';
import { CaseUnfreezed } from '@/application-events/CaseUnfreezed';
import { CaseUnlocked } from '@/application-events/CaseUnlocked';
import { CaseRestored } from '@/application-events/CaseRestored';
import { CaseRestoringFailed } from '@/application-events/CaseRestoringFailed';
import { CaseUpdated } from '@/application-events/CaseUpdated';
import { CaseUpdateFailed } from '@/application-events/CaseUpdateFailed';
import { Case, CaseForUpdate } from '@/entities/Cases';
import type { CaseMetaData } from '@/entities/CaseMetaData';
import type { Version } from '@/entities/CRF';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import { CaseError, CaseErrorType } from '@/errors/CaseError';
import type { AccessLevelsTree } from '@/interfaces/AccessLevelsTree';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import type { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { KeyStorageInterface, MultiKeysDTO } from '@/interfaces/KeyStorageInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import { AccessLevels } from '@/types/AccessLevels';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { dfs } from '@/utils/dfs';
import type { Motivations } from '@/utils/MapForMotivations';
import { Crypt } from '@/utils/Crypt';
import { Stringifier } from '@/utils/SerDes';

@injectable()
export class CaseService extends DandelionService implements CaseServiceInterface {
  constructor(
    @inject(NamedBindings.CaseStorage) protected caseStorage: CaseStorageInterface,
    @inject(NamedBindings.KeyStorage) protected keyStorage: KeyStorageInterface,
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.CurrentContextService) protected currentContext: CurrentContextService,
    @inject(NamedBindings.CRFService) protected crfService: CRFServiceInterface,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface,
    @inject(NamedBindings.CRFVersionSelectionStrategy) protected crfSelector: CRFVersionSelectionStrategyInterface
  ) {
    super();
  }

  async getAllCaseMetaDataFor(caseType: string): Promise<CaseMetaData[]> {
    return await this.caseStorage.getAllCaseMetaData(caseType);
  }

  async getCaseMetaDataFor(caseID: string): Promise<CaseMetaData> {
    try {
      return await this.caseStorage.getCaseMetaData(caseID);
    } catch (e: unknown) {
      throw new CaseError(
        `Found an error on getting caseMetaData for id: ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        }`,
        CaseErrorType.CASE_METADATA_NOT_FOUND,
        e
      );
    }
  }

  async getDataFor(caseID: string): Promise<Case> {
    try {
      const partialStoredCases = await this.caseStorage.getPartials(caseID);
      const caseMetaData = await this.caseStorage.getCaseMetaData(caseID);
      // Create model from partials (TODO: unify with createModelFromPartialStoredCases)
      const model: { [i: string]: any } = {};
      for (const p of partialStoredCases) {
        const canReadPartialCase = this.userService.hasReadAccessRights(p.accessLevel);
        if (canReadPartialCase && this.coreConfiguration.encrypt) {
          const key = await this.keyStorage.getKey(caseMetaData.caseID, p.accessLevel);
          _.merge(model, await Crypt.decrypt(p.data, key));
        } else if (canReadPartialCase) {
          const parsedData = new Stringifier().deserialize(p.data);
          _.merge(model, parsedData);
          for (const d of Object.getOwnPropertySymbols(parsedData)) {
            //@ts-expect-error necessary
            model[d] = _.cloneDeep(parsedData[d]);
          }
        }
      }
      await this.ensureAllCrfsAreInitialized(model, caseMetaData);
      return new Case(caseID, model, caseMetaData);
    } catch (e: unknown) {
      throw new CaseError(
        `Found an error on getting data for id: ${caseID ?? '< EMPTY ID >'}`,
        CaseErrorType.PARTIALS_NOT_FOUND,
        e
      );
    }
  }

  protected async ensureAllCrfsAreInitialized(model: { [i: string]: any }, caseMetaData: CaseMetaData) {
    const crfs = await this.crfSelector.getCRFConfigurationByCase(model, caseMetaData);
    crfs.forEach((crf) => {
      if (!(crf.crfName in model)) {
        model[crf.crfName] = {};
      }
    });
  }

  protected async createModelFromPartialStoredCases(
    partialStoredCases: PartialStoredCase[],
    caseID: string,
    keyMap: MultiKeysDTO[]
  ): Promise<{
    [i: string]: any;
  }> {
    const model: { [i: string]: any } = {};
    for (const p of partialStoredCases) {
      const canReadPartialCase = this.userService.hasReadAccessRights(p.accessLevel);
      if (!canReadPartialCase) {
        continue;
      }
      if (this.coreConfiguration.encrypt) {
        const key = keyMap.find((k) => k.accessLevel === p.accessLevel && k.caseID === caseID)?.key;
        if (!key) {
          throw new CaseError(
            `Cannot find key for decrypting case ${caseID}, accessLevel ${p.accessLevel}`,
            CaseErrorType.KEY_NOT_FOUND
          );
        }
        _.merge(model, await Crypt.decrypt(p.data, key));
      } else {
        const parsedData = new Stringifier().deserialize(p.data);
        _.merge(model, parsedData);
        for (const d of Object.getOwnPropertySymbols(parsedData)) {
          //@ts-expect-error necessary
          model[d] = _.cloneDeep(parsedData[d]);
        }
      }
    }
    return model;
  }

  async getMultipleDataFor(caseIDs: string[]): Promise<Case[]> {
    try {
      const { successCases: cases } = await this.caseStorage.getMultiplePartials(caseIDs);
      const { successCases: casesMetaData } = await this.caseStorage.getMultipleCaseMetaData(caseIDs);
      let keysMap: MultiKeysDTO[] = [];
      if (this.coreConfiguration.encrypt) {
        keysMap = (await this.keyStorage.getMultipleKeys(caseIDs)).successCases;
      }
      const promises = await Promise.allSettled(
        cases.map(async (c) => {
          if (!c.partials?.length) {
            throw new CaseError(
              `Error on getting partials for id: ${
                c.caseID || '< EMPTY ID >'
              }, partials are not present or have zero length`,
              CaseErrorType.PARTIALS_NOT_FOUND
            );
          }
          const caseMetaData = casesMetaData.find((cm) => cm.caseID === c.caseID)?.caseMetaData;
          if (!caseMetaData) {
            throw new CaseError(
              `Error on getting metadata for id: ${c.caseID || '< EMPTY ID >'}`,
              CaseErrorType.CASE_METADATA_NOT_FOUND
            );
          }
          const model = await this.createModelFromPartialStoredCases(c.partials, c.caseID, keysMap);
          this.ensureAllCrfsAreInitialized(model, caseMetaData);
          return new Case(c.caseID, model, caseMetaData);
        })
      );
      return promises.filter((p): p is PromiseFulfilledResult<Case> => p.status === 'fulfilled').map((p) => p.value);
    } catch (e: unknown) {
      throw new CaseError('Found an error on getting data for id:', CaseErrorType.PARTIALS_NOT_FOUND, e);
    }
  }

  /** This method uses the storage to save model and caseMetaData
   * @param caseToCreate
   * @throws error{CaseError} if there is a problem with crearing the case */
  async create(caseToCreate: Case): Promise<void> {
    try {
      const { caseID, data, caseMetaData } = caseToCreate;
      const partialCases = await this.extractPartialsFromModel(data, caseMetaData.crfVersions);
      const partialsToBeSaved: Map<string, PartialStoredCase> = new Map();
      const stringifier = new Stringifier();
      for (const [accessLevel, p] of partialCases.entries()) {
        if (caseMetaData.frozenAccessLevels.size == 0) {
          caseMetaData.frozenAccessLevels.set(accessLevel, false);
        }
        if (this.coreConfiguration.encrypt) {
          const generatedKey = Crypt.generateKey();
          await this.keyStorage.saveKey(caseID, generatedKey, accessLevel);
          // We get it again from storage to be sure it has been correctly saved
          const keyToBeUsed = await this.keyStorage.getKey(caseID, accessLevel);
          partialsToBeSaved.set(accessLevel, { accessLevel, data: await Crypt.encrypt(p, keyToBeUsed) });
        } else {
          partialsToBeSaved.set(accessLevel, { accessLevel, data: stringifier.serialize(p) });
        }
      }
      await this.caseStorage.createCase(Array.from(partialsToBeSaved.values()), caseMetaData);
      const clonedModel = _.cloneDeep(data);
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseCreated,
        new CaseCreated(clonedModel, caseID, caseMetaData.lastUpdate, this.currentContext.containerType)
      );
    } catch (e: unknown) {
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseCreationFailed,
        new CaseCreationFailed(caseToCreate.caseID, e, this.currentContext.containerType)
      );
      throw new CaseError(
        `Found an error on creating a case 
        ${caseToCreate.caseID ? caseToCreate.caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'}`,
        CaseErrorType.CREATE_CASE_FAILED,
        e
      );
    }
  }

  /** This method uses the storage to update model and saveMetaData
   * @param caseForUpdate
   * @throws error{CaseError} if there is a problem with updating the case */
  async update(caseForUpdate: CaseForUpdate): Promise<void> {
    const clonedModel = _.cloneDeep(caseForUpdate.data);
    try {
      const partialCases = await this.extractPartialsFromModel(
        caseForUpdate.data,
        caseForUpdate.saveMetaData.crfVersions
      );
      const partialsToBeSaved: Map<string, PartialStoredCase> = new Map();

      for (const [accessLevel, p] of partialCases.entries()) {
        let keyToBeUsed: string;

        if (this.coreConfiguration.encrypt) {
          try {
            // Try to get the encryption key for the access level
            keyToBeUsed = await this.keyStorage.getKey(caseForUpdate.caseID, accessLevel);
          } catch (error) {
            if (this.isKeyNotFoundError(error)) {
              // If the key doesn't exist, generate and save a new one
              const generatedKey = Crypt.generateKey();
              await this.keyStorage.saveKey(caseForUpdate.caseID, generatedKey, accessLevel);
              keyToBeUsed = await this.keyStorage.getKey(caseForUpdate.caseID, accessLevel);
            } else {
              throw new Error(
                `Unexpected error when loading encryption key for case ${caseForUpdate.caseID}, accessLevel ${accessLevel}`,
                { cause: error }
              );
            }
          }
          partialsToBeSaved.set(accessLevel, { accessLevel, data: await Crypt.encrypt(p, keyToBeUsed) });
        } else {
          partialsToBeSaved.set(accessLevel, { accessLevel, data: new Stringifier().serialize(p) });
        }
      }

      await this.caseStorage.updatePartials(caseForUpdate.caseID, Array.from(partialsToBeSaved.values()), {
        status: caseForUpdate.saveMetaData.status,
        crfVersions: caseForUpdate.saveMetaData.crfVersions,
        lastUpdate: caseForUpdate.saveMetaData.lastUpdate,
      });

      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseUpdated,
        new CaseUpdated(
          new CaseForUpdate(caseForUpdate.caseID, clonedModel, caseForUpdate.saveMetaData),
          this.currentContext.containerType
        )
      );
    } catch (e) {
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseUpdateFailed,
        new CaseUpdateFailed(clonedModel, caseForUpdate.caseID, e, this.currentContext.containerType)
      );
      throw new CaseError(
        `Found an error on updating case 
        ${
          caseForUpdate.caseID ? caseForUpdate.caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        }`,
        CaseErrorType.UPDATE_CASE_FAILED,
        e
      );
    }
  }

  /** This method uses the storage to delete the case
   * @param caseID
   * @throws error{CaseError} if there is a problem with deleting the case*/
  async deleteCase(caseID: string): Promise<void> {
    try {
      await this.caseStorage.delete(caseID);
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseDeleted,
        new CaseDeleted(caseID, this.currentContext.containerType)
      );
    } catch (e: unknown) {
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseDeleted,
        new CaseDeletionFailed(caseID, e, this.currentContext.containerType)
      );
      throw new CaseError(
        `Found an error on deleting case ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        }`,
        CaseErrorType.DELETE_CASE_FAILED,
        e
      );
    }
  }

  /** This method uses the storage to restore the case
   * @param caseID
   * @throws error{CaseError}*/
  async restoreCase(caseID: string): Promise<void> {
    try {
      await this.caseStorage.restore(caseID);
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseRestored,
        new CaseRestored(caseID, this.currentContext.containerType)
      );
    } catch (e) {
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CaseRestored,
        new CaseRestoringFailed(caseID, e, this.currentContext.containerType)
      );
      throw new CaseError(
        `Found an error on restoring case ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        }`,
        CaseErrorType.RESTORE_CASE_FAILED,
        e
      );
    }
  }

  /** This method uses the storage to permanently delete the case
   * @param caseID
   * @param motivation
   * @throws error{CaseError} if there is a problem with permanently deleting the case */
  async permanentlyDeleteCase(caseID: string, motivation: Motivations): Promise<void> {
    try {
      await this.caseStorage.permanentlyDelete(caseID);
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CasePermanentlyDeleted,
        new CasePermanentlyDeleted(caseID, motivation, this.currentContext.containerType)
      );
    } catch (e) {
      getContext(this.currentContext.context).dispatchEvent(
        ApplicationEvents.CasePermanentlyDeleted,
        new CasePermanentlyDeletionFailed(caseID, motivation, e, this.currentContext.containerType)
      );
      throw new CaseError(
        `Found an error on permanently deleting case ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        }`,
        CaseErrorType.PERMANENTLY_DELETE_CASE_FAILED,
        e
      );
    }
  }

  async lock(caseID: string): Promise<void> {
    return await this.manageLock(caseID, true);
  }

  async unlock(caseID: string): Promise<void> {
    await this.manageLock(caseID, false);
  }

  protected async manageLock(caseID: string, toLock: boolean): Promise<void> {
    toLock ? await this.caseStorage.lock(caseID) : await this.caseStorage.unlock(caseID);
    const model = (await this.getDataFor(caseID)).data;
    const caseMetaData = await this.getCaseMetaDataFor(caseID);
    const lockedEvent = toLock ? CaseLocked : CaseUnlocked;
    getContext(this.currentContext.context).dispatchEvent(
      toLock ? ApplicationEvents.CaseLocked : ApplicationEvents.CaseUnlocked,
      new lockedEvent(new CaseForUpdate(caseID, model, caseMetaData), this.currentContext.containerType)
    );
  }

  async freeze(caseID: string, accessLevelsToBeFroze?: string[]): Promise<void> {
    await this.manageFreeze(caseID, true, accessLevelsToBeFroze);
  }

  async unfreeze(caseID: string, accessLevelsToBeUnfroze?: string[]): Promise<void> {
    await this.manageFreeze(caseID, false, accessLevelsToBeUnfroze);
  }

  protected async manageFreeze(caseID: string, toFreeze: boolean, accessLevelsToBeChanged?: string[]): Promise<void> {
    const xCase = await this.getDataFor(caseID);
    const accessLevels = accessLevelsToBeChanged ?? Array.from(xCase.caseMetaData.frozenAccessLevels.keys());
    toFreeze
      ? await this.caseStorage.freeze(caseID, accessLevels)
      : await this.caseStorage.unfreeze(caseID, accessLevels);
    const model = xCase.data;
    const freezeEvent = toFreeze ? CaseFreezed : CaseUnfreezed;
    getContext(this.currentContext.context).dispatchEvent(
      ApplicationEvents.CaseUpdated,
      new freezeEvent(new CaseForUpdate(caseID, model, xCase.caseMetaData), this.currentContext.containerType)
    );
  }

  async getSingleCaseHistory(caseID: string) {
    return await this.caseStorage.getCaseHistory(caseID);
  }

  protected async extractPartialsFromModel(model: any, crfVersions: Map<string, Version>): Promise<Map<string, any>> {
    try {
      // Copy the model to avoid side effects
      const copied_model = _.cloneDeep(model);
      const partialCases: Map<string, any> = new Map();
      const levels: AccessLevelsTree = {};
      // Create access levels for each CRF, they will be saved all together
      crfVersions.forEach((crfVersion, crfName) => {
        const crfLevels = this.crfService.getAccessLevelsForCRF(crfName, crfVersion);
        _.merge(levels, crfLevels);
      });
      // Depth First Search iterator on all variables with an accessLevel
      await dfs(levels, (_currentNode, variable, accessLevel) => {
        if (this.userService.hasWriteAccessRights(accessLevel)) {
          if (!partialCases.has(accessLevel)) {
            partialCases.set(accessLevel, {});
          }
          const levelModel = partialCases.get(accessLevel) as Record<string, any>;
          // We copy the variable from model to the relative partial
          _.set(levelModel, variable, _.get(copied_model, variable));
        }
        // Done with this variable, we remove it, so it doesn't remain in "public" partial
        _.unset(copied_model, variable);
      });
      // We put in "public" what remains from model (all variables without accessLevel indicated)
      partialCases.set(AccessLevels.public, copied_model);
      return partialCases;
    } catch (e) {
      throw new CaseError('Found an error on extract partials from model', CaseErrorType.EXTRACTING_PARTIALS_FAILED, e);
    }
  }

  // Helper method to check if an error indicates a missing key
  private isKeyNotFoundError(error: any): boolean {
    return error.response?.status === 404 || error.message?.includes?.('Key not found');
  }
}

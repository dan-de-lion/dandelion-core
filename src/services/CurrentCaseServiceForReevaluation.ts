import { getContext } from '@/ContainersConfig';
import { CaseOpenedForReevaluation } from '@/application-events/CaseOpenedForReevaluation';
import { CaseClosedForReevaluation } from '@/application-events/CaseClosedForReevaluation';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import CurrentCaseService from '@/services/CurrentCaseService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { substituteModelInPlace } from '@/utils/substituteModelInPlace';
import { CaseMetaData } from '@/entities/CaseMetaData';

export class CurrentCaseServiceForReevaluation extends CurrentCaseService {
  async open(caseID: string, model: any, caseMetaData: CaseMetaData, shouldLock = true) {
    this.currentCRFVersions.value = await this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase(
      model,
      caseMetaData
    );
    if (shouldLock) {
      await this.caseService.lock(caseID);
    }

    this.caseStatusService.startStatusService(Array.from(this.currentCRFVersions.value.keys()));
    //this should be the last operation because watches are fired by it
    this.currentCaseID.value = caseID;
    substituteModelInPlace(this.currentModel, model);
    this.setupWatchOnModel();

    const event = new CaseOpenedForReevaluation(caseID, this.contextService.containerType);
    getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseOpenedForReevaluation, event);
  }

  async terminateEditing(shouldLock = true): Promise<void> {
    const event = new CaseClosedForReevaluation(this.currentCaseID.value ?? '');
    const caseID = this.currentCaseID.value;
    if (!caseID) {
      throw new ApplicationError('Case ID is undefined for terminate the editing', AppErrorType.CASE_ID_UNDEFINED);
    }
    this.currentCaseID.value = undefined;
    this.resetModel();
    this.caseStatusService.reset();
    this.unmountWatchOnModel();
    getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseClosed, event);
    if (shouldLock) {
      await this.caseService.unlock(caseID);
    }
    return Promise.resolve();
  }
}

import { getContainer } from '@/ContainersConfig';
import { inject, injectable } from 'inversify';
import { NamedBindings } from '@/types/NamedBindings';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import CaseReevaluationService from '@/services/CaseReevaluationService';
import { CachedCaseListElement } from '@/services/CaseListService';
import { MagicNumbers } from '@/types/MagicNumbers';
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';

@injectable()
export class SchedulerService {
  constructor(
    @inject(NamedBindings.CoreConfiguration)
    protected coreConfiguration: CoreConfigurationInterface
  ) {}

  startPeriodicReevaluation(periodInMinutes = MagicNumbers.periodicallyRunReevaluationMinutes) {
    this.coreConfiguration.listOfCaseTypes.forEach((caseType) => {
      const container = getContainer(caseType.caseName);
      const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
      const reevaluationService = container.get(NamedBindings.CaseReevaluationService) as CaseReevaluationService;
      setInterval(async () => {
        try {
          const allCases = Array.from(caseListService.allCases) as Array<CachedCaseListElement>;
          await reevaluationService.run(allCases);
        } catch (error) {
          console.error('Error during periodic reevaluation:', error);
        }
      }, periodInMinutes * 60 * 1000);
    });
  }
}

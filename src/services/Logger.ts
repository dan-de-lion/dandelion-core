import { inject, injectable } from 'inversify';
import { toast, ToastType } from 'vue3-toastify';
import { DandelionService } from './DandelionService';
import type TranslationService from './TranslationService';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export class ConsoleLogger extends DandelionService implements LoggerInterface {
  constructor(@inject(NamedBindings.TranslationService) protected ts: TranslationService) {
    super();
  }

  public log(message: string, viewOnScreen?: boolean) {
    this.displayLog(message, 'info', viewOnScreen);
  }

  public logSuccess(message: string, viewOnScreen?: boolean) {
    this.displayLog(message, 'success', viewOnScreen);
  }

  public logWarning(message: string, viewOnScreen?: boolean) {
    this.displayLog(message, 'warning', viewOnScreen);
  }

  public logError(message: string, viewOnScreen?: boolean) {
    this.displayLog(message, 'error', viewOnScreen);
  }

  private displayLog(message: string, type: ToastType, viewOnScreen?: boolean) {
    const translatedMessage = this.ts.t(message);
    switch (type) {
      case 'info':
      case 'success':
        console.log(translatedMessage);
        break;
      case 'warning':
        console.warn(translatedMessage);
        break;
      case 'error':
        console.error(translatedMessage);
        break;
    }
    if (viewOnScreen) {
      toast(translatedMessage, {
        autoClose: 2500,
        type,
        theme: 'colored',
        transition: 'slide',
        dangerouslyHTMLString: false,
        hideProgressBar: false,
      });
    }
  }
}

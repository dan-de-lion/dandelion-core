import { injectable } from 'inversify';
import { v4 } from 'uuid';

@injectable()
export class DandelionService {
  readonly id: string;
  readonly isDebug: boolean;

  constructor() {
    /* --- Enable this when we refactor inverisy.config ---
    const sharedCoreConfig = getContainer(Contexts.SHARED).get(NamedBindings.CoreConfiguration) as CoreConfiguration;*/
    this.isDebug = false;
    this.id = v4();
    if (this.isDebug) {
      console.log(`${this.constructor.name} ID: ${this.id}`);
    }
  }
}

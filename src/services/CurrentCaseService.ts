import { inject, injectable } from 'inversify';
import _, { cloneDeep, isEqual } from 'lodash';
import { v4 as uuid } from 'uuid';
import { computed, type DeepReadonly, reactive, readonly, ref, type Ref, type UnwrapNestedRefs } from 'vue';
import { DandelionService } from './DandelionService';
import { watchThrottled } from '@vueuse/core';
import { getContext } from '@/ContainersConfig';
import { CaseClosed } from '@/application-events/CaseClosed';
import { CaseOpenedForEditing } from '@/application-events/CaseOpenedForEditing';
import { CaseOpeningFailed } from '@/application-events/CaseOpeningFailed';
import { CaseForUpdate } from '@/entities/Cases';
import type { Version } from '@/entities/CRF';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import { FlowError, FlowErrorType } from '@/errors/FlowError';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type { CaseListService } from '@/services/CaseListService';
import type CurrentContextService from '@/services/CurrentContextService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { NamedBindings } from '@/types/NamedBindings';
import { DANDELION_MODEL_UUID, substituteModelInPlace } from '@/utils/substituteModelInPlace';
import Helpers from '@/utils/Helpers';
import { CrfStatus } from '@/services/CaseStatusService';

@injectable()
export default class CurrentCaseService extends DandelionService implements CurrentCaseServiceInterface {
  protected currentModel;
  protected previousModel = {};
  protected previousStatuses: Array<[string, CrfStatus]> = [];
  protected currentCaseID: Ref<string | undefined> = ref(undefined);
  protected currentCRFVersions: Ref<Map<string, Version>> = ref(new Map());
  protected previousCrfVersions: Map<string, Version> = new Map();
  protected currentFrozenMap: Ref<Map<string, boolean>> = ref(new Map());
  protected watchOnModel: (() => void) | null = null;
  public isCaseOpened = computed(() => this.currentCaseID.value !== undefined);
  public isModelChanged = ref(false);

  constructor(
    @inject(NamedBindings.CurrentContextService)
    protected contextService: CurrentContextService,
    @inject(NamedBindings.CaseService)
    protected caseService: CaseServiceInterface,
    @inject(NamedBindings.CRFVersionSelectionStrategy)
    protected crfVersionSelectionStrategy: CRFVersionSelectionStrategyInterface,
    @inject(NamedBindings.CaseStatusService)
    protected caseStatusService: CaseStatusServiceInterface,
    @inject(NamedBindings.CaseListService)
    protected caseListService: CaseListService,
    @inject(NamedBindings.CoreConfiguration) coreConfiguration: CoreConfigurationInterface,
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.CRFService) protected crfService: CRFServiceInterface
  ) {
    super();
    this.currentModel = reactive(coreConfiguration.debug ? { [DANDELION_MODEL_UUID]: uuid() } : {});
  }

  setupWatchOnModel() {
    this.isModelChanged.value = false;
    this.watchOnModel = watchThrottled(
      this.currentModel,
      () => (this.isModelChanged.value = !_.isEqual(this.previousModel, this.currentModel)),
      { throttle: 500 }
    );
  }

  unmountWatchOnModel() {
    this.isModelChanged.value = false;
    this.watchOnModel?.();
    this.watchOnModel = null;
  }

  async terminateEditing() {
    const event = new CaseClosed(this.currentCaseID.value ?? '', this.contextService.containerType);
    const caseID = this.currentCaseID.value;
    if (!caseID) {
      throw new ApplicationError('Case ID is undefined for terminate the editing', AppErrorType.CASE_ID_UNDEFINED);
    }
    this.currentCaseID.value = undefined;
    this.resetModel();
    this.caseStatusService.reset();
    this.unmountWatchOnModel();
    getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseClosed, event);
    await this.caseService.unlock(caseID);
    return Promise.resolve();
  }

  /**
   * Opens a case for editing.
   * @param caseID
   * @param _model ignored, the model is taken from the service
   * @throws {ApplicationError} if the case is not found
   * @throws {ApplicationError} if there is no Version available for the CRF
   */
  async open(caseID: string, _model = {}, _caseMetaData = {}) {
    // If someone has opened a case, without terminate to edit the previous one, we need to terminate it
    if (this.currentCaseID.value !== undefined) {
      await this.terminateEditing();
    }
    await this.caseService.lock(caseID);

    try {
      const defaultCase = await this.caseService.getDataFor(caseID);
      const frozenAccessLevels = new Map<string, boolean>();
      defaultCase?.caseMetaData?.frozenAccessLevels?.forEach((isFrozen, accessLevel) =>
        frozenAccessLevels.set(accessLevel, isFrozen)
      );
      substituteModelInPlace(this.currentModel, defaultCase.data);

      this.setupWatchOnModel();
      this.currentFrozenMap.value = frozenAccessLevels;
      const caseMetaData = await this.caseService.getCaseMetaDataFor(caseID);
      const crfVersions = await this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase(
        defaultCase.data,
        caseMetaData
      );

      if (!isEqual(this.currentCRFVersions.value, crfVersions)) {
        this.previousCrfVersions = crfVersions;
        this.currentCRFVersions.value = crfVersions;
      }

      this.caseStatusService.startStatusService(Array.from(crfVersions.keys()));
      this.previousModel = cloneDeep(this.currentModel);
      this.previousStatuses = Array.from(caseMetaData.status.entries());
      //this should be the last operation because watches are fired by it
      this.currentCaseID.value = caseID;

      const event = new CaseOpenedForEditing(caseID, this.contextService.containerType);
      getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseOpenedForEditing, event);
    } catch (e: unknown) {
      const event = new CaseOpeningFailed(caseID, e, this.contextService.containerType);
      getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseOpeningFailed, event);
      throw e;
    }
  }

  canRead(variable: string, crfName: string, crfVersion: Version): boolean {
    return this.userService.canRead(variable, crfName, crfVersion);
  }

  canWrite(variable: string, crfName: string, crfVersion: Version): boolean {
    const accessLevels = this.crfService.getAccessLevelsForCRF(crfName, crfVersion);
    const currentAccessLevel = Helpers.searchMostSpecificVariablePrefix(variable, accessLevels);
    if (
      this.currentFrozenMap.value.has(currentAccessLevel) &&
      this.currentFrozenMap.value.get(currentAccessLevel) === true
    ) {
      return false;
    }
    return this.userService.canWrite(variable, crfName, crfVersion);
  }

  /**
   * Saves the current case.
   * @throws {FlowError} if there is no current case
   * @throws {FlowError} if the current case is not found
   * @throws {CRFError} if there is no Version available for the CRF
   * @throws {CaseError} if there is a problem in updating the case
   */
  async save() {
    if (!this.currentCaseID.value || !this.currentCRFVersions) {
      throw new FlowError('No current case found in CurrentCaseService', FlowErrorType.CURRENT_CASE_NOT_FOUND);
    }
    const caseID = this.currentCaseID.value;
    const model = this.currentModel;
    // force the update of the MapForStatus
    this.caseStatusService.updateStatus();
    const canWriteStatuses = Array.from(this.caseStatusService.getStatusMap().value.entries()).filter(
      ([accessLevel]) => this.userService.hasWriteAccessRights(accessLevel) || accessLevel === '*'
    );
    const status = new Map(canWriteStatuses);
    // merge with the old statuses
    for (const [variable, crfStatus] of this.previousStatuses) {
      if (!status.has(variable)) {
        status.set(variable, crfStatus);
      }
    }
    const caseMetaData = await this.caseService.getCaseMetaDataFor(caseID);
    const referenceDateChanged = this.crfVersionSelectionStrategy.getReferenceDateIfChanged(
      this.previousModel,
      model,
      caseMetaData
    );
    const updatedCrfVersions = referenceDateChanged
      ? await this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase(model, caseMetaData)
      : this.currentCRFVersions.value;
    const lastUpdate = new Date();
    await this.caseService.update(
      new CaseForUpdate(caseID, model, { crfVersions: updatedCrfVersions, status, lastUpdate })
    );
    await this.caseListService.updateCase(caseID, lastUpdate);
    if (referenceDateChanged) {
      await this.caseListService.changedReferenceDate(caseID, new Date(referenceDateChanged), lastUpdate);
    }
  }

  getCurrentCase(): DeepReadonly<UnwrapNestedRefs<Ref<string | undefined>>> {
    return readonly(this.currentCaseID);
  }

  updateModel(newModel: Record<string, any>) {
    substituteModelInPlace(this.currentModel, newModel);
    this.previousModel = cloneDeep(this.currentModel);
  }

  resetModel() {
    this.updateModel({});
  }

  getCrfVersions(): Readonly<Ref<ReadonlyMap<string, Version>>> {
    return readonly(this.currentCRFVersions);
  }

  getPreviousCrfVersions(): Readonly<ReadonlyMap<string, Version>> {
    return readonly(this.previousCrfVersions);
  }

  getFrozenAccessLevels(): Readonly<Ref<ReadonlyMap<string, boolean>>> {
    return readonly(this.currentFrozenMap);
  }

  getModel(): any {
    return this.currentModel;
  }

  getPreviousModel(): any {
    return this.previousModel;
  }

  getAccessLevelForVariable(variableName: string) {
    return this.crfService.getAccessLevelForVariable(variableName, this.currentCRFVersions.value);
  }
}

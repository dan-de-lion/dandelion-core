import { injectable } from 'inversify';
import _, { cloneDeep } from 'lodash';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import { CaseClosedForCreation } from '@/application-events/CaseClosedForCreation';
import { CaseOpenedForCreation } from '@/application-events/CaseOpenedForCreation';
import { FlowError, FlowErrorType } from '@/errors/FlowError';
import { Case } from '@/entities/Cases';
import { CaseInfo, CaseMetaData } from '@/entities/CaseMetaData';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import type CaseReevaluationService from '@/services/CaseReevaluationService';
import CurrentCaseService from '@/services/CurrentCaseService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { NamedBindings } from '@/types/NamedBindings';
import { substituteModelInPlace } from '@/utils/substituteModelInPlace';
import Helpers from '@/utils/Helpers';
import { CRFError, CRFErrorType } from '@/errors/CRFError';

@injectable()
export default class CurrentCaseServiceForNewCase extends CurrentCaseService {
  /**
   * Opens a case for editing.
   * @param caseID
   * @throws {ApplicationError} if the case is not found
   * @throws {ApplicationError} if there is no Version available for the CRF
   */
  async open(caseID: string, model: any) {
    // If someone has opened a case, without terminate to edit the previous one, we need to terminate it
    if (this.currentCaseID.value !== undefined) {
      await this.terminateEditing();
    }
    this.currentCRFVersions.value = await this.crfVersionSelectionStrategy.getUpdatedCRFVersionsByDate(new Date());
    this.caseStatusService.startStatusService(Array.from(this.currentCRFVersions.value.keys()));
    //this should be the last operation because watches are fired by it
    this.currentCaseID.value = caseID;
    substituteModelInPlace(this.currentModel, model ?? {});
    const event = new CaseOpenedForCreation(caseID, this.contextService.containerType);
    getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseOpenedForCreation, event);
  }

  async save() {
    if (!this.currentCaseID.value || !this.currentCRFVersions) {
      throw new FlowError(
        'No current case found in CurrentCaseServiceForNewCase',
        FlowErrorType.CURRENT_CASE_NOT_FOUND
      );
    }
    const caseType = this.contextService.getCaseConfiguration();
    const referenceDate = Helpers.getObjByPath(caseType.referenceDateVariableName, _.cloneDeep(this.currentModel));
    if (!referenceDate) {
      throw new CRFError(
        `The referenceDate in Case configuration for CRF ${caseType.caseName} is not present`,
        CRFErrorType.REFERENCE_DATE_NOT_FOUND
      );
    }
    const referenceDateAsDate = new Date(referenceDate);
    if (!(referenceDateAsDate instanceof Date) || Number.isNaN(referenceDateAsDate.getTime())) {
      throw new CRFError(
        `The referenceDate in Case configuration for CRF ${caseType.caseName} is not correct`,
        CRFErrorType.REFERENCE_DATE_WRONG_FORMAT
      );
    }
    const caseReevaluationService: CaseReevaluationService = getContainer(
      this.contextService.context,
      ContainerTypes.REEVALUATION
    ).get(NamedBindings.CaseReevaluationService);
    const caseID = this.currentCaseID.value;
    const creationDate = new Date();
    const data = cloneDeep(this.currentModel);
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseInfo: CaseInfo = {
      caseID,
      creationDate,
      centreCode,
      caseType: this.contextService.getCaseConfiguration().caseName,
    };
    const caseForUpdate = await caseReevaluationService.reevaluateCase(
      new Case(caseID, data, caseInfo as CaseMetaData),
      false
    );
    await this.caseService.create(
      new Case(caseForUpdate.caseID, caseForUpdate.data, {
        ...caseInfo,
        ...caseForUpdate.saveMetaData,
        frozenAccessLevels: new Map(),
      })
    );
    await this.caseListService.addCase(caseID, creationDate);
    if (caseType.referenceDateVariableName) {
      await this.caseListService.changedReferenceDate(caseID, referenceDateAsDate, creationDate);
    }
  }

  async terminateEditing() {
    const event = new CaseClosedForCreation(this.currentCaseID.value ?? '');
    const caseID = this.currentCaseID.value;
    if (!caseID) {
      throw new ApplicationError('Case ID is undefined for terminate the editing', AppErrorType.CASE_ID_UNDEFINED);
    }
    this.currentCaseID.value = undefined;
    this.resetModel();
    this.caseStatusService.reset();
    getContext(this.contextService.context).dispatchEvent(ApplicationEvents.CaseClosed, event);
    return Promise.resolve();
  }
}

import { inject, injectable } from 'inversify';
import _ from 'lodash';
import { DandelionService } from './DandelionService';
import { CaseMetaData } from '@/entities/CaseMetaData';
import type { CRFVersionValidity, Version } from '@/entities/CRF';
import type { CrfStartAndEndDates, CrfWithValidity } from '@/entities/CrfWithValidity';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import { ActivationPeriodsForCrfStorageInterface } from '@/interfaces/ActivationPeriodsForCrfStorageInterface';
import type { CRFConfigurationInterface, CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import { NamedBindings } from '@/types/NamedBindings';
import Helpers from '@/utils/Helpers';

@injectable()
export class CRFVersionSelectionStrategy extends DandelionService implements CRFVersionSelectionStrategyInterface {
  constructor(
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfiguration,
    @inject(NamedBindings.CRFService) protected crfService: CRFServiceInterface,
    @inject(NamedBindings.CurrentContextService) protected currentContextService: CurrentContextService,
    @inject(NamedBindings.ActivationPeriodForCentreCodeStorage)
    protected activationPeriodsForCrfStorage: ActivationPeriodsForCrfStorageInterface,
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface
  ) {
    super();
  }

  /** This method uses the storage to get the CRF versions for the given model
   * @param model the model to get the CRF versions for
   * @throws error{ApplicationError} if there is a problem with getting CRF versions i.e. no CRF Version is found */
  public async getUpdatedCRFVersionsByCase(model: any, caseMetaData: CaseMetaData): Promise<Map<string, Version>> {
    const caseConfiguration = this.currentContextService.getCaseConfiguration();
    this.failIfCaseConfigurationNotInCoreConfiguration(caseConfiguration);
    const referenceDate = this.getReferenceDate(caseConfiguration.referenceDateVariableName, model, caseMetaData);
    return this.getUpdatedCRFVersionsByDate(referenceDate);
  }

  public async getUpdatedCRFVersionsByDate(referenceDate: Date): Promise<Map<string, Version>> {
    const caseConfiguration = this.currentContextService.getCaseConfiguration();
    try {
      //Check validity of Crfs
      const validCrfConfigurations: CRFConfigurationInterface[] = await this.getValidCrfConfigurationsByReferenceDate(
        caseConfiguration,
        referenceDate
      );
      // get an Array of CrfVersions and CrfNames
      const validCrfsWithVersion = await Promise.all(
        validCrfConfigurations.map(async (crfConfiguration) => ({
          version: await this.getVersions(referenceDate, crfConfiguration.crfName),
          crfName: crfConfiguration.crfName,
        }))
      );
      //convert to Map
      return validCrfsWithVersion.reduce(
        (result, { version, crfName }) => result.set(crfName, version),
        new Map<string, Version>()
      );
    } catch (e) {
      throw new CRFError('Error catched on getting updated CRF Versions', CRFErrorType.CRF_VERSIONS_NOT_FOUND, e);
    }
  }

  protected failIfCaseConfigurationNotInCoreConfiguration(caseConfiguration: CaseConfiguration) {
    const caseTypeFound = this.coreConfiguration.listOfCaseTypes.some(
      (caseType) => caseType.caseName === caseConfiguration.caseName
    );
    if (!caseTypeFound) {
      throw new ApplicationError(
        `Case type ${caseConfiguration.caseName} not found in listOfCaseTypes`,
        AppErrorType.CASE_TYPE_NOT_FOUND
      );
    }
  }

  protected getReferenceDate(variableName: string, model: any, _caseMetaData: CaseMetaData): Date {
    return this.checkReferenceDate(new Date(Helpers.getObjByPath(variableName, model)));
  }

  protected checkReferenceDate(referenceDate: Date): Date {
    const caseConfiguration = this.currentContextService.getCaseConfiguration();
    if (!referenceDate) {
      throw new CRFError(
        `The referenceDate in Case configuration for CRF 
      ${caseConfiguration.caseName} is not present`,
        CRFErrorType.REFERENCE_DATE_NOT_FOUND
      );
    }
    if (!(referenceDate instanceof Date) || Number.isNaN(referenceDate.getTime())) {
      throw new CRFError(
        `The referenceDate in Case configuration for CRF ${caseConfiguration.caseName} is not correct`,
        CRFErrorType.REFERENCE_DATE_WRONG_FORMAT
      );
    }
    return referenceDate;
  }

  public getReferenceDateIfChanged(oldModel: any, newModel: any, caseMetaData: CaseMetaData): Date | false {
    const caseConfiguration = this.currentContextService.getCaseConfiguration();
    const newReferenceDate = this.getReferenceDate(caseConfiguration.referenceDateVariableName, newModel, caseMetaData);
    return this.getReferenceDate(caseConfiguration.referenceDateVariableName, oldModel, caseMetaData).getTime() !==
      newReferenceDate.getTime()
      ? newReferenceDate
      : false;
  }

  public async getCRFConfigurationByCase(model: any, caseMetaData: CaseMetaData): Promise<CRFConfigurationInterface[]> {
    const caseConfiguration = this.currentContextService.getCaseConfiguration();
    this.failIfCaseConfigurationNotInCoreConfiguration(caseConfiguration);
    const referenceDate = this.getReferenceDate(caseConfiguration.referenceDateVariableName, model, caseMetaData);
    return await this.getValidCrfConfigurationsByReferenceDate(caseConfiguration, referenceDate);
  }

  public async getCRFConfigurationsByCentreCode(inputCentreCode?: string): Promise<CRFConfigurationInterface[]> {
    const centreCode = inputCentreCode ?? this.userService?.getCurrentUser().value?.centre ?? '';
    return (
      (await this.activationPeriodsForCrfStorage.getCrfsWithValidityForCentreCode(centreCode)).map(
        (c: CrfWithValidity) => ({ crfName: c.crfName })
      ) ?? []
    );
  }

  protected async getValidCrfConfigurationsByReferenceDate(
    caseConfiguration: CaseConfiguration,
    referenceDate: Date
  ): Promise<CRFConfigurationInterface[]> {
    const allCrfs = caseConfiguration.getAllCRFs();
    const centreCode = this.userService.getCurrentUser().value?.centre || '*';
    const crfsForCentre = await this.activationPeriodsForCrfStorage.getCrfsWithValidityForCentreCode(centreCode);
    // Filter valid CRFs present in the JSON
    return allCrfs.filter((crfConfiguration) => {
      const crfWithValidity = crfsForCentre.find((c: CrfWithValidity) => crfConfiguration.crfName === c.crfName);
      // Check validity
      if (crfWithValidity) {
        return this.areSomeValid(crfWithValidity, referenceDate);
      }
      // If the CRF does not exist in the JSON, consider it NOT valid
      // That is because otherwise there's no way to exclude a CRF from a
      return false;
    });
  }

  protected areSomeValid(crfWithValidity: CrfWithValidity, referenceDate: Date): boolean {
    if (crfWithValidity.isAlwaysActive == true) {
      return true;
    }
    return crfWithValidity.crfValidityArray.some((validity) => this.isValid(validity, referenceDate));
  }

  protected isValid(validity: CrfStartAndEndDates, referenceDate: Date) {
    return (
      (validity.endDate && referenceDate >= validity.startDate && referenceDate <= validity.endDate) ||
      (!validity.endDate && referenceDate >= validity.startDate)
    );
  }

  /** This method returns the version of the CRF for the given data
   * @param data
   * @param crfName
   * @param caseTypeName
   * @returns {Version}
   * @throws error{CRFError} if no version is found */
  protected async getVersions(referenceDate: Date, crfName: string): Promise<Version> {
    const listOfCrfVersions: CRFVersionValidity[] = await this.crfService.getCRFActiveVersions(crfName);
    if (listOfCrfVersions.length < 1) {
      throw new CRFError(`No active version found for CRF ${crfName}`, CRFErrorType.ACTIVE_VERSIONS_NOT_FOUND);
    }
    const orderedList = listOfCrfVersions.sort(this.compareByStartDate);
    const lastMajor: CRFVersionValidity = orderedList[orderedList.length - 1];
    // assign the last version if no version is found
    const result =
      _.findLast(
        orderedList,
        (item, index) =>
          item.startDate < referenceDate &&
          (index === orderedList.length - 1 || orderedList[index + 1].startDate >= referenceDate)
      ) ?? lastMajor;
    return result.lastAvailable;
  }

  protected compareByStartDate(x: CRFVersionValidity, y: CRFVersionValidity): number {
    if (x.startDate < y.startDate) {
      return -1;
    }
    if (x.startDate > y.startDate) {
      return 1;
    }
    return 0;
  }
}

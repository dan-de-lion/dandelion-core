import { injectable } from 'inversify';
import type { KeyStorageInterface, MultiKeysDTO, MultiKeysResponse } from '@/interfaces/KeyStorageInterface';
import { StorageError, StorageErrorType } from '@/errors/StorageError';

@injectable()
export class InMemoryKeyStorage implements KeyStorageInterface {
  protected storage = new Map();

  async saveKey(caseID: string, key: string, accessLevel: string): Promise<void> {
    if (!this.storage.has(caseID)) {
      this.storage.set(caseID, new Map());
    }
    this.storage.get(caseID).set(accessLevel, key);
  }

  async getKey(caseID: string, accessLevel: string): Promise<string> {
    if (!this.storage.has(caseID) || !this.storage.get(caseID).has(accessLevel)) {
      throw new StorageError(
        `Cannot find key for ${caseID}, accessLevel ${accessLevel}`,
        StorageErrorType.GETTING_KEY_FAILED
      );
    }
    return this.storage.get(caseID).get(accessLevel) ?? '';
  }

  async getMultipleKeys(caseIDs: string[]): MultiKeysResponse {
    const successCases: Array<MultiKeysDTO> = [];
    const maybeKeys = caseIDs.map((caseID) => ({ caseID, keysMap: this.storage.get(caseID) }));
    maybeKeys.forEach((mk) => {
      if (mk.keysMap && mk.keysMap instanceof Map) {
        mk.keysMap.forEach((key, accessLevel) => successCases.push({ caseID: mk.caseID, accessLevel, key }));
      }
    });
    return {
      successCases,
      failedCases: maybeKeys
        .filter((k) => k.keysMap === undefined)
        .map(({ caseID }) => ({ caseID, accessLevel: '*', error: `Keys not found` })),
    };
  }
}

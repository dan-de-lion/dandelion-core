import { injectable } from 'inversify';
import { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { ZsourceEvents, ZsourceEventsSchema } from '@/utils/ValidationSchemas/SourceEventSchema';
import { parseElementWithZod, ZodSchemas } from '@/utils/ValidationSchemas/handleZod';

@injectable()
export class LocalSynchEventsStorage {
  protected localStorage = window.localStorage;

  constructor(protected baseKey: string = 'synchedEvents') {}

  async loadAllEvents(centreCode: string, caseType: string): Promise<Array<SourceEventForList>> {
    const storages = Object.keys(localStorage);
    const eventKey = `${this.baseKey}-${centreCode}-${caseType}`;
    const eventTypes = storages.filter((storage) => storage.startsWith(eventKey));
    const events: Array<SourceEventForList> = [];

    for (const eventType of eventTypes) {
      const parsedEvents: ZsourceEvents = parseElementWithZod(
        ZsourceEventsSchema,
        ZodSchemas.SOURCE_EVENT,
        JSON.parse(localStorage.getItem(eventType) ?? '[]')
      );
      events.push(...parsedEvents);
    }
    return events.sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime());
  }

  async saveEvent(event: SourceEventForList, centreCode: string, caseType: string): Promise<void> {
    let events: ZsourceEvents = [];
    const eventKey = `${this.baseKey}-${centreCode}-${caseType}`;
    const loadedEvents = localStorage.getItem(eventKey);
    if (loadedEvents) {
      events = parseElementWithZod(ZsourceEventsSchema, ZodSchemas.SOURCE_EVENT, JSON.parse(loadedEvents)) ?? [];
    }
    events.push(event);
    this.localStorage.setItem(eventKey, JSON.stringify(events));
  }
}

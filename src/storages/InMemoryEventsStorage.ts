import { injectable } from 'inversify';
import type { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import { DandelionService } from '@/services/DandelionService';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';

@injectable()
export class InMemoryEventsStorage extends DandelionService implements EventsStorageInterface {
  protected eventMap: Map<string, Map<string, Map<string, SourceEventForList>>> = new Map();
  protected snapshotsMap: Map<string, Map<string, Array<CaseListSnapshot>>> = new Map();

  constructor() {
    super();
  }

  async getLastSnapshotAndEvents(centreCode: string, caseType: string): Promise<SnapshotAndEvents> {
    const events = await this.loadAllEvents(centreCode, caseType);
    const snapshots = this.snapshotsMap.get(centreCode)?.get(caseType) ?? [];
    // if there is no snapshot, return all the events
    if (snapshots.length === 0) {
      return {
        events,
        snapshot: { cachedCaseList: [], date: new Date(''), numberOfEvents: events.length },
      };
    }
    // get the snapshot with the higher lastUpdate
    const lastSnapshot = snapshots.reduce((prev, current) => (prev.date > current.date ? prev : current));
    // get the events after the lastUpdate of the snapshot
    const eventsAfterSnapshot = events.filter((event) => event.timestamp > lastSnapshot.date);
    return { events: eventsAfterSnapshot, snapshot: lastSnapshot };
  }

  async saveSnapshot(snapshot: CaseListSnapshot, centreCode: string, caseType: string) {
    const snapshots = this.snapshotsMap.get(centreCode)?.get(caseType) ?? [];
    snapshot.date = new Date();
    snapshots.push(snapshot);
    this.snapshotsMap.set(centreCode, new Map([[caseType, snapshots]]));
  }

  async getEvent(caseID: string, centreCode: string, caseType: string): Promise<SourceEventForList | undefined> {
    const centreCodeMap = this.eventMap.get(centreCode);
    if (!centreCodeMap) {
      return;
    }
    const caseTypeMap = centreCodeMap.get(caseType);
    if (!caseTypeMap) {
      return;
    }
    const event = caseTypeMap.get(caseID);
    return event;
  }

  async loadAllEvents(centreCode: string, caseType: string): Promise<Array<SourceEventForList>> {
    const centreCodeMap = this.eventMap.get(centreCode);
    if (!centreCodeMap) {
      return [];
    }
    const caseTypeMap = centreCodeMap.get(caseType);
    if (!caseTypeMap) {
      return [];
    }
    const events = Array.from(caseTypeMap.values());
    const sortedEvents = events.sort((a, b) => {
      return a.timestamp.getTime() - b.timestamp.getTime();
    });
    return sortedEvents;
  }

  async saveEvent(event: SourceEventForList, centreCode: string, caseType: string) {
    if (!this.eventMap.has(centreCode)) {
      this.eventMap.set(centreCode, new Map());
    }
    const centreCodeMap = this.eventMap.get(centreCode);
    if (!centreCodeMap?.has(caseType)) {
      centreCodeMap?.set(caseType, new Map());
    }
    const caseTypeMap = centreCodeMap?.get(caseType);
    caseTypeMap?.set(event.id ?? 'noID', event);
  }

  reset() {
    this.eventMap = new Map();
    this.snapshotsMap = new Map();
  }
}

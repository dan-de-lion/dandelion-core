import { injectable } from 'inversify';
import type { CaseMetaData, SaveMetaData } from '@/entities/CaseMetaData';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type {
  CaseStorageInterface,
  MultiCaseMetaDTO,
  MultiCaseMetaResponse,
  MultiPartialsDTO,
  MultiPartialsResponse,
} from '@/interfaces/CaseStorageInterface';
import { DandelionService } from '@/services/DandelionService';
import { MagicNumbers } from '@/types/MagicNumbers';

@injectable()
export class InMemoryCaseStorage extends DandelionService implements CaseStorageInterface {
  caseListMap: Map<string, Map<string, PartialStoredCase>> = new Map();
  caseMetaDataMap: Map<string, CaseMetaData> = new Map();
  deletedCaseListMap: Map<string, Map<string, PartialStoredCase>> = new Map();
  historyMap: Map<string, HistoryPartialStoredCase[]> = new Map();

  constructor() {
    super();
  }

  async getAllCaseMetaData(_caseType: string): Promise<CaseMetaData[]> {
    return Array.from(this.caseMetaDataMap.values());
  }

  async getCaseMetaData(caseID: string): Promise<CaseMetaData> {
    return (
      this.caseMetaDataMap.get(caseID) ??
      (() => {
        throw new StorageError(
          `No case with this ID: ${caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID)}`,
          StorageErrorType.CASE_ID_NOT_FOUND
        );
      })()
    );
  }

  async getMultipleCaseMetaData(caseIDs: string[]): MultiCaseMetaResponse {
    const maybeCaseMetadata = caseIDs.map((caseID) => ({ caseID, caseMetaData: this.caseMetaDataMap.get(caseID) }));
    return {
      successCases: maybeCaseMetadata.filter(
        (m): m is MultiCaseMetaDTO => m !== undefined && m.caseMetaData !== undefined
      ),
      failedCases: maybeCaseMetadata
        .filter((m) => m?.caseMetaData === undefined)
        .map(({ caseID }) => ({ caseID, error: `Metadata not found` })),
    };
  }

  async getPartials(caseID: string): Promise<Array<PartialStoredCase>> {
    return Array.from(
      this.getCaseOrThrow(
        caseID,
        StorageErrorType.PARTIALS_NOT_FOUND,
        'Failed to get partials, no case with this ID:'
      ).values()
    );
  }

  async getMultiplePartials(caseIDs: string[]): MultiPartialsResponse {
    const maybePartials = caseIDs.map((caseID) => ({
      caseID,
      partials: Array.from(
        this.caseListMap.get(caseID)?.values?.() ?? this.deletedCaseListMap.get(caseID)?.values?.() ?? []
      ),
    }));
    return {
      successCases: maybePartials.filter((p): p is MultiPartialsDTO => p?.partials?.length > 0),
      failedCases: maybePartials
        .filter((p) => !p?.partials?.length)
        .map(({ caseID }) => ({ caseID, error: `Partials not found` })),
    };
  }

  protected updateCaseListMap(caseID: string, partialStoredCases: Array<PartialStoredCase>) {
    partialStoredCases.forEach((partial) => {
      const caseMap = this.caseListMap.get(caseID) ?? new Map();
      caseMap.set(partial.accessLevel, partial);
      this.caseListMap.set(caseID, caseMap);
    });
  }

  async updatePartials(caseID: string, partialStoredCases: Array<PartialStoredCase>, saveMetaData: SaveMetaData) {
    const casesKey = `partials-${caseID}`;
    const oldCases = await this.getPartials(caseID);
    const historyCases = this.historyMap.get(casesKey) || [];
    const oldCaseMetaData = this.caseMetaDataMap.get(caseID)!;

    historyCases.push({
      creationDate: new Date().toISOString(),
      case_: oldCases,
      caseMetaData_: oldCaseMetaData,
    });
    this.historyMap.set(casesKey, historyCases);
    this.caseMetaDataMap.set(caseID, { ...oldCaseMetaData, ...saveMetaData });
    this.updateCaseListMap(caseID, partialStoredCases);
  }

  async createCase(partialStoredCases: Array<PartialStoredCase>, caseMetaData: CaseMetaData) {
    this.updateCaseListMap(caseMetaData.caseID, partialStoredCases);
    this.caseMetaDataMap.set(caseMetaData.caseID, caseMetaData);
  }

  protected getCaseOrThrow(
    caseID: string,
    errorType: StorageErrorType,
    errorMessage: string
  ): Map<string, PartialStoredCase> {
    const caseData = this.caseListMap.get(caseID) ?? this.deletedCaseListMap.get(caseID);
    if (!caseData) {
      throw new StorageError(
        `${errorMessage} ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        } in inMemoryCaseStorage`,
        errorType
      );
    }
    return caseData;
  }

  async delete(caseID: string) {
    if (this.caseListMap.get(caseID) !== null && this.caseListMap.get(caseID) !== undefined) {
      const xCase = this.caseListMap.get(caseID);
      this.deletedCaseListMap.set(caseID, xCase!);
      this.caseListMap.delete(caseID);
    } else {
      throw new StorageError(
        `Failed to delete, no case with this ID: ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        } in inMemoryCaseStorage`,
        StorageErrorType.DELETE_CASE_FAILED
      );
    }
  }

  async restore(caseID: string) {
    if (this.deletedCaseListMap.get(caseID) !== null && this.deletedCaseListMap.get(caseID) !== undefined) {
      const xCase = this.deletedCaseListMap.get(caseID);
      this.caseListMap.set(caseID, xCase!);
      this.deletedCaseListMap.delete(caseID);
    } else {
      throw new StorageError(
        `Failed to restore, no case deleted with this ID: ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        } in inMemoryCaseStorage`,
        StorageErrorType.RESTORE_CASE_FAILED
      );
    }
  }

  async permanentlyDelete(caseID: string) {
    this.getCaseOrThrow(
      caseID,
      StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED,
      'Failed to permanently delete, no case with this ID:'
    );
    this.caseListMap.delete(caseID);
    this.deletedCaseListMap.delete(caseID);
  }

  async getHistory(): Promise<Map<string, HistoryPartialStoredCase[]>> {
    return this.historyMap;
  }

  async getCaseHistory(caseID: string): Promise<HistoryPartialStoredCase[]> {
    const casesKey = `partials-${caseID}`;
    const history = await this.getHistory();
    return history.get(casesKey) || [];
  }

  async freeze(caseID: string, frozenAccessLevels: string[]) {
    await this.updateFreezeStatus(caseID, frozenAccessLevels, true);
  }

  async unfreeze(caseID: string, frozenAccessLevels: string[]) {
    await this.updateFreezeStatus(caseID, frozenAccessLevels, false);
  }

  async lock(caseID: string) {
    await this.updateLockStatus(caseID, true);
  }

  async unlock(caseID: string) {
    await this.updateLockStatus(caseID, false);
  }

  protected async updateFreezeStatus(caseID: string, frozenAccessLevels: string[], toFreeze: boolean) {
    try {
      const caseMetaData = await this.getCaseMetaData(caseID);
      caseMetaData.frozenAccessLevels.forEach((_isFrozen, accessLevel) => {
        if (frozenAccessLevels.includes(accessLevel)) {
          caseMetaData.frozenAccessLevels.set(accessLevel, toFreeze);
        }
      });
      this.caseMetaDataMap.set(caseID, caseMetaData);
    } catch (e) {
      const errorType = toFreeze ? StorageErrorType.FREEZE_FAILED : StorageErrorType.UNFREEZE_FAILED;
      throw new StorageError(
        `${toFreeze ? 'Freezing' : 'Unfreezing'} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in inMemoryCaseStorage`,
        errorType,
        e
      );
    }
  }

  protected async updateLockStatus(caseID: string, toLock: boolean) {
    try {
      const caseMetaData = await this.getCaseMetaData(caseID);
      const lockDuration = MagicNumbers.localStorageLockExpiresMinutes * MagicNumbers.minuteInMS;
      const updatedCaseMetaData: CaseMetaData = {
        ...caseMetaData,
        isLocked: toLock,
        lockUser: toLock ? 'Admin' : '',
        lockExpires: toLock ? new Date(Date.now() + lockDuration) : new Date(0),
      };
      this.caseMetaDataMap.set(caseID, updatedCaseMetaData);
    } catch (e) {
      const errorType = toLock ? StorageErrorType.LOCK_FAILED : StorageErrorType.UNLOCK_FAILED;
      throw new StorageError(
        `${toLock ? 'Locking' : 'Unlocking'} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in inMemoryCaseStorage`,
        errorType,
        e
      );
    }
  }

  reset() {
    this.caseListMap = new Map();
    this.caseMetaDataMap = new Map();
    this.deletedCaseListMap = new Map();
    this.historyMap = new Map();
  }
}

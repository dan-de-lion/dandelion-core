import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { DataSynchStorageInterface } from '@/interfaces/DataSynchStorageInterface';
import { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { NamedBindings } from '@/types/NamedBindings';
import type { CaseMetaData } from '@/entities/CaseMetaData';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import { KeyStorageInterface } from '@/interfaces/KeyStorageInterface';
import { Crypt } from '@/utils/Crypt';
import { Stringifier } from '@/utils/SerDes';
import _ from 'lodash';
import { inject, injectable } from 'inversify';

type SynchedCaseDTO = { caseMetaData: CaseMetaData; model: Record<string, any> };
type SynchStorageDTO = { [caseID: string]: SynchedCaseDTO };

@injectable()
export class LocalDataSynchStorage implements DataSynchStorageInterface {
  protected localStorage = window.localStorage;

  constructor(
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.KeyStorage) protected keyStorage: KeyStorageInterface,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface,
    protected baseKey = 'dataSynch'
  ) {}

  async saveSynch(partialStoredCases: PartialStoredCase[], caseMetaData: CaseMetaData): Promise<void> {
    if (partialStoredCases.length <= 0) {
      return;
    }

    const model: { [i: string]: any } = {};
    for (const key in partialStoredCases) {
      if (Object.hasOwn(partialStoredCases, key)) {
        const p = partialStoredCases[key];
        if (p.accessLevel === 'sensitiveData' || p.accessLevel === 'notToSynchronize') {
          continue;
        }
        const canReadPartialCase = this.userService.hasReadAccessRights(p.accessLevel);

        if (canReadPartialCase && this.coreConfiguration.encrypt) {
          const encryptionKey = await this.keyStorage.getKey(caseMetaData.caseID, p.accessLevel);
          try {
            const decryptedData = await Crypt.decrypt(p.data, encryptionKey);
            _.merge(model, decryptedData);
          } catch (error) {
            // Handle decryption error, e.g., log or rethrow
            console.error('Decryption failed:', error);
            throw new Error('Failed to decrypt data');
          }
        } else if (canReadPartialCase) {
          const parsedData = new Stringifier().deserialize(p.data);
          _.merge(model, parsedData);
          for (const d of Object.getOwnPropertySymbols(parsedData)) {
            // necessary
            //@ts-expect-error
            model[d] = _.cloneDeep(parsedData[d]);
          }
        }
      }
    }

    // If never synched, initialize to empty object
    let synchedData: SynchStorageDTO = {};
    const loadedSynchedData = this.localStorage.getItem(this.baseKey);
    if (loadedSynchedData) {
      try {
        synchedData = JSON.parse(loadedSynchedData);
      } catch (error) {
        console.error('Failed to parse synchronized data:', error);
      }
    }

    // Overwrite existing value or add if not existing
    synchedData[caseMetaData.caseID] = _.cloneDeep({ caseMetaData, model });
    this.localStorage.setItem(this.baseKey, JSON.stringify(synchedData));
  }

  async permanentlyDelete(caseID: string): Promise<void> {
    const loadedSynchedData = this.localStorage.getItem(this.baseKey);
    if (!loadedSynchedData) {
      return;
    }

    let synchedData: SynchStorageDTO;
    try {
      synchedData = JSON.parse(loadedSynchedData);
    } catch (error) {
      console.error('Failed to parse synchronized data:', error);
      return;
    }

    if (!synchedData || !(caseID in synchedData)) {
      return;
    }

    delete synchedData[caseID];
    this.localStorage.setItem(this.baseKey, JSON.stringify(synchedData));
  }
}

import axios, { type AxiosRequestTransformer, type AxiosResponse, type AxiosResponseTransformer } from 'axios';
import { inject, injectable } from 'inversify';
import _ from 'lodash';
import type { CaseMetaData, SaveMetaData } from '@/entities/CaseMetaData';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type {
  CaseStorageInterface,
  MultiCaseMetaResponse,
  MultiPartialsResponse,
} from '@/interfaces/CaseStorageInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import { DandelionService } from '@/services/DandelionService';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { AxiosReviver, AxiosSerializer } from '@/utils/SerDes';

@injectable()
export class RESTCaseStorage extends DandelionService implements CaseStorageInterface {
  protected readonly ROW_BASE_URL_SINGLE_ROW: string;
  protected readonly ROW_BASE_URL: string;
  protected readonly LOCK_BASE_URL: string;
  protected cache = new Map<string, { value: PartialStoredCase[]; expires: Date }>();

  private validateBaseUrl(url: string): string {
    return url.endsWith('/') ? url : `${url}/`;
  }

  protected axiosInstance = axios.create({
    transformRequest: [AxiosSerializer, ...(axios.defaults.transformRequest as AxiosRequestTransformer[])],
    transformResponse: [...(axios.defaults.transformResponse as AxiosResponseTransformer[]), AxiosReviver],
  });

  constructor(
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.CurrentContextService)
    protected currentContextService: CurrentContextService,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface
  ) {
    super();
    if (this.coreConfiguration.baseUrlForCases) {
      this.ROW_BASE_URL = this.validateBaseUrl(this.coreConfiguration.baseUrlForCases);
    } else {
      throw new StorageError(
        'baseUrlForCases is not defined in core configuration.',
        StorageErrorType.BAD_CONFIGURATION
      );
    }

    this.ROW_BASE_URL_SINGLE_ROW = `${this.ROW_BASE_URL}row/`;
    this.LOCK_BASE_URL = `${this.ROW_BASE_URL}row/lock/`;
    this.axiosInstance.interceptors.request.use((config: any) => {
      config.headers = this.userService.getHeaders();
      return config;
    });
  }

  checkResponseStatus(response: AxiosResponse<any, any>) {
    if (response.status < 200 || response.status > 299) {
      throw new StorageError(`Status: ${response.status}`, StorageErrorType.BAD_RESPONSE);
    }
  }

  async getAllCaseMetaData(caseType: string): Promise<CaseMetaData[]> {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const url = `getAllCaseMetaData/${centreCode}/${caseType}/`;
    const response = await this.axiosInstance.get(this.ROW_BASE_URL + url);
    this.checkResponseStatus(response);
    const allCaseMetaData = response.data;
    return Promise.resolve(allCaseMetaData);
  }

  async getCaseMetaData(caseID: string): Promise<CaseMetaData> {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration().caseName;
    const url = `getCaseMetaData/${centreCode}/${caseID}/${caseType}/`;
    const response = await this.axiosInstance.get(this.ROW_BASE_URL_SINGLE_ROW + url);
    this.checkResponseStatus(response);
    const caseMetaData = response.data;
    return Promise.resolve(caseMetaData);
  }

  async getPartials(caseID: string): Promise<PartialStoredCase[]> {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration().caseName;
    const url = `${centreCode}/${caseID}/${caseType}/`;

    //TODO: verificare se la cache è necessaria
    if (this.cache.has(url) && this.cache.get(url)!.expires > new Date()) {
      return Promise.resolve(_.cloneDeep(this.cache.get(url)!.value));
    }

    const data = (await this.axiosInstance.get(this.ROW_BASE_URL_SINGLE_ROW + url)).data.partialStoredCases;
    const partialStoredCases: Array<PartialStoredCase> = data.map((d: any) => ({
      accessLevel: d.accessLevel,
      data: d.data,
    }));

    this.cache.set(url, {
      value: partialStoredCases,
      expires: new Date(
        new Date().getTime() + MagicNumbers.RestEventStorageCacheExpiresMinutes * MagicNumbers.minuteInMS
      ),
    });
    return Promise.resolve(partialStoredCases);
  }

  async manageCase(
    action: string,
    caseID: string,
    partialStoredCases: PartialStoredCase[],
    caseMetaData: CaseMetaData | SaveMetaData
  ): Promise<void> {
    try {
      if (partialStoredCases.length <= 0) {
        return;
      }
      const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
      const caseType = this.currentContextService.getCaseConfiguration().caseName;
      const partialsToBeSaved = partialStoredCases.reduce((prev, curr) => {
        prev[curr.accessLevel] = curr;
        return prev;
      }, {} as { [i: string]: PartialStoredCase });

      const key = `${centreCode}/${caseID}/${caseType}/`;
      const url = `${this.ROW_BASE_URL_SINGLE_ROW}${action}/${key}`;
      const data = { partialsToBeSaved, [action === 'create' ? 'caseMetaData' : 'saveMetaData']: caseMetaData };
      const response = await this.axiosInstance.post(url, data);
      this.checkResponseStatus(response);
      this.cache.delete(key);
    } catch (e) {
      const errorType = action === 'create' ? StorageErrorType.CREATE_CASE_FAILED : StorageErrorType.UPDATE_CASE_FAILED;
      throw new StorageError(
        `${action === 'create' ? 'Create' : 'Update partials for'}} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in rest storage`,
        errorType,
        e
      );
    }
  }

  async createCase(partialStoredCases: PartialStoredCase[], caseMetaData: CaseMetaData): Promise<void> {
    await this.manageCase('create', caseMetaData.caseID, partialStoredCases, caseMetaData);
  }

  async updatePartials(
    caseID: string,
    partialStoredCases: PartialStoredCase[],
    saveMetaData: SaveMetaData
  ): Promise<void> {
    await this.manageCase('update', caseID, partialStoredCases, saveMetaData);
  }

  delete(_caseID: string): Promise<void> {
    return Promise.resolve();
  }

  restore(_caseID: string): Promise<void> {
    return Promise.resolve();
  }

  async permanentlyDelete(caseID: string): Promise<void> {
    try {
      const centreCode = this.userService.getCurrentUser()?.value?.centre;
      const caseType = this.currentContextService.getCaseConfiguration().caseName;
      const response = await this.axiosInstance.delete(
        `${this.ROW_BASE_URL_SINGLE_ROW}${centreCode}/${caseID}/${caseType}/`
      );
      this.checkResponseStatus(response);
    } catch (e) {
      throw new StorageError(
        `Failed to permanently delete case ${caseID} in rest storage`,
        StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED,
        e
      );
    }
  }

  async lock(caseID: string): Promise<void> {
    await this.updateLockStatus(caseID, true);
  }

  async unlock(caseID: string): Promise<void> {
    await this.updateLockStatus(caseID, false);
  }

  async updateLockStatus(caseID: string, toLock: boolean): Promise<void> {
    try {
      const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
      const caseType = this.currentContextService.getCaseConfiguration().caseName;
      const response = toLock
        ? await this.axiosInstance.post(`${this.LOCK_BASE_URL}${centreCode}/${caseID}/${caseType}/`, '')
        : await this.axiosInstance.delete(`${this.LOCK_BASE_URL}${centreCode}/${caseID}/${caseType}/`);
      this.checkResponseStatus(response);
    } catch (e) {
      const errorType = toLock ? StorageErrorType.LOCK_FAILED : StorageErrorType.UNLOCK_FAILED;
      throw new StorageError(
        `${toLock ? 'Locking' : 'Unlocking'} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in rest storage`,
        errorType,
        e
      );
    }
  }

  async freeze(caseID: string, frozenAccessLevels: string[]): Promise<void> {
    await this.updateFreezeStatus(caseID, frozenAccessLevels, true);
  }

  async unfreeze(caseID: string, frozenAccessLevels: string[]): Promise<void> {
    await this.updateFreezeStatus(caseID, frozenAccessLevels, false);
  }

  async updateFreezeStatus(caseID: string, frozenAccessLevels: string[], toFreeze: boolean): Promise<void> {
    try {
      const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
      const caseType = this.currentContextService.getCaseConfiguration().caseName;
      const response = toFreeze
        ? await this.axiosInstance.post(
            `${this.ROW_BASE_URL_SINGLE_ROW}freeze/${centreCode}/${caseID}/${caseType}/`,
            frozenAccessLevels
          )
        : await this.axiosInstance.delete(
            `${this.ROW_BASE_URL_SINGLE_ROW}freeze/${centreCode}/${caseID}/${caseType}/`,
            {
              data: frozenAccessLevels,
            }
          );
      this.checkResponseStatus(response);
      return Promise.resolve();
    } catch (e) {
      const errorType = toFreeze ? StorageErrorType.FREEZE_FAILED : StorageErrorType.UNFREEZE_FAILED;
      throw new StorageError(
        `${toFreeze ? 'Freezing' : 'Unfreezing'} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in rest storage`,
        errorType,
        e
      );
    }
  }

  getCaseHistory(_caseID: string, _lastK?: number): Promise<HistoryPartialStoredCase[]> {
    throw new StorageError('getCaseHistory in RestCaseStorage', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  async getMultiplePartials(caseIDs: string[]): MultiPartialsResponse {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration().caseName;
    const url = `${this.ROW_BASE_URL}getMultiplePartials/${centreCode}/${caseType}/`;

    try {
      const response = await this.axiosInstance.post(url, caseIDs);
      this.checkResponseStatus(response);
      const data = response.data;
      // Success cases
      const successCases = data.successCases.map((c: any) => ({
        caseID: c.caseID,
        partials: c.partials.map((d: any) => ({
          accessLevel: d.accessLevel,
          data: d.data,
        })),
      }));

      // Failed cases
      const failedCases = data.failedCases.map((c: any) => ({
        caseID: c.caseID,
        error: c.error,
      }));

      return { successCases, failedCases };
    } catch (error) {
      // TODO: avoid throw and response with all failedCases? It's more consistent with interface
      throw new StorageError('Failed to fetch multiple case partials', StorageErrorType.BAD_RESPONSE, error);
    }
  }

  async getMultipleCaseMetaData(caseIDs: string[]): MultiCaseMetaResponse {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration().caseName;
    const url = `${this.ROW_BASE_URL}getMultipleCaseMetaData/${centreCode}/${caseType}/`;

    try {
      const response = await this.axiosInstance.post(url, caseIDs);
      this.checkResponseStatus(response);
      const data = response.data;

      // Success cases
      const successCases = data.successCases.map((c: any) => ({
        caseID: c.caseID,
        caseMetaData: c.metaData, // Assuming backend returns this correctly
      }));

      // Failed cases
      const failedCases = data.failedCases.map((c: any) => ({
        caseID: c.caseID,
        error: c.error,
      }));

      return { successCases, failedCases };
    } catch (error) {
      throw new StorageError('Failed to fetch multiple case metadata', StorageErrorType.BAD_RESPONSE, error);
    }
  }
}

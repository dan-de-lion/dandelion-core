import { injectable, inject } from 'inversify';
import { useDebounceFn } from '@vueuse/core';
import { CrfWithValidity } from '@/entities/CrfWithValidity';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import { ActivationPeriodsForCrfStorageInterface } from '@/interfaces/ActivationPeriodsForCrfStorageInterface';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import CurrentContextService from '@/services/CurrentContextService';
import { DandelionService } from '@/services/DandelionService';
import TranslationService from '@/services/TranslationService';
import { NamedBindings } from '@/types/NamedBindings';
import {
  ZcrfWithValidityForCentreCode,
  ZcrfWithValidityForCentreCodeSchema,
} from '@/utils/ValidationSchemas/CRFWithValiditySchema';
import { parseElementWithZod, ZodSchemas } from '@/utils/ValidationSchemas/handleZod';

@injectable()
export class ActivationPeriodsForCrfStorage
  extends DandelionService
  implements ActivationPeriodsForCrfStorageInterface
{
  protected debouncedNoMainCrfFoundError: (mainCrf: string) => void;
  constructor(
    @inject(NamedBindings.CurrentContextService) protected currentContextService: CurrentContextService,
    @inject(NamedBindings.Logger) protected logger: LoggerInterface,
    @inject(NamedBindings.TranslationService) protected ts: TranslationService
  ) {
    super();

    this.debouncedNoMainCrfFoundError = useDebounceFn((mainCrfName) => {
      this.logger.logError(this.ts.t('noMainCrfFoundMsg', { mainCrfName }), true);
      throw new CRFError(this.ts.t('noMainCrfFoundMsg', { mainCrfName }), CRFErrorType.CRF_NOT_FOUND);
    }, 500);
  }

  async getCrfsWithValidityForCentreCode(centreCode: string): Promise<CrfWithValidity[]> {
    const result = await fetch('/crfWithValidityForCentreCode.json');
    if (!result.ok) {
      return this.currentContextService
        .getCaseConfiguration()
        .getAllCRFs()
        .map((crf) => ({
          crfName: crf.crfName,
          crfValidityArray: [],
          isAlwaysActive: true,
        }));
    }
    try {
      const jsonData = await result.json();
      const validatedCrfsWithValidity = this.validateCrfsWithValidity(jsonData);
      const mainCrfName = this.currentContextService.getCaseConfiguration().getMainCRF().crfName;
      const crfsForCentre = validatedCrfsWithValidity.get(centreCode);
      if (!crfsForCentre) {
        const mainCrf = Array.from(validatedCrfsWithValidity.values())
          .flat()
          .find((crf) => crf.crfName === mainCrfName);
        if (mainCrf) {
          return [mainCrf];
        }
        this.debouncedNoMainCrfFoundError(mainCrfName);
      }
      //@ts-expect-error
      return crfsForCentre;
    } catch (e) {
      throw new CRFError(`Error parsing Crf with validity for centre code`, CRFErrorType.CRF_WITH_VALIDIY_NOT_FOUND, e);
    }
  }

  protected validateCrfsWithValidity(crfsWithValidityJsonData: any): Map<string, CrfWithValidity[]> {
    const crfsWithValidity: ZcrfWithValidityForCentreCode = parseElementWithZod(
      ZcrfWithValidityForCentreCodeSchema,
      ZodSchemas.CRF_WITH_VALIDITY_CENTRE_CODE,
      crfsWithValidityJsonData
    );
    return new Map(Object.entries(crfsWithValidity));
  }
}

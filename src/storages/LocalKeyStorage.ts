import { injectable } from 'inversify';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type { KeyStorageInterface, MultiKeysDTO, MultiKeysResponse } from '@/interfaces/KeyStorageInterface';

@injectable()
export class LocalKeyStorage implements KeyStorageInterface {
  protected localStorage = window.localStorage;

  async saveKey(caseID: string, key: string, accessLevel: string): Promise<void> {
    try {
      this.localStorage.setItem(`key-${caseID}-${accessLevel}`, key);
      return Promise.resolve();
    } catch (e) {
      throw new StorageError('Failed saving the key in LocalKeyStorage', StorageErrorType.SAVING_KEY_FAILED);
    }
  }

  async getKey(caseID: string, accessLevel: string): Promise<string> {
    try {
      const key = this.localStorage.getItem(`key-${caseID}-${accessLevel}`) ?? '';
      return key;
    } catch (e) {
      throw new StorageError('Failed to get the key in LocalKeyStorage', StorageErrorType.GETTING_KEY_FAILED);
    }
  }

  async getMultipleKeys(caseIDs: string[]): MultiKeysResponse {
    const successCases: Array<MultiKeysDTO> = [];
    const maybeKeys = caseIDs.map((caseID) => ({
      caseID,
      storageKeysList: Object.keys(this.localStorage).filter((k) => k.startsWith(`key-${caseID}`)),
    }));
    maybeKeys.forEach((mk) =>
      mk.storageKeysList.forEach((skl) =>
        successCases.push({
          caseID: mk.caseID,
          accessLevel: skl.replace(`key-${mk.caseID}-`, ''),
          key: this.localStorage.getItem(skl)!,
        })
      )
    );

    return {
      successCases,
      failedCases: maybeKeys
        .filter((k) => !k.storageKeysList?.length)
        .map(({ caseID }) => ({ caseID, accessLevel: '*', error: `Keys not found` })),
    };
  }
}

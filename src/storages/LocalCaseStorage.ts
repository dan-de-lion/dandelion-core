import { injectable } from 'inversify';
import _ from 'lodash';
import { getContainer } from '@/ContainersConfig';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import type { SaveMetaData, CaseMetaData } from '@/entities/CaseMetaData';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type {
  CaseStorageInterface,
  MultiCaseMetaDTO,
  MultiCaseMetaErrorDTO,
  MultiCaseMetaResponse,
  MultiPartialsResponse,
} from '@/interfaces/CaseStorageInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { DandelionService } from '@/services/DandelionService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { Stringifier } from '@/utils/SerDes';
import { MagicNumbers } from '@/types/MagicNumbers';
import { CaseData } from '@/entities/Cases';

@injectable()
class LocalCaseDTO {
  protected readonly serializedData: string;

  constructor(readonly caseMetaData: SaveMetaData, readonly partialStoredCases: Array<PartialStoredCase>) {
    this.serializedData = new Stringifier().serialize(_.cloneDeep({ caseMetaData, partialStoredCases }));
  }

  public static from(caseMetaData: SaveMetaData, partialStoredCases: Array<PartialStoredCase>): string {
    const instance = new LocalCaseDTO(caseMetaData, partialStoredCases);
    return instance.serializedData;
  }
}

export class LocalCaseStorage extends DandelionService implements CaseStorageInterface {
  protected localStorage = window.localStorage;
  protected userService: UserServiceInterface;
  protected coreConfiguration: CoreConfigurationInterface;

  constructor(protected baseKey: string = '') {
    super();
    this.userService = getContainer(Contexts.SHARED).get(NamedBindings.UserService) as UserServiceInterface;
    this.coreConfiguration = getContainer(Contexts.SHARED).get(
      NamedBindings.CoreConfiguration
    ) as CoreConfigurationInterface;
  }

  async getAllCaseMetaData(caseType: string): Promise<CaseMetaData[]> {
    const result: CaseMetaData[] = [];
    for (const key of Object.keys(localStorage)) {
      if (key.startsWith('partials-')) {
        const raw = localStorage.getItem(key)!;
        const partials = new Stringifier<{
          partialStoredCases: PartialStoredCase[];
          caseMetaData: CaseMetaData;
        }>().deserialize(raw);
        if (caseType == partials.caseMetaData.caseType) {
          result.push(partials.caseMetaData);
        }
      }
    }
    return result;
  }

  protected async deserializeCase(caseID: string): Promise<CaseData> {
    try {
      const raw =
        this.localStorage.getItem(`${this.baseKey}partials-${caseID}`) ??
        this.localStorage.getItem(`${this.baseKey}del-${caseID}`)!;
      return new Stringifier<CaseData>().deserialize(raw);
    } catch (e) {
      throw new StorageError(
        `Deserialization for case: ${caseID} failed in local storage, check 'deserialize' in SerDes`,
        StorageErrorType.PARTIALS_NOT_DESERIALIZED,
        e
      );
    }
  }

  async getCaseMetaData(caseID: string): Promise<CaseMetaData> {
    const caseMetaData = (await this.deserializeCase(caseID))?.caseMetaData;
    if (!caseMetaData) {
      throw new StorageError(`Case: ${caseID} has empty caseMetaData`, StorageErrorType.CASE_METADATA_NOT_FOUND);
    }
    return caseMetaData;
  }

  async getPartials(caseID: string): Promise<PartialStoredCase[]> {
    const partialStoredCases = (await this.deserializeCase(caseID))?.partialStoredCases;
    if (!partialStoredCases || !partialStoredCases.length || partialStoredCases.length === 0) {
      throw new StorageError(
        `Failed to get partials, no case with this ID: ${
          caseID ? caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID) : 'empty ID'
        } in LocalCaseStorage`,
        StorageErrorType.PARTIALS_NOT_FOUND
      );
    }
    const caseMetaData = await this.getCaseMetaData(caseID);
    const user = this.userService.getCurrentUser().value?.username;
    if (caseMetaData.isLocked == true && caseMetaData.lockUser != user) {
      throw new StorageError(
        `Case ${caseID.substring(0, MagicNumbers.lenghtOfAbbreviatedCaseID)} is locked in his caseMetaData`,
        StorageErrorType.IS_LOCKED
      );
    }
    return partialStoredCases.map((s) => ({
      ...s,
      data: !this.coreConfiguration.encrypt ? new Stringifier().serialize(s.data) : s.data,
    }));
  }

  async getMultiplePartials(caseIDs: string[]): MultiPartialsResponse {
    const successCases: { caseID: string; partials: PartialStoredCase[] }[] = [];
    const failedCases: { caseID: string; error: string }[] = [];
    for (const caseID of caseIDs) {
      try {
        successCases.push({ caseID, partials: await this.getPartials(caseID) });
      } catch (e: any) {
        failedCases.push({ caseID, error: `Cannot get partials for case ${caseID}: ${e?.message || e?.toString?.()}` });
      }
    }
    return { successCases, failedCases };
  }

  async getMultipleCaseMetaData(caseIDs: string[]): MultiCaseMetaResponse {
    const successCases: Array<MultiCaseMetaDTO> = [];
    const failedCases: Array<MultiCaseMetaErrorDTO> = [];
    for (const caseID of caseIDs) {
      try {
        successCases.push({ caseID, caseMetaData: await this.getCaseMetaData(caseID) });
      } catch (e: any) {
        failedCases.push({ caseID, error: `Cannot get metadata for case ${caseID}: ${e?.message || e?.toString?.()}` });
      }
    }
    return { successCases, failedCases };
  }

  async updatePartials(caseID: string, partialStoredCases: Array<PartialStoredCase>, saveMetaData: SaveMetaData) {
    try {
      const partialsToBeSaved = await this.deserializePartials(partialStoredCases);
      const partialStoredCasesKey = `${this.baseKey}partials-${caseID}`;
      const history: Map<string, HistoryPartialStoredCase[]> = (await this.getHistory()) ?? new Map();
      const oldPartialsToBeSaved = await this.deserializePartials(await this.getPartials(caseID));
      const oldCaseMetaData = await this.getCaseMetaData(caseID);
      const historyCase: HistoryPartialStoredCase = {
        creationDate: new Date().toISOString(),
        case_: oldPartialsToBeSaved,
        caseMetaData_: oldCaseMetaData,
      };
      const historyCases = history.get(partialStoredCasesKey) || [];
      historyCases.push(historyCase);
      history.set(partialStoredCasesKey, historyCases);
      this.localStorage.setItem(`cases-history`, new Stringifier().serialize(_.cloneDeep(history)));
      for (const oldP of oldPartialsToBeSaved) {
        if (!partialsToBeSaved.find((p) => p.accessLevel === oldP.accessLevel)) {
          partialsToBeSaved.push(oldP);
        }
      }
      this.localStorage.setItem(
        partialStoredCasesKey,
        LocalCaseDTO.from({ ...oldCaseMetaData, ...saveMetaData }, partialsToBeSaved)
      );
    } catch (e) {
      throw new StorageError(
        `Update partials failed for case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} in local storage`,
        StorageErrorType.UPDATE_CASE_FAILED,
        e
      );
    }
  }

  async createCase(partialStoredCases: Array<PartialStoredCase>, caseMetaData: CaseMetaData) {
    try {
      const partialStoredCasesKey = `${this.baseKey}partials-${caseMetaData.caseID}`;
      const partialsToBeSaved = await this.deserializePartials(partialStoredCases);
      this.localStorage.setItem(partialStoredCasesKey, LocalCaseDTO.from(caseMetaData, partialsToBeSaved));
    } catch (e) {
      throw new StorageError(
        `Create case ${caseMetaData.caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in local storage`,
        StorageErrorType.CREATE_CASE_FAILED,
        e
      );
    }
  }

  async deserializePartials(partialStoredCases: PartialStoredCase[]): Promise<PartialStoredCase[]> {
    return !this.coreConfiguration.encrypt
      ? partialStoredCases.map((s) => {
          const toBeSaved: PartialStoredCase = {
            ..._.cloneDeep(s),
            data: new Stringifier<string>().deserialize(s.data),
          };
          return toBeSaved;
        })
      : partialStoredCases;
  }

  protected checkIfCaseExists(caseID: string, prefix: string, errorMessage: string, errorType: StorageErrorType) {
    const item = this.localStorage.getItem(`${this.baseKey}${prefix}-${caseID}`);
    if (!item) {
      throw new StorageError(errorMessage, errorType);
    }
  }

  protected moveItem(caseID: string, fromPrefix: string, toPrefix: string) {
    const item = this.localStorage.getItem(`${this.baseKey}${fromPrefix}-${caseID}`);
    if (item !== null) {
      this.localStorage.setItem(`${this.baseKey}${toPrefix}-${caseID}`, item);
      this.localStorage.removeItem(`${this.baseKey}${fromPrefix}-${caseID}`);
    }
  }

  async delete(caseID: string) {
    this.checkIfCaseExists(
      caseID,
      'partials',
      `Failed to delete, no case with this ID: ${caseID} in LocalCaseStorage`,
      StorageErrorType.DELETE_CASE_FAILED
    );
    this.moveItem(caseID, 'partials', 'del');
  }

  async permanentlyDelete(caseID: string) {
    const prefix = this.localStorage.getItem(`${this.baseKey}partials-${caseID}`) ? 'partials' : 'del';
    this.checkIfCaseExists(
      caseID,
      prefix,
      `Failed to permanently delete, no case with this ID: ${caseID} in LocalCaseStorage`,
      StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED
    );
    this.localStorage.removeItem(`${this.baseKey}${prefix}-${caseID}`);
  }

  async restore(caseID: string) {
    this.checkIfCaseExists(
      caseID,
      'del',
      `Failed to restore, no case deleted with this ID: ${caseID} in LocalCaseStorage`,
      StorageErrorType.RESTORE_CASE_FAILED
    );
    this.moveItem(caseID, 'del', 'partials');
  }

  protected async getHistory(): Promise<Map<string, HistoryPartialStoredCase[]>> {
    if (!this.localStorage.getItem(`cases-history`)) {
      return new Map();
    }
    return new Stringifier<Map<string, HistoryPartialStoredCase[]>>().deserialize(
      this.localStorage.getItem(`cases-history`)!
    );
  }

  async getCaseHistory(caseID: string): Promise<HistoryPartialStoredCase[]> {
    const history = await this.getHistory();
    if (history && history.has(`${this.baseKey}partials-${caseID}`)) {
      return history.get(`${this.baseKey}partials-${caseID}`)!;
    }
    return [];
  }

  async lock(caseID: string) {
    await this.updateLockStatus(caseID, true);
  }

  async unlock(caseID: string) {
    await this.updateLockStatus(caseID, false);
  }

  protected async updateLockStatus(caseID: string, toLock: boolean) {
    try {
      const partials = await this.getPartials(caseID);
      const partialsToBeSaved = await this.deserializePartials(partials);
      const caseMetaData = await this.getCaseMetaData(caseID);
      const lockDuration = MagicNumbers.localStorageLockExpiresMinutes * MagicNumbers.minuteInMS;
      const updatedCaseMetaData: CaseMetaData = {
        ...caseMetaData,
        isLocked: toLock,
        lockUser: toLock ? this.userService.getCurrentUser()?.value?.username || '' : '',
        lockExpires: toLock ? new Date(Date.now() + lockDuration) : new Date(0),
      };
      const partialStoredCasesKey = `${this.baseKey}partials-${caseID}`;
      this.localStorage.setItem(partialStoredCasesKey, LocalCaseDTO.from(updatedCaseMetaData, partialsToBeSaved));
    } catch (e) {
      const errorType = toLock ? StorageErrorType.LOCK_FAILED : StorageErrorType.UNLOCK_FAILED;
      throw new StorageError(
        `${toLock ? 'Locking' : 'Unlocking'} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in local storage`,
        errorType,
        e
      );
    }
  }

  async freeze(caseID: string, frozenAccessLevels: string[]) {
    await this.updateFreezeStatus(caseID, frozenAccessLevels, true);
  }

  async unfreeze(caseID: string, frozenAccessLevels: string[]) {
    await this.updateFreezeStatus(caseID, frozenAccessLevels, false);
  }

  protected async updateFreezeStatus(caseID: string, frozenAccessLevels: string[], toFreeze: boolean) {
    try {
      const partials = await this.getPartials(caseID);
      const partialsToBeSaved = await this.deserializePartials(partials);
      const caseMetaData = await this.getCaseMetaData(caseID);
      frozenAccessLevels.forEach((accessLevel) => {
        caseMetaData.frozenAccessLevels.set(accessLevel, toFreeze);
      });
      const partialStoredCasesKey = `${this.baseKey}partials-${caseID}`;
      this.localStorage.setItem(partialStoredCasesKey, LocalCaseDTO.from(caseMetaData, partialsToBeSaved));
    } catch (e) {
      const errorType = toFreeze ? StorageErrorType.FREEZE_FAILED : StorageErrorType.UNFREEZE_FAILED;
      throw new StorageError(
        `${toFreeze ? 'Freezing' : 'Unfreezing'} case ${caseID.substring(
          0,
          MagicNumbers.lenghtOfAbbreviatedCaseID
        )} failed in local storage`,
        errorType,
        e
      );
    }
  }
}

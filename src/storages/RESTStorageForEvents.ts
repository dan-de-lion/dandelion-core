import axios from 'axios';
import { inject, injectable } from 'inversify';
import { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import { DandelionService } from '@/services/DandelionService';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { ZcaseListSnapshot, ZcaseListSnapshotSchema } from '@/utils/ValidationSchemas/SnapshotSchema';
import { ZsourceEvents, ZsourceEventsSchema } from '@/utils/ValidationSchemas/SourceEventSchema';
import { parseElementWithZod, ZodSchemas } from '@/utils/ValidationSchemas/handleZod';

@injectable()
export class RESTStorageForEvents extends DandelionService implements EventsStorageInterface {
  protected readonly BASE_URL_GET;
  protected readonly BASE_URL_POST;
  protected cache: SourceEventForList[] | null = null;
  protected cacheExpiring: Date = new Date();

  constructor(
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.CurrentContextService) protected currentContextService: CurrentContextService,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface
  ) {
    super();
    this.BASE_URL_GET = this.coreConfiguration.baseUrlForEvents;
    this.BASE_URL_POST = `${this.coreConfiguration.baseUrlForEvents}event/`;
    axios.interceptors.request.use((config: any) => {
      config.headers = this.userService.getHeaders();
      return config;
    });
  }

  async getLastSnapshotAndEvents(): Promise<SnapshotAndEvents> {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration().caseName;
    const { events, snapshot } = (
      await axios.get(this.BASE_URL_GET + 'GetSnapshotAndEvents/' + `${centreCode}/${caseType}/`)
    ).data;
    return {
      events: parseElementWithZod(
        ZsourceEventsSchema,
        ZodSchemas.SOURCE_EVENT,
        events.map((event: any) => JSON.parse(event.data))
      ) as ZsourceEvents,
      snapshot: parseElementWithZod(
        ZcaseListSnapshotSchema,
        ZodSchemas.SNAPSHOT,
        //Remove default array value when issue 77 of backend will be release
        new CaseListSnapshot(snapshot.cachedCaseList ?? [], snapshot.date, snapshot.numberOfEvents)
      ) as ZcaseListSnapshot,
    };
  }

  async saveSnapshot(snapshot: CaseListSnapshot): Promise<void> {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration()?.caseName;
    await axios.post(
      this.BASE_URL_POST + 'snapshot/save/' + `${centreCode}/${caseType}/`,
      {
        cachedCaseList: snapshot.cachedCaseList,
        date: snapshot.date,
        numberOfEvents: snapshot.numberOfEvents,
      },
      { headers: this.userService.getHeaders() }
    );
  }

  async loadAllEvents(): Promise<SourceEventForList[]> {
    if (this.cacheExpiring > new Date() && this.cache) {
      return this.cache;
    }
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const caseType = this.currentContextService.getCaseConfiguration()?.caseName;
    const request = `${this.BASE_URL_GET + centreCode}/${caseType}/`;
    const eventsResponse = (await axios.get(request)).data;
    if (!eventsResponse) {
      throw new StorageError(
        `In RESTStorage for Event, request: ${request} failed`,
        StorageErrorType.AXIOS_REQUEST_FAILED
      );
    }
    const events: ZsourceEvents = parseElementWithZod(
      ZsourceEventsSchema,
      ZodSchemas.SOURCE_EVENT,
      eventsResponse.json()
    );
    this.cache = events;
    this.cacheExpiring = new Date(
      new Date().getTime() + MagicNumbers.RestCaseStorageCacheExpiresMinutes * MagicNumbers.minuteInMS
    );
    return events;
  }

  async saveEvent(event: SourceEventForList, centreCode: string, caseType: string): Promise<void> {
    const request = `${this.BASE_URL_POST + centreCode}/${caseType}/`;
    const response = await axios.post(request, event, { headers: this.userService.getHeaders() });
    if (!response) {
      throw new StorageError(
        `In RESTStorage for Event, request: ${request} failed`,
        StorageErrorType.AXIOS_REQUEST_FAILED
      );
    }
    this.cacheExpiring = new Date();
    this.cache = null;
  }
}

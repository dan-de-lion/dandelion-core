import { inject, injectable } from 'inversify';
import { CRF, CRFVersionValidity, type Version } from '@/entities/CRF';
import { CrfData, CRFDataClassInterface, CRFValuesSetInterface, CRFVariableInterface } from '@/entities/CRFEntities';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import type { AccessLevelsTree } from '@/interfaces/AccessLevelsTree';
import type { CoreConfigurationInterface, CRFConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import { DandelionService } from '@/services/DandelionService';
import { ConsoleLogger } from '@/services/Logger';
import { MagicStrings } from '@/types/MagicStrings';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';
import type { XSLT2Processor } from '@/types/XSLT2Processor';
import { Zversions, ZversionsSchema } from '@/utils/ValidationSchemas/CRFVersionSchema';
import { parseElementWithZod, ZodSchemas } from '@/utils/ValidationSchemas/handleZod';
import Helpers from '@/utils/Helpers';

@injectable()
export class CrfServiceFromFolder extends DandelionService implements CRFServiceInterface {
  protected versionsCache: Map<string, CRFVersionValidity[]> = new Map();
  protected crfCache: Map<string, Map<Version, CRF>> = new Map();
  constructor(
    @inject(NamedBindings.Logger) protected logger: ConsoleLogger,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface,
    protected pathToFolder = '/examples/crfs'
  ) {
    super();
    this.pathToFolder = pathToFolder;
  }

  async getCRFActiveVersions(crfName: string): Promise<CRFVersionValidity[]> {
    if (this.versionsCache.has(crfName)) {
      return this.versionsCache.get(crfName) ?? [];
    }
    const versionsResult = await fetch(`${this.pathToFolder}/${crfName}/CRF_validity.json`);
    if (!versionsResult.ok) {
      throw new CRFError(`CRF_validity.json is required in CRF ${crfName}`, CRFErrorType.CRF_VALIDITY_NOT_FOUND);
    }
    const versionsJSON = await versionsResult.json();
    const crfValidated = this.validateCRFVersions(versionsJSON);
    this.versionsCache.set(crfName, crfValidated);
    return crfValidated;
  }

  protected validateCRFVersions(versionsJSON: any): CRFVersionValidity[] {
    const versionsData: Zversions = parseElementWithZod(ZversionsSchema, ZodSchemas.CRF_VERSION, versionsJSON);
    return versionsData.versions.map((version) => {
      return new CRFVersionValidity(version.major, version.lastAvailable, version.startDate);
    });
  }

  async getCRF(crfName: string, crfVersion: Version): Promise<CRF> {
    const cachedCRF = this.crfCache.get(crfName)?.get(crfVersion);
    if (cachedCRF) {
      return cachedCRF;
    }
    const pathString = `${this.pathToFolder}/${crfName}/${crfVersion}`;
    const toXml = `${pathString}/${crfName}_${crfVersion}.xml`;
    const toJs = `${pathString}/${crfName}_functions_${crfVersion}.js`;
    const toCss = `${pathString}/${crfName}_style_${crfVersion}.css`;
    const toTranslation = `${pathString}/${crfName}_translations_${crfVersion}.json`;
    try {
      const [xml, js, css, translation] = await Promise.all([
        fetch(toXml),
        fetch(toJs),
        fetch(toCss, { headers: { Accept: 'text/css' } }),
        fetch(toTranslation),
      ]);
      if (!xml.ok) {
        throw new CRFError(`XML files is required in CRF ${crfName}`, CRFErrorType.XML_NOT_FOUND);
      }
      const xmlText = xml.ok && xml.status != 202 ? await xml.text() : '';
      const accessLevels = await this.loadAccessLevels(xmlText, PathToXsl.SCHEMA_TO_ACCESS_LEVELS, crfName);
      const resultCRF = new CRF(
        crfName,
        crfVersion,
        xmlText,
        js.ok && js.status != 202 ? await js.text() : '',
        css.ok && css.status != 202 ? await css.text() : '',
        translation.ok && translation.status != 202 ? await translation.json() : '',
        accessLevels,
        await this.getXmlData(crfName, xmlText)
      );
      this.crfCache.set(crfName, new Map<Version, CRF>().set(crfVersion, resultCRF));
      return resultCRF;
    } catch (error) {
      return Promise.reject(error);
    }
  }

  async getLatestVersion(crfName: string): Promise<Version> {
    const versions = await this.getCRFActiveVersions(crfName);
    const latestItem = versions.reduce((latest, current) => {
      return current.startDate > latest.startDate ? current : latest;
    });
    return latestItem.lastAvailable;
  }

  async loadAccessLevels(xml: string, path: string, crfName: string): Promise<AccessLevelsTree> {
    let resultCRF = await this.transformCrf(xml, path, crfName, 'dataClass');
    resultCRF = `{"${crfName}": {${resultCRF}}}`;
    //It will remove commas that are not part of a JSON structure or other alphanumeric sequence, not easy to implement in xsl
    resultCRF = resultCRF.replace(/,(?!\s*?[{["'\w])/g, '');
    return <AccessLevelsTree>JSON.parse(resultCRF);
  }

  getAccessLevelsForCRF(crfName: string, crfVersion: Version) {
    const resultCRF = this.crfCache.get(crfName)?.get(crfVersion);
    if (resultCRF) {
      return resultCRF.accessLevels;
    }
    this.logger.logError(`There is no loaded CRF with this name and version: ${crfName},${crfVersion}`);
    throw new CRFError(
      `There is no CRF with this name and version: ${crfName},${crfVersion} in CRF cache`,
      CRFErrorType.CRF_NOT_FOUND
    );
  }

  public isCRFPath(url: string) {
    return (
      ((url.endsWith('.css') && url.includes('_style_')) ||
        (url.endsWith('.js') && url.includes('_functions_')) ||
        (url.endsWith('.json') && url.includes('_translations_'))) &&
      url.includes(this.pathToFolder)
    );
  }

  protected async transformCrf(xml: string, path: string, crfName: string, obj: string, newCase?: boolean) {
    // necessary
    // @ts-expect-error
    const processor: XSLT2Processor = new XSLT2Processor();
    const parser = new DOMParser();
    await processor.importStylesheetURI(this.coreConfiguration.baseUrlForXslt + path);
    processor.setParameter(null, 'root', crfName);
    const xmlDoc = parser.parseFromString(xml, 'text/xml');
    // we always start from the root object of the xml
    const objectsToTransform = xmlDoc.getElementsByTagName(obj);
    // the fragment is made up of many strings
    let resultCRF = '';
    let fragment: DocumentFragment;
    if (!newCase) {
      fragment = processor.transformToFragment(objectsToTransform[0], document);
    } else {
      const objectToTransform = Array.from(objectsToTransform).find(
        (obj) => obj.getAttribute('name') === MagicStrings.NEW_CASE_DATACLASS
      )!;
      fragment = processor.transformToFragment(objectToTransform, document);
    }
    for (const i in fragment.childNodes) {
      // necessary
      // eslint-disable-next-line no-prototype-builtins
      if (Object.hasOwn(fragment.childNodes, i)) {
        resultCRF += fragment.childNodes[i].nodeValue;
      }
    }
    return resultCRF;
  }

  /**
   * Get the CRF map of all the variables in the xml
   * @param xml the xml string from which you want to extract the values
   * @param path string for import the correct stylesheet
   * @param crfName the name of the crf which you want to extract from
   */
  protected async getMapOfVariables(xml: string, path: string, crfName: string): Promise<Array<CRFVariableInterface>> {
    let resultCRF = await this.transformCrf(xml, path, crfName, 'dataClass');
    //create the json array, not easy to implement in xsl
    resultCRF = `[${resultCRF}]`;
    //remove the last comma, not easy to implement in xsl
    return JSON.parse(resultCRF.replace(/,([^,]*)$/, '$1')) as Array<CRFVariableInterface>;
  }

  protected async getNewCaseMapOfVariables(
    xml: string,
    path: string,
    crfName: string
  ): Promise<Array<CRFVariableInterface>> {
    let resultCRF = await this.transformCrf(xml, path, crfName, 'dataClass', true);
    //create the json array, not easy to implement in xsl
    resultCRF = `[${resultCRF}]`;
    //remove the last comma, not easy to implement in xsl
    return JSON.parse(resultCRF.replace(/,([^,]*)$/, '$1')) as Array<CRFVariableInterface>;
  }

  /**
   * Return a json string with all the valuesSet and the value present in the xml
   * @param xml the xml string from which you want to extract the values
   * @param path string for import the correct stylesheet
   */
  protected async getAllValuesSet(xml: string, path: string, crfName: string): Promise<Array<CRFValuesSetInterface>> {
    return JSON.parse(await this.transformCrf(xml, path, crfName, 'root')) as Array<CRFValuesSetInterface>;
  }

  protected async getAllDataClasses(xml: string, path: string, crfName: string): Promise<Array<CRFDataClassInterface>> {
    let resultCRF = await this.transformCrf(xml, path, crfName, 'dataClass');
    resultCRF = `[${resultCRF}]`;
    return JSON.parse(resultCRF.replace(/,(?=\s*\])/g, '')) as Array<CRFDataClassInterface>;
  }

  public async getMultipleXmlData(crfs: CRFConfigurationInterface[]): Promise<CrfData> {
    // create an object with array of allValues and allVariables foreach crf
    const crfDataArray = await Promise.all(
      crfs.map(async ({ crfName }) => {
        const crf = await this.getCRF(crfName, await this.getLatestVersion(crfName));
        //call the functions to get the arrays from xsl
        return this.getXmlData(crfName, crf.xml);
      })
    );
    // unify every crf array of Values and Variables into a single Array
    const allValues = crfDataArray.flatMap(({ allValues }) => allValues);
    const allVariables = crfDataArray.flatMap(({ allVariables }) => allVariables);
    const allDataClasses = crfDataArray.flatMap(({ allDataClasses }) => allDataClasses);
    //delete duplicate variables
    const uniqueAllVariables = Array.from(new Map(allVariables.map((item) => [item.fullName, item])).values());
    return { allValues, allVariables: uniqueAllVariables, allDataClasses };
  }

  private async getXmlData(crfName: string, xml: string): Promise<CrfData> {
    const [allValues, standardVariables, newCaseVariables, allDataClasses] = await Promise.all([
      this.getAllValuesSet(xml, '/Schemas/SchemaToGetEveryValuesSet.xsl', crfName),
      this.getMapOfVariables(xml, '/Schemas/SchemaToMapOfVariables.xsl', crfName),
      this.getNewCaseMapOfVariables(xml, '/Schemas/SchemaToMapOfVariables.xsl', crfName),
      this.getAllDataClasses(xml, '/Schemas/SchemaToGetEveryDataClass.xsl', crfName),
    ]);
    const allVariables = [...standardVariables, ...newCaseVariables];
    //delete duplicate variables
    const uniqueAllVariables = Array.from(new Map(allVariables.map((item) => [item.fullName, item])).values());
    return { allValues, allVariables: uniqueAllVariables, allDataClasses };
  }

  public getCrfNameFromVariable(variableName: string): string {
    return variableName.split('.')[0];
  }

  public getAccessLevelForVariable(variableName: string, crfVersions: Map<string, Version>) {
    const crfName = this.getCrfNameFromVariable(variableName);
    const crfVersion = crfVersions.get(crfName);
    if (!crfVersion) {
      throw new Error();
    }
    const accessLevels = this.getAccessLevelsForCRF(crfName, crfVersion);
    return Helpers.searchMostSpecificVariablePrefix(variableName, accessLevels);
  }
}

import axios, { AxiosResponse } from 'axios';
import { inject, injectable } from 'inversify';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { KeyStorageInterface, MultiKeysResponse } from '@/interfaces/KeyStorageInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { NamedBindings } from '@/types/NamedBindings';
import { StorageError, StorageErrorType } from '@/errors/StorageError';

@injectable()
export class RESTKeyStorage implements KeyStorageInterface {
  protected readonly BASE_URL;

  constructor(
    @inject(NamedBindings.UserService) protected userService: UserServiceInterface,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface
  ) {
    this.BASE_URL = this.coreConfiguration.baseUrlForKeys;
    axios.interceptors.request.use((config: any) => {
      config.headers = this.userService.getHeaders();
      return config;
    });
  }

  async saveKey(caseID: string, key: string, accessLevel: string): Promise<void> {
    const centreCode = this.userService.getCurrentUser().value?.centre || '*';
    await axios.post(`${this.BASE_URL}${centreCode}/${caseID}/${accessLevel}`, { value: key });
  }

  async getKey(caseID: string, accessLevel: string): Promise<string> {
    const centreCode = this.userService.getCurrentUser().value?.centre || '*';
    const array: Array<{ accessLevel: string; value: string }> = (
      await axios.get(`${this.BASE_URL}${centreCode}/${caseID}`)
    ).data;

    // Find the entry with the matching access level
    const entry = array.find((k) => k.accessLevel === accessLevel);

    if (!entry) {
      throw new Error(`Key not found for access level "${accessLevel}" in case "${caseID}"`);
    }

    return entry.value;
  }

  async getMultipleKeys(caseIDs: string[]): MultiKeysResponse {
    const centreCode = this.userService.getCurrentUser()?.value?.centre || '*';
    const url = `${this.BASE_URL}getKeys/${centreCode}`;

    try {
      const response = await axios.post(url, caseIDs);
      this.checkResponseStatus(response);
      const data = response.data;

      // Success cases
      const successCases = data.successCases.map((c: any) => ({
        caseID: c.caseID,
        accessLevel: c.accessLevel,
        key: c.key,
      }));

      // Failed cases
      const failedCases: Array<{ caseID: string; accessLevel: string; error: string }> = data.failedCases.map(
        (c: any) => ({
          caseID: c.caseID,
          accessLevel: c.accessLevel,
          error: c.error,
        })
      );

      return { successCases, failedCases };
    } catch (error: any) {
      // TODO: avoid throw and response with all failedCases? It's more consistent with interface
      throw new StorageError(`Failed to fetch keys: ${error?.message}`, StorageErrorType.GETTING_KEY_FAILED);
    }
  }

  checkResponseStatus(response: AxiosResponse<any, any>) {
    if (response.status < 200 || response.status > 299) {
      throw new StorageError(
        `Response status: ${response.status}, ${response.statusText}`,
        StorageErrorType.BAD_RESPONSE
      );
    }
  }
}

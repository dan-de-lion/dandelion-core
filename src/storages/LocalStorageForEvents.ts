import { injectable } from 'inversify';
import type { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import { DandelionService } from '@/services/DandelionService';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { ZcaseListSnapshots, ZcaseListSnapshotsSchema } from '@/utils/ValidationSchemas/SnapshotSchema';
import { ZsourceEvents, ZsourceEventsSchema } from '@/utils/ValidationSchemas/SourceEventSchema';
import { parseElementWithZod, ZodSchemas } from '@/utils/ValidationSchemas/handleZod';

@injectable()
export class LocalStorageForEvents extends DandelionService implements EventsStorageInterface {
  protected localStorage = window.localStorage;

  constructor(protected baseKey: string = 'events', protected baseSnapshotKey = 'snapshotEvents') {
    super();
  }

  async loadAllEvents(centreCode: string, caseType: string): Promise<Array<SourceEventForList>> {
    const storages = Object.keys(localStorage);
    const eventKey = `${this.baseKey}-${centreCode}-${caseType}`;
    const eventTypes = storages.filter((storage) => storage.startsWith(eventKey));
    let events: Array<SourceEventForList> = [];

    for (const eventType of eventTypes) {
      const parsedEvents: ZsourceEvents = parseElementWithZod(
        ZsourceEventsSchema,
        ZodSchemas.SOURCE_EVENT,
        JSON.parse(localStorage.getItem(eventType)!)
      );
      events.push(...parsedEvents);
    }
    const sortedEvents = events.sort((a, b) => {
      return a.timestamp.getTime() - b.timestamp.getTime();
    });
    return sortedEvents;
  }

  async saveEvent(event: SourceEventForList, centreCode: string, caseType: string): Promise<void> {
    let events: ZsourceEvents = [];
    const eventKey = `${this.baseKey}-${centreCode}-${caseType}`;
    const loadedEvents = localStorage.getItem(eventKey);
    if (loadedEvents) {
      events = parseElementWithZod(ZsourceEventsSchema, ZodSchemas.SOURCE_EVENT, JSON.parse(loadedEvents));
    }
    events.push(event);
    this.localStorage.setItem(eventKey, JSON.stringify(events));
  }

  protected getLoadedSnapshots(centreCode: string, caseType: string): ZcaseListSnapshots {
    let snapshots: ZcaseListSnapshots = [];
    const snapshotKey = `${this.baseSnapshotKey}-${centreCode}-${caseType}`;
    const loadedSnapshots = this.localStorage.getItem(snapshotKey);
    if (loadedSnapshots) {
      snapshots = parseElementWithZod(ZcaseListSnapshotsSchema, ZodSchemas.SNAPSHOT, JSON.parse(loadedSnapshots));
    }
    return snapshots;
  }

  async getLastSnapshotAndEvents(centreCode: string, caseType: string): Promise<SnapshotAndEvents> {
    const events = await this.loadAllEvents(centreCode, caseType);
    const snapshots: ZcaseListSnapshots = this.getLoadedSnapshots(centreCode, caseType);

    if (snapshots.length == 0) {
      return { events, snapshot: { cachedCaseList: [], date: new Date(''), numberOfEvents: 0 } };
    }
    // get the snapshot with the higher lastUpdate
    const lastSnapshot = snapshots.reduce((prev, current) => (prev.date > current.date ? prev : current));
    // get the events after the lastUpdate of the snapshot
    const eventsAfterSnapshot = events.filter((e) => e.timestamp > lastSnapshot.date);
    return { events: eventsAfterSnapshot, snapshot: lastSnapshot };
  }

  async saveSnapshot(snapshot: CaseListSnapshot, centreCode: string, caseType: string): Promise<void> {
    const snapshots: ZcaseListSnapshots = this.getLoadedSnapshots(centreCode, caseType);
    snapshot.date = new Date();
    snapshots.push(snapshot);
    this.localStorage.setItem(`${this.baseSnapshotKey}-${centreCode}-${caseType}`, JSON.stringify(snapshots));
  }
}

export interface AccessLevelsTree {
  [i: string]: AccessLevelsTree | { accessLevel: string } | undefined;
}

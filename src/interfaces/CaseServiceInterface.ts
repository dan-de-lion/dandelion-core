import { Case, CaseForUpdate } from '@/entities/Cases';
import type { CaseMetaData } from '@/entities/CaseMetaData';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import type { Motivations } from '@/utils/MapForMotivations';

export interface CaseServiceInterface {
  getDataFor(caseID: string): Promise<Case>;
  getCaseMetaDataFor(caseID: string): Promise<CaseMetaData>;
  getAllCaseMetaDataFor(caseType: string): Promise<Array<CaseMetaData>>;
  /**
   * This method uses the storage to save model and caseMetaData
   * @param caseID
   * @param caseMetaData
   * @param model
   * @throws error{ApplicationError}
   */
  create(caseToCreate: Case): Promise<void>;
  /**
   * This method uses the storage to save model and caseMetaData
   * @param caseID the caseID of the case to update
   * @param caseMetaData the caseMetaData to update
   * @param model the model to update
   * @throws error{ApplicationError} if the caseID is not found or there is an error while updating
   */
  update(caseForUpdate: CaseForUpdate): Promise<void>;
  deleteCase(caseID: string): Promise<void>;
  restoreCase(caseID: string): Promise<void>;
  freeze(caseID: string, accessLevelsToBeFroze?: string[] | undefined): Promise<void>;
  unfreeze(caseID: string, accessLevelsToBeFroze?: string[] | undefined): Promise<void>;
  lock(caseID: string): Promise<void>;
  unlock(caseID: string): Promise<void>;
  permanentlyDeleteCase(caseID: string, motivation: Motivations | string): Promise<void>;
  getSingleCaseHistory(caseID: string): Promise<HistoryPartialStoredCase[]>;
  getMultipleDataFor(caseIDs: string[]): Promise<Case[]>;
}

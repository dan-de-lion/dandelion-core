import type { CaseMetaData } from '@/entities/CaseMetaData';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';

export interface DataSynchStorageInterface {
  saveSynch(partialStoredCases: PartialStoredCase[], caseMetaData: CaseMetaData): Promise<void>;
  permanentlyDelete(caseID: string): Promise<void>;
}

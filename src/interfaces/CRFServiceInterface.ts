import { AccessLevelsTree } from './AccessLevelsTree';
import { CRFConfigurationInterface } from './CoreConfiguration';
import type { CRF, CRFVersionValidity, Version } from '@/entities/CRF';
import { CrfData } from '@/entities/CRFEntities';

export interface CRFServiceInterface {
  /**
   * Returns the CRF with the given name and version
   * @param crfName
   * @param crfVersion
   * @returns {Promise<CRF>}
   * @throws {CRFError} if the CRF is not found
   */
  getCRF(crfName: string, crfVersion: string): Promise<CRF>;
  /**
   * Returns the CRF versions that are active for the given CRF name (foreach Major, get the latest Minor)
   * @param crfName
   * @returns {Promise<CRF>}
   * @throws {CRFError} if the CRF configuration is not found
   */
  getCRFActiveVersions(crfName: string): Promise<Array<CRFVersionValidity>>;
  /**
   * Returns the AccessLevels for the given CRF name and version
   * @param crfName
   * @param crfVersion
   * @returns {Version}
   * @throws {CRFError} if the CRF configuration is not found
   */
  getAccessLevelsForCRF(crfName: string, crfVersion: Version): AccessLevelsTree;
  getAccessLevelForVariable(variableName: string, crfVersions: Map<string, Version>): string;
  getLatestVersion(crfName: string): Promise<Version>;
  isCRFPath(url: string): boolean;
  getMultipleXmlData(crfs: CRFConfigurationInterface[]): Promise<CrfData>;
  getCrfNameFromVariable(variableName: string): string;
}

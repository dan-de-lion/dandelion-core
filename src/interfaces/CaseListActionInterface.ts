import type { CaseListActionParams } from '@/interfaces/CaseListActionParams';

export interface CaseListActionInterface {
  name: string;
  actionClass: string;
  icon?: string;
  label?: string;
  action: (caseListActionParams: CaseListActionParams) => void;
  isVisible?: (caseListActionParams: CaseListActionParams) => boolean;
}

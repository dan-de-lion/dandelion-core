import { Language } from '@/services/TranslationService';
import type { Resource } from 'i18next';
import { injectable } from 'inversify';
import type { Component } from 'vue';
import type { ActionHookInterface } from '@/interfaces/ActionHookInterface';
import { AccessLevelAndCrfConfigurations } from '@/types/AccessLevels';
import { MagicNumbers } from '@/types/MagicNumbers';
import type { TypesOfActionHooks, TypesOfDisplayHooks } from '@/types/TypesOfHooks';

export declare type MarginForScroll = [number, number];

export interface CRFConfigurationInterface {
  readonly crfName: string;
}

export interface CaseLinkConfigurationInterface {
  readonly caseTypeName: string;
  readonly caseIDVariable: string;
}

export interface CaseConfigurationInterface {
  readonly caseName: string;
  readonly mainCrf?: CRFConfigurationInterface;
  readonly childrenCRFs?: Array<CRFConfigurationInterface>;
  readonly caseLinkingConfiguration?: Array<CaseLinkConfigurationInterface>;
  // the reference date is the variable used to select the CRF version
  // if the reference date, the reference date is the creation date of the case
  readonly referenceDateVariableName: string;
  readonly cachedVariables?: Array<string>;
}

export class CaseConfiguration implements CaseConfigurationInterface {
  readonly caseName: string = '';
  readonly mainCrf?: CRFConfigurationInterface;
  readonly childrenCRFs?: CRFConfigurationInterface[];
  readonly caseLinkingConfiguration?: CaseLinkConfigurationInterface[];
  readonly cachedVariables?: Array<string> = [];
  readonly referenceDateVariableName: string = '';

  constructor(config: CaseConfigurationInterface) {
    Object.assign(this, config);
  }

  getMainCRF(): CRFConfigurationInterface {
    return this.mainCrf ?? { crfName: this.caseName };
  }

  getAllCRFs(): CRFConfigurationInterface[] {
    return [this.getMainCRF(), ...(this.childrenCRFs ?? [])];
  }
}

export class DefaultRequiredForStatus {
  readonly general: number = MagicNumbers.coreConfigurationDefaultRequiredForStatusGeneral;
  readonly errors: number = MagicNumbers.coreConfigurationDefaultRequiredForStatusErrors;
  readonly warnings: number = MagicNumbers.coreConfigurationDefaultRequiredForStatusWarnings;
}

export interface CoreConfigurationInterface {
  readonly baseUrlForXslt: string;
  readonly baseUrlForOverrides?: string;
  readonly baseUrlForCases?: string;
  readonly baseUrlForKeys?: string;
  readonly baseUrlForEvents?: string;
  readonly baseUrlForCrfs?: string;
  readonly baseUrlForAuth?: string;
  readonly listOfOverridesSchemas?: string[];
  readonly encrypt?: boolean;
  readonly debug?: boolean;
  readonly statisticianMode?: boolean;
  readonly defaultLanguage?: Language;
  readonly additionalTranslations?: Resource;
  readonly listOfCaseTypes: CaseConfiguration[];
  readonly marginForScroll?: MarginForScroll;
  readonly accessLevelAndCrfConfigurations?: Array<{ accessLevel: string; crfName: string }>;
  readonly defaultRequiredForStatus?: DefaultRequiredForStatus;
  readonly pluggableDataTypes?: Record<string, Component>;
  readonly actionHooks?: Map<TypesOfActionHooks, ActionHookInterface>;
  readonly displayHooks?: Map<TypesOfDisplayHooks, Component[]>;
  readonly showDifferencesOnSave?: boolean;
  readonly debounceForUpdatingStatus?: number;
  readonly maxCasesLoadedPerRequest?: number;
}
@injectable()
export class CoreConfiguration implements CoreConfigurationInterface {
  readonly baseUrlForXslt: string = '/xslt/';
  readonly baseUrlForOverrides?: string = '/xslt-overrides/';
  readonly baseUrlForCases?: string = '/Rows/';
  readonly baseUrlForKeys?: string = '/Keys/';
  readonly baseUrlForEvents?: string = '/Events/';
  readonly baseUrlForCrfs?: string = '/crfs/';
  readonly baseUrlForAuth?: string = '/Auth/';
  readonly listOfOverridesSchemas?: string[] = [];
  readonly encrypt?: boolean = true;
  public debug = false;
  public statisticianMode = false;
  readonly defaultLanguage: Language = Language.en;
  readonly additionalTranslations: Resource = {};
  readonly listOfCaseTypes: CaseConfiguration[] = [];
  readonly marginForScroll?: MarginForScroll = [0, 0];
  readonly accessLevelAndCrfConfigurations?: AccessLevelAndCrfConfigurations = [{ accessLevel: '*', crfName: '*' }];
  readonly defaultRequiredForStatus?: DefaultRequiredForStatus = {
    general: MagicNumbers.coreConfigurationDefaultRequiredForStatusGeneral,
    errors: MagicNumbers.coreConfigurationDefaultRequiredForStatusErrors,
    warnings: MagicNumbers.coreConfigurationDefaultRequiredForStatusWarnings,
  };
  readonly pluggableDataTypes?: Record<string, Component>;
  readonly actionHooks?: Map<TypesOfActionHooks, ActionHookInterface> = new Map();
  readonly displayHooks?: Map<TypesOfDisplayHooks, Component[]> = new Map();
  readonly showDifferencesOnSave = false;
  readonly debounceForUpdatingStatus = 500;
  readonly maxCasesLoadedPerRequest = 100;

  constructor(config: CoreConfigurationInterface) {
    Object.assign(this, config);
  }
}

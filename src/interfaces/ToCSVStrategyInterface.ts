import type { CsvFile } from '@/entities/exportStrategy/CSVFile';
import { CaseMetaData } from '@/entities/CaseMetaData';

export interface ToCSVStrategy {
  getHeader(): Array<string>;
  getBody(parentIndexes: Array<number>, data: any, caseMetaData: CaseMetaData): Array<string>;
  getAdditionalCSVs(): Array<CsvFile>;
}

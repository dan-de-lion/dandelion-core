import { DeepReadonly, Ref, UnwrapNestedRefs, UnwrapRef } from 'vue';
import { FilterInterface } from './FilterInterface';
import { CaseListElement, ReadonlyCaseList } from '@/entities/CaseListElement';
import { CachedCaseListElement } from '@/services/CaseListService';

export interface CaseListServiceInterface {
  /** All cases in the list, no filters, no slices, no cached variables / metadata */
  readonly allCases: DeepReadonly<CachedCaseListElement[]>;
  /** All filtered cases, no slices, available only if some filter is active */
  readonly filteredCases: ReadonlyCaseList;
  /** All cases, filtered (if filters are active) and sliced: this is what the CaseListComponent should be showing */
  readonly slicedCases: ReadonlyCaseList;

  /** All deleted cases in the list, no filters, no slices, no cached variables / metadata */
  readonly allDeletedCases: DeepReadonly<CachedCaseListElement[]>;
  /** All deleted filtered cases, no slices, available only if some filter is active */
  readonly filteredDeletedCases: ReadonlyCaseList;
  /** All deleted cases, filtered (if filters are active) and sliced: this is what the DeletedCaseListComponent should be showing */
  readonly slicedDeletedCases: ReadonlyCaseList;

  /** Start of the slice / page, inclusive, i.e. if startList = 10, first element index is 10 */
  readonly startList: Ref<number>;
  /** End of the slice / page, exclusive, i.e. if endList = 20, last element index is 19 */
  readonly endList: Ref<number>;
  /** Start of the slice / page for deleted cases, inclusive, i.e. if startList = 10, first element index is 10 */
  readonly startDeletedList: Ref<number>;
  /** End of the slice / page for deleted cases, exclusive, i.e. if endList = 20, last element index is 19 */
  readonly endDeletedList: Ref<number>;

  // Methods to add events to the list
  addCase(id: string, timestamp: Date): Promise<void>;
  updateCase(id: string, timestamp: Date): Promise<void>;
  removeCase(id: string, timestamp: Date): Promise<void>;
  permanentlyDeleteCase(id: string, motivation: string, timestamp: Date): Promise<void>;
  restoreCase(id: string, timestamp: Date): Promise<void>;
  changedReferenceDate(id: string, changedReferenceDate: Date, timestamp: Date): Promise<void>;

  isUpdatingCaseList(): DeepReadonly<Ref<boolean>>;
  updateCaseList(): Promise<void>;

  getPermanentlyDeletedList(): Array<string>; // TODO: uniform with other lists

  // Utils
  retrieveCaseListElement(cachedCase: CachedCaseListElement): Promise<CaseListElement>;
  retrieveCaseListElements(cases: Array<CachedCaseListElement>): Promise<Array<CaseListElement>>;

  readonly areFiltersActive: Ref<boolean>;
  setFilters(arrayOfFilter: Array<FilterInterface>): Promise<void>;
}

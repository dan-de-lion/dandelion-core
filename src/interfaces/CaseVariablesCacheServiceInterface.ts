import { CaseListElement } from '@/entities/CaseListElement';
import { Case, CaseForUpdate } from '@/entities/Cases';

export interface CaseVariablesCacheServiceInterface {
  /**
   * @returns the CaseListElement if already present in cache, otherwise returns undefined
   */
  getCaseListElementFromCache(caseID: string): CaseListElement | undefined;
  invalidateCacheFor(caseID: string): void;
  updateCache(): void;
  createCacheOfSpecificCase(defaultCase: Case): void;
  updateCacheOfSpecificCase(caseForUpdate: CaseForUpdate): void;
  invalidateForAll(): void;
}

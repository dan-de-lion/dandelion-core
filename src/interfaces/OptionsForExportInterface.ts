export interface OptionsForExportInterface {
  splitColumnForSingleChoiceValues?: boolean;
  splitColumnForMultipleChoiceValues?: boolean;
  splitColumnForGroups?: boolean;
  csvSeparator?: string;
  exportLabelForVariableNames?: boolean;
  exportLabelForValues?: boolean;
  removeEmptyRows?: boolean;
  removeEmptyColumns?: boolean;
  removeCsvFilesWithoutRows?: boolean;
  removeCsvFilesWithoutColumns?: boolean;
  exportOnlyFilteredCases?: boolean;
}

import type { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';

export interface EventsStorageInterface {
  loadAllEvents(centreCode: string, caseType: string): Promise<Array<SourceEventForList>>;
  saveEvent(event: SourceEventForList, centreCode: string, caseType: string): Promise<void>;
  getLastSnapshotAndEvents(centreCode: string, caseType: string): Promise<SnapshotAndEvents>;
  saveSnapshot(snapshot: CaseListSnapshot, centreCode: string, caseType: string): Promise<void>;
}

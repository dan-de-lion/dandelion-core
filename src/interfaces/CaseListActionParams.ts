import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';

export interface CaseListActionParams {
  readonly element: ReadonlyCaseListElement;
  readonly elementIndex: number;
}

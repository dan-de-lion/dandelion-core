export interface MonitorQueryInterface {
  date: string;
  user: string;
  questionAnswer: string;
  status: string;
}

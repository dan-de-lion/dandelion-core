import { CaseMetaData } from '@/entities/CaseMetaData';
import type { Version } from '@/entities/CRF';
import { CRFConfigurationInterface } from './CoreConfiguration';

export interface CRFVersionSelectionStrategyInterface {
  /**
   * This method returns the CRF versions for the current caseType
   * Foreach CRF it returns the latest version using the selection strategy
   * @param model
   * @returns {Map<string, Version>} the CRF versions for the current caseType
   * @throws error{ApplicationError} if there is an error while getting the CRF versions
   */
  getUpdatedCRFVersionsByCase(model: any, caseMetaData: CaseMetaData): Promise<Map<string, Version>>;
  getUpdatedCRFVersionsByDate(referenceDate: Date): Promise<Map<string, Version>>;
  getCRFConfigurationByCase(model: any, caseMetaData: CaseMetaData): Promise<CRFConfigurationInterface[]>;
  // TODO: maybe find a better place for this, probably CRFService when it will be a real service and not bound to Shared
  getCRFConfigurationsByCentreCode(centreCode?: string): Promise<CRFConfigurationInterface[]>;
  /**
   * This method check if reference date is changed in two different models
   * @param oldModel
   * @param newModel
   * @returns {boolean}is or not changed
   * @throws error{ApplicationError} if there is an error while getting the CRF versions
   */
  getReferenceDateIfChanged(oldModel: any, newModel: any, caseMetaData: CaseMetaData): Date | false;
}

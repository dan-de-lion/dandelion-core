import { CrfWithValidity } from '@/entities/CrfWithValidity';

export interface ActivationPeriodsForCrfStorageInterface {
  getCrfsWithValidityForCentreCode(centreCode: string): Promise<CrfWithValidity[]>;
}

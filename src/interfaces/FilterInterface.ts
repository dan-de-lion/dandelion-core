import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';

export interface FilterInterface {
  apply(value: ReadonlyCaseListElement): boolean;
  isActive(): boolean;
}

export interface ActionHookInterface {
  run(context: string): Promise<boolean>;
}

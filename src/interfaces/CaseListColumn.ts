import { ReadonlyCaseListElement } from '@/entities/CaseListElement';

export interface CaseListColumn {
  getHeading(useLabel?: boolean): string;
  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string;
}

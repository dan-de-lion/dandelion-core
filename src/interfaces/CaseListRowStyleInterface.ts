import { ComputedRef } from 'vue';
import { ReadonlyCaseListElement } from '@/entities/CaseListElement';

export interface CaseListRowStyleInterface {
  applyStyle: (caseListElement: ReadonlyCaseListElement) => { [x: string]: ComputedRef<boolean> };
}

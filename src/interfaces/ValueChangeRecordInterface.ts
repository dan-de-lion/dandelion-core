export interface ValueChangeRecordInterface {
  date: string;
  user: string;
  reason: string;
  newValue: any;
}

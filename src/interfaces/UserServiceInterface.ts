import type { DeepReadonly, Ref, UnwrapNestedRefs } from 'vue';
import type { Version } from '@/entities/CRF';
import type { User } from '@/entities/User';
import type { AccessRights } from '@/types/AccessRights';

export interface UserServiceInterface {
  getCurrentUser(): DeepReadonly<UnwrapNestedRefs<Ref<User | null>>>;
  getHeaders(): any;
  canRead(variable: string, crfName: string, crfVersion: Version): boolean;
  canWrite(variable: string, crfName: string, crfVersion: Version): boolean;
  getAccessRights(): Map<string, AccessRights>;
  hasReadAccessRights(accessLevel: any): boolean;
  hasWriteAccessRights(accessLevel: any): boolean;
}

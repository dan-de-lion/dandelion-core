export interface CurrentPageServiceInterface {
  get(id: string): boolean;
  set(id: string, showParents?: boolean): void;
  scrollToVariable(fullNameForID: string, e?: Event): void;
}

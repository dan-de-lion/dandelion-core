export interface LoggerInterface {
  log: (message: string, viewOnScreen?: boolean) => void;
  logError: (message: string, viewOnScreen?: boolean) => void;
}

import type { ExportButtonParameters } from '@/entities/exportStrategy/ExportButtonParameters';

export interface CustomExportsStorageInterface {
  getCustomButtons(): Promise<Array<ExportButtonParameters>>;
  saveAllCustomButtons(buttons: Array<ExportButtonParameters>): Promise<boolean>;
  saveCustomButton(button: ExportButtonParameters): Promise<boolean>;
}

import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import type { CaseMetaData, SaveMetaData } from '@/entities/CaseMetaData';

export type MultiPartialsDTO = { caseID: string; partials: PartialStoredCase[] };
export type MultiPartialsErrorDTO = { caseID: string; error: string };
export type MultiPartialsResponse = Promise<{
  successCases: Array<MultiPartialsDTO>;
  failedCases: Array<MultiPartialsErrorDTO>;
}>;

export type MultiCaseMetaDTO = { caseID: string; caseMetaData: CaseMetaData };
export type MultiCaseMetaErrorDTO = { caseID: string; error: string };
export type MultiCaseMetaResponse = Promise<{
  successCases: Array<MultiCaseMetaDTO>;
  failedCases: Array<MultiCaseMetaErrorDTO>;
}>;

export interface CaseStorageInterface {
  /**
   * Returns the partials for the given caseID
   * @param caseID
   * @throws {StorageError} if there is no partials for the given caseID or there is an error while getting the case
   * @returns {Array<PartialStoredCase>} the partials for the given caseID
   */
  getPartials(caseID: string): Promise<PartialStoredCase[]>;

  /**
   * Allows to get Partials for multiple caseIDs with one call
   * Don' throw if some fails, it puts them in the response `failedCases`
   */
  getMultiplePartials(caseIDs: string[]): MultiPartialsResponse;

  /**
   * Returns the CaseMetaData for the given caseID
   * @param caseID
   */
  getCaseMetaData(caseID: string): Promise<CaseMetaData>;

  /**
   * Allows to get CaseMetaData for multiple caseIDs with one call
   * Don' throw if some fails, it puts them in the response `failedCases`
   */
  getMultipleCaseMetaData(caseIDs: string[]): MultiCaseMetaResponse;

  /**
   * Returns the CaseMetaData for all the cases
   * useful for reevaluation of cases, statistics on status etc...
   * @param caseType
   */
  getAllCaseMetaData(caseType: string): Promise<Array<CaseMetaData>>;

  /**
   * Saves the partials for the given caseID
   * @param caseID
   * @param partialStoredCases
   * @param saveMetaData
   * @throws {StorageError} if there is an error while saving
   */
  updatePartials(
    caseID: string,
    partialStoredCases: Array<PartialStoredCase>,
    saveMetaData: SaveMetaData
  ): Promise<void>;

  /**
   * Create the partials for the given caseID (in the CaseMetaData)
   * It is the only entry point to create a case, you must not pass the caseID in the partials, you have to place it in the CaseMetaData
   * @param partialStoredCases
   * @param caseMetaData
   * @throws {StorageError} if there is an error while saving
   */
  createCase(partialStoredCases: Array<PartialStoredCase>, caseMetaData: CaseMetaData): Promise<void>;

  /**
   * Returns the history of the partials for the given caseID
   * @param caseID
   * @param lastK
   * @throws {StorageError} if there is no partials for the given caseID or there is an error while getting the case
   * @returns {HistoryPartialStoredCase[]}
   */
  getCaseHistory(caseID: string, lastK?: number): Promise<HistoryPartialStoredCase[]>;

  /**
   * Returns the partial for the given caseID
   * @param caseID
   * @throws {StorageError} if there is no partial with the given caseID or there is an error while deleting
   */
  delete(caseID: string): Promise<void>;

  /**
   * Restore the partial for the given caseID
   * @param caseID
   * @throws {StorageError} if there is no partial with the given caseID or there is an error while restoring
   */
  restore(caseID: string): Promise<void>;

  /**
   * Permanently delete the partial for the given caseID
   * @param caseID
   * @throws {StorageError} if there is no partial with the given caseID or there is an error while deleting
   */
  permanentlyDelete(caseID: string): Promise<void>;

  freeze(caseID: string, frozenAccessLevels: string[]): Promise<void>;

  unfreeze(caseID: string, frozenAccessLevels: string[]): Promise<void>;

  lock(caseID: string): Promise<void>;

  unlock(caseID: string): Promise<void>;
}

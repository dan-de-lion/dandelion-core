export type MultiKeysDTO = { caseID: string; accessLevel: string; key: string };
export type MultiKeysErrorDTO = { caseID: string; accessLevel: string; error: string };
export type MultiKeysResponse = Promise<{ successCases: Array<MultiKeysDTO>; failedCases: Array<MultiKeysErrorDTO> }>;

export interface KeyStorageInterface {
  saveKey(caseID: string, key: string, accessLevel: string): Promise<void>;
  getKey(caseID: string, accessLevel: string): Promise<string>;
  getMultipleKeys(caseIDs: string[]): MultiKeysResponse;
}

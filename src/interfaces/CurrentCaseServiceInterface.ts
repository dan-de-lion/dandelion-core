import type { ComputedRef, DeepReadonly, Ref, UnwrapNestedRefs } from 'vue';
import { CaseMetaData } from '@/entities/CaseMetaData';
import type { Version } from '@/entities/CRF';

export interface CurrentCaseServiceInterface {
  getModel(): any;
  getPreviousModel(): any;
  getCrfVersions(): Readonly<Ref<ReadonlyMap<string, Version>>>;
  getPreviousCrfVersions(): Readonly<ReadonlyMap<string, Version>>;
  terminateEditing(): Promise<void>;
  open(caseID: string, model?: any, caseMetaData?: CaseMetaData): Promise<void>;
  save(): Promise<void>;
  canWrite(variable: string, crfName: string, crfVersion: Version): boolean;
  canRead(variable: string, crfName: string, crfVersion: Version): boolean;
  getCurrentCase(): DeepReadonly<UnwrapNestedRefs<Ref<string | undefined>>>;
  getFrozenAccessLevels(): Readonly<Ref<ReadonlyMap<string, boolean>>>;
  resetModel(): any;
  isCaseOpened: ComputedRef<boolean>;
  isModelChanged: Ref<boolean>;
}

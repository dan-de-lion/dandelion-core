import type { CaseCacheDTO } from '@/services/CaseVariablesCacheService';

export interface VariablesCacheStorageInterface {
  getCachedCases(): Map<string, CaseCacheDTO>;
  overwriteCache(cache: Map<string, CaseCacheDTO>): void;
}

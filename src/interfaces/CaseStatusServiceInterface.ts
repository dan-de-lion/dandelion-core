import type { Ref } from 'vue';
import { CrfStatus } from '@/services/CaseStatusService';

export interface CaseStatusServiceInterface {
  getStatus(): Ref<number>;
  startStatusService(availableCrfs: string[]): void;
  getListOfVariablesByAccessLevelAndStatus(): Readonly<
    Ref<ReadonlyMap<string, ReadonlyMap<number, ReadonlySet<string>>>>
  >;
  reset(): void;
  updateValue(variableName: string, value: any, accessLevel: string): void;
  finishedMounting(): void;
  registerVariable(variableName: string, accessLevel: string, statusLevel?: number): void;
  getStatusMap(): Ref<ReadonlyMap<string, CrfStatus>>;
  setHiddenVisibility(variable: string, accessLevel: string): void;
  updateStatus(): void;
  updateStatusFn(): void;
  addError(variable: string): void;
  removeError(variable: string): void;
  addWarning(variable: string): void;
  removeWarning(variable: string): void;
  addConfirmedWarning(variable: string): void;
  removeConfirmedWarning(variable: string): void;
  addRequirement(variable: string): void;
  removeRequirement(variable: string): void;
  getErrors(): Set<string>;
  getWarnings(): Set<string>;
  getConfirmedWarnings(): Set<string>;
  getMissingList(accessLevels: Array<string>, statuses: Array<number>, prefixes: Array<string>): Array<string>;
}

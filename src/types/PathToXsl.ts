export enum PathToXsl {
  SCHEMA_TO_COLLECTION_PAGE = 'Schemas/SchemaToCollectionPage.xsl',
  SCHEMA_TO_ERROR_LIST = 'Schemas/SchemaToErrorList.xsl',
  SCHEMA_TO_FILTER_PAGE = 'Schemas/SchemaToFilterPage.xsl',
  SCHEMA_TO_MISSING_LIST = 'Schemas/SchemaToMissingList.xsl',
  SCHEMA_TO_TREE = 'Schemas/SchemaToTree.xsl',
  SCHEMA_TO_WARNING_LIST = 'Schemas/SchemaToWarningList.xsl',
  SCHEMA_TO_DIFFS = 'Schemas/SchemaToDiffs.xsl',
  SCHEMA_TO_ACCESS_LEVELS = 'Schemas/SchemaToAccessLevels.xsl',
}

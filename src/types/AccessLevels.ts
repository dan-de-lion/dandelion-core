export enum AccessLevels {
  public = 'public',
}

export type AccessLevelAndCrfConfigurations = Array<{ accessLevel: string; crfName: string }>;

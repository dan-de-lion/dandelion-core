export declare type XSLT2Processor = {
  importStylesheetURI(url: string): Promise<void>;
  transformToFragment(obj: Element, document: Document): DocumentFragment;
  setParameter(context: string | null, variableName: string, variableValue: string);
};

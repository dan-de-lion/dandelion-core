const Contexts = {
  DEFAULT: 'default',
  FAST: 'fast',
  SHARED: 'shared',
  TEST: 'test',
};

export { Contexts };

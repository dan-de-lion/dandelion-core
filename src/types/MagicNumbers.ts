export enum MagicNumbers {
  coreConfigurationDefaultRequiredForStatusGeneral = 3,
  coreConfigurationDefaultRequiredForStatusErrors = 2,
  coreConfigurationDefaultRequiredForStatusWarnings = 2,
  maxDepthForCRFComponentAndTree = 1000,
  eventsForNewSnapshot = 10,
  periodicallyStatusCheckTime = 10000,
  periodicallyProcessQueueTime = 100,
  preventMultipleClicksOnNewCaseTime = 1000,
  localStorageLockExpiresMinutes = 5,
  RestCaseStorageCacheExpiresMinutes = 0.5,
  RestEventStorageCacheExpiresMinutes = 1,
  minuteInMS = 60000,
  lenghtOfAbbreviatedCaseID = 6,
  periodicallyRunReevaluationMinutes = 60, //1 hour
}

export enum ApplicationEvents {
  CaseClosed = 'CaseClosed',
  CaseCreated = 'CaseCreated',
  CaseCreationCanceled = 'CaseCreationCanceled',
  CaseCreationFailed = 'CaseCreationFailed',
  CaseDeleted = 'CaseDeleted',
  CaseDeletionFailed = 'CaseDeletionFailed',
  CaseFreezed = 'CaseFreezed',
  CaseListUpdated = 'CaseListUpdated',
  CaseLocked = 'CaseLocked',
  CaseOpenedForEditing = 'CaseOpenedForEditing',
  CaseOpenedForCreation = 'CaseOpenedForCreation',
  CaseOpenedForReevaluation = 'CaseOpenedForReevaluation',
  CaseOpeningFailed = 'CaseOpeningFailed',
  CasePermanentlyDeleted = 'CasePermanentlyDeleted',
  CasePermanentlyDeletionFailed = 'CasePermanentlyDeletionFailed',
  CaseRestored = 'CaseRestored',
  CaseRestoringFailed = 'CaseRestoringFailed',
  CaseUnfreezed = 'CaseUnfreezed',
  CaseUnlocked = 'CaseUnlocked',
  CaseUpdated = 'CaseUpdated',
  CaseUpdateFailed = 'CaseUpdateFailed',
  NewCaseRequested = 'NewCaseRequested',
  ReferenceDateChanged = 'ReferenceDateChanged',
  VariableHovered = 'VariableHovered',
  CRFMounted = 'CRFMounted',
  OpenCaseRequested = 'OpenCaseRequested',
}

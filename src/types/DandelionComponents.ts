export enum DandelionComponents {
  SCC = 'single-case-component',
  NCC = 'new-case-component',
  TREE = 'tree-component',
  FAST = 'fast-compilation-component',
  DIFF = 'diff-list-component',
  ERRORS = 'errors-list-component',
  MISSINGS = 'missings-list-component',
  WARNINGS = 'warnings-list-component',
  DEV = 'developer-errors-render-component',
}

export type AccessRights = { read_roles: Array<string>; write_roles: Array<string> };

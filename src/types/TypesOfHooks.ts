export enum TypesOfDisplayHooks {
  AfterVariable = 'AfterVariable',
  AfterChangedVariable = 'AfterChangedVariable',
  BeforeVariable = 'BeforeVariable',
}

export enum TypesOfActionHooks {
  BeforeSaving = 'BeforeSaving',
  BeforeCreating = 'BeforeCreating',
  BeforeNewCaseRequesting = 'BeforeNewCaseRequesting',
  BeforeDeleting = 'BeforeDeleting',
  BeforePermanentlyDeleting = 'BeforePermanentlyDeleting',
  BeforeCreationCanceling = 'BeforeCreationCanceling',
  BeforeExporting = 'BeforeExporting',
  BeforeClosing = 'BeforeClosing',
}

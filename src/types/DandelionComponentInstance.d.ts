import type { ComponentPublicInstance } from './vue/dist/vue.esm-bundler.js';
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';

export declare type DandelionComponentInstance = ComponentPublicInstance & {
  model: any;
  context: string;
  caseStatusService: CaseStatusServiceInterface;
  currentCaseService: CurrentCaseServiceInterface;
  coreConfigurationService: CoreConfigurationInterface;
};

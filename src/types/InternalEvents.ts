export enum InternalEvents {
  ComputedItemsUpdated = 'ComputedItemsUpdated',
  ComputedValueUpdated = 'ComputedValueUpdated',
}

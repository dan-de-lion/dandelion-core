export class ValuesSetValue extends String {
  constructor(protected value: string, protected label?: string, protected numericalValue?: number) {
    super(value);
  }

  getValue() {
    return this.value;
  }

  getLabel() {
    return this.label ?? this.value;
  }

  getNumericalValue() {
    return this.numericalValue ?? NaN;
  }

  equals(otherValue: ValuesSetValue | string) {
    return this.toString() === otherValue.toString();
  }
}

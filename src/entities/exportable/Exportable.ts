import { CaseMetaData } from '@/entities/CaseMetaData';

export abstract class Exportable {
  readonly variableName;
  protected constructor(protected heading: string, variableName?: string) {
    this.variableName = variableName ?? '';
  }
  getHeading(useLabel?: boolean): string {
    return useLabel ? this.heading : this.variableName;
  }
  abstract getExportableValue(
    parentIndexes: Array<number>,
    data: any,
    caseMetaData: CaseMetaData,
    useLabel?: boolean
  ): string;
}

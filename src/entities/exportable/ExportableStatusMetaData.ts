import { CaseMetaData } from '@/entities/CaseMetaData';
import { Exportable } from '@/entities/exportable/Exportable';

export class ExportableStatusMetaData extends Exportable {
  constructor(heading: string, variableName: string, protected crfName: string) {
    super(heading, variableName);
  }

  /**
   * This method returns the values for each element present in the columns
   * @param _parentIndexes
   * @param _data
   * @param caseMetaData
   * @returns the label of the cachedVariable if useLabel is true, else returns the value
   */
  getExportableValue(_parentIndexes: Array<number>, _data: any, caseMetaData: CaseMetaData): string {
    let caseListElementSelectedStatus: any;

    if (caseMetaData.status.has(this.crfName)) {
      caseListElementSelectedStatus = caseMetaData.status.get(this.crfName);
      if (!caseListElementSelectedStatus) {
        // Handle the error appropriately
        throw new Error(`Status not found for ${this.crfName}`);
      }

      return caseListElementSelectedStatus.toString();
    }
    return '';
  }
}

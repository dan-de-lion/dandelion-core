import Helpers from '@/utils/Helpers';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { Exportable } from '@/entities/exportable/Exportable';

export class ExportableMetaData extends Exportable {
  /**
   *
   * @param heading
   * @param variableName
   * @param meta is the name of the metaData to export
   * @param labelMap
   */
  constructor(heading: string, variableName: string, protected meta: string, protected labelMap?: Map<string, string>) {
    super(heading, variableName);
  }

  /**
   * This method returns the values for each element present in the columns
   * @param _parentIndexes
   * @param _data
   * @param caseMetaData
   * @param useLabel is a parameter which is used to select if the method has to return the label of each caseListElement
   * @returns the label of the cachedVariable if useLabel is true, else returns the value
   */
  getExportableValue(_parentIndexes: Array<number>, _data: any, caseMetaData: CaseMetaData, useLabel = true): string {
    let caseListElementSelectedMeta: any;

    if (this.meta in caseMetaData) {
      caseListElementSelectedMeta = Helpers.getObjByPath(this.meta, caseMetaData);

      if (useLabel && this.labelMap && this.labelMap.has(caseListElementSelectedMeta)) {
        return this.labelMap?.get(caseListElementSelectedMeta.toString()) ?? '';
      }

      if (Object.prototype.toString.call(caseListElementSelectedMeta) === '[object Date]') {
        return (caseListElementSelectedMeta as Date).toLocaleString('en-GB', {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit',
          hour12: false, // Use 24-hour format
        });
      }
      if (Object.prototype.toString.call(caseListElementSelectedMeta) === '[object Map]') {
        return (Array.from(caseListElementSelectedMeta) as Array<any>)
          .map(([key, value]) => `${key}: ${value}`)
          .join(', ');
      }
      return caseListElementSelectedMeta.toString();
    }
    return '';
  }
}

import { CaseMetaData } from '@/entities/CaseMetaData';
import { Exportable } from '@/entities/exportable/Exportable';

export class ExportableCrfVersionMetaData extends Exportable {
  constructor(heading: string, variableName: string, protected crfName: string, protected label?: string) {
    super(heading, variableName);
  }

  /**
   * This method returns the values for each element present in the columns
   * @param _parentIndexes
   * @param _data
   * @param caseMetaData
   * @param useLabel is a parameter which is used to select if the method has to return the label of each caseListElement
   * @returns the label of the cachedVariable if useLabel is true, else returns the value
   */
  getExportableValue(_parentIndexes: Array<number>, _data: any, caseMetaData: CaseMetaData, useLabel = true): string {
    if (caseMetaData.crfVersions.has(this.crfName)) {
      //TODO: error management
      const caseListElementSelectedStatus = caseMetaData.crfVersions.get(this.crfName);
      if (!caseListElementSelectedStatus) {
        // Handle the error appropriately
        throw new Error(`CRF version not found for ${this.crfName}`);
      }

      if (useLabel && this.label) {
        return this.label;
      }
      return caseListElementSelectedStatus;
    }
    return '';
  }
}

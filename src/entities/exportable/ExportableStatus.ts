import { CaseMetaData } from '@/entities/CaseMetaData';
import { Exportable } from '@/entities/exportable/Exportable';

export class ExportableStatus extends Exportable {
  constructor(heading: string, variableName: string, protected crfName: string) {
    super(heading, variableName);
  }

  /**
   * This method returns the values for each element present in the columns
   * @param _parentIndexes
   * @param _data
   * @param caseMetaData
   * @returns the label of the cachedVariable if useLabel is true, else returns the value
   */
  getExportableValue(_parentIndexes: Array<number>, _data: any, caseMetaData: CaseMetaData): string {
    if (!caseMetaData.status.has(this.crfName)) {
      return '';
    }
    return caseMetaData.status.get(this.crfName)?.toString() ?? '';
  }
}

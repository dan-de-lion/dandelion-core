import { CaseMetaData } from '@/entities/CaseMetaData';
import { Exportable } from '@/entities/exportable/Exportable';

export class ExportableFreeze extends Exportable {
  constructor(heading: string) {
    super(heading);
  }

  getExportableValue(_parentIndexes: number[], _data: any, caseMetaData: CaseMetaData): string {
    //if the accessLevel is not in the map, we would to show a white cells in the csv
    const isFrozen = caseMetaData.frozenAccessLevels.get(this.heading) ?? '';
    return isFrozen.toString();
  }
  getHeading(): string {
    return this.heading;
  }
}

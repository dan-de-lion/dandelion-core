import { CaseMetaData } from '@/entities/CaseMetaData';
import { Exportable } from '@/entities/exportable/Exportable';

export class ExportableId extends Exportable {
  constructor(heading: string, variableName: string, protected lengthOfDisplayedId: number) {
    super(heading, variableName);
  }

  /**
   * This method returns the values for each element present in the columns
   * @param _parentIndexes
   * @param _data
   * @param caseMetaData
   * @returns the label of the cachedVariable if useLabel is true, else returns the value
   */
  getExportableValue(_parentIndexes: Array<number>, _data: any, caseMetaData: CaseMetaData): string {
    return caseMetaData.caseID.substring(0, this.lengthOfDisplayedId);
  }
}

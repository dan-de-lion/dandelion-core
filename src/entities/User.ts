export class User {
  constructor(readonly username: string, readonly centre: string, readonly role: string) {}
}

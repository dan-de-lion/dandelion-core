import type { OptionsForExportInterface } from '@/interfaces/OptionsForExportInterface';

export declare type ExportButtonParameters = {
  id: string;
  description: string;
  icon: string;
  options: OptionsForExportInterface;
};

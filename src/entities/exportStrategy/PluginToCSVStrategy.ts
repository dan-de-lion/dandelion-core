import _ from 'lodash';
import { DefaultToCSVStrategy } from '@/entities/exportStrategy/DefaultToCSVStrategy';
import type { OptionsForExportInterface } from '@/interfaces/OptionsForExportInterface';
import type { CsvFile } from '@/entities/exportStrategy/CSVFile';
import type { CsvTable } from '@/entities/exportStrategy/CSVTable';
import type { CRFVariableInterface } from '@/entities/CRFEntities';
import Helpers from '@/utils/Helpers';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { ExportConfiguration } from '@/services/CaseDataExporterService';

export class PluginToCSVStrategy extends DefaultToCSVStrategy {
  protected additionalCSV: CsvFile = { info: { name: 'PLUGIN' }, csv: [] };
  protected headings: Array<string> = ['CASE_GUID'];
  protected bodies: CsvTable = [];

  constructor(
    protected selectedVariable: CRFVariableInterface,
    exportableVariable: ExportableVariable,
    exportConfig: ExportConfiguration
  ) {
    super(exportableVariable, exportConfig);
  }

  getHeader(): Array<string> {
    return [];
  }

  getBody(parentIndexes: Array<number>, data: any, caseMetaData: CaseMetaData): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    let cachedVariable;

    if (!Helpers.getObjByPath(this.selectedVariable.fullName, data)) {
      cachedVariable = '';
    } else {
      cachedVariable = Helpers.getObjByPath(this.selectedVariable.fullName, data) ?? '';
    }

    const pluginToBeExported: any = _.cloneDeep(cachedVariable);

    if (pluginToBeExported instanceof Array) {
      if (pluginToBeExported.length === 0) {
        return [];
      }

      if (this.headings.length === 1) {
        pluginToBeExported.forEach((element: any) => {
          for (const key in element) {
            if (Object.prototype.hasOwnProperty.call(element, key) && !this.headings.includes(key)) {
              this.headings.push(key);
            }
          }
        });
      }

      pluginToBeExported.forEach((element: any) => {
        const itemBody = [caseMetaData.caseID];
        for (const key in element) {
          if (Object.prototype.hasOwnProperty.call(element, key)) {
            itemBody.push(element[key]); // Add the property value to valuesArray
          }
        }
        this.bodies.push(itemBody);
      });

      return [];
    }

    this.headings.push(...super.getHeader());
    const itemBody = [caseMetaData.caseID];
    itemBody.push(...super.getBody(parentIndexes, data, caseMetaData));
    this.bodies.push(itemBody);

    return [];
  }

  getAdditionalCSVs(): Array<CsvFile> {
    this.bodies.unshift(this.headings);
    this.additionalCSV.csv = this.bodies;

    return [this.additionalCSV];
  }
}

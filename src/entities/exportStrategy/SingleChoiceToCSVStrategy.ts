import { DefaultToCSVStrategy } from '@/entities/exportStrategy/DefaultToCSVStrategy';
import type { CRFValueInterface, CRFValuesSetInterface, CRFVariableInterface } from '@/entities/CRFEntities';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { ExportConfiguration } from '@/services/CaseDataExporterService';

export class SingleChoiceToCSVStrategy extends DefaultToCSVStrategy {
  constructor(
    protected allValues: Array<CRFValuesSetInterface>,
    protected selectedVariable: CRFVariableInterface,
    exportableVariable: ExportableVariable,
    exportConfig: ExportConfiguration
  ) {
    super(exportableVariable, exportConfig);
  }

  getValuesSet(allValues: Array<CRFValuesSetInterface>, selectedVariable: CRFVariableInterface): CRFValuesSetInterface {
    const selectedValuesSet = allValues.find((valuesSet) => valuesSet.valuesSetName === selectedVariable.valuesSet);
    if (!selectedValuesSet) {
      throw new CRFError(
        `No values Set with this name: ${selectedVariable.valuesSet}`,
        CRFErrorType.VALUES_SET_NOT_FOUND
      );
    }
    return selectedValuesSet;
  }

  getHeader(): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    if (this.exportConfig.options && this.exportConfig.options.splitColumnForSingleChoiceValues) {
      const selectedValuesSet = (this.allValues as Array<CRFValuesSetInterface>).find(
        (valuesSet) => valuesSet.valuesSetName === (this.selectedVariable as CRFVariableInterface).valuesSet
      );
      return selectedValuesSet!.valuesSetList.map((xValue) => `${selectedValuesSet!.valuesSetName}.${xValue.name}`);
    }
    return super.getHeader();
  }

  getBody(parentIndexes: Array<number>, data: any, caseMetaData: CaseMetaData): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    if (this.exportConfig.options && this.exportConfig.options.splitColumnForSingleChoiceValues) {
      const selectedValuesSet = this.getValuesSet(this.allValues, this.selectedVariable);
      const content = this.exportableVariable.getExportableValue(parentIndexes, data, caseMetaData);
      return (
        selectedValuesSet?.valuesSetList.map((xValue: CRFValueInterface) =>
          content == `${selectedValuesSet?.valuesSetName}.${xValue.name}` ? 'true' : 'false'
        ) ?? []
      );
    }
    return super.getBody(parentIndexes, data, caseMetaData);
  }
}

import { ToCSVStrategyFactory } from '@/entities/exportStrategy/ToCSVStrategyFactory';
import { DefaultToCSVStrategy } from '@/entities/exportStrategy/DefaultToCSVStrategy';
import type { ToCSVStrategy } from '@/interfaces/ToCSVStrategyInterface';
import type { OptionsForExportInterface } from '@/interfaces/OptionsForExportInterface';
import type {
  CRFDataClassInterface,
  CRFDataClassChildrenVariableInterface,
  CRFValuesSetInterface,
  CRFVariableInterface,
} from '@/entities/CRFEntities';
import type { CsvFile } from '@/entities/exportStrategy/CSVFile';
import type { CsvTable } from '@/entities/exportStrategy/CSVTable';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { ExportConfiguration } from '@/services/CaseDataExporterService';

export class SetToCSVStrategy extends DefaultToCSVStrategy {
  protected additionalCSV: Array<CsvFile> = [{ info: { name: '' }, csv: [] }];
  protected headings: Array<Array<string>> = [['CASE_GUID']];
  protected bodies: CsvTable = [];
  protected internalExportables: Array<ToCSVStrategy> = [];

  constructor(
    protected allVariables: Array<CRFVariableInterface>,
    protected allValues: Array<CRFValuesSetInterface>,
    protected allDataClasses: Array<CRFDataClassInterface>,
    protected selectedVariable: CRFVariableInterface | CRFDataClassChildrenVariableInterface,
    exportableVariable: ExportableVariable,
    protected depth = 0,
    exportConfig: ExportConfiguration
  ) {
    super(exportableVariable, exportConfig);

    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      !this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      this.additionalCSV[0].info.name = this.exportableVariable.variableName.replace(/\[[^\]]*\]/g, '');

      const selectedDataClass = this.findSelectedDataClass();

      if (!selectedDataClass) {
        return;
      }

      this.internalExportables.push(
        ...selectedDataClass.dataClassList.map((child: CRFDataClassChildrenVariableInterface) => {
          const name = child.fullName.split('.').pop();
          const exportableVariable = new ExportableVariable(
            child.fullName,
            `${this.exportableVariable.variableName}[index${this.depth.toString()}].${name}`
          );
          return ToCSVStrategyFactory.getStrategy(
            exportableVariable,
            this.exportConfig,
            this.allValues,
            this.allVariables,
            this.allDataClasses,
            this.depth + 1
          );
        })
      );
    }
  }

  getHeader(): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    if (this.headings.length === 1) {
      const itemHeader = [];
      for (let i = 0; i <= this.depth; i++) {
        itemHeader.push(`index${i}`);
      }
      itemHeader.push('guid');
      const itemToInsert = this.internalExportables.flatMap((exportable) => exportable.getHeader());
      itemHeader.push(...itemToInsert);
      this.headings.push(itemHeader);
    }

    return [];
  }

  getBody(parentIndexes: Array<number>, data: any, caseMetaData: CaseMetaData): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    const selectedDataClass = this.findSelectedDataClass();

    if (selectedDataClass && selectedDataClass.dataClassList) {
      const content = this.exportableVariable.getContent(parentIndexes, data);
      content?.forEach((xItem: any, index: number) => {
        const variablesItemBody = this.internalExportables.flatMap((exportable) =>
          exportable.getBody(parentIndexes.concat(index), data, caseMetaData)
        );
        const itemBody: string[] = [
          caseMetaData.caseID,
          ...parentIndexes.map((v) => v.toString()),
          index.toString(),
          xItem.guid,
          ...variablesItemBody,
        ];
        this.bodies.push(itemBody);
      });
    }

    return [];
  }

  findSelectedDataClass(): CRFDataClassInterface | undefined {
    const isCRFVariableInterface = (
      b: CRFVariableInterface | CRFDataClassChildrenVariableInterface
    ): b is CRFVariableInterface => (b as CRFVariableInterface).fullName !== undefined;

    const mySelectedVariable = this.selectedVariable;

    if (isCRFVariableInterface(mySelectedVariable)) {
      const variableToFind = this.allVariables.find((xVariable) => xVariable.fullName === mySelectedVariable.fullName);

      return (this.allDataClasses as Array<CRFDataClassInterface>).find(
        (dataClass) => variableToFind && dataClass.dataClassName === variableToFind.itemsDataClass
      );
    }
    return (this.allDataClasses as Array<CRFDataClassInterface>).find(
      (dataClass) => dataClass.dataClassName === this.selectedVariable.itemsDataClass
    );
  }

  getAdditionalCSVs(): Array<CsvFile> {
    this.bodies.unshift(this.headings.flatMap((header) => header));
    this.additionalCSV[0].csv = this.bodies;

    const childrenAdditionalCSV = this.internalExportables.flatMap((exportable) => exportable.getAdditionalCSVs());

    return [...this.additionalCSV, ...childrenAdditionalCSV];
  }
}

import { DefaultToCSVStrategy } from '@/entities/exportStrategy/DefaultToCSVStrategy';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import type { OptionsForExportInterface } from '@/interfaces/OptionsForExportInterface';
import type { CsvFile } from '@/entities/exportStrategy/CSVFile';
import {
  type CRFDataClassInterface,
  type CRFDataClassChildrenVariableInterface,
  type CRFValuesSetInterface,
  CRFVariableInterface,
} from '@/entities/CRFEntities';
import { ToCSVStrategyFactory } from '@/entities/exportStrategy/ToCSVStrategyFactory';
import { CaseMetaData } from '../CaseMetaData';
import { ExportConfiguration } from '@/services/CaseDataExporterService';

export class GroupToCSVStrategy extends DefaultToCSVStrategy {
  constructor(
    protected allVariables: Array<CRFVariableInterface>,
    protected allValues: Array<CRFValuesSetInterface>,
    protected allDataClass: Array<CRFDataClassInterface>,
    protected selectedVariable: CRFVariableInterface,
    protected exportableVariable: ExportableVariable,
    exportConfig: ExportConfiguration
  ) {
    super(exportableVariable, exportConfig);
  }

  getBody(parentIndexes: Array<number>, data: any, caseMetaData: CaseMetaData): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    const selectedDataClass = this.findSelectedDataClass();
    let result: Array<string> = [];
    if (selectedDataClass && selectedDataClass.dataClassList) {
      selectedDataClass.dataClassList?.forEach((child: CRFDataClassChildrenVariableInterface) => {
        const exportableVariable = new ExportableVariable(child.fullName, `${child.fullName}`);
        const strategy = ToCSVStrategyFactory.getStrategy(
          exportableVariable,
          this.exportConfig,
          this.allValues,
          this.allVariables,
          this.allDataClass,
          0
        );
        const body = strategy.getBody(parentIndexes, data, caseMetaData);
        if (this.exportConfig.options && this.exportConfig.options.splitColumnForGroups) {
          result = result.concat(body);
        } else {
          //@ts-expect-error
          result.push(body);
        }
      });
    }

    return result;
  }

  getHeader(): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }

    const selectedDataClass = this.findSelectedDataClass();
    let result: Array<string> = [];
    if (selectedDataClass && selectedDataClass.dataClassList) {
      selectedDataClass.dataClassList?.forEach((child: CRFDataClassChildrenVariableInterface) => {
        const exportableVariable = new ExportableVariable(
          child.label !== '' && this.exportConfig.options?.exportLabelForVariableNames ? child.label : child.fullName,
          `${child.fullName}`
        );
        const strategy = ToCSVStrategyFactory.getStrategy(
          exportableVariable,
          this.exportConfig,
          this.allValues,
          this.allVariables,
          this.allDataClass,
          0
        );
        const header = strategy.getHeader();
        if (this.exportConfig.options?.splitColumnForGroups) {
          result = result.concat(header);
        } else {
          //@ts-expect-error
          result.push(header);
        }
      });
    }

    return result;
  }

  getAdditionalCSVs(): Array<CsvFile> {
    return [];
  }

  findSelectedDataClass(): CRFDataClassInterface | undefined {
    const isCRFGroupWithDataClass = (b: CRFVariableInterface): b is CRFVariableInterface => {
      return (b as CRFVariableInterface).type === 'group' && (b as CRFVariableInterface).dataClass !== undefined;
    };

    if (isCRFGroupWithDataClass(this.selectedVariable)) {
      const variableToFind = this.allVariables.find(
        (xVariable) => xVariable.fullName === this.selectedVariable.fullName
      );
      const result = (this.allDataClass as Array<CRFDataClassInterface>).find(
        (dataClass) => variableToFind && dataClass.dataClassName === variableToFind.dataClass
      );
      return result;
    }
    return (this.allDataClass as Array<CRFDataClassInterface>).find(
      (dataClass) => dataClass.dataClassName === this.selectedVariable.fullName
    );
  }
}

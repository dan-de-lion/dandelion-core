import type { ToCSVStrategy } from '@/interfaces/ToCSVStrategyInterface';
import type { CsvFile } from '@/entities/exportStrategy/CSVFile';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { ExportConfiguration } from '@/services/CaseDataExporterService';

export class DefaultToCSVStrategy implements ToCSVStrategy {
  constructor(protected exportableVariable: ExportableVariable, protected exportConfig: ExportConfiguration) {}

  getBody(parentIndexes: Array<number>, data: any, caseMetaData: CaseMetaData): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }
    return [
      this.exportableVariable.getExportableValue(
        parentIndexes,
        data,
        caseMetaData,
        this.exportConfig.options ? this.exportConfig.options.exportLabelForValues : false
      ),
    ];
  }

  getHeader(): Array<string> {
    //second screening of excluded variables
    if (
      this.exportConfig &&
      this.exportConfig.excludedVariables &&
      this.exportConfig.excludedVariables.includes(this.exportableVariable.variableName)
    ) {
      return [];
    }
    return [
      this.exportableVariable.getHeading(
        this.exportConfig.options ? this.exportConfig.options.exportLabelForVariableNames : undefined
      ),
    ];
  }

  getAdditionalCSVs(): Array<CsvFile> {
    return [];
  }
}

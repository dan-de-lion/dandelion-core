import type { ValuesSetValue } from '@/entities/ValuesSetValue';

export type CsvTable = Array<Array<string | ValuesSetValue | ValuesSetValue[]>>;

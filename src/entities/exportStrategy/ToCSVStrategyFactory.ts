import type { ToCSVStrategy } from '@/interfaces/ToCSVStrategyInterface';
import { DefaultToCSVStrategy } from '@/entities/exportStrategy/DefaultToCSVStrategy';
import { SingleChoiceToCSVStrategy } from '@/entities/exportStrategy/SingleChoiceToCSVStrategy';
import { MultipleChoiceToCSVStrategy } from '@/entities/exportStrategy/MultipleChoiceToCSVStrategy';
import { SetToCSVStrategy } from '@/entities/exportStrategy/SetToCSVStrategy';
import { PluginToCSVStrategy } from '@/entities/exportStrategy/PluginToCSVStrategy';
import type { CRFDataClassInterface, CRFValuesSetInterface, CRFVariableInterface } from '@/entities/CRFEntities';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import { GroupToCSVStrategy } from '@/entities/exportStrategy/GroupToCSVStrategy';
import { ExportConfiguration } from '@/services/CaseDataExporterService';

export class ToCSVStrategyFactory {
  static getStrategy(
    exportableVariable: ExportableVariable,
    exportConfig: ExportConfiguration,
    allValues: Array<CRFValuesSetInterface>,
    allVariables: Array<CRFVariableInterface>,
    allDataClasses: Array<CRFDataClassInterface>,
    depth: number
  ): ToCSVStrategy {
    //We use this replace to remove all indexing in a set children: crfName.set[index0].child => crfName.set.child
    const fullName = exportableVariable.variableName.replace(/\[[^\]]*\]/g, '');

    const selectedVariable = allVariables.find((xVariable) => xVariable.fullName === fullName);

    if (!selectedVariable) {
      throw new ApplicationError(
        `No variable with this fullName: ${fullName} when exporting, check caseListColumn passed to exporter`,
        AppErrorType.VARIABLE_NOT_FOUND
      );
    }

    switch (selectedVariable.type) {
      case 'singlechoice':
        return new SingleChoiceToCSVStrategy(allValues, selectedVariable, exportableVariable, exportConfig);
      case 'multiplechoice':
        return new MultipleChoiceToCSVStrategy(allValues, selectedVariable, exportableVariable, exportConfig);
      case 'group':
        return new GroupToCSVStrategy(
          allVariables,
          allValues,
          allDataClasses,
          selectedVariable,
          exportableVariable,
          exportConfig
        );
      case 'set':
        return new SetToCSVStrategy(
          allVariables,
          allValues,
          allDataClasses,
          selectedVariable,
          exportableVariable,
          depth,
          exportConfig
        );
      case 'plugin':
        return new PluginToCSVStrategy(selectedVariable, exportableVariable, exportConfig);
      default:
        return new DefaultToCSVStrategy(exportableVariable, exportConfig);
    }
  }
}

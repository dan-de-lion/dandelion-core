import type { CsvTable } from '@/entities/exportStrategy/CSVTable';

export type CsvFile = { info: { name: string }; csv: CsvTable };

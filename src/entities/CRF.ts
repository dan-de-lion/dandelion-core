import _ from 'lodash';
import type { Resource } from 'i18next';
import { CrfData } from './CRFEntities';
import type { AccessLevelsTree } from '@/interfaces/AccessLevelsTree';

export declare type Version = `${number}.${number}.${number}`;

export class CRFVersionValidity {
  constructor(readonly majorVersion: number, readonly lastAvailable: Version, readonly startDate: Date) {}
}

export class CRF {
  constructor(
    readonly name: string,
    readonly version: Version,
    readonly xml = '',
    readonly js = '',
    readonly css = '',
    readonly translations: Resource = {},
    readonly accessLevels: AccessLevelsTree = {},
    readonly crfData: CrfData = { allValues: [], allDataClasses: [], allVariables: [] }
  ) {}
}

export function needToBeReevaluated(a: Version, b?: Version): boolean {
  if (!b) {
    return true;
  }
  // get the first two number of the version a and b
  // for example a = 1.0.0 and b = 2.3.4, becomes 1.0 and 2.3
  const firstTwoNumbersOfA = a.split('.').slice(0, 2).join('.');
  const firstTwoNumbersOfB = b.split('.').slice(0, 2).join('.');

  return firstTwoNumbersOfA !== firstTwoNumbersOfB;
}

export class SynchListElement {
  constructor(readonly caseID: string, readonly lastUpdate: Date) {}
}

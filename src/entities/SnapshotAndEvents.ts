import type { CachedCaseListElement } from '@/services/CaseListService';
import { SourceEventForList } from '@/source-events/list/SourceEventForList';

export class CaseListSnapshot {
  constructor(
    readonly cachedCaseList: Array<CachedCaseListElement>,
    public date: Date,
    readonly numberOfEvents: number
  ) {}
}

export class SnapshotAndEvents {
  constructor(readonly events: Array<SourceEventForList>, readonly snapshot: CaseListSnapshot) {}
}

import { computed, ComputedRef } from 'vue';
import { ReadonlyCaseListElement } from '../CaseListElement';
import { CaseListRowStyleInterface } from '@/interfaces/CaseListRowStyleInterface';

export default class FreezeStyle implements CaseListRowStyleInterface {
  constructor(protected rowClass: string) {}

  applyStyle(caseListElement: ReadonlyCaseListElement): { [x: string]: ComputedRef<boolean> } {
    const isFrozen = computed(() => {
      for (const value of caseListElement.caseMetaData.frozenAccessLevels.values()) {
        if (value === true) {
          return true;
        }
      }
      return false;
    });
    return { [this.rowClass]: isFrozen };
  }
}

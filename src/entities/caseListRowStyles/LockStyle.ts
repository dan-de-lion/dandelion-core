import { computed, ComputedRef } from 'vue';
import { ReadonlyCaseListElement } from '../CaseListElement';
import { CaseListRowStyleInterface } from '@/interfaces/CaseListRowStyleInterface';

export default class LockStyle implements CaseListRowStyleInterface {
  constructor(protected rowClass: string) {}

  applyStyle(caseListElement: ReadonlyCaseListElement): { [x: string]: ComputedRef<boolean> } {
    const isLocked = computed(() => Boolean(caseListElement.caseMetaData.isLocked));
    return { [this.rowClass]: isLocked };
  }
}

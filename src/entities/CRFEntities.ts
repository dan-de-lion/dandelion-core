export type CRFDataClassInterface = {
  dataClassName: string;
  dataClassList: Array<CRFDataClassChildrenVariableInterface>;
};

export type CRFDataClassChildrenVariableInterface = {
  fullName: string;
  label: string;
  type: string;
  dataClass: string;
  valuesSet?: string;
  itemsDataClass?: string;
};

export type CRFValueInterface = {
  name: string;
  label: string;
  numericalValue: number;
};

export type CRFValuesSetInterface = {
  valuesSetName: string;
  valuesSetList: Array<CRFValueInterface>;
};

export type CRFVariableInterface = {
  fullName: string;
  type: string;
  dataClass?: string;
  valuesSet?: string;
  itemsDataClass?: string;
};

export type CrfData = {
  allValues: CRFValuesSetInterface[];
  allVariables: CRFVariableInterface[];
  allDataClasses: CRFDataClassInterface[];
};

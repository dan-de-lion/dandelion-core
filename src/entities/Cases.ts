import type { CaseMetaData, SaveMetaData } from './CaseMetaData';
import { PartialStoredCase } from './PartialStoredCase';

export class Case {
  constructor(readonly caseID: string, readonly data: Record<string, any>, readonly caseMetaData: CaseMetaData) {}
}

export class CaseData {
  constructor(readonly partialStoredCases: PartialStoredCase[], readonly caseMetaData: CaseMetaData) {}
}

export class CaseForUpdate {
  constructor(readonly caseID: string, readonly data: Record<string, any>, readonly saveMetaData: SaveMetaData) {}
}

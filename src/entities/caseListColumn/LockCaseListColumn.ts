import { getContainer } from '@/ContainersConfig';
import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';
import type { CaseListColumn } from '@/interfaces/CaseListColumn';
import type TranslationService from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

export class LockCaseListColumn implements CaseListColumn {
  protected ts: TranslationService;
  constructor(protected heading: string) {
    this.ts = getContainer(Contexts.DEFAULT).get(NamedBindings.TranslationService);
  }

  protected getLock(caseListElement: ReadonlyCaseListElement, _parentIndexes: Array<number>) {
    const isLocked = caseListElement.caseMetaData.isLocked;
    const lockExpires = new Date(caseListElement.caseMetaData.lockExpires!);
    const tooltipContent = isLocked
      ? this.ts.t('caseLockedMsg', {
          user: caseListElement.caseMetaData.lockUser!,
          lockExpires: lockExpires.toISOString(),
        })
      : `${this.ts.t('Case Unlocked')}`;
    return { isLocked, tooltipContent };
  }

  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string {
    const lockInfo = this.getLock(caseListElement, []);
    return `
    <div class="row-icon-wrapper case-${lockInfo.isLocked ? 'freezed' : 'unfreezed'}-icon-wrapper">
        <div class="hoverable-row-icon">
            <ion-icon name="${lockInfo.isLocked ? 'lock-closed' : 'lock-open-outline'}"></ion-icon>
            <span class="tooltip">${lockInfo.tooltipContent}</span>
        </div>
    </div>`;
  }

  getHeading(): string {
    return this.heading;
  }
}

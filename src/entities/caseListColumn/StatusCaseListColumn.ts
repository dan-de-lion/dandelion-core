import type { CaseListColumn } from '@/interfaces/CaseListColumn';
import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';

export class StatusCaseListColumn implements CaseListColumn {
  constructor(protected heading: string, protected crfName: string, protected accessLevel: string) {}

  getContent(caseListElement: ReadonlyCaseListElement): string {
    if (
      !caseListElement.caseMetaData.status.has(this.accessLevel) ||
      !caseListElement.caseMetaData.status.get(this.accessLevel)?.has(this.crfName)
    ) {
      return '';
    }
    return caseListElement.caseMetaData.status.get(this.accessLevel)?.get(this.crfName)?.toString?.() ?? '';
  }

  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string {
    const statusValue = this.getContent(caseListElement);
    return `<span class='row-value status-value ${this.crfName}-status-value status-${statusValue}-value'>${statusValue}</span>`;
  }

  getHeading(): string {
    return this.heading;
  }
}

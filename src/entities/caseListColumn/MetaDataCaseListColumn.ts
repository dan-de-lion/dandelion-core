import type { CaseListColumn } from '@/interfaces/CaseListColumn';
import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';
import Helpers from '@/utils/Helpers';

export class MetaDataCaseListColumn implements CaseListColumn {
  constructor(protected heading: string, protected meta: string, protected labelMap?: Map<string, string>) {}

  getContent(caseListElement: ReadonlyCaseListElement): string {
    let caseListElementSelectedMeta: any;

    if (this.meta in caseListElement.caseMetaData) {
      caseListElementSelectedMeta = Helpers.getObjByPath(this.meta, caseListElement.caseMetaData);

      if (this.labelMap && this.labelMap.has(caseListElementSelectedMeta)) {
        return this.labelMap.get(caseListElementSelectedMeta.toString())!;
      }

      if (Object.prototype.toString.call(caseListElementSelectedMeta) === '[object Date]') {
        return (caseListElementSelectedMeta as Date).toLocaleString('en-GB', {
          year: 'numeric',
          month: '2-digit',
          day: '2-digit',
          hour: '2-digit',
          minute: '2-digit',
          hour12: false, // Use 24-hour format
        });
      }
      if (Object.prototype.toString.call(caseListElementSelectedMeta) === '[object Map]') {
        let result = '';
        for (const [key, value] of caseListElementSelectedMeta.entries()) {
          result += `${key}: ${value}, `;
        }
        return result.slice(0, -2);
      }
      return caseListElementSelectedMeta;
    }
    return '';
  }

  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string {
    return `<span class='row-value meta-data-value ${this.heading
      .toLocaleLowerCase()
      .replace(/\s+/g, '-')}-meta-data-value'>${this.getContent(caseListElement)}</span>`;
  }

  getHeading(): string {
    return this.heading;
  }
}

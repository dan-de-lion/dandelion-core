import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';
import type { CaseListColumn } from '@/interfaces/CaseListColumn';

export class CrfVersionMetaDataCaseListColumn implements CaseListColumn {
  constructor(protected heading: string, protected crfName: string) {}

  protected getContent(caseListElement: ReadonlyCaseListElement) {
    const crfVersion = caseListElement.caseMetaData.crfVersions.get(this.crfName);
    if (crfVersion !== undefined) {
      return crfVersion;
    }
    return '';
  }

  getContentAsHTML(caseListElement: ReadonlyCaseListElement) {
    return `<span class='row-value crf-version-value ${this.crfName.toLocaleLowerCase()}-crf-version-value'>${this.getContent(
      caseListElement
    )}</span>`;
  }

  getHeading(): string {
    return this.heading;
  }
}

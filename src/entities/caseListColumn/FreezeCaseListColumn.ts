import { getContainer } from '@/ContainersConfig';
import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';
import type { CaseListColumn } from '@/interfaces/CaseListColumn';
import type TranslationService from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

export class FreezeCaseListColumn implements CaseListColumn {
  protected ts: TranslationService;
  constructor(protected heading: string) {
    this.ts = getContainer(Contexts.DEFAULT).get(NamedBindings.TranslationService);
  }

  protected getFreeze(caseListElement: ReadonlyCaseListElement) {
    let isFrozen = false;
    const tooltipLines: string[] = [];

    caseListElement.caseMetaData.frozenAccessLevels.forEach((freezeValue, accessLevel) => {
      const freezeStatus = freezeValue ? 'Freezed' : 'Unfreezed';
      tooltipLines.push(`${this.ts.t(`${freezeStatus} for`)} accessLevel ${accessLevel}`);
      if (freezeValue) {
        isFrozen = freezeValue;
        return;
      }
    });
    const tooltipContent = tooltipLines.join('<br>');
    return { isFrozen, tooltipContent };
  }

  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string {
    const freezeInfo = this.getFreeze(caseListElement);
    return `
    <div class="row-icon-wrapper case-${freezeInfo.isFrozen ? 'freezed' : 'unfreezed'}-icon-wrapper">
      <div class="hoverable-row-icon">
          <ion-icon name="${freezeInfo.isFrozen ? 'snow-outline' : 'close-outline'}"></ion-icon>
          <span class="tooltip">${freezeInfo.tooltipContent}</span>
      </div>
    </div>`;
  }

  getHeading(): string {
    return this.heading;
  }
}

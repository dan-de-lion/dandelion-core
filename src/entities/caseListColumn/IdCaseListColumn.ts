import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';
import type { CaseListColumn } from '@/interfaces/CaseListColumn';

export class IdCaseListColumn implements CaseListColumn {
  constructor(protected heading: string, protected lengthOfDisplayedId: number = 6) {}

  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string {
    const string = caseListElement.caseMetaData.caseID.substring(0, this.lengthOfDisplayedId);
    return `<span class='row-value case-id-value case-id-${string}-value'>${string}</span>`;
  }

  getHeading(): string {
    return this.heading;
  }
}

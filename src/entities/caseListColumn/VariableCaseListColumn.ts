import type { CaseListColumn } from '@/interfaces/CaseListColumn';
import type { ReadonlyCaseListElement } from '@/entities/CaseListElement';

export class VariableCaseListColumn implements CaseListColumn {
  constructor(protected heading: string, protected variableName: string) {}

  getContentAsHTML(caseListElement: ReadonlyCaseListElement): string {
    let content = caseListElement.cache.get(this.variableName);
    if (content === null || content === undefined || (typeof content === 'number' && Number.isNaN(content))) {
      content = '';
    }
    if (Object.prototype.toString.call(content) === '[object Date]') {
      content = (content as Date).toLocaleString('en-GB', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        hour12: false, // Use 24-hour format
      });
    }
    return `<span class='row-value variable-value ${this.heading
      .toLocaleLowerCase()
      .replace(/\s+/g, '-')}-variable-value'>${content.getLabel?.() ?? content.toString()}</span>`;
  }

  getVariableName(): string {
    return this.variableName;
  }

  getHeading(useLabel = true): string {
    return useLabel ? this.heading : this.getVariableName();
  }
}

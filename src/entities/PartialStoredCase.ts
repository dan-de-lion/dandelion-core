export class PartialStoredCase {
  constructor(readonly data: string, readonly accessLevel: string) {}
}

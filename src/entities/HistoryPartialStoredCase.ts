import { CaseMetaData } from './CaseMetaData';
import type { PartialStoredCase } from './PartialStoredCase';

export class HistoryPartialStoredCase {
  constructor(
    readonly creationDate: string,
    readonly case_: PartialStoredCase[],
    readonly caseMetaData_: CaseMetaData
  ) {}
}

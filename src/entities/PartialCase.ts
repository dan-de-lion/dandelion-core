import type { CaseMetaData } from '@/entities/CaseMetaData';

export class PartialCase {
  constructor(
    readonly data: Record<string, any>,
    readonly caseID: string,
    readonly caseMetaData: CaseMetaData,
    readonly accessLevel: string
  ) {}
}

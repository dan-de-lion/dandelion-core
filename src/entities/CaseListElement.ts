import type { ComputedRef, DeepReadonly, UnwrapNestedRefs } from 'vue';
import type { CaseMetaData } from '@/entities/CaseMetaData';

export class CaseListElement {
  constructor(
    readonly caseMetaData: ComputedRef<CaseMetaData>,
    readonly cache: ComputedRef<Map<string, any>>,
    readonly lastUpdate: Date
  ) {}
}
export type ReadonlyCaseList = DeepReadonly<UnwrapNestedRefs<Array<CaseListElement>>>;
export type ReadonlyCaseListElement = DeepReadonly<UnwrapNestedRefs<CaseListElement>>;

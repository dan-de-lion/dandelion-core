export type CrfWithValidity = {
  crfName: string;
  crfValidityArray: Array<CrfStartAndEndDates>;
  isAlwaysActive?: boolean;
};

export type CrfStartAndEndDates = {
  startDate: Date;
  endDate?: Date;
};

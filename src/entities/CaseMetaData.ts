import type { Version } from './CRF';
import { CrfStatus } from '@/services/CaseStatusService';

export type CaseInfo = {
  readonly caseID: string;
  readonly creationDate: Date;
  readonly caseType: string;
  readonly centreCode: string;
};

export type SaveMetaData = {
  readonly status: Map<string, CrfStatus>; // CrfName -> status
  readonly crfVersions: Map<string, Version>; // CrfName -> version
  readonly lastUpdate: Date;
};

export type StateMetaData = {
  readonly frozenAccessLevels: Map<string, boolean>; // AccessLevel -> isFrozen
  readonly lockUser?: string;
  readonly lockExpires?: Date;
  readonly isLocked?: boolean;
};

export type CaseMetaData = CaseInfo & SaveMetaData & StateMetaData;

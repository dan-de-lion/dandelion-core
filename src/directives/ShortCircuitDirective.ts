import _ from 'lodash';
import { type DirectiveBinding, type VNode, type ObjectDirective, watch } from 'vue';
import { ComputedValueUpdated } from '@/internal-events/ComputedValueUpdated';

export default class ShortCircuitDirective implements ObjectDirective {
  public mounted(el: HTMLElement, binding: DirectiveBinding, _vNode: VNode) {
    if (binding.value === undefined || binding.value === null) {
      return;
    }
    ShortCircuitDirective.propagateNewValue(binding.value, el);
  }

  public updated(el: HTMLElement, binding: DirectiveBinding, _vNode: VNode) {
    // Checks if the new value is deeply equal to the previous one
    if (binding.value !== null && typeof binding.value === 'object' && _.isEqual(binding.value, binding.oldValue)) {
      return;
    }

    if (binding.value === binding.oldValue) {
      return;
    }

    if (binding.value === undefined) {
      return;
    }
    ShortCircuitDirective.propagateNewValue(binding.value, el);
  }

  public unmounted(el: HTMLElement, _binding: DirectiveBinding, _vNode: VNode) {
    // Unwatch deep watcher for variables that needed it
    //@ts-expect-error TODO: find a place where to save unwatchers
    el.unwatchShortCircuit?.();
  }

  protected static propagateNewValue(value: any, el: HTMLElement) {
    // If value is a promise we will update model only when promise is fullfilled
    if (value instanceof Promise) {
      value.then((v) => el?.dispatchEvent(new ComputedValueUpdated(v)));
      return;
    }
    // If array (multiplechoices and sets) or object (groups and dataclass instances)
    // the value is not deep watched, so Vue does not call "update" hook
    // then we should take care of deep watching
    if (Array.isArray(value) || value?.constructor === Object) {
      //@ts-expect-error TODO: find a place where to save unwatchers
      el.unwatchShortCircuit?.();
      //@ts-expect-error TODO: find a place where to save unwatchers
      el.unwatchShortCircuit = watch(value, (newValue) => el?.dispatchEvent(new ComputedValueUpdated(newValue)), {
        deep: true,
      });
    }
    // Except for Promises, we immediately update the model
    el?.dispatchEvent(new ComputedValueUpdated(value));
  }
}

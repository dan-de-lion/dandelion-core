import { toNumber } from 'lodash';
import CustomModelDirective, { updateValue, type DandelionDirectiveBinding } from './CustomModelDirective';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import type { ComputedValueUpdated } from '@/internal-events/ComputedValueUpdated';
import { InternalEvents } from '@/types/InternalEvents';
import Helpers from '@/utils/Helpers';

export default class CustomModelDirectiveForCheckboxes extends CustomModelDirective {
  public static updateView(el: HTMLElement, valueOfModel: any) {
    for (let i = 0; i < el.children.length; i++) {
      const firstChild = el.children.item(i)?.firstChild;
      // If it hasn't got any child node, like a span, continue
      if (firstChild === null) {
        continue;
      }
      if (valueOfModel === null || valueOfModel === undefined) {
        (firstChild as HTMLInputElement).checked = false;
      } else if (valueOfModel instanceof Array) {
        const { value } = el.children.item(i)?.firstChild as HTMLInputElement;
        (firstChild as HTMLInputElement).checked = valueOfModel.some((v: ValuesSetValue) => v.toString() === value);
      } else {
        (firstChild as HTMLInputElement).checked = false;
      }
    }
  }

  public beforeMount(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );

    CustomModelDirective.myBeforeMount(el, path, instance, requiredForStatus);
    CustomModelDirective.setupWatcher(
      el,
      path,
      instance,
      CustomModelDirectiveForCheckboxes.updateView,
      CustomModelDirective.updateStatusService,
      requiredForStatus
    );
  }

  public created(el: HTMLElement, binding: DandelionDirectiveBinding) {
    CustomModelDirectiveForCheckboxes.addListenerOnUpdateCheckboxes(el, binding);
    CustomModelDirectiveForCheckboxes.addListenerOnInputCheckboxes(el, binding);
  }

  public static addListenerOnInputCheckboxes(el: HTMLElement, binding: DandelionDirectiveBinding) {
    el.childNodes.forEach((cn) => {
      const type = cn.nodeName;
      // Attach the input and update listeners only on LI nodes
      if (type !== 'LI') {
        return;
      }
      //@ts-expect-error
      cn.onInputHandler = onInputCheckboxes.bind(null, binding, el);
      //@ts-expect-error
      cn.addEventListener('input', cn.onInputHandler);
    });
  }
  public static addListenerOnUpdateCheckboxes(el: HTMLElement, binding: DandelionDirectiveBinding) {
    //@ts-ignore
    el.onComputedValueUpdatedHandler = onComputedValueUpdatedCheckboxes.bind(null, binding, el);
    //@ts-ignore
    el.addEventListener(InternalEvents.ComputedValueUpdated, el.onComputedValueUpdatedHandler);
  }

  public unmounted(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    el.removeEventListener(
      InternalEvents.ComputedValueUpdated,
      //@ts-expect-error
      el.onComputedValueUpdatedHandler
    );
    el.childNodes.forEach((cn) => {
      const type = cn.nodeName;
      // Attach the input and update listeners only on LI nodes
      if (type !== 'LI') {
        return;
      }
      //@ts-expect-error
      cn.removeEventListener('input', cn.onInputHandler);
    });
    CustomModelDirective.myUnmounted(el, instance, path);
  }
}

function onInputCheckboxes({ value: path, instance }: DandelionDirectiveBinding, el: HTMLElement, e: Event) {
  // if the variable is null or undefined, set it to an empty array
  let variable: any = Helpers.getObjByPath(path, instance.model);
  // if the variable is not an array, set it to an empty array -> this should never happen, but if we have a singlechoice that becomes a multiplechoice, this could happen
  if (variable === null || variable === undefined || !(variable instanceof Array)) {
    variable = [];
  }
  const { value } = e.target as HTMLInputElement;
  const index = variable.findIndex((v: ValuesSetValue) => v.toString() === value);
  if (index < 0) {
    const fs = e.target as HTMLInputElement;
    const excludes = fs.getAttribute('excludes');
    const arrayOfExcludes: Array<string> = excludes!.split(',') ?? [];
    arrayOfExcludes.forEach((x) => {
      variable.forEach((valuesSetElement: ValuesSetValue, index: number) => {
        const firstDotIndex = valuesSetElement.indexOf('.');
        const e = new ValuesSetValue(valuesSetElement.substring(firstDotIndex + 1));
        if (e.equals(x)) {
          variable.splice(index, 1);
        }
      });
    });
    const myLabel = (e.target as HTMLInputElement).getAttribute('my-label');
    const myNumericalValue = Number((e.target as HTMLInputElement).getAttribute('my-numerical-value'));
    variable.push(new ValuesSetValue(value, myLabel ?? value, myNumericalValue));
  } else {
    variable.splice(index, 1);
  }
  updateValue(path, instance, variable, el);
  CustomModelDirectiveForCheckboxes.updateView(el, Helpers.getObjByPath(path, instance.model));
  e.stopPropagation();
}

function onComputedValueUpdatedCheckboxes(
  { value: path, instance }: DandelionDirectiveBinding,
  el: HTMLElement,
  e: Event
) {
  e.stopPropagation();
  const updateEvent = e as ComputedValueUpdated;
  const changedValue: Array<ValuesSetValue | string> | any = updateEvent.changedValue;

  if (changedValue === null) {
    return;
  }

  if (!(changedValue instanceof Array)) {
    throw new CoreError(
      'The return value of the computedFormula is not an Array! check the computedFormula',
      CoreErrorType.DIRECTIVE
    );
  }
  let arrayValues: Array<ValuesSetValue> | any = Helpers.getObjByPath(path, instance.model);
  // if the variable is not an array, set it to an empty array -> this should never happen, but if we have a singlechoice that becomes a multiplechoice, this could happen
  if (arrayValues === null || arrayValues === undefined || !(arrayValues instanceof Array)) {
    arrayValues = [];
  }
  arrayValues.splice(0);
  el.childNodes.forEach((child) => {
    if (!(child.firstChild instanceof HTMLInputElement)) {
      throw new CoreError(
        'Found a children of multiplechoiceVariable that is not an html input element!',
        CoreErrorType.DIRECTIVE
      );
    }
    const valueAttribute = child.firstChild.getAttribute('value');

    if (!valueAttribute) {
      throw new CoreError('Found a checkbox without a value!', CoreErrorType.DIRECTIVE);
    }

    const found = changedValue.find((v: ValuesSetValue | string) => v.toString() === valueAttribute);

    if (!found) {
      return;
    }

    if (found instanceof ValuesSetValue) {
      arrayValues.push(found);
    } else {
      const myLabel = child.firstChild.getAttribute('my-label') ?? valueAttribute;
      const myNumericalValue =
        child.firstChild.getAttribute('my-numerical-value') === null
          ? NaN
          : Number(child.firstChild.getAttribute('my-numerical-value'));

      arrayValues.push(new ValuesSetValue(valueAttribute, myLabel ?? valueAttribute, myNumericalValue));
    }
  });
  updateValue(path, instance, arrayValues, el);
  CustomModelDirectiveForCheckboxes.updateView(el, arrayValues);
}

import { toNumber } from 'lodash';
import CustomModelDirective, { updateValue, type DandelionDirectiveBinding } from './CustomModelDirective';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import type { ComputedValueUpdated } from '@/internal-events/ComputedValueUpdated';
import { InternalEvents } from '@/types/InternalEvents';
import Helpers from '@/utils/Helpers';

function onInputSinglechoice({ instance, value: path }: DandelionDirectiveBinding, el: HTMLElement, e: Event) {
  const valueOfInput = (e.target as HTMLInputElement).getAttribute('value');
  const myLabel = (e.target as HTMLInputElement).getAttribute('my-label');
  const myNumericalValue = Number((e.target as HTMLInputElement).getAttribute('my-numerical-value'));
  const newValue = new ValuesSetValue(valueOfInput!, myLabel ?? valueOfInput!, myNumericalValue ?? NaN);
  updateValue(path, instance, newValue, el);
  CustomModelDirectiveForSinglechoice.updateView(el, Helpers.getObjByPath(path, instance.model));
  e.stopPropagation();
}

function onComputedValueUpdatedSinglechoice(
  { instance, value: path }: DandelionDirectiveBinding,
  el: HTMLElement,
  e: Event
) {
  const updateEvent = e as ComputedValueUpdated;
  const { changedValue } = updateEvent;

  el.childNodes.forEach((child) => {
    if (
      child.firstChild instanceof HTMLInputElement &&
      changedValue !== null &&
      child.firstChild.getAttribute('value') === changedValue.toString()
    ) {
      const myLabel = child.firstChild.getAttribute('my-label');
      const myNumericalValue = Number(child.firstChild.getAttribute('my-numerical-value'));
      // Update the value based on shorted-circuit
      const newValue = new ValuesSetValue(changedValue, myLabel ?? changedValue, myNumericalValue);
      updateValue(path, instance, newValue, el);
      // Set the IL Input item checked
      CustomModelDirectiveForSinglechoice.updateView(el, changedValue);
      e.stopPropagation();
    }
  });
}

export default class CustomModelDirectiveForSinglechoice extends CustomModelDirective {
  public beforeMount(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );
    CustomModelDirective.myBeforeMount(el, path, instance, requiredForStatus);
    CustomModelDirective.setupWatcher(
      el,
      path,
      instance,
      CustomModelDirectiveForSinglechoice.updateView,
      CustomModelDirective.updateStatusService,
      requiredForStatus
    );
  }

  public static updateView(el: HTMLElement, value: ValuesSetValue) {
    if (value === undefined) {
      return;
    }
    for (let i = 0; i < el.children.length; i++) {
      const firstChild = el.children.item(i)?.firstChild;
      // If it hasn't got any child node, like a span, continue
      if (firstChild === null) {
        continue;
      }
      (firstChild as HTMLInputElement).checked = (firstChild as HTMLInputElement).value === value.toString();
    }
  }

  public created(el: HTMLElement, binding: DandelionDirectiveBinding) {
    CustomModelDirectiveForSinglechoice.addListenerOnInputSinglechoice(el, binding);
    CustomModelDirectiveForSinglechoice.addListenerOnUpdateSinglechoice(el, binding);
  }

  public static addListenerOnInputSinglechoice(el: HTMLElement, binding: DandelionDirectiveBinding) {
    el.childNodes.forEach((cn) => {
      const type = cn.nodeName;
      // Attach the input and update listeners only on LI nodes
      if (type !== 'LI') {
        return;
      }
      //@ts-expect-error
      cn.onInputHandler = onInputSinglechoice.bind(null, binding, el);
      //@ts-expect-error
      cn.addEventListener('input', cn.onInputHandler);
    });
  }

  public static addListenerOnUpdateSinglechoice(el: HTMLElement, binding: DandelionDirectiveBinding) {
    //@ts-expect-error
    el.onComputedValueUpdatedHandler = onComputedValueUpdatedSinglechoice.bind(null, binding, el);
    //@ts-expect-error
    el.addEventListener(InternalEvents.ComputedValueUpdated, el.onComputedValueUpdatedHandler);
  }

  public unmounted(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    el.removeEventListener(
      InternalEvents.ComputedValueUpdated,
      //@ts-expect-error
      el.onComputedValueUpdatedHandler
    );
    el.childNodes.forEach((cn) => {
      const type = cn.nodeName;
      // Attach the input and update listeners only on LI nodes
      if (type !== 'LI') {
        return;
      }
      //@ts-expect-error
      cn.removeEventListener('input', cn.onInputHandler);
    });
    CustomModelDirective.myUnmounted(el, instance, path);
  }
}

import type { ObjectDirective } from 'vue';
import Helpers from '../utils/Helpers';
import type { DandelionDirectiveBinding } from '@/directives/CustomModelDirective';

export default class ResetWhenHidden implements ObjectDirective {
  public unmounted(_: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    Helpers.unsetObjByPath(path, instance.model);
  }
}

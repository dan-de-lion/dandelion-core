import { toNumber } from 'lodash';
import type { DirectiveBinding } from 'vue';
import CustomModelDirective, { type DandelionDirectiveBinding, updateValue } from './CustomModelDirective';
import { ValuesSetValue } from '@/entities/ValuesSetValue';

function onInputSelect({ instance, value: path }: DandelionDirectiveBinding, el: HTMLElement, e: Event) {
  const valueOfInput = (el as HTMLSelectElement).value;
  const selectedValue = (e.target as HTMLSelectElement).value;
  const childOptionElements = (e.target as HTMLSelectElement).querySelectorAll('option');

  const selectedChildOptionElement = Array.from(childOptionElements).find((option) => option.value === selectedValue);

  const myLabel = (selectedChildOptionElement as HTMLOptionElement).getAttribute('my-label');
  const myNumericalValue = Number((selectedChildOptionElement as HTMLOptionElement).getAttribute('my-numerical-value'));

  const newValue = new ValuesSetValue(valueOfInput!, myLabel ?? valueOfInput!, myNumericalValue ?? NaN);
  updateValue(path, instance, newValue, el);
  e.stopPropagation();
}

export default class CustomModelDirectiveForSelect extends CustomModelDirective {
  public created(el: HTMLElement, binding: DirectiveBinding) {
    CustomModelDirectiveForSelect.addListenerOnInputSelect(el, binding);
    CustomModelDirective.addListenerOnUpdate(binding, el);
  }
  public static addListenerOnInputSelect(el: HTMLElement, binding: DirectiveBinding) {
    //@ts-expect-error
    el.onInputHandler = onInputSelect.bind(null, binding, el);
    //@ts-expect-error
    el.addEventListener('input', el.onInputHandler);
  }

  public beforeMount(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );
    CustomModelDirective.myBeforeMount(el, path, instance, requiredForStatus);
    CustomModelDirective.setupWatcher(
      el,
      path,
      instance,
      CustomModelDirectiveForSelect.updateView,
      CustomModelDirective.updateStatusService,
      requiredForStatus
    );
  }

  protected static updateView(el: HTMLElement, value: any) {
    (el as HTMLInputElement).value = value;
  }
}

import _ from 'lodash';
import { type DirectiveBinding, type VNode, type ObjectDirective, watch } from 'vue';
import type { DandelionDirectiveBinding } from '@/directives/CustomModelDirective';
import { ComputedItemsUpdated } from '@/internal-events/ComputedItemsUpdated';

export default class ComputedItemsDirective implements ObjectDirective {
  public mounted(el: HTMLElement, { value }: DandelionDirectiveBinding, vNode: VNode) {
    if (value === undefined || value === null) {
      return;
    }
    const itemKeyValue = el.attributes.getNamedItem('item-key')?.value;
    if (!itemKeyValue) {
      return;
    }
    vNode?.el?.dispatchEvent(new ComputedItemsUpdated(value, itemKeyValue));

    //@ts-expect-error
    el.unwatchComputedItems = watch(
      value,
      (newValue: string[]) => el?.dispatchEvent(new ComputedItemsUpdated(newValue, itemKeyValue)),
      { deep: true }
    );
  }

  public updated(el: HTMLElement, binding: DirectiveBinding, vNode: VNode) {
    // This checks if the new value is an Object or an Array and performs deep comparison
    if (binding.value !== null && typeof binding.value === 'object' && _.isEqual(binding.value, binding.oldValue)) {
      return;
    }

    if (binding.value === binding.oldValue) {
      return;
    }

    if (binding.value === undefined || !vNode || !vNode.el) {
      return;
    }
    const itemKeyValue = el.attributes.getNamedItem('item-key')?.value;
    if (!itemKeyValue) {
      return;
    }
    const { value } = binding;
    vNode?.el?.dispatchEvent(new ComputedItemsUpdated(value, itemKeyValue));

    //@ts-expect-error
    el?.unwatchComputedItems?.();
    //@ts-expect-error
    el.unwatchComputedItems = watch(
      value,
      (newValue: string[]) => el?.dispatchEvent(new ComputedItemsUpdated(newValue, itemKeyValue)),
      { deep: true }
    );
  }

  public unmounted(el: HTMLElement) {
    //@ts-expect-error
    el?.unwatchComputedItems?.();
  }
}

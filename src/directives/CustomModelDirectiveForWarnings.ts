import { toRef, watch } from 'vue';
import CustomModelDirective, { type DandelionDirectiveBinding } from '@/directives/CustomModelDirective';
import CustomModelDirectiveForBoolean from '@/directives/CustomModelDirectiveForBoolean';
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import type { DandelionComponentInstance } from '@/types/DandelionComponentInstance';
import { InternalEvents } from '@/types/InternalEvents';
import Helpers from '@/utils/Helpers';

export class CustomModelDirectiveForWarnings extends CustomModelDirectiveForBoolean {
  created(el: HTMLElement, binding: DandelionDirectiveBinding) {
    super.created(el, binding);
    const { instance, value: variablePath } = binding;
    instance.caseStatusService.addWarning(variablePath);
  }

  public beforeMount(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    if (Helpers.getObjByPath(path, instance.model) === undefined) {
      CustomModelDirective.initVModel(el as HTMLInputElement, path, instance);
    }
    CustomModelDirective.setupStatusService(
      path,
      instance,
      instance.coreConfigurationService.defaultRequiredForStatus!.warnings
    );

    CustomModelDirective.setupWatcher(
      el,
      path,
      instance,
      CustomModelDirectiveForBoolean.updateView,
      CustomModelDirective.updateStatusService,
      instance.coreConfigurationService.defaultRequiredForStatus!.warnings
    );
    CustomModelDirectiveForWarnings.watchConfirmation(el, path, instance);
  }

  public mounted(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    const { caseStatusService } = instance;
    const value = Helpers.getObjByPath(path, instance.model);
    CustomModelDirectiveForWarnings.changeConfirmation(value, el, caseStatusService, path);
  }

  public unmounted(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    const { caseStatusService, currentCaseService, model } = instance;

    //@ts-expect-error
    el.removeEventListener('input', el.onInputHandler);
    el.removeEventListener(
      InternalEvents.ComputedValueUpdated,
      //@ts-expect-error
      el.onComputedValueUpdatedHandler
    );

    //@ts-expect-error
    el.unwatch!();
    //@ts-expect-error
    el.unwatchForClass?.();

    if (currentCaseService.isCaseOpened.value) {
      const accessLevel = currentCaseService.getAccessLevelForVariable(path);
      Helpers.unsetObjByPath(path, model); //Necessary for warnings we don't use ResetWhenHidden
      caseStatusService.setHiddenVisibility(path, accessLevel);
      caseStatusService.removeWarning(path);
    }
  }

  //TODO-When CSS :has is supported from all browsers remove that (at least 2025)

  protected static watchConfirmation(el: HTMLElement, path: string, instance: DandelionComponentInstance) {
    const { caseStatusService } = instance;
    const variableParent = path.substring(0, path.lastIndexOf('.'));
    const variableSpecificName = path.substring(path.lastIndexOf('.') + 1);

    const variableRef = toRef(
      Helpers.getObjByPath(variableParent ? `model.${variableParent}` : 'model', instance),
      variableSpecificName
    );

    //@ts-expect-error: we need to put the unwatch function somewhere that persists through directive calls
    el.unwatchForClass = watch(variableRef, (value: boolean) => {
      CustomModelDirectiveForWarnings.changeConfirmation(value, el, caseStatusService, path);
    });
  }

  protected static changeConfirmation(
    value: boolean,
    el: HTMLElement,
    caseStatusService: CaseStatusServiceInterface,
    path: string
  ) {
    if (value) {
      el.parentElement!.classList.add('solvedWarning');
      el.parentElement!.classList.remove('warning');
      caseStatusService.removeWarning(path);
      caseStatusService.addConfirmedWarning(path);
    } else {
      el.parentElement!.classList.add('warning');
      el.parentElement!.classList.remove('solvedWarning');
      caseStatusService.addWarning(path);
      caseStatusService.removeConfirmedWarning(path);
    }
  }
}

import CustomModelDirective, { updateValue, type DandelionDirectiveBinding } from './CustomModelDirective';
import { DataCollectionError } from '@/errors/DataCollectionError';
import Helpers from '@/utils/Helpers';

export default class CustomModelDirectiveForErrors extends CustomModelDirective {
  public created(_: HTMLElement, { instance, value: variablePath }: DandelionDirectiveBinding) {
    instance.caseStatusService.addError(variablePath);
  }

  public beforeMount(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    CustomModelDirective.setupStatusService(
      path,
      instance,
      instance.coreConfigurationService.defaultRequiredForStatus!.errors
    );
    //We put in model new error to make the variable missing and make status work correctly (see isVariableMissing of caseStatusService)
    updateValue(path, instance, new DataCollectionError(), el);
  }

  public unmounted(_: HTMLElement, { instance, value: variablePath }: DandelionDirectiveBinding) {
    const { caseStatusService, currentCaseService, model } = instance;

    if (currentCaseService.isCaseOpened.value) {
      Helpers.unsetObjByPath(variablePath, model); //Necessary for errors we don't use ResetWhenHidden
      const accessLevel = currentCaseService.getAccessLevelForVariable(variablePath);
      caseStatusService.setHiddenVisibility(variablePath, accessLevel);
      caseStatusService.removeError(variablePath);
    }
  }
}

import { toNumber } from 'lodash';
import { type DirectiveBinding, type ObjectDirective, toRef, type VNode, watch } from 'vue';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import { ComputedValueUpdated } from '@/internal-events/ComputedValueUpdated';
import type { DandelionComponentInstance } from '@/types/DandelionComponentInstance';
import { InternalEvents } from '@/types/InternalEvents';
import Helpers from '@/utils/Helpers';
import { getContainer } from '@/ContainersConfig';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import type TranslationService from '@/services/TranslationService';

export type DandelionDirectiveBinding = DirectiveBinding & {
  instance: DandelionComponentInstance;
};

function onInput({ value: path, instance }: DandelionDirectiveBinding, el: HTMLElement, e: Event) {
  // if the event is coming from a child element, ignore it
  if (e.target !== el) {
    e.stopPropagation();
    // the event is probably an event not caught from other input elements that are inside the current DOM element
    return;
  }

  if (e instanceof ComputedValueUpdated) {
    throw new CoreError('Input event called with a ComputedValueUpdated', CoreErrorType.DIRECTIVE);
  }
  const value =
    (e.target as HTMLInputElement)?.type === 'number'
      ? parseFloat((e.target as HTMLInputElement).value)
      : (e.target as HTMLInputElement).value;

  updateValue(path, instance, value, el);
  e.stopPropagation();
}

// TODO: maybe we should always pass here also for "manual" update of variables?
// BUT we don't have instance and context, so maybe it's better to make watch works for all cases?
export function updateValue(path: string, instance: any, value: any, el: HTMLElement) {
  Helpers.setObjByPath(path, instance.model, value);
  updateCaseStatus(instance, el, path, value);
}

export function updateCaseStatus(instance: any, _el: HTMLElement, path: any, newValue: any) {
  const { caseStatusService, currentCaseService } = instance;
  const accessLevel = currentCaseService.getAccessLevelForVariable(path);
  caseStatusService.updateValue(path, newValue, accessLevel);
}

function onComputedValueUpdated({ value: path, instance }: DandelionDirectiveBinding, el: HTMLElement, e: Event) {
  e.stopPropagation();
  const updateEvent = e as ComputedValueUpdated;
  const { changedValue } = updateEvent;

  if (changedValue === null) {
    return;
  }
  // Update the value based on shorted-circuit
  updateValue(path, instance, changedValue, el);
}

export default class CustomModelDirective implements ObjectDirective {
  protected static setupWatcher(
    el: HTMLElement,
    path: string,
    instance: DandelionComponentInstance,
    viewUpdaterFn: (el: HTMLElement, value: any) => void,
    statusUpdaterFn: (
      path: string,
      value: any,
      instance: DandelionComponentInstance,
      requiredForStatus: number
    ) => void,
    requiredForStatus: number
  ) {
    const variableParent = path.substring(0, path.lastIndexOf('.'));
    const variableSpecificName = path.substring(path.lastIndexOf('.') + 1);

    const variableRef = toRef(
      Helpers.getObjByPath(variableParent ? `model.${variableParent}` : 'model', instance),
      variableSpecificName
    );
    const { model } = instance;
    //@ts-expect-error: we need to put the unwatch function somewhere that persists through directive calls
    el.unwatch = watch(
      variableRef,
      () => {
        const value = Helpers.getObjByPath(path, model);
        viewUpdaterFn(el, value);
        statusUpdaterFn(path, value, instance, requiredForStatus);
      },
      {
        /* even if not necessary for some type of variables, we need this for all variables with results objects or arrays and on other variables,
          it doesn't create problems */
        deep: true,
      }
    );
    const value = Helpers.getObjByPath(path, model);
    viewUpdaterFn(el, value);
    statusUpdaterFn(path, value, instance, requiredForStatus);
  }

  protected static updateView(el: HTMLElement, value: any) {
    if (
      el instanceof HTMLInputElement &&
      (<HTMLInputElement>el).type === 'number' &&
      isNaN(value) &&
      /* Need for references(when we empty a input number also the reference must react). Warning: if you type '1.' the reference will empty */
      isNaN(parseFloat((<HTMLInputElement>el).value))
    ) {
      //this solves the emptying of a number variable when inserting a dot
      return;
    }
    const ts = getContainer(Contexts.SHARED)?.get(NamedBindings.TranslationService) as TranslationService;

    if (Array.isArray(value)) {
      (el as HTMLInputElement).value = value
        .map((v) => (v instanceof ValuesSetValue ? ts.t(v.getLabel()) : v))
        .join(', ');
    } else {
      (el as HTMLInputElement).value = value instanceof ValuesSetValue ? ts.t(value.getLabel()) : value;
    }
  }

  protected static updateStatusService(
    path: string,
    value: any,
    instance: DandelionComponentInstance,
    requiredForStatus: number
  ) {
    if (isNaN(requiredForStatus)) {
      //this is the case when requiredForStatus = false
      return;
    }
    updateValue(path, instance, value, instance.$el);
  }

  protected static initVModel(el: HTMLInputElement, path: string, instance: DandelionComponentInstance) {
    // Check if initial value is present
    const initValue: Attr | null = el.attributes.getNamedItem('init-value');
    // necessary for eval
    const { model } = instance;
    // If initial value is defined, set the value do the initial value
    if (initValue !== null) {
      try {
        updateValue(path, instance, eval(initValue.value), el);
      } catch (e: any) {
        throw new CoreError('Evaluation of the initial variable failed', CoreErrorType.DIRECTIVE);
      }
    } else {
      /* If initial value is NOT set, set the value as empty string */
      updateValue(path, instance, '', el);
    }
  }

  public beforeMount(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );

    CustomModelDirective.myBeforeMount(el, path, instance, requiredForStatus);

    CustomModelDirective.setupWatcher(
      el,
      path,
      instance,
      CustomModelDirective.updateView,
      CustomModelDirective.updateStatusService,
      requiredForStatus
    );
  }

  public static myBeforeMount(
    el: HTMLElement,
    path: string,
    instance: DandelionComponentInstance,
    requiredForStatus: number
  ) {
    if (Helpers.getObjByPath(path, instance.model) === undefined) {
      CustomModelDirective.initVModel(el as HTMLInputElement, path, instance);
    }
    CustomModelDirective.setupStatusService(path, instance, requiredForStatus);
  }

  public static setupStatusService(path: string, instance: DandelionComponentInstance, requiredForStatus: number) {
    if (isNaN(requiredForStatus)) {
      //this is the case when requiredForStatus = false
      return;
    }
    const { currentCaseService, caseStatusService } = instance;

    const accessLevel = currentCaseService.getAccessLevelForVariable(path);
    caseStatusService.registerVariable(path, accessLevel, requiredForStatus);
  }

  public unmounted(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    CustomModelDirective.myUnmounted(el, instance, path);
  }

  public static myUnmounted(el: HTMLElement, instance: DandelionComponentInstance, path: string) {
    /*
     * NOTE: This does not clear the variable, otherwise the sets would not work as expected
     * when trying to remove an item. The variable is cleared by the customIf directive.
     */
    const { caseStatusService, currentCaseService } = instance;

    //@ts-expect-error
    el.removeEventListener('input', el.onInputHandler);
    el.removeEventListener(
      InternalEvents.ComputedValueUpdated,
      //@ts-expect-error
      el.onComputedValueUpdatedHandler
    );
    //@ts-expect-error
    el.unwatch?.();

    if (currentCaseService.isCaseOpened.value) {
      const accessLevel = currentCaseService.getAccessLevelForVariable(path);
      caseStatusService.setHiddenVisibility(path, accessLevel);
    }
  }

  public created(el: HTMLElement, binding: DirectiveBinding, _: VNode) {
    // Attach the event listeners that update the model
    CustomModelDirective.addListenerOnInput(binding, el);
    CustomModelDirective.addListenerOnUpdate(binding, el);
  }

  public static addListenerOnInput(binding: DirectiveBinding<any>, el: HTMLElement) {
    //@ts-ignore
    el.onInputHandler = onInput.bind(null, binding, el);
    //@ts-ignore
    el.addEventListener('input', el.onInputHandler);
  }

  public static addListenerOnUpdate(binding: DirectiveBinding, el: HTMLElement) {
    //@ts-ignore
    el.onComputedValueUpdatedHandler = onComputedValueUpdated.bind(null, binding, el);
    //@ts-ignore
    el.addEventListener(InternalEvents.ComputedValueUpdated, el.onComputedValueUpdatedHandler);
  }
}

import type { ObjectDirective } from 'vue';
import CustomModelDirective, { updateValue, type DandelionDirectiveBinding } from './CustomModelDirective';
import { toNumber } from 'lodash';
import Helpers from '@/utils/Helpers';

export default class CustomModelDirectiveForRequirements implements ObjectDirective {
  public created(_el: HTMLElement, { instance, value: variablePath }: DandelionDirectiveBinding) {
    instance.caseStatusService.addRequirement(variablePath);
  }

  public beforeMount(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );
    CustomModelDirective.setupStatusService(path, instance, requiredForStatus);

    // if is a dataclass, we need to empty the model
    // this is counterintuitive, custom model directive for requirements fires when the dataclass is not active
    if (path.endsWith('.ACTIVE')) {
      updateValue(path.replace('.ACTIVE', ''), instance, {}, el);
    }
    updateValue(path, instance, false, el);
  }

  public unmounted(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    CustomModelDirective.myUnmounted(el, instance, path);

    const { caseStatusService, model, currentCaseService } = instance;

    if (currentCaseService.isCaseOpened.value) {
      Helpers.unsetObjByPath(path, model);

      const accessLevel = currentCaseService.getAccessLevelForVariable(path);
      caseStatusService.setHiddenVisibility(path, accessLevel);
      caseStatusService.removeRequirement(path);
    }
  }
}

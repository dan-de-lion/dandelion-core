import { toNumber } from 'lodash';
import { v4 as uuid } from 'uuid';
import CustomModelDirective, {
  updateValue,
  type DandelionDirectiveBinding,
  updateCaseStatus,
} from './CustomModelDirective';
import type { ComputedItemsUpdated } from '@/internal-events/ComputedItemsUpdated';
import { InternalEvents } from '@/types/InternalEvents';
import Helpers from '@/utils/Helpers';

function onComputedItemsUpdated({ instance, value: path }: DandelionDirectiveBinding, el: HTMLElement, e: Event) {
  const currentValue: Array<any> = Helpers.getObjByPath(path, instance.model);
  const itemsUpdateEvent = e as ComputedItemsUpdated;
  const { changedValue, itemsKeyValue } = itemsUpdateEvent;

  const newValue = changedValue.map((valueOfKey) => {
    const found = currentValue.find((el) => el[itemsKeyValue].toString() === valueOfKey.toString());
    return found ?? { [itemsKeyValue]: valueOfKey, guid: uuid() };
  });

  currentValue.splice(0, currentValue.length, ...newValue);
  updateValue(path, instance, currentValue, el);
  e.stopPropagation();
}

export default class CustomModelDirectiveForSets extends CustomModelDirective {
  public created(el: HTMLElement, binding: DandelionDirectiveBinding) {
    CustomModelDirective.addListenerOnInput(binding, el);
    CustomModelDirective.addListenerOnUpdate(binding, el);
    //@ts-ignore
    el.onComputedItemsUpdatedHandler = onComputedItemsUpdated.bind(null, binding, el);
    //@ts-ignore
    el.addEventListener(InternalEvents.ComputedItemsUpdated, el.onComputedItemsUpdatedHandler);
  }

  public beforeMount(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );

    CustomModelDirective.myBeforeMount(el, path, instance, requiredForStatus);
    // this is a trigger for the case status service in order to remove the set (non the elements of the set) from the list of requirements
    updateCaseStatus(instance, el, path, '***');
  }

  public unmounted(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    el.removeEventListener(
      InternalEvents.ComputedItemsUpdated,
      //@ts-expect-error
      el.onComputedItemsUpdatedHandler
    );
    CustomModelDirective.myUnmounted(el, instance, path);
  }
}

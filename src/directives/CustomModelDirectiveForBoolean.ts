import _, { toNumber } from 'lodash';
import type { DirectiveBinding } from 'vue';
import CustomModelDirective, { type DandelionDirectiveBinding, updateValue } from './CustomModelDirective';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import { ComputedValueUpdated } from '@/internal-events/ComputedValueUpdated';
import { InternalEvents } from '@/types/InternalEvents';
import Helpers from '@/utils/Helpers';

function onInputBoolean(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding, e: Event) {
  if (e instanceof ComputedValueUpdated) {
    throw new CoreError('Input event called with an UpdateEvent', CoreErrorType.DIRECTIVE);
  }
  const previous: boolean = Helpers.getObjByPath(path, instance.model);
  const now = el as HTMLInputElement;
  if (previous === now.checked) {
    return;
  }
  updateValue(path, instance, !previous, el);
  e.stopPropagation();
}

function onComputedValueUpdatedBoolean({ value: path, instance }: DandelionDirectiveBinding, e: Event) {
  const updateEvent = e as ComputedValueUpdated;
  const { changedValue } = updateEvent;
  // If the shorted-circuit-value is not significant, not update the value
  // i.e. Weight not inserted, IMC can't be calculated, then, the shorted-circuit-value calculated is an empty string
  // than, the IMC can be inserted manually
  if (changedValue === null) {
    return;
  }

  if (!(updateEvent.target instanceof HTMLOutputElement || updateEvent.target instanceof HTMLInputElement)) {
    throw new CoreError('Cannot assign short-circuit directive to a non input element', CoreErrorType.DIRECTIVE);
  }

  // Update the value based on shorted-circuit
  updateValue(path, instance, changedValue, updateEvent.target);
  e.stopPropagation();
}

export default class CustomModelDirectiveForBoolean extends CustomModelDirective {
  protected static updateView(el: HTMLElement, value: any) {
    if (_.isBoolean(value)) {
      (el as HTMLInputElement)!.checked = value;
    } else {
      (el as HTMLInputElement)!.checked = false;
    }
  }

  public beforeMount(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    const requiredForStatus = toNumber(
      el.attributes.getNamedItem('required-for-status')?.value ??
        instance.coreConfigurationService.defaultRequiredForStatus!.general
    );
    CustomModelDirective.myBeforeMount(el, path, instance, requiredForStatus);
    CustomModelDirective.setupWatcher(
      el,
      path,
      instance,
      CustomModelDirectiveForBoolean.updateView,
      CustomModelDirective.updateStatusService,
      requiredForStatus
    );
  }

  public created(el: HTMLElement, binding: DirectiveBinding) {
    // Attach the event listener that update the model
    CustomModelDirectiveForBoolean.addListenerOnInputBoolean(el, binding);
    CustomModelDirectiveForBoolean.addListenerOnUpdateBoolean(el, binding);
  }

  protected static addListenerOnInputBoolean(el: HTMLElement, binding: DirectiveBinding) {
    //@ts-ignore
    el.onInputHandler = onInputBoolean.bind(null, el, binding);
    //@ts-ignore
    el.addEventListener('input', el.onInputHandler);
  }

  protected static addListenerOnUpdateBoolean(el: HTMLElement, binding: DirectiveBinding) {
    //@ts-ignore
    el.onComputedValueUpdatedHandler = onComputedValueUpdatedBoolean.bind(null, binding);
    //@ts-ignore
    el.addEventListener(InternalEvents.ComputedValueUpdated, el.onComputedValueUpdatedHandler);
  }
}

import type { ObjectDirective } from 'vue';
import CustomModelDirective, { updateValue, type DandelionDirectiveBinding } from './CustomModelDirective';
import Helpers from '@/utils/Helpers';

export default class CustomModelDirectiveForCrfNotActive implements ObjectDirective {
  public created(_el: HTMLElement, _: DandelionDirectiveBinding) {
    // Necessary to overwrite parent "created" method
  }

  public beforeMount(el: HTMLElement, { instance, value: path }: DandelionDirectiveBinding) {
    CustomModelDirective.setupStatusService(path, instance, 1 /* when CRF is not active, it has status 0 */);

    // When this directive is shown, this means the crf is not active
    // because the conditions of activateIf are not met, so we need to:
    // 1. empty the CRF model
    updateValue(path.replace('.ACTIVE', ''), instance, {}, el);
    // 2. set the variable to false
    updateValue(path, instance, false, el);
  }

  public unmounted(el: HTMLElement, { value: path, instance }: DandelionDirectiveBinding) {
    const { caseStatusService, model, currentCaseService } = instance;

    if (currentCaseService.isCaseOpened.value) {
      Helpers.unsetObjByPath(path, model);

      const accessLevel = currentCaseService.getAccessLevelForVariable(path);
      caseStatusService.setHiddenVisibility(path, accessLevel);
    }
  }
}

import type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
import { ContainerTypes, getContainer } from '@/ContainersConfig';
import { NamedBindings } from '@/types/NamedBindings';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';

export function initializeNeededServices(context: string, containerType: ContainerTypes) {
  const currentCaseServiceTypeMap = {
    [ContainerTypes.NEW_CASE]: NamedBindings.CurrentCaseServiceForNewCase,
    [ContainerTypes.REEVALUATION]: NamedBindings.CurrentCaseServiceForReevaluation,
    [ContainerTypes.DEFAULT]: NamedBindings.CurrentCaseService,
  };

  const currentCaseServiceType = currentCaseServiceTypeMap[containerType] || NamedBindings.CurrentCaseService;
  const currentCaseService = getContainer(context, containerType).get(
    currentCaseServiceType
  ) as CurrentCaseServiceInterface;

  const caseStatusService: CurrentCaseServiceInterface = getContainer(context, containerType).get(
    NamedBindings.CaseStatusService
  );

  const coreConfigurationService: CoreConfigurationInterface = getContainer(context, containerType).get(
    NamedBindings.CoreConfiguration
  );
  const model = currentCaseService.getModel();

  return {
    caseStatusService,
    currentCaseService,
    coreConfigurationService,
    model,
  };
}

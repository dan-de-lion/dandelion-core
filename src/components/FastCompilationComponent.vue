<script setup lang="ts">
import { onErrorCaptured, type PropType, reactive, Ref, ref, toRefs, watch } from 'vue';
import { ContainerTypes, addContext, getContainer } from '@/ContainersConfig';
import PaginatedCaseList from '@/components/CaseList/PaginatedCaseListComponent.vue';
import CRFRenderComponent from '@/components/Internals/CRFRenderComponent.vue';
import ErrorDisplayerComponent from '@/components/Debug/ErrorDisplayerComponent.vue';
import { Case } from '@/entities/Cases';
import { Version } from '@/entities/CRF';
import { CaseMetaData } from '@/entities/CaseMetaData';
import type { ReadonlyCaseList, ReadonlyCaseListElement } from '@/entities/CaseListElement';
import type { CRF } from '@/entities/CRF';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import type { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import { CaseConfiguration } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { FilterInterface } from '@/interfaces/FilterInterface';
import type CaseReevaluationService from '@/services/CaseReevaluationService';
import type { CaseService } from '@/services/CaseService';
import type TranslationService from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { DandelionComponents } from '@/types/DandelionComponents';
import { NamedBindings } from '@/types/NamedBindings';
import { TypesOfActionHooks } from '@/types/TypesOfHooks';
import Helpers from '@/utils/Helpers';

const props = defineProps({
  context: {
    type: String as PropType<string>,
    required: false,
    default: Contexts.DEFAULT,
  },
  numberOfElementsPerPage: {
    type: Number as PropType<number>,
    required: false,
    default: 20,
  },
  startingDataclass: {
    type: String as PropType<string>,
    required: false,
    default: 'FastCompilation',
  },
  filters: {
    type: Array as PropType<FilterInterface[]>,
    required: false,
    default: () => [],
  },
});

const showErrorDialog = ref(false);
const errorContent = ref('');

onErrorCaptured((error, _vm, _info) => {
  if (error instanceof CRFError) {
    errorContent.value = `fastCompilation DataClass is necessary in CRF: ${error.message}`;
  }
  showErrorDialog.value = true;
});

const container = getContainer(props.context, ContainerTypes.REEVALUATION);
const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
const ts = container.get(NamedBindings.TranslationService) as TranslationService;
const caseService = getContainer(props.context).get(NamedBindings.CaseService) as CaseService;
const crfService = container.get(NamedBindings.CRFService) as CRFServiceInterface;
const caseServiceForReevaluation = container.get(NamedBindings.CaseReevaluationService) as CaseReevaluationService;

caseListService.setFilters(props.filters);
const caseList: ReadonlyCaseList = caseListService.slicedCases;
const caseWithVersions: Ref<Map<string, Map<string, Version>>> = ref(new Map());
const isUpdatingCaseList = caseListService.isUpdatingCaseList();
const listOfModelsAndMetaData: Map<string, { data: any; caseMetaData: CaseMetaData }> = reactive(
  new Map<string, any>()
);
const { numberOfElementsPerPage } = toRefs(props);

caseListService.startList.value = 0;
caseListService.endList.value = numberOfElementsPerPage.value;

async function updateVersionsForFast(): Promise<void> {
  if (caseList.length === 0) {
    return;
  }

  await Promise.all(
    caseList.map(async (xCase) => {
      const parser = new DOMParser();

      try {
        const crfs: Array<CRF> = await Promise.all(
          Array.from(xCase.caseMetaData.crfVersions?.entries()).map(([crfName, version]) =>
            crfService.getCRF(crfName, version)
          )
        );

        crfs.forEach((crf) => {
          let xmlDoc: Document;
          try {
            xmlDoc = parser.parseFromString(crf.xml, 'text/xml');
          } catch (e) {
            throw new CRFError(
              `parser.parseFromString failed for CRF: ${crf.name} v${crf.version}`,
              CRFErrorType.XML_PARSING,
              e
            );
          }

          // check if the dataclass is present in the CRF XML
          if (xmlDoc.querySelectorAll(`dataClass[name='${props.startingDataclass}']`).length !== 0) {
            // update versions map for this case
            const previousVersions = caseWithVersions.value.get(xCase.caseMetaData.caseID) ?? new Map();
            previousVersions.set(crf.name, crf.version);
            caseWithVersions.value.set(xCase.caseMetaData.caseID, previousVersions);
          }
        });
      } catch (e) {
        console.error(`Error processing case ${xCase.caseMetaData.caseID}:`, e);
        throw e;
      }
    })
  );
}

await updateVersionsForFast();

watch(caseList, async () => {
  const promisesToWait = caseList.map(async (xCase: ReadonlyCaseListElement) => {
    addContext(
      `fast_${xCase.caseMetaData.caseID}`,
      new CaseConfiguration({
        caseName: 'Admission',
        referenceDateVariableName: 'Admission.admissionDate',
      })
    );
    listOfModelsAndMetaData.set(xCase.caseMetaData.caseID, {
      data: (await caseService.getDataFor(xCase.caseMetaData.caseID)).data,
      caseMetaData: xCase.caseMetaData as CaseMetaData,
    });
  });
  await Promise.all(promisesToWait);
  await updateVersionsForFast();
});

async function saveCase(caseForReevaluation: Case) {
  await Helpers.runHook(props.context, TypesOfActionHooks.BeforeSaving);
  const caseRevaluated = await caseServiceForReevaluation.reevaluateCase(caseForReevaluation);
  await caseService.update(caseRevaluated);
}

async function saveAllCases() {
  for (const xCase of caseList) {
    await saveCase(
      new Case(
        xCase.caseMetaData.caseID,
        listOfModelsAndMetaData.get(xCase.caseMetaData.caseID)?.data as any,
        xCase.caseMetaData as CaseMetaData
      )
    );
  }
}

caseListService.startList.value = 0;
caseListService.endList.value = numberOfElementsPerPage.value;

async function updateCurrentPage({ startIndex, endIndex }: { startIndex: number; endIndex: number }) {
  caseListService.startList.value = startIndex;
  caseListService.endList.value = endIndex;
  await caseListService.updateCaseList();
}
</script>

<template>
  <section :class="[DandelionComponents.FAST, 'dandelion-cmp', 'fast-case-list', 'case-list']">
    <template v-if="caseWithVersions.size > 0 && !isUpdatingCaseList">
      <table class="fast-case-list-table case-list-table">
        <thead>
          <tr>
            <th class="case-id-head-cell head-cell" v-html="ts.t('Id')" />
            <th class="case-head-cell head-cell" v-html="ts.t('Case')" />
          </tr>
        </thead>
        <tbody>
          <tr v-for="xCase in caseWithVersions.keys()" :key="xCase" class="fast-case-list-row">
            <td v-html="xCase" />
            <td>
              <CRFRenderComponent
                ref="FCC"
                :component-name="DandelionComponents.FAST"
                :crfs-with-versions="caseWithVersions.get(xCase)"
                :starting-dataclass="startingDataclass"
                :model="listOfModelsAndMetaData.get(xCase)?.data"
                :context="'fast_' + xCase"
              />
              <div>
                <button
                  @click="
                    saveCase(
                      new Case(
                        xCase,
                        listOfModelsAndMetaData.get(xCase)?.data as any,
                        listOfModelsAndMetaData.get(xCase)?.caseMetaData as CaseMetaData
                      )
                    )
                  "
                  class="save-case-in-fast-btn fast-btn"
                  v-html="ts.t('Save')"
                />
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      <PaginatedCaseList
        :context="context"
        :number-of-elements-per-page="numberOfElementsPerPage"
        @update="updateCurrentPage"
        :deleted-case-list="false"
      />
      <div class="fast-compilation-utils-buttons">
        <button @click="saveAllCases" class="save-all-cases-in-fast-btn fast-btn" v-html="ts.t('Save All Cases')" />
      </div>
    </template>
    <div
      v-else-if="isUpdatingCaseList"
      class="loading-fast-compilation-msg loading-msg fast-msg"
      v-html="ts.t('Loading fast compilation...')"
    />
    <ErrorDisplayerComponent
      v-else
      :context="context"
      :error-content="ts.t('No Crfs found with Fast compilation DataClass')"
    />
  </section>
</template>

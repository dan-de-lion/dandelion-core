<script async lang="ts" setup>
import { v4 } from 'uuid';
import { ref, onMounted, createApp, type PropType, onBeforeUnmount, type App, toRaw } from 'vue';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import { CRFMounted } from '@/application-events/CRFMounted';
import CopyToClipboard from '@/components/Debug/CopyToClipboardComponent.vue';
import type { CRF, Version } from '@/entities/CRF';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import type { CRFToHTMLService } from '@/services/CRFToHTMLService';
import type TranslationService from '@/services/TranslationService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';
import { createCustomComponentStructure } from '@/utils/customComponentStructure';
import Helpers from '@/utils/Helpers';
import CurrentContextService from '@/services/CurrentContextService';

const props = defineProps({
  crfsWithVersions: {
    type: Object as PropType<ReadonlyMap<string, Version>>,
    required: true,
    default: new Map(),
  },
  context: {
    type: String as PropType<string>,
    required: false,
    default: Contexts.DEFAULT,
  },
  path: {
    type: String as PropType<string>,
    required: false,
    default: PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
  },
  model: {
    type: Object as PropType<any>,
    required: false,
    default: () => ({}),
  },
  componentName: {
    type: String as PropType<string>,
    required: true,
    default: '',
  },
  startingDataclass: {
    type: String as PropType<string>,
    required: false,
    default: '',
  },
  showGroupsInTree: {
    type: Boolean as PropType<boolean>,
    required: false,
    default: true,
  },
  showDataclassInstancesInTree: {
    type: Boolean as PropType<boolean>,
    required: false,
    default: true,
  },
  showSetsInTree: {
    type: Boolean as PropType<boolean>,
    required: false,
    default: true,
  },
  maxDepth: {
    type: Number as PropType<number>,
    required: false,
    default: MagicNumbers.maxDepthForCRFComponentAndTree,
  },
  crfWrappingFunction: {
    type: Function as PropType<any>,
    required: false,
    default: (html: any, _crf: any) => html,
  },
  crfRoot: {
    type: String as PropType<string>,
    required: false,
    default: undefined,
  },
  containerType: {
    type: String as PropType<ContainerTypes>,
    required: false,
    default: () => ContainerTypes.DEFAULT,
  },
  prefixForID: {
    type: String as PropType<string>,
    required: false,
    default: '',
  },
});

const container = getContainer(props.context);
const crfService = container.get(NamedBindings.CRFService) as CRFServiceInterface;
const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
const translationService = container.get(NamedBindings.TranslationService) as TranslationService;
const currentContextService = container.get(NamedBindings.CurrentContextService) as CurrentContextService;
const crfToHTMLService = container.get(NamedBindings.CRFToHTMLService) as CRFToHTMLService;
const logger = container.get(NamedBindings.Logger) as LoggerInterface;

const { crfWrappingFunction } = toRaw(props);
const errors = ref<Array<string>>([]);
let helperFunctions = {};
const mountId = ref<string>(`${props.componentName}-content-${v4()}`);
let baseUrlForXslt: string = coreConfiguration.baseUrlForXslt;
let customComponent: App | null = null;

const crfs: Array<CRF> = await Promise.all(
  Array.from(props.crfsWithVersions?.entries()).map(([crfName, version]) => crfService.getCRF(crfName, version))
);
crfs.forEach((crf) => translationService.addTranslations(crf.translations));
const unifiedJS = crfs.map((crf) => crf.js).join('');

if (
  coreConfiguration.baseUrlForOverrides &&
  coreConfiguration.listOfOverridesSchemas?.length! > 0 &&
  coreConfiguration.listOfOverridesSchemas?.includes(props.path)
) {
  const response = await fetch(coreConfiguration.baseUrlForOverrides + props.path);
  if (response.status !== 404 && response.status !== 202) {
    baseUrlForXslt = coreConfiguration.baseUrlForOverrides;
  }
}

const html = (
  await Promise.all(
    crfs.map((crf) =>
      crfWrappingFunction(
        crfToHTMLService.getHTMLfromXML(
          crf.xml,
          crf.name,
          crf.version,
          baseUrlForXslt + props.path,
          props.showGroupsInTree,
          props.showDataclassInstancesInTree,
          props.showSetsInTree,
          props.maxDepth,
          props.startingDataclass,
          // replace(/\s/g, '') => replaces spaces with empty char, needed for preventing possible errors using name containing spaces
          props.crfRoot ?? `model.${crf.name.replace(/\s/g, '')}`,
          props.prefixForID
        ),
        crf
      )
    )
  )
).join('');

try {
  helperFunctions = await import(`data:text/javascript,${unifiedJS}`);
} catch (e) {
  errors.value.push(`Problem importing JS code into the Render Component: ${e}`);
}

if (coreConfiguration.debug) {
  logger.log(`getHTMLFromXML_${mountId.value}`);
}

onMounted(() => {
  try {
    // create the Vue instance from the component generated from the html, binding context
    customComponent = createApp(
      createCustomComponentStructure(html, props.model, helperFunctions, props.containerType, props.context)
    );

    // add custom css
    const css = crfs.map((crf) => crf.css).join('');
    Helpers.addCss(css);

    // mount the component created on the mountId
    customComponent.mount(`#${mountId.value}`);

    // dispatch the event that the component is mounted
    getContext(props.context).dispatchEvent(
      ApplicationEvents.CRFMounted,
      new CRFMounted(currentContextService.containerType)
    );
  } catch (e) {
    logger.logError(`Problem mounting the component. Here you can see the error message: ${e}`);
    errors.value.push(`Problem mounting the component. Here you can see the error message: ${e}`);
  }
});

onBeforeUnmount(() => {
  if (customComponent) {
    customComponent.unmount();
  }
});
</script>

<template>
  <template v-if="errors.length > 0">
    <section v-for="(error, index) in errors" :key="index" class="crf-errors">
      <span v-html="error" />
    </section>
  </template>
  <Suspense>
    <CopyToClipboard
      :component-name="componentName"
      :context="context"
      :html="html"
      :model="model"
      :debug="coreConfiguration.debug"
    />
  </Suspense>
  <section :id="mountId" :class="[`${componentName}-content`, 'dandelion-cmp-content']" />
</template>

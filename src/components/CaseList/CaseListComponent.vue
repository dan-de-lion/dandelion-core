<script setup lang="ts">
import _ from 'lodash';
import { ref, type PropType, toRaw, computed } from 'vue';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import CaseListHeadComponent from './CaseListHeaderComponent.vue';
import CaseListRowComponent from './CaseListRowComponent.vue';
import NonPaginatedCaseListComponent from './NonPaginatedCaseListComponent.vue';
import PaginatedCaseList from './PaginatedCaseListComponent.vue';
import DeletionDialog from '@/components/Dialogs/DeletionDialogComponent.vue';
import PermaDeletionDialog from '@/components/Dialogs/PermaDeletionDialogComponent.vue';
import { VariableCaseListColumn } from '@/entities/caseListColumn/VariableCaseListColumn';
import type { ReadonlyCaseList } from '@/entities/CaseListElement';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import type { CaseListActionParams } from '@/interfaces/CaseListActionParams';
import type { CaseListActionInterface } from '@/interfaces/CaseListActionInterface';
import type { CaseListColumn } from '@/interfaces/CaseListColumn';
import { CaseListRowStyleInterface } from '@/interfaces/CaseListRowStyleInterface';
import type { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import type TranslationService from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { OpenCaseRequested } from '@/application-events/OpenCaseRequested';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';

const props = defineProps({
  caseListColumns: {
    type: Array as PropType<Array<CaseListColumn>>,
    required: false,
    default: () => new Array<CaseListColumn>(),
  },
  caseListRowStyles: {
    type: Array as PropType<Array<CaseListRowStyleInterface>>,
    required: false,
    default: () => new Array<CaseListRowStyleInterface>(),
  },
  context: {
    type: String as PropType<string>,
    required: false,
    default: Contexts.DEFAULT,
  },
  additionalActions: {
    type: Array as PropType<Array<CaseListActionInterface>>,
    required: false,
    default: () => [],
  },
  addDefaultActions: {
    type: Boolean as PropType<boolean>,
    required: false,
    default: true,
  },
  commonPagination: {
    type: Boolean as PropType<boolean>,
    required: false,
    default: false,
  },
  numberOfElementsPerPage: {
    type: Number as PropType<number>,
    required: false,
    default: 20,
  },
  openOnLineClick: {
    type: Boolean as PropType<boolean>,
    required: false,
    default: false,
  },
});

const container = getContainer(props.context);
const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
const currentContext = container.get(NamedBindings.CurrentContextService) as CurrentContextService;
const ts = container.get(NamedBindings.TranslationService) as TranslationService;
const isUpdatingCaseList = caseListService.isUpdatingCaseList();
const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;

const caseList: ReadonlyCaseList = caseListService.slicedCases;
const deletionDialogVisibility = ref(false);
const permaDeletionDialogVisibility = ref(false);
const deletingCaseID = ref('');
const arrayOfVariablesToBeCached = new Array<string>();

// if there are columns that are not cached, throw an error
const cachedVariables = currentContext.getCaseConfiguration().cachedVariables;
const caseListVariables = toRaw(props.caseListColumns)
  .filter((column) => column instanceof VariableCaseListColumn)
  .map((column) => (column as VariableCaseListColumn).getVariableName());

if (_.intersection(caseListVariables, cachedVariables).length < caseListVariables.length) {
  throw new ApplicationError('All columns must be cached in case configuration', AppErrorType.COLUMNS_NOT_CACHED);
}

props.caseListColumns.forEach((caseListColumn: CaseListColumn) => {
  if (caseListColumn instanceof VariableCaseListColumn) {
    arrayOfVariablesToBeCached.push(caseListColumn.getVariableName());
  }
});

caseListService.startList.value = 0;
caseListService.endList.value = props.numberOfElementsPerPage;

const defaultActions = [
  {
    name: 'deleteCase',
    actionClass: 'delete-case',
    label: 'Delete case',
    action: (values: CaseListActionParams) => showDeletionDialog(values.element.caseMetaData.caseID),
  },
  {
    name: 'openCase',
    actionClass: 'open-case',
    label: 'Open case',
    action: (values: CaseListActionParams) => openCase(values.element.caseMetaData.caseID),
    isVisible: () => !props.openOnLineClick,
  },
];
const arrayOfActions: Array<CaseListActionInterface> = [
  ...props.additionalActions,
  ...(props.addDefaultActions ? defaultActions : []),
];

function openCase(caseID: string) {
  getContext(props.context).dispatchEvent(
    ApplicationEvents.OpenCaseRequested,
    new OpenCaseRequested(ContainerTypes.DEFAULT, caseID)
  );
}

function showDeletionDialog(caseID: string) {
  deletingCaseID.value = caseID;
  deletionDialogVisibility.value = true;
}

function closeDeletionDialog() {
  deletionDialogVisibility.value = false;
}

function showPermaDeletionDialog() {
  permaDeletionDialogVisibility.value = true;
}

function closePermaDeletionDialog() {
  permaDeletionDialogVisibility.value = false;
}

async function updateCurrentPage({ startIndex, endIndex }: { startIndex: number; endIndex: number }) {
  caseListService.startList.value = startIndex;
  caseListService.endList.value = endIndex;
  await caseListService.updateCaseList();
}

caseListService.updateCaseList();

const areFiltersActive = computed(() => caseListService.areFiltersActive.value);
</script>

<template>
  <div class="case-list-wrapper">
    <DeletionDialog
      :context="context"
      :deleting-case-i-d="deletingCaseID"
      :deletion-dialog-visibility="deletionDialogVisibility"
      @close-deletion="closeDeletionDialog"
      @show-perma-deletion="showPermaDeletionDialog"
    />
    <PermaDeletionDialog
      :context="context"
      :deleting-case-i-d="deletingCaseID"
      :perma-deletion-dialog-visibility="permaDeletionDialogVisibility"
      @close-perma-deletion="closePermaDeletionDialog"
    />

    <section class="case-list">
      <!-- TODO: show it under rows (or in pagination) when cases are loading / filtering
      <section v-if="isUpdatingCaseList" class="loading-case-list-msg loading-msg case-list-msg">
        <span v-html="ts.t('Loading case list...')" />
      </section>
      -->
      <section v-if="caseListService.allCases.length === 0" class="no-cases-in-list-msg no-cases-msg case-list-msg">
        <span v-html="ts.t('No Cases In List')" />
      </section>
      <section
        v-else-if="areFiltersActive && caseListService.filteredCases.length === 0"
        class="no-items-found-in-list-msg case-list-msg"
      >
        <span v-html="ts.t('No found items')" />
      </section>
      <table v-else class="case-list-table">
        <CaseListHeadComponent :case-list-columns="caseListColumns" />
        <tbody>
          <CaseListRowComponent
            v-for="(row, rowIndex) in caseList"
            :row-index="rowIndex"
            :key="row?.caseMetaData?.caseID"
            :case-list-columns="caseListColumns"
            :case-list-row-styles="caseListRowStyles"
            :context="context"
            :actions="arrayOfActions"
            :open-on-line-click="openOnLineClick"
          />
        </tbody>
      </table>

      <template v-if="!commonPagination">
        <NonPaginatedCaseListComponent
          :context="context"
          :number-of-elements-per-page="numberOfElementsPerPage"
          :deleted-case-list="false"
          @update="updateCurrentPage"
        />
      </template>
      <template v-if="commonPagination">
        <PaginatedCaseList
          :context="context"
          :number-of-elements-per-page="numberOfElementsPerPage"
          :deleted-case-list="false"
          @update="updateCurrentPage"
        />
      </template>
    </section>
  </div>
</template>

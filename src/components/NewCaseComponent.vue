<script async lang="ts" setup>
import { onErrorCaptured, type PropType, ref, watch } from 'vue';
import type { CRF, CRFVersionValidity, Version } from '@/entities/CRF';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import { CaseCreationCanceled } from '@/application-events/CaseCreationCanceled';
import CRFRenderComponent from '@/components/Internals/CRFRenderComponent.vue';
import ErrorDisplayerComponent from '@/components/Debug/ErrorDisplayerComponent.vue';
import CaseFaker from '@/components/Debug/CaseFaker.vue';
import NoCaseSelectedComponent from '@/components/NoCaseSelectedComponent.vue';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type CaseStatusService from '@/services/CaseStatusService';
import { CRFVersionSelectionStrategy } from '@/services/CRFVersionSelectionStrategy';
import type CurrentCaseServiceForNewCase from '@/services/CurrentCaseServiceForNewCase';
import type CurrentContextService from '@/services/CurrentContextService';
import type TranslationService from '@/services/TranslationService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { DandelionComponents } from '@/types/DandelionComponents';
import { MagicNumbers } from '@/types/MagicNumbers';
import { MagicStrings } from '@/types/MagicStrings';
import { NamedBindings } from '@/types/NamedBindings';
import { TypesOfActionHooks } from '@/types/TypesOfHooks';
import Helpers from '@/utils/Helpers';

const props = defineProps({
  crfName: {
    type: String as PropType<string>,
    required: false,
    default: undefined,
  },
  context: {
    type: String as PropType<string>,
    required: false,
    default: Contexts.DEFAULT,
  },
  startingDataclass: {
    type: String as PropType<string>,
    required: false,
    default: MagicStrings.NEW_CASE_DATACLASS,
  },
  path: {
    type: String as PropType<string>,
    required: false,
    default: undefined,
  },
  statusRequired: {
    type: Number as PropType<number>,
    required: false,
    default: 3,
  },
});

const newCaseContainer = getContainer(props.context, ContainerTypes.NEW_CASE);
const currentContextService = newCaseContainer.get(NamedBindings.CurrentContextService) as CurrentContextService;
const currentCaseService = newCaseContainer.get(
  NamedBindings.CurrentCaseServiceForNewCase
) as CurrentCaseServiceForNewCase;
const caseStatusService = newCaseContainer.get(NamedBindings.CaseStatusService) as CaseStatusService;
const crfService = newCaseContainer.get(NamedBindings.CRFService) as CRFServiceInterface;
const coreConfiguration = newCaseContainer.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
const crfVersionSelectionStrategy = newCaseContainer.get(
  NamedBindings.CRFVersionSelectionStrategy
) as CRFVersionSelectionStrategy;
const ts = newCaseContainer.get(NamedBindings.TranslationService) as TranslationService;

const isCreatable = ref(false);
const isSaveable = ref(true);
const isLoading = ref(false);
const status = caseStatusService.getStatus();
const showErrorDialog = ref(false);
const errorContent = ref('');
const isCaseOpened = currentCaseService.isCaseOpened;

onErrorCaptured((error, _vm, _info) => {
  if (error instanceof CRFError) {
    errorContent.value = `NewCase DataClass is necessary in CRF: ${error.toString()}`;
  }
  showErrorDialog.value = true;
});

watch(status, () => {
  isCreatable.value = status.value >= props.statusRequired;
});

const crfName = props.crfName ?? currentContextService.getCaseConfiguration().getMainCRF().crfName;
const version = (await crfVersionSelectionStrategy.getUpdatedCRFVersionsByDate(new Date())).get(crfName);
if (!version) {
  throw new CRFError(`No active version found for CRF ${crfName}`, CRFErrorType.ACTIVE_VERSIONS_NOT_FOUND);
}

const crf: CRF = await crfService.getCRF(crfName, version);
const crfMap = new Map<string, Version>();
crfMap.set(crf.name, crf.version);
ts.addTranslations(crf.translations);

const newCaseModel = currentCaseService.getModel();

async function resetComponent() {
  await currentCaseService.terminateEditing();
  setTimeout(() => {
    isSaveable.value = true;
  }, MagicNumbers.preventMultipleClicksOnNewCaseTime);
}

async function onCaseCreation() {
  await Helpers.runHook(props.context, TypesOfActionHooks.BeforeCreating);
  isSaveable.value = false;
  isLoading.value = true;
  await currentCaseService.save();
  await resetComponent();
  isLoading.value = false;
}

async function onCaseCreationCanceled() {
  await Helpers.runHook(props.context, TypesOfActionHooks.BeforeCreationCanceling);
  await resetComponent();
  const event = new CaseCreationCanceled(currentContextService.containerType);
  getContext(props.context).dispatchEvent(ApplicationEvents.CaseCreationCanceled, event);
}
</script>

<template>
  <section v-if="isCaseOpened" :class="[DandelionComponents.NCC, 'dandelion-cmp']">
    <template v-if="coreConfiguration.debug">
      <ErrorDisplayerComponent v-if="showErrorDialog" :context="context" :error-content="errorContent" />
      <CaseFaker />
    </template>

    <form @submit.stop.prevent="onCaseCreation()" :class="[`${DandelionComponents.NCC}-form`, 'dandelion-cmp-form']">
      <Suspense>
        <CRFRenderComponent
          ref="NCC"
          :starting-dataclass="startingDataclass"
          :component-name="DandelionComponents.NCC"
          :crfs-with-versions="crfMap"
          :model="newCaseModel"
          :path="path"
          :context="context"
          :container-type="ContainerTypes.NEW_CASE"
        />
        <template #fallback>
          <!-- the SPAN (or any other HTML element) is mandatory to prevent complaining about multiple root nodes inside Suspense -->
          <span class="loading-new-case-msg loading-msg" v-html="ts.t('Loading new case...')" />
        </template>
      </Suspense>
      <button
        type="submit"
        class="create-case-btn new-case-btn primary"
        :disabled="!isCreatable || !isSaveable || isLoading"
      >
        <span v-if="isLoading" class="button-spinner" />
        <span v-else v-html="ts.t('Create')" />
      </button>

      <button
        type="button"
        class="cancel-creation-btn new-case-btn secondary"
        v-html="ts.t('Cancel')"
        @click="onCaseCreationCanceled"
      />
    </form>
  </section>
  <NoCaseSelectedComponent v-else />
</template>

<style>
.button-spinner {
  width: 0.75rem;
  height: 0.75rem;
  border: 3px solid rgba(255, 255, 255, 0.2);
  border-top: 3px solid #fff;
  border-radius: 50%;
  animation: spin 1s linear infinite;
}

@keyframes spin {
  0% {
    transform: rotate(0deg);
  }

  100% {
    transform: rotate(360deg);
  }
}
</style>

<script lang="ts" setup>
import { isEqual } from 'lodash';
import { onErrorCaptured, type PropType, ref } from 'vue';
import { getContainer } from '@/ContainersConfig';
import CRFRenderComponent from '@/components/Internals/CRFRenderComponent.vue';
import DeveloperErrorsDialogComponent from '@/components/Dialogs/DeveloperErrorsDialogComponent.vue';
import DiffDialog from '@/components/Dialogs/DiffDialog.vue';
import ErrorDisplayerComponent from '@/components/Debug/ErrorDisplayerComponent.vue';
import NoCaseSelectedComponent from '@/components/NoCaseSelectedComponent.vue';
import type { CRF } from '@/entities/CRF';
import { CRFError } from '@/errors/CRFError';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import type CurrentPageService from '@/services/CurrentPageService';
import type TranslationService from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { DandelionComponents } from '@/types/DandelionComponents';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { TypesOfActionHooks } from '@/types/TypesOfHooks';
import Helpers from '@/utils/Helpers';

addEventListener('beforeunload', (event) => {
  if (isModelChanged.value) {
    // if the model is changed, the user will be asked if he wants to leave the page
    event.preventDefault();
  }
});

const props = defineProps({
  context: {
    type: String as PropType<string>,
    required: false,
    default: Contexts.DEFAULT,
  },
  path: {
    type: String as PropType<string | undefined>,
    required: false,
    default: undefined,
  },
  startingDataclass: {
    type: String as PropType<string>,
    required: false,
    default: '',
  },
  defaultPage: {
    type: String as PropType<string>,
    required: false,
    default: '',
  },
});

const container = getContainer(props.context);
const ts = container.get(NamedBindings.TranslationService) as TranslationService;
const caseService = container.get(NamedBindings.CaseService) as CaseServiceInterface;
const logger = container.get(NamedBindings.Logger) as LoggerInterface;
const currentCaseService = container.get(NamedBindings.CurrentCaseService) as CurrentCaseServiceInterface;
const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfiguration;
const currentPageService = container.get(NamedBindings.CurrentPageService) as CurrentPageService;
const currentContext = container.get(NamedBindings.CurrentContextService) as CurrentContextService;
const crfVersionSelectionStrategy = container.get(
  NamedBindings.CRFVersionSelectionStrategy
) as CRFVersionSelectionStrategyInterface;

const showError = ref(false);
const errorContent = ref('');
const showDiffs = ref(false);
const showDevErrorsDialog = ref(false);
const isSaveable = ref(true);
const oldModel = ref({});
const isCaseOpened = currentCaseService.isCaseOpened;
const isModelChanged = currentCaseService.isModelChanged;

onErrorCaptured((error, _vm, _info) => {
  if (error instanceof CRFError) {
    errorContent.value = error.toString();
  }
  showError.value = true;
});

const model = currentCaseService.getModel();
const crfVersions = currentCaseService.getCrfVersions();
const mainCrf = currentContext.getCaseConfiguration().getMainCRF();

if (mainCrf) {
  const currentPage = props.defaultPage || mainCrf.crfName;
  currentPageService.set(Helpers.getContextForID(props.context, coreConfiguration) + currentPage);
}

const crfWrappingFunction = async (html: Promise<string>, crf: CRF) => {
  let result = '';
  result += `<section :id="getContextForID + '${crf.name}'" v-show="listOfActivePages.includes(getContextForID + '${crf.name}')">`;
  result += await html;
  result += '</section>';
  return result;
};

const saveCase = async () => {
  await Helpers.runHook(props.context, TypesOfActionHooks.BeforeSaving);
  await currentCaseService.save();
  setTimeout(() => {
    isSaveable.value = true;
  }, MagicNumbers.preventMultipleClicksOnNewCaseTime);
};

const checkReferenceDateChanged = async () => {
  isSaveable.value = false;
  if (await shouldShowDiffsBeforeSaving()) {
    showDiffs.value = true;
  } else {
    await saveCase();
  }
};

const shouldShowDiffsBeforeSaving = async () => {
  if (!coreConfiguration.showDifferencesOnSave) {
    return false;
  }
  try {
    const caseMetaData = await caseService.getCaseMetaDataFor(currentCaseService.getCurrentCase().value!);
    oldModel.value = currentCaseService.getPreviousModel();
    const referenceDateChanged = crfVersionSelectionStrategy.getReferenceDateIfChanged(
      oldModel.value,
      model,
      caseMetaData
    );
    if (!referenceDateChanged) {
      return false;
    }
    const previousVersions = currentCaseService.getPreviousCrfVersions();
    const updatedCrfVersions = await crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase(model, caseMetaData);
    return !isEqual(previousVersions, updatedCrfVersions);
  } catch (e: any) {
    logger.logError(e);
    return false;
  }
};

const closeDiff = () => {
  showDiffs.value = false;
  isSaveable.value = true;
};

const closeCase = async () => {
  await Helpers.runHook(props.context, TypesOfActionHooks.BeforeClosing);
  currentCaseService.terminateEditing();
};
</script>

<template>
  <section v-if="isCaseOpened" :class="[DandelionComponents.SCC, 'dandelion-cmp']">
    <DiffDialog
      v-if="coreConfiguration.showDifferencesOnSave"
      :context="context"
      :old-model="oldModel"
      :diff-dialog-visibility="showDiffs"
      @close-diff-dialog="closeDiff"
    ></DiffDialog>
    <DeveloperErrorsDialogComponent
      v-if="coreConfiguration.debug"
      :model="model"
      :context="context"
      :starting-dataclass="startingDataclass"
      :crf-versions="crfVersions"
      :crf-wrapping-function="crfWrappingFunction"
      :dev-errors-dialog-visibility="showDevErrorsDialog"
      @close-dev-errors="showDevErrorsDialog = false"
    ></DeveloperErrorsDialogComponent>
    <form
      @submit.stop.prevent="checkReferenceDateChanged()"
      :class="[`${DandelionComponents.SCC}-form`, 'dandelion-cmp-form']"
    >
      <Suspense>
        <CRFRenderComponent
          ref="CRC"
          :component-name="DandelionComponents.SCC"
          :context="context"
          :crfs-with-versions="crfVersions"
          :model="model"
          :path="path"
          :starting-dataclass="startingDataclass"
          :crf-wrapping-function="crfWrappingFunction"
        />
        <template #fallback>
          <!-- the SPAN (or any other HTML element) is mandatory to prevent complaining about multiple root nodes inside Suspense -->
          <span class="loading-single-case-msg loading-msg" v-html="ts.t('Loading single case...')" />
        </template>
      </Suspense>
      <ErrorDisplayerComponent v-if="showError" :context="context" :error-content="errorContent" />
      <div class="single-case-btns">
        <button
          type="submit"
          class="save-case-btn single-case-btn primary"
          :disabled="!isSaveable"
          v-html="ts.t('Save')"
        />
        <button
          type="button"
          @click="closeCase"
          class="close-case-btn single-case-btn secondary"
          v-html="ts.t('Close')"
        />
        <button
          type="button"
          v-if="coreConfiguration.debug"
          @click="showDevErrorsDialog = true"
          class="open-developer-errors-dialog-btn single-case-btn secondary"
          v-html="ts.t('Developer Errors')"
        />
      </div>
    </form>
  </section>
  <NoCaseSelectedComponent v-else />
</template>

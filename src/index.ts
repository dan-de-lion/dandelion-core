// Import vue component
import {
  addContext,
  ContainerTypes,
  type DynamicServiceInfo,
  getContainer,
  getContainerOwnership,
  getContext,
  getOwnershipForAllContainers,
  initializeShared,
  isSharedDynamicServiceInfo,
  isSharedServiceInfo,
  registerStatefulServices,
  removeContext,
  type ServiceInfo,
  type SharedDynamicServiceInfo,
  type SharedServiceInfo,
} from './ContainersConfig';
import { UserService } from '../test/mock-services/UserService';
import { CaseClosed } from '@/application-events/CaseClosed';
import { CaseCreated } from '@/application-events/CaseCreated';
import { CaseCreationCanceled } from '@/application-events/CaseCreationCanceled';
import { CaseCreationFailed } from '@/application-events/CaseCreationFailed';
import { CaseDeleted } from '@/application-events/CaseDeleted';
import { CaseDeletionFailed } from '@/application-events/CaseDeletionFailed';
import { CaseListUpdated } from '@/application-events/CaseListUpdated';
import { CRFMounted } from '@/application-events/CRFMounted';
import { CaseOpenedForEditing } from '@/application-events/CaseOpenedForEditing';
import { CaseOpenedForReevaluation } from '@/application-events/CaseOpenedForReevaluation';
import { CaseOpeningFailed } from '@/application-events/CaseOpeningFailed';
import { CasePermanentlyDeleted } from '@/application-events/CasePermanentlyDeleted';
import { CasePermanentlyDeletionFailed } from '@/application-events/CasePermanentlyDeletionFailed';
import { CaseRestored } from '@/application-events/CaseRestored';
import { CaseRestoringFailed } from '@/application-events/CaseRestoringFailed';
import { CaseUpdated } from '@/application-events/CaseUpdated';
import { CaseUpdateFailed } from '@/application-events/CaseUpdateFailed';
import { OpenCaseRequested } from '@/application-events/OpenCaseRequested';
import { NewCaseRequested } from '@/application-events/NewCaseRequested';
import { ReferenceDateChanged } from '@/application-events/ReferenceDateChanged';
import { VariableHovered } from '@/application-events/VariableHovered';
import CaseListComponent from '@/components/CaseList/CaseListComponent.vue';
import CaseListRowComponent from '@/components/CaseList/CaseListRowComponent.vue';
import CaseListHeaderComponent from '@/components/CaseList/CaseListHeaderComponent.vue';
import DeletedCaseListComponent from '@/components/CaseList/DeletedCaseListComponent.vue';
import EmptyCaseList from '@/components/CaseList/EmptyCaseList.vue';
import IdAndMainStatusCaseList from '@/components/CaseList/IdAndMainStatusCaseList.vue';
import IdAndStatusesCaseList from '@/components/CaseList/IdAndStatusesCaseList.vue';
import OnlyIdCaseList from '@/components/CaseList/OnlyIdCaseList.vue';
import OnlyMainStatusCaseList from '@/components/CaseList/OnlyMainStatusCaseList.vue';
import OnlyStatusesCaseList from '@/components/CaseList/OnlyStatusesCaseList.vue';
import CaseStatusComponent from '@/components/CaseStatusComponent.vue';
import DiffDialog from '@/components/Dialogs/DiffDialog.vue';
import CRFRenderComponent from '@/components/Internals/CRFRenderComponent.vue';
import DataclassItemComponent from '@/components/Internals/DataclassItemComponent.vue';
import ErrorsListComponent from '@/components/ErrorsListComponent.vue';
import ExportPanel from '@/components/Export/ExportPanel.vue';
import FastCompilationComponent from '@/components/FastCompilationComponent.vue';
import FilterComponent from '@/components/FilterComponent.vue';
import MissingsListComponent from '@/components/MissingsListComponent.vue';
import NewCaseComponent from '@/components/NewCaseComponent.vue';
import NewCaseButtonComponent from '@/components/NewCaseButtonComponent.vue';
import SingleCaseComponent from '@/components/SingleCaseComponent.vue';
import TreeComponent from '@/components/TreeComponent.vue';
import WarningsListComponent from '@/components/WarningsListComponent.vue';
import DiffListComponent from '@/components/DiffListComponent.vue';
import HistoryComponent from '@/components/HistoryComponent.vue';
import MissingListCounter from '@/components/MissingsListCounter.vue';
import CustomModelDirective from '@/directives/CustomModelDirective';
import type { ExportButtonParameters } from '@/entities/exportStrategy/ExportButtonParameters';
import { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import { CaseError, CaseErrorType } from '@/errors/CaseError';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import { DandelionError } from '@/errors/DandelionError';
import { FlowError, FlowErrorType } from '@/errors/FlowError';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
import { ComputedItemsUpdated } from '@/internal-events/ComputedItemsUpdated';
import { ComputedValueUpdated } from '@/internal-events/ComputedValueUpdated';
import { CaseDataExporterService } from '@/services/CaseDataExporterService';
import { CaseListService } from '@/services/CaseListService';
import CaseReevaluationService from '@/services/CaseReevaluationService';
import { CaseService } from '@/services/CaseService';
import CaseStatusService from '@/services/CaseStatusService';
import CaseVariablesCacheService from '@/services/CaseVariablesCacheService';
import { CRFToHTMLService } from '@/services/CRFToHTMLService';
import { CRFVersionSelectionStrategy } from '@/services/CRFVersionSelectionStrategy';
import { CRFVersionSelectionStrategyByCreationDate } from '@/services/CRFVersionSelectionStrategyByCreationDate';
import CurrentCaseService from '@/services/CurrentCaseService';
import CurrentContextService from '@/services/CurrentContextService';
import CurrentPageService from '@/services/CurrentPageService';
import CurrentCaseServiceForNewCase from '@/services/CurrentCaseServiceForNewCase';
import { CurrentCaseServiceForReevaluation } from '@/services/CurrentCaseServiceForReevaluation';
import { DandelionService } from '@/services/DandelionService';
import { DataSynchListService } from '@/services/DataSynchListService';
import { DataSynchService } from '@/services/DataSynchService';
import { ConsoleLogger } from '@/services/Logger';
import TranslationService, { Language } from '@/services/TranslationService';
import { CreateCaseEvent } from '@/source-events/list/CreateCaseEvent';
import { DeleteCaseEvent } from '@/source-events/list/DeleteCaseEvent';
import { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { PermanentlyDeleteCaseEvent } from '@/source-events/list/PermanentlyDeleteCaseEvent';
import { RestoreCase } from '@/source-events/list/RestoreCase';
import { UpdateCaseEvent } from '@/source-events/list/UpdateCaseEvent';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { TypesOfActionHooks, TypesOfDisplayHooks } from '@/types/TypesOfHooks';
import Helpers from '@/utils/Helpers';
import { Motivations } from '@/utils/MapForMotivations';
import { SchedulerService } from '@/services/SchedulerService';
import { Exportable } from './entities/exportable/Exportable';
import { ExportableVariable } from './entities/exportable/ExportableVariable';
import { ExportableCrfVersionMetaData } from './entities/exportable/ExportableCrfVersionMetaData';
import { ExportableFreeze } from './entities/exportable/ExportableFreeze';
import { ExportableId } from './entities/exportable/ExportableId';
import { ExportableMetaData } from './entities/exportable/ExportableMetaData';
import { ExportableStatus } from './entities/exportable/ExportableStatus';
import { ExportableStatusMetaData } from './entities/exportable/ExportableStatusMetaData';
import {
  CRFDataClassInterface,
  CRFDataClassChildrenVariableInterface,
  CRFValueInterface,
  CRFValuesSetInterface,
  CRFVariableInterface,
  CrfData,
} from './entities/CRFEntities';

// Declare install function executed by Vue.use()
function install(Vue: any) {
  // necessary
  // @ts-ignore
  if (install.installed) {
    return;
  }
  // necessary
  // @ts-ignore
  install.installed = true;
  Vue.component('CRFRenderComponent', CRFRenderComponent);
}

// Create module definition for Vue.use()
const plugin = {
  install,
};

// Auto-install when vue is found (for example. in browser via <script> tag)
let GlobalVue = null;
if (typeof window !== 'undefined') {
  // necessary
  // @ts-ignore
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  // necessary
  // @ts-ignore
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

export default install;

export function setupDandelion({
  coreConfiguration,
  sharedServices,
  services,
}: {
  coreConfiguration: CoreConfiguration;
  sharedServices: Array<SharedServiceInfo<any> | SharedDynamicServiceInfo<any>>;
  services: Array<ServiceInfo<any> | DynamicServiceInfo<any>>;
}) {
  initializeShared();
  getContainer(Contexts.SHARED)
    .bind(NamedBindings.CoreConfiguration)
    .toDynamicValue(() => coreConfiguration)
    .inSingletonScope();
  getContainer(Contexts.SHARED).bind(NamedBindings.Scheduler).to(SchedulerService).inSingletonScope();

  for (const service of sharedServices) {
    if (isSharedServiceInfo(service)) {
      getContainer(Contexts.SHARED).bind(service.name).to(service.class).inSingletonScope();
    } else if (isSharedDynamicServiceInfo(service)) {
      getContainer(Contexts.SHARED).bind(service.name).toDynamicValue(service.instance).inSingletonScope();
    }
  }

  for (const service of sharedServices) {
    getContainer(Contexts.SHARED).get(service.name);
  }

  registerStatefulServices([
    ...services,
    {
      name: NamedBindings.CaseDataExporterService,
      class: CaseDataExporterService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CaseReevaluationService,
      class: CaseReevaluationService,
      containerTypes: [ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CaseListService,
      class: CaseListService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CaseService,
      class: CaseService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CaseStatusService,
      class: CaseStatusService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.NEW_CASE, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.CurrentCaseServiceForNewCase,
      class: CurrentCaseServiceForNewCase,
      containerTypes: [ContainerTypes.NEW_CASE],
    },
    {
      name: NamedBindings.CurrentCaseServiceForReevaluation,
      class: CurrentCaseServiceForReevaluation,
      containerTypes: [ContainerTypes.REEVALUATION],
    },
    { name: NamedBindings.CurrentCaseService, class: CurrentCaseService, containerTypes: [ContainerTypes.DEFAULT] },
    {
      name: NamedBindings.CurrentPageService,
      class: CurrentPageService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.DataSynchService,
      class: DataSynchService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
    {
      name: NamedBindings.DataSynchListService,
      class: DataSynchListService,
      containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION],
    },
  ]);

  const caseTypes = coreConfiguration.listOfCaseTypes.slice();

  // the default context is mapped to the first caseType
  const defaultContext = addContext(caseTypes[0].caseName, caseTypes[0]);

  // remove the first caseType from the list
  caseTypes.shift();

  // create own context for every caseType
  caseTypes.forEach((caseType) => {
    addContext(caseType.caseName, caseType);
  });
}

// Config
export { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
export type {
  CaseConfigurationInterface,
  CoreConfigurationInterface,
  CaseLinkConfigurationInterface,
  CRFConfigurationInterface,
  MarginForScroll,
} from '@/interfaces/CoreConfiguration';
export { initializeNeededServices } from '@/composables/PluginVariableComposable';

// Contexts
export {
  addContext,
  Contexts,
  getContainer,
  getContext,
  removeContext,
  ContainerTypes,
  getContainerOwnership,
  getOwnershipForAllContainers,
};

// Components
export {
  CaseListComponent,
  CaseListRowComponent,
  CaseListHeaderComponent,
  CaseStatusComponent,
  CRFRenderComponent,
  DataclassItemComponent,
  DeletedCaseListComponent,
  DiffDialog,
  EmptyCaseList,
  IdAndMainStatusCaseList,
  IdAndStatusesCaseList,
  OnlyIdCaseList,
  OnlyMainStatusCaseList,
  OnlyStatusesCaseList,
  ErrorsListComponent,
  FastCompilationComponent,
  FilterComponent,
  MissingsListComponent,
  MissingListCounter,
  NewCaseComponent,
  NewCaseButtonComponent,
  SingleCaseComponent,
  TreeComponent,
  WarningsListComponent,
  DiffListComponent,
  HistoryComponent,
  ExportPanel,
};

// Directives
export { CustomModelDirective };
export { default as ComputedItemsDirective } from '@/directives/ComputedItemsDirective';
export { default as CustomModelDirectiveForBoolean } from '@/directives/CustomModelDirectiveForBoolean';
export { default as CustomModelDirectiveForCheckboxes } from '@/directives/CustomModelDirectiveForCheckboxes';
export { default as CustomModelDirectiveForErrors } from '@/directives/CustomModelDirectiveForErrors';
export { default as CustomModelDirectiveForRequirements } from '@/directives/CustomModelDirectiveForRequirements';
export { default as CustomModelDirectiveForSelect } from '@/directives/CustomModelDirectiveForSelect';
export { default as CustomModelDirectiveForSets } from '@/directives/CustomModelDirectiveForSets';
export { default as CustomModelDirectiveForSinglechoice } from '@/directives/CustomModelDirectiveForSinglechoice';
export { CustomModelDirectiveForWarnings } from '@/directives/CustomModelDirectiveForWarnings';
export { default as ShortCircuitDirective } from '@/directives/ShortCircuitDirective';

// Entities
export { CrfVersionMetaDataCaseListColumn } from '@/entities/caseListColumn/CrfVersionMetaDataCaseListColumn';
export { FreezeCaseListColumn } from '@/entities/caseListColumn/FreezeCaseListColumn';
export { IdCaseListColumn } from '@/entities/caseListColumn/IdCaseListColumn';
export { LockCaseListColumn } from '@/entities/caseListColumn/LockCaseListColumn';
export { MetaDataCaseListColumn } from '@/entities/caseListColumn/MetaDataCaseListColumn';
export { StatusCaseListColumn } from '@/entities/caseListColumn/StatusCaseListColumn';
export { VariableCaseListColumn } from '@/entities/caseListColumn/VariableCaseListColumn';
export { CaseListElement, type ReadonlyCaseListElement, type ReadonlyCaseList } from '@/entities/CaseListElement';
export type { CachedCaseListElement } from '@/services/CaseListService';
export type { CaseMetaData, SaveMetaData, CaseInfo, StateMetaData } from '@/entities/CaseMetaData';
export { CRF, CRFVersionValidity, type Version } from '@/entities/CRF';
export { PartialStoredCase } from '@/entities/PartialStoredCase';
export { PartialCase } from '@/entities/PartialCase';
export { Case, CaseForUpdate } from '@/entities/Cases';
export { SynchListElement } from '@/entities/SynchListElement';
export { ValuesSetValue } from '@/entities/ValuesSetValue';
export { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
export type { CrfWithValidity, CrfStartAndEndDates } from '@/entities/CrfWithValidity';
export type { CaseCacheDTO, CaseID } from '@/services/CaseVariablesCacheService';
export { User } from '@/entities/User';
export { HistoryPartialStoredCase };
export { ExportButtonParameters };
export { ApplicationError };
export { CaseError };
export { CoreError };
export { CRFError };
export { DandelionError };
export { FlowError };
export { StorageError };
export { Exportable };
export { ExportableVariable };
export { ExportableFreeze };
export { ExportableCrfVersionMetaData };
export { ExportableId };
export { ExportableMetaData };
export { ExportableStatus };
export { ExportableStatusMetaData };
export { type CRFDataClassInterface };
export { type CRFDataClassChildrenVariableInterface };
export { type CRFValueInterface };
export { type CRFValuesSetInterface };
export { type CRFVariableInterface };
export { type CrfData };

// Enum
export { Motivations };
export { NamedBindings };
export { Language };
export { MagicNumbers };
export { ApplicationEvents };
export { TypesOfDisplayHooks };
export { TypesOfActionHooks };
export { AppErrorType };
export { CaseErrorType };
export { CoreErrorType };
export { CRFErrorType };
export { FlowErrorType };
export { StorageErrorType };
export { PathToXsl } from '@/types/PathToXsl';

// Events
export {
  CaseClosed,
  CaseCreated,
  CaseCreationCanceled,
  CaseCreationFailed,
  CaseDeleted,
  CaseDeletionFailed,
  CaseListUpdated,
  CRFMounted,
  CaseOpenedForEditing,
  CaseOpenedForReevaluation,
  CaseOpeningFailed,
  CasePermanentlyDeleted,
  CasePermanentlyDeletionFailed,
  CaseRestored,
  CaseRestoringFailed,
  CaseUpdated,
  CaseUpdateFailed,
  ComputedItemsUpdated,
  ComputedValueUpdated,
  OpenCaseRequested,
  NewCaseRequested,
  ReferenceDateChanged,
  VariableHovered,
};

// Eventsrc
export {
  CreateCaseEvent,
  DeleteCaseEvent,
  SourceEventForList,
  PermanentlyDeleteCaseEvent,
  RestoreCase,
  UpdateCaseEvent,
};

// Types
export type { AccessLevelsTree } from '@/interfaces/AccessLevelsTree';
export type { ActionHookInterface } from '@/interfaces/ActionHookInterface';
export type { CaseListActionInterface } from '@/interfaces/CaseListActionInterface';
export type { CaseListActionParams } from '@/interfaces/CaseListActionParams';
export type { CaseListColumn } from '@/interfaces/CaseListColumn';
export type { CaseListRowStyleInterface } from '@/interfaces/CaseListRowStyleInterface';
export type { FilterInterface } from '@/interfaces/FilterInterface';
export type { AccessLevels, AccessLevelAndCrfConfigurations } from '@/types/AccessLevels';
export type { AccessRights } from '@/types/AccessRights';

// Services
export {
  CaseListService,
  CaseReevaluationService,
  CaseService,
  CaseStatusService,
  CaseVariablesCacheService,
  CRFToHTMLService,
  CRFVersionSelectionStrategy,
  CRFVersionSelectionStrategyByCreationDate,
  CurrentCaseService,
  CurrentContextService,
  CurrentPageService,
  DataSynchListService,
  DataSynchService,
  TranslationService,
  CaseDataExporterService,
  ConsoleLogger,
  DandelionService,
  UserService,
  SchedulerService,
};

// Service interfaces
export type { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
export type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
export type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
export type { CaseVariablesCacheServiceInterface } from '@/interfaces/CaseVariablesCacheServiceInterface';
export type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
export type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
export type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
export type { LoggerInterface } from '@/interfaces/LoggerInterface';
export type { UserServiceInterface } from '@/interfaces/UserServiceInterface';

// Storage interfaces
export type {
  CaseStorageInterface,
  MultiCaseMetaDTO,
  MultiCaseMetaErrorDTO,
  MultiCaseMetaResponse,
  MultiPartialsDTO,
  MultiPartialsErrorDTO,
  MultiPartialsResponse,
} from '@/interfaces/CaseStorageInterface';
export type { ActivationPeriodsForCrfStorageInterface } from '@/interfaces/ActivationPeriodsForCrfStorageInterface';
export type { CustomExportsStorageInterface } from '@/interfaces/CustomExportsStorageInterface';
export type { DataSynchStorageInterface } from '@/interfaces/DataSynchStorageInterface';
export type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
export type {
  KeyStorageInterface,
  MultiKeysDTO,
  MultiKeysErrorDTO,
  MultiKeysResponse,
} from '@/interfaces/KeyStorageInterface';
export type { VariablesCacheStorageInterface } from '@/interfaces/VariablesCacheStorageInterface';

// Utils
export { Helpers };
export { Crypt } from '@/utils/Crypt';
export { substituteModelInPlace } from '@/utils/substituteModelInPlace';
export { type SerDes, SerDesBrick, Stringifier, AxiosStringifier, AxiosReviver, AxiosSerializer } from '@/utils/SerDes';

//Zod
export { parseElementWithZod, ZodSchemas } from '@/utils/ValidationSchemas/handleZod';
export { type Zversions, ZversionsSchema } from '@/utils/ValidationSchemas/CRFVersionSchema';
export {
  type ZcrfWithValidityForCentreCode,
  ZcrfWithValidityForCentreCodeSchema,
} from '@/utils/ValidationSchemas/CRFWithValiditySchema';
export {
  type ZcaseListSnapshot,
  type ZcaseListSnapshots,
  ZcaseListSnapshotSchema,
} from '@/utils/ValidationSchemas/SnapshotSchema';
export { type ZsourceEvents, ZsourceEventsSchema } from '@/utils/ValidationSchemas/SourceEventSchema';

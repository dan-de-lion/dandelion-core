import { InternalEvents } from '@/types/InternalEvents';

export class ComputedItemsUpdated extends Event {
  constructor(readonly changedValue: Array<string>, readonly itemsKeyValue: string) {
    super(InternalEvents.ComputedItemsUpdated);
  }
}

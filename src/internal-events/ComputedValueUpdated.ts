import { InternalEvents } from '@/types/InternalEvents';

export class ComputedValueUpdated extends Event {
  readonly changedValue: any;

  constructor(changedValue: any) {
    super(InternalEvents.ComputedValueUpdated);
    this.changedValue = changedValue;
  }
}

import { faker } from '@faker-js/faker';
import { getContainer } from '@/ContainersConfig';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { NamedBindings } from '@/types/NamedBindings';
import { DandelionService } from '@/services/DandelionService';
import CurrentContextService from '@/services/CurrentContextService';
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { Case } from '@/entities/Cases';

export class CasesFaker extends DandelionService {
  protected caseService: CaseServiceInterface;
  protected currentContextService: CurrentContextService;
  protected caseListService: CaseListServiceInterface;
  protected crfVersionStrategy: CRFVersionSelectionStrategyInterface;
  protected userService: UserServiceInterface;

  constructor() {
    super();
    this.caseService = getContainer().get(NamedBindings.CaseService) as CaseServiceInterface;
    this.currentContextService = getContainer().get(NamedBindings.CurrentContextService) as CurrentContextService;
    this.caseListService = getContainer().get(NamedBindings.CaseListService) as CaseListServiceInterface;
    this.crfVersionStrategy = getContainer().get(
      NamedBindings.CRFVersionSelectionStrategy
    ) as CRFVersionSelectionStrategyInterface;
    this.userService = getContainer().get(NamedBindings.UserService) as UserServiceInterface;
  }

  async generateFakeCases(casesNumber: number): Promise<void> {
    for (let i = 0; i < casesNumber; i++) {
      const accessLevel = faker.helpers.arrayElement(['public']);
      const caseType = this.currentContextService.getCaseConfiguration().caseName;
      const caseID = faker.string.uuid();
      const creationDate = new Date();
      // const isLocked = faker.datatype.boolean(0.25); cant'create a case already locked

      const model = {
        [caseType]: {
          nome: faker.person.firstName(),
          admissionDate: faker.date
            .between({ from: '2024-06-01T00:00:00.000Z', to: new Date() })
            .toISOString()
            .split('T')[0],
          vivoMorto: faker.helpers.arrayElement(['Vivo', 'Morto']),
          booleanProva: faker.datatype.boolean(),
          multipleProva: faker.helpers.arrayElements(
            ['multipleSet.uno', 'multipleSet.due', 'multipleSet.tre'],
            faker.number.int({ min: 0, max: 3 })
          ),
          REQUIREMENT_requirement: faker.datatype.boolean(),
          WARNING_warning_CONFIRMED: faker.datatype.boolean(),
        },
      };

      const caseMetaData: CaseMetaData = {
        caseID,
        centreCode: this.userService.getCurrentUser().value?.centre || '',
        creationDate,
        caseType,
        status: new Map().set(caseType, faker.number.int(4)),
        crfVersions: await this.crfVersionStrategy.getUpdatedCRFVersionsByCase(model, {
          creationDate,
          caseType,
        } as CaseMetaData),
        lastUpdate: creationDate,
        /* isLocked,
                 lockUser: isLocked ? faker.internet.userName() : '',
                 lockExpires: isLocked ? faker.date.future({ years: 1 }) : new Date(0), */
        frozenAccessLevels: new Map().set(accessLevel, faker.datatype.boolean(0.25)),
      };

      await this.caseService.create(new Case(caseID, model, caseMetaData));
      await this.caseListService.addCase(caseID, creationDate);
      await this.caseListService.changedReferenceDate(caseID, new Date(model[caseType].admissionDate), creationDate);
    }
  }
}

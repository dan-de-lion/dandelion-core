import { injectable } from 'inversify';

export enum Motivations {
  DUPLICATE = 'DUPLICATE',
  INSERTION_ERROR = 'INSERTION_ERROR',
  OTHER_MOTIVATIONS = 'OTHER_MOTIVATIONS',
  REQUESTED_BY_PATIENT = 'REQUESTED_BY_PATIENT',
}

@injectable()
export class MapForMotivations {
  protected map: Map<Motivations, string> = new Map();

  constructor() {
    this.map.set(Motivations.DUPLICATE, 'The record was a duplicate');
    this.map.set(Motivations.INSERTION_ERROR, 'The record was inserted by error');
    this.map.set(Motivations.OTHER_MOTIVATIONS, 'Other motivations');
    this.map.set(Motivations.REQUESTED_BY_PATIENT, 'The patient has requested cancellation from records');
  }

  get() {
    return this.map;
  }
}

import { ValuesSetValue } from '@/entities/ValuesSetValue';

export const DANDELION_MODEL_UUID = Symbol.for('dandelion_model_uuid');
function isDictionaryOrArray(variable: string | any) {
  return variable !== null && typeof variable === 'object' && !(variable instanceof ValuesSetValue);
}

function removeEmptySpotsFromArray(original: Record<string | number, any>) {
  for (let k = original.length - 1; k >= 0; k--) {
    if (!original[k]) {
      original.splice(k, 1);
    }
  }
}

function fillDataInPlace(original: Record<string | number, any>, newData: Record<string | number, any>): void {
  if (!newData) {
    return;
  }

  for (const obj in newData) {
    if (isDictionaryOrArray(newData[obj]) && isDictionaryOrArray(original[obj])) {
      fillDataInPlace(original[obj], newData[obj]);
    } else {
      original[obj] = newData[obj];
    }
  }

  for (const obj of Object.getOwnPropertySymbols(newData)) {
    // necessary
    //@ts-expect-error
    if (isDictionaryOrArray(newData[obj]) && isDictionaryOrArray(original[obj])) {
      // necessary
      //@ts-expect-error
      fillDataInPlace(original[obj], newData[obj]);
    } else {
      // necessary
      //@ts-expect-error
      original[obj] = newData[obj];
    }
  }
}

function removeElementsNotInNewData(original: Record<string | number, any>, newData: Record<string | number, any>) {
  if (!newData || !original) {
    return;
  }

  for (const obj in original) {
    if (!(obj in newData)) {
      delete original[obj];
    } else if (isDictionaryOrArray(original[obj])) {
      removeElementsNotInNewData(original[obj], newData[obj]);
    }
  }
  for (const obj of Object.getOwnPropertySymbols(original)) {
    if (!Object.getOwnPropertySymbols(original).includes(obj) && obj !== DANDELION_MODEL_UUID) {
      // necessary
      //@ts-expect-error
      delete original[obj];
      // necessary
      //@ts-expect-error
    } else if (isDictionaryOrArray(original[obj])) {
      // necessary
      // @ts-expect-error
      removeElementsNotInNewData(original[obj], newData[obj]);
    }
  }

  if (Array.isArray(original)) {
    removeEmptySpotsFromArray(original);
  }
}

export function substituteModelInPlace(original: Record<string, any>, newData: Record<string, any>) {
  removeElementsNotInNewData(original, newData);
  fillDataInPlace(original, newData);
}

import { v4 as uuid } from 'uuid';
import { defineComponent, reactive, computed } from 'vue';
import variablePatternsJson from '../../public/variablePatterns.json';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import { VariableHovered } from '@/application-events/VariableHovered';
import DataclassItemComponent from '@/components/Internals/DataclassItemComponent.vue';
import SetComponent from '@/components/Internals/SetComponent.vue';
import SetItemComponent from '@/components/Internals/SetItemComponent.vue';
import DiffListComponent from '@/components/DiffListComponent.vue';
import ResetWhenHidden from '@/directives/ResetWhenHidden';
import ComputedItemsDirective from '@/directives/ComputedItemsDirective';
import CustomModelDirective from '@/directives/CustomModelDirective';
import CustomModelDirectiveForBoolean from '@/directives/CustomModelDirectiveForBoolean';
import CustomModelDirectiveForCheckboxes from '@/directives/CustomModelDirectiveForCheckboxes';
import CustomModelDirectiveForErrors from '@/directives/CustomModelDirectiveForErrors';
import CustomModelDirectiveForRequirements from '@/directives/CustomModelDirectiveForRequirements';
import CustomModelDirectiveForSinglechoice from '@/directives/CustomModelDirectiveForSinglechoice';
import { CustomModelDirectiveForWarnings } from '@/directives/CustomModelDirectiveForWarnings';
import CustomModelDirectiveForSelect from '@/directives/CustomModelDirectiveForSelect';
import CustomModelDirectiveForSets from '@/directives/CustomModelDirectiveForSets';
import ShortCircuitDirective from '@/directives/ShortCircuitDirective';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import type CurrentPageService from '@/services/CurrentPageService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { TypesOfDisplayHooks } from '@/types/TypesOfHooks';
import Helpers from '@/utils/Helpers';
import TranslationService from '@/services/TranslationService';
import CustomModelDirectiveForCrfNotActive from '@/directives/CustomModelDirectiveForCrfNotActive';

export function createCustomComponentStructure(
  html: string,
  startingModel: any,
  helperFunctions: any,
  containerType: ContainerTypes = ContainerTypes.DEFAULT,
  context = Contexts.DEFAULT
) {
  const requiredServices = [
    NamedBindings.CaseStatusService,
    NamedBindings.CoreConfiguration,
    NamedBindings.CurrentPageService,
    NamedBindings.TranslationService,
    NamedBindings.UserService,
  ];
  const missingServices = requiredServices.filter((service) => !getContainer(context, containerType).isBound(service));

  if (missingServices.length > 0) {
    throw new ApplicationError(
      `Cannot create custom component structure, missing services: ${missingServices.join(', ')},`,
      AppErrorType.MISSING_SERVICES
    );
  }
  const coreConfiguration = getContainer(context, containerType).get(
    NamedBindings.CoreConfiguration
  ) as CoreConfigurationInterface;
  return defineComponent({
    template:
      !html && coreConfiguration.debug
        ? `<span v-html='translationService.t("Insert a valid XML")'></span> `
        : `${html}`,
    mixins: [{ methods: helperFunctions }],
    setup() {
      const model = reactive(startingModel);
      const caseStatusService: CaseStatusServiceInterface = getContainer(context, containerType).get(
        NamedBindings.CaseStatusService
      );
      const currentCaseServiceTypeMap = {
        [ContainerTypes.NEW_CASE]: NamedBindings.CurrentCaseServiceForNewCase,
        [ContainerTypes.REEVALUATION]: NamedBindings.CurrentCaseServiceForReevaluation,
        [ContainerTypes.DEFAULT]: NamedBindings.CurrentCaseService,
      };

      const currentCaseServiceType = currentCaseServiceTypeMap[containerType] || NamedBindings.CurrentCaseService;
      const currentCaseService = getContainer(context, containerType).get(currentCaseServiceType);
      const coreConfigurationService = getContainer(context, containerType).get(
        NamedBindings.CoreConfiguration
      ) as CoreConfigurationInterface;
      const currentPageService = getContainer(context, containerType).get(
        NamedBindings.CurrentPageService
      ) as CurrentPageService;
      const userService = getContainer(context, containerType).get(NamedBindings.UserService) as UserServiceInterface;
      const listOfActivePages = currentPageService.listOfActivePages;

      const resetRadioButton = (name: string) => {
        Helpers.setObjByPath(name, model, '');
      };

      // first thisSet is empty
      const thisSet: any = [];

      const translationService = getContainer(context, containerType).get(
        NamedBindings.TranslationService
      ) as TranslationService;

      const getContextForID = computed(() => {
        // Contexts.DEFAULT is the shorthand for the real default context, which depends on the caseTypes that are used in the application
        // In the case of default context we don't add a prefix to IDs
        if (context === Contexts.DEFAULT || context === coreConfiguration.listOfCaseTypes[0].caseName) {
          return '';
        }
        return `${context}_`;
      });

      const setYear = (date: string, year: number) => {
        if (!date) {
          return '';
        }
        return year.toString() + date.substring(4);
      };

      const addElementToSet = (thisSet: Array<any>) => {
        const guid = uuid();
        thisSet.push({ guid });
        return guid;
      };

      const removeElementFromSet = (thisSet: Array<any>, indexToRemove: number) => {
        if (indexToRemove > -1) {
          thisSet.splice(indexToRemove, 1);
        }
      };

      const onHover = (fullName: string) => {
        const event = new VariableHovered(fullName, containerType);
        getContext(context).dispatchEvent(ApplicationEvents.VariableHovered, event);
      };

      const getHooksByType = (type: TypesOfDisplayHooks) => coreConfiguration.displayHooks?.get(type) || [];

      const checkValidity = (variableName: string, model: any, patternName: string) => {
        try {
          const value = Helpers.getObjByPath(variableName, model);
          if (!value) {
            return false;
          }
          const variablePatterns: Record<string, { pattern: string }> = variablePatternsJson;
          const pattern: string = variablePatterns[patternName]?.pattern;
          if (!pattern) {
            return false;
          }
          return !new RegExp(pattern).test(value);
        } catch {
          return false;
        }
      };

      const checkMinValidity = (variableName: string, model: any, min: number) => {
        try {
          const value = Helpers.getObjByPath(variableName, model);
          if (min !== undefined && value < min && !isNaN(value)) {
            return true;
          }
          return false;
        } catch (error) {
          return false;
        }
      };

      const checkMaxValidity = (variableName: string, model: any, max: number) => {
        try {
          const value = Helpers.getObjByPath(variableName, model);
          if (max !== undefined && value > max && !isNaN(value)) {
            return true;
          }
          return false;
        } catch (error) {
          return false;
        }
      };

      const nextItem = (thisSet: any, thisIndex: number, step?: number) => {
        if (!thisSet || !thisIndex || thisIndex < 0) {
          return undefined;
        }
        if (step !== undefined) {
          return thisSet[thisIndex + step];
        }
        return thisSet[thisIndex + 1];
      };

      const previousItem = (thisSet: any, thisIndex: number, step?: number) => {
        if (!thisSet || !thisIndex || thisIndex < 0) {
          return undefined;
        }
        if (step !== undefined) {
          return thisSet[thisIndex - step];
        }
        return thisSet[thisIndex - 1];
      };

      const firstItem = (thisSet: any) => {
        if (!thisSet) {
          return undefined;
        }
        return thisSet[0];
      };

      const lastItem = (thisSet: any) => {
        if (!thisSet) {
          return undefined;
        }
        return thisSet[thisSet.length - 1];
      };

      const convertValue = (value: any) =>
        value
          ? value instanceof ValuesSetValue
            ? convertSingleChoice(value)
            : value instanceof Array
            ? convertMultipleChoice(value)
            : value.toString()
          : '';

      const convertSingleChoice = (singleValue: ValuesSetValue) =>
        singleValue instanceof ValuesSetValue
          ? translationService.t(singleValue.getLabel())
          : translationService.t(singleValue);

      const convertMultipleChoice = (multipleValue: Array<ValuesSetValue>) => {
        const value = multipleValue
          ? multipleValue.map((item) =>
              item instanceof ValuesSetValue
                ? translationService.t(item.getLabel())
                : translationService.t('This is not a ValuesSetValue')
            )
          : [];
        return value.sort().join(', ');
      };

      const compareSets = (oldSet: Array<any>, newSet: Array<any>) => {
        if (oldSet.length !== newSet.length) {
          return true;
        }
        const sortedArray1 = [...oldSet].sort((a, b) => a.guid.localeCompare(b.guid));
        const sortedArray2 = [...newSet].sort((a, b) => a.guid.localeCompare(b.guid));
        for (let i = 0; i < sortedArray1.length; i++) {
          const item1 = sortedArray1[i];
          const item2 = sortedArray2[i];
          if (item1.guid !== item2.guid) {
            return true;
          }
          const keys1 = Object.keys(item1);
          const keys2 = Object.keys(item2);
          if (keys1.length !== keys2.length) {
            return true;
          }
          for (const key of keys1) {
            if (item1[key].toString() !== item2[key].toString()) {
              return true;
            }
          }
        }
        return false;
      };

      const compareSetsItems = (oldSet: Array<any>, newSet: Array<any>) => {
        const results: Array<any> = [];
        const oldSetMap = oldSet.reduce((map, item) => {
          map[item.guid] = item;
          return map;
        }, {});
        const newSetMap = newSet.reduce((map, item) => {
          map[item.guid] = item;
          return map;
        }, {});
        newSet.forEach((newItem) => {
          const setItemGuid = newItem.guid;
          const oldItem = oldSetMap[setItemGuid];
          if (!oldItem) {
            results.push({
              message: `<span class='added'>${translationService.t('Item with guid')}: <strong>${setItemGuid}</strong> 
                ${translationService.t('has been added')}</span>`,
              setItemGuid,
              oldItem: '',
              newItem,
            });
          } else if (JSON.stringify(oldItem) !== JSON.stringify(newItem)) {
            results.push({
              message: `<span class='modified'>${translationService.t(
                'Item with guid'
              )}: <strong>${setItemGuid}</strong>
                 ${translationService.t('has been modified')}</span>`,
              setItemGuid,
              oldItem,
              newItem,
            });
          } else {
            results.push({
              message: `<span class='unchanged'>${translationService.t(
                'Item with guid'
              )}: <strong>${setItemGuid}</strong> 
                ${translationService.t('has not been modified')}</span>`,
              setItemGuid,
              oldItem: '',
              newItem: '',
            });
          }
        });
        oldSet.forEach((oldItem) => {
          const setItemGuid = oldItem.guid;
          if (!newSetMap[oldItem.guid]) {
            results.push({
              message: `<span class='removed'>${translationService.t('Item with guid')}: <strong>${setItemGuid}</strong>
                ${translationService.t('has been removed')}</span>`,
              setItemGuid,
              oldItem,
              newItem: '',
            });
          }
        });
        return results;
      };

      const mergeModels = (oldModel: any, newModel: any) => {
        const oldModelGuidMap = new Map();
        const newModelGuidMap = new Map();
        if (oldModel) {
          oldModel.forEach((item: any) => {
            oldModelGuidMap.set(item.guid, item);
          });
        }
        if (newModel) {
          newModel.forEach((item: any) => {
            newModelGuidMap.set(item.guid, item);
          });
        }
        const models: any[] = [];
        if (oldModel) {
          oldModel.forEach((item: any) => {
            const guid = item.guid;
            const oldModelValue = oldModelGuidMap.get(guid);
            const newModelValue = newModelGuidMap.get(guid);
            const modelsObj: any = {
              oldModel: oldModelValue || {},
              newModel: newModelValue || {},
            };
            models.push(modelsObj);
          });
        }
        if (newModel) {
          newModel.forEach((item: any) => {
            const guid = item.guid;
            if (!oldModelGuidMap.has(guid)) {
              const modelsObj: any = {
                oldModel: {},
                newModel: item,
              };
              models.push(modelsObj);
            }
          });
        }
        return models;
      };

      return {
        model,
        resetRadioButton,
        caseStatusService,
        currentCaseService,
        currentPageService,
        translationService,
        listOfActivePages,
        coreConfigurationService,
        thisSet,
        userService,
        getContextForID,
        setYear,
        addElementToSet,
        removeElementFromSet,
        onHover,
        getHooksByType,
        checkValidity,
        checkMinValidity,
        checkMaxValidity,
        nextItem,
        previousItem,
        firstItem,
        lastItem,
        convertValue,
        convertSingleChoice,
        convertMultipleChoice,
        compareSets,
        compareSetsItems,
        mergeModels,
        ValuesSetValue,
        TypesOfDisplayHooks,
        container: getContainer(context, containerType), // this is necessary for the external functions of the xml
        containerType,
        context,
        index: undefined, // Prevents Vue warns, remove this if every CRF become a DataClassItemComponent
        thisObject: undefined, // Prevents Vue warns, remove this if every CRF become a DataClassItemComponent
      };
    },
    components: {
      setcomponent: SetComponent,
      setitemcomponent: SetItemComponent,
      dataclassitemcomponent: DataclassItemComponent,
      difflistcomponent: DiffListComponent,
      ...coreConfiguration.pluggableDataTypes,
    },
    directives: {
      'computed-items': new ComputedItemsDirective(),
      'reset-when-hidden': new ResetWhenHidden(),
      'custom-model': new CustomModelDirective(),
      'custom-model-for-boolean': new CustomModelDirectiveForBoolean(),
      'custom-model-for-checkboxes': new CustomModelDirectiveForCheckboxes(),
      'custom-model-for-errors': new CustomModelDirectiveForErrors(),
      'custom-model-for-requirements': new CustomModelDirectiveForRequirements(),
      'custom-model-for-crf-not-active': new CustomModelDirectiveForCrfNotActive(),
      'custom-model-for-select': new CustomModelDirectiveForSelect(),
      'custom-model-for-sets': new CustomModelDirectiveForSets(),
      'custom-model-for-singlechoice': new CustomModelDirectiveForSinglechoice(),
      'custom-model-for-warnings': new CustomModelDirectiveForWarnings(),
      'short-circuit': new ShortCircuitDirective(),
    },
    mounted() {
      if (this.caseStatusService) {
        this.caseStatusService.finishedMounting();
      }
    },
  });
}

import { PropType } from 'vue';
import { ContainerTypes } from '@/ContainersConfig';

export const useHookProps = () => ({
  fullName: {
    type: String as PropType<string>,
    required: true,
    default: '',
  },
  context: {
    type: String as PropType<string>,
    required: true,
    default: '',
  },
  containerType: {
    type: String as PropType<ContainerTypes>,
    required: true,
    default: '',
  },
});

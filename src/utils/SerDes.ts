import _ from 'lodash';
import { PartialStoredCase } from '@/entities/PartialStoredCase';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { DataCollectionError } from '@/errors/DataCollectionError';

type Class<T> = new (...args: any[]) => T;
type DandelionSerializedType<ST> = { __dandelionType__: string; __value__: ST };

export interface SerDes<InType, OutType> {
  serialize(object: InType): OutType;

  deserialize(serialized: OutType): InType;
}

export abstract class SerDesBrick<T, ST> {
  protected constructor(protected myClass: Class<T>) {}
  protected abstract replacer(object: T): ST;
  protected abstract reviver(object: ST): T;

  tryReplace(object: any): DandelionSerializedType<ST> {
    return this.shouldReplace(object)
      ? {
          __value__: this.replacer(object),
          __dandelionType__: this.myClass.name,
        }
      : object;
  }

  tryRevive(object: any): T {
    return this.shouldRevive(object) ? this.reviver(object.__value__) : object;
  }

  protected shouldReplace(object: any): boolean {
    return object instanceof this.myClass;
  }

  protected shouldRevive(object: any): boolean {
    return (
      object !== undefined &&
      object !== null &&
      Object.prototype.hasOwnProperty.call(object, '__dandelionType__') &&
      object.__dandelionType__ === this.myClass.name
    );
  }
}

type SerializedValuesSetvalue = { value: string; label: string; numericalValue: number };

class SerializerFoValuesSetValue extends SerDesBrick<ValuesSetValue, SerializedValuesSetvalue> {
  constructor() {
    super(ValuesSetValue);
  }

  protected replacer(o: ValuesSetValue) {
    return { label: o.getLabel(), value: o.toString(), numericalValue: o.getNumericalValue() };
  }

  protected reviver(o: any) {
    return new ValuesSetValue(o.value, o.label, o.numericalValue);
  }
}

class SerializerForNumbers extends SerDesBrick<Number, string> {
  constructor() {
    super(Number);
  }

  tryReplace(object: any): DandelionSerializedType<string> {
    return this.shouldReplace(object) ? this.replacer(object) : object;
  }

  tryRevive(object: any): number {
    return this.shouldRevive(object) ? this.reviver(object) : object;
  }

  protected shouldReplace(object: any): boolean {
    return typeof object === 'number' && isNaN(object);
  }

  protected shouldRevive(object: any): boolean {
    return object === '__dandelionNaN__';
  }

  protected replacer(i: number) {
    return '__dandelionNaN__';
  }

  protected reviver(o: string): number {
    return NaN;
  }
}

class SerializerForDates extends SerDesBrick<Date, string> {
  constructor() {
    super(Date);
  }

  protected replacer(i: Date) {
    return i.toISOString();
  }

  protected reviver(o: string): Date {
    return new Date(o);
  }
}

class SerializerForErrors extends SerDesBrick<DataCollectionError, string> {
  constructor() {
    super(DataCollectionError);
  }

  protected replacer(_i: DataCollectionError) {
    return 'ERROR_IS_ACTIVE';
  }

  protected reviver(_o: string): DataCollectionError {
    return new DataCollectionError();
  }
}

type SerializedMap<K, V> = [K, V][];

class SerializerForMap<K, V> extends SerDesBrick<Map<K, V>, SerializedMap<K, V>> {
  constructor() {
    super(Map);
  }

  protected replacer(o: Map<K, V>) {
    return Array.from(o.entries());
  }

  protected reviver(o: any) {
    return new Map<K, V>(o);
  }
}

class SerializerForSymbolKeys extends SerDesBrick<unknown, string> {
  constructor() {
    super(Object);
  }

  tryReplace(object: any) {
    return this.shouldReplace(object) ? this.replacer(object) : object;
  }

  tryRevive(object: any) {
    return this.shouldRevive(object) ? this.reviver(object) : object;
  }

  protected shouldReplace(object: any): boolean {
    return object !== null && object !== undefined && Object.getOwnPropertySymbols(object).length !== 0;
  }

  protected shouldRevive(object: any): boolean {
    return (
      object !== undefined &&
      object !== null &&
      typeof object === 'object' &&
      Object.getOwnPropertyNames(object).some((k) => k.includes('__dandelionHook__'))
    );
  }

  protected replacer(o: any) {
    const newO = _.cloneDeep(o);
    for (const s of Object.getOwnPropertySymbols(o)) {
      newO[`__dandelionHook__${s.description}`] = o[s];
      delete newO[s];
    }
    return newO;
  }

  protected reviver(o: any) {
    const newO = _.cloneDeep(o);
    for (const s of Object.getOwnPropertyNames(o).filter((s) => s.includes('__dandelionHook__'))) {
      newO[Symbol.for(s.replace('__dandelionHook__', ''))] = o[s];
      delete newO[s];
    }
    return newO;
  }
}

export class Stringifier<T> implements SerDes<T, string> {
  protected listOfSerDes: SerDesBrick<any, any>[] = [
    new SerializerForNumbers(),
    new SerializerForDates(),
    new SerializerFoValuesSetValue(),
    new SerializerForMap(),
    new SerializerForErrors(),
    new SerializerForSymbolKeys(),
  ];

  serialize(object: T): string {
    /* we need that for Date, that runs toJSON before the reviver
       this[key] permit to access the object before run into toJSON method*/

    // necessary
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this;
    return JSON.stringify(object, function (key) {
      // necessary
      // eslint-disable-next-line no-invalid-this
      return that.listOfSerDes.reduce((prev, s) => s.tryReplace(prev), this[key]);
    });
  }

  deserialize(serialized: string): T {
    return JSON.parse(serialized, (_key, value) => this.listOfSerDes.reduce((prev, s) => s.tryRevive(prev), value));
  }
}

export class AxiosStringifier<T> implements SerDes<T, any> {
  protected listOfSerDes: SerDesBrick<any, any>[] = [
    new SerializerForDates(),
    new SerializerFoValuesSetValue(),
    new SerializerForMap(),
    new SerializerForErrors(),
  ];

  serialize = (data: any): any => {
    if (Array.isArray(data)) {
      return data.map(this.serialize);
    }

    const newData = this.listOfSerDes.reduce((prev, s) => s.tryReplace(prev), data);

    if (newData !== null && newData !== undefined && [Object, PartialStoredCase].includes(newData.constructor)) {
      return Object.fromEntries(Object.entries(newData).map(([key, value]) => [key, this.serialize(value)]));
    }

    return newData;
  };

  deserialize = (data: any): any => {
    if (Array.isArray(data)) {
      return data.map(this.deserialize);
    }

    const newData = this.listOfSerDes.reduce((prev, s) => s.tryRevive(prev), data);

    if (newData !== null && newData !== undefined && [Object, PartialStoredCase].includes(newData.constructor)) {
      return Object.fromEntries(Object.entries(newData).map(([key, value]) => [key, this.deserialize(value)]));
    }

    return newData;
  };
}

const listOfSerDes: SerDesBrick<any, any>[] = [
  new SerializerForNumbers(),
  new SerializerForDates(),
  new SerializerFoValuesSetValue(),
  new SerializerForMap(),
  new SerializerForErrors(),
  new SerializerForSymbolKeys(),
];

export const AxiosSerializer = (data: any): any => {
  if (Array.isArray(data)) {
    return data.map(AxiosSerializer);
  }

  const newData = listOfSerDes.reduce((prev, s) => s.tryReplace(prev), data);

  if (newData !== null && newData !== undefined && [Object, PartialStoredCase].includes(newData.constructor)) {
    return Object.fromEntries(Object.entries(newData).map(([key, value]) => [key, AxiosSerializer(value)]));
  }
  return newData;
};

export const AxiosReviver = (data: any): any => {
  if (Array.isArray(data)) {
    return data.map(AxiosReviver);
  }

  const newData = listOfSerDes.reduce((prev, s) => s.tryRevive(prev), data);

  if (newData !== null && newData !== undefined && [Object, PartialStoredCase].includes(newData.constructor)) {
    return Object.fromEntries(Object.entries(newData).map(([key, value]) => [key, AxiosReviver(value)]));
  }

  return newData;
};

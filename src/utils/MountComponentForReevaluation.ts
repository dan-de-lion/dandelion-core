import { ContainerTypes, getContainer } from '@/ContainersConfig';
import { NamedBindings } from '../types/NamedBindings';
import { mount } from '@vue/test-utils';
import type { CRF } from '../entities/CRF';
import type { CRFServiceInterface } from '../interfaces/CRFServiceInterface';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import type { CRFToHTMLService } from '@/services/CRFToHTMLService';
import { Contexts } from '@/types/Contexts';
import { createCustomComponentStructure } from '@/utils/customComponentStructure';

export default async function mountComponentForReevaluation(
  CRFService: CRFServiceInterface,
  selectionStrategy: CRFVersionSelectionStrategyInterface,
  caseMetaData: CaseMetaData,
  crfNames: Array<string>,
  path: string,
  showGroupsInTree: boolean,
  showDataclassInstancesInTree: boolean,
  showSetsInTree: boolean,
  maxDepth: number,
  startingDataclass: string,
  context: string,
  model: any = {},
  prefixForID = ''
) {
  let crfs: Array<CRF> = [];
  let html = '';
  try {
    crfs = await Promise.all(
      crfNames.map(async (crfName) => {
        const crfVersion = (await selectionStrategy.getUpdatedCRFVersionsByCase(model, caseMetaData)).get(crfName);
        if (!crfVersion) {
          throw new CRFError('Error catched on getting updated CRF Versions', CRFErrorType.CRF_VERSIONS_NOT_FOUND);
        }
        return CRFService.getCRF(crfName, crfVersion);
      })
    );

    const crfToHTMLService = getContainer(Contexts.SHARED).get(NamedBindings.CRFToHTMLService) as CRFToHTMLService;
    html = (
      await Promise.all(
        crfs.map((crf) =>
          crfToHTMLService.getHTMLfromXML(
            crf.xml,
            crf.name,
            crf.version,
            path,
            showGroupsInTree,
            showDataclassInstancesInTree,
            showSetsInTree,
            maxDepth,
            startingDataclass,
            `model.${crf.name.replace(/\s/g, '')}`,
            prefixForID
          )
        )
      )
    ).join('');
  } catch (e) {
    throw new CRFError(`Failed to load crf for reevaluation: ${e}`, CRFErrorType.FAILED_TO_LOAD);
  }

  let helperFunctions = {};
  const unifiedJS = crfs.map((crf) => crf.js).join('');

  helperFunctions = await import(`data:text/javascript,${unifiedJS}`);

  const customComponent = createCustomComponentStructure(
    html,
    model,
    helperFunctions,
    ContainerTypes.REEVALUATION,
    context
  );

  return mount(customComponent);
}

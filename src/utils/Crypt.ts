import sodium from 'libsodium-wrappers';
import { Stringifier } from '@/utils/SerDes';

export class Crypt {
  static generateKey(): string {
    const key = sodium.crypto_secretbox_keygen();
    return sodium.to_hex(key);
  }

  static async encrypt(clearObject: any, myKey: string): Promise<string> {
    await sodium.ready;
    const key = sodium.from_hex(myKey);
    const nonce = sodium.randombytes_buf(sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES);
    const encrypted = sodium.crypto_aead_xchacha20poly1305_ietf_encrypt(
      new Stringifier().serialize(clearObject),
      null,
      null,
      nonce,
      key
    );
    // necessary
    // @ts-ignore
    const nonce_and_ciphertext = Crypt.concatTypedArray(Uint8Array, nonce, encrypted); //https://github.com/jedisct1/libsodium.js/issues/130#issuecomment-361399594
    return Crypt.u_btoa(nonce_and_ciphertext);
  }

  static async decrypt(encObject: string, myKey: string | undefined): Promise<any> {
    await sodium.ready;
    const key = sodium.from_hex(myKey!);
    const nonce_and_ciphertext = Crypt.u_atob(encObject); //converts ascii string of garbled text into binary
    const nonce = nonce_and_ciphertext.slice(0, sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES); //https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray/slice
    const ciphertext = nonce_and_ciphertext.slice(sodium.crypto_aead_xchacha20poly1305_ietf_NPUBBYTES);
    const result = sodium.crypto_aead_xchacha20poly1305_ietf_decrypt(nonce, ciphertext, null, nonce, key, 'text');
    return new Stringifier().deserialize(result);
  }

  protected static concatTypedArray(ResultConstructor: any) {
    let totalLength = 0;

    const _len = arguments.length,
      arrays = Array(_len > 1 ? _len - 1 : 0);
    for (let _key = 1; _key < _len; _key++) {
      arrays[_key - 1] = arguments[_key];
    }

    let _iteratorNormalCompletion: boolean | undefined = true;
    let _didIteratorError = false;
    let _iteratorError = null;

    try {
      for (
        let _iterator = arrays[Symbol.iterator](), _step;
        !(_iteratorNormalCompletion = (_step = _iterator.next()).done);
        _iteratorNormalCompletion = true
      ) {
        const arr = _step.value;

        totalLength += arr.length;
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        // necessary
        // @ts-ignore
        if (!_iteratorNormalCompletion && _iterator.return) {
          // necessary
          // @ts-ignore
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    const result = new ResultConstructor(totalLength);
    let offset = 0;
    let _iteratorNormalCompletion2: boolean | undefined = true;
    let _didIteratorError2 = false;
    let _iteratorError2 = null;

    try {
      // necessary
      // @ts-ignore
      for (
        let _iterator2 = arrays[Symbol.iterator](), _step2;
        !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done);
        _iteratorNormalCompletion2 = true
      ) {
        const _arr = _step2.value;

        result.set(_arr, offset);
        offset += _arr.length;
      }
    } catch (err) {
      _didIteratorError2 = true;
      _iteratorError2 = err;
    } finally {
      try {
        // necessary
        // @ts-ignore
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
          // necessary
          // @ts-ignore
          _iterator2.return();
        }
      } finally {
        if (_didIteratorError2) {
          throw _iteratorError2;
        }
      }
    }

    return result;
  }

  protected static u_btoa(buffer: any) {
    //https://stackoverflow.com/a/43271130/
    const binary = [];
    const bytes = new Uint8Array(buffer);
    for (let i = 0; i < bytes.byteLength; i++) {
      binary.push(String.fromCharCode(bytes[i]));
    }
    return btoa(binary.join(''));
  }

  protected static u_atob(ascii: string) {
    //https://stackoverflow.com/a/43271130/
    return Uint8Array.from(atob(ascii), (c) => c.charCodeAt(0));
  }
}

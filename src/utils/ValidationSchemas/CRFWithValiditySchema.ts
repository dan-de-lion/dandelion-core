import { z } from 'zod';

const ZcrfValiditySchema = z.object({
  startDate: z
    .string()
    .regex(/^\d{4}-\d{2}-\d{2}$/, { message: 'startDate must be in YYYY-MM-DD format' })
    .pipe(z.coerce.date()),
  endDate: z
    .string()
    .regex(/^\d{4}-\d{2}-\d{2}$/, { message: 'endDate must be in YYYY-MM-DD format' })
    .pipe(z.coerce.date())
    .optional(),
});

const ZcrfWithValiditySchema = z.object({
  crfName: z.string(),
  crfValidityArray: z.array(ZcrfValiditySchema),
  isAlwaysActive: z.boolean().optional(),
});

const zCentreCode = z.string({ message: 'Centre code must be a string' });
export const ZcrfWithValidityForCentreCodeSchema = z.record(zCentreCode, z.array(ZcrfWithValiditySchema));
export type ZcrfWithValidityForCentreCode = z.infer<typeof ZcrfWithValidityForCentreCodeSchema>;

import { z } from 'zod';
import { v4 } from 'uuid';

const ZsourceEventSchema = z.object({
  id: z
    .string()
    .uuid()
    .default(() => v4()),
  handler: z.string(),
  caseID: z.string(),
  username: z.string(),
  referenceDate: z
    .string()
    .optional()
    .pipe(z.coerce.date())
    .refine((val) => !Number.isNaN(val.getTime()), { message: 'referenceDate must be a valid Date' })
    .optional(),
  motivations: z.string().optional(),
  timestamp: z
    .string()
    .pipe(z.coerce.date())
    .refine((val) => !Number.isNaN(val.getTime()), { message: 'timestamp must be a valid Date' }),
});

export const ZsourceEventsSchema = z.array(ZsourceEventSchema);

export type ZsourceEvents = z.infer<typeof ZsourceEventsSchema>;

import { z } from 'zod';
import { Version } from '@/entities/CRF';

export const ZversionSchema = z.object({
  major: z
    .string()
    .regex(/^\d+$/, { message: 'major must be a string representing a number' })
    .transform((val) => Number.parseInt(val, 10)),
  startDate: z
    .string()
    .regex(/^\d{4}-\d{2}-\d{2}$/, { message: 'startDate must be in YYYY-MM-DD format' })
    .pipe(z.coerce.date()),
  lastAvailable: z
    .string()
    .regex(/^\d+\.\d+\.\d+$/, { message: 'lastAvailable does not respect the Version pattern' })
    .refine((val): val is Version => /^\d+\.\d+\.\d+$/.test(val), {
      message: 'lastAvailable must be a valid CRF version',
    }),
});

export const ZversionsSchema = z.object({
  versions: z.array(ZversionSchema),
});

export type Zversions = z.infer<typeof ZversionsSchema>;

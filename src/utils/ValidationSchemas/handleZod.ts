import { getContainer } from '@/ContainersConfig';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import TranslationService from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { ZodArray, ZodError, ZodObject, ZodRecord, ZodTypeAny } from 'zod';

export enum ZodSchemas {
  CRF_WITH_VALIDITY_CENTRE_CODE = 'Crf with validity for centre code',
  CRF_VERSION = 'Crf version',
  SNAPSHOT = 'Snapshot',
  SOURCE_EVENT = 'Source Event',
}

export function parseElementWithZod(
  schema: ZodObject<any> | ZodArray<any> | ZodRecord<ZodTypeAny>,
  schemaName: ZodSchemas,
  elementToBeParsed: any
): any {
  try {
    return schema.parse(elementToBeParsed);
  } catch (e) {
    handleZodError(e, schemaName);
  }
}

function handleZodError(e: unknown, schemaName: ZodSchemas): never {
  const ts = getContainer(Contexts.SHARED).get(NamedBindings.TranslationService) as TranslationService;
  if (e instanceof ZodError) {
    const errorMessages = e.issues
      .map((issue) =>
        ts.t('zodError', { schemaName, path: (issue.path[1] ?? issue.path[0]).toString(), message: issue.message })
      )
      .join('; ');
    throw new ApplicationError(errorMessages, AppErrorType.ZOD_ERROR);
  } else {
    throw e;
  }
}

import { z } from 'zod';

const ZcachedCaseListElementSchema = z.object({
  caseID: z.string(),
  lastUpdate: z
    .string()
    .pipe(z.coerce.date())
    .refine((val) => !Number.isNaN(val.getTime()), { message: 'lastUpdate must be a valid Date' }),
  referenceDate: z
    .string()
    .pipe(z.coerce.date())
    .refine((val) => !Number.isNaN(val.getTime()), { message: 'referenceDate must be a valid Date' }),
});

export const ZcaseListSnapshotSchema = z.object({
  cachedCaseList: z.array(ZcachedCaseListElementSchema),
  date: z
    .string()
    .pipe(z.coerce.date())
    .refine((val) => !Number.isNaN(val.getTime()), { message: 'date must be a valid Date' }),
  numberOfEvents: z.number(),
});

export const ZcaseListSnapshotsSchema = z.array(ZcaseListSnapshotSchema);

export type ZcaseListSnapshot = z.infer<typeof ZcaseListSnapshotSchema>;
export type ZcaseListSnapshots = z.infer<typeof ZcaseListSnapshotsSchema>;

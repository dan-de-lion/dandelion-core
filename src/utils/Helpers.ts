import _ from 'lodash';
import '../../public/frameless-xslt2.js';
import { getContainer } from '@/ContainersConfig';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { Contexts } from '@/types/Contexts';
import type { TypesOfActionHooks } from '@/types/TypesOfHooks';
import { NamedBindings } from '@/types/NamedBindings';
import type CurrentContextService from '@/services/CurrentContextService';

export default class Helpers {
  public static setObjByPath(path: string, obj: any, value: any) {
    _.set(obj, path, value);
  }

  public static unsetObjByPath(path: string, obj: any) {
    _.unset(obj, path);
  }

  public static getObjByPath(path: string, obj: any) {
    return _.get(obj, path);
  }

  public static addCss(css: string) {
    const style = document.createElement('style');
    style.innerText = css;
    document.head.appendChild(style);
  }

  public static getContextForID(context: string, coreConfiguration: CoreConfigurationInterface) {
    // Contexts.DEFAULT is the shorthand for the real default context, which depends on the caseTypes that are used in the application
    // In the case of default context we don't add a prefix to IDs
    if (context === Contexts.DEFAULT || context === coreConfiguration.listOfCaseTypes[0].caseName) {
      return '';
    }
    return `${context}_`;
  }

  public static async runHook(context: string, actionHookType: TypesOfActionHooks) {
    const coreConfiguration = getContainer(context).get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const currentContextService = getContainer(context).get(
      NamedBindings.CurrentContextService
    ) as CurrentContextService;

    if (coreConfiguration.actionHooks?.has(actionHookType)) {
      const canSave = await coreConfiguration.actionHooks.get(actionHookType)!.run(currentContextService.context);
      if (!canSave) {
        throw new ApplicationError(
          `ActionHook '${actionHookType}' run method returned false`,
          AppErrorType.ACTION_HOOK_ERROR
        );
      }
    }
  }

  /**
   * @__PURE__
   * This function searches for the most specific variable prefix in the accessLevels tree
   * @param fullName
   * @param accessLevels
   */
  public static searchMostSpecificVariablePrefix(fullName: string, accessLevels: any) {
    let chosenAccessLevel = 'public';
    const cleanedVariable = fullName.replace(/\[[^\]]+\]/g, '');
    const splittedCleanedVariable = cleanedVariable.split('.');

    function dfs(currentAccessLevelTree: any, level = 0) {
      const children = Object.getOwnPropertyNames(currentAccessLevelTree);

      // if there is a child "accessLevel"
      if (children.includes('accessLevel')) {
        chosenAccessLevel = currentAccessLevelTree.accessLevel;
      }
      const currentChild = splittedCleanedVariable[level];

      // if there is a child with the variable, next step
      if (children.includes(currentChild)) {
        dfs(currentAccessLevelTree[currentChild], level + 1);
      }
    }

    dfs(accessLevels);
    return chosenAccessLevel;
  }
}

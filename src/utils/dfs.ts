// implementation of depth first search
export async function dfs(
  currentNode: any,
  // callback function to apply on each node that is not an object (it is a string)
  apply: (currentNode: any, path: string, child: any) => void,
  path = ''
): Promise<void> {
  const children = Object.getOwnPropertyNames(currentNode);

  // first object children, then string children, in order to get the most specific variable prefix (depth first search)
  children.sort((a, b) => {
    if (typeof currentNode[a] === 'object' && typeof currentNode[b] === 'string') {
      return -1;
    } else if (typeof currentNode[a] === 'string' && typeof currentNode[b] === 'object') {
      return 1;
    }
    return 0;
  });

  for (const child of children) {
    if (typeof currentNode[child] === 'object') {
      // if the child is an object, call dfs on it
      await dfs(currentNode[child], apply, path ? `${path}.${child}` : child);
    } else {
      // if the child is a string, apply the callback function because it is a leaf (most specific variable prefix)
      apply(currentNode, path, currentNode[child]);
    }
  }
}

import { Container, interfaces } from 'inversify';
import PubSub from 'pubsub-js';
import 'reflect-metadata';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import type { CaseConfiguration, CoreConfiguration, CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import CaseVariablesCacheService from '@/services/CaseVariablesCacheService';
import { CRFToHTMLService } from '@/services/CRFToHTMLService';
import CurrentContextService from '@/services/CurrentContextService';
import TranslationService from '@/services/TranslationService';
import type { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { MapForMotivations } from '@/utils/MapForMotivations';

export enum ContainerTypes {
  DEFAULT = 'default',
  NEW_CASE = 'newCase',
  REEVALUATION = 'reevaluation',
}

export class Context {
  constructor(
    readonly name: string,
    readonly container: Container,
    readonly newCaseContainer: Container,
    readonly reevaluateContainer: Container
  ) {}

  public dispatchEvent(type: ApplicationEvents, event: Event) {
    /* if we are in shared context, we want to dispatch event without the context */
    if (this.name === Contexts.SHARED) {
      PubSub.publish(`${type}`, event);
    } /* we are in a normal context, we publish the event only in the context-based channel */ else {
      PubSub.publish(`${type}.${this.name}`, event);
    }
  }

  public addEventListener(type: ApplicationEvents, fn: (message: any, event: Event) => any): string {
    if (this.name === Contexts.SHARED) {
      return PubSub.subscribe(`${type}`, fn);
    }
    return PubSub.subscribe(`${type}.${this.name}`, fn);
  }

  public removeAllEventListeners(type: string) {
    if (this.name === Contexts.SHARED) {
      PubSub.unsubscribe(`${type}`);
    } else {
      PubSub.unsubscribe(`${type}.${this.name}`);
    }
  }

  public removeEventListenerByToken(token: string) {
    PubSub.unsubscribe(token);
  }
}

export declare type SharedServiceInfo<T> = {
  name: string;
  class: new (...args: any[]) => T;
};

export declare type SharedDynamicServiceInfo<T> = {
  name: string;
  instance: interfaces.DynamicValue<T>;
};

export declare type ServiceInfo<T> = SharedServiceInfo<T> & {
  containerTypes: ContainerTypes[];
};

export declare type DynamicServiceInfo<T> = SharedDynamicServiceInfo<T> & {
  containerTypes: ContainerTypes[];
};

export function isServiceInfo(
  serviceInfo: ServiceInfo<any> | DynamicServiceInfo<any>
): serviceInfo is ServiceInfo<any> {
  return (serviceInfo as ServiceInfo<any>).class !== undefined;
}

export function isSharedServiceInfo(
  serviceInfo: SharedServiceInfo<any> | SharedDynamicServiceInfo<any>
): serviceInfo is SharedServiceInfo<any> {
  return (serviceInfo as SharedServiceInfo<any>).class !== undefined;
}

export function isDynamicServiceInfo(
  serviceInfo: ServiceInfo<any> | DynamicServiceInfo<any>
): serviceInfo is DynamicServiceInfo<any> {
  return (serviceInfo as DynamicServiceInfo<any>).instance !== undefined;
}

export function isSharedDynamicServiceInfo(
  serviceInfo: ServiceInfo<any> | SharedDynamicServiceInfo<any>
): serviceInfo is SharedDynamicServiceInfo<any> {
  return (serviceInfo as SharedDynamicServiceInfo<any>).instance !== undefined;
}

const contexts = new Map<string, Context>();
const services: Array<ServiceInfo<any> | DynamicServiceInfo<any>> = [];

export function initializeShared() {
  const container = new Container();

  container.bind(NamedBindings.TranslationService).to(TranslationService).inSingletonScope();
  container.bind(NamedBindings.MapForMotivations).to(MapForMotivations).inSingletonScope();
  container.bind(NamedBindings.CRFToHTMLService).to(CRFToHTMLService).inSingletonScope();
  container.bind(NamedBindings.CaseVariablesCacheService).to(CaseVariablesCacheService).inSingletonScope();
  container
    .bind(NamedBindings.CurrentContextService)
    .toDynamicValue(() => new CurrentContextService(Contexts.SHARED))
    .inSingletonScope();
  contexts.set(Contexts.SHARED, new Context(Contexts.SHARED, container, new Container(), new Container()));
}

/**
 * function used for registering stateful services that are bound once we make and addContext('contextName')
 * @param servicesToRegister stateful services to bind
 */
function registerStatefulServices(servicesToRegister: Array<ServiceInfo<any> | DynamicServiceInfo<any>>) {
  // empty the services array
  services.splice(0, services.length);
  services.push(...servicesToRegister);
}

function getRealDefaultContext(): string {
  const sharedContext = contexts.get(Contexts.SHARED);
  if (!sharedContext) {
    throw new CoreError(`Shared context not created`, CoreErrorType.CREATE_CONTEXT_FAILED);
  }
  return (sharedContext.container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface).listOfCaseTypes[0]
    .caseName;
}

function getContainer(context = Contexts.DEFAULT, containerType: ContainerTypes = ContainerTypes.DEFAULT): Container {
  let contextName = context;

  // if the context requested is the default one, check for the default caseName associated context
  // Contexts.DEFAULT is the shorthand for the real default contexts, which depends on the caseTypes that are used in the application
  if (contextName === Contexts.DEFAULT) {
    contextName = getRealDefaultContext();
  }
  const _context = contexts.get(contextName);
  if (!_context) {
    throw new CoreError(`No context with this name: ${contextName}`, CoreErrorType.CONTEXT_NOT_FOUND);
  }

  if (containerType === 'default') {
    return _context.container;
  } else if (containerType === 'newCase') {
    return _context.newCaseContainer;
  } else if (containerType === 'reevaluation') {
    return _context.reevaluateContainer;
  }
  throw new CoreError(
    `No container with this name: ${context} and caseType: ${containerType}`,
    CoreErrorType.CONTAINER_NOT_FOUND
  );
}

/**
 * binds the stateful services on a just added context
 * if stateful services are already bounded in the shared context, removes and rebind them
 * @param contextName where we want to bind the stateful services
 */
function bindStatefulServices(contextName: string) {
  const context: Context = getContext(contextName);
  const container = context.container;
  const newCaseContainer = context.newCaseContainer;
  const reevaluateContainer = context.reevaluateContainer;

  services.forEach((service: ServiceInfo<any> | DynamicServiceInfo<any>) => {
    service.containerTypes.forEach((containerType: ContainerTypes) => {
      if (containerType === ContainerTypes.DEFAULT) {
        if (container.isCurrentBound(service.name)) {
          container.unbind(service.name);
        }
        // if service.class is defined, bind it to the container
        if (isDynamicServiceInfo(service)) {
          container.bind(service.name).toDynamicValue(service.instance).inSingletonScope();
        } else if (isServiceInfo(service)) {
          container.bind(service.name).to(service.class).inSingletonScope();
        }
      } else if (containerType === ContainerTypes.NEW_CASE) {
        if (isDynamicServiceInfo(service)) {
          newCaseContainer.bind(service.name).toDynamicValue(service.instance).inSingletonScope();
        } else if (isServiceInfo(service)) {
          newCaseContainer.bind(service.name).to(service.class).inSingletonScope();
        }
      } else if (containerType === ContainerTypes.REEVALUATION) {
        if (reevaluateContainer.isBound(service.name)) {
          reevaluateContainer.unbind(service.name);
        }
        if (isDynamicServiceInfo(service)) {
          reevaluateContainer.bind(service.name).toDynamicValue(service.instance).inSingletonScope();
        } else if (isServiceInfo(service)) {
          reevaluateContainer.bind(service.name).to(service.class).inSingletonScope();
        }
      }
    });
  });
}

export function getOwnershipForAllContainers() {
  contexts.forEach((context) => {
    getContainerOwnership(context.name);
  });
}

/**
 * Get the ownership of a context
 * In this way none of the child component will have the ownership of the context, and will not "flush" the ref/reactive variables
 * The problem is that when a component that have firstly got a service, will deactivate ref/reactive variables
 * @param contextName
 */
export function getContainerOwnership(contextName: string) {
  const container = getContainer(contextName);
  const newCaseContainer = getContainer(contextName, ContainerTypes.NEW_CASE);
  const reevaluationContainer = getContainer(contextName, ContainerTypes.REEVALUATION);

  // @ts-expect-error
  newCaseContainer._bindingDictionary.getMap().forEach((_value, key) => {
    newCaseContainer.get(key);
  });

  // @ts-expect-error
  reevaluationContainer._bindingDictionary.getMap().forEach((_value, key) => {
    reevaluationContainer.get(key);
  });

  // @ts-expect-error
  container._bindingDictionary.getMap().forEach((_value, key) => {
    container.get(key);
  });
}

/**
 * entry point for adding a context
 * the function will copy all the services from the shared container,
 * and it'll bind the currentContextService (utils)
 * @param name of the context to be added
 * @param caseType of the context, if not provided select the first caseType from configuration
 */
function addContext(name: string, caseType?: CaseConfiguration) {
  const sharedContainer = contexts.get(Contexts.SHARED)!.container;
  let myCaseType = caseType;
  if (!myCaseType) {
    const sharedConfig = sharedContainer.get(NamedBindings.CoreConfiguration) as CoreConfiguration;
    myCaseType = sharedConfig.listOfCaseTypes[0];
  }
  const container = new Container();
  container.parent = sharedContainer;
  const reevaluationContainer = new Container();
  reevaluationContainer.parent = sharedContainer;

  container
    .bind(NamedBindings.CurrentContextService)
    .toDynamicValue(() => new CurrentContextService(name, myCaseType))
    .inSingletonScope();

  reevaluationContainer
    .bind(NamedBindings.CurrentContextService)
    .toDynamicValue(() => new CurrentContextService(name, myCaseType, ContainerTypes.REEVALUATION))
    .inSingletonScope();

  if (!(container instanceof Container)) {
    throw new CoreError('Error in creating default container', CoreErrorType.CREATE_CONTAINER_FAILED);
  }

  if (!(reevaluationContainer instanceof Container)) {
    throw new CoreError('Error in creating reevaluation container', CoreErrorType.CREATE_CONTAINER_FAILED);
  }

  const newCaseContainer = new Container();
  newCaseContainer.parent = container;
  newCaseContainer
    .bind(NamedBindings.CurrentContextService)
    .toDynamicValue(() => new CurrentContextService(name, myCaseType, ContainerTypes.NEW_CASE))
    .inSingletonScope();
  const contextCreated = new Context(name, container, newCaseContainer, reevaluationContainer);
  contexts.set(name, contextCreated);

  bindStatefulServices(name);
  return contextCreated;
}

function removeContext(name: string) {
  contexts.delete(name);
}

/**
 * retrieves the context from all the contexts
 * @param context to retrieve
 */
function getContext(context: string): Context {
  let contextName = context;
  // if the context requested is the default one, check for the default caseName associated context
  // Contexts.DEFAULT is the shorthand for the real default contexts, which depends on the caseTypes that are used in the application
  if (contextName === Contexts.DEFAULT) {
    contextName = getRealDefaultContext();
  }
  const _context = contexts.get(contextName);
  if (!_context) {
    throw new CoreError(`No context with this name: ${contextName}`, CoreErrorType.CONTEXT_NOT_FOUND);
  }
  return _context;
}

export { getContainer, addContext, registerStatefulServices, getContext, bindStatefulServices, removeContext };

import { SourceEventForList } from './SourceEventForList';

export class UpdateCaseEvent extends SourceEventForList {
  constructor(readonly caseID: string, username: string, timestamp: Date) {
    super('applyUpdateCase', caseID, username, timestamp);
  }
}

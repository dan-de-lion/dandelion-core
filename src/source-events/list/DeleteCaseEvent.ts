import { SourceEventForList } from './SourceEventForList';

export class DeleteCaseEvent extends SourceEventForList {
  constructor(readonly caseID: string, username: string, timestamp: Date) {
    super('applyDeleteCase', caseID, username, timestamp);
  }
}

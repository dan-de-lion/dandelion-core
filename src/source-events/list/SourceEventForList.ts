import { v4 } from 'uuid';

export class SourceEventForList {
  readonly id: string;
  readonly handler: string;

  constructor(handler: string, readonly caseID: string, readonly username: string, readonly timestamp: Date) {
    this.id = v4();
    this.handler = handler;
  }
}

import type { Motivations } from '@/utils/MapForMotivations';
import { SourceEventForList } from './SourceEventForList';

export class PermanentlyDeleteCaseEvent extends SourceEventForList {
  constructor(
    readonly caseID: string,
    protected _motivations: Motivations | string /* motivations are here because are saved in storage */,
    username: string,
    timestamp: Date
  ) {
    super('applyPermanentlyDeleteCase', caseID, username, timestamp);
  }
}

import { SourceEventForList } from '@/source-events/list/SourceEventForList';

export class ChangedReferenceDateEvent extends SourceEventForList {
  constructor(readonly caseID: string, readonly referenceDate: Date, username: string, timestamp: Date) {
    super('applyChangedReferenceDate', caseID, username, timestamp);
  }
}

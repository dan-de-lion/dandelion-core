import { SourceEventForList } from './SourceEventForList';

export class RestoreCase extends SourceEventForList {
  constructor(readonly caseID: string, username: string, timestamp: Date) {
    super('applyRestoreCase', caseID, username, timestamp);
  }
}

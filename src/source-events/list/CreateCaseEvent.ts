import { SourceEventForList } from './SourceEventForList';

export class CreateCaseEvent extends SourceEventForList {
  constructor(readonly caseID: string, username: string, timestamp: Date) {
    super('applyCreateCase', caseID, username, timestamp);
  }
}

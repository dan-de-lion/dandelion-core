import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseCreated extends Event {
  constructor(
    readonly model: any,
    readonly caseID: string,
    readonly lastUpdate: Date,
    readonly containerType: ContainerTypes
  ) {
    super(ApplicationEvents.CaseCreated);
  }
}

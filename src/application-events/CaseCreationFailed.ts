import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseCreationFailed extends Event {
  constructor(readonly caseID: string, readonly error: unknown, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CaseCreationFailed);
  }
}

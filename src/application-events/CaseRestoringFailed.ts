import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseRestoringFailed extends Event {
  constructor(readonly caseID: string, readonly error: unknown, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CaseRestoringFailed);
  }
}

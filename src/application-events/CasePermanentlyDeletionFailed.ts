import type { Motivations } from '@/utils/MapForMotivations';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CasePermanentlyDeletionFailed extends Event {
  constructor(
    readonly caseID: string,
    readonly motivation: Motivations | string,
    readonly error: unknown,
    readonly containerType: ContainerTypes
  ) {
    super(ApplicationEvents.CasePermanentlyDeletionFailed);
  }
}

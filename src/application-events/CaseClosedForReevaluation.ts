import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseClosedForReevaluation extends Event {
  constructor(readonly caseID: string, readonly containerType: ContainerTypes = ContainerTypes.REEVALUATION) {
    super(ApplicationEvents.CaseClosed);
  }
}

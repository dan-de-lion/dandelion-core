import { ContainerTypes } from '@/ContainersConfig';
import { CaseForUpdate } from '@/entities/Cases';
import { ApplicationEvents } from '@/types/ApplicationEvents';

export class CaseUpdated extends Event {
  constructor(readonly caseForUpdate: CaseForUpdate, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CaseUpdated);
  }
}

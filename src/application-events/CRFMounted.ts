import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CRFMounted extends Event {
  constructor(readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CRFMounted);
  }
}

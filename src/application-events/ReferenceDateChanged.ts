import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class ReferenceDateChanged extends Event {
  constructor(
    readonly caseID: string,
    readonly referenceDate: Date,
    readonly lastUpdate: Date,
    readonly containerType: ContainerTypes
  ) {
    super(ApplicationEvents.ReferenceDateChanged);
  }
}

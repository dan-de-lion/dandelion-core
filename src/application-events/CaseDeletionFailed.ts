import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseDeletionFailed extends Event {
  constructor(readonly caseID: string, readonly error: unknown, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CaseDeletionFailed);
  }
}

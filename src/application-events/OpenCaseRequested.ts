import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class OpenCaseRequested extends Event {
  constructor(readonly containerType: ContainerTypes, readonly caseID: string) {
    super(ApplicationEvents.OpenCaseRequested);
  }
}

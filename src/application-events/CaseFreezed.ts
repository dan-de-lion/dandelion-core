import { CaseUpdated } from './CaseUpdated';
import { ContainerTypes } from '@/ContainersConfig';
import { CaseForUpdate } from '@/entities/Cases';

export class CaseFreezed extends CaseUpdated {
  constructor(readonly caseForUpdate: CaseForUpdate, readonly containerType: ContainerTypes) {
    super(caseForUpdate, containerType);
  }
}

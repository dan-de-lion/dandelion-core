import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class NewCaseRequested extends Event {
  constructor(readonly containerType: ContainerTypes) {
    super(ApplicationEvents.NewCaseRequested);
  }
}

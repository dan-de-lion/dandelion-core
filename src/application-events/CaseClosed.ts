import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseClosed extends Event {
  constructor(readonly caseID: string, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CaseClosed);
  }
}

import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseListUpdated extends Event {
  constructor(readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CaseListUpdated);
  }
}

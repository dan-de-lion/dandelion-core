import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseClosedForCreation extends Event {
  constructor(readonly caseID: string, readonly containerType: ContainerTypes = ContainerTypes.NEW_CASE) {
    super(ApplicationEvents.CaseClosed);
  }
}

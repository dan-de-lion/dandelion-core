import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class VariableHovered extends Event {
  constructor(readonly variableName: string, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.VariableHovered);
  }
}

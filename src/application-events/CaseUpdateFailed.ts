import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CaseUpdateFailed extends Event {
  constructor(
    readonly model: any,
    readonly caseID: string,
    readonly error: unknown,
    readonly containerType: ContainerTypes
  ) {
    super(ApplicationEvents.CaseUpdateFailed);
  }
}

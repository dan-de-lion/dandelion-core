import type { Motivations } from '@/utils/MapForMotivations';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { ContainerTypes } from '@/ContainersConfig';

export class CasePermanentlyDeleted extends Event {
  constructor(readonly caseID: string, readonly motivation: Motivations, readonly containerType: ContainerTypes) {
    super(ApplicationEvents.CasePermanentlyDeleted);
  }
}

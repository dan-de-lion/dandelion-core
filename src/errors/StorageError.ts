import { DandelionError } from './DandelionError';

export enum StorageErrorType {
  CASE_METADATA_NOT_FOUND = 'Problem with getting caseMetaData',
  PARTIALS_NOT_FOUND = 'Problem with getting data',
  SNAPSHOTS_NOT_FOUND = 'Problem with getting snapshots',
  CASE_ID_NOT_FOUND = 'Case id not found',
  CREATE_CASE_FAILED = 'Problem with creating case',
  UPDATE_CASE_FAILED = 'Problem with updating case',
  DELETE_CASE_FAILED = 'Problem with deleting case',
  RESTORE_CASE_FAILED = 'Problem with restoring case',
  PERMANENTLY_DELETE_CASE_FAILED = 'Problem with permanently deleting case',
  METHOD_NOT_IMPLEMENTED = 'Method not implemented:',
  PARTIALS_NOT_DESERIALIZED = 'Problem with partials deserialization',
  FREEZE_FAILED = 'Problem with freezing case',
  UNFREEZE_FAILED = 'Problem with unfreezing case',
  LOCK_FAILED = 'Problem with locking case',
  UNLOCK_FAILED = 'Problem with unlocking case',
  IS_LOCKED = 'Problem with getting partials, case is locked',
  GETTING_KEY_FAILED = 'Problem with getting decryption key',
  SAVING_KEY_FAILED = 'Problem with saving key',
  AXIOS_REQUEST_FAILED = 'Problem with axios request',
  BAD_RESPONSE = 'The call returned an incorrect status',
  BAD_CONFIGURATION = 'The configuration is missing a required parameter',
}

export class StorageError extends DandelionError {
  constructor(message: string, type: StorageErrorType, nestedError?: unknown) {
    super(`${type} => ${message}`, nestedError);
  }
}

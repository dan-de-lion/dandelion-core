import { DandelionError } from './DandelionError';

export enum CaseErrorType {
  CASE_METADATA_NOT_FOUND = 'Problem with getting caseMetaData',
  KEY_NOT_FOUND = 'Problem getting key for decrypting',
  PARTIALS_NOT_FOUND = 'Problem with getting data',
  CREATE_CASE_FAILED = 'Problem with creating case',
  UPDATE_CASE_FAILED = 'Problem with updating case',
  DELETE_CASE_FAILED = 'Problem with deleting case',
  FREEZE_FAILED = 'Problem with freezing case',
  RESTORE_CASE_FAILED = 'Problem with restoring case',
  PERMANENTLY_DELETE_CASE_FAILED = 'Problem with permanently deleting case',
  EXTRACTING_PARTIALS_FAILED = 'Problem with extracting partials',
  SYNCH_ERROR = 'Problem with synchronization of data',
}

export class CaseError extends DandelionError {
  constructor(message: string, type: CaseErrorType, nestedError?: unknown) {
    super(`${type} => ${message}`, nestedError);
  }
}

import { DandelionError } from './DandelionError';

export enum FlowErrorType {
  CURRENT_CASE_NOT_FOUND = 'Problem with getting current case',
}

export class FlowError extends DandelionError {
  constructor(message: string, type: FlowErrorType, nestedError?: unknown) {
    super(`${type} => ${message}`, nestedError);
  }
}

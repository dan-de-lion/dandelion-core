import { DandelionError } from './DandelionError';

export enum CoreErrorType {
  CONTEXT_NOT_FOUND = 'Problem with getting context',
  CONTAINER_NOT_FOUND = 'Problem with getting container',
  XSLT_NOT_FOUND = 'Problem with loading XSLT',
  XSL_TRANSFORMATION = 'Problem when executing XSL transformation',
  CREATE_CONTEXT_FAILED = 'Problem with creating context',
  CREATE_CONTAINER_FAILED = 'Problem with creating container',
  DIRECTIVE = 'Problem with directives',
  CACHE_CREATION_FAILED = 'Problem with creating cache',
}

export class CoreError extends DandelionError {
  constructor(message: string, type: CoreErrorType, nestedError?: unknown) {
    super(`${type} => ${message}`, nestedError);
  }
}

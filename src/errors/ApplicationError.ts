import { DandelionError } from './DandelionError';

export enum AppErrorType {
  CASE_ID_UNDEFINED = 'CaseID is undefined',
  GENERIC_ERROR = 'Generic error',
  CASE_CONFIGURATION_NOT_FOUND = 'Problem with getting caseConfiguration',
  ACCESS_RIGHTS_ERROR = 'Problem with getting access rights for selected user',
  ACTION_HOOK_ERROR = 'Problem with running the hook',
  MISSING_SERVICES = 'There are some unbound services',
  METHOD_NOT_IMPLEMENTED = 'Method not implemented:',
  VARIABLE_NOT_FOUND = 'Problem with getting variable',
  COLUMNS_NOT_CACHED = 'Problem with cached variables',
  CASE_TYPE_NOT_FOUND = 'Problem with getting case type',
  CACHED_CASE_NOT_FOUND = 'Problem with getting cached case',
  ZOD_ERROR = 'Problem with parsing your element',
}

export class ApplicationError extends DandelionError {
  constructor(message: string, type: AppErrorType, nestedError?: unknown) {
    super(`${type} => ${message}`, nestedError);
  }
}

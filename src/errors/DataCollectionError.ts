export class DataCollectionError extends Error {
  constructor() {
    super();
    this.name = 'DataCollectionError';
  }

  toString() {
    return 'ERROR_IS_ACTIVE';
  }
}

import { DandelionError } from './DandelionError';

export enum CRFErrorType {
  CRF_NOT_FOUND = 'Problem with getting CRF',
  XML_NOT_FOUND = 'Problem with getting XML',
  CRF_VALIDITY_NOT_FOUND = 'CRF Validity not found',
  CRF_WITH_VALIDIY_NOT_FOUND = 'CRF for centre code not found',
  STARTING_DATACLASS_NOT_FOUND = 'Problem with searching for main Dataclass',
  XML_PARSING = 'Problem when parsing XML of crf',
  REFERENCE_DATE_NOT_FOUND = 'Problem with getting reference date',
  REFERENCE_DATE_WRONG_FORMAT = 'Problem with parsing reference date',
  CRF_VERSIONS_NOT_FOUND = 'Problem with getting CRF Versions',
  ACTIVE_VERSIONS_NOT_FOUND = 'Problem with getting active versions',
  VALUES_SET_NOT_FOUND = 'Problem with getting values set',
  FAILED_TO_LOAD = 'Failed to load',
}

export class CRFError extends DandelionError {
  constructor(message: string, type: CRFErrorType, nestedError?: unknown) {
    super(`${type} => ${message}`, nestedError);
  }
}

/// <reference types="vitest" />

import { defineConfig } from 'vite';
import Vue from '@vitejs/plugin-vue';
import { resolve } from 'path';

export default defineConfig({
  plugins: [Vue()],
  resolve: {
    alias: {
      // 'vue': 'vue/dist/vue.esm-bundler.js',
      '@': resolve(__dirname, 'src'),
    },
  },
  test: {
    globals: true,
    environment: 'jsdom',
    // exclude: ['./test/xslt-static/*'],
    environmentOptions: {
      jsdom: {
        // url: 'http://host.docker.internal:3000',
        url: 'http://localhost:3000',
      },
    },
  },
});

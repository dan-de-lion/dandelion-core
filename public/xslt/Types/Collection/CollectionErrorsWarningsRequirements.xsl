<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## ERRORS and WARNINGS ########## -->

    <xsl:template match="error">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:call-template name="createError">
            <xsl:with-param name="parentName" select="$parentName" />
            <xsl:with-param name="errorMessage" select="." />
            <xsl:with-param name="ifCondition" select="@if" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="warning">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:call-template name="createWarning">
            <xsl:with-param name="parentName" select="$parentName" />
            <xsl:with-param name="warningMessage" select="." />
            <xsl:with-param name="ifCondition" select="@if" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="requirement">
        <xsl:param name="parentName" />
        <xsl:param name="withReferenceID" />
        <xsl:call-template name="createRequirement">
            <xsl:with-param name="parentName" select="$parentName" />
            <xsl:with-param name="requirementMessage" select="." />
            <xsl:with-param name="satisfiedIfCondition" select="@satisfiedIf" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="description">
        <p class="variableDescription">
            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="." />
                </xsl:with-param>
            </xsl:call-template>
        </p>
    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="value" mode="checkbox">
        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="myValue">
            <xsl:choose>
                <xsl:when test="$valuesSetName">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '-', $myValue)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="concat($variableName, '-', $myValue)" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="groupName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="$withReferenceID" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="$variableName" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="name" select="@name" />

        <xsl:variable name="arrayOfExcludes">

            <!--manage excludes of the current value-->
            <xsl:apply-templates select="./excludes" />

            <xsl:if test="not(../../valuesGroup)">
                <!--the current value has attribute excludes=* and select all variable with name different from its-->
                <xsl:if test="./excludes='*'">
                    <xsl:for-each select="../value[@name!=$name]">
                        <xsl:value-of select="concat(@name, ',')" />
                    </xsl:for-each>
                </xsl:if>

                <!--the current value select all variables with name different from its and attribute excludes=*-->
                <xsl:for-each select="../value[@name!=$name and ./excludes='*']">
                    <xsl:value-of select="concat(@name, ',')" />
                </xsl:for-each>

                <!--the current value select all variables with name different from its and attribute excludes equal to its-->
                <xsl:for-each select="../value[@name!=$name and ./excludes=$name]">
                    <xsl:value-of select="concat(@name, ',')" />
                </xsl:for-each>
            </xsl:if>

            <xsl:if test="../../valuesGroup">
                <xsl:variable name="valuesGroupName" select="../@name" />
                <xsl:variable name="completeName" select="concat($valuesGroupName, '.', $name)" />

                <!--the current value has attribute excludes=* -->
                <xsl:if test="./excludes='*'">
                    <!--the current value select all values in its valuesGroup with name different from its-->
                    <xsl:for-each select="../../valuesGroup[@name=$valuesGroupName]/value[@name!=$name]">
                        <xsl:value-of select="concat(../@name, '.', @name, ',')" />
                    </xsl:for-each>

                    <!--the current value select all values in valuesGroup with name different from its-->
                    <xsl:for-each select="../../valuesGroup[@name!=$valuesGroupName]/value">
                        <xsl:value-of select="concat(../@name, '.', @name, ',')" />
                    </xsl:for-each>
                </xsl:if>

                <!--the current value select all variables with name different from its and attribute excludes=* inside a valuesGroup with the same name-->
                <xsl:for-each select="../../valuesGroup[@name=$valuesGroupName]/value[@name!=$name and ./excludes='*']">
                    <xsl:value-of select="concat(../@name, '.', @name, ',')" />
                </xsl:for-each>

                <!--the current value select all variables with name equal to its and attribute excludes=* inside a valuesGroup with different name from its-->
                <xsl:for-each select="../../valuesGroup[@name!=$valuesGroupName]/value[./excludes='*']">
                    <xsl:value-of select="concat(../@name, '.', @name, ',')" />
                </xsl:for-each>

                <!--the current value select all variables with name different from its and attribute excludes equal to its inside a valuesGroup with the same name-->
                <xsl:for-each
                    select="../../valuesGroup[@name=$valuesGroupName]/value[@name!=$name and ./excludes=$completeName]"
                >
                    <xsl:value-of select="concat(../@name, '.', @name, ',')" />
                </xsl:for-each>

                <!--the current value select all variables with name equal to its and attribute excludes equal to its inside a valuesGroup with different name from its-->
                <xsl:for-each select="../../valuesGroup[@name!=$valuesGroupName]/value[./excludes=$completeName]">
                    <xsl:value-of select="concat(../@name, '.', @name, ',')" />
                </xsl:for-each>

            </xsl:if>
        </xsl:variable>

        <li>
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <input type="checkbox" value="{ $myValue }" excludes="{$arrayOfExcludes}">
                <xsl:attribute name=":name" select="concat('getContextForID + ', $apos , $groupName, $apos)" />

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="$computedFormula and $computedFormula != ''">
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', $computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:if test="@label">
                    <xsl:attribute name="my-label" select="@label" />
                </xsl:if>

                <xsl:if test="@numericalValue">
                    <xsl:attribute name="my-numerical-value" select="@numericalValue" />
                </xsl:if>
            </input>

            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="separatedTooltip" select="'true'" />
                <xsl:with-param name="cssClass" select="'valueLabel'" />
            </xsl:call-template>
            <xsl:apply-templates select="./description" /> 
        </li>
    </xsl:template>


    <xsl:template match="value" mode="select">
        <xsl:param name="valuesSetName" />

        <xsl:variable name="myValue">
            <xsl:choose>
                <xsl:when test="$valuesSetName != ''">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <option value="{ $myValue }">
            <xsl:variable name="text">
                <xsl:choose>
                    <xsl:when test="@label and @label != ''">
                        <xsl:value-of select="@label" />
                    </xsl:when>
                    <xsl:when test="@name">
                        <xsl:value-of select="@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'-- this variable has no label or name! --'" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>

            <xsl:variable name="escapedString">
                <xsl:call-template name="escape">
                    <xsl:with-param name="string" select="$text" />
                </xsl:call-template>
            </xsl:variable>
            
            <xsl:variable
                name="translation"
                select="concat('translationService.t(', $apos, $escapedString, $apos, ')')"
            />
            
            <xsl:attribute name="v-html">
                <xsl:value-of select="normalize-space($translation)" disable-output-escaping="yes" />
            </xsl:attribute>

            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$myValue" />
            </xsl:call-template>

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <xsl:if test="@label">
                <xsl:attribute name="my-label" select="@label" />
            </xsl:if>

            <xsl:if test="@numericalValue">
                <xsl:attribute name="my-numerical-value" select="@numericalValue" />
            </xsl:if>
        </option>
    </xsl:template>

    <xsl:template match="value" mode="radio">
        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />

        <xsl:variable name="myValue">
            <xsl:choose>
                <xsl:when test="$valuesSetName">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="fullName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '-', $myValue)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="concat($variableName, '-', $myValue)" />
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="groupName">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="$withReferenceID" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="$variableName" />
            </xsl:if>
        </xsl:variable>

        <li>
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <xsl:call-template name="applyVisibility">
                <xsl:with-param name="fullName" select="$fullName" />
            </xsl:call-template>

            <input type="radio" value="{ $myValue }">
                <xsl:attribute name=":name" select="concat('getContextForID + ', $apos , $groupName, $apos)" />

                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="$computedFormula and $computedFormula != ''">
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', $computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $variableName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>

                <xsl:if test="@label">
                    <xsl:attribute name="my-label" select="@label" />
                </xsl:if>

                <xsl:if test="@numericalValue">
                    <xsl:attribute name="my-numerical-value" select="@numericalValue" />
                </xsl:if>
            </input>

            <xsl:call-template name="addVariableLabel">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="separatedTooltip" select="'true'" />
                <xsl:with-param name="cssClass" select="'valueLabel'" />
            </xsl:call-template>
        </li>
        <xsl:apply-templates select="./description" />
    </xsl:template>

    <xsl:template match="valuesGroup" mode="radio">
        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="groupValuesSetName">
            <xsl:choose>
                <xsl:when test="$valuesSetName">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <header>
            <h4>
                <xsl:call-template name="getTranslatedLabel" />
            </h4>

            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$groupValuesSetName" />
            </xsl:call-template>

            <xsl:attribute
                name="v-on:mouseover.stop"
                select="concat('onHover(', $apos, $groupValuesSetName, $apos, ')')"
            />
            <xsl:apply-templates select="./description" />      
        </header>

        <xsl:apply-templates select="value" mode="radio">
            <xsl:with-param name="valuesSetName" select="$groupValuesSetName" />
            <xsl:with-param name="variableName" select="$variableName" />
            <xsl:with-param name="computedFormula" select="$computedFormula" />
            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="valuesGroup" mode="checkbox">
        <xsl:param name="valuesSetName" />
        <xsl:param name="variableName" />
        <xsl:param name="computedFormula" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="groupValuesSetName">
            <xsl:choose>
                <xsl:when test="$valuesSetName">
                    <xsl:value-of select="concat($valuesSetName, '.', @name)" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@name" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <header>
            <h4 class="valuesGroupDescription">
                <xsl:call-template name="getTranslatedLabel" />
            </h4>
            
            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$groupValuesSetName" />
            </xsl:call-template>

            <xsl:attribute
                name="v-on:mouseover.stop"
                select="concat('onHover(', $apos, $groupValuesSetName, $apos, ')')"
            />
            <xsl:apply-templates select="./description" />      
        </header>

        <xsl:apply-templates select="value" mode="checkbox">
            <xsl:with-param name="valuesSetName" select="$groupValuesSetName" />
            <xsl:with-param name="variableName" select="$variableName" />
            <xsl:with-param name="computedFormula" select="$computedFormula" />
            <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="valuesGroup" mode="select">
        <xsl:param name="valuesSetName" />
        <xsl:variable name="groupValuesSetName" select="concat($valuesSetName, '.', @name)" />

        <optgroup>
            <xsl:attribute name="label">
                <xsl:choose>
                    <xsl:when test="@label and @label != ''">
                        <xsl:value-of select="@label" />
                    </xsl:when>
                    <xsl:when test="@name">
                        <xsl:value-of select="@name" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'-- this variable has no label or name! --'" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>

            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$groupValuesSetName" />
            </xsl:call-template>

            <xsl:apply-templates select="value" mode="select">
                <xsl:with-param name="valuesSetName" select="$groupValuesSetName" />
            </xsl:apply-templates>
        </optgroup>
    </xsl:template>
    
</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable[ @dataType = 'number' ] ">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="{ @dataType }" init-value="NaN">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>

                <xsl:apply-templates select="@precision" />

                <xsl:if test="@min and @min != ''">
                    <xsl:attribute name="min" select="@min" />
                </xsl:if>

                <xsl:if test="@max and @max != ''">
                    <xsl:attribute name="max" select="@max" />
                </xsl:if>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:if test="@unitOfMeasure">
                <span class="unitOfMeasure">
                    <xsl:call-template name="translateString">
                        <xsl:with-param name="string">
                            <xsl:value-of select="@unitOfMeasure" />
                        </xsl:with-param>
                    </xsl:call-template>
                </span>
            </xsl:if>

            <xsl:if test="@min and @max">
                <span class="range-description">
                    <xsl:variable name="interpolationValues">
                        <xsl:text>{ "min": "</xsl:text>
                        <xsl:value-of select="@min" />
                        <xsl:text>", "max": "</xsl:text>
                        <xsl:value-of select="@max" />
                        <xsl:text>" }</xsl:text>
                    </xsl:variable>
                    <xsl:variable
                        name="translation"
                        select="concat('translationService.t(', $apos, 'rangeDescription', $apos, ', ', $interpolationValues, ')')"
                    />
                    <xsl:attribute name="v-html" select="$translation" />
                </span>
            </xsl:if>

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <xsl:if test="@min and @min != ''">
                <xsl:variable name="min" select="concat('MIN')" />
                <xsl:call-template name="createError">
                    <xsl:with-param name="parentName" select="concat($fullName, '_')" />
                    <xsl:with-param name="errorMessage" select="'minError'" />
                    <xsl:with-param
                        name="ifCondition"
                        select="concat('checkMinValidity(', $apos,  $fullName , $apos, ', model, ', @min,')')"
                    />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="suffix" select="concat('MIN')" />
                    <xsl:with-param name="value" select="@min" />
                </xsl:call-template>
            </xsl:if>

            <xsl:if test="@max and @max != ''">
                <xsl:call-template name="createError">
                    <xsl:with-param name="parentName" select="concat($fullName, '_')" />
                    <xsl:with-param name="errorMessage" select="'maxError'" />
                    <xsl:with-param
                        name="ifCondition"
                        select="concat('checkMaxValidity(', $apos,  $fullName , $apos, ', model, ', @max,')')"
                    />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="suffix" select="concat('MAX')" />
                    <xsl:with-param name="value" select="@max" />
                </xsl:call-template>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'text' or @dataType = 'email' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="{ @dataType }">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>

                <!-- text input maxlength -->
                <xsl:if test="@max">
                    <xsl:attribute name="maxlength" select="@max" />
                </xsl:if>
            </input>

           <xsl:if test="@pattern">
                <xsl:call-template name="createError">
                    <xsl:with-param name="parentName" select="concat($fullName, '_')" />
                    <xsl:with-param name="errorMessage" select="concat('pattern-', @pattern, '-error')" />
                    <xsl:with-param
                        name="ifCondition"
                        select="concat('checkValidity(', $apos,  $fullName , $apos, ', model, ', $apos, @pattern, $apos, ')')"
                    />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="suffix" select="concat('PATTERN')" />
                </xsl:call-template>
            </xsl:if>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'output' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <output required-for-status="false">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>
            </output>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'color' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="{ @dataType }">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'support'] ">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div style="display: none">
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="{ @dataType }" required-for-status="false">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'text' and @interface = 'textArea' ] ">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <textarea>
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>
            </textarea>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'time' ] ">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="time">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'date' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="date">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>

                <xsl:if test="@minDate">
                    <xsl:attribute name=":min" select="@minDate" />
                </xsl:if>

                <xsl:if test="@maxDate and @maxDate != 'today'">
                    <xsl:attribute name=":max" select="@maxDate" />
                </xsl:if>

                <xsl:if test="@maxDate = 'today'">
                    <xsl:attribute name=":max" select="concat('new Date().toISOString().split(`T`)[0]','')" />
                </xsl:if>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'datetime' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="datetime-local">
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:call-template>

                <xsl:if test="@minDate">
                    <xsl:attribute name=":min" select="@minDate" />
                </xsl:if>

                <xsl:if test="@maxDate and @maxDate != 'today'">
                    <xsl:attribute name=":max" select="@maxDate" />
                </xsl:if>

                <xsl:if test="@maxDate = 'today'">
                    <xsl:attribute name=":max" select="concat('new Date().toISOString().split(`T`)[0]','')" />
                </xsl:if>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'boolean' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div class="boolean-variable">
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <input type="checkbox" init-value="false" v-custom-model-for-boolean="'{$fullName}'">
                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="@computedFormula and @computedFormula != '' ">
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:apply-templates select="@computedFormula" />

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                </xsl:call-template>
            </input>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'singlechoice' and ( not (@interface) or @interface = 'radio' ) ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div class="radiolist-wrapper">
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                <xsl:with-param name="cssLabelClass">radioDescription</xsl:with-param>
                <xsl:with-param name="element">span</xsl:with-param>
            </xsl:call-template>

            <ul class="radiolist" v-custom-model-for-singlechoice="'{$fullName}'">
                <xsl:if test="@initValue">
                    <xsl:attribute name="init-value">
                        <xsl:value-of select="@initValue" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:apply-templates select="@computedFormula" />

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                </xsl:call-template>

                <xsl:variable name="valuesSetName" select="@valuesSet" />

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="radio">
                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/valuesGroup" mode="radio">
                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>

                <xsl:apply-templates select="valuesGroup" mode="radio">
                    <xsl:with-param name="valuesSetName" select="''" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>

                <xsl:apply-templates select="value" mode="radio">
                    <xsl:with-param name="valuesSetName" select="''" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>
            </ul>

            <button class="variable-reset" type="button">
                <xsl:attribute name=":disabled">
                    <xsl:choose>
                        <xsl:when test="@computedFormula and @computedFormula != '' ">
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of
                                select="concat('!currentCaseService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                            />
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:attribute>
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('resetRadioButton(', $apos, $fullName, $apos, ')')"
                />
                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">Reset</xsl:with-param>
                </xsl:call-template>
            </button>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>

    <xsl:template match="variable[ @dataType = 'singlechoice' and @interface = 'dropdown' ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <select>
                <xsl:call-template name="inputAttributes">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                    <xsl:with-param name="customModelDirective" select="'v-custom-model-for-select'" />
                </xsl:call-template>

                <option value="" my-label="none" />

                <xsl:variable name="valuesSetName" select="@valuesSet" />

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="select">
                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                </xsl:apply-templates>

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/valuesGroup" mode="select">
                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                </xsl:apply-templates>

                <xsl:apply-templates select="valuesGroup" mode="select">
                    <xsl:with-param name="valuesSetName" select="''" />
                </xsl:apply-templates>

                <xsl:apply-templates select="value" mode="select">
                    <xsl:with-param name="valuesSetName" select="''" />
                </xsl:apply-templates>
            </select>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>

    </xsl:template>

    <xsl:template match="variable[ @dataType = 'multiplechoice' and ( not (@interface) or @interface = 'checkbox' ) ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div class="checkboxlist-wrapper">
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                <xsl:with-param name="cssLabelClass">checkBoxDescription</xsl:with-param>
                <xsl:with-param name="element">span</xsl:with-param>
            </xsl:call-template>

            <ul class="checkboxlist" v-custom-model-for-checkboxes="'{$fullName}'" init-value="[]">
                <xsl:if test="@initValue">
                    <xsl:attribute name="init-value">
                        <xsl:value-of select="@initValue" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:apply-templates select="@computedFormula" />

                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                </xsl:call-template>

                <xsl:variable name="valuesSetName" select="@valuesSet" />

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/value" mode="checkbox">
                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />

                </xsl:apply-templates>

                <xsl:apply-templates select="//valuesSet[ @name = $valuesSetName ]/valuesGroup" mode="checkbox">
                    <xsl:with-param name="valuesSetName" select="$valuesSetName" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>

                <xsl:apply-templates select="valuesGroup" mode="checkbox">
                    <xsl:with-param name="valuesSetName" select="''" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>

                <xsl:apply-templates select="value" mode="checkbox">
                    <xsl:with-param name="valuesSetName" select="''" />
                    <xsl:with-param name="variableName" select="$fullName" />
                    <xsl:with-param name="computedFormula" select="@computedFormula" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                    <xsl:with-param name="requiredFromParent" select="$requiredFromParent" />
                </xsl:apply-templates>
            </ul>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
            </xsl:call-template>
        </div>
    </xsl:template>


    <xsl:template match="variable[ @dataType = 'multiplechoice' and @interface = 'autocomplete' ] ">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <div>
            <xsl:call-template name="wrapperAttributes">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>

            <xsl:apply-templates select="./description" />

            <xsl:call-template name="afterVariable">
                <xsl:with-param name="fullName" select="$fullName" />
                <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
            </xsl:call-template>
        </div>

    </xsl:template>

    <!--   #### PLUGGABLE DATA TYPE #### -->
    <xsl:template match="variable[ @dataType = 'plugin' and @interface ]">
        <xsl:param name="parent" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="nextReferenceID">
            <xsl:if test="$withReferenceID != ''">
                <xsl:value-of select="concat($withReferenceID, '.', @name)" />
            </xsl:if>
            <xsl:if test="$withReferenceID = ''">
                <xsl:value-of select="''" />
            </xsl:if>
        </xsl:variable>

        <!-- add canRead -->
        <Suspense>
            <xsl:element name="{@interface}">
                <xsl:if test="@requiredForStatus">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="@requiredForStatus" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
                    <xsl:attribute name="required-for-status">
                        <xsl:value-of select="$requiredFromParent" />
                    </xsl:attribute>
                </xsl:if>

                <xsl:call-template name="applyVisibility">
                    <xsl:with-param name="fullName" select="$fullName" />
                </xsl:call-template>
                <xsl:apply-templates select="./customAttribute" />
                <xsl:attribute name=":context">context</xsl:attribute>
                <xsl:attribute name=":container-type">containerType</xsl:attribute>
                <xsl:attribute name=":model">model</xsl:attribute>

                <xsl:call-template name="addID">
                    <xsl:with-param name="fullName" select="$fullName" />
                    <xsl:with-param name="withReferenceID" select="$nextReferenceID" />
                </xsl:call-template>

                <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            </xsl:element>
        </Suspense>
    </xsl:template>

    <!--  #### CUSTOM ATTRIBUTE FOR PLUGGABLE DATA TYPE #### -->
    <xsl:template match="customAttribute">
        <xsl:attribute name="{@name}">
            <xsl:value-of select="." />
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeRadioNotDefined" mode="radio" />
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeSelectNotDefined" mode="select" />
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeCheckboxNotDefined" mode="checkbox" />
</xsl:stylesheet>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="root[ @accessLevel ]">
        <li class="developerError">
            <xsl:value-of select="concat('Root tag cannot contain accessLevel')" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'group' and not(@dataClass) and @initValue] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Init value attribute should not be in variable: ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'group' and not(@dataClass) and not (./variable)] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('There are no childrens in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="dataClass[ @activateIf ]">
        <xsl:if test="not (position() = 1)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('ActivateIf in dataClass ', $apos, @name, $apos, ' is wrong, it should be only in first dataClass')"
                />
            </li>
        </xsl:if>
    </xsl:template>

   <xsl:template match="dataClass [ @initValue ]">
        <li class="developerError">
            <xsl:value-of select="concat('Init value attribute should not be in dataClass: ', $apos, @name, $apos)" />
        </li>
        <xsl:next-match />
    </xsl:template>

   <xsl:template match="dataClass [ @name = 'NewCase' ]">
        <xsl:if test="not (position() = last())">
            <li class="developerError">
                <xsl:value-of select="concat('New Case should be the last dataClass')" />
            </li>
        </xsl:if>
        <xsl:next-match />
    </xsl:template>

   <xsl:template match="dataClass [ not (./variable) ]">
        <li class="developerError">
            <xsl:value-of select="concat('There are no childrens in dataClass ', $apos, @name, $apos)" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="dataClass[ @accessLevel ]">
        <li class="developerError">
            <xsl:value-of select="concat('DataClass ', $apos, @name, $apos, ' cannot contain accessLevel')" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="dataClass[contains(@name, '.')]">
        <li class="developerError">
            <xsl:value-of select="'Name in dataClass ', $apos, @name, $apos, ' should not contain dots'" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'group' and @dataClass != '' and @initValue]">
        <li class="developerError">
            <xsl:value-of
                select="concat('Init value attribute is not in group variable: ', $apos, @name, $apos, ' that is a dcInstance of ', @dataClass)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'group' and @dataClass != '' and ./variable]">
        <li class="developerError">
            <xsl:value-of
                select="concat('In variable ', $apos, @name, $apos, ' that is a type ', @dataType, ' you cannot have both an attribute ', $apos, 'dataClass', $apos, ' and variable tags inside the group.')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>
</xsl:stylesheet>

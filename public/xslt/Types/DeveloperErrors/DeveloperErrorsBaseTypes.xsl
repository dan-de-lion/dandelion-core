<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template
        match="variable[(@dataType = 'output' or @dataType = 'support') and (not(@requiredForStatus) or @requiredForStatus != 'false')]"
    >
        <li class="developerError">
            <xsl:value-of
                select="concat('RequiredForStatus should be specified and set to ', $apos, 'false', $apos, ' in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable[(@dataType = 'output' or @dataType = 'support') and (not(@computedFormula))]">
        <li class="developerError">
            <xsl:value-of
                select="concat('Computed Formula attribute missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType = 'singlechoice']">
        <xsl:variable name="valuesSetName" select="@valuesSet" />
        <xsl:if test="//valuesSet[ @name = $valuesSetName ]/value/excludes">
            <li class="developerError">
                <xsl:value-of
                    select="concat('Value in valuesSet ', $apos, @valuesSet, $apos, ' in variable ', $apos, @name, $apos, ' shouldn', $apos, 't have excludes tag. Do you intend multiplechoice instaed?')"
                />
            </li>
        </xsl:if>
        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template
        match="variable[@dataType = 'singlechoice' and (not(@valuesSet) and not(./valuesGroup) and not(./value))]"
    >
        <li class="developerError">
            <xsl:value-of
                select="concat('ValuesSet and Value tags missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template
        match="variable[@dataType = 'multiplechoice' and (not(@valuesSet) and not(./valuesGroup) and not(./value))]"
    >
        <li class="developerError">
            <xsl:value-of
                select="concat('ValuesSet and Value tags missing in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template match="valuesSet[@accessLevel]">
        <li class="developerError">
            <xsl:value-of select="concat('ValuesSet ', $apos, @name, $apos, ' cannot contain accessLevel')" />
        </li>
    </xsl:template>

    <xsl:template match="valuesSet[contains(@name, '.')]">
        <li class="developerError">
            <xsl:value-of select="concat('Name in ValuesSet ', $apos, @name, $apos, ' should not contain dots')" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="valuesSet[./valuesGroup and ./value]">
        <li class="developerError">
            <xsl:value-of
                select="concat('There is a valuesGroup and a value at the same level inside valuesSet ', $apos, @name , $apos )"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="valuesGroup[not(@name)]">
        <li class="developerError">
            <xsl:value-of
                select="concat('There is a valuesGroup without a name inside variable ', $apos, ../@name , $apos )"
            />
        </li>
    </xsl:template>

    <xsl:template match="valuesGroup[not(./value)]">
        <li class="developerError">
            <xsl:value-of select="concat('No values found inside valuesGroup ', $apos, @name, $apos)" />
        </li>
    </xsl:template>

    <xsl:template match="value[./excludes and (../../valuesGroup)]">
        <xsl:variable name="fullName" select="concat(../@name, '.', @name)" />
        <xsl:if test="./excludes = $fullName">
            <li class="developerError">
                <xsl:value-of
                    select="concat('Value tag ', $apos, $fullName, $apos, ' cannot contains an excludes tag with the complete name of valuesGroup and value')"
                />
            </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="value[./excludes and not(../../valuesGroup)]">
        <xsl:if test="./excludes = @name">
            <li class="developerError">
                <xsl:value-of
                    select="concat('Excludes tag ', $apos, @name, $apos, ' cannot contains the name of the value tag')"
                />
            </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable[@dataType = 'set' and not (@itemsDataClass)]">
        <li class="developerError">
            <xsl:value-of
                select="concat('ItemsDataClass attribute missing in variable ', $apos, @name, $apos, ' that is a type ', $apos, 'set' , $apos)"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable[@dataType = 'set' and @itemsDataClass and @separateInPages]">
        <xsl:variable name="dataType" select="@itemsDataClass" />

        <xsl:if test="not(//dataClass[ @name = $dataType ]/variable/@itemLabel)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('ItemLabel attribute missing in dataClass ', $apos, @itemsDataClass, $apos, ' in variable ', $apos, @name, $apos, ' that is a type set')"
                />
            </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable[@dataType = 'set' and @itemsDataClass and @computedItems]">
        <xsl:variable name="dataType" select="@itemsDataClass" />

        <xsl:if test="not(//dataClass[ @name = $dataType ]/variable/@itemKey)">
            <li class="developerError">
                <xsl:value-of
                    select="concat('ItemKey attribute missing in dataClass ', $apos, @itemsDataClass, $apos, ' in variable ', $apos, @name, $apos, ' that is a type set')"
                />
            </li>
        </xsl:if>
    </xsl:template>

    <xsl:template match="variable[@dataType = 'set' and @itemsDataType]">
        <li class="developerError">
            <xsl:value-of
                select="concat('ItemsDataType attribute in variable: ', $apos, @name, $apos, ' is no longer supported from @0.7. Replace it with: itemsDataClass')"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable[@dataType = 'reference' and contains(@referencedFullName, '?')]">
        <li class="developerError">
            <xsl:value-of
                select="concat('ReferencedFullName cannot contain question marks in variable ', $apos, @name, $apos, ' that is a type reference')"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable[@dataType = 'reference' and @fullname]">
        <li class="developerError">
            <xsl:value-of
                select="concat('Fullname in variable: ', $apos, @name, $apos, ' is no longer supported from @0.7. Replace it with: referencedFullName')"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable[@itemKey and @itemKey != 'true']">
        <li class="developerError">
            <xsl:value-of
                select="concat('ItemKey in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' should always be true')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[not (@name)]">
        <li class="developerError">
            <xsl:value-of select="concat('Name missing in a variable that is a type ', @dataType)" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@name = '']">
        <li class="developerError">
            <xsl:value-of select="concat('Name in a variable that is a type ', @dataType, ' should not be empty')" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@visible]">
        <li class="developerError">
            <xsl:value-of
                select="concat('Visible in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' is no longer supported from @0.7. Replace it with: visibleIf')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@visibleIf = '']">
        <li class="developerError">
            <xsl:value-of
                select="concat('VisibleIf in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' should not be empty')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@computedFormula = '']">
        <li class="developerError">
            <xsl:value-of
                select="concat('Computed Formula in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' should not be empty')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[contains(@computedFormula, 'currentSet')]">
        <li class="developerError">
            <xsl:value-of
                select="concat('CurrentSet in computedFormula in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' is no longer supported from @0.7. Replace it with: thisSet')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType = '']">
        <li class="developerError">
            <xsl:value-of
                select="concat('DataType in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' should not be empty')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@valuesSet = '']">
        <li class="developerError">
            <xsl:value-of
                select="concat('ValuesSet in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' should not be empty')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@itemsDataClass = '']">
        <li class="developerError">
            <xsl:value-of
                select="concat('ItemsDataClass in variable ', $apos, @name, $apos, ' that is a type ', @dataType,' should not be empty')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[not (@dataType)]">
        <li class="developerError">
            <xsl:value-of select="concat('DataType attribute missing in variable ', $apos, @name, $apos)" />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'number' and (@max or @min or @precision or @measureUnit)] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Number attributes should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'text' and  @interface = 'textArea'] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Interface ', $apos, 'textArea', $apos, ' attribute, should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'singlechoice' and @dataType != 'multiplechoice' and  @valuesSet] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('ValuesSet attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
    </xsl:template>

    <xsl:template match="variable[@dataType != 'date' and  (@minDate or @maxDate)] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Date attributes should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'group' and @dataType != 'set' and  @separateInPages] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('SeparateInPages attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'group' and  @dataClass] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('DataClass attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'set' and  @itemsDataClass] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('ItemsDataClass attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'group' and ./variable] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('There shouldn', $apos, 't be other variables as children of variable ', $apos, @name, $apos, ' that is a type ', @dataType, ' and not a type ', $apos, 'group.', $apos, ' Maybe tag variable is not closed?')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType != 'text' and @pattern] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Pattern attribute should not be in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@pattern and (@pattern = '' or @pattern = ' ')] ">
        <li class="developerError">
            <xsl:value-of
                select="concat('Pattern attribute should not be empty in variable ', $apos, @name, $apos, ' that is a type ', @dataType)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[@dataType = 'plugin']">
        <xsl:variable name="regexPattern">^[a-z0-9_]+$</xsl:variable>
        <xsl:choose>
            <xsl:when test="matches(@interface, $regexPattern)">
                <!-- The string respect the regex -->
            </xsl:when>
            <xsl:otherwise>
                <!-- The string doesn't respect the regex, generate a message of error -->
                <li class="developerError">
                    <xsl:value-of
                        select="concat('The interface value in variable ', $apos, @name, $apos, ' that is a type plugin, does not respect regex ', $apos, $regexPattern, $apos, '. The interface should not have upper case or special characters')"
                    />
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="variable[contains(@name, '.')]">
        <li class="developerError">
            <xsl:value-of
                select="concat('Name in a variable ', $apos, @name, $apos, ' that is a type ', @dataType,', should not contain dots')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="variable[//dataClass/@name = @dataType]">
        <li class="developerError">
            <xsl:value-of
                select="concat('DataType=', $apos, @dataType, $apos, ' in variable: ', $apos, @name, $apos, ' is no longer supported from @0.7. Replace it with: dataType= ', $apos, 'group', $apos, ' dataClass=', $apos, @dataType, $apos)"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeRadioNotDefined" mode="radio" />
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeSelectNotDefined" mode="select" />
    <xsl:template match="ThisIsUsefulOnlyForRemoveErrorOfModeCheckboxNotDefined" mode="checkbox" />

</xsl:stylesheet>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="@* | node()" />

    <xsl:template match="root">
        <xsl:apply-templates select="dataClass" />
        <xsl:apply-templates select="valuesSet" />
    </xsl:template>

    <xsl:template match="variable [ @dataType = 'group' ]">
        <xsl:apply-templates select="./variable" />
        <xsl:apply-templates select="./requirement" />
    </xsl:template>

    <xsl:template match="dataClass">
        <xsl:apply-templates select="./variable" />
        <xsl:apply-templates select="./requirement" />
    </xsl:template>

    <xsl:template match="valuesSet">
        <xsl:apply-templates select="value" />
        <xsl:apply-templates select="valuesGroup" />
    </xsl:template>

    <xsl:template>
        <xsl:apply-templates select="./error" />
        <xsl:apply-templates select="./warning" />
        <xsl:apply-templates select="./requirement" />
    </xsl:template>

    <xsl:template match="variable[@ignoreCRFErrors = 'true']">
    </xsl:template>
</xsl:stylesheet>

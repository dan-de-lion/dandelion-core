<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="requirement[@if]">
        <li class="developerError">
            <xsl:value-of
                select="concat('If in variable: ', $apos, @name, $apos, ' is no longer supported from @0.7. Replace it with: satisfiedIf')"
            />
        </li>
        <xsl:next-match />
    </xsl:template>

</xsl:stylesheet>

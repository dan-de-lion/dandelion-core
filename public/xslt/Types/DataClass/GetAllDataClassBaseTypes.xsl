<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="variable [ @dataType= 'group' and (@dataClass = '' or not(@dataClass))] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        {
        "dataClassName": "<xsl:value-of select="concat($root, '.', $fullName)" />",
        "dataClassList": [
        <xsl:apply-templates select="./variable" mode="variableInsideDataClass">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>
        ]
        },
        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="variable [ @dataType= 'group' and @dataClass != ''] ">
        <xsl:param name="parent" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        <xsl:variable name="dataClass" select="@dataClass" />

        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]" mode="dataClassSearched">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="variable [ @dataType != 'group'] ">
        <!-- This is the case when a default variable/error/warning/requirement is matched, in these cases is right to do nothing -->
    </xsl:template>

    <xsl:template match="variable [ @dataType= 'set' ] ">
        <xsl:param name="parent" />

        <xsl:variable name="dataClass" select="@itemsDataClass" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />

        <xsl:apply-templates select="//dataClass[ @name = $dataClass ]" mode="dataClassSearched">
            <xsl:with-param name="parent" select="$fullName" />
        </xsl:apply-templates>

    </xsl:template>


</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- ######## DATACLASS ######## -->
    <!-- group of variables that can be inserted in different point of the data collection -->
<!--    <xsl:template match="root">-->
<!--        <xsl:apply-templates select="//variable">-->
<!--            <xsl:with-param name="parent" select="@name"/>-->
<!--        </xsl:apply-templates>-->
<!--    </xsl:template>-->

    <xsl:template match="dataClass">
        <xsl:param name="parent" />

        <xsl:variable name="selectedParent">
            <xsl:call-template name="selectParent">
                <xsl:with-param name="parent" select="$parent" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="$selectedParent" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="dataClass" mode="dataClassSearched">

        <xsl:param name="parent" />
        {
        "dataClassName": "<xsl:value-of select="@name" />",
        "dataClassList": [
        <xsl:apply-templates select="./variable" mode="variableInsideDataClass">
            <xsl:with-param name="parent" select="concat($parent, '.')" />
        </xsl:apply-templates>
        ]
        },
        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($parent, '.')" />
        </xsl:apply-templates>


    </xsl:template>

    <!-- ######## INSTANCE OF DATACLASS ######## -->
    <!-- variable with datatype not in BaseTypes -->
    <!-- searches for a dataclass that has the same name of the datatype -->

    <xsl:template match="variable" mode="variableInsideDataClass">
        <xsl:param name="parent" />
        <xsl:variable name="dataType" select="@dataType" />

        <xsl:variable name="fullName" select="concat($parent, @name)" />
        {
            "fullName" : "<xsl:value-of select="concat($root, '.', $fullName)" />",
            "label" : "<xsl:value-of select="@label" />",
            "type" : "<xsl:value-of select="$dataType" />"
            <xsl:if test="(($dataType = 'singlechoice') or ($dataType = 'multiplechoice')) and @valuesSet">
                ,
                "valuesSet" : "<xsl:value-of select="@valuesSet" />"
            </xsl:if>
            <xsl:if test="(($dataType = 'singlechoice') or ($dataType = 'multiplechoice')) and not(@valuesSet)">
                ,
                "valuesSet" : "<xsl:value-of select="@name" />"
            </xsl:if>
            <xsl:if test="(@dataType = 'set')">
                ,
                "itemsDataClass" : "<xsl:value-of select="@itemsDataClass" />"
            </xsl:if>
        } ,

        <xsl:apply-templates select="./variable">
            <xsl:with-param name="parent" select="concat($fullName, '.')" />
        </xsl:apply-templates>
    </xsl:template>
</xsl:stylesheet>

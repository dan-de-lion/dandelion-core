<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

    <xsl:template match="variable">
        <!-- This is necessary to prevent other variable datatypes from doing anything -->
    </xsl:template>

</xsl:stylesheet>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="root">
        <!--Create the array with all ValuesSet and the value inside variable -->
        [
        <!--Count the templates matches, for understand if insert a comma-->
            <xsl:variable name="variableWithValueCount">
                <xsl:value-of select="count(*/variable[value])" />
            </xsl:variable>
            <xsl:variable name="valuesSetCount">
                <xsl:value-of select="count(valuesSet)" />
            </xsl:variable>

        <!--Select the templates and in case, insert a comma in the middle -->
            <xsl:apply-templates select="//variable[value]" />
            <xsl:if test="$variableWithValueCount > 0 and $valuesSetCount > 0">,</xsl:if>
            <xsl:apply-templates select="valuesSet" />
        ]
    </xsl:template>

    <xsl:template match="valuesSet">
        <!--The position and last methods must be here because they give the position of the last template selected, so if we put the position/last method under the two apply templates, it's wrong -->

        <xsl:variable name="position"><xsl:value-of select="position()" /></xsl:variable>
        <xsl:variable name="last"><xsl:value-of select="last()" /></xsl:variable>

        {
            "valuesSetName":"<xsl:value-of select="@name" />",
            "valuesSetList":
                [   <xsl:apply-templates select="valuesGroup" />
                    <xsl:apply-templates select="value" />  ]
        }

        <xsl:if test="$position &lt; $last">,</xsl:if>
    </xsl:template>

    <xsl:template match="valuesGroup">
        <xsl:variable name="position"><xsl:value-of select="position()" /></xsl:variable>
        <xsl:variable name="last"><xsl:value-of select="last()" /></xsl:variable>

        <xsl:apply-templates select="./value">
            <xsl:with-param name="valuesGroupName" select="concat(@name, '.')" />
        </xsl:apply-templates>
        
        <xsl:if test="$position &lt; $last">,</xsl:if>
    </xsl:template>

    <xsl:template match="value">
        <xsl:param name="valuesGroupName" />
        {
            "name": "<xsl:value-of select="concat($valuesGroupName, @name)" />",
            "label": "<xsl:value-of select="@label" />",
            "numericalValue": "<xsl:value-of select="@numericalValue" />"
        }
        <xsl:if test="position() &lt; last()">,</xsl:if>
    </xsl:template>

    <xsl:template match="variable[value]">

        <xsl:variable name="position"><xsl:value-of select="position()" /></xsl:variable>
        <xsl:variable name="last"><xsl:value-of select="last()" /></xsl:variable>

        {
            "valuesSetName":"<xsl:value-of select="@name" />",
            "valuesSetList":
                [   <xsl:apply-templates select="valuesGroup" />
                    <xsl:apply-templates select="value" />  ]
        }

        <xsl:if test="$position &lt; $last or count(//valuesSet) &gt; 0">,</xsl:if>
    </xsl:template>
</xsl:stylesheet>

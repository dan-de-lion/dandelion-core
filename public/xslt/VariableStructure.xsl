<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!-- ######## VARIABLES COMPACT ATTRIBUTES ######## -->

    <xsl:template name="wrapperAttributes">
        <xsl:param name="fullName" />
        <xsl:param name="separatedTooltip" select="'true'" />
        <xsl:param name="element" select="'label'" />
        <xsl:param name="cssLabelClass" select="'variableLabel'" />
        <xsl:param name="withReferenceID" />

        <xsl:variable name="getHooksByType">getHooksByType(TypesOfDisplayHooks.BeforeVariable)</xsl:variable>
        <component>
            <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
            <xsl:attribute name=":is">componentName</xsl:attribute>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            <xsl:attribute name=":context">context</xsl:attribute>
            <xsl:attribute name=":container-type">containerType</xsl:attribute>
        </component>

        <xsl:attribute name="v-if">
            <xsl:value-of
                select="concat('currentCaseService.canRead(', $apos,  $fullName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
            />
        </xsl:attribute>

        <xsl:call-template name="applyVisibility">
            <xsl:with-param name="fullName" select="$fullName" />
        </xsl:call-template>

        <xsl:call-template name="addWrapperID">
            <xsl:with-param name="fullName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>

        <xsl:call-template name="addVariableLabel">
            <xsl:with-param name="fullName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
            <xsl:with-param name="separatedTooltip" select="$separatedTooltip" />
            <xsl:with-param name="cssClass" select="$cssLabelClass" />
            <xsl:with-param name="element" select="$element" />
        </xsl:call-template>

        <xsl:if test="$statisticianMode = 'true'">
            <span class="statistician" v-html="'{$fullName}'" />
        </xsl:if>
    </xsl:template>

    <xsl:template name="inputAttributes">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="requiredFromParent" select="''" />
        <xsl:param name="customModelDirective" select="'v-custom-model'" />

        <xsl:apply-templates select="@computedFormula" />

        <xsl:if test="@requiredForStatus">
            <xsl:attribute name="required-for-status">
                <xsl:value-of select="@requiredForStatus" />
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="not(@requiredForStatus) and $requiredFromParent != ''">
            <xsl:attribute name="required-for-status">
                <xsl:value-of select="$requiredFromParent" />
            </xsl:attribute>
        </xsl:if>

        <xsl:if test="@initValue">
            <xsl:attribute name="init-value">
                <xsl:value-of select="@initValue" />
            </xsl:attribute>
        </xsl:if>

        <xsl:attribute name="{$customModelDirective}">
            <xsl:value-of select="concat($apos, $fullName, $apos)" />
        </xsl:attribute>

        <xsl:attribute name=":disabled">
            <xsl:choose>
                <xsl:when test="@computedFormula and @computedFormula != '' ">
                    <xsl:value-of
                        select="concat('!currentCaseService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ') || (', @computedFormula, ') !== null')"
                    />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of
                        select="concat('!currentCaseService?.canWrite(', $apos, $fullName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                    />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>

        <xsl:call-template name="addID">
            <xsl:with-param name="fullName" select="$fullName" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:call-template>
    </xsl:template>

    <xsl:template name="afterVariable">
        <xsl:param name="fullName" />
        <xsl:param name="withReferenceID" />

        <xsl:variable name="getHooksByType">getHooksByType(TypesOfDisplayHooks.AfterVariable)</xsl:variable>

        <component>
            <xsl:attribute name="v-for" select="concat('componentName in ', $getHooksByType)" />
            <xsl:attribute name=":is">componentName</xsl:attribute>
            <xsl:attribute name=":full-name" select="concat($apos, $fullName, $apos)" />
            <xsl:attribute name=":context">context</xsl:attribute>
            <xsl:attribute name=":container-type">containerType</xsl:attribute>
        </component>

        <xsl:apply-templates select="./error">
            <xsl:with-param name="parentName" select="concat($fullName, '_')" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
        <xsl:apply-templates select="./warning">
            <xsl:with-param name="parentName" select="concat($fullName, '_')" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
        <xsl:apply-templates select="./requirement">
            <xsl:with-param name="parentName" select="concat($fullName, '_')" />
            <xsl:with-param name="withReferenceID" select="$withReferenceID" />
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template name="createError">
        <xsl:param name="parentName" />
        <xsl:param name="errorMessage" />
        <xsl:param name="ifCondition" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="suffix" />
        <xsl:param name="value" />
        <xsl:param name="clickable" />

        <xsl:variable name="IDattribute">
            <xsl:if test="$suffix !=''">
                <xsl:value-of select="concat('ERROR_', $suffix)" />
            </xsl:if>
            <xsl:if test="$suffix =''">
                <xsl:value-of select="concat('ERROR_', @name)" />
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="errorName" select="concat($parentName, $IDattribute)" />

        <div
            class="error"
            v-custom-model-for-errors="'{$errorName}'"
            init-value="null"
            v-if="{ $ifCondition } &amp;&amp; { concat('currentCaseService.canRead(', $apos,  $errorName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')') }"
        >

            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!currentCaseService?.canWrite(', $apos, $errorName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:if test="$clickable">
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $errorName, $apos), ')')"
                />
            </xsl:if>

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$parentName" />
                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                <xsl:with-param name="IDattribute" select="$IDattribute" />
            </xsl:call-template>

            <xsl:if test="$value = ''">
                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">
                        <xsl:value-of select="$errorMessage" />
                    </xsl:with-param>
                </xsl:call-template>
            </xsl:if>
            <xsl:if test="$value != ''">
                <xsl:variable name="interpolationValues">
                    <xsl:text>{ "value": "</xsl:text>
                    <xsl:value-of select="$value" />
                    <xsl:text>" }</xsl:text>
                </xsl:variable>
                <xsl:variable
                    name="translation"
                    select="concat('translationService.t(', $apos, $errorMessage, $apos, ', ', $interpolationValues, ')')"
                />
                <span v-html="{$translation}" />
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template name="createWarning">
        <xsl:param name="parentName" />
        <xsl:param name="warningMessage" />
        <xsl:param name="ifCondition" />
        <xsl:param name="withReferenceID" />
        <xsl:param name="clickable" />
        <xsl:variable name="IDattribute" select="concat('WARNING_', @name, '_CONFIRMED')" />
        <xsl:variable name="warningName" select="concat($parentName, $IDattribute)" />

        <div
            v-if="{ $ifCondition } &amp;&amp; { concat('currentCaseService.canRead(', $apos,  $warningName , $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')') }"
            class="warning"
        >
            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!currentCaseService?.canWrite(', $apos, $warningName, $apos, ',', $apos, $crfName, $apos,',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:if test="$clickable = 'true'">
                <xsl:attribute
                    name="v-on:click.stop"
                    select="concat('currentPageService.scrollToVariable(', concat('getContextForID + ', $apos, $warningName, $apos), ')')"
                />
            </xsl:if>

            <xsl:call-template name="addWrapperID">
                <xsl:with-param name="fullName" select="$parentName" />
                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                <xsl:with-param name="IDattribute" select="$IDattribute" />
            </xsl:call-template>

            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="$warningMessage" />
                </xsl:with-param>
            </xsl:call-template>

            <xsl:variable name="nameForID">
                <xsl:if test="$withReferenceID != ''">
                    <xsl:if test="$IDattribute != ''">
                        <xsl:value-of select="concat($withReferenceID, '_' , $IDattribute)" />
                    </xsl:if>
                    <xsl:if test="$IDattribute = ''">
                        <xsl:value-of select="$withReferenceID" />
                    </xsl:if>
                </xsl:if>
                <xsl:if test="$withReferenceID = ''">
                    <xsl:if test="$IDattribute != ''">
                        <xsl:value-of select="concat($parentName, $IDattribute)" />
                    </xsl:if>
                    <xsl:if test="$IDattribute = ''">
                        <xsl:value-of select="$parentName" />
                    </xsl:if>
                </xsl:if>
            </xsl:variable>

            <xsl:variable
                name="IDwithoutPrefix"
                select="concat('getContextForID + ' , $apos , $nameForID, $suffix, $apos)"
            />

            <xsl:variable name="id">
                <xsl:if test="$prefixForID != ''">
                    <xsl:value-of select="concat($apos, $prefixForID, $apos, ' + ', $IDwithoutPrefix)" />
                </xsl:if>
                <xsl:if test="$prefixForID = ''">
                    <xsl:value-of select="$IDwithoutPrefix" />
                </xsl:if>
            </xsl:variable>

            <input type="checkbox" v-custom-model-for-warnings="'{$warningName}'">
                <xsl:attribute
                    name=":disabled"
                    select="concat('!currentCaseService.canWrite(', $apos, $warningName, $apos, ', ', $apos, $crfName, $apos, ', ', $apos, $crfVersion, $apos, ')')"
                />
                <xsl:attribute name=":id" select="$id" />
                <xsl:attribute name="v-on:click.stop" />
            </input>
            <label>
                <xsl:attribute name=":for" select="$id" />
                <xsl:attribute name="v-on:click.stop" />
                <xsl:call-template name="translateString">
                    <xsl:with-param name="string">I confirm the inserted data</xsl:with-param>
                </xsl:call-template>
            </label>
        </div>
    </xsl:template>

    <xsl:template name="createRequirement">
        <xsl:param name="parentName" />
        <xsl:param name="requirementMessage" />
        <xsl:param name="satisfiedIfCondition" />
        <xsl:param name="withReferenceID" />
        <xsl:variable name="IDattribute" select="concat('REQUIREMENT_', @name)" />
        <xsl:variable name="requirementName" select="concat($parentName, $IDattribute)" />

        <div
            class="requirement"
            v-if="!({ $satisfiedIfCondition })"
            v-custom-model-for-requirements="'{$requirementName}'"
            init-value="null"
        >
            <xsl:attribute
                name=":class"
                select="concat('{ unwritable: ', concat('!currentCaseService?.canWrite(', $apos, $requirementName, $apos, ',', $apos, $crfName, $apos, ',', $apos, $crfVersion, $apos,')'), '}')"
            />

            <xsl:if test="@requiredForStatus">
                <xsl:attribute name="required-for-status">
                    <xsl:value-of select="@requiredForStatus" />
                </xsl:attribute>
            </xsl:if>

            <xsl:call-template name="addID">
                <xsl:with-param name="fullName" select="$parentName" />
                <xsl:with-param name="withReferenceID" select="$withReferenceID" />
                <xsl:with-param name="IDattribute" select="$IDattribute" />
            </xsl:call-template>

            <xsl:call-template name="translateString">
                <xsl:with-param name="string">
                    <xsl:value-of select="$requirementMessage" />
                </xsl:with-param>
            </xsl:call-template>
        </div>
    </xsl:template>
</xsl:stylesheet>

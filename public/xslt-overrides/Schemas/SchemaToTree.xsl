<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:import href="../../xslt/Utils.xsl" />
    <xsl:import href="../../xslt/VariableAttributes.xsl" />
    <xsl:import href="../../xslt/VariableStructure.xsl" />
    <xsl:import href="../../xslt/Types/Trees/TreeAttributes.xsl" />
    <xsl:import href="../../xslt/Types/Trees/TreeBaseTypes.xsl" />
    <xsl:import href="../../xslt/Types/Trees/TreeDataClassTypes.xsl" />
    <xsl:import href="../../xslt/Types/Trees/TreeSetTypes.xsl" />

    <xsl:output method="html" indent="yes" />

  <xsl:variable name="apos">'</xsl:variable>

  <xsl:param name="root" />
  <xsl:param name="showGroupsInTree" select="false()" />
  <xsl:param name="showDataclassInstancesInTree" select="true()" />
  <xsl:param name="showSetsInTree" select="false()" />
  <xsl:param name="maxDepth" />
  <xsl:param name="statisticianMode" />
  <xsl:param name="crfName" />
  <xsl:param name="crfVersion" />
</xsl:stylesheet>

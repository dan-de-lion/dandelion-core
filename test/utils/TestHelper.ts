import * as fs from 'node:fs';
import path from 'path';
import { afterEach, beforeAll, beforeEach } from 'vitest';
import 'reflect-metadata';
import { enableAutoUnmount, mount } from '@vue/test-utils';
import { addContext, ContainerTypes, DynamicServiceInfo, getContainer, ServiceInfo } from '@/ContainersConfig';
import { PathToXsl, setupDandelion } from '@/index';
import { CaseMetaData } from '@/entities/CaseMetaData';
import type { Version } from '@/entities/CRF';
import { PartialStoredCase } from '@/entities/PartialStoredCase';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
import { CrfServiceFromFolderForTests } from '../mock-services/CrfServiceFromFolderForTests';
import { MockCRFVersionSelectionStrategy } from '../mock-services/MockCRFVersionSelectionStrategy';
import { UserService } from '../mock-services/UserService';
import type { CRFToHTMLService } from '@/services/CRFToHTMLService';
import CaseStatusService from '@/services/CaseStatusService';
import { ConsoleLogger } from '@/services/Logger';
import { AccessLevelAndCrfConfigurations } from '@/types/AccessLevels'
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { createCustomComponentStructure } from '@/utils/customComponentStructure';
import { DataSynchStorage } from '../mock-repositories/DataSynchStorage';
import { SynchedLocalStorageEvents } from '../mock-repositories/SynchedLocalStorageForEvents';
import { InMemoryCacheStorageForTests } from '../mock-repositories/InMemoryCacheStorageForTests';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
import { InMemoryEventsStorage } from '@/storages/InMemoryEventsStorage';
import { InMemoryKeyStorage } from '@/storages/InMemoryKeyStorage';
import { LocalCaseStorage } from '@/storages/LocalCaseStorage';
import { LocalStorageForEvents } from '@/storages/LocalStorageForEvents';
import { LocalKeyStorage } from '@/storages/LocalKeyStorage';
import { RESTCaseStorage } from '@/storages/RESTCaseStorage';
import { RESTStorageForEvents } from '@/storages/RESTStorageForEvents';
import { RESTKeyStorage } from '@/storages/RESTKeyStorage';
import PluginComuniForTest from '../componentsWrapper/PluginComuniForTest.vue';
import { ActivationPeriodsForCrfStorageForTests } from '../mock-repositories/ActivationPeriodsForCrfStorageForTests';

export default class TestHelper {
  static async mountComponentForTests(
    xml: string,
    pathString: string,
    crfName: string,
    crfVersion: Version,
    showGroupsInTree?: boolean,
    showDataclassInstancesInTree?: boolean,
    showSetsInTree?: boolean,
    maxDepth?: number,
    model: any = undefined,
    helperFunctionsContent = '',
    context = Contexts.TEST,
    startingDataclass = '',
    prefixForID = '',
    containerType = ContainerTypes.DEFAULT,
    useFullCrfRoot = false) {

    const container = getContainer(context, containerType);
    const crfServiceFromFolder = container.get(NamedBindings.CRFService) as CrfServiceFromFolderForTests;
    crfServiceFromFolder.getCrfNameFromVariable = () => crfName;

    const crfToHTMLService: CRFToHTMLService = container.get(NamedBindings.CRFToHTMLService);
    const html = await crfToHTMLService.getHTMLfromXML(
      xml,
      crfName,
      crfVersion,
      `public/xslt/${pathString}`,
      showGroupsInTree,
      showDataclassInstancesInTree,
      showSetsInTree,
      maxDepth,
      startingDataclass,
      useFullCrfRoot ? `model.${crfName}` : `model`,
      prefixForID
    );

    // necessary
    const helperFunctions = await import(`data:text/javascript,${helperFunctionsContent}`);

    const myModel = model ?? (containerType === ContainerTypes.NEW_CASE
      ? (container.get(NamedBindings.CurrentCaseServiceForNewCase) as CurrentCaseServiceInterface).getModel()
      : (container.get(NamedBindings.CurrentCaseService) as CurrentCaseServiceInterface).getModel());

    const customComponent = createCustomComponentStructure(html, myModel, helperFunctions, containerType, context);
    return mount(customComponent);
  }

  static requireFile(path: string) {
    return fs.readFileSync(`./test/fixtures/xml${path}`).toString();
  }

  static requireFileJSON(path: string) {
    return fs.readFileSync(`./test/fixtures/json${path}`).toString();
  }

  static setupContainer(caseName = 'Admission', cachedVariables: string[] = [], storageBindings: BindingStorages = [],
    accessLevelAndCrfConfigurations: AccessLevelAndCrfConfigurations = [{ accessLevel: "*", crfName: "*" }]) {
    setupDandelion({
      coreConfiguration: new CoreConfiguration({
        baseUrlForXslt: 'public/xslt/',
        encrypt: false,
        debug: false,
        accessLevelAndCrfConfigurations,
        listOfCaseTypes: [new CaseConfiguration({
          caseName,
          caseLinkingConfiguration: [{ caseTypeName: 'Patient', caseIDVariable: 'admission.patientId' }],
          mainCrf: { crfName: caseName },
          referenceDateVariableName: 'model.referenceDate',
          cachedVariables
        })],
        pluggableDataTypes: { comuni: PluginComuniForTest },
        debounceForUpdatingStatus: 0
      }),
      sharedServices: [
        { name: NamedBindings.Logger, class: ConsoleLogger },
        { name: NamedBindings.CRFService, class: CrfServiceFromFolderForTests },
        { name: NamedBindings.DataSynchStorage, class: DataSynchStorage },
        { name: NamedBindings.StorageForSynchEvents, class: SynchedLocalStorageEvents },
        { name: NamedBindings.VariablesCacheStorage, class: InMemoryCacheStorageForTests },
        { name: NamedBindings.UserService, class: UserService },
      ],
      services: [{
        name: NamedBindings.CRFVersionSelectionStrategy, instance: () => new MockCRFVersionSelectionStrategy(caseName),
        containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.NEW_CASE, ContainerTypes.REEVALUATION],
      },
      {
        name: NamedBindings.ActivationPeriodForCentreCodeStorage, class: ActivationPeriodsForCrfStorageForTests,
        containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION]
      },
      ...storageBindings
      ],
    });
    addContext(Contexts.TEST);
    if (storageBindings.length == 0) {
      getContainer(Contexts.SHARED).bind(NamedBindings.CaseStorage).toConstantValue(new InMemoryCaseStorage());
      getContainer(Contexts.SHARED).bind(NamedBindings.KeyStorage).toConstantValue(new InMemoryKeyStorage());
      getContainer(Contexts.SHARED).bind(NamedBindings.StorageForEvents).toConstantValue(new InMemoryEventsStorage());
    }
  }

  static htmlStringToFragment(htmlString: string): DocumentFragment {
    // generate a document and an element
    const doc = new DOMParser().parseFromString(htmlString, 'text/html');
    const element = doc.createElement('section');
    element.innerHTML = htmlString;
    // We extract content of the template in the first DataClassItemComponent
    return element.querySelector('template')!.content;
  }

  static mockStatusService(context: string) {
    const caseStatusService = getContainer(context).get(NamedBindings.CaseStatusService) as CaseStatusService;
    caseStatusService.calculateStatus = () => 0;
    caseStatusService.registerVariable = () => { };
    caseStatusService.updateValue = () => { };
    const crfService = getContainer(context).get(NamedBindings.CRFService) as CrfServiceFromFolderForTests;
    crfService.getAccessLevelForVariable = () => "public";
  }

  static testSetupWithCaseOpening(useStandardCaseStatusService?: boolean) {
    beforeAll(() => {
      TestHelper.setupContainer();
      if (!useStandardCaseStatusService) TestHelper.mockStatusService(Contexts.TEST);
      getContainer(Contexts.SHARED).rebind(NamedBindings.CaseStorage).toConstantValue(getFakeCaseStorage);
    });

    beforeEach(async () => {
      const currentCaseService = getContainer(Contexts.TEST).get(NamedBindings.CurrentCaseService) as CurrentCaseServiceInterface;
      await currentCaseService.open('12345', {});
    });

    afterEach(async () => {
      const currentCaseService = getContainer(Contexts.TEST).get(NamedBindings.CurrentCaseService) as CurrentCaseServiceInterface;
      await currentCaseService.terminateEditing();
    });
    enableAutoUnmount(afterEach);
  }

  static async mountStandardCollectionComponent(xmlPath: string) {
    const xml = TestHelper.requireFile(xmlPath);
    return await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, path.parse(xmlPath).name, '0.0.1');
  }
}

export function sleep(seconds = 1) {
  return new Promise((resolve) => setTimeout(resolve, 1000 * seconds));
}

export type BindingStorages = Array<ServiceInfo<any> | DynamicServiceInfo<any>>;
export type StorageConfiguration = { name: string, bindings: BindingStorages }

export const storageConfigurations: Array<StorageConfiguration> =
  [{
    name: 'InMemoryStorages',
    bindings: [
      { name: NamedBindings.CaseStorage, class: InMemoryCaseStorage, containerTypes: [ContainerTypes.DEFAULT] },
      { name: NamedBindings.StorageForEvents, class: InMemoryEventsStorage, containerTypes: [ContainerTypes.DEFAULT] },
      { name: NamedBindings.KeyStorage, class: InMemoryKeyStorage, containerTypes: [ContainerTypes.DEFAULT] }]
  },
  {
    name: 'LocalStorages',
    bindings: [
      { name: NamedBindings.CaseStorage, class: LocalCaseStorage, containerTypes: [ContainerTypes.DEFAULT] },
      { name: NamedBindings.StorageForEvents, class: LocalStorageForEvents, containerTypes: [ContainerTypes.DEFAULT] },
      { name: NamedBindings.KeyStorage, class: LocalKeyStorage, containerTypes: [ContainerTypes.DEFAULT] }]
  },
  /*{
    name: 'RESTStorages',
    bindings: [
      { name: NamedBindings.CaseStorage, class: RESTCaseStorage, containerTypes: [ContainerTypes.DEFAULT] },
      { name: NamedBindings.StorageForEvents, class: RESTStorageForEvents, containerTypes: [ContainerTypes.DEFAULT] },
      { name: NamedBindings.KeyStorage, class: RESTKeyStorage, containerTypes: [ContainerTypes.DEFAULT] }]
  }*/];

export const getFakeCaseStorage = {
  getPartials: (_: string) => [{ data: '{}', accessLevel: 'public' }] as PartialStoredCase[],
  getCaseMetaData: (caseID: string) => ({ caseID, status: new Map() } as CaseMetaData),
  lock: (_caseID: string) => { }, // fake method only for test
  unlock: (_caseID: string) => { } // fake method only for test
};
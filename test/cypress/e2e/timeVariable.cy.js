import '../../../public/frameless-xslt2.js';

describe('XSLT Time Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-time-variable.xml', () => {
    cy.readFile('test/fixtures/xml/timeVariable/standard-time-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .should('not.have.attr', 'v-if', 'false')
          .should('have.attr', ':id', "'model.TimeVariable-wrapper'");

        // test standard attributes
        cy.get('input')
          .should('have.attr', 'type', 'time')
          .should('have.attr', 'v-custom-model', "'model.TimeVariable'");

        // test the no-defined-label case
        cy.get('label').should('have.text', "{{ 'TimeVariable' }}");
      }
    );
  });

  it('should have correct v-if attribute set', () => {
    cy.readFile('test/fixtures/xml/timeVariable/not-visible-time-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.state('document').write(html);

        // test that it should not be visible, it should have a v-if statement with x == 4
        cy.get('div').should('have.attr', 'v-if', 'x == 4');
      }
    );
  });

  it('should have a custom label', () => {
    cy.readFile('test/fixtures/xml/timeVariable/custom-label-time-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test the custom label
        cy.get('label').should('have.text', "{{ 'testLabel' }}");
      }
    );
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/timeVariable/group-time-variable.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('input')
        .should('have.attr', 'v-custom-model', "'model.Group.TimeVariable'")
        .should('have.attr', ':id', "'model.Group.TimeVariable'");
    });
  });

  it('should have computedFormula attributes', () => {
    cy.readFile('test/fixtures/xml/timeVariable/computedformula-time-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the 2nd input have computed formula
        cy.get('div>input').eq(1).should('have.attr', 'v-short-circuit', 'model.TimeVariable');
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

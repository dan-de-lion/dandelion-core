import '../../../public/frameless-xslt2.js';

describe('XSLT Singlechoice Dropdown Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-singlechoice-dropdown--variable.xml', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceDropdownVariable/standard-singlechoice-dropdown-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div').should('not.have.attr', 'v-if', 'false');

      // test standard attributes
      cy.get('select')
        .should('have.attr', 'v-custom-model', "'model.dropVariable'")
        // test if the number of <li> is equals at xml valuesSet + 1, because there is an option empty
        .find('option')
        .should('have.length', 4);

      //Test that all options have correct value attributes
      cy.get('select>option').eq(0).should('have.attr', 'value', '');
      cy.get('select>option').eq(1).should('have.attr', 'value', 'sets.first');
      cy.get('select>option').eq(2).should('have.attr', 'value', 'sets.second');
      cy.get('select>option').eq(3).should('have.attr', 'value', 'sets.third');
    });
  });

  it('should have correct v-if attribute set', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceDropdownVariable/not-visible-singlechoice-dropdown-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div').should('have.attr', 'v-if', 'false');
    });
  });

  it('should have a custom label', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceDropdownVariable/custom-label-singlechoice-dropdown-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      cy.get('label').should('exist');
    });
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceDropdownVariable/group-singlechoice-dropdown-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('select').should('have.attr', 'v-custom-model', "'model.Group.dropVariable'");
    });
  });

  it('should have computedFormula attributes', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceDropdownVariable/computedformula-singlechoice-dropdown-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('select').should('have.attr', 'v-short-circuit', 'sets.second');
    });
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

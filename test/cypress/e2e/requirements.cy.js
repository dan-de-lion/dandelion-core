import '../../../public/frameless-xslt2.js';

describe('Requirements', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from text-variable-requirement.xml', () => {
    cy.readFile('test/fixtures/xml/requirements/text-variable-requirement.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(2)
          .should('have.attr', 'class', 'requirement')
          .should(
            'have.attr',
            'v-if',
            '!(model.TextVariable?.length > model.TextVariable2?.length)'
          )
          .should('have.attr', ':id', "'model.REQUIREMENT_text'");
      }
    );
  });

  it('should create the correct html from color-variable-requirement.xml', () => {
    cy.readFile('test/fixtures/xml/requirements/color-variable-requirement.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(1)
          .should('have.attr', 'class', 'requirement')
          .should('have.attr', 'v-if', "!(model.ColorVariable == '#00f900')")
          .should('have.attr', ':id', "'model.REQUIREMENT_color'");
      }
    );
  });

  it('should create the correct html from boolean-variable-requirement.xml', () => {
    cy.readFile('test/fixtures/xml/requirements/boolean-variable-requirement.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(1)
          .should('have.attr', 'class', 'requirement')
          .should('have.attr', 'v-if', '!(model.BooleanVariable == true)')
          .should('have.attr', ':id', "'model.REQUIREMENT_boolean'");
      }
    );
  });

  it('should create the correct html from date-variable-requirement.xml', () => {
    cy.readFile('test/fixtures/xml/requirements/date-variable-requirement.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(1)
          .should('have.attr', 'class', 'requirement')
          .should('have.attr', 'v-if', '!(Date.parse(model.DateVariable) > Date.now())')
          .should('have.attr', ':id', "'model.REQUIREMENT_date'");
      }
    );
  });

  it('should create the correct html from multiplechoice-variable-requirement.xml', () => {
    cy.readFile('test/fixtures/xml/requirements/multiplechoice-variable-requirement.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(1)
          .should('have.attr', 'class', 'requirement')
          .should('have.attr', 'v-if', '!(model.MultiplechoiceVariable?.length > 2)')
          .should('have.attr', ':id', "'model.REQUIREMENT_multiplechoice'");
      }
    );
  });

  it('should create the correct html from singlechoice-variable-requirement.xml', () => {
    cy.readFile('test/fixtures/xml/requirements/singlechoice-variable-requirement.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(2)
          .should('have.attr', 'class', 'requirement')
          .should('have.attr', 'v-if', "!(model.SinglechoiceVariable =='sets.first')")
          .should('have.attr', ':id', "'model.REQUIREMENT_singlechoice'");
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

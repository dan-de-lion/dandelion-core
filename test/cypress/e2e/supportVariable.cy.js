import '../../../public/frameless-xslt2.js';

describe('XSLT Support Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-support-variable.xml', () => {
    cy.readFile('test/fixtures/xml/supportVariable/standard-support-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be display: none
        cy.get('div').should('have.attr', 'style', 'display:none;');

        // test standard attributes
        cy.get('input')
          .should('have.attr', 'type', 'support')
          .should('have.attr', 'v-custom-model', "'model.SupportVariable'");

        // test the no-defined-label case
        cy.get('label').should('exist');
      }
    );
  });

  it('should have a custom label', () => {
    cy.readFile('test/fixtures/xml/supportVariable/custom-label-support-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test the custom label
        cy.get('label').should('exist');
      }
    );
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/supportVariable/group-support-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input')
          .should('have.attr', 'v-custom-model', "'model.Group.SupportVariable'")
          .should('have.attr', ':id', "'model.Group.SupportVariable'");
      }
    );
  });

  it('should have computedFormula attributes', () => {
    cy.readFile('test/fixtures/xml/supportVariable/computedformula-support-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input').should('have.attr', 'v-short-circuit', '8');
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

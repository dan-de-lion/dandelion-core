import '../../../public/frameless-xslt2.js';

describe('XSLT Color Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-color-variable.xml', () => {
    cy.readFile('test/fixtures/xml/colorVariable/standard-color-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div').should('not.have.attr', 'v-if', 'false');

        // test standard attributes
        cy.get('input')
          .should('have.attr', 'type', 'color')
          .should('have.attr', 'v-custom-model', "'model.ColorVariable'");

        // test the no-defined-label case
        cy.get('label').should('exist');
      }
    );
  });

  it('should have correct v-if attribute set', () => {
    cy.readFile('test/fixtures/xml/colorVariable/not-visible-color-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should not be visible
        cy.get('div').should('have.attr', 'v-if', 'false');
      }
    );
  });

  it('should have a custom label', () => {
    cy.readFile('test/fixtures/xml/colorVariable/custom-label-color-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test the custom label
        cy.get('label').should('exist');
      }
    );
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/colorVariable/group-color-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input')
          .should('have.attr', 'v-custom-model', "'model.Group.ColorVariable'")
          .should('have.attr', ':id', "'model.Group.ColorVariable'");
      }
    );
  });

  it('should have computedFormula attributes', () => {
    cy.readFile('test/fixtures/xml/colorVariable/computedformula-color-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input').should('have.attr', 'v-short-circuit', "'#669c35'");
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

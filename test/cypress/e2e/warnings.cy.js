import '../../../public/frameless-xslt2.js';

describe('Warnings', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from text-variable-warning.xml', () => {
    cy.readFile('test/fixtures/xml/warnings/text-variable-warning.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(2)
        .should('have.attr', 'v-if', 'model.TextVariable?.length > model.TextVariable2?.length')
        .should('have.attr', ':id', "'model.WARNING_MyWarningTest_CONFIRMED'");

      cy.get('input')
        .eq(2)
        .should(
          'have.attr',
          'v-custom-model-for-boolean',
          "'model.WARNING_MyWarningTest_CONFIRMED'"
        );
    });
  });

  it('should create the correct html from boolean-variable-warning.xml', () => {
    cy.readFile('test/fixtures/xml/warnings/boolean-variable-warning.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(1)
        .should('have.attr', 'v-if', 'model.BooleanVariable == true')
        .should('have.attr', ':id', "'model.WARNING_MyWarningTest_CONFIRMED'");

      cy.get('input')
        .eq(1)
        .should(
          'have.attr',
          'v-custom-model-for-boolean',
          "'model.WARNING_MyWarningTest_CONFIRMED'"
        );
    });
  });

  it('should create the correct html from color-variable-warning.xml', () => {
    cy.readFile('test/fixtures/xml/warnings/color-variable-warning.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(1)
        .should('have.attr', 'v-if', "model.ColorVariable == '#00f900'")
        .should('have.attr', ':id', "'model.WARNING_MyWarningTest_CONFIRMED'");

      cy.get('input')
        .eq(1)
        .should(
          'have.attr',
          'v-custom-model-for-boolean',
          "'model.WARNING_MyWarningTest_CONFIRMED'"
        );
    });
  });

  it('should create the correct html from date-variable-warning.xml', () => {
    cy.readFile('test/fixtures/xml/warnings/date-variable-warning.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(1)
        .should('have.attr', 'v-if', 'Date.parse(model.DateVariable) > Date.now()')
        .should('have.attr', ':id', "'model.WARNING_MyWarningTest_CONFIRMED'");

      cy.get('input')
        .eq(1)
        .should(
          'have.attr',
          'v-custom-model-for-boolean',
          "'model.WARNING_MyWarningTest_CONFIRMED'"
        );
    });
  });

  it('should create the correct html from multiplechoice-variable-warning.xml', () => {
    cy.readFile('test/fixtures/xml/warnings/multiplechoice-variable-warning.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(1)
          .should('have.attr', 'v-if', 'model.MultiplechoiceVariable?.length > 2')
          .should('have.attr', ':id', "'model.WARNING_MyWarningTest_CONFIRMED'");

        cy.get('input')
          .eq(3)
          .should(
            'have.attr',
            'v-custom-model-for-boolean',
            "'model.WARNING_MyWarningTest_CONFIRMED'"
          );
      }
    );
  });

  it('should create the correct html from singlechoice-variable-warning.xml', () => {
    cy.readFile('test/fixtures/xml/warnings/singlechoice-variable-warning.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(2)
          .should('have.attr', 'v-if', "model.SinglechoiceVariable == 'sets.first'")
          .should('have.attr', ':id', "'model.WARNING_MyWarningTest_CONFIRMED'");

        cy.get('input')
          .eq(6)
          .should(
            'have.attr',
            'v-custom-model-for-boolean',
            "'model.WARNING_MyWarningTest_CONFIRMED'"
          );
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

import '../../../public/frameless-xslt2.js';

describe('Errors', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from text-variable-error.xml', () => {
    cy.readFile('test/fixtures/xml/errors/text-variable-error.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(2)
        .should('have.attr', 'class', 'error')
        .should('have.attr', 'v-if', 'model.TextVariable?.length > model.TextVariable2?.length')
        .should('have.attr', ':id', "'model.ERROR_MyTestError'");
    });
  });

  it('should create the correct html from color-variable-error.xml', () => {
    cy.readFile('test/fixtures/xml/errors/color-variable-error.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(1)
        .should('have.attr', 'class', 'error')
        .should('have.attr', 'v-if', "model.ColorVariable == '#00f900'")
        .should('have.attr', ':id', "'model.ERROR_MyTestError'");
    });
  });

  it('should create the correct html from boolean-variable-error.xml', () => {
    cy.readFile('test/fixtures/xml/errors/boolean-variable-error.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(1)
        .should('have.attr', 'class', 'error')
        .should('have.attr', 'v-if', 'model.BooleanVariable == true')
        .should('have.attr', ':id', "'model.ERROR_MyTestError'");
    });
  });

  it('should create the correct html from date-variable-error.xml', () => {
    cy.readFile('test/fixtures/xml/errors/date-variable-error.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div')
        .eq(1)
        .should('have.attr', 'class', 'error')
        .should('have.attr', 'v-if', 'Date.parse(model.DateVariable) > Date.now()')
        .should('have.attr', ':id', "'model.ERROR_MyTestError'");
    });
  });

  it('should create the correct html from multiplechoice-variable-error.xml', () => {
    cy.readFile('test/fixtures/xml/errors/multiplechoice-variable-error.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(1)
          .should('have.attr', 'class', 'error')
          .should('have.attr', 'v-if', 'model.MultiplechoiceVariable?.length > 2')
          .should('have.attr', ':id', "'model.ERROR_MyTestError'");
      }
    );
  });

  it('should create the correct html from singlechoice-variable-error.xml', () => {
    cy.readFile('test/fixtures/xml/errors/singlechoice-variable-error.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .eq(2)
          .should('have.attr', 'class', 'error')
          .should('have.attr', 'v-if', "model.SinglechoiceVariable == 'sets.first'")
          .should('have.attr', ':id', "'model.ERROR_MyTestError'");
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

import '../../../public/frameless-xslt2.js';

describe('XSLT Date Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-date-variable.xml', () => {
    cy.readFile('test/fixtures/xml/dateVariable/standard-date-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div')
          .should('not.have.attr', 'v-if', 'false')
          .should('have.attr', ':id', "'model.startingDate-wrapper'");

        // test standard attributes
        cy.get('input')
          .should('have.attr', 'type', 'date')
          .should('have.attr', 'max', "{{ Date.now() | date:'yyyy-MM-dd' }}")
          .should('have.attr', 'min', '2000-01-01')
          .should('have.attr', 'v-custom-model', "'model.startingDate'");

        // test the no-defined-label case
        cy.get('label').should('have.text', "{{ 'startingDate' }}");
      }
    );
  });

  it('should have correct v-if attribute set', () => {
    cy.readFile('test/fixtures/xml/dateVariable/not-visible-date-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should not be visible, it should have a v-if statement with x == 4
        cy.get('div').should('have.attr', 'v-if', 'x == 4');
      }
    );
  });

  it('should have a custom label', () => {
    cy.readFile('test/fixtures/xml/dateVariable/custom-label-date-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        cy.get('label').should('have.text', "{{ 'dateCustomLabel' }}");
      }
    );
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/dateVariable/group-date-variable.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('input')
        .should('have.attr', 'v-custom-model', "'model.Group.DateVariable'")
        .should('have.attr', ':id', "'model.Group.DateVariable'");
    });
  });

  it('should have computedFormula attributes', () => {
    cy.readFile('test/fixtures/xml/dateVariable/computedformula-date-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the 2nd input have computed formula
        cy.get('div>input').eq(1).should('have.attr', 'v-short-circuit', 'model.DateVariable');
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

import '../../../public/frameless-xslt2.js';

describe('XSLT Set Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-set-variable.xml', () => {
    cy.readFile('test/fixtures/xml/setVariable/standard-set-variable.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.log(html);
      cy.state('document').write(html);

      // test that it should be display: none
      cy.get('fieldset').should('have.class', 'set');

      cy.get('ul').should('have.attr', ':id', "'model.SetVariable'");

      // external button
      cy.get('button')
        .eq(1)
        .should('have.attr', 'v-on:click', 'addElementToSet(model.SetVariable)')
        .should('exist');

      // internal button for each <li>
      cy.get('ul').get('li').get('button').should('exist');

      // should have a legend tag
      cy.get('legend').should('exist');

      // should the <li> cycle on rigth element
      cy.get('ul')
        .get('li')
        .should('have.attr', 'v-for', '(childElement, index) in model.SetVariable')
        .should('have.attr', 'track-by', 'childelement.guid');
    });
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/setVariable/group-set-variable.xml').then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('ul').should('have.attr', ':id', "'model.Group.SetVariable'");
    });
  });

  it('should have computedFormula attributes', () => {
    cy.readFile('test/fixtures/xml/setVariable/computedformula-set-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('ul').should('have.attr', 'v-short-circuit', 'test');
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

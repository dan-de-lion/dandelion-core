import '../../../public/frameless-xslt2.js';

describe('XSLT Singlechoice Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-singlechoice-variable.xml', () => {
    cy.readFile('test/fixtures/xml/singlechoiceVariable/standard-singlechoice-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should have an <ul>
        cy.get('ul')
          .should('have.attr', 'class', 'radiolist')
          // test if the number of <li> is equals at xml valuesSet
          .find('li')
          .should('have.length', 3);

        // test that it should be visible
        cy.get('div').should('not.have.attr', 'v-if', 'false');

        // test standard attributes
        cy.get('input')
          .should('have.attr', 'type', 'radio')
          .should('have.attr', 'value', 'sets.first');

        //Test that all radio inputs have the same name and the same v-custom-model
        cy.get('li>input').each((element, index, list) => {
          cy.wrap(element).should('have.attr', ':name', "'model.SinglechoiceVariable'");
        });

        //Test that all radio inputs have correct value attributes
        cy.get('li>input').eq(0).should('have.attr', 'value', 'sets.first');
        cy.get('li>input').eq(1).should('have.attr', 'value', 'sets.second');
        cy.get('li>input').eq(2).should('have.attr', 'value', 'sets.third');
      }
    );
  });

  it('should have correct v-if attribute set', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceVariable/not-visible-singlechoice-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);

      cy.state('document').write(html);

      // test that it should be visible
      cy.get('div').should('have.attr', 'v-if', 'false');
    });
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/singlechoiceVariable/group-singlechoice-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input').should('have.attr', ':id', "'model.Group.SinglechoiceVariable-sets.first'");
      }
    );
  });

  it('should have computedFormula attributes', () => {
    cy.readFile(
      'test/fixtures/xml/singlechoiceVariable/computedformula-singlechoice-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('ul').should('have.attr', 'v-short-circuit', 'sets.second');
    });
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

import '../../../public/frameless-xslt2.js';

describe('XSLT Group Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-group-variable.xml', () => {
    cy.readFile('test/fixtures/xml/groupVariable/standard-group-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div').should('not.have.attr', 'v-if', 'false');

        cy.get('fieldset').should('exist');
      }
    );
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

import '../../../public/frameless-xslt2.js';

describe('XSLT Boolean Variable', () => {
  afterEach(() => {
    cy.reload();
  });

  it('should create the correct html from standard-boolean-variable.xml', () => {
    cy.readFile('test/fixtures/xml/booleanVariable/standard-boolean-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.log(html);
        cy.state('document').write(html);

        // test that it should be visible
        cy.get('div').should('not.have.attr', 'v-if', 'false');

        // test standard attributes
        cy.get('input')
          .should('have.attr', 'type', 'checkbox')
          .should('have.attr', 'v-custom-model-for-boolean', "'model.BooleanVariable'");

        // test the no-defined-label case
        cy.get('label').should('have.text', "{{ 'BooleanVariable' }}");
      }
    );
  });

  it('should have correct v-if attribute set', () => {
    cy.readFile('test/fixtures/xml/booleanVariable/not-visible-boolean-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);

        cy.state('document').write(html);

        // test that it should not be visible
        cy.get('div').should('have.attr', 'v-if', 'false');
      }
    );
  });

  it('should have a custom label', () => {
    cy.readFile('test/fixtures/xml/booleanVariable/custom-label-boolean-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test the custom label
        cy.get('label').should('exist');
      }
    );
  });

  it('should be correctly placed in a variable group', () => {
    cy.readFile('test/fixtures/xml/booleanVariable/group-boolean-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input')
          .should('have.attr', 'v-custom-model-for-boolean', "'model.Group.BooleanVariable'")
          .should('have.attr', ':id', "'model.Group.BooleanVariable'");
      }
    );
  });

  it('should have computedFormula true attribute', () => {
    cy.readFile('test/fixtures/xml/booleanVariable/computedformula-true-boolean-variable.xml').then(
      async (xmlText) => {
        const html = await getHTMLFromXMl(xmlText);
        cy.log(html);
        cy.state('document').write(html);

        // test that the input should be present
        cy.get('input').should('have.attr', 'v-short-circuit', 'true');
      }
    );
  });

  it('should have computedFormula false attributes', () => {
    cy.readFile(
      'test/fixtures/xml/booleanVariable/computedformula-false-boolean-variable.xml'
    ).then(async (xmlText) => {
      const html = await getHTMLFromXMl(xmlText);
      cy.log(html);
      cy.state('document').write(html);

      // test that the input should be present
      cy.get('input').should('have.attr', 'v-short-circuit', 'false');
    });
  });
});

async function getHTMLFromXMl(xmlText) {
  const processor = new XSLT2Processor();
  const parser = new DOMParser();

  await processor.importStylesheetURI('/xslt/Schemas/schemaToCollectionPage.xsl');
  const xmlDoc = parser.parseFromString(xmlText, 'text/xml');
  const objectByTagName = xmlDoc.getElementsByTagName('dataClass');

  const fragment = processor.transformToFragment(objectByTagName[0], document);

  const div = document.createElement('div');
  div.appendChild(fragment.cloneNode(true));

  return div.innerHTML;
}

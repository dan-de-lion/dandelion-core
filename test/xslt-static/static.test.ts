import { describe, test } from 'vitest';
import '../../public/frameless-xslt2';
import { CRFError, CRFErrorType } from '@/errors/CRFError';

// @ts-ignore
const processor = new XSLT2Processor();
processor.setParameter(null, 'root', 'model');

const parser = new DOMParser();
await processor.importStylesheetURI('public/xslt/Schemas/SchemaToCollectionPage.xsl');
processor.setParameter(null, 'maxDepth', 500);
processor.setParameter(null, 'statisticianMode', 'false');

async function getHTML(xml: string, fileName: string) {
  const xmlDoc = parser.parseFromString(xml, 'text/xml');
  const startingDataClass = '';

  const objectToTransform =
    startingDataClass === ''
      ? xmlDoc.getElementsByTagName('dataClass')
      : xmlDoc.querySelectorAll(`dataClass[name='${startingDataClass}']`);
  if (objectToTransform.length < 1 || !objectToTransform[0]) {
    throw new CRFError(`Starting Dataclass absent in crf`, CRFErrorType.STARTING_DATACLASS_NOT_FOUND);
  }

  const fragment = processor.transformToFragment(objectToTransform[0], document);
  if (!fragment?.firstChild) {
    return '';
  }
  let resultHTML = '';

  for (const i in fragment.childNodes) {
    // eslint-disable-next-line no-prototype-builtins
    if (fragment.childNodes.hasOwnProperty(i)) {
      resultHTML += (fragment.childNodes[i] as HTMLElement).outerHTML
        .replace(/&amp;/g, '&')
        .replace(/my-custom-template/g, 'template');
    }
  }
  return { resultHTML, fileName };
}

function formatHTML(html: string) {
  const tab = '\t';
  let result = '';
  let indent = '';

  html.split(/>\s*</).forEach((element) => {
    if (element.match(/^\/\w/)) {
      indent = indent.substring(tab.length);
    }
    result += `${indent}<${element}>\n`;

    if (element.match(/^<?\w[^>]*[^/]$/) && !element.startsWith('input')) {
      indent += tab;
    }
  });
  return result.substring(1, result.length - 2);
}

describe('Test XSLT transformation works correctly', () => {
  test('create all the static html output', async () => {
    const fs = require('fs');
    const files = await Promise.all(
      fs
        .readdirSync('./test/xslt-static/xml')
        // filter only xml files
        .filter((file: string) => file.endsWith('.xml'))
        .map((file: string) => {
          const xml_file = fs.readFileSync(`./test/xslt-static/xml/${file}`).toString();
          return getHTML(xml_file, file);
        })
    );
    files.forEach(({ resultHTML, fileName }) => {
      const fileNameWithoutExtension = fileName.slice(0, -4);
      fs.writeFileSync(`./test/xslt-static/html/${fileNameWithoutExtension}.html`, formatHTML(resultHTML));
    });
  });
  // long test, adding test timeout
  test('create all the fixtures html output', async () => {
    const fs = require('fs');
    const files = await Promise.all(
      fs
        // read all the files in all the subdirectories
        .readdirSync('./test/fixtures/xml', { withFileTypes: true })
        // filter only directories
        .filter((dirent: any) => dirent.isDirectory())
        // get the path of the directory
        .map((dirent: any) => dirent.name)
        // for each directory, create  a directory in the fixtures folder for the html files (needed for the writeFileSync)
        .map((directory: string) => {
          fs.mkdirSync(`./test/xslt-static/html/fixtures/${directory}`, { recursive: true });
          return directory;
        })
        // get all the files in the directory and save the directory name
        .map((directory: string) => ({
          files: fs.readdirSync(`./test/fixtures/xml/${directory}`),
          directory,
        }))
        // map each filename to the directory path
        // @ts-ignore
        .map(({ files, directory }) => files.map((file: string) => `${directory}/${file}`))
        // flatten the array
        .reduce((acc: string[], val: string[]) => acc.concat(val), [])
        // filter only xml files
        .filter((file: string) => file.endsWith('.xml'))
        // get the content of the file
        .map((file: string) => {
          const xml_file = fs.readFileSync(`./test/fixtures/xml/${file}`).toString();
          return getHTML(xml_file, file);
        })
    );
    files.forEach(({ resultHTML, fileName }) => {
      const fileNameWithoutExtension = fileName.slice(0, -4);
      fs.writeFileSync(`./test/xslt-static/html/fixtures/${fileNameWithoutExtension}.html`, formatHTML(resultHTML));
    });
  }, 20000);
});

import { injectable } from 'inversify';
import { Case, CaseForUpdate } from '@/entities/Cases';
import type { CaseMetaData } from '@/entities/CaseMetaData';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { DandelionService } from '@/services/DandelionService';
import type { Motivations } from '@/utils/MapForMotivations';

@injectable()
export class MockCaseService extends DandelionService implements CaseServiceInterface {
  constructor() {
    super();
  }

  async getMultipleDataFor(caseIDs: string[]): Promise<Case[]> {
    throw new ApplicationError('Get Multiple Data for in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  getAllCaseMetaDataFor(_caseType: string): Promise<CaseMetaData[]> {
    throw new ApplicationError('Get All CaseMetaDataFor in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  freezeCase(_caseID: string, _accessLevelsToBeFroze?: string[] | undefined): Promise<void> {
    throw new ApplicationError('Freeze case in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  unfreezeCase(_caseID: string, _accessLevelsToBeFroze?: string[] | undefined): Promise<void> {
    throw new ApplicationError('Unfreeze case in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  getDataFor(_caseID: string): Promise<any> {
    throw new ApplicationError('Get Data for in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  getCaseMetaDataFor(_caseID: string): Promise<CaseMetaData> {
    throw new ApplicationError('Get caseMetaData for in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  create(_caseToCreate: Case): Promise<void> {
    throw new ApplicationError('Create in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  update(_caseForUpdate: CaseForUpdate): Promise<void> {
    throw new ApplicationError('Update in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  deleteCase(_caseID: string): Promise<void> {
    throw new ApplicationError('Delete case in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  restoreCase(_caseID: string): Promise<void> {
    throw new ApplicationError('Restore case in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  permanentlyDeleteCase(_caseID: string, _motivation: Motivations | string): Promise<void> {
    throw new ApplicationError('Permanently delete case in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  getSingleCaseHistory(_caseID: string): Promise<HistoryPartialStoredCase[]> {
    throw new ApplicationError('Get single case history in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  lock(_caseID: string): Promise<void> {
    throw new ApplicationError('Lock in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  unlock(_caseID: string): Promise<void> {
    throw new ApplicationError('Unlock in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  freeze(_caseID: string, _accessLevelsToBeFroze?: string[] | undefined): Promise<void> {
    throw new ApplicationError('Freeze in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  unfreeze(_caseID: string, _accessLevelsToBeFroze?: string[] | undefined): Promise<void> {
    throw new ApplicationError('Unfreze in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

}

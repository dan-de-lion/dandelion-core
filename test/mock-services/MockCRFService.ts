import { injectable } from 'inversify';
import 'reflect-metadata';
import { CRF, CRFVersionValidity as CRFMajorVersionValidity, type Version } from '@/entities/CRF';
import { CrfData } from '@/entities/CRFEntities';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import type { AccessLevelsTree } from '@/interfaces/AccessLevelsTree';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import { CRFConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { DandelionService } from '@/services/DandelionService';


@injectable()
export default class MockCRFService extends DandelionService implements CRFServiceInterface {
  constructor() {
    super();
  }

  public async getCRF(name: string, _version: Version): Promise<CRF> {
    return new CRF(name, '0.0.1');
  }

  public async getCRFActiveVersions(_name: string): Promise<Array<CRFMajorVersionValidity>> {
    return [new CRFMajorVersionValidity(0, '0.0.1', new Date())];
  }

  getAccessLevelsForCRF(crfName: string, crfVersion: Version): AccessLevelsTree {
    return new CRF(crfName, crfVersion).accessLevels;
  }

  async getLatestVersion(_crfName: string): Promise<Version> {
    return '0.0.1';
  }

  getMultipleXmlData(_crfs: CRFConfigurationInterface[]): Promise<CrfData> {
    throw new Error('Method not implemented.');
  }

  getXmlData(_crfs: Array<CRFConfigurationInterface>): Promise<CrfData> {
    throw new ApplicationError('Get Xml Data in MockCaseService', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  isCRFPath(url: string) {
    return (
      ((url.endsWith('.css') && url.includes('_style_')) ||
        (url.endsWith('.js') && url.includes('_functions_')) ||
        (url.endsWith('.json') && url.includes('_translations_'))) &&
      url.includes('/examples/crfs')
    );
  }

  getAccessLevelForVariable(variableName: string, crfVersions: Map<string, `${number}.${number}.${number}`>): string {
    return "";
  }

  getCrfNameFromVariable(fullName: string): string {
    return ""
  }
}

import { inject, injectable } from 'inversify';
import TestHelper from '../utils/TestHelper';
import { CRF, CRFVersionValidity, type Version } from '@/entities/CRF';
import type { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { ConsoleLogger } from '@/services/Logger';
import { CrfServiceFromFolder } from '@/storages/CrfServiceFromFolder';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export class CrfServiceFromFolderForTests extends CrfServiceFromFolder {
  constructor(@inject(NamedBindings.Logger) protected logger: ConsoleLogger,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface,
    protected pathToFolder: string = '/examples/crfs', protected accessLevels = {}) {
    super(logger, coreConfiguration);
  }

  async getCRFActiveVersions(crfName: string): Promise<CRFVersionValidity[]> {
    const versions = TestHelper.requireFileJSON(`/${crfName}/CRF_validity.json`);
    return this.validateCRFVersions(JSON.parse(versions))
  }

  async getCRF(crfName: string, crfVersion: Version): Promise<CRF> {
    return new CRF(crfName, crfVersion);
  }

  async getLatestVersion(_crfName: string): Promise<Version> {
    return '0.0.1';
  }

  getAccessLevelsForCRF(crfName: string, _crfVersion: Version) {
    return crfName === 'Admission' ? this.accessLevels : {};
  }
}

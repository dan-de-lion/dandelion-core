import { injectable } from 'inversify';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { Version } from '@/entities/CRF';
import type { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import { DandelionService } from '@/services/DandelionService';
import { CRFConfigurationInterface } from '@/interfaces/CoreConfiguration';

@injectable()
export class MockCRFVersionSelectionStrategy extends DandelionService implements CRFVersionSelectionStrategyInterface {
  constructor(private crfName: string = 'Admission') {
    super();
  }

  getUpdatedCRFVersionsByCase(_model: any, _caseMetaData: CaseMetaData): Promise<Map<string, Version>> {
    return this.getUpdatedCRFVersionsByDate(new Date());
  }

  getUpdatedCRFVersionsByDate(_refDate: Date): Promise<Map<string, Version>> {
    return Promise.resolve(new Map().set(this.crfName, '0.0.1'));
  }

  async getCRFConfigurationByCase(_model: any, _caseMetaData: CaseMetaData): Promise<CRFConfigurationInterface[]> {
    return [{ crfName: this.crfName }];
  }

  async getCRFConfigurationsByCentreCode(_centreCode?: string | undefined): Promise<CRFConfigurationInterface[]> {
    return [{ crfName: this.crfName }];
  }

  getReferenceDateIfChanged(_oldModel: any, _newModel: any, _caseMetaData: CaseMetaData): Date | false {
    return false;
  }

  getVersionFor(_data: any, _CRFName: string): Promise<`${number}.${number}.${number}`> {
    return Promise.resolve('0.0.1');
  }
}

import { injectable, inject } from 'inversify';
import { type ReminderStorage, Reminder } from '../mock-repositories/ReminderStorage';
import { DandelionService } from '@/services/DandelionService';
import { MagicNumbers } from '@/types/MagicNumbers';

export interface ReminderServiceInterface {
  addReminderToQueue(
    id: string,
    title: string,
    text: string,
    dimissionDate: Date,
    reminderDate: Date,
    daysBeforeExpiration: number
  ): void;
  removeReminder(id: string): Promise<string>;
  getAllReminders(): Promise<Array<Reminder>>;
  removeAllReminders(id: string): Promise<string>;
}

export enum ReminderStatus {
  Active = 'Active',
  Inactive = 'Inactive',
  Expired = 'Expired',
}

@injectable()
export class ReminderService extends DandelionService implements ReminderServiceInterface {
  protected queue = new Array<Reminder>();

  constructor(
    @inject('ReminderStorage')
    protected reminderStorage: ReminderStorage
  ) {
    super();
    this.periodicallyProcessQueue();
    this.periodicallyCheckStatus();
  }
  addReminderToQueue(
    id: string,
    title: string,
    text: string,
    dimissionDate: Date,
    reminderDate: Date,
    daysBeforeExpiration: number
  ): any {
    this.queue.push(new Reminder(id, title, text, dimissionDate, reminderDate, daysBeforeExpiration));
    //todo: add mechanism to suspend periodicallyProcessQueue when list is empty
  }

  protected async periodicallyCheckStatus() {
    const list = await this.reminderStorage.getAll();
    if (!list.find((reminder) => reminder.dimissionDate)) {
      for (const element of list) {
        const reminder = new Reminder(
          element.id,
          element.title,
          element.text,
          element.dimissionDate,
          element.reminderDate,
          element.daysBeforeExpiration
        );
        reminder.reminderStatus = ReminderStatus.Inactive;
        this.queue.push(reminder);
      }
    }
    if (list.find((reminder) => reminder.dimissionDate)) {
      const filteredList = list.filter((reminder) => reminder.dimissionDate);
      for (const element of filteredList) {
        const reminder = new Reminder(
          element.id,
          element.title,
          element.text,
          element.dimissionDate,
          element.reminderDate,
          element.daysBeforeExpiration
        );
        reminder.reminderStatus = ReminderStatus.Active;
        this.queue.push(reminder);
      }
    }
    if (list.find((reminder) => reminder.expirationDate.getTime() < new Date().getTime())) {
      const filteredList = list.filter((reminder) => reminder.expirationDate.getTime() < new Date().getTime());
      for (const element of filteredList) {
        const reminder = new Reminder(
          element.id,
          element.title,
          element.text,
          element.dimissionDate,
          element.reminderDate,
          element.daysBeforeExpiration
        );
        reminder.reminderStatus = ReminderStatus.Expired;
        this.queue.push(reminder);
      }
    }
    await this.addOrUpdateReminder();
    setTimeout(() => this.periodicallyCheckStatus(), MagicNumbers.periodicallyStatusCheckTime);
  }

  protected async periodicallyProcessQueue() {
    await this.addOrUpdateReminder();
    setTimeout(() => this.periodicallyProcessQueue(), MagicNumbers.periodicallyProcessQueueTime);
  }

  protected async addOrUpdateReminder(): Promise<void> {
    const reminder = this.queue.shift();
    if (!reminder) {
      return;
    }
    const list = await this.reminderStorage.getAll();
    if (!list.find((element) => element.id == reminder.id)) {
      reminder.reminderStatus = ReminderStatus.Inactive;
      reminder.expirationDate = new Date(reminder.reminderDate);
      reminder.expirationDate.setDate(reminder.expirationDate.getDate() + reminder.daysBeforeExpiration);
      await this.reminderStorage.add(reminder);
      return;
    }
    const reminderIndex = list.findIndex((element) => element.id == reminder.id);
    reminder.expirationDate = new Date(reminder.reminderDate);
    reminder.expirationDate.setDate(reminder.expirationDate.getDate() + reminder.daysBeforeExpiration);
    await this.reminderStorage.update(reminder, reminderIndex);
  }

  async removeReminder(id: string): Promise<string> {
    await this.reminderStorage.delete(id);
    return id;
  }

  async removeAllReminders(id: string): Promise<string> {
    const list = await this.reminderStorage.getAll();
    for (const element of list) {
      const reminder = new Reminder(
        element.id,
        element.title,
        element.text,
        element.dimissionDate,
        element.reminderDate,
        element.daysBeforeExpiration
      );
      reminder.caseID = id;
      this.queue.push(reminder);
      await this.addOrUpdateReminder();
    }
    const newList = await this.reminderStorage.getAll();
    const remindersToBeDeletedList = newList.filter((element) => (element.caseID = id));
    for (const element of remindersToBeDeletedList) {
      await this.reminderStorage.delete(element.id);
    }
    return id;
  }

  async getAllReminders(): Promise<Array<Reminder>> {
    return await this.reminderStorage.getAll();
  }

  isOpen() {
    return true;
  }
}

import { injectable } from 'inversify';
import { readonly, ref, type DeepReadonly, type Ref } from 'vue';
import type { Version } from '@/entities/CRF';
import { User } from '@/entities/User';
import { AppErrorType, ApplicationError } from '@/errors/ApplicationError';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { DandelionService } from '@/services/DandelionService';
import type { AccessRights } from '@/types/AccessRights';
@injectable()
export class UserServiceWithWriteRights extends DandelionService implements UserServiceInterface {
  readonly userArray: Array<User> = [
    new User('Admin', 'Ospedale.bologna', 'Admin'),
    new User('SimpleUser', 'Ospedale.bologna', 'User'),
  ];
  protected user: Ref<User | null> = ref(null);

  constructor() {
    super();
    this.setCurrentUser(this.userArray[1].username);
  }

  public getCurrentUser(): DeepReadonly<Ref<User | null>> {
    return readonly(this.user);
  }

  setCurrentUser(username: string) {
    this.user.value = this.userArray.find((el) => el.username == username) ?? new User('', '', '');
  }

  getHeaders() {
    throw new ApplicationError('Get headers in UserServiceWithoutWriteRights', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }

  canRead(): boolean {
    return true;
  }

  canWrite(): boolean {
    return true;
  }

  hasReadAccessRights(): boolean {
    return true;
  }

  hasWriteAccessRights(): boolean {
    return true;
  }

  getAccessRights(): Map<string, AccessRights> {
    return new Map();
  }

  searchMostSpecificVariablePrefix(_fullName: string, _crfName: string, _crfVersion: Version): string {
    throw new ApplicationError('Search most specific variable prefix in UserServiceWithWriteRights', AppErrorType.METHOD_NOT_IMPLEMENTED);
  }
}

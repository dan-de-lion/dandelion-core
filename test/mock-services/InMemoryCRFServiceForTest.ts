import { inject, injectable } from 'inversify';
import 'reflect-metadata';
import type { CRF, CRFVersionValidity, Version } from '@/entities/CRF';
import type { AccessLevelsTree } from '@/interfaces/AccessLevelsTree';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { ConsoleLogger } from '@/services/Logger';
import { CrfServiceFromFolder } from '@/storages/CrfServiceFromFolder';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export default class InMemoryCRFServiceForTest extends CrfServiceFromFolder {
  constructor(
    @inject(NamedBindings.Logger) protected logger: ConsoleLogger,
    @inject(NamedBindings.CoreConfiguration) protected coreConfiguration: CoreConfigurationInterface,
    protected crfs: CRF[], protected crfVersions: Map<string, CRFVersionValidity[]>, protected accessLevels = {}) {
    super(logger, coreConfiguration);
  }

  public async getCRF(name: string, version: string): Promise<CRF> {
    const foundCRF = this.crfs.find((crf) => crf.name === name && (!version || crf.version === version));
    return foundCRF ? foundCRF : Promise.reject();
  }

  public async getCRFActiveVersions(name: string): Promise<Array<CRFVersionValidity>> {
    return this.crfVersions.get(name) ?? [];
  }

  getAccessLevelsForCRF(_crfName: string, _crfVersion: Version): AccessLevelsTree {
    return this.accessLevels;
  }

  async getLatestVersion(_crfName: string): Promise<Version> {
    return '0.0.1';
  }

  isCRFPath(url: string) {
    return (
      ((url.endsWith('.css') && url.includes('_style_')) ||
        (url.endsWith('.js') && url.includes('_functions_')) ||
        (url.endsWith('.json') && url.includes('_translations_'))) &&
      url.includes('/examples/crfs')
    );
  }
}

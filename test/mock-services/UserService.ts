import { inject, injectable } from 'inversify';
import { type DeepReadonly, type Ref, ref, readonly } from 'vue';
import type { Version } from '@/entities/CRF';
import { User } from '@/entities/User';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import type { CRFServiceInterface } from '@/interfaces/CRFServiceInterface';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import { DandelionService } from '@/services/DandelionService';
import type { AccessRights } from '@/types/AccessRights';
import { NamedBindings } from '@/types/NamedBindings';
import Helpers from '@/utils/Helpers';

@injectable()
export class UserService extends DandelionService implements UserServiceInterface {
  static defaultUserArray: Array<User> = [
    new User('Admin', 'Ospedale.bologna', 'Admin'),
    new User('SimpleUser', 'Ospedale.bologna', 'User'),
  ];
  protected user: Ref<User | null> = ref(null);

  constructor(
    @inject(NamedBindings.CRFService)
    protected crfService: CRFServiceInterface,
    protected userArray: Array<User> = UserService.defaultUserArray
  ) {
    super();
    if (!this.userArray || this.userArray.length == 0) {
      this.userArray = UserService.defaultUserArray;
    }
    //TODO use environment variables when
    this.setCurrentUser(this.userArray[0].username);
  }

  public getCurrentUser(): DeepReadonly<Ref<User | null>> {
    return readonly(this.user);
  }

  public setCurrentUser(username: string) {
    this.user.value = this.userArray.find((el) => el.username == username) ?? new User('', '', '');
  }

  getHeaders(): any {
    return {};
  }

  getAccessRights(): Map<string, AccessRights> {
    const accessRights = new Map<string, any>();

    accessRights.set('OnlyAdmin', {
      read_roles: ['Admin'],
      write_roles: ['Admin'],
    });
    accessRights.set('OnlyUser', {
      read_roles: ['User'],
      write_roles: ['User'],
    });
    accessRights.set('public', {
      read_roles: ['Admin', 'User'],
      write_roles: ['Admin', 'User'],
    });
    return accessRights;
  }

  canRead(variable: string, crfName: string, crfVersion: Version): boolean {
    const cleanedVariable = variable.replace(/\[\d+\]/g, '');

    // remove crfName from variable
    const accessLevels = this.crfService.getAccessLevelsForCRF(crfName, crfVersion);
    const chosenLevel = Helpers.searchMostSpecificVariablePrefix(cleanedVariable, accessLevels);

    if (chosenLevel === 'public') {
      return true;
    }
    if (this.user.value == null) {
      return false;
    }
    return this.getAccessRights().get(chosenLevel)!.read_roles.includes(this.user.value.role);
  }

  canWrite(variable: string, crfName: string, crfVersion: Version): boolean {
    const accessLevels = this.crfService.getAccessLevelsForCRF(crfName, crfVersion);
    const chosenLevel = Helpers.searchMostSpecificVariablePrefix(variable, accessLevels);

    if (chosenLevel === 'public') {
      return true;
    }
    if (this.user.value == null) {
      return false;
    }
    return this.getAccessRights().get(chosenLevel)!.write_roles.includes(this.user.value.role);
  }

  hasReadAccessRights(accessLevel: string): boolean {
    try {
      if (accessLevel == 'public') {
        return true;
      }
      if (this.user.value == null) {
        return false;
      }
      const myRoles = this.user.value.role;
      const rolesThatCanRead = this.getAccessRights().get(accessLevel)?.read_roles ?? [];
      return rolesThatCanRead.includes(myRoles);
    } catch (e) {
      throw new ApplicationError('User roles or AccessRights not set', AppErrorType.ACCESS_RIGHTS_ERROR);
    }
  }

  hasWriteAccessRights(accessLevel: string): boolean {
    try {
      if (accessLevel == 'public') {
        return true;
      }
      if (this.user.value == null) {
        return false;
      }
      const myRoles = this.user.value.role;
      const rolesThatCanWrite = this.getAccessRights().get(accessLevel)?.write_roles ?? [];
      return rolesThatCanWrite.includes(myRoles);
    } catch (e) {
      throw new ApplicationError('User roles or AccessRights not set', AppErrorType.ACCESS_RIGHTS_ERROR);
    }
  }
}

import { injectable, inject } from 'inversify';
import type { UserService } from '../../test/mock-services/UserService'
import type { VariablesCacheStorageInterface } from '@/interfaces/VariablesCacheStorageInterface';
import type CurrentContextService from '@/services/CurrentContextService';
import type { CaseCacheDTO, CaseID } from '@/services/CaseVariablesCacheService';
import { DandelionService } from '@/services/DandelionService';
import { NamedBindings } from '@/types/NamedBindings';
import { Stringifier } from '@/utils/SerDes';

const cacheStringifier = new Stringifier<Map<CaseID, CaseCacheDTO>>();
@injectable()
export class CacheStorageForTests extends DandelionService implements VariablesCacheStorageInterface {

  constructor(
    @inject(NamedBindings.CurrentContextService) protected contextService: CurrentContextService,
    @inject(NamedBindings.UserService) protected userService: UserService
  ) {
    super();
  }

  getCachedCases(): Map<string, CaseCacheDTO> {
    // Vanno recuperati ogni volta perche' caseType e user possono cambiare
    const caseType = this.contextService.getCaseConfiguration().caseName ?? '';
    const centreCode = this.userService.getCurrentUser().value?.centre ?? '';
    const localStorageCache = window.localStorage.getItem(`cachedCases_${caseType}_${centreCode}`);
    return localStorageCache ? cacheStringifier.deserialize(localStorageCache) : new Map();
  }

  overwriteCache(cache: Map<string, CaseCacheDTO>): void {
    // Vanno recuperati ogni volta perche' caseType e user possono cambiare
    const caseType = this.contextService.getCaseConfiguration().caseName ?? '';
    const centreCode = this.userService.getCurrentUser().value?.centre ?? '';
    window.localStorage.setItem(`cachedCases_${caseType}_${centreCode}`, cacheStringifier.serialize(cache));
  }
}

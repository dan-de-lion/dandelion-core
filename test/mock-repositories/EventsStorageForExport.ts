import { injectable } from 'inversify';
import type { CaseMetaData } from '@/entities/CaseMetaData';
import { CaseData } from '@/entities/Cases';
import { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import { DandelionService } from '@/services/DandelionService';
import { CreateCaseEvent } from '@/source-events/list/CreateCaseEvent';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { AxiosStringifier } from '@/utils/SerDes';

@injectable()
export class EventsStorageForExport extends DandelionService implements EventsStorageInterface {
  //First string key is centre code, second string key caseType
  protected eventMap: Map<string, Map<string, Map<string, SourceEventForList>>> = new Map();
  protected snapshotsMap: Map<string, Map<string, Array<CaseListSnapshot>>> = new Map();
  protected axiosStringifier = new AxiosStringifier<JSON>();

  constructor(protected caseData: CaseData) {
    super();
    const caseMetadata: CaseMetaData = this.axiosStringifier.deserialize(this.caseData?.caseMetaData);
    this.eventMap.set(
      'Ospedale.bologna',
      new Map().set(
        caseMetadata?.caseType,
        new Map().set('applyCreateCase', new CreateCaseEvent(caseMetadata?.caseID, '', new Date()))
      )
    );
    this.snapshotsMap.set('Ospedale.bologna', new Map().set(caseMetadata?.caseType, [new CaseListSnapshot([], new Date(), 0)]));
  }

  async loadAllEvents(centreCode: string, caseType: string): Promise<SourceEventForList[]> {
    const centreCodeMap = this.eventMap.get(centreCode);
    if (!centreCodeMap) {
      return [];
    }
    const caseTypeMap = centreCodeMap.get(caseType);
    if (!caseTypeMap) {
      return [];
    }
    return Array.from(caseTypeMap.values());
  }

  saveEvent(_event: SourceEventForList, _centreCode: string, _caseType: string): Promise<void> {
    throw new StorageError('Save event in EventsStorageForExport', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  async getLastSnapshotAndEvents(centreCode: string, caseType: string): Promise<SnapshotAndEvents> {
    const events = await this.loadAllEvents(centreCode, caseType);
    const snapshot = this.snapshotsMap.get(centreCode)?.get(caseType)?.[0]!;
    return { events, snapshot };
  }

  saveSnapshot(_snapshot: CaseListSnapshot, _centreCode: string, _caseType: string): Promise<void> {
    throw new StorageError('Save snapshot in EventsStorageForExport', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }
}

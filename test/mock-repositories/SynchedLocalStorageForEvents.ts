import { injectable } from 'inversify';
import type { CaseListSnapshot, SnapshotAndEvents } from '@/entities/SnapshotAndEvents';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import { DandelionService } from '@/services/DandelionService';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';

@injectable()
export class SynchedLocalStorageEvents extends DandelionService implements EventsStorageInterface {
  protected localStorage = window.localStorage;

  constructor(protected saveKey = 'synchEvents') {
    super();
  }

  async getLastSnapshotAndEvents(_centreCode: string, _caseType: string): Promise<SnapshotAndEvents> {
    throw new StorageError('Get snapshot and events in SynchedLocalStorageEvents', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  async saveSnapshot(_snapshot: CaseListSnapshot, _centreCode: string, _caseType: string): Promise<void> {
    throw new StorageError('Save snapshot in SynchedLocalStorageEvents', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  async loadAllEvents(): Promise<Array<SourceEventForList>> {
    const arrayOfEvents = JSON.parse(this.localStorage.getItem(this.saveKey) ?? '[]') as Array<SourceEventForList>;
    const resultArray: Array<SourceEventForList> = [];

    arrayOfEvents.forEach((event: SourceEventForList) => {
      resultArray.push({ ...event, timestamp: event.timestamp });
    });
    return resultArray;
  }

  async saveEvent(event: SourceEventForList, _centreCode: string, _caseType: string): Promise<void> {
    const array = JSON.parse(this.localStorage.getItem(this.saveKey) ?? '[]');
    array.push(event);
    this.localStorage.setItem(this.saveKey, JSON.stringify(array));
  }
}

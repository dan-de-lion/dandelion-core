import axios, { type AxiosRequestTransformer, type AxiosResponseTransformer } from 'axios';
import { injectable } from 'inversify';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import type { CaseMetaData } from '@/entities/CaseMetaData';
import type { DataSynchStorageInterface } from '@/interfaces/DataSynchStorageInterface';
import { AxiosReviver, AxiosSerializer } from '@/utils/SerDes';
import { StorageError, StorageErrorType } from '@/errors/StorageError';

@injectable()
export class DataSynchStorage implements DataSynchStorageInterface {
  protected SYNCH_BASE_URL = 'http://localhost:5042/synch';
  protected axiosInstance = axios.create({
    transformRequest: [AxiosSerializer, ...(axios.defaults.transformRequest as AxiosRequestTransformer[])],
    transformResponse: [...(axios.defaults.transformResponse as AxiosResponseTransformer[]), AxiosReviver],
  });

  async saveSynch(partialStoredCases: PartialStoredCase[], caseMetaData: CaseMetaData): Promise<void> {
    if (partialStoredCases.length <= 0) {
      return Promise.resolve();
    }
    const data = partialStoredCases.reduce((prev, curr) => {
      if (curr.accessLevel == 'SensitiveData') {
        return prev;
      }
      prev[curr.accessLevel] = curr;
      return prev;
    }, {} as { [i: string]: any });
    await this.axiosInstance.post(
      this.SYNCH_BASE_URL +
      `/SetDocument?collectionName=NewDandelionProsafeTest&condition={"__guid__":"${caseMetaData.caseID}"}`,
      {
        data,
        caseMetaData,
      }
    );
    return Promise.resolve();
  }

  async permanentlyDelete(_caseID: string): Promise<void> {
    try {
      await this.axiosInstance.post(
       `${this.SYNCH_BASE_URL}/RemoveDocument?collectionName=NewDandelionProsafeTest&condition={"caseMetaData.caseID":"${_caseID}"}`
      );
    } catch (error: any) {
      throw new StorageError(
        `Failed to permanently delete case ${_caseID}: ${error.message}`,
        StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED,
      );
   }
  }         
}

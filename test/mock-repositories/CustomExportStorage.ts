import { injectable } from 'inversify';
import _ from 'lodash';
import { reactive } from 'vue';
import type { ExportButtonParameters } from '@/entities/exportStrategy/ExportButtonParameters';
import { DandelionService } from '@/services/DandelionService';
import { Stringifier } from '@/utils/SerDes';

@injectable()
export class CustomExportStorage extends DandelionService {
  protected localStorage = window.localStorage;
  protected defaultCustomButtons: Array<ExportButtonParameters> = reactive([]);
  constructor() {
    super();
    if (!this.localStorage.getItem(`customButtons`)) {
      this.saveAllCustomButtons(this.defaultCustomButtons);
    }
  }

  getCustomButtons(): Promise<Array<ExportButtonParameters>> {
    const buttons = new Stringifier<ExportButtonParameters[]>().deserialize(
      this.localStorage.getItem(`customButtons`)!
    );
    return Promise.resolve(buttons);
  }

  protected saveAllCustomButtons(buttons: Array<ExportButtonParameters>): Promise<boolean> {
    this.localStorage.setItem('customButtons', JSON.stringify(buttons));
    return Promise.resolve(true);
  }

  async saveCustomButton(newButton: ExportButtonParameters): Promise<boolean> {
    const buttons = await this.getCustomButtons();
    buttons.push(newButton);
    this.saveAllCustomButtons(buttons);
    return Promise.resolve(true);
  }

  async deleteCutomButton(index: number): Promise<boolean> {
    const buttons = await this.getCustomButtons();
    buttons.splice(index, 1);
    this.saveAllCustomButtons(buttons);
    return Promise.resolve(true);
  }
}

import { injectable } from 'inversify';
import type { CRF } from '@/entities/CRF';
import { DandelionService } from '@/services/DandelionService';

@injectable()
export default class LocalStorageCRFStorage extends DandelionService {
  protected localStorage = window.localStorage;

  constructor() {
    super();
  }

  public setCRF(crf: CRF): void {
    const localStorageCrfs = this.localStorage.getItem('CRFs');
    const crfs: Array<CRF> = localStorageCrfs !== null ? JSON.parse(localStorageCrfs) : [];
    const crfIndex = crfs.findIndex((myCrf) => myCrf.name === crf.name && myCrf.version === crf.version);
    if (crfIndex > -1) {
      crfs[crfIndex] = crf;
    } else {
      crfs.push(crf);
    }
    this.localStorage.setItem('CRFs', JSON.stringify(crfs));
  }

  public getCRF(crfName: string, crfVersion: string): CRF | null {
    const localStorageCrfs = this.localStorage.getItem('CRFs');
    const crfs: Array<CRF> = localStorageCrfs !== null ? JSON.parse(localStorageCrfs) : [];
    let result: CRF | null = null;

    if (crfVersion !== null) {
      crfs.forEach((crf) => {
        if (crf.name === crfName && String(crf.version) === crfVersion) {
          result = crf;
        }
      });
      return result;
    }
    return crfs.find((crf) => crf.name === crfName) ?? null;
  }

  public getAllCRFs(): Array<CRF> {
    const localStorageCrfs = this.localStorage.getItem('CRFs');
    return localStorageCrfs !== null ? JSON.parse(localStorageCrfs) : [];
  }
}

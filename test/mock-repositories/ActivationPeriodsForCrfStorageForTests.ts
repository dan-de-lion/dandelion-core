import { injectable, inject } from 'inversify';
import TestHelper from '../utils/TestHelper';
import { CrfWithValidity } from '@/entities/CrfWithValidity';
import { LoggerInterface } from '@/interfaces/LoggerInterface';
import CurrentContextService from '@/services/CurrentContextService';
import TranslationService from '@/services/TranslationService';
import { ActivationPeriodsForCrfStorage } from '@/storages/ActivationPeriodsForCrfStorage';
import { NamedBindings } from '@/types/NamedBindings';

@injectable()
export class ActivationPeriodsForCrfStorageForTests extends ActivationPeriodsForCrfStorage {
  constructor(
    @inject(NamedBindings.CurrentContextService) protected currentContextService: CurrentContextService,
    @inject(NamedBindings.Logger) protected logger: LoggerInterface,
    @inject(NamedBindings.TranslationService) protected ts: TranslationService,
    protected path = '') {
    super(currentContextService, logger, ts);
  }

  async getCrfsWithValidityForCentreCode(centreCode: string): Promise<CrfWithValidity[]> {
    const result = JSON.parse(TestHelper.requireFileJSON(`/ActivationPeriods/${this.path}`));
    const validatedCrfsWithValidity = this.validateCrfsWithValidity(result);
    const crfsForCentre = validatedCrfsWithValidity.get(centreCode);
    return crfsForCentre ?? [];
  }

  setPath(path: string) {
    this.path = path;
  }
}

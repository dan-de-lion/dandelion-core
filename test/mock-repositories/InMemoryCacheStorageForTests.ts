import { injectable } from 'inversify';
import type { VariablesCacheStorageInterface } from '@/interfaces/VariablesCacheStorageInterface';
import type { CaseCacheDTO } from '@/services/CaseVariablesCacheService';
import { DandelionService } from '@/services/DandelionService';

@injectable()
export class InMemoryCacheStorageForTests extends DandelionService implements VariablesCacheStorageInterface {
  protected cachedCases: Map<string, CaseCacheDTO> = new Map();

  constructor() {
    super();
  }

  getCachedCases(): Map<string, CaseCacheDTO> {
    return this.cachedCases;
  }

  overwriteCache(cache: Map<string, CaseCacheDTO>): void {
    this.cachedCases = cache;
  }
}

import axios from "axios";
import { toast } from "vue3-toastify";
import { injectable } from "inversify";
import { SourceEventForList } from "@/source-events/list/SourceEventForList";

@injectable()
export class EventSynchStorage {

  protected SYNCH_BASE_URL = 'http://localhost:5042/synch';

  async loadAllEvents(): Promise<SourceEventForList[]> {

    let data = [];
    try {
      data = (
        await axios.get(this.SYNCH_BASE_URL +
          `/FindDocuments?CollectionName=NewDandelionProsafeEventsTest&condition={}&sorting={}&projection={}`)
      ).data;
    } catch (error: any) {
      this.ErrorHandler(error);
    }
    const resultArray = Array.isArray(data) ? data : [];
    return resultArray;
  }

  async saveEvent(event: SourceEventForList): Promise<string> {
    try {
      await axios.post(encodeURI(this.SYNCH_BASE_URL + `/SetDocument?collectionName=NewDandelionProsafeEventsTest&condition={"caseID":"${event.id}","handler":"${event.handler}"}`), event);
    } catch (error: any) {
      this.ErrorHandler(error);
    }
    // todo: verify if we need to handle error
    return event.id;
  }

  private ErrorHandler(error: any) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      const responseHeadersHTML = `<details><summary>Response Headers</summary><dl>${formatHeadersAsHTML(
        error.response.headers
      )}</dl></details>`;
      const errorData = `<details><summary>Response Data</summary><dl>${formatData(
        error.response.data
      )}</dl></details>`;
      const errorString = error.message + '\n' + errorData + '\n' + responseHeadersHTML;
      console.log(errorString);
      setTimeout(
        () => toast.error(errorString, {
          dangerouslyHTMLString: true,
          closeOnClick: false,
          autoClose: false,
        }),
        0
      );
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      if (error.request.response === '') {
        setTimeout(() => toast.error('Error: ' + error.message), 0);
      } else {
        setTimeout(() => toast.error(error.request), 0);
      }
    } else {
      // Something happened in setting up the request that triggered an Error
      setTimeout(() => toast.error('Error: ' + error.message), 0);
    }
    console.log(error.config);
  }
}

function formatHeadersAsHTML(headers: any) {
  return Object.entries(headers)
    .map(([key, value]) => `${key}: ${value}`)
    .join('\n');
}

function formatData(data: any) {
  return Object.entries(data)
    .map(([key, value]) => `${key}: ${value}`)
    .join('\n');
}

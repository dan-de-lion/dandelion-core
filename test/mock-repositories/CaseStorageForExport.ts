import { injectable } from 'inversify';
import type { CaseMetaData, SaveMetaData } from '@/entities/CaseMetaData';
import { CaseData } from '@/entities/Cases';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import type { PartialStoredCase } from '@/entities/PartialStoredCase';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import type { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import { DandelionService } from '@/services/DandelionService';
import { AxiosStringifier } from '@/utils/SerDes';

@injectable()
export class CaseStorageForExportTests extends DandelionService implements CaseStorageInterface {
  caseListMap: Map<string, Map<string, PartialStoredCase>> = new Map();
  caseMetaDataMap: Map<string, CaseMetaData> = new Map();
  caseMap: Map<string, PartialStoredCase> = new Map();
  protected axiosStringifier = new AxiosStringifier<JSON>();

  constructor(protected caseData: CaseData) {
    super();
    const partialStoredCases: PartialStoredCase[] = this.axiosStringifier.deserialize(this.caseData.partialStoredCases);
    const caseMetadata: CaseMetaData = this.axiosStringifier.deserialize(this.caseData.caseMetaData);
    for (const partialStoredCase of partialStoredCases) {
      this.caseMap.set(partialStoredCase.accessLevel, partialStoredCase);
    }
    this.caseListMap.set(caseMetadata.caseID, this.caseMap);
    this.caseMetaDataMap.set(caseMetadata.caseID, caseMetadata);
  }

  async getAllCaseMetaData(_caseType: string): Promise<CaseMetaData[]> {
    const allCaseMetaData = Array.from(this.caseMetaDataMap.values())
    await new Promise((resolve) => setTimeout(resolve, 1));
    return allCaseMetaData;
  }

  async getCaseMetaData(caseID: string): Promise<CaseMetaData> {
    const caseMetaData = this.caseMetaDataMap.get(caseID);
    if (!caseMetaData) {
      throw new StorageError(`CaseMetaData undefined for id: ${caseID}, you have not passed the right case in the constructor`, StorageErrorType.CASE_METADATA_NOT_FOUND);
    }
    return caseMetaData;
  }

  async getPartials(caseID: string): Promise<PartialStoredCase[]> {
    const partialStoredCases = this.caseListMap.get(caseID);
    if (!partialStoredCases) {
      throw new StorageError(`PartialStoredCase undefined for id: ${caseID}, you have not passed the right case in the constructor`, StorageErrorType.PARTIALS_NOT_FOUND);
    }
    return Array.from(partialStoredCases.values());
  }

  async getMultiplePartials(_caseIDs: string[]): Promise<{ successCases: { caseID: string; partials: PartialStoredCase[] }[]; failedCases: { caseID: string; error: string }[]; }> {
    return { successCases: [], failedCases: [] };
    // throw new StorageError('Get Multiple Partials in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  async getMultipleCaseMetaData(_caseIDs: string[]): Promise<{ successCases: { caseID: string; caseMetaData: CaseMetaData }[]; failedCases: { caseID: string; error: string }[]; }> {
    return { successCases: [], failedCases: [] };
    // throw new StorageError('Get Multiple CaseMetaData in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  updatePartials(_caseID: string, _partialStoredCases: PartialStoredCase[], _saveMetaData: SaveMetaData): Promise<void> {
    throw new StorageError('Update partials in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  createCase(_partialStoredCases: PartialStoredCase[], _caseMetaData: CaseMetaData): Promise<void> {
    throw new StorageError('Create case in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  delete(_caseID: string): Promise<void> {
    throw new StorageError('Delete in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  restore(_caseID: string): Promise<void> {
    throw new StorageError('Restore in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  permanentlyDelete(_caseID: string): Promise<void> {
    throw new StorageError('Permanently Delete in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  getCaseHistory(_caseID: string): Promise<HistoryPartialStoredCase[]> {
    throw new StorageError('Get Case History in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  lock(_caseID: string): Promise<void> {
    throw new StorageError('Lock in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  unlock(_caseID: string): Promise<void> {
    throw new StorageError('Unlock in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  freeze(_caseID: string, _frozenAccessLevels: string[]): Promise<void> {
    throw new StorageError('Freeze in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }

  unfreeze(_caseID: string, _frozenAccessLevels: string[]): Promise<void> {
    throw new StorageError('Unfreeze in CaseStorageForExportTests', StorageErrorType.METHOD_NOT_IMPLEMENTED);
  }
}

import { injectable } from 'inversify';
import { DandelionService } from '@/services/DandelionService';

export class Reminder {
  reminderStatus: string;
  expirationDate: Date;
  caseID: string;
  constructor(
    readonly id: string,
    readonly title: string,
    readonly text: string,
    readonly dimissionDate: Date,
    readonly reminderDate: Date,
    readonly daysBeforeExpiration: number
  ) {
    this.reminderStatus = '';
    this.expirationDate = new Date('');
    this.caseID = '';
  }
}

@injectable()
export class ReminderStorage extends DandelionService {
  protected localStorage = window.localStorage;
  protected reminderList = new Array<Reminder>();

  constructor() {
    super();
  }

  async add(object: Reminder): Promise<void> {
    this.reminderList.push(object);
    await this.save();
  }

  async update(object: Reminder, objIndex: number) {
    this.reminderList[objIndex] = object;
    await this.save();
  }

  async getAll(): Promise<Array<Reminder>> {
    const list: Array<Reminder> = JSON.parse(this.localStorage.getItem('remindersList') ?? '[]');
    list.forEach((element) => {
      element.expirationDate = new Date(element.expirationDate);
    });
    return list;
  }

  async get(id: string): Promise<Reminder> {
    this.reminderList = await this.getAll();
    const filteredNotification = this.reminderList.find((reminder) => reminder.id == id);
    return filteredNotification ?? Promise.reject('no reminders found in storage');
  }

  async delete(id: string): Promise<void | Reminder> {
    this.reminderList = await this.getAll();
    this.reminderList = this.reminderList.filter((object) => object.id !== id);
    await this.save();
  }

  async save() {
    this.localStorage.setItem('remindersList', JSON.stringify(this.reminderList));
  }
}

import { describe, expect, test } from 'vitest';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { substituteModelInPlace } from '@/utils/substituteModelInPlace';

describe('In-place Model substitution', () => {
  test('Existing variables are replaced', () => {
    const model = { first: "I'm a string", second: 23 };
    const newData = { first: 'Substitute string', second: 42 };

    substituteModelInPlace(model, newData);
    expect(model.first).toEqual(newData.first);
    expect(model.second).toEqual(newData.second);
  });

  test('New variables not in model are added', () => {
    const model: any = { first: "I'm a string" };
    const newData = { first: 'Substitute string', second: 42 };

    substituteModelInPlace(model, newData);
    expect(model.first).toEqual(newData.first);
    expect(model.second).toEqual(newData.second);
  });

  test('Variables not in new data are removed', () => {
    const model = { first: "I'm a string", second: 23 };
    const newData = { first: 'Substitute string' };

    substituteModelInPlace(model, newData);
    expect(model.second).toBeUndefined();
  });

  test('Existing deep variables are replaced', () => {
    const model = { group: { subGroup: { first: "I'm a string", second: 23 } } };
    const newData = { group: { subGroup: { first: 'Substitute string', second: 42 } } };

    const referenceBeforeUpdate = model.group.subGroup;

    substituteModelInPlace(model, newData);
    expect(model.group.subGroup.first).toEqual(newData.group.subGroup.first);
    expect(model.group.subGroup.second).toEqual(newData.group.subGroup.second);
    expect(model.group.subGroup).toBe(referenceBeforeUpdate);
  });

  test('New deep variables not in model are added', () => {
    const model: any = { group: { subGroup: { second: 23 } } };
    const newData = { group: { subGroup: { first: "I'm a string", second: 42 } } };

    substituteModelInPlace(model, newData);
    expect(model.group.subGroup.first).toEqual(newData.group.subGroup.first);
  });

  test('Deep variables not in new data are removed', () => {
    const model = { group: { subGroup: { first: "I'm a string", second: 23 } } };
    const newData = { group: { subGroup: { second: 42 } } };

    substituteModelInPlace(model, newData);
    expect(model.group.subGroup.first).toBeUndefined();
  });

  test('Existing groups are replaced', () => {
    const model = { group: { first: "I'm a string" } };
    const newData = { group: { second: 42 } };

    const referenceBeforeUpdate = model.group;

    substituteModelInPlace(model, newData);
    expect(model.group).toEqual(newData.group);
    expect(model.group).toBe(referenceBeforeUpdate);
  });

  test('New groups not in model are added', () => {
    const model: any = { group: { first: "I'm a string" } };
    const newData = { otherGroup: { second: 42 } };

    substituteModelInPlace(model, newData);
    expect(model.otherGroup).toEqual(newData.otherGroup);
  });

  test('Groups not in new data are removed', () => {
    const model = { group: { first: "I'm a string" } };
    const newData = { otherGroup: { second: 42 } };

    substituteModelInPlace(model, newData);
    expect(model.group).toBeUndefined();
  });

  test('Existing deep groups are replaced', () => {
    const model = { group: { subGroup: { first: "I'm a string" } } };
    const newData = { group: { subGroup: { second: 42 } } };

    const referenceBeforeUpdate = model.group.subGroup;

    substituteModelInPlace(model, newData);
    expect(model.group.subGroup).toEqual(newData.group.subGroup);
    expect(model.group.subGroup).toBe(referenceBeforeUpdate);
  });

  test('New deep groups not in model are added', () => {
    const model: any = { group: { subGroup: { first: "I'm a string" } } };
    const newData = { group: { otherSubGroup: { second: 42 } } };

    substituteModelInPlace(model, newData);
    expect(model.group.otherSubGroup).toEqual(newData.group.otherSubGroup);
  });

  test('Deep groups not in new data are removed', () => {
    const model = { group: { subGroup: { first: "I'm a string" } } };
    const newData = { group: { otherSubGroup: { second: 42 } } };

    substituteModelInPlace(model, newData);
    expect(model.group.subGroup).toBeUndefined();
  });

  test('Existing arrays are replaced', () => {
    const model = { a: ["I'm a string", 23] };
    const newData = { a: [23, "I'm a string"] };

    const referenceBeforeUpdate = model.a;

    substituteModelInPlace(model, newData);
    expect(model.a[0]).toEqual(newData.a[0]);
    expect(model.a[1]).toEqual(newData.a[1]);
    expect(model.a).toBe(referenceBeforeUpdate);
  });

  test('New arrays not in model are added', () => {
    const model: any = { a: ["I'm a string", 23] };
    const newData = { b: [23, "I'm a string"] };

    substituteModelInPlace(model, newData);
    expect(model.b).toEqual(newData.b);
  });

  test('Arrays not in new data are removed', () => {
    const model: any = { a: ["I'm a string", 23] };
    const newData = { b: [23, "I'm a string"] };

    substituteModelInPlace(model, newData);
    expect(model.a).toBeUndefined();
  });

  test('Arrays elements not in new data are removed', () => {
    const model = { a: ["I'm a string", 23] };
    const newData = { a: ["I'm a string"] };

    substituteModelInPlace(model, newData);

    expect(model.a.length).toEqual(1);
    expect(model.a[0]).toEqual(newData.a[0]);
  });

  test('Existing ValuesSetValue are replaced', () => {
    const model = { a: new ValuesSetValue('iam.value', 'I am Value', 0) };
    const newData = { a: new ValuesSetValue('iam.newData', 'I am new Data', 1) };
    substituteModelInPlace(model, newData);
    expect(model.a).toStrictEqual(newData.a);
    expect(model.a).toStrictEqual(newData.a);
  });
  test('New ValuesSetValue not in model are added', () => {
    const model: any = { a: new ValuesSetValue('iam.value', 'I am Value', 0) };
    const newData = { b: new ValuesSetValue('iam.anotherValue', 'I am Another Value', 0) };

    substituteModelInPlace(model, newData);
    expect(model.b).toEqual(newData.b);
  });
  test('ValuesSetValue not in new data are removed', () => {
    const model: any = { a: new ValuesSetValue('iam.value', 'I am Value', 0) };
    const newData = { b: new ValuesSetValue('iam.anotherValue', 'I am another Value', 0) };

    substituteModelInPlace(model, newData);
    expect(model.a).toBeUndefined();
  });
  test('ValuesSetValue elements not in new data are removed', () => {
    const model = { a: new ValuesSetValue('iam.value', 'I am Value', 0) };
    const newData = { a: new ValuesSetValue('iam.value') };

    substituteModelInPlace(model, newData);

    expect(model.a).toEqual(newData.a);
  });
  test('Existing Arrays of ValuesSetValue are replaced', () => {
    const model = {
      a: [
        new ValuesSetValue('iam.value', 'I am Value', 0),
        new ValuesSetValue('iam.secondValue', 'I am second Value', 1),
      ],
    };
    const newData = {
      a: [
        new ValuesSetValue('iam.newData', 'I am new Data', 2),
        new ValuesSetValue('iam.newSecondValue', 'I am new second Value', 3),
      ],
    };

    const referenceBeforeUpdate = model.a;

    substituteModelInPlace(model, newData);

    expect(model.a[0]).toEqual(newData.a[0]);
    expect(model.a[1]).toEqual(newData.a[1]);
    expect(model.a).toBe(referenceBeforeUpdate);
  });
  test('New Arrays of ValuesSetValue not in model are added', () => {
    const model: any = {
      a: [
        new ValuesSetValue('iam.value', 'I am Value', 0),
        new ValuesSetValue('iam.secondValue', 'I am second Value', 1),
      ],
    };
    const newData = {
      b: [
        new ValuesSetValue('iam.newData', 'I am new Data', 2),
        new ValuesSetValue('iam.newSecondValue', 'I am new second Value', 3),
      ],
    };

    substituteModelInPlace(model, newData);
    expect(model.b).toEqual(newData.b);
  });
  test('Arrays of ValuesSetValue not in new data are removed', () => {
    const model: any = {
      a: [
        new ValuesSetValue('iam.value', 'I am Value', 0),
        new ValuesSetValue('iam.secondValue', 'I am second Value', 1),
      ],
    };
    const newData = {
      b: [
        new ValuesSetValue('iam.newData', 'I am new Data', 2),
        new ValuesSetValue('iam.newSecondValue', 'I am new second Value', 3),
      ],
    };

    substituteModelInPlace(model, newData);
    expect(model.a).toBeUndefined();
  });
  test('Arrays of ValuesSetValue elements not in new data are removed', () => {
    const model = {
      a: [
        new ValuesSetValue('iam.newData', 'I am new Data', 2),
        new ValuesSetValue('iam.newSecondValue', 'I am new second Value', 3),
      ],
    };
    const newData = {
      a: [
        new ValuesSetValue('iam.newSecondValue', 'I am new second Value', 3),
        new ValuesSetValue('iam.newData', 'I am new Data', 2),
      ],
    };

    substituteModelInPlace(model, newData);

    expect(model.a).toEqual(newData.a);
  });

  test('Mupltiple arrays elements not in new data are removed', () => {
    const model = { a: ["I'm a string", 23, 42, 56] };
    const newData = { a: [23] };

    substituteModelInPlace(model, newData);

    expect(model.a.length).toEqual(1);
    expect(model.a[0]).toEqual(newData.a[0]);
  });

  test('Existing deep arrays are replaced', () => {
    const model = { arrayGroup: { a: ["I'm a string", 23] } };
    const newData = { arrayGroup: { a: [23, "I'm a string"] } };

    const referenceBeforeUpdate = model.arrayGroup.a;

    substituteModelInPlace(model, newData);
    expect(model.arrayGroup.a[0]).toEqual(newData.arrayGroup.a[0]);
    expect(model.arrayGroup.a[1]).toEqual(newData.arrayGroup.a[1]);
    expect(model.arrayGroup.a).toBe(referenceBeforeUpdate);
  });

  test('New deep arrays not in model are added', () => {
    const model: any = { arrayGroup: { a: ["I'm a string", 23] } };
    const newData = { arrayGroup: { b: [23, "I'm a string"] } };

    substituteModelInPlace(model, newData);
    expect(model.arrayGroup.b).toEqual(newData.arrayGroup.b);
  });

  test('Deep arrays not in new data are removed', () => {
    const model: any = { arrayGroup: { a: ["I'm a string", 23] } };
    const newData = { arrayGroup: { b: [23, "I'm a string"] } };

    substituteModelInPlace(model, newData);
    expect(model.arrayGroup.a).toBeUndefined();
  });

  test('Deep arrays elements not in new data are removed', () => {
    const model = { arrayGroup: { a: ["I'm a string", 23] } };
    const newData = { arrayGroup: { a: [23] } };

    substituteModelInPlace(model, newData);

    expect(model.arrayGroup.a.length).toEqual(1);
    expect(model.arrayGroup.a[0]).toEqual(newData.arrayGroup.a[0]);
  });

  test('Mupltiple deep arrays elements not in new data are removed', () => {
    const model = { arrayGroup: { a: ["I'm a string", 23, 42, 56] } };
    const newData = { arrayGroup: { a: [42] } };

    substituteModelInPlace(model, newData);

    expect(model.arrayGroup.a.length).toEqual(1);
    expect(model.arrayGroup.a[0]).toEqual(newData.arrayGroup.a[0]);
  });

  test('Existing groups children of arrays are replaced', () => {
    const model = [{ group: { first: "I'm a string" } }];
    const newData = [{ group: { second: 42 } }];

    const referenceBeforeUpdate = model[0].group;

    substituteModelInPlace(model, newData);
    expect(model[0].group).toEqual(newData[0].group);
    expect(model[0].group).toBe(referenceBeforeUpdate);
  });

  test('New groups children of arrays not in model are added', () => {
    const model: any = [{ group: { first: "I'm a string" } }];
    const newData = [{ otherGroup: { second: 42 } }];

    substituteModelInPlace(model, newData);
    expect(model[0].otherGroup).toEqual(newData[0].otherGroup);
  });

  test('Groups children of arrays not in new data are removed', () => {
    const model = [{ group: { first: "I'm a string" } }];
    const newData = [{ otherGroup: { second: 42 } }];

    substituteModelInPlace(model, newData);
    expect(model[0].group).toBeUndefined();
  });

  test('Complex model is correctly updated', () => {
    const model = {
      a: [
        { group: { first: "I'm a string", second: "I'm the second element", third: 125 } },
        ['one', 'two', 'three', 'four'],
        42,
      ],
      b: {
        barr: [99, 88, 77, { value: 66 }, 55],
      },
    };
    const newData = {
      a: [
        { group: { first: "I'm a string", third: 125, fourth: 'Added element' } },
        ['one', 'four', 'six'],
      ],
      ab: ['meh'],
      b: {
        barr: [99, 88, 77, { value: [6, 6] }, 55, 44, 33, 22, 11],
      },
    };

    const ref1 = (model.a[0] as any).group.third;
    const ref2 = model.a[1];
    const ref3 = model.b.barr[3];

    substituteModelInPlace(model, newData);
    expect(model).toEqual(newData);
    expect((model.a[0] as any).group.third).toBe(ref1);
    expect(model.a[1]).toBe(ref2);
    expect(model.b.barr[3]).toBe(ref3);
  });
});

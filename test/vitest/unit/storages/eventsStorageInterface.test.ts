import { v4 } from 'uuid';
import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import TestHelper, { storageConfigurations } from '../../../utils/TestHelper';
import MockCRFService from '../../../mock-services/MockCRFService';
import { getContainer, getContext } from '@/ContainersConfig';
import type { EventsStorageInterface } from '@/interfaces/EventsStorageInterface';
import type { SourceEventForList } from '@/source-events/list/SourceEventForList';
import { InMemoryEventsStorage } from '@/storages/InMemoryEventsStorage';
import { LocalStorageForEvents } from '@/storages/LocalStorageForEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

describe.each(storageConfigurations)('Test for EventsStorageInterface, using $name', ({ name, bindings }) => {
  let eventsStorage: EventsStorageInterface;
  beforeAll(() => {
    TestHelper.setupContainer('Admission', [], bindings);
    TestHelper.mockStatusService(Contexts.TEST);
    getContext(Contexts.TEST).container.bind(NamedBindings.CRFService).to(MockCRFService).inSingletonScope();
    eventsStorage = getContainer(Contexts.TEST).get(NamedBindings.StorageForEvents) as EventsStorageInterface;
  });

  beforeEach(() => {
    if (eventsStorage instanceof InMemoryEventsStorage) {
      eventsStorage.reset();
    } else if (eventsStorage instanceof LocalStorageForEvents) {
      window.localStorage.clear();
    }
  });

  const storageNameMap: Record<string, string> = { InMemoryStorages: 'InMemoryEventsStorage', LocalStorages: 'LocalStorageForEvents', RESTStorages: 'RESTStorageForEvents' };
  let storageName = storageNameMap[name] || 'UnknownStorage';
  const event1: SourceEventForList = { timestamp: new Date('2023-11-03 21:30:15:000'), handler: 'applyCreateCase', username: '', id: v4(), caseID: '', };
  const event2: SourceEventForList = { timestamp: new Date('2023-10-30 22:30:20:000'), handler: 'applyUpdateCase', username: '', id: v4(), caseID: '', };
  const event3: SourceEventForList = { timestamp: new Date('2023-10-30 21:30:15:000'), handler: 'applyUpdateCase', username: '', id: v4(), caseID: '', };

  test(`Should return the events in the correct order (timestamp Asc), in ${storageName}`, async () => {
    await Promise.all([event1, event2, event3].map((evt) => eventsStorage.saveEvent(evt, '', '')));
    const events: SourceEventForList[] = await eventsStorage.loadAllEvents('', '');
    expect(events[0]).toEqual(event3);
    expect(events[1]).toEqual(event2);
    expect(events[2]).toEqual(event1);
  });
});
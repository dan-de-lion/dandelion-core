import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import 'reflect-metadata';
import MockCRFService from '../../../mock-services/MockCRFService';
import TestHelper, { storageConfigurations } from '../../../utils/TestHelper';
import { getContainer, getContext } from '@/ContainersConfig';
import { PartialStoredCase } from '@/entities/PartialStoredCase';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
import { LocalCaseStorage } from '@/storages/LocalCaseStorage';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { Stringifier } from '@/utils/SerDes';

describe.each(storageConfigurations)('Test for Serialization, using $name', ({ name, bindings }) => {
  let caseStorage: CaseStorageInterface;
  beforeAll(() => {
    TestHelper.setupContainer('Admission', [], bindings);
    TestHelper.mockStatusService(Contexts.TEST);
    getContext(Contexts.TEST).container.bind(NamedBindings.CRFService).to(MockCRFService).inSingletonScope();
    caseStorage = getContainer(Contexts.TEST).get(NamedBindings.CaseStorage) as CaseStorageInterface;
  });

  beforeEach(() => {
    if (caseStorage instanceof InMemoryCaseStorage) {
      caseStorage.reset();
    } else if (caseStorage instanceof LocalCaseStorage) {
      window.localStorage.clear();
    }
  });

  const storageNameMap: Record<string, string> = { InMemoryStorages: 'inMemoryCaseStorage', LocalStorages: 'LocalCaseStorage', RESTStorages: 'RESTCaseStorage' };
  let storageName = storageNameMap[name] || 'UnknownStorage';
  const stringifier = new Stringifier();
  const arrayOfPartials = [
    new PartialStoredCase(stringifier.serialize({ x: new ValuesSetValue('test.first'), birthdate: new Date('02/02/2022') }), 'sensitive'),
    new PartialStoredCase(stringifier.serialize({ x: [new ValuesSetValue('test.second'), new ValuesSetValue('test.third')] }), 'public'),
  ];
  const caseID = '1';
  const caseMetaData = {
    caseID, centreCode: '', lastUpdate: new Date(), crfVersions: new Map(), caseType: 'Admission',
    creationDate: new Date(), status: new Map(), frozenAccessLevels: new Map()
  };

  test(`ValuesSetValue singlechoice, in ${storageName}`, async () => {
    await caseStorage.createCase(arrayOfPartials, caseMetaData);
    const model = stringifier.deserialize((await caseStorage.getPartials(caseID))[0].data) as Record<string, any>;
    expect(model.x.getValue()).toEqual(new ValuesSetValue('test.first').getValue());
  });

  test(`ValuesSetValue multiplechoice, in ${storageName}`, async () => {
    await caseStorage.createCase(arrayOfPartials, caseMetaData);
    const model = stringifier.deserialize((await caseStorage.getPartials(caseID))[1].data) as Record<string, any>;
    expect(model.x[0].getValue()).toEqual(new ValuesSetValue('test.second').getValue());
    expect(model.x[1].getValue()).toEqual(new ValuesSetValue('test.third').getValue());
  });

  test(`Status map, in ${storageName}`, async () => {
    caseMetaData.status = new Map().set('admission', 1);
    await caseStorage.createCase(arrayOfPartials, caseMetaData);
    expect((await caseStorage.getCaseMetaData(caseID)).status).toEqual(new Map().set('admission', 1));
  });

  test(`CrfVersions map, in ${storageName}`, async () => {
    caseMetaData.crfVersions = new Map().set('admission', '0.0.1');
    await caseStorage.createCase(arrayOfPartials, caseMetaData);
    expect((await caseStorage.getCaseMetaData(caseID)).crfVersions).toEqual(new Map().set('admission', '0.0.1'));
  });

  test(`Date inside model, in ${storageName}`, async () => {
    await caseStorage.createCase(arrayOfPartials, caseMetaData);
    const model = stringifier.deserialize((await caseStorage.getPartials(caseID))[0].data) as Record<string, any>;
    expect(model.birthdate).toEqual(new Date('02/02/2022'));
    expect(model.birthdate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
  });
});
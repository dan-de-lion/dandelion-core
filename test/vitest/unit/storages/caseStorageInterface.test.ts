import 'reflect-metadata';
import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import { getContainer, getContext } from '@/ContainersConfig';
import MockCRFService from '../../../mock-services/MockCRFService';
import TestHelper, { storageConfigurations } from '../../../utils/TestHelper';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { PartialStoredCase } from '@/entities/PartialStoredCase';
import { StorageError, StorageErrorType } from '@/errors/StorageError';
import { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
import { LocalCaseStorage } from '@/storages/LocalCaseStorage';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

describe.each(storageConfigurations)('Test for CaseStorageInterface, using $name', ({ name, bindings }) => {
  let caseStorage: CaseStorageInterface;
  beforeAll(() => {
    TestHelper.setupContainer('Admission', [], bindings);
    TestHelper.mockStatusService(Contexts.TEST);
    getContext(Contexts.TEST).container.bind(NamedBindings.CRFService).to(MockCRFService).inSingletonScope();
    caseStorage = getContainer(Contexts.TEST).get(NamedBindings.CaseStorage) as CaseStorageInterface;
  });

  beforeEach(() => {
    if (caseStorage instanceof InMemoryCaseStorage) {
      caseStorage.reset();
    } else if (caseStorage instanceof LocalCaseStorage) {
      window.localStorage.clear();
    }
  });

  const storageNameMap: Record<string, string> = { InMemoryStorages: 'inMemoryCaseStorage', LocalStorages: 'LocalCaseStorage', RESTStorages: 'RESTCaseStorage' };
  let storageName = storageNameMap[name] || 'UnknownStorage';
  const caseID = '1';
  const failedToGetPartialsMsg = `Failed to get partials, no case with this ID: ${caseID} in ${storageName}`;
  const failedToDeleteMsg = `Failed to delete, no case with this ID: ${caseID} in ${storageName}`;
  const failedToRestoreMsg = `Failed to restore, no case deleted with this ID: ${caseID} in ${storageName}`;
  const failedToPermDeleteMsg = `Failed to permanently delete, no case with this ID: ${caseID} in ${storageName}`;
  const firstpartials = [new PartialStoredCase('{}', 'private'), new PartialStoredCase('{}', 'public')];
  const secondpartials = [new PartialStoredCase('{}', 'private')];
  const caseMetaData: CaseMetaData = {
    caseID, centreCode: '', lastUpdate: new Date(), crfVersions: new Map().set('admission', 1), caseType: 'Admission',
    creationDate: new Date(), status: new Map().set('admission', new Map()), frozenAccessLevels: new Map()
  };

  describe(`Crud actions for: ${storageName}`, () => {
    test(`Should return the correct PartialStoredCase if: saveAllPartials - getPartials, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.createCase(secondpartials, { ...caseMetaData, caseID: '2' });
      expect((await caseStorage.getPartials(caseID))[0]).toEqual(firstpartials[0]);
      expect((await caseStorage.getPartials(caseID))[1]).toEqual(firstpartials[1]);
      expect((await caseStorage.getPartials('2'))[0]).toEqual(secondpartials[0]);
    });

    test(`Should return correct deleted case if: saveAllPartials - delete - getPartials, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.delete(caseID);
      expect((await caseStorage.getPartials(caseID))[0]).toEqual(firstpartials[0]);
    });

    test(`Should return the correct PartialStoredCase if: saveAllPartials - delete - restore - getPartials, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.delete(caseID);
      await caseStorage.restore(caseID);
      expect((await caseStorage.getPartials(caseID))[0]).toEqual(firstpartials[0]);
    });

    test(`Should throw error if: saveAllPartials - delete - permanentlyDelete - getPartials, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.delete(caseID);
      await caseStorage.permanentlyDelete(caseID);
      await expect(caseStorage.getPartials(caseID)).rejects.toThrow(new StorageError(failedToGetPartialsMsg, StorageErrorType.PARTIALS_NOT_FOUND));
    });

    test(`Should throw error if: saveAllPartials - delete - restore - permanentlyDelete - getPartials, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.delete(caseID);
      await caseStorage.restore(caseID);
      await caseStorage.permanentlyDelete(caseID);
      await expect(caseStorage.getPartials(caseID)).rejects.toThrow(new StorageError(failedToGetPartialsMsg, StorageErrorType.PARTIALS_NOT_FOUND));
    });

    test(`Should throw error if: saveAllPartials - delete - delete, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.delete(caseID);
      await expect(caseStorage.delete(caseID)).rejects.toThrow(new StorageError(failedToDeleteMsg, StorageErrorType.DELETE_CASE_FAILED));
    });

    test(`Should throw error if: saveAllPartials - restore, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await expect(caseStorage.restore(caseID)).rejects.toThrow(new StorageError(failedToRestoreMsg, StorageErrorType.RESTORE_CASE_FAILED));
    });

    test(`Should throw error if: saveAllPartials - permanentlyDelete - getPartials, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.permanentlyDelete(caseID);
      await expect(caseStorage.getPartials(caseID)).rejects.toThrow(new StorageError(failedToGetPartialsMsg, StorageErrorType.PARTIALS_NOT_FOUND));
    });

    test(`Should throw error if: saveAllPartials - permanentlyDelete - restore, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.permanentlyDelete(caseID);
      await expect(caseStorage.restore(caseID)).rejects.toThrow(new StorageError(failedToRestoreMsg, StorageErrorType.RESTORE_CASE_FAILED));
    });

    test(`Should throw error if: saveAllPartials - permanentlyDelete - delete, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.permanentlyDelete(caseID);
      await expect(caseStorage.delete(caseID)).rejects.toThrow(new StorageError(failedToDeleteMsg, StorageErrorType.DELETE_CASE_FAILED));
    });

    test(`Should throw error if: saveAllPartials - permanentlyDelete - permanentlyDelete, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.permanentlyDelete(caseID);
      await expect(caseStorage.permanentlyDelete(caseID)).rejects.toThrow(new StorageError(failedToPermDeleteMsg, StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED));
    });

    test(`Should return undefined if: getPartials, in ${storageName}`, async () => {
      expect(caseStorage.getPartials(caseID)).rejects.toThrow(new StorageError(failedToGetPartialsMsg, StorageErrorType.PARTIALS_NOT_FOUND));
    });

    test(`Should throw error if: delete, in ${storageName}`, async () => {
      await expect(caseStorage.delete(caseID)).rejects.toThrow(new StorageError(failedToDeleteMsg, StorageErrorType.DELETE_CASE_FAILED));
    });

    test(`Should throw error if: restore, in ${storageName}`, async () => {
      await expect(caseStorage.restore(caseID)).rejects.toThrow(new StorageError(failedToRestoreMsg, StorageErrorType.RESTORE_CASE_FAILED));
    });

    test(`Should throw error if: permanentlyDelete, in ${storageName}`, async () => {
      await expect(caseStorage.permanentlyDelete(caseID)).rejects.toThrow(new StorageError(failedToPermDeleteMsg, StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED));
    });
  });

  describe(`Correct loading for: ${storageName}`, () => {
    test(`Should correct load timestamp on storage.getCaseMetaData(), in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      const caseMetaDataFromStorage = await caseStorage.getCaseMetaData(caseID);
      // Check that the date format is ISO 8601 type
      expect(caseMetaDataFromStorage.creationDate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
      expect(caseMetaDataFromStorage.lastUpdate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
    });

    // test(`Should correct load timestamp after delete, in ${storageName}`, async () => {
    //   await caseStorage.createCase(firstpartials, caseMetaData);
    //   await caseStorage.delete(caseID);
    //   const caseMetaDataFromStorage = await caseStorage.getCaseMetaData(caseID);
    //   expect((caseMetaDataFromStorage.creationDate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
    //   expect((caseMetaDataFromStorage.lastUpdate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
    // });

    test(`Should correct load timestamp after restore, in ${storageName}`, async () => {
      await caseStorage.createCase(firstpartials, caseMetaData);
      await caseStorage.delete(caseID);
      await caseStorage.restore(caseID);
      const caseMetaDataFromStorage = await caseStorage.getCaseMetaData(caseID);
      expect(caseMetaDataFromStorage.creationDate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
      expect(caseMetaDataFromStorage.lastUpdate.toISOString()).toMatch(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
    });
  });
});
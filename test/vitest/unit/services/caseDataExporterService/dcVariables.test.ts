import 'reflect-metadata';
import _ from 'lodash';
import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import DcCase from '../../../../fixtures/json/dcCase.json';
import { CaseStorageForExportTests } from '../../../../mock-repositories/CaseStorageForExport';
import { EventsStorageForExport } from '../../../../mock-repositories/EventsStorageForExport';
import InMemoryCRFServiceForTest from '../../../../mock-services/InMemoryCRFServiceForTest';
import TestHelper from '../../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import { ExportableVariable } from '@/entities/exportable/ExportableVariable';
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { CaseDataExporterService, ExportConfiguration } from '@/services/CaseDataExporterService';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { Stringifier } from '@/utils/SerDes';

describe('CaseDataExporterService export variables in a dataClass', () => {
  let caseDataExporterService: CaseDataExporterService;
  const stringifier = new Stringifier<any>();
  const variablesToBeCached: Array<string> = [
    'Export.mainDcInstance[0].textVariable',
    'Export.mainDcInstance[0].numberVariable',
    'Export.mainDcInstance[0].outputVariable',
    'Export.mainDcInstance[0].colorVariable',
    'Export.mainDcInstance[0].dateVariable',
    'Export.mainDcInstance[0].timeVariable',
    'Export.mainDcInstance[0].booleanVariable',
    'Export.mainDcInstance[0].singlechoiceVariable',
    'Export.mainDcInstance[0].multiplechoiceVariable',
    'Export.mainDcInstance[0].dcInstance.textInsideDC',
    'Export.mainDcInstance[0].groupVariable.textInsideGroup',
    'Export.mainDcInstance[0].setVariable[0].textInsideSet',
    'Export.mainDcInstance[0].pluginVariable',
  ];
  const caseID: string = DcCase.caseMetaData.caseID;
  const setItemID: string = stringifier.deserialize(DcCase.partialStoredCases[0].data).Export.mainDcInstance.setVariable[0].guid;

  beforeAll(() => {
    TestHelper.setupContainer('Export', variablesToBeCached);
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const xml = TestHelper.requireFile('/export/variables-in-dc.xml');
    //@ts-expect-error
    container.bind(NamedBindings.StorageForEvents).toDynamicValue(() => new EventsStorageForExport(DcCase)).inSingletonScope();
    //@ts-expect-error
    container.bind(NamedBindings.CaseStorage).toDynamicValue(() => new CaseStorageForExportTests(DcCase)).inSingletonScope();
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.Logger) as ConsoleLogger;
    container.bind(NamedBindings.CRFService).toDynamicValue(() => new InMemoryCRFServiceForTest(logger, coreConfiguration,
      [new CRF('Export', '0.0.1', xml)], new Map<string, CRFVersionValidity[]>([['Export', [new CRFVersionValidity(0, '0.0.1', new Date('1990-12-12'))]],]))).inSingletonScope();
    const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
    caseListService.updateCaseList();
    container.rebind(NamedBindings.CaseDataExporterService).to(CaseDataExporterService).inSingletonScope();
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST);
    caseDataExporterService = container.get(NamedBindings.CaseDataExporterService) as CaseDataExporterService;
  });


  const defaultExportConfig: ExportConfiguration = {
    variableList: [],
    excludedVariables: [],
    crfList: [],
    excludedCrfs: [],
    options: {
      exportLabelForValues: false,
      exportLabelForVariableNames: false,
      csvSeparator: ';',
      splitColumnForSingleChoiceValues: false,
      splitColumnForMultipleChoiceValues: false,
      splitColumnForGroups: false,
    }
  };

  describe('Export with default options', () => {
    test('text variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Text', 'Export.mainDcInstance.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.textVariable\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Number', 'Export.mainDcInstance.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.numberVariable\r\n${caseID};3`);
    });

    test('output variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Output', 'Export.mainDcInstance.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.outputVariable\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Color', 'Export.mainDcInstance.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.colorVariable\r\n${caseID};#e53838`);
    });

    test('date variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Date', 'Export.mainDcInstance.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.dateVariable\r\n${caseID};2023-12-11`);
    });

    test('time variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Time', 'Export.mainDcInstance.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.timeVariable\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Boolean', 'Export.mainDcInstance.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.booleanVariable\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainDcInstance.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.singlechoiceVariable\r\n${caseID};siNoValuesSet.si`);
    });

    test('multiplechoice variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainDcInstance.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.multiplechoiceVariable\r\n${caseID};bananaKiwiValuesSet.banana,bananaKiwiValuesSet.kiwi`);
    });

    test('dc instance', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('DC', 'Export.mainDcInstance.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.mainDcInstance.dcInstance.textInsideDC\r\n' + caseID + ';Testo DC');
    });

    test('group variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Group', 'Export.mainDcInstance.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.mainDcInstance.groupVariable.textInsideGroup\r\n' + caseID + ';Testo Group');
    });

    test('set variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Set', 'Export.mainDcInstance.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig)).find((c) => c.info.name === 'Export.mainDcInstance.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.mainDcInstance.setVariable[index0].textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Comune', 'Export.mainDcInstance.pluginVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.pluginVariable\r\n${caseID};C910`);
    });
  });

  describe('Export with exportLabelForValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainDcInstance.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.singlechoiceVariable\r\n${caseID};si`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainDcInstance.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainDcInstance.multiplechoiceVariable\r\n${caseID};banana, kiwi`);
    });
  });

  describe('Export with exportLabelForVariableNames true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForVariableNames = true;

    test('text variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Text', 'Export.mainDcInstance.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Text\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Number', 'Export.mainDcInstance.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Number\r\n${caseID};3`);
    });

    test('output variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Output', 'Export.mainDcInstance.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Output\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Color', 'Export.mainDcInstance.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Color\r\n${caseID};#e53838`);
    });

    test('date variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Date', 'Export.mainDcInstance.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Date\r\n${caseID};2023-12-11`);
    });

    test('time variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Time', 'Export.mainDcInstance.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Time\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Boolean', 'Export.mainDcInstance.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Boolean\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainDcInstance.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Singlechoice\r\n${caseID};siNoValuesSet.si`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainDcInstance.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Multiplechoice\r\n${caseID};bananaKiwiValuesSet.banana,bananaKiwiValuesSet.kiwi`);
    });

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('DC', 'Export.mainDcInstance.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC\r\n' + caseID + ';Testo DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.mainDcInstance.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group\r\n' + caseID + ';Testo Group');
    });

    test('set variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Set', 'Export.mainDcInstance.setVariable'),];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'Export.mainDcInstance.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.mainDcInstance.setVariable.textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Comune', 'Export.mainDcInstance.pluginVariable'),];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Comune\r\n${caseID};C910`);
    });
  });

  describe('Export with exportLabelForValues and exportLabelForVariableNames true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForValues = true;
    exportConfig.options!.exportLabelForVariableNames = true;

    test('text variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Text', 'Export.mainDcInstance.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Text\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Number', 'Export.mainDcInstance.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Number\r\n${caseID};3`);
    });

    test('output variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Output', 'Export.mainDcInstance.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Output\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Color', 'Export.mainDcInstance.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Color\r\n${caseID};#e53838`);
    });

    test('date variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Date', 'Export.mainDcInstance.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Date\r\n${caseID};2023-12-11`);
    });

    test('time variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Time', 'Export.mainDcInstance.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Time\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Boolean', 'Export.mainDcInstance.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Boolean\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainDcInstance.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Singlechoice\r\n${caseID};si`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainDcInstance.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Multiplechoice\r\n${caseID};banana, kiwi`);
    });

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('DC', 'Export.mainDcInstance.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC\r\n' + caseID + ';Testo DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.mainDcInstance.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group\r\n' + caseID + ';Testo Group');
    });

    test('set variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Set', 'Export.mainDcInstance.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'Export.mainDcInstance.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.mainDcInstance.setVariable.textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Comune', 'Export.mainDcInstance.pluginVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Comune\r\n${caseID};C910`);
    });
  });

  describe('Export with separateColumnForSingleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForSingleChoiceValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainDcInstance.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;siNoValuesSet.si;siNoValuesSet.no\r\n${caseID};true;false`);
    });
  });

  describe('Export with separateColumnForMultipleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForMultipleChoiceValues = true;

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainDcInstance.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;bananaKiwiValuesSet.banana;bananaKiwiValuesSet.kiwi\r\n${caseID};true;true`);
    });
  });

  describe('Export with separateColumnForSingleChoiceValues and separateColumnForMultipleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForSingleChoiceValues = true;
    exportConfig.options!.splitColumnForMultipleChoiceValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainDcInstance.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;siNoValuesSet.si;siNoValuesSet.no\r\n${caseID};true;false`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainDcInstance.multiplechoiceVariable'),];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;bananaKiwiValuesSet.banana;bananaKiwiValuesSet.kiwi\r\n${caseID};true;true`);
    });
  });
});

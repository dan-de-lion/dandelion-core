import 'reflect-metadata';
import _ from 'lodash';
import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import GroupCase from '../../../../fixtures/json/groupCase.json';
import { CaseStorageForExportTests } from '../../../../mock-repositories/CaseStorageForExport';
import { EventsStorageForExport } from '../../../../mock-repositories/EventsStorageForExport';
import InMemoryCRFServiceForTest from '../../../../mock-services/InMemoryCRFServiceForTest';
import TestHelper from '../../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import { ExportableVariable } from "@/entities/exportable/ExportableVariable";
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { CaseDataExporterService, ExportConfiguration } from '@/services/CaseDataExporterService';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { Stringifier } from '@/utils/SerDes';

describe('CaseDataExporterService export variables in a group', () => {
  let caseDataExporterService: CaseDataExporterService;
  const stringifier = new Stringifier<any>();
  const variablesToBeCached: Array<string> = [
    'Export.mainGroup.textVariable',
    'Export.mainGroup.numberVariable',
    'Export.mainGroup.outputVariable',
    'Export.mainGroup.colorVariable',
    'Export.mainGroup.dateVariable',
    'Export.mainGroup.timeVariable',
    'Export.mainGroup.booleanVariable',
    'Export.mainGroup.singlechoiceVariable',
    'Export.mainGroup.multiplechoiceVariable',
    'Export.mainGroup.dcInstance.textInsideDC',
    'Export.mainGroup.groupVariable.textInsideGroup',
    'Export.mainGroup.setVariable[0].textInsideSet',
    'Export.mainGroup.pluginVariable',
  ];
  const caseID: string = GroupCase.caseMetaData.caseID;
  const setItemID: string = stringifier.deserialize(GroupCase.partialStoredCases[0].data).Export.mainGroup.setVariable[0].guid;

  beforeAll(() => {
    TestHelper.setupContainer('Export', variablesToBeCached);
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const xml = TestHelper.requireFile('/export/variables-in-group.xml');
    //@ts-expect-error
    container.bind(NamedBindings.StorageForEvents).toDynamicValue(() => new EventsStorageForExport(GroupCase)).inSingletonScope();
    //@ts-expect-error
    container.bind(NamedBindings.CaseStorage).toDynamicValue(() => new CaseStorageForExportTests(GroupCase)).inSingletonScope();
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.CoreConfiguration) as ConsoleLogger;
    container.bind(NamedBindings.CRFService).toDynamicValue(() => new InMemoryCRFServiceForTest(logger, coreConfiguration,
      [new CRF('Export', '0.0.1', xml)], new Map<string, CRFVersionValidity[]>([['Export', [new CRFVersionValidity(0, '0.0.1', new Date('1990-12-12'))]]]))).inSingletonScope();
    const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
    caseListService.updateCaseList();
    container.rebind(NamedBindings.CaseDataExporterService).to(CaseDataExporterService).inSingletonScope();
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST);
    caseDataExporterService = container.get(NamedBindings.CaseDataExporterService) as CaseDataExporterService;
  });

  const defaultExportConfig: ExportConfiguration = {
    variableList: [],
    excludedVariables: [],
    crfList: [],
    excludedCrfs: [],
    options: {
      exportLabelForValues: false,
      exportLabelForVariableNames: false,
      csvSeparator: ';',
      splitColumnForSingleChoiceValues: false,
      splitColumnForMultipleChoiceValues: false,
      splitColumnForGroups: false,
    }
  };

  describe('Export with default options', () => {
    test('text variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Text', 'Export.mainGroup.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.textVariable\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Number', 'Export.mainGroup.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.numberVariable\r\n${caseID};3`);
    });

    test('output variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Output', 'Export.mainGroup.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.outputVariable\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Color', 'Export.mainGroup.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.colorVariable\r\n${caseID};#2e487a`);
    });

    test('date variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Date', 'Export.mainGroup.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.dateVariable\r\n${caseID};2023-12-08`);
    });

    test('time variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Time', 'Export.mainGroup.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.timeVariable\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Boolean', 'Export.mainGroup.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.booleanVariable\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainGroup.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.singlechoiceVariable\r\n${caseID};siNoValuesSet.si`);
    });

    test('multiplechoice variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainGroup.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.multiplechoiceVariable\r\n${caseID};bananaKiwiValuesSet.banana,bananaKiwiValuesSet.kiwi`);
    });

    test('dc instance', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.mainGroup.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.mainGroup.dcInstance.textInsideDC\r\n' + caseID + ';Testo DC');
    });

    test('group variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Group', 'Export.mainGroup.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.mainGroup.groupVariable.textInsideGroup\r\n' + caseID + ';Testo Group');
    });

    test('set variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Set', 'Export.mainGroup.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig)).find((c) => c.info.name === 'Export.mainGroup.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.mainGroup.setVariable[index0].textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Comune', 'Export.mainGroup.pluginVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.pluginVariable\r\n${caseID};C910`);
    });
  });

  describe('Export with exportLabelForValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainGroup.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.singlechoiceVariable\r\n${caseID};si`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainGroup.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.mainGroup.multiplechoiceVariable\r\n${caseID};banana, kiwi`);
    });
  });

  describe('Export with exportLabelForVariableNames true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForVariableNames = true;

    test('text variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Text', 'Export.mainGroup.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Text\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Number', 'Export.mainGroup.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Number\r\n${caseID};3`);
    });

    test('output variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Output', 'Export.mainGroup.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Output\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Color', 'Export.mainGroup.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Color\r\n${caseID};#2e487a`);
    });

    test('date variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Date', 'Export.mainGroup.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Date\r\n${caseID};2023-12-08`);
    });

    test('time variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Time', 'Export.mainGroup.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Time\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Boolean', 'Export.mainGroup.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Boolean\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainGroup.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Singlechoice\r\n${caseID};siNoValuesSet.si`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainGroup.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Multiplechoice\r\n${caseID};bananaKiwiValuesSet.banana,bananaKiwiValuesSet.kiwi`);
    });

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.mainGroup.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC\r\n' + caseID + ';Testo DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.mainGroup.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group\r\n' + caseID + ';Testo Group');
    });

    test('set variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Set', 'Export.mainGroup.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'Export.mainGroup.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.mainGroup.setVariable.textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Comune', 'Export.mainGroup.pluginVariable')];
      exportConfig.options!.exportLabelForVariableNames = true;
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Comune\r\n${caseID};C910`);
    });
  });

  describe('Export with exportLabelForValues and exportLabelForVariableNames true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForValues = true;
    exportConfig.options!.exportLabelForVariableNames = true;

    test('text variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Text', 'Export.mainGroup.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Text\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Number', 'Export.mainGroup.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Number\r\n${caseID};3`);
    });

    test('output variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Output', 'Export.mainGroup.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Output\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Color', 'Export.mainGroup.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Color\r\n${caseID};#2e487a`);
    });

    test('date variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Date', 'Export.mainGroup.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Date\r\n${caseID};2023-12-08`);
    });

    test('time variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Time', 'Export.mainGroup.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Time\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Boolean', 'Export.mainGroup.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Boolean\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainGroup.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Singlechoice\r\n${caseID};si`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainGroup.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Multiplechoice\r\n${caseID};banana, kiwi`);
    });

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.mainGroup.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC\r\n' + caseID + ';Testo DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.mainGroup.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group\r\n' + caseID + ';Testo Group');
    });

    test('set variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Set', 'Export.mainGroup.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'Export.mainGroup.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.mainGroup.setVariable.textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Comune', 'Export.mainGroup.pluginVariable')];
      exportConfig.options!.exportLabelForVariableNames = true;
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Comune\r\n${caseID};C910`);
    });
  });

  describe('Export with separateColumnForSingleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForSingleChoiceValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainGroup.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;siNoValuesSet.si;siNoValuesSet.no\r\n${caseID};true;false`);
    });
  });

  describe('Export with separateColumnForMultipleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForMultipleChoiceValues = true;

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainGroup.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;bananaKiwiValuesSet.banana;bananaKiwiValuesSet.kiwi\r\n${caseID};true;true`);
    });
  });

  describe('Export with separateColumnForSingleChoiceValues and separateColumnForMultipleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForSingleChoiceValues = true;
    exportConfig.options!.splitColumnForMultipleChoiceValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.mainGroup.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;siNoValuesSet.si;siNoValuesSet.no\r\n${caseID};true;false`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.mainGroup.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;bananaKiwiValuesSet.banana;bananaKiwiValuesSet.kiwi\r\n${caseID};true;true`);
    });
  });
});
import 'reflect-metadata';
import _ from 'lodash';
import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import FirstLevelCase from '../../../../fixtures/json/firstLevelCase.json';
import { CaseStorageForExportTests } from '../../../../mock-repositories/CaseStorageForExport';
import { EventsStorageForExport } from '../../../../mock-repositories/EventsStorageForExport';
import InMemoryCRFServiceForTest from '../../../../mock-services/InMemoryCRFServiceForTest';
import TestHelper from '../../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import { ExportableVariable } from "@/entities/exportable/ExportableVariable";
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { CaseDataExporterService, ExportConfiguration } from '@/services/CaseDataExporterService';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { Stringifier } from '@/utils/SerDes';

describe('CaseDataExporterService first level variables export', () => {
  let caseDataExporterService: CaseDataExporterService;
  const stringifier = new Stringifier<any>();
  const variablesToBeCached: Array<string> = [
    'Export.textVariable',
    'Export.numberVariable',
    'Export.outputVariable',
    'Export.colorVariable',
    'Export.dateVariable',
    'Export.timeVariable',
    'Export.booleanVariable',
    'Export.singlechoiceVariable',
    'Export.multiplechoiceVariable',
    'Export.dcInstance.textInsideDC',
    'Export.dcInstance.numberInsideDC',
    'Export.groupVariable.textInsideGroup',
    'Export.groupVariable.numberInsideGroup',
    'Export.setVariable[0].textInsideSet',
    'Export.pluginVariable',
  ];
  const caseID: string = FirstLevelCase.caseMetaData.caseID;
  const setItemID: string = stringifier.deserialize(FirstLevelCase.partialStoredCases[0].data).Export.setVariable[0].guid;

  beforeAll(async () => {
    TestHelper.setupContainer('Export', variablesToBeCached);
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const xml = TestHelper.requireFile('/export/variables-first-level.xml');
    //@ts-expect-error
    container.bind(NamedBindings.StorageForEvents).toDynamicValue(() => new EventsStorageForExport(FirstLevelCase)).inSingletonScope();
    //@ts-expect-error
    container.bind(NamedBindings.CaseStorage).toDynamicValue(() => new CaseStorageForExportTests(FirstLevelCase)).inSingletonScope();
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.CoreConfiguration) as ConsoleLogger;
    container.bind(NamedBindings.CRFService).toDynamicValue(() => new InMemoryCRFServiceForTest(logger, coreConfiguration,
      [new CRF('Export', '0.0.1', xml)], new Map<string, CRFVersionValidity[]>([['Export', [new CRFVersionValidity(0, '0.0.1', new Date('1990-12-12'))]],]))).inSingletonScope();
    const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
    await caseListService.updateCaseList();
    container.rebind(NamedBindings.CaseDataExporterService).to(CaseDataExporterService).inSingletonScope();
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST);
    caseDataExporterService = container.get(NamedBindings.CaseDataExporterService) as CaseDataExporterService;
  });

  const defaultExportConfig: ExportConfiguration = {
    variableList: [],
    excludedVariables: [],
    crfList: [],
    excludedCrfs: [],
    options: {
      exportLabelForValues: false,
      exportLabelForVariableNames: false,
      csvSeparator: ';',
      splitColumnForSingleChoiceValues: false,
      splitColumnForMultipleChoiceValues: false,
      splitColumnForGroups: false,
    }
  };

  describe('Export with default options', () => {
    test('text variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Text', 'Export.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.textVariable\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Number', 'Export.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.numberVariable\r\n${caseID};3`);
    });

    test('output variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Output', 'Export.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.outputVariable\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Color', 'Export.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.colorVariable\r\n${caseID};#37ff00`);
    });

    test('date variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Date', 'Export.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.dateVariable\r\n${caseID};2023-11-23`);
    });

    test('time variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Time', 'Export.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.timeVariable\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Boolean', 'Export.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.booleanVariable\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.singlechoiceVariable'),];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.singlechoiceVariable\r\n${caseID};siNoValuesSet.no`);
    });

    test('multiplechoice variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.multiplechoiceVariable'),];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.multiplechoiceVariable\r\n${caseID};bananaKiwiValuesSet.banana,bananaKiwiValuesSet.kiwi`);
    });

    test('dc instance', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.dcInstance.textInsideDC;Export.dcInstance.numberInsideDC\r\n' + caseID + ';Testo DC;Number DC');
    });

    test('group variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Group', 'Export.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.groupVariable.textInsideGroup;Export.groupVariable.numberInsideGroup\r\n' + caseID + ';Testo Group;Number Group');
    });

    test('set variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Set', 'Export.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig)).find((c) => c.info.name === 'Export.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.setVariable[index0].textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('set child', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Set', 'Export.setVariable[0]')];
      const excludedVariables: Array<string> = [];/*
      const returnArray = (await caseDataExporterService.toCSV({ variableList, options excludedVariables, })).find((c) => c.info.name === 'Export.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;index0;guid;Export.setVariable[index0].textInsideSet\r\n' + caseId + ';0;${setItemId};Testo Set');
       Eventually add this test for every export test file and for the various export options*/
    });

    test('plugin variable', async () => {
      defaultExportConfig.variableList = [new ExportableVariable('Comune', 'Export.pluginVariable')];
      const returnArray = (await caseDataExporterService.toCSV(defaultExportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.pluginVariable\r\n${caseID};C910`);
    });
  });

  describe('Export with exportLabelForValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.singlechoiceVariable\r\n${caseID};no`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Export.multiplechoiceVariable\r\n${caseID};banana, kiwi`);
    });
  });

  describe('Export with exportLabelForVariableNames true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForVariableNames = true;

    test('text variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Text', 'Export.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Text\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Number', 'Export.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Number\r\n${caseID};3`);
    });

    test('output variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Output', 'Export.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Output\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Color', 'Export.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Color\r\n${caseID};#37ff00`);
    });

    test('date variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Date', 'Export.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Date\r\n${caseID};2023-11-23`);
    });

    test('time variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Time', 'Export.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Time\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Boolean', 'Export.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Boolean\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Singlechoice\r\n${caseID};siNoValuesSet.no`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Multiplechoice\r\n${caseID};bananaKiwiValuesSet.banana,bananaKiwiValuesSet.kiwi`);
    });

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC;Number Inside DC\r\n' + caseID + ';Testo DC;Number DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group;Number Inside Group\r\n' + caseID + ';Testo Group;Number Group');
    });

    test('set variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Set', 'Export.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'Export.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.setVariable.textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Comune', 'Export.pluginVariable')];
      exportConfig.options!.exportLabelForVariableNames = true;
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Comune\r\n${caseID};C910`);
    });
  });

  describe('Export with exportLabelForValues and exportLabelForVariableNames true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForValues = true;
    exportConfig.options!.exportLabelForVariableNames = true;

    test('text variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Text', 'Export.textVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Text\r\n${caseID};Testo`);
    });

    test('number variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Number', 'Export.numberVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Number\r\n${caseID};3`);
    });

    test('output variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Output', 'Export.outputVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Output\r\n${caseID};Testo`);
    });

    test('color variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Color', 'Export.colorVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Color\r\n${caseID};#37ff00`);
    });

    test('date variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Date', 'Export.dateVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Date\r\n${caseID};2023-11-23`);
    });

    test('time variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Time', 'Export.timeVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Time\r\n${caseID};11:11`);
    });

    test('boolean variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Boolean', 'Export.booleanVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Boolean\r\n${caseID};true`);
    });

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Singlechoice\r\n${caseID};no`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Multiplechoice\r\n${caseID};banana, kiwi`);
    });

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC;Number Inside DC\r\n' + caseID + ';Testo DC;Number DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group;Number Inside Group\r\n' + caseID + ';Testo Group;Number Group');
    });

    test('set variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Set', 'Export.setVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'Export.setVariable')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;index0;guid;Export.setVariable.textInsideSet\r\n${caseID};0;${setItemID};Testo Set`);
    });

    test('plugin variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Comune', 'Export.pluginVariable')];
      exportConfig.options!.exportLabelForVariableNames = true;
      const returnArray = (await caseDataExporterService.toCSV(exportConfig)).find((c) => c.info.name === 'PLUGIN')?.returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;Comune\r\n${caseID};C910`);
    });
  });

  describe('Export with separateColumnForSingleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForSingleChoiceValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;siNoValuesSet.si;siNoValuesSet.no\r\n${caseID};false;true`);
    });
  });

  describe('Export with separateColumnForMultipleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForMultipleChoiceValues = true;

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;bananaKiwiValuesSet.banana;bananaKiwiValuesSet.kiwi\r\n${caseID};true;true`);
    });
  });

  describe('Export with separateColumnForSingleChoiceValues and separateColumnForMultipleChoiceValues true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForSingleChoiceValues = true;
    exportConfig.options!.splitColumnForMultipleChoiceValues = true;

    test('singlechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Singlechoice', 'Export.singlechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;siNoValuesSet.si;siNoValuesSet.no\r\n${caseID};false;true`);
    });

    test('multiplechoice variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Multiplechoice', 'Export.multiplechoiceVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual(`CASE_GUID;bananaKiwiValuesSet.banana;bananaKiwiValuesSet.kiwi\r\n${caseID};true;true`);
    });
  });

  describe('Export with separateColumnsForGroups true', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.splitColumnForGroups = true;

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.dcInstance.textInsideDC;Export.dcInstance.numberInsideDC\r\n' + caseID + ';Testo DC;Number DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Export.groupVariable.textInsideGroup;Export.groupVariable.numberInsideGroup\r\n' + caseID + ';Testo Group;Number Group');
    });
  });

  describe('Export with separateColumnsForGroups false', () => {
    const exportConfig = _.cloneDeep(defaultExportConfig);
    exportConfig.options!.exportLabelForVariableNames = true;

    test('dc instance', async () => {
      exportConfig.variableList = [new ExportableVariable('Dc Instance', 'Export.dcInstance')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside DC;Number Inside DC\r\n' + caseID + ';Testo DC;Number DC');
    });

    test('group variable', async () => {
      exportConfig.variableList = [new ExportableVariable('Group', 'Export.groupVariable')];
      const returnArray = (await caseDataExporterService.toCSV(exportConfig))[0].returnArray;
      expect(returnArray).toStrictEqual('CASE_GUID;Text Inside Group;Number Inside Group\r\n' + caseID + ';Testo Group;Number Group');
    });
  });

});

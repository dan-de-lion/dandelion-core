import 'reflect-metadata';
import { beforeAll, describe, expect, test } from 'vitest';
import { UserService } from '../../../mock-services/UserService';
import TestHelper from '../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { ConsoleLogger } from '@/services/Logger';
import { CrfServiceFromFolder } from '@/storages/CrfServiceFromFolder';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

describe('User Service', () => {
  let us: UserService;
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    us = new UserService(new CrfServiceFromFolder(
      getContainer(Contexts.TEST).get(NamedBindings.Logger) as ConsoleLogger,
      getContainer(Contexts.TEST).get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface));
  });

  describe('UserService returns correct accessRights', () => {
    test('SimpleUser', async () => {
      us.setCurrentUser('SimpleUser');
      expect(us.hasReadAccessRights('OnlyUser')).toBe(true);
      expect(us.hasWriteAccessRights('OnlyUser')).toBe(true);
      expect(us.hasReadAccessRights('public')).toBe(true);
      expect(us.hasWriteAccessRights('public')).toBe(true);
      expect(us.hasReadAccessRights('Admin')).toBe(false);
      expect(us.hasWriteAccessRights('Admin')).toBe(false);
      expect(us.hasReadAccessRights('aFakeIdentifier')).toBe(false);
      expect(us.hasWriteAccessRights('aFakeIdentifier')).toBe(false);
    });
  });
});
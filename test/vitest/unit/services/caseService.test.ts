import { v4 } from 'uuid';
import { beforeAll, beforeEach, describe, expect, test, vi } from 'vitest';
import { getContainer } from '@/ContainersConfig';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
import MockCRFService from '../../../mock-services/MockCRFService';
import { UserServiceWithWriteRights } from '../../../mock-services/UserServiceWithWriteRights';
import TestHelper from '../../../utils/TestHelper';
import { Case, CaseForUpdate } from '@/entities/Cases';
import type { Version } from '@/entities/CRF';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { ValuesSetValue } from '@/entities/ValuesSetValue';
import { type CoreConfigurationInterface, CaseConfiguration } from '@/interfaces/CoreConfiguration';
import CurrentContextService from '@/services/CurrentContextService';
import { CaseService } from '@/services/CaseService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { InMemoryKeyStorage } from '@/storages/InMemoryKeyStorage';
import { Stringifier } from '@/utils/SerDes';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { KeyStorageInterface } from '@/interfaces/KeyStorageInterface';
import { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import { CRFVersionSelectionStrategyInterface } from '@/interfaces/CRFVersionSelectionStrategyInterface';
import {  } from 'node:test';

let caseService: CaseServiceInterface;
let keyStorage: KeyStorageInterface;
let caseStorage: CaseStorageInterface;
let caseID: string;
let caseMetaData: CaseMetaData;

describe('Case Service', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    caseService = getContainer(Contexts.TEST).get(NamedBindings.CaseService) as CaseServiceInterface;
    keyStorage = getContainer(Contexts.TEST).get(NamedBindings.KeyStorage) as KeyStorageInterface;
    caseStorage = getContainer(Contexts.TEST).get(NamedBindings.CaseStorage) as CaseStorageInterface;
  });

  beforeEach(() => {
    caseID = v4();
    caseMetaData = {
      caseID,
      centreCode: '',
      caseType: 'test',
      status: new Map(),
      lastUpdate: new Date(),
      creationDate: new Date(),
      frozenAccessLevels: new Map(),
      crfVersions: new Map().set('test', '0.0.1'),
    }
  })

  test('Case service save empty partials with empty model', async () => {
    const partials = await testCreateCaseWithCaseService({ accessLevel: 'MyAccessLevel' }, {});
    expect(partials.length).toBe(2);
    expect(partials.map((p) => p.accessLevel)).toContain('public');
    expect(partials.map((p) => p.accessLevel)).toContain('MyAccessLevel');
    expect(partials.map((p) => JSON.parse(p.data))).toEqual([{}, {}]);
  });

  test('Case service save all in one partial when variables from the same access level', async () => {
    const accessLevels = {
      rootGroup: {
        accessLevel: 'MyAccessLevel',
      },
      varOutsideGroup: {
        accessLevel: 'MyAccessLevel',
      },
    };
    const model = {
      rootGroup: {
        varInsideGroup: 'valueInsideGroup',
        numberInsideGroup: 42,
      },
      varOutsideGroup: 'valueOutsideGroup',
    };

    const partials = await testCreateCaseWithCaseService(accessLevels, model);
    expect(partials.length).toEqual(2);
    expect(partials.map((p) => p.accessLevel)).toContain('public');
    expect(partials.map((p) => p.accessLevel)).toContain('MyAccessLevel');
    expect(JSON.parse(partials.find((p) => p.accessLevel == 'public')?.data!)).toEqual({});
    expect(JSON.parse(partials.find((p) => p.accessLevel == 'MyAccessLevel')?.data!)).toEqual(model);
  });

  test('Case service save variables in partials relative to their access level', async () => {
    const accessLevels = {
      trauma: {
        CasoClinico: {
          accessLevel: 'OnlyClinician',
          nome: {
            accessLevel: 'SensitiveData',
          },
          cognome: {
            accessLevel: 'SensitiveData',
          },
          dataNascita: {
            accessLevel: 'SensitiveData',
          },
          riscontro: {
            accessLevel: 'HospitalInCharge',
          },
        },
        DatiImportati: {
          NOME: {
            accessLevel: 'SensitiveData',
          },
          COGNOME: {
            accessLevel: 'SensitiveData',
          },
          DT_NASCITA: {
            accessLevel: 'SensitiveData',
          },
        },
        Logistica118: {
          accessLevel: 'OnlyAreu',
          ospedale: {
            accessLevel: 'HospitalInCharge',
          },
        },
        Compilazione118: {
          accessLevel: 'OnlyAreu',
        },
        CompilazioneOspedalieraCoreMinimo: {
          accessLevel: 'OnlyClinician',
        },
        CompilazioneOspedalieraDatiClinici: {
          accessLevel: 'OnlyClinician',
        },
      },
    };
    const model = {
      trauma: {
        CasoClinico: {
          riscontro: new ValuesSetValue('Appropriatezza.appropriato', 'Appropriato'),
          modalitaDiAccesso: new ValuesSetValue('ModalitaDiAccesso.trasferitoDaUnAltroOspedale', 'Trasferito da un altro ospedale'),
          nome: 'Mario',
          cognome: 'Rossi',
          dataNascita: '2000-01-01',
        },
        CompilazioneOspedalieraCoreMinimo: {
          sdoOspedale: '1231234561',
        },
        Logistica118: {
          ospedale: new ValuesSetValue('bergamo'),
          gestitoDaAreu: ''
        },
        DatiImportati: {
          COGNOME: '',
          NOME: '',
          DT_NASCITA: '',
          GEN: '',
        },
        Compilazione118: {
          tipoTrauma: '',
        },
        CompilazioneOspedalieraDatiClinici: {
          alertTraumaTeam: '',
        },
      },
    };
    const partials = await testCreateCaseWithCaseService(accessLevels, model);
    expect(partials.length).toEqual(5);
    expect(partials.map((p) => p.accessLevel)).toContain('public');
    expect(partials.map((p) => p.accessLevel)).toContain('SensitiveData');
    expect(partials.map((p) => p.accessLevel)).toContain('HospitalInCharge');
    expect(partials.map((p) => p.accessLevel)).toContain('OnlyAreu');
    expect(partials.map((p) => p.accessLevel)).toContain('OnlyClinician');
    expect(new Stringifier().deserialize(partials.find((p) => p.accessLevel == 'public')?.data!)).toEqual({
      trauma: {
        DatiImportati: {
          GEN: '',
        },
      },
    });
    expect(new Stringifier().deserialize(partials.find((p) => p.accessLevel == 'SensitiveData')?.data!)).toEqual({
      trauma: {
        CasoClinico: {
          nome: 'Mario',
          cognome: 'Rossi',
          dataNascita: '2000-01-01',
        },
        DatiImportati: {
          COGNOME: '',
          DT_NASCITA: '',
          NOME: '',
        },
      },
    });
    expect(new Stringifier().deserialize(partials.find((p) => p.accessLevel == 'HospitalInCharge')?.data!)).toEqual({
      trauma: {
        CasoClinico: {
          riscontro: new ValuesSetValue('Appropriatezza.appropriato', 'Appropriato', NaN),
        },
        Logistica118: {
          ospedale: new ValuesSetValue('bergamo', 'bergamo', NaN),
        },
      },
    });
    expect(new Stringifier().deserialize(partials.find((p) => p.accessLevel == 'OnlyAreu')?.data!)).toEqual({
      trauma: {
        Compilazione118: {
          tipoTrauma: '',
        },
        Logistica118: {
          gestitoDaAreu: '',
        },
      },
    });
    expect(new Stringifier().deserialize(partials.find((p) => p.accessLevel == 'OnlyClinician')?.data!)).toEqual({
      trauma: {
        CasoClinico: {
          modalitaDiAccesso: new ValuesSetValue('ModalitaDiAccesso.trasferitoDaUnAltroOspedale', 'Trasferito da un altro ospedale', NaN),
        },
        CompilazioneOspedalieraCoreMinimo: {
          sdoOspedale: '1231234561',
        },
        CompilazioneOspedalieraDatiClinici: {
          alertTraumaTeam: '',
        },
      },
    });
  });

  describe('Keys creation', () => {
    beforeAll(() => {
      keyStorage = new InMemoryKeyStorage();
      caseStorage = new InMemoryCaseStorage();
      const crfService = new MockCRFService();
      crfService.getAccessLevelsForCRF = (_n: string, _v: Version) => {
        return {
          rootGroup: {
            accessLevel: 'MyAccessLevel',
          },
          varOutsiddddeGroup: {
            accessLevel: 'MyOtherAccessLevel',
          },
        };
      };
      const currentContextService = new CurrentContextService(Contexts.TEST, new CaseConfiguration({ caseName: 'test', referenceDateVariableName: '' }));
      const coreConfiguration = { ...getContainer(Contexts.TEST).get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface, encrypt: true };
      const userService = new UserServiceWithWriteRights();
      const crfSelector = getContainer(Contexts.TEST).get(NamedBindings.CRFVersionSelectionStrategy) as CRFVersionSelectionStrategyInterface
      caseService = new CaseService(caseStorage, keyStorage, userService, currentContextService, crfService, coreConfiguration, crfSelector,);
    });

    test('Case service creates encryption key for each and only the accessLevel on patient creation', async () => {
      const { key1, key2, keyPublic } = await createAndGetKeys();
      expect(key1).not.toBeFalsy();
      expect(key2).not.toBeFalsy();
      expect(keyPublic).not.toBeFalsy();
      await expect(keyStorage.getKey(caseID, "AccessLevelNotPresent")).rejects.toThrow();
    });

    test('Keys are saved before data', async () => {
      const keySpy = vi.spyOn(keyStorage, 'saveKey');
      const dataSpy = vi.spyOn(caseStorage, 'createCase');
      await createAndGetKeys();
      expect(keySpy.mock.invocationCallOrder).toEqual([1, 2, 3]);
      expect(dataSpy.mock.invocationCallOrder).toEqual([4]);
    });

    test('Case update does not update keys', async () => {
      const { key1, key2, keyPublic } = await createAndGetKeys();
      await caseService.update(new CaseForUpdate(caseID, {}, caseMetaData));
      expect(key1).toEqual(await keyStorage.getKey(caseID, "MyAccessLevel"));
      expect(key2).toEqual(await keyStorage.getKey(caseID, "MyOtherAccessLevel"));
      expect(keyPublic).toEqual(await keyStorage.getKey(caseID, "public"));
    });
  })

  async function testCreateCaseWithCaseService(accessLevels: any, model: any) {
    const crfService = new MockCRFService();
    crfService.getAccessLevelsForCRF = (_n: string, _v: Version) => accessLevels;
    const currentContextService = new CurrentContextService(Contexts.TEST, new CaseConfiguration({ caseName: 'test', referenceDateVariableName: '' }));
    const coreConfiguration = getContainer(Contexts.TEST).get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const userService = new UserServiceWithWriteRights();
    const crfSelector = getContainer(Contexts.TEST).get(NamedBindings.CoreConfiguration) as CRFVersionSelectionStrategyInterface;
    const caseService = new CaseService(caseStorage, new InMemoryKeyStorage(), userService, currentContextService, crfService, coreConfiguration, crfSelector);
    await caseService.create(new Case(caseID, model, caseMetaData));
    return await caseStorage.getPartials(caseID);
  }

  async function createAndGetKeys() {
    await caseService.create(new Case(caseID, {}, caseMetaData));
    const key1 = await keyStorage.getKey(caseID, "MyAccessLevel");
    const key2 = await keyStorage.getKey(caseID, "MyOtherAccessLevel");
    const keyPublic = await keyStorage.getKey(caseID, "public");
    return { key1, key2, keyPublic };
  }
});
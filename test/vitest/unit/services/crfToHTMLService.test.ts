import path from 'path';
import { beforeAll, describe, expect, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import { ApplicationError, AppErrorType } from '@/errors/ApplicationError';
import { CoreError, CoreErrorType } from '@/errors/CoreError';
import { CRFError, CRFErrorType } from '@/errors/CRFError';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import { CRFToHTMLService } from '@/services/CRFToHTMLService';
import { Language } from '@/services/TranslationService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';

describe('CRFToHTMLService', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });

  test('should return an error if the xml is empty', async () => {
    const crfToHtmlService = new CRFToHTMLService(
      new CoreConfiguration({
        baseUrlForXslt: 'public/xslt/',
        debug: true,
        statisticianMode: false,
        defaultLanguage: Language.it,
        listOfCaseTypes: [new CaseConfiguration({ caseName: 'FastCompilation', referenceDateVariableName: 'model.textVariable' })],
        defaultRequiredForStatus: { general: 3, errors: 2, warnings: 2 },
      }));
    await expect(crfToHtmlService.getHTMLfromXML('', 'emptyString', '0.0.1', `/public/xslt/${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`))
      .rejects.toEqual(new ApplicationError('Generic error in getHTMLfromXML', AppErrorType.GENERIC_ERROR,
        new CRFError('Starting Dataclass [0] absent in CRF emptyString v0.0.1?', CRFErrorType.STARTING_DATACLASS_NOT_FOUND)));
  });

  test('should return an error if baseURL is not correct', async () => {
    const xml = TestHelper.requireFile('/textVariable/standard-text-variable.xml');
    const coreConfiguration = new CoreConfiguration({
      baseUrlForXslt: '/public/wrongPath/',
      debug: true,
      statisticianMode: false,
      defaultLanguage: Language.it,
      listOfCaseTypes: [new CaseConfiguration({ caseName: 'FastCompilation', referenceDateVariableName: 'model.textVariable' })],
      defaultRequiredForStatus: { general: 3, errors: 2, warnings: 2 },
    });
    const crfToHtmlService = new CRFToHTMLService(coreConfiguration);
    await expect(crfToHtmlService.getHTMLfromXML(xml, 'emptyString', '0.0.1', `${coreConfiguration.baseUrlForXslt}${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`))
      .rejects.toEqual(new ApplicationError('Generic error in getHTMLfromXML', AppErrorType.GENERIC_ERROR,
        new CoreError('processor.importStylesheetURI failed for XSLT: /public/wrongPath/Schemas/SchemaToCollectionPage.xsl', CoreErrorType.XSLT_NOT_FOUND)));
  });

  test('should return an error when xml is malformed', async () => {
    const crfToHtmlService = new CRFToHTMLService(
      new CoreConfiguration({
        baseUrlForXslt: 'public/xslt/',
        debug: true,
        statisticianMode: false,
        defaultLanguage: Language.it,
        listOfCaseTypes: [new CaseConfiguration({ caseName: 'FastCompilation', referenceDateVariableName: 'model.textVariable' })],
        defaultRequiredForStatus: { general: 3, errors: 2, warnings: 2 },
      })
    );
    await expect(crfToHtmlService.getHTMLfromXML('<dataClass>', 'malformed', '0.0.1', `/public/xslt/${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`))
      .rejects.toEqual(new ApplicationError('Generic error in getHTMLfromXML', AppErrorType.GENERIC_ERROR,
        new CRFError('Starting Dataclass [0] absent in CRF malformed v0.0.1?', CRFErrorType.STARTING_DATACLASS_NOT_FOUND)));
  });

  describe('Boolean variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/booleanVariable/custom-label-boolean-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('customLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/booleanVariable/group-boolean-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model-for-boolean')).toBe("'groupVariable.booleanVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.booleanVariable'");
    });

    test('should have computedFormula true attribute', async () => {
      const document = await transformAndGetDocumentFragment('/booleanVariable/computedformula-true-boolean-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-short-circuit')).toBe('true');
    });

    test('should have computedFormula false attribute', async () => {
      const document = await transformAndGetDocumentFragment('/booleanVariable/computedformula-false-boolean-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-short-circuit')).toBe('false');
    });

    test('should have disable attribute', async () => {
      const document = await transformAndGetDocumentFragment('/booleanVariable/standard-boolean-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute(':disabled')).not.toBe(null);
    });
  });

  describe('Color variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/colorVariable/custom-label-color-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('customLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/colorVariable/group-color-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model')).toBe("'groupVariable.colorVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.colorVariable'");
    });

    test('should have computedFormula attributes', async () => {
      const document = await transformAndGetDocumentFragment('/colorVariable/computedformula-color-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-short-circuit')).toBe("'#669c35'");
    });
  });

  describe('DataClass variable', () => {
    test('should create the correct html from standard-dataclass-variable.xml', async () => {
      const document = await transformAndGetDocumentFragment('/dataclassVariable/standard-dataclass-variable.xml');
      const fieldset = document.querySelector('fieldset') as HTMLFieldSetElement;
      const template = document.querySelector('template') as HTMLTemplateElement;
      const div = template.content.querySelectorAll('div')[0];
      const input = template.content.querySelectorAll('input')[0];
      expect(fieldset?.getAttribute('v-if')).toBe("currentCaseService?.canRead('dcInstance','standard-dataclass-variable','0.0.1')");
      expect(div.getAttribute('v-if')).toBe("currentCaseService?.canRead('dcInstance.textVariable','standard-dataclass-variable','0.0.1')");
      expect(input.getAttribute('v-custom-model')).toBe("'dcInstance.textVariable'");
    });
  });

  describe('Date variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/dateVariable/custom-label-date-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('dateCustomLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/dateVariable/group-date-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model')).toBe("'groupVariable.dateVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.dateVariable'");
    });

    test('should have computedFormula attributes', async () => {
      const document = await transformAndGetDocumentFragment('/dateVariable/computedformula-date-variable.xml');
      const input = document.querySelectorAll('input')[1] as HTMLInputElement;
      expect(input.getAttribute('v-short-circuit')).toBe('model.dateVariable');
    });
  });

  describe('Group variable', () => {
    test('should create the correct html from standard-group-variable.xml', async () => {
      const document = await transformAndGetDocumentFragment('/groupVariable/standard-group-variable.xml');
      const fieldset = document.querySelector('fieldset');
      expect(fieldset?.getAttribute('v-custom-model')).toBe("'groupVariable'");
    });
  });

  describe('Multiplechoice variable', () => {
    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/group-multiplechoice-variable.xml');
      const ul = document.querySelector('ul') as HTMLUListElement;
      expect(ul.getAttribute('v-custom-model-for-checkboxes')).toBe("'groupVariable.multiplechoiceVariable'");
      expect(ul.getAttribute(':id')).toBe("getContextForID + 'groupVariable.multiplechoiceVariable'");
    });

    test('should have correct ID for values wrappers', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const valuesLIs = document.querySelectorAll('li') as NodeListOf<HTMLLIElement>;
      expect(valuesLIs[0].getAttribute(':id')).toBe("getContextForID + 'multiplechoiceVariable-sets.first-wrapper'");
      expect(valuesLIs[1].getAttribute(':id')).toBe("getContextForID + 'multiplechoiceVariable-sets.second-wrapper'");
      expect(valuesLIs[2].getAttribute(':id')).toBe("getContextForID + 'multiplechoiceVariable-sets.third-wrapper'");
    });

    test('should have correct ID for values', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':id')).toBe("getContextForID + 'multiplechoiceVariable-sets.first'");
      expect(valuesInputs[1].getAttribute(':id')).toBe("getContextForID + 'multiplechoiceVariable-sets.second'");
      expect(valuesInputs[2].getAttribute(':id')).toBe("getContextForID + 'multiplechoiceVariable-sets.third'");
    });

    test('should have correct name for values', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':name')).toBe("getContextForID + 'multiplechoiceVariable'");
      expect(valuesInputs[1].getAttribute(':name')).toBe("getContextForID + 'multiplechoiceVariable'");
      expect(valuesInputs[2].getAttribute(':name')).toBe("getContextForID + 'multiplechoiceVariable'");
    });

    test("should have correct 'for' for labels", async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const valuesLabels = document.querySelectorAll('li label') as NodeListOf<HTMLLIElement>;
      expect(valuesLabels[0].getAttribute(':for')).toBe("getContextForID + 'multiplechoiceVariable-sets.first'");
      expect(valuesLabels[1].getAttribute(':for')).toBe("getContextForID + 'multiplechoiceVariable-sets.second'");
      expect(valuesLabels[2].getAttribute(':for')).toBe("getContextForID + 'multiplechoiceVariable-sets.third'");
    });

    test('should have correct ID for values wrappers when in set', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/in-set-multiplechoice-variable.xml');
      const valuesLIs = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li') as NodeListOf<HTMLLIElement>;
      expect(valuesLIs[0].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.first-wrapper'");
      expect(valuesLIs[1].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.second-wrapper'");
      expect(valuesLIs[2].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.third-wrapper'");
    });

    test('should have correct ID for values when in set', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/in-set-multiplechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.first'");
      expect(valuesInputs[1].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.second'");
      expect(valuesInputs[2].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.third'");
    });

    test('should have correct name for values when in set', async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/in-set-multiplechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':name')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable'");
      expect(valuesInputs[1].getAttribute(':name')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable'");
      expect(valuesInputs[2].getAttribute(':name')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable'");
    });

    test("should have correct 'for' for labels when in set", async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/in-set-multiplechoice-variable.xml');
      const valuesLabels = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li label') as NodeListOf<HTMLLIElement>;
      expect(valuesLabels[0].getAttribute(':for')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.first'");
      expect(valuesLabels[1].getAttribute(':for')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.second'");
      expect(valuesLabels[2].getAttribute(':for')).toBe("getContextForID + 'mySet[' + index0 + '].multiplechoiceVariable-sets.third'");
    });

    test("should have correct 'canWrite' on input when in set", async () => {
      const document = await transformAndGetDocumentFragment('/multiplechoiceVariable/in-set-multiplechoice-variable.xml');
      const valuesLabels = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesLabels[0].getAttribute(':disabled')).toContain("canWrite('mySet[' + index0 + '].multiplechoiceVariable',");
      expect(valuesLabels[1].getAttribute(':disabled')).toContain("canWrite('mySet[' + index0 + '].multiplechoiceVariable',");
      expect(valuesLabels[2].getAttribute(':disabled')).toContain("canWrite('mySet[' + index0 + '].multiplechoiceVariable',");
    });
  });

  describe('Number variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/numberVariable/custom-label-number-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('customLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/numberVariable/group-number-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model')).toBe("'groupVariable.numberVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.numberVariable'");
    });
  });

  describe('Set variable', () => {
    test('should be correctly placed in a variable group', async () => {
      const htmlString = await getHtmlString('/setVariable/group-set-variable.xml');
      expect(htmlString).toContain(`v-custom-model-for-sets="'groupVariable.setVariable'"`);
      expect(htmlString).toContain(`:id="getContextForID + 'groupVariable.setVariable'"`);
    });

    test('Should set correct id of nested set', async () => {
      const htmlString = await getHtmlString('/setVariable/nested-set-variable.xml');
      expect(htmlString.includes(`v-on:click.stop="currentPageService.set(getContextForID + 'setVariable[' + index0 + ']')"`)).toBeTruthy();
      expect(htmlString.includes(`currentPageService.set(getContextForID + 'setVariable[' + index0 + '].setChild[' + (thisSet.length - 1) + ']')`)).toBeTruthy();
      expect(htmlString.includes(`v-on:click.stop="currentPageService.set(getContextForID + 'setVariable[' + index0 + '].setChild[' + index1 + ']')"`)).toBeTruthy();
    });

    test('should set have correct canRead', async () => {
      const htmlString = await getHtmlString('/setVariable/set-with-separate-in-page.xml');
      expect(htmlString).toContain("canRead('setVariable[' + index0 + '].textVariable','set-with-separate-in-page','0.0.1')");
    });

    test('should correctly have hooks and onHover inside', async () => {
      const document = await transformAndGetDocumentFragment('/setVariable/standard-set-variable.xml');
      const beforeHook = document.querySelector('component') as HTMLElement;
      const beforeHookText = beforeHook.getAttribute('v-for');
      const fieldset = document.querySelector('fieldset') as HTMLFieldSetElement;
      const onHoverSet = fieldset.getAttribute('v-on:mouseover.stop');
      const set = fieldset.getElementsByTagName('setcomponent')[0] as HTMLDivElement;
      const setTemplate = set.getElementsByTagName('template')[0] as HTMLTemplateElement;
      const setItem = setTemplate.content.querySelector('setitemcomponent') as HTMLDivElement;
      const setItemTemplate = setItem.getElementsByTagName('template')[0].content.querySelector('template') as HTMLTemplateElement;
      const dataClass = setItemTemplate.content.querySelector('.itemsDC') as HTMLDivElement;
      const subVariable = dataClass.getElementsByTagName('div')[0] as HTMLDivElement;
      const onHoverSubVariable = subVariable.getAttribute('v-on:mouseover.stop');
      const afterHook = setTemplate.content.lastChild as HTMLSpanElement;
      const afterHooktext = afterHook.getAttribute('v-for');
      expect(beforeHookText).toBe('componentName in getHooksByType(TypesOfDisplayHooks.BeforeVariable)');
      expect(onHoverSet).toBe("onHover('setVariable')");
      expect(onHoverSubVariable).toBe("onHover('setVariable[' + index0 + '].textVariable')");
      expect(afterHooktext).toBe('componentName in getHooksByType(TypesOfDisplayHooks.AfterVariable)');
    });
  });

  describe('Singlechoice dropdown variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceDropdownVariable/custom-label-singlechoice-dropdown-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('dropCustomLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceDropdownVariable/group-singlechoice-dropdown-variable.xml');
      const select = document.querySelector('select') as HTMLSelectElement;
      expect(select.getAttribute('v-custom-model-for-select')).toBe("'groupVariable.dropVariable'");
      expect(select.getAttribute(':id')).toBe("getContextForID + 'groupVariable.dropVariable'");
    });
  });

  describe('Singlechoice variable', () => {
    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/group-singlechoice-variable.xml');
      const ul = document.querySelector('ul') as HTMLUListElement;
      expect(ul.getAttribute('v-custom-model-for-singlechoice')).toBe("'groupVariable.singlechoiceVariable'");
      expect(ul.getAttribute(':id')).toBe("getContextForID + 'groupVariable.singlechoiceVariable'");
    });

    test('should have disable attribute', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/computedformula-singlechoice-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      const input2 = document.querySelectorAll('input')[1] as HTMLInputElement;
      const input3 = document.querySelectorAll('input')[2] as HTMLInputElement;
      expect(input.getAttribute(':disabled')).not.toBe(null);
      expect(input2.getAttribute(':disabled')).not.toBe(null);
      expect(input3.getAttribute(':disabled')).not.toBe(null);
    });

    test('should have correct ID for values wrappers', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const valuesLIs = document.querySelectorAll('li') as NodeListOf<HTMLLIElement>;
      expect(valuesLIs[0].getAttribute(':id')).toBe("getContextForID + 'singlechoiceVariable-sets.first-wrapper'");
      expect(valuesLIs[1].getAttribute(':id')).toBe("getContextForID + 'singlechoiceVariable-sets.second-wrapper'");
      expect(valuesLIs[2].getAttribute(':id')).toBe("getContextForID + 'singlechoiceVariable-sets.third-wrapper'");
    });

    test('should have correct ID for values', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':id')).toBe("getContextForID + 'singlechoiceVariable-sets.first'");
      expect(valuesInputs[1].getAttribute(':id')).toBe("getContextForID + 'singlechoiceVariable-sets.second'");
      expect(valuesInputs[2].getAttribute(':id')).toBe("getContextForID + 'singlechoiceVariable-sets.third'");
    });

    test('should have correct name for values', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':name')).toBe("getContextForID + 'singlechoiceVariable'");
      expect(valuesInputs[1].getAttribute(':name')).toBe("getContextForID + 'singlechoiceVariable'");
      expect(valuesInputs[2].getAttribute(':name')).toBe("getContextForID + 'singlechoiceVariable'");
    });

    test('should have correct "for" for labels', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const valuesLabels = document.querySelectorAll('li label') as NodeListOf<HTMLLIElement>;
      expect(valuesLabels[0].getAttribute(':for')).toBe("getContextForID + 'singlechoiceVariable-sets.first'");
      expect(valuesLabels[1].getAttribute(':for')).toBe("getContextForID + 'singlechoiceVariable-sets.second'");
      expect(valuesLabels[2].getAttribute(':for')).toBe("getContextForID + 'singlechoiceVariable-sets.third'");
    });

    test('should have correct ID for values wrappers when in set', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/in-set-singlechoice-variable.xml');
      const valuesLIs = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li') as NodeListOf<HTMLLIElement>;
      expect(valuesLIs[0].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.first-wrapper'");
      expect(valuesLIs[1].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.second-wrapper'");
      expect(valuesLIs[2].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.third-wrapper'");
    });

    test('should have correct ID for values when in set', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/in-set-singlechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.first'");
      expect(valuesInputs[1].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.second'");
      expect(valuesInputs[2].getAttribute(':id')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.third'");
    });

    test('should have correct name for values when in set', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/in-set-singlechoice-variable.xml');
      const valuesInputs = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesInputs[0].getAttribute(':name')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable'");
      expect(valuesInputs[1].getAttribute(':name')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable'");
      expect(valuesInputs[2].getAttribute(':name')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable'");
    });

    test('should have correct "for" for labels when in set', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/in-set-singlechoice-variable.xml');
      const valuesLabels = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li label') as NodeListOf<HTMLLIElement>;
      expect(valuesLabels[0].getAttribute(':for')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.first'");
      expect(valuesLabels[1].getAttribute(':for')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.second'");
      expect(valuesLabels[2].getAttribute(':for')).toBe("getContextForID + 'mySet[' + index0 + '].singlechoiceVariable-sets.third'");
    });

    test('should have correct "canWrite" on input when in set', async () => {
      const document = await transformAndGetDocumentFragment('/singlechoiceVariable/in-set-singlechoice-variable.xml');
      const valuesLabels = document.querySelectorAll('template')[0].content.querySelectorAll('template')[0].content.querySelectorAll('template')[0]
        .content.querySelectorAll('li input') as NodeListOf<HTMLLIElement>;
      expect(valuesLabels[0].getAttribute(':disabled')).toContain("canWrite('mySet[' + index0 + '].singlechoiceVariable',");
      expect(valuesLabels[1].getAttribute(':disabled')).toContain("canWrite('mySet[' + index0 + '].singlechoiceVariable',");
      expect(valuesLabels[2].getAttribute(':disabled')).toContain("canWrite('mySet[' + index0 + '].singlechoiceVariable',");
    });
  });

  describe('Support variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/supportVariable/custom-label-support-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('customLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/supportVariable/group-support-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model')).toBe("'groupVariable.supportVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.supportVariable'");
    });
  });

  describe('Text variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/textVariable/custom-label-text-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('customLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/textVariable/group-text-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model')).toBe("'groupVariable.textVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.textVariable'");
    });
  });

  describe('Time variable', () => {
    test('should have a custom label', async () => {
      const document = await transformAndGetDocumentFragment('/timeVariable/custom-label-time-variable.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(label.innerHTML).toContain("translationService.t('testLabel')");
    });

    test('should be correctly placed in a variable group', async () => {
      const document = await transformAndGetDocumentFragment('/timeVariable/group-time-variable.xml');
      const input = document.querySelector('input') as HTMLInputElement;
      expect(input.getAttribute('v-custom-model')).toBe("'groupVariable.timeVariable'");
      expect(input.getAttribute(':id')).toBe("getContextForID + 'groupVariable.timeVariable'");
    });
  });

  describe('Reference variable', () => {
    test('should have the label of the reference variable', async () => {
      const document = await transformAndGetDocumentFragment('/reference/text-with-reference.xml');
      const label = document.querySelector('label') as HTMLLabelElement;
      const referenceLabel = document.querySelectorAll('label')[1] as HTMLLabelElement;
      expect(label).toBeDefined();
      expect(referenceLabel).toBeDefined();
      expect(label.innerHTML).toStrictEqual(referenceLabel.innerHTML);
    });

    test('should have different IDs for the reference and regular variable', async () => {
      const document = await transformAndGetDocumentFragment('/reference/text-with-reference.xml');
      const regularVariable = document.querySelectorAll('div')[0] as HTMLDivElement;
      const referenceVariable = document.querySelectorAll('div')[1] as HTMLDivElement;
      const regularVariableID = regularVariable.getAttribute(':id');
      const referenceVariableID = referenceVariable.getAttribute(':id');
      expect(regularVariableID).toBeTruthy();
      expect(referenceVariableID).toBeTruthy();
      expect(regularVariableID).not.toEqual(referenceVariableID);
    });

    test("should have same 'for' of label and 'id' of input in reference variable", async () => {
      const document = await transformAndGetDocumentFragment('/reference/text-with-reference.xml');
      const referenceVariable = document.querySelectorAll('div')[1] as HTMLDivElement;
      const referenceLabel = referenceVariable.querySelector('label') as HTMLLabelElement;
      const referenceInput = referenceVariable.querySelector('input') as HTMLInputElement;
      const inputId = referenceInput.getAttribute(':id');
      const labelFor = referenceLabel.getAttribute(':for');
      expect(labelFor).toBeTruthy();
      expect(labelFor).toEqual(inputId);
    });

    test('should have same IDs for the reference variable and her input', async () => {
      const document = await transformAndGetDocumentFragment('/reference/text-with-reference.xml');
      const referenceVariable = document.querySelectorAll('div')[1] as HTMLDivElement;
      const referenceInput = referenceVariable.getElementsByTagName('input')[0] as HTMLInputElement;
      const referenceId = referenceVariable.getAttribute(':id');
      const inputID = referenceInput.getAttribute(':id');
      expect(referenceId).toEqual(`${inputID!.slice(0, -1)}-wrapper'`);
    });
  });

  describe('Errors', () => {
    test('Errors should have the correct id and v-custom-model', async () => {
      const document = await transformAndGetDocumentFragment('/errors/text-variable-error.xml');
      const error = document.querySelector('.error') as HTMLDivElement;
      const errorId = error.getAttribute(':id');
      const errorCustomModel = error.getAttribute('v-custom-model-for-errors');
      expect(errorId).toBe("getContextForID + 'groupVariable.ERROR_myTestError'");
      expect(errorCustomModel).toBe("'groupVariable.ERROR_myTestError'");
    });
  });

  describe('Warning', () => {
    test('Warnings should have the correct id and v-custom-model', async () => {
      const document = await transformAndGetDocumentFragment('/warnings/text-variable-warning.xml');
      const elementSection = document.querySelector('section') as HTMLDivElement;
      const elementFieldset = elementSection.lastChild as HTMLFieldSetElement;
      const warning = elementFieldset.querySelector('.warning') as HTMLDivElement;
      const warningId = warning.getAttribute(':id');
      const warningInput = warning.getElementsByTagName('input')[0] as HTMLDivElement;
      const warningCustomModel = warningInput.getAttribute('v-custom-model-for-warnings');
      expect(warningId).toBe("getContextForID + 'groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper'");
      expect(warningCustomModel).toBe("'groupVariable.WARNING_myTestWarning_CONFIRMED'");
    });

    test("Warnings should have same 'for' of label and 'id' of input", async () => {
      const document = await transformAndGetDocumentFragment('/warnings/text-variable-warning.xml');
      const elementSection = document.querySelector('section') as HTMLDivElement;
      const warning = elementSection.lastChild as HTMLDivElement;
      const warningLabel = warning.querySelector('label') as HTMLLabelElement;
      const warningInput = warning.querySelector('input') as HTMLInputElement;
      const inputId = warningInput.getAttribute(':id');
      const labelFor = warningLabel.getAttribute(':for');
      expect(labelFor).toBeTruthy();
      expect(labelFor).toEqual(inputId);
    });
  });

  describe('Missings', () => {
    test('Missings should have the correct id and v-custom-model', async () => {
      const document = await transformAndGetDocumentFragment('/requirements/text-variable-requirement.xml');
      const requirement = document.querySelector('.requirement') as HTMLDivElement;
      const requirementId = requirement.getAttribute(':id');
      const requirementCustomModel = requirement.getAttribute('v-custom-model-for-requirements');
      expect(requirementId).toBe("getContextForID + 'groupVariable.REQUIREMENT_myTestRequirement'");
      expect(requirementCustomModel).toBe("'groupVariable.REQUIREMENT_myTestRequirement'");
    });
  });

  describe('v-on:mouseover.stop have correct name', () => {
    test('Fieldset', async () => {
      const document = await transformAndGetDocumentFragment('/computedFormula/computedFormula-with-values-instances-dataclass.xml');
      const fieldset = document.querySelector('fieldset');
      expect(fieldset?.getAttribute('v-on:mouseover.stop')).toBe("onHover('dataclass')");
    });
  });

  describe('hooks should have correct name', () => {
    test('component', async () => {
      const document = await transformAndGetDocumentFragment('/thisSet/set-in-groups.xml');
      const component = document.querySelectorAll('component');
      expect(component[2]?.getAttribute(':full-name')).toBe("'groupVariable.dcInstance'");
    });
  });

  describe('dataClassItemComponent have correct fullName', () => {
    test('span', async () => {
      const document = await transformAndGetDocumentFragment('/thisSet/set-in-groups.xml');
      const dataClassItemComponent = document.querySelectorAll('dataclassitemcomponent');
      expect(dataClassItemComponent[0]?.getAttribute(':full-name')).toBe("'groupVariable.dcInstance'");
    });
  });

  // todo: investigate why this test is failing on the CI
  // describe('dataClassItemComponent inside Set have correct thisIndex', () => {
  //   test('first level', async () => {
  //     const document = await getDocumentFragment('/page/separate-in-page-with-dataclass-in-set.xml');
  //     const section = document.querySelector('section');
  //     const fieldSet = section?.querySelector('fieldSet');
  //     const setComponent = fieldSet?.querySelector('setcomponent');
  //     const ul = setComponent?.querySelector('template')?.content.querySelector('ul');
  //     const li = ul?.querySelector('li');
  //     const setItemComponent = li?.querySelector('setitemcomponent');
  //     const dataClassItemComponent = setItemComponent?.querySelector('template')?.content.querySelector('dataclassitemcomponent');
  //     expect(dataClassItemComponent?.getAttribute(':this-index')).toBe('index0');
  //   });
  // });

  describe('Values inside Singlechoice (without valuesSet) have correct value', () => {
    test('first level', async () => {
      const document = await transformAndGetDocumentFragment('/valuesSetValue/singlechoice/value-inside-singlechoice.xml');
      const section = document?.querySelector('section');
      const div = section?.querySelector('div');
      const ul = div?.querySelector('ul');
      const li = ul?.querySelector('li');
      const input = li?.querySelector('input');
      expect(input?.getAttribute('value')).toBe('one');
    });
  });

  describe('Excludes', () => {
    test("should have correct 'excludes' on input", async () => {
      const document = await transformAndGetDocumentFragment('/excludes/standard-excludes.xml');
      const input = document.querySelectorAll('input');
      expect(input[0].getAttribute('excludes')).toBe('second,');
      expect(input[1].getAttribute('excludes')).toBe('first,');
    });

    test('Excludes with name and * should be set correctly inside different ValuesGroup', async () => {
      const document = await transformAndGetDocumentFragment('/excludes/excludes-in-different-valuesGroup-with-variable-name-and-star.xml');
      const input = document.querySelectorAll('input');
      expect(input[0].getAttribute('excludes')).toBe('group.second,group2.fifth,');
      expect(input[1].getAttribute('excludes')).toBe('group2.third,group2.fifth,group.first,');
      expect(input[2].getAttribute('excludes')).toBe('group2.fourth,group2.fifth,group.second,');
      expect(input[3].getAttribute('excludes')).toBe('group2.fifth,group2.third,');
      expect(input[4].getAttribute('excludes')).toBe('group2.third,group2.fourth,group.first,group.second,');
    });

    test('Excludes with same name inside different ValuesGroup should be correctly set', async () => {
      const document = await transformAndGetDocumentFragment('/excludes/excludes-with-multiple-value-with-same-name.xml');
      const input = document.querySelectorAll('input');
      expect(input[0].getAttribute('excludes')).toBe('group1.b,group2.a,group2.b,');
      expect(input[1].getAttribute('excludes')).toBe('group1.a,');
      expect(input[2].getAttribute('excludes')).toBe('group1.a,');
      expect(input[3].getAttribute('excludes')).toBe('group1.a,');
    });
  });
});

async function getHtmlString(xmlPath: string) {
  const xml = TestHelper.requireFile(xmlPath);
  const container = getContainer(Contexts.DEFAULT);
  const crfToHTMLService: CRFToHTMLService = container.get(NamedBindings.CRFToHTMLService);
  return await crfToHTMLService.getHTMLfromXML(xml, path.parse(xmlPath).name, '0.0.1', `/public/xslt/${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`);
}

async function transformAndGetDocumentFragment(xmlPath: string): Promise<DocumentFragment> {
  const htmlString = await getHtmlString(xmlPath);
  return TestHelper.htmlStringToFragment(htmlString);
}

import { beforeAll, beforeEach, describe, expect, test, vi } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { v4 } from 'uuid';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import { CaseUpdated } from '@/application-events/CaseUpdated';
import { CaseFreezed } from '@/application-events/CaseFreezed';
import { CaseLocked } from '@/application-events/CaseLocked';
import { CaseUnfreezed } from '@/application-events/CaseUnfreezed';
import { CaseUnlocked } from '@/application-events/CaseUnlocked';
import { CaseMetaData, SaveMetaData } from '@/entities/CaseMetaData';
import { Case, CaseForUpdate } from '@/entities/Cases';
import type { HistoryPartialStoredCase } from '@/entities/HistoryPartialStoredCase';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { FreezeStrategy } from '@/services/FreezeStrategy';
import CurrentContextService from '@/services/CurrentContextService';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

let caseService: CaseServiceInterface;

describe('Freeze Strategy', () => {
    let freezeStrategy: FreezeStrategy;

    const completedSaveMetaData: SaveMetaData = {
        status: new Map().set('*', new Map().set('Admission', 3)),
        crfVersions: new Map(),
        lastUpdate: new Date()
    };
    const unCompletedSaveMetaData: SaveMetaData = {
        status: new Map().set('*', new Map().set('Admission', 2)),
        crfVersions: new Map(),
        lastUpdate: new Date()
    };

    const freezedMetaData = { frozenAccessLevels: new Map().set('public', true) } as CaseMetaData;
    const unfreezedMetaData = { frozenAccessLevels: new Map().set('public', false) } as CaseMetaData;

    beforeAll(() => {
        TestHelper.setupContainer();
        TestHelper.mockStatusService(Contexts.TEST);
        caseService = {
            freeze: vi.fn().mockResolvedValue(true),
            unfreeze: vi.fn().mockResolvedValue(true),
            async getDataFor(): Promise<Case> {
                return new Case('', {}, {} as CaseMetaData);
            },
            getCaseMetaDataFor: vi.fn().mockResolvedValue({} as CaseMetaData),
            async getAllCaseMetaDataFor(): Promise<Array<CaseMetaData>> {
                return [] as CaseMetaData[];
            },
            async getMultipleDataFor(): Promise<Case[]> {
                return [] as Case[];
            },
            async create(): Promise<void> { },
            async update(): Promise<void> { },
            async deleteCase(): Promise<void> { },
            async restoreCase(): Promise<void> { },
            async lock(): Promise<void> { },
            async unlock(): Promise<void> { },
            async permanentlyDeleteCase(): Promise<void> { },
            async getSingleCaseHistory(): Promise<HistoryPartialStoredCase[]> {
                return [] as HistoryPartialStoredCase[];
            }
        };
        getContainer(Contexts.TEST).rebind(NamedBindings.CaseService).toConstantValue(caseService);
        const currentContextService = getContainer(Contexts.TEST).get(NamedBindings.CurrentContextService) as CurrentContextService;
        freezeStrategy = new FreezeStrategy(caseService, currentContextService);
    });

    beforeEach(() => {
        freezeStrategy.activate();
        vi.clearAllMocks();
    });

    test('When a case is not completed and is unfreezed, the case have to remain unfreezed', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(unfreezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseUpdated, new CaseUpdated(new CaseForUpdate(caseID, {}, unCompletedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });

    test('When a case is completed and is unfreezed, the case have to be freezed', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(unfreezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseUpdated, new CaseUpdated(new CaseForUpdate(caseID, {}, completedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });

    test('When a case is not completed and freezed, the case have to be unfreezed', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(freezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseUpdated, new CaseUpdated(new CaseForUpdate(caseID, {}, unCompletedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).toBeCalled();
    });

    test('When a case is completed and freezed, the case have to remain freezed', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(freezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseUpdated, new CaseUpdated(new CaseForUpdate(caseID, {}, completedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });

    test('When a CaseFreezed Event is dispatched, the case does not have to change', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(freezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseFreezed, new CaseFreezed(new CaseForUpdate(caseID, {}, completedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });

    test('When a CaseUnfreezed Event is dispatched, the case does not have to change', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(unfreezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseUnfreezed, new CaseUnfreezed(new CaseForUpdate(caseID, {}, completedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });

    test('When a CaseLocked Event is dispatched, the case does not have to change', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(freezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseLocked, new CaseLocked(new CaseForUpdate(caseID, {}, completedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });

    test('When a CaseUnlocked Event is dispatched, the case does not have to change', async () => {
        const caseID = v4();
        vi.spyOn(caseService, 'getCaseMetaDataFor').mockResolvedValueOnce(unfreezedMetaData);
        getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseUnlocked, new CaseUnlocked(new CaseForUpdate(caseID, {}, completedSaveMetaData), ContainerTypes.DEFAULT));
        await new Promise((resolve) => setTimeout(resolve, 0));
        expect(caseService.freeze).not.toBeCalled();
        expect(caseService.unfreeze).not.toBeCalled();
    });
});
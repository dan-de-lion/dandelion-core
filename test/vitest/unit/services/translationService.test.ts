import { describe, expect, test } from 'vitest';
import TranslationService, { Language } from '@/services/TranslationService';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';

describe('Translations', () => {
  const ts = new TranslationService(new CoreConfiguration({
    baseUrlForXslt: '',
    listOfCaseTypes: [new CaseConfiguration({ caseName: '', referenceDateVariableName: '' })],
  }));

  describe('Translation work correctly', () => {
    test('Translation without translator mode activated and without transduction', () => {
      expect(ts.t('Test')).toBe('Test');
    });
    
    test('Translation with translator mode activated but without transduction', () => {
      ts.setTranslatorMode(true);
      expect(ts.t('Test')).toBe('<strong style="color: red;">Test</strong>');
    });

    test('Translation with translator mode activated and with transduction', () => {
      ts.setTranslatorMode(true);
      ts.addTranslations({ en: { translation: { Test: 'Test2' } } });
      expect(ts.t('Test')).toBe('Test2');
    });
  });

  describe('TranslationService change language', () => {
    test('Translation Service work correctly to change language', () => {
      ts.setTranslatorMode(false); //TODO: side effect of previous tests, should instead reinitialize translation every time
      expect(ts.t('New hospitalization')).toBe('New hospitalization');
      ts.setLanguage(Language.it);
      expect(ts.t('New hospitalization')).toBe('Nuovo ricovero');
    });
  });
});
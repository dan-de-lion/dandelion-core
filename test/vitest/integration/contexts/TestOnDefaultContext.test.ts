import { Container } from 'inversify';
import { expect, beforeAll, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { getContext, Context, getContainer } from '@/ContainersConfig';
import { Contexts } from '@/types/Contexts';

describe('Context default', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
  });

  test('check that exists context equal to first CaseType', async () => {
    const context = getContext('Admission');
    expect(context).toBeDefined();
    expect(context instanceof Context).toBe(true);
  });

  test('check default context exists and equals to first caseType', async () => {
    const context = getContext(Contexts.DEFAULT);
    expect(context).toBeDefined();
    expect(context instanceof Context).toBe(true);
    expect(context).toBe(getContext('Admission'));
  });

  test("check getContext throw error when context doesn't exists", async () => {
    expect(() => getContext('pippo')).toThrowError('No context with this name: pippo');
  });

  test('check that exists container equal to first CaseType', async () => {
    const container = getContainer('Admission');
    expect(container).toBeDefined();
    expect(container instanceof Container).toBe(true);
  });

  test('check default container exists and equals to first caseType', async () => {
    const container = getContainer(Contexts.DEFAULT);
    expect(container).toBeDefined();
    expect(container instanceof Container).toBe(true);
    expect(container).toBe(getContainer('Admission'));
  });
  
  test("check getContainer throw error when container doesn't exists", async () => {
    expect(() => getContainer('pippo')).toThrowError('No context with this name: pippo');
  });
});

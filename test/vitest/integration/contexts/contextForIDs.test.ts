import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Context default', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Text Variable should have ID without context when context is "Contexts.DEFAULT"', async () => {
      const xml = TestHelper.requireFile('/textVariable/standard-text-variable.xml');
      const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, 'standard-text-variable', '0.0.1',
        false, false, false, 0, {}, '', Contexts.DEFAULT);
      const inputId = component.find('input')?.attributes('id');
      expect(inputId).toBe('textVariable');
    });

    test('Text Variable should have ID without context when context is the default one', async () => {
      const xml = TestHelper.requireFile('/textVariable/standard-text-variable.xml');
      const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, 'standard-text-variable', '0.0.1',
        false, false, false, 0, {}, '', 'Admission');
      const inputId = component.find('input')?.attributes('id');
      expect(inputId).toBe('textVariable');
    });

    test('Text Variable should have ID with context when context is not the default one', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/standard-text-variable.xml');
      const inputId = component.find('input')?.attributes('id');
      expect(inputId).toBe('test_textVariable');
    });
  });
});
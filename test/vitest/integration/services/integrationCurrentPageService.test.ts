import { beforeAll, beforeEach, expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import type CurrentPageService from '@/services/CurrentPageService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';

describe('Integration tests for CurrentPageService', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });

  beforeEach(() => {
    const currentPageService = getContainer(Contexts.TEST).get(NamedBindings.CurrentPageService) as CurrentPageService;
    currentPageService.set('');
  });

  test('standard set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/standard-separate-in-page.xml');
    const textInput = component.findAll('input')[0];
    const textInput2 = component.findAll('input')[1];
    const currentPageService = getContainer(Contexts.TEST).get(NamedBindings.CurrentPageService) as CurrentPageService;
    expect(textInput.isVisible()).toBe(false);
    expect(textInput2.isVisible()).toBe(false);
    currentPageService.set('test_mainGroup.groupVariable.textVariable');
    // TODO: try to restructure currentPageService to have a
    // reactive var to be put in the model (returned from get)
    await component.vm.$forceUpdate();
    expect(textInput.isVisible()).toBe(true);
    expect(textInput2.isVisible()).toBe(false);
  });

  test('standard scrollToVariable', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/standard-separate-in-page.xml');
    const textInput = component.findAll('input')[0];
    const textInput2 = component.findAll('input')[1];
    const currentPageService = getContainer(Contexts.TEST).get(NamedBindings.CurrentPageService) as CurrentPageService;
    expect(textInput.isVisible()).toBe(false);
    expect(textInput2.isVisible()).toBe(false);
    await currentPageService.scrollToVariable('test_mainGroup.groupVariable.text1');
    await component.vm.$forceUpdate();
    expect(textInput.isVisible()).toBe(true);
    expect(textInput2.isVisible()).toBe(false);
  });

  test('set should set all the variables', async () => {
    const currentPageService = getContainer(Contexts.TEST).get(NamedBindings.CurrentPageService) as CurrentPageService;
    currentPageService.set('test_mainGroup.groupVariable.text1');
    expect(currentPageService.get('test_mainGroup')).toBeTruthy();
    expect(currentPageService.get('test_mainGroup.groupVariable')).toBeTruthy();
    expect(currentPageService.get('test_mainGroup.groupVariable.text1')).toBeTruthy();
    expect(currentPageService.get('dovrei fallire')).toBeFalsy();
  });
});

import { v4 } from 'uuid';
import { beforeAll, describe, expect, test, beforeEach } from 'vitest';
import InMemoryCRFServiceForTest from '../../../mock-services/InMemoryCRFServiceForTest';
import { UserService } from '../../../mock-services/UserService';
import TestHelper from '../../../utils/TestHelper';
import { addContext, getContainer } from '@/ContainersConfig';
import { Case } from '@/entities/Cases';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CaseService } from '@/services/CaseService';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';

describe('Case Service Access Rights', () => {
  let caseService: CaseService;
  let userService: UserService;
  const caseID = v4();
  const standardModel = { Admission: { name: 'marco', surname: 'rossi' } };
  const setModel = { Admission: { names: [{ name: 'Marco' }, { name: 'Mario' }] } };
  const caseMetaData: CaseMetaData = {
    caseID,
    centreCode: '',
    status: new Map(),
    crfVersions: new Map().set('Admission', '0.0.1'),
    creationDate: new Date(),
    frozenAccessLevels: new Map(),
    lastUpdate: new Date(),
    caseType: 'Admission',
  };

  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.Logger) as ConsoleLogger;
    const crfService = new InMemoryCRFServiceForTest(logger, coreConfiguration, [new CRF('Admission', '0.0.1.', '')],
      new Map().set('Admission', [new CRFVersionValidity(0, '0.0.1', new Date(2020, 1, 3))]),
      {
        Admission: {
          name: { accessLevel: 'OnlyAdmin' },
          names: {
            name: { accessLevel: 'OnlyAdmin' },
          }
        }
      });
    userService = new UserService(crfService);
    getContainer(Contexts.TEST).bind(NamedBindings.CRFService).toDynamicValue(() => crfService).inSingletonScope();
    getContainer(Contexts.TEST).bind(NamedBindings.UserService).toDynamicValue(() => userService).inSingletonScope();
    caseService = container.get(NamedBindings.CaseService) as CaseService;
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST);
    const caseStorage = container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage;
    caseStorage.reset();
  })

  test('An Admin user should get the right variables of a case created by an Admin user', async () => {
    userService.setCurrentUser('Admin');
    await caseService.create(new Case(caseID, standardModel, caseMetaData));
    const defaultCase = await caseService.getDataFor(caseID);
    expect(defaultCase.data.Admission.name).toBeDefined();
  });

  test("A SimpleUser shouldn't get the right variables of a case created by an Admin user", async () => {
    userService.setCurrentUser('Admin');
    await caseService.create(new Case(caseID, standardModel, caseMetaData));
    userService.setCurrentUser('SimpleUser');
    const defaultCase = await caseService.getDataFor(caseID);
    expect(defaultCase.data.Admission.name).toBeUndefined();
  });

  test('A SimpleUser should get the right variables of a case created by a SimpleUser', async () => {
    userService.setCurrentUser('SimpleUser');
    await caseService.create(new Case(caseID, standardModel, caseMetaData));
    const defaultCase = await caseService.getDataFor(caseID);
    expect(defaultCase.data.Admission.name).toBeUndefined();
  });

  test('An Admin should get the right variables of a case created by a SimpleUser', async () => {
    userService.setCurrentUser('SimpleUser');
    await caseService.create(new Case(caseID, standardModel, caseMetaData));
    userService.setCurrentUser('Admin');
    const defaultCase = await caseService.getDataFor(caseID);
    expect(defaultCase.data.Admission.name).toBeUndefined();
  });

  test('In a set an Admin user should get the right variables of a case created by an Admin user', async () => {
    userService.setCurrentUser('Admin');
    await caseService.create(new Case(caseID, setModel, caseMetaData));
    const defaultCase = await caseService.getDataFor(caseID);
    expect(defaultCase.data.Admission.names[0].name).toBeDefined();
    expect(defaultCase.data.Admission.names[1].name).toBeDefined();
  });

  test('In a set a SimpleUser should get the right variables of a case created by an Admin user', async () => {
    if (new Date() > new Date('2025-12-20')) {
      userService.setCurrentUser('Admin');
      await caseService.create(new Case(caseID, setModel, caseMetaData));
      userService.setCurrentUser('SimpleUser');
      const defaultCase = await caseService.getDataFor(caseID);
      expect(defaultCase.data.Admission.names[0].name).toBeUndefined();
      expect(defaultCase.data.Admission.names[1].name).toBeUndefined();
    }
  });

  test('In a set a SimpleUser should get the right variables of a case created by a SimpleUser', async () => {
    if (new Date() > new Date('2025-12-20')) {
      userService.setCurrentUser('SimpleUser');
      await caseService.create(new Case(caseID, setModel, caseMetaData));
      const defaultCase = await caseService.getDataFor(caseID);
      expect(defaultCase.data.Admission.names[0].name).toBeUndefined();
      expect(defaultCase.data.Admission.names[1].name).toBeUndefined();
    }
  });

  test('In a set an Admin should get the right variables of a case created by a SimpleUser', async () => {
    if (new Date() > new Date('2025-12-20')) {
      userService.setCurrentUser('SimpleUser');
      await caseService.create(new Case(caseID, setModel, caseMetaData));
      userService.setCurrentUser('Admin');
      const defaultCase = await caseService.getDataFor(caseID);
      expect(defaultCase.data.Admission.names[0].name).toBeUndefined();
      expect(defaultCase.data.Admission.names[1].name).toBeUndefined();
    }
  });
});
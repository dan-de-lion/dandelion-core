import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import { ContainerTypes, getContainer, getContext } from "@/ContainersConfig";
import { CacheStorageForTests } from '../../../mock-repositories/CacheStorageForTests';
import { InMemoryCacheStorageForTests } from '../../../mock-repositories/InMemoryCacheStorageForTests';
import InMemoryCRFServiceForTest from '../../../mock-services/InMemoryCRFServiceForTest';
import TestHelper, { sleep } from '../../../utils/TestHelper';
import { CaseCreated } from '@/application-events/CaseCreated';
import { CasePermanentlyDeleted } from '@/application-events/CasePermanentlyDeleted';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { CaseListService } from '@/services/CaseListService';
import type { CaseService } from '@/services/CaseService';
import CaseStatusService from '@/services/CaseStatusService';
import { CRFVersionSelectionStrategy } from '@/services/CRFVersionSelectionStrategy';
import { ConsoleLogger } from '@/services/Logger';
import { InMemoryEventsStorage } from '@/storages/InMemoryEventsStorage';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { Motivations } from '@/utils/MapForMotivations';

describe('Case List Service', () => {
  let caseListService: CaseListServiceInterface;
  let caseService: CaseServiceInterface;

  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.Logger) as ConsoleLogger;
    container.bind(NamedBindings.CRFService).toDynamicValue(() => new InMemoryCRFServiceForTest(logger, coreConfiguration,
      [new CRF('Admission', '0.0.1',
        `<root>
          <dataClass>
            <variable name="firstname" dataType="text" />
            <variable name="lastname" dataType="text" />
            <variable name="test" dataType="text" computedFormula="model.firstname" />
          </dataClass>
        </root>`
      )],
      new Map<string, CRFVersionValidity[]>([['Admission', [new CRFVersionValidity(0, '1.0.0', new Date('1990-12-12'))]]]))).inSingletonScope();

    container.rebind(NamedBindings.CRFVersionSelectionStrategy).to(CRFVersionSelectionStrategy).inSingletonScope();
    container.bind(NamedBindings.CaseStatusService).to(CaseStatusService).inSingletonScope();
    container.bind(NamedBindings.StorageForEvents).to(InMemoryEventsStorage).inSingletonScope();
    container.bind(NamedBindings.VariablesCacheStorage).to(CacheStorageForTests).inSingletonScope();
    container.bind(NamedBindings.CaseListService).to(CaseListService).inSingletonScope();

    /*(container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage).createCase([new PartialStoredCase('{}', 'public')],
      {
        caseID: '12345',
        centreCode: '',
        creationDate: new Date(),
        caseType: 'Admission',
        status: new Map(),
        crfVersions: new Map(),
        frozenAccessLevels: new Map(),
        lastUpdate: new Date(),
      });*/
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST);
    container.rebind(NamedBindings.StorageForEvents).to(InMemoryEventsStorage).inSingletonScope();
    container.rebind(NamedBindings.VariablesCacheStorage).to(InMemoryCacheStorageForTests).inSingletonScope();
    container.rebind(NamedBindings.CaseListService).to(CaseListService).inSingletonScope();
    caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
    caseService = container.get(NamedBindings.CaseService) as CaseService;
  });

  test('should have empty lists at start', async () => {
    expect((caseListService.slicedCases).length).toBe(0);
    expect(caseListService.slicedDeletedCases.length).toBe(0);
    expect(caseListService.getPermanentlyDeletedList().length).toBe(0);
  });

  test('should permanently delete correctly a patient from patientList', async () => {
    getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CaseCreated, new CaseCreated({}, 'test', new Date(), ContainerTypes.DEFAULT));
    getContext(Contexts.TEST).dispatchEvent(ApplicationEvents.CasePermanentlyDeleted,
      new CasePermanentlyDeleted(Contexts.TEST, Motivations.OTHER_MOTIVATIONS, ContainerTypes.DEFAULT));
    await sleep(0.1);
    expect((caseListService.slicedCases).length).toBe(0);
    expect(caseListService.slicedDeletedCases.length).toBe(0);
  });
});

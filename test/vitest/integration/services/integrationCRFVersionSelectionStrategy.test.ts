import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import { addContext, ContainerTypes, getContainer } from '@/ContainersConfig';
import { setupDandelion } from '@/index';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { CRFVersionSelectionStrategy } from '@/services/CRFVersionSelectionStrategy';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { CrfServiceFromFolderForTests } from '../../../mock-services/CrfServiceFromFolderForTests';
import { UserService } from '../../../mock-services/UserService';
import { ActivationPeriodsForCrfStorageForTests } from '../../../mock-repositories/ActivationPeriodsForCrfStorageForTests';

describe('GetUpdatedCRFVersionsByCase', () => {
  beforeAll(() => {
    setupDandelion({
      coreConfiguration: new CoreConfiguration({
        baseUrlForXslt: 'public/xslt/',
        encrypt: false,
        debug: false,
        listOfCaseTypes: [new CaseConfiguration({
          caseName: 'Admission',
          childrenCRFs: [{ crfName: 'core' }, { crfName: 'covid' }],
          caseLinkingConfiguration: [{ caseTypeName: 'Patient', caseIDVariable: 'admission.patientId' }],
          mainCrf: { crfName: 'Admission' },
          referenceDateVariableName: 'Admission.DateVariable',
        })],
      }),
      sharedServices: [
        { name: NamedBindings.Logger, class: ConsoleLogger },
        { name: NamedBindings.CRFService, class: CrfServiceFromFolderForTests },
        { name: NamedBindings.UserService, class: UserService },
      ],
      services: [{ name: NamedBindings.CRFVersionSelectionStrategy, class: CRFVersionSelectionStrategy, containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION] },
      { name: NamedBindings.ActivationPeriodForCentreCodeStorage, class: ActivationPeriodsForCrfStorageForTests, containerTypes: [ContainerTypes.DEFAULT, ContainerTypes.REEVALUATION] }]
    });
    addContext(Contexts.TEST);
  });

  let crfVersionSelectionStrategy: CRFVersionSelectionStrategy;
  let activationPeriodForCentreCodeStorage: ActivationPeriodsForCrfStorageForTests;
  const caseMetaData = { caseID: '1', status: new Map(), creationDate: new Date(), caseType: 'Admission' } as CaseMetaData;

  beforeEach(() => {
    crfVersionSelectionStrategy = getContainer(Contexts.TEST).get(NamedBindings.CRFVersionSelectionStrategy) as CRFVersionSelectionStrategy;
    activationPeriodForCentreCodeStorage = getContainer(Contexts.TEST).get(NamedBindings.ActivationPeriodForCentreCodeStorage) as ActivationPeriodsForCrfStorageForTests;
  });

  describe('No CRF of 3 present in ActivationPeriod json file', () => {
    test('getUpdatedVersions return empty map of versions for a specific old date', async () => {
      expect(await getCrfVersion('NoCrfPeriods.json', 'v-10-10')).toStrictEqual(new Map());
    });

    test('getUpdatedVersions return empty map of versions even for a correct date', async () => {
      expect(await getCrfVersion('NoCrfPeriods.json', '2023-10-10')).toStrictEqual(new Map());
    });
  });

  describe('Only 1 CRF of 3 present in ActivationPeriod json file', () => {
    describe('One Period', () => {
      test('getUpdatedVersions return correct map of versions for a valid date', async () => {
        expect(await getCrfVersion('AdmissionWithOnePeriod.json', '2020-10-10')).toStrictEqual(new Map().set('Admission', '0.0.1'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date', async () => {
        expect(await getCrfVersion('AdmissionWithOnePeriod.json', '2021-10-10')).toStrictEqual(new Map());
      });
    });

    describe('TwoPeriods', () => {
      test('getUpdatedVersions return correct map of versions for a valid date', async () => {
        expect(await getCrfVersion('AdmissionWithTwoPeriods.json', '2022-10-10')).toStrictEqual(new Map().set('Admission', '0.1.0'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date', async () => {
        expect(await getCrfVersion('AdmissionWithTwoPeriods.json', '2023-10-10')).toStrictEqual(new Map());
      });
    });
  });

  describe('Only 2 CRF of 3 present in ActivationPeriod json file', () => {
    describe('One Period', () => {
      test('getUpdatedVersions return correct map of versions for a valid date of Admission and not for core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithOnePeriod.json', '2020-02-02')).toStrictEqual(new Map().set('Admission', '0.0.1'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission and valid for core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithOnePeriod.json', '2020-10-10')).toStrictEqual(new Map().set('core', '0.0.2'));
      });

      test('getUpdatedVersions return correct map of versions for a valid date of Admission and core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithOnePeriod.json', '2020-06-03')).toStrictEqual(new Map().set('Admission', '0.0.1').set('core', '0.0.2'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission and core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithOnePeriod.json', '2023-10-10')).toStrictEqual(new Map());
      });
    });

    describe('Two Periods', () => {
      test('getUpdatedVersions return correct map of versions for a valid date of Admission and not for core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithTwoPeriods.json', '2021-02-02')).toStrictEqual(new Map().set('Admission', '0.1.0'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission and valid for core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithTwoPeriods.json', '2021-10-10')).toStrictEqual(new Map().set('core', '0.2.0'));
      });

      test('getUpdatedVersions return correct map of versions for a valid date of Admission and core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithTwoPeriods.json', '2021-06-03')).toStrictEqual(new Map().set('Admission', '0.1.0').set('core', '0.2.0'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission and core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreWithTwoPeriods.json', '2024-10-10')).toStrictEqual(new Map());
      });
    });
  });

  describe('3 CRF of 3 present in ActivationPeriod json file', () => {
    describe('One Period', () => {
      test('getUpdatedVersions return correct map of versions for a valid date of Admission and not for core and covid', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithOnePeriod.json', '2020-02-02')).toStrictEqual(new Map().set('Admission', '0.0.1'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission and covid and valid for core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithOnePeriod.json', '2020-06-06')).toStrictEqual(new Map().set('core', '0.0.2'));
      });

      test('getUpdatedVersions return correct map of versions for a valid date of covid and not for Admission and core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithOnePeriod.json', '2020-10-10')).toStrictEqual(new Map().set('covid', '0.0.3'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission,core and covid', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithOnePeriod.json', '2023-04-03')).toStrictEqual(new Map());
      });

      test('getUpdatedVersions return correct map of versions for a valid date of Admission and core and not for covid', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithOnePeriod.json', '2020-04-03')).toStrictEqual(new Map().set('Admission', '0.0.1').set('core', '0.0.2'));
      });

      test('getUpdatedVersions return correct map of versions for a valid date of core and covid and not for Admission', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithOnePeriod.json', '2020-08-07')).toStrictEqual(new Map().set('core', '0.0.2').set('covid', '0.0.3'));
      });
    });

    describe('Two Periods', () => {
      test('getUpdatedVersions return correct map of versions for a valid date of Admission, core and covid', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithTwoPeriods.json', '2021-03-03')).toStrictEqual(
          new Map().set('Admission', '0.1.0').set('core', '0.2.0').set('covid', '0.3.0'));
      });

      test('getUpdatedVersions return correct map of versions for a non valid date of Admission, covid and core', async () => {
        expect(await getCrfVersion('AdmissionAndCoreAndCovidWithTwoPeriods.json', '2023-06-06')).toStrictEqual(new Map());
      });
    });
  });

  async function getCrfVersion(jsonPath: string, dateString: string) {
    activationPeriodForCentreCodeStorage.setPath(jsonPath);
    return await crfVersionSelectionStrategy.getUpdatedCRFVersionsByCase({ Admission: { DateVariable: dateString } }, caseMetaData);
  }
});

import "../../../../public/frameless-xslt2";
import { v4 } from "uuid";
import { beforeAll, beforeEach, describe, expect, test, vitest } from "vitest";
import InMemoryCRFServiceForTest from "../../../mock-services/InMemoryCRFServiceForTest";
import TestHelper, { sleep } from "../../../utils/TestHelper";
import { CaseCreated } from "@/application-events/CaseCreated";
import { CaseDeleted } from "@/application-events/CaseDeleted";
import { CasePermanentlyDeleted } from "@/application-events/CasePermanentlyDeleted";
import { CaseRestored } from "@/application-events/CaseRestored";
import { addContext, ContainerTypes, getContainer, getContext } from "@/ContainersConfig";
import { Case, CaseForUpdate } from "@/entities/Cases";
import { CaseMetaData } from "@/entities/CaseMetaData";
import { CRF, CRFVersionValidity } from "@/entities/CRF";
import type { PartialStoredCase } from "@/entities/PartialStoredCase";
import { CaseError, CaseErrorType } from "@/errors/CaseError";
import { StorageError, StorageErrorType } from "@/errors/StorageError";
import type { CaseServiceInterface } from "@/interfaces/CaseServiceInterface";
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import { CurrentCaseServiceInterface } from "@/interfaces/CurrentCaseServiceInterface";
import { ConsoleLogger } from "@/services/Logger";
import type { InMemoryCaseStorage } from "@/storages/InMemoryCaseStorage";
import type { InMemoryEventsStorage } from "@/storages/InMemoryEventsStorage";
import { ApplicationEvents } from "@/types/ApplicationEvents";
import { Contexts } from "@/types/Contexts";
import { NamedBindings } from "@/types/NamedBindings";
import { Motivations } from "@/utils/MapForMotivations";

describe('Case Service', () => {
  let caseService: CaseServiceInterface;
  let currentCaseServiceForNewCase: CurrentCaseServiceInterface;
  let caseStorage: InMemoryCaseStorage;
  let eventStorage: InMemoryEventsStorage;
  let caseID = v4();
  const defaultCaseMetaData: CaseMetaData = {
    caseID,
    centreCode: '',
    status: new Map(),
    crfVersions: new Map(),
    frozenAccessLevels: new Map(),
    creationDate: new Date(),
    lastUpdate: new Date(),
    caseType: '',
  };
  const model = { model: { referenceDate: new Date() } };

  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const reevaluationContainer = getContainer(Contexts.TEST, ContainerTypes.REEVALUATION);
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.Logger) as ConsoleLogger;
    const inMemoryCRFServiceForTest = new InMemoryCRFServiceForTest(logger, coreConfiguration,
      [new CRF('Admission', '1.0.0',
        `<root>
          <dataClass>
            <variable name="firstname" dataType="text" />
            <variable name="lastname" dataType="text" />
            <variable name="test" dataType="text" computedFormula="model.firstname" />
          </dataClass>
        </root>`,
      )],
      new Map<string, CRFVersionValidity[]>([['Admission', [new CRFVersionValidity(1, '1.0.0', new Date('1990-12-12'))]]]));

    reevaluationContainer.bind(NamedBindings.CRFService).toDynamicValue(() => inMemoryCRFServiceForTest).inSingletonScope();
    reevaluationContainer.rebind(NamedBindings.CRFVersionSelectionStrategy).toDynamicValue(() => ({
      getVersionFor: async () => '1.0.0',
      getUpdatedCRFVersionsByCase: async () => new Map().set('Admission', '1.0.0'),
      getCRFConfigurationByCase: async () => [{ crfName: 'Admission' }],
      getCRFConfigurationsByCentreCode: async () => [{ crfName: 'Admission' }]
    }));
    const caseService = container.get(NamedBindings.CaseService) as CaseServiceInterface;
    reevaluationContainer.rebind(NamedBindings.CaseService).toDynamicValue(() => caseService);
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST);
    const newCaseContainer = getContainer(Contexts.TEST, ContainerTypes.NEW_CASE);
    caseService = container.get(NamedBindings.CaseService) as CaseServiceInterface;
    currentCaseServiceForNewCase = newCaseContainer.get(NamedBindings.CurrentCaseServiceForNewCase) as CurrentCaseServiceInterface;
    caseStorage = container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage;
    caseStorage.reset();
    eventStorage = container.get(NamedBindings.StorageForEvents) as InMemoryEventsStorage;
    eventStorage.reset();
  });

  test('should dispatch CaseCreated event on newCase', async () => {
    let eventInContextTest = new CaseCreated({}, '', new Date(), ContainerTypes.NEW_CASE);
    const eventInContextDefault = new CaseCreated({}, '', new Date(), ContainerTypes.DEFAULT);
    getContext(Contexts.TEST).addEventListener(ApplicationEvents.CaseCreated, (e: CaseCreated) => eventInContextTest = e);
    await currentCaseServiceForNewCase.open(caseID, model);
    await currentCaseServiceForNewCase.save();
    await sleep(0.1);
    expect(eventInContextTest).toBe(`${ApplicationEvents.CaseCreated}.test`);
    expect(eventInContextDefault.caseID).toBe('');
  });

  test('should dispatch CaseDeleted event on caseDeleted', async () => {
    let eventInContextTest = new CaseDeleted('', ContainerTypes.NEW_CASE);
    let eventInContextDefault = new CaseDeleted('', ContainerTypes.DEFAULT);
    getContext(Contexts.TEST).addEventListener(ApplicationEvents.CaseDeleted, (e: CaseDeleted) => eventInContextTest = e);
    getContext(Contexts.DEFAULT).addEventListener(ApplicationEvents.CaseDeleted, (e: CaseDeleted) => eventInContextDefault = e);
    await currentCaseServiceForNewCase.open(caseID, model);
    await currentCaseServiceForNewCase.save();
    await caseService.deleteCase(caseID);
    await sleep(0.1);
    expect(undefined).toBeUndefined();
    expect(eventInContextTest).toBe(`${ApplicationEvents.CaseDeleted}.test`);
    expect(eventInContextDefault.caseID).toBe('');
  });

  test('should dispatch CaseRestored event on caseRestore', async () => {
    let eventInContextTest = new CaseRestored('', ContainerTypes.NEW_CASE);
    let eventInContextDefault = new CaseRestored('', ContainerTypes.DEFAULT);
    getContext(Contexts.TEST).addEventListener(ApplicationEvents.CaseRestored, (e: CaseRestored) => eventInContextTest = e);
    getContext(Contexts.DEFAULT).addEventListener(ApplicationEvents.CaseRestored, (e: CaseRestored) => eventInContextDefault = e);
    await currentCaseServiceForNewCase.open(caseID, model);
    await currentCaseServiceForNewCase.save();
    await caseService.deleteCase(caseID);
    await sleep(0.1);
    await caseService.restoreCase(caseID);
    await sleep(0.1);
    expect(eventInContextTest).toBe(`${ApplicationEvents.CaseRestored}.test`);
    expect(eventInContextDefault.caseID).toBe('');
  });

  test('should dispatch CasePermanentlyDelete event on casePermanentlyDeleted', async () => {
    let eventInContextTest = new CasePermanentlyDeleted('', Motivations.INSERTION_ERROR, ContainerTypes.NEW_CASE);
    let eventInContextDefault = new CasePermanentlyDeleted('', Motivations.INSERTION_ERROR, ContainerTypes.DEFAULT);
    getContext(Contexts.TEST).addEventListener(ApplicationEvents.CasePermanentlyDeleted, (e: CasePermanentlyDeleted) => eventInContextTest = e);
    getContext(Contexts.DEFAULT).addEventListener(ApplicationEvents.CasePermanentlyDeleted, (e: CasePermanentlyDeleted) => eventInContextDefault = e);
    await currentCaseServiceForNewCase.open(caseID, model);
    await currentCaseServiceForNewCase.save();
    await caseService.permanentlyDeleteCase(caseID, Motivations.INSERTION_ERROR);
    await sleep(0.1);
    expect(eventInContextTest).toBe(`${ApplicationEvents.CasePermanentlyDeleted}.test`);
    expect(eventInContextDefault.caseID).toBe('');
  });

  test('should give back the correct caseMetaData when getCaseMetaDataFor() method is called', async () => {
    await caseService.create(new Case(caseID, {}, defaultCaseMetaData));
    const caseMetaData = await caseService.getCaseMetaDataFor(caseID);
    expect(caseMetaData?.caseID).toEqual(caseID);
  });

  test('should correctly save new datas when save() method is called', async () => {
    await currentCaseServiceForNewCase.open(caseID, model);
    await currentCaseServiceForNewCase.save();
    const caseMetaData = await caseService.getCaseMetaDataFor(caseID);
    await caseService.update(new CaseForUpdate(caseID, { name: 'ciao' }, caseMetaData));
    const defaultCase = await caseService.getDataFor(caseID);
    const partials = await caseStorage.getPartials(caseID);
    let dataToBeAnalyzed2: PartialStoredCase;
    partials.forEach((partial) => {
      if (partial.accessLevel === 'public') {
        dataToBeAnalyzed2 = partial;
      }
    });
    expect(partials.some((da) => da.accessLevel == 'public')).toBe(true);
    expect(dataToBeAnalyzed2!).toBeDefined();
    expect(defaultCase.data.name).toBe('ciao');
  });

  test('should return error if there are not any cases with the specified "caseID"', async () => {
    await expect(() => caseService.getCaseMetaDataFor('1')).rejects.toEqual(
      new CaseError('Found an error on getting caseMetaData for id: 1', CaseErrorType.CASE_METADATA_NOT_FOUND,
        new StorageError('No case with this ID: 1', StorageErrorType.CASE_ID_NOT_FOUND)));
  });

  test('should correctly create a new case when newCase() method is called', async () => {
    await caseService.create(new Case(caseID, {}, defaultCaseMetaData));
    const data = await caseStorage.getPartials(caseID);
    expect(data).toBeDefined();
  });

  test('should give back the correct data when getDataFor() method is called', async () => {
    await caseService.create(new Case(caseID, { Admission: { name: 'marco', surname: 'rossi' } }, defaultCaseMetaData));
    const defaultCase = await caseService.getDataFor(caseID);
    expect(defaultCase.data.Admission.name).toEqual('marco');
    expect(defaultCase.data.Admission.surname).toEqual('rossi');
  });

  test('should return error when getDataFor() tries to give back a case which does not exist', async () => {
    await expect(() => caseService.getDataFor('i')).rejects.toEqual(
      new CaseError('Found an error on getting data for id: i', CaseErrorType.PARTIALS_NOT_FOUND,
        new StorageError('No case with this ID: i', StorageErrorType.CASE_ID_NOT_FOUND)));
  });

  test('should return error when restoreCase() tries to delete a case which does not exist', async () => {
    await expect(() => caseService.restoreCase('i')).rejects.toEqual(
      new CaseError('Found an error on restoring case i', CaseErrorType.RESTORE_CASE_FAILED,
        new StorageError('Failed to restore, no case deleted i in inMemory storage', StorageErrorType.RESTORE_CASE_FAILED)));
  });

  test('should return error when deleteCase() tries to delete a case which does not exist', async () => {
    await expect(() => caseService.deleteCase('i')).rejects.toEqual(
      new CaseError('Found an error on deleting case i', CaseErrorType.DELETE_CASE_FAILED,
        new StorageError('Failed to delete, no case i in inMemory storage', StorageErrorType.DELETE_CASE_FAILED)));
  });

  test('should return error when permanentlyDeleteCase() tries to delete a case which does not exist', async () => {
    await expect(() => caseService.permanentlyDeleteCase('i', Motivations.OTHER_MOTIVATIONS)).rejects.toEqual(
      new CaseError('Found an error on permanently deleting case i', CaseErrorType.PERMANENTLY_DELETE_CASE_FAILED,
        new StorageError('Failed to permanently delete, no case i in inMemory storage', StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED)));
  });

  test('should save "public" partials even when not present in the data passed', async () => {
    await caseService.create(new Case(caseID, {}, defaultCaseMetaData));
    const partialStoredCases = await caseStorage.getPartials(caseID);
    let dataToBeAnalyzed: PartialStoredCase;
    partialStoredCases.forEach((da) => {
      if (da.accessLevel == 'public') {
        dataToBeAnalyzed = da;
      }
    });
    expect(partialStoredCases.some((da) => da.accessLevel === 'public')).toBe(true);
    expect(dataToBeAnalyzed!).toBeDefined();
  });

  test('should not launch a CaseDeleted event on deleteCase if the caseID does not exist', async () => {
    const mockDocument = {
      dispatchEvent: vitest.fn(),
      addEventListener: (_a: any, _b: any) => { },
    };
    //@ts-expect-error
    global.document = mockDocument;
    // Richiamo il metodo che si suppone lanci l'evento
    await expect(() => caseService.deleteCase('')).rejects.toEqual(
      new CaseError('Found an error on deleting case empty ID', CaseErrorType.DELETE_CASE_FAILED,
        new StorageError('Failed to delete, no case empty ID in inMemory storage', StorageErrorType.DELETE_CASE_FAILED)));
    //  TODO: control
  });

  test('should not launch a CaseRestored event on caseRestored if the caseID does not exist', async () => {
    const mockDocument = {
      dispatchEvent: vitest.fn(),
      addEventListener: (_a: any, _b: any) => { },
    };
    //@ts-expect-error
    global.document = mockDocument;
    // Richiamo il metodo che si suppone lanci l'evento
    //  TODO: control
    await expect(() => caseService.restoreCase('')).rejects.toEqual(
      new CaseError('Found an error on restoring case empty ID', CaseErrorType.RESTORE_CASE_FAILED,
        new StorageError('Failed to restore, no case deleted empty ID in inMemory storage', StorageErrorType.RESTORE_CASE_FAILED)));
  });

  test('should not launch a CasePermanentlyDeleted event on casePermanentlyDeleted if the caseID does not exist', async () => {
    const mockDocument = {
      dispatchEvent: vitest.fn(),
      addEventListener: (_a: any, _b: any) => { },
    };
    //@ts-expect-error
    global.document = mockDocument;
    // Richiamo il metodo che si suppone lanci l'evento
    await expect(() => caseService.permanentlyDeleteCase('', Motivations.OTHER_MOTIVATIONS)).rejects.toEqual(
      new CaseError('Found an error on permanently deleting case empty ID', CaseErrorType.PERMANENTLY_DELETE_CASE_FAILED,
        new StorageError('Failed to permanently delete, no case empty ID in inMemory storage', StorageErrorType.PERMANENTLY_DELETE_CASE_FAILED)));
    //  TODO: control
  });

  test('should reset New Case model correctly', async () => {
    await currentCaseServiceForNewCase.open(caseID, {});
    const model = await currentCaseServiceForNewCase.getModel();
    expect(model).toStrictEqual({});
    model.Admission = {};
    model.Admission.textVariable = 'Prova';
    expect(model).toStrictEqual({ Admission: { textVariable: 'Prova' } });
    currentCaseServiceForNewCase.resetModel();
    expect(model).toStrictEqual({});
  });
});
// import { beforeAll, beforeEach, expect, describe } from 'vitest';
// import { addContext, getContext } from '@/ContainersConfig';
// import TestHelper, { sleep } from '../../../../utils/TestHelper';
//import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
// import { InMemoryEventsStorage } from '@/storages/InMemoryEventsStorage';
// import type { CaseMetaData } from '@/entities/CaseMetaData';
// import type { Version } from '@/entities/CRF';
// import { CaseListService } from '@/services/CaseListService';
// import { CaseService } from '@/services/CaseService';
// import { DataSynchListService } from '@/services/DataSynchListService';
// import { DataSynchService } from '@/services/DataSynchService';
// import { Contexts } from '@/types/Contexts';
// import { NamedBindings } from '@/types/NamedBindings';
// import { Motivations } from '@/utils/MapForMotivations';
// import type { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
// import type { CoreConfiguration } from '@/interfaces/CoreConfiguration';
//
// describe('Sequence Of Synch', () => {
//   beforeAll(() => {
//     TestHelper.setupContainer();
//     TestHelper.mockStatusService(Contexts.TEST);
//   });
//
//   beforeEach(() => {
//     const container = getContext(Contexts.TEST).container;
//     (container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage).reset();
//     (container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage).reset();
//     (container.get(NamedBindings.StorageForEvents) as InMemoryEventsStorage).reset();
//   });
//
//   test('Casual', async () => {
//     // const container = getContext(Contexts.TEST).container;
//     // if (new Date() > new Date('2025-12-20')) {
//     //   const dataSynchService = container.get(NamedBindings.DataSynchService) as DataSynchService;
//     //   const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
//     //
//     //   const numberOfSequence = 15;
//     //   const list = [];
//     //   for (let i = 0; i < numberOfSequence; i++) {
//     //     list.push(Math.floor(Math.random() * 5));
//     //   }
//     //
//     //   for (const number of list) {
//     //     switch (number) {
//     //       //CREATE
//     //       case 1:
//     //         const storage = container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage;
//     //
//     //         const caseMetaData = new CaseMetaData(
//     //           'test',
//     //           true,
//     //           new Map<string, number>(),
//     //           new Map<string, Version>(),
//     //           new Date(),
//     //           new Date(),
//     //           ''
//     //         );
//     //         const caseService = container.get(NamedBindings.CaseService) as InMemoryCaseServiceForTest;
//     //         await caseListService.addCase(caseMetaData.caseID);
//     //         await dataSynchService.synchronize();
//     //         expect((await storage.getPartials(caseMetaData.caseID))?.length > 0!);
//     //         break;
//     //     }
//     //   }
//     // }
//   });
//
//   test('add1-synch1-update2-synch2', async () => {
//     const container = getContext(Contexts.TEST).container;
//     const caseService = container.get(NamedBindings.CaseService) as CaseService;
//     const caseService2 = new CaseService(
//       container.get(NamedBindings.CaseStorage),
//       container.get(NamedBindings.KeyStorage),
//       container.get(NamedBindings.CRFVersionSelectionStrategy),
//       container.get(NamedBindings.UserService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.CRFService),
//       container.get(NamedBindings.CoreConfiguration),
//       container.get(NamedBindings.CaseVariablesCacheService)
//     );
//
//     const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
//     const caseListService2 = new CaseListService(
//       container.get(NamedBindings.StorageForEvents),
//       container.get(NamedBindings.CaseVariablesCacheService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const inMemorySynchEventsStorage = new InMemoryEventsStorage();
//
//     const dataSynchListService = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchListService2 = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const dataSynchService = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService,
//       dataSynchListService,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchService2 = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService2,
//       dataSynchListService2,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const caseMetaData = {
//       caseID: 'testt',
//       lastUpdate: new Date(),
//       crfVersions: new Map<string, Version>(),
//       caseType: 'Admission',
//       creationDate: new Date(),
//       status: new Map<string, number>(),
//     };
//     await caseService.create('testt', caseMetaData, {});
//     await caseListService.addCase(caseMetaData.caseID, caseMetaData.lastUpdate);
//     await dataSynchService.synchronize();
//
//     await sleep();
//
//     const oldMetaData = await caseService.getCaseMetadataFor(caseMetaData.caseID);
//     if (!oldMetaData) {
//       return console.log('oldmetadata is undefined for ', caseMetaData.caseID);
//     }
//     const newMetadata: CaseMetaData = {
//       ...oldMetaData,
//       lastUpdate: new Date(),
//     };
//
//     await caseService2.update(caseMetaData.caseID, newMetadata, {});
//     await caseListService2.updateCase(caseMetaData.caseID, newMetadata.lastUpdate);
//
//     await sleep();
//
//     const coreConfig: CoreConfiguration = container.get(NamedBindings.CoreConfiguration);
//     coreConfig.debug = true;
//     await dataSynchService2.synchronize();
//     coreConfig.debug = false;
//
//     const storage = container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage;
//     const synchStorage = container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage;
//     const result = await storage.getPartials(caseMetaData.caseID);
//     const result2 = await synchStorage.getPartials(caseMetaData.caseID);
//     expect(result2).toStrictEqual(result);
//   });
//
//   test('add1-update1-synch1-del2-synch2', async () => {
//     const container = getContext(Contexts.TEST).container;
//
//     const caseService = container.get(NamedBindings.CaseService) as CaseService;
//     const caseService2 = new CaseService(
//       container.get(NamedBindings.CaseStorage),
//       container.get(NamedBindings.KeyStorage),
//       container.get(NamedBindings.CRFVersionSelectionStrategy),
//       container.get(NamedBindings.UserService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.CRFService),
//       container.get(NamedBindings.CoreConfiguration),
//       container.get(NamedBindings.CaseVariablesCacheService)
//     );
//
//     const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
//     const caseListService2 = new CaseListService(
//       container.get(NamedBindings.StorageForEvents),
//       container.get(NamedBindings.CaseVariablesCacheService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const inMemorySynchEventsStorage = new InMemoryEventsStorage();
//
//     const dataSynchListService = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchListService2 = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const dataSynchService = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService,
//       dataSynchListService,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchService2 = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService2,
//       dataSynchListService2,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const caseMetaData = {
//       caseID: 'test',
//       lastUpdate: new Date(),
//       crfVersions: new Map<string, Version>(),
//       caseType: 'Admission',
//       creationDate: new Date(),
//       status: new Map<string, number>(),
//     };
//
//     await caseService.create(caseMetaData.caseID, caseMetaData, {});
//     await caseListService.addCase(caseMetaData.caseID, caseMetaData.lastUpdate);
//
//     const oldMetaData = await caseService.getCaseMetadataFor(caseMetaData.caseID);
//     if (!oldMetaData) {
//       return console.log('oldmetadata is undefined for ', caseMetaData.caseID);
//     }
//     const newMetadata: CaseMetaData = {
//       ...oldMetaData,
//       lastUpdate: new Date(),
//     };
//     await caseService.update(caseMetaData.caseID, newMetadata, { ciao: 'ciao' });
//     await dataSynchService.synchronize();
//
//     await sleep(0.1);
//
//     await caseService2.deleteCase(caseMetaData.caseID);
//
//     await sleep(0.1);
//
//     await caseListService2.updateCaseList();
//     await dataSynchService2.synchronize();
//
//     await sleep(0.1);
//
//     const storage = container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage;
//     const synchStorage = container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage;
//     const result = await storage.getPartials(caseMetaData.caseID);
//     const result2 = await synchStorage.getPartials(caseMetaData.caseID);
//     expect(result2).toStrictEqual(result);
//   });
//
//   test('add1-delete1-synch1-restore2-synch2', async () => {
//     const container = getContext(Contexts.TEST).container;
//
//     const caseService = container.get(NamedBindings.CaseService) as CaseService;
//     const caseService2 = new CaseService(
//       container.get(NamedBindings.CaseStorage),
//       container.get(NamedBindings.KeyStorage),
//       container.get(NamedBindings.CRFVersionSelectionStrategy),
//       container.get(NamedBindings.UserService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.CRFService),
//       container.get(NamedBindings.CoreConfiguration),
//       container.get(NamedBindings.CaseVariablesCacheService)
//     );
//
//     const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
//
//     const inMemorySynchEventsStorage = new InMemoryEventsStorage();
//
//     const dataSynchListService = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchListService2 = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const dataSynchService = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService,
//       dataSynchListService,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const caseMetaData = {
//       caseID: 'test',
//       lastUpdate: new Date(),
//       crfVersions: new Map<string, Version>(),
//       caseType: 'Admission',
//       creationDate: new Date(),
//       status: new Map<string, number>(),
//     };
//
//     await caseService.create(caseMetaData.caseID, caseMetaData, {});
//     await caseListService.addCase(caseMetaData.caseID, caseMetaData.lastUpdate);
//     await caseListService.updateCaseList();
//     await caseService.deleteCase(caseMetaData.caseID);
//     await dataSynchService.synchronize();
//
//     await sleep();
//
//     const caseListService2 = new CaseListService(
//       container.get(NamedBindings.StorageForEvents),
//       container.get(NamedBindings.CaseVariablesCacheService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const dataSynchService2 = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService2,
//       dataSynchListService2,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     await caseListService2.updateCaseList();
//     await caseService2.restoreCase(caseMetaData.caseID);
//     await caseListService2.updateCaseList();
//     await dataSynchService2.synchronize();
//
//     await sleep();
//
//     const storage = container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage;
//     const synchStorage = container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage;
//     const result = await storage.getPartials(caseMetaData.caseID);
//     const result2 = await synchStorage.getPartials(caseMetaData.caseID);
//     expect(result2).toStrictEqual(result);
//   });
//
//   test('add1-synch1-permanentlyDelete2-synch2', async () => {
//     const container = getContext(Contexts.TEST).container;
//
//     const caseService = container.get(NamedBindings.CaseService) as CaseService;
//     const caseService2 = new CaseService(
//       container.get(NamedBindings.CaseStorage),
//       container.get(NamedBindings.KeyStorage),
//       container.get(NamedBindings.CRFVersionSelectionStrategy),
//       container.get(NamedBindings.UserService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.CRFService),
//       container.get(NamedBindings.CoreConfiguration),
//       container.get(NamedBindings.CaseVariablesCacheService)
//     );
//
//     const caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
//     const caseListService2 = new CaseListService(
//       container.get(NamedBindings.StorageForEvents),
//       container.get(NamedBindings.CaseVariablesCacheService),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const inMemorySynchEventsStorage = new InMemoryEventsStorage();
//
//     const dataSynchListService = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchListService2 = new DataSynchListService(
//       inMemorySynchEventsStorage,
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const dataSynchService = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService,
//       dataSynchListService,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//     const dataSynchService2 = new DataSynchService(
//       container.get(NamedBindings.CaseStorage),
//       caseListService2,
//       dataSynchListService2,
//       container.get(NamedBindings.DataSynchStorage),
//       container.get(NamedBindings.CurrentContextService),
//       container.get(NamedBindings.UserService)
//     );
//
//     const caseMetaData = {
//       caseID: 'test',
//       lastUpdate: new Date(),
//       crfVersions: new Map<string, Version>(),
//       caseType: 'Admission',
//       creationDate: new Date(),
//       status: new Map<string, number>(),
//     };
//
//     await caseService.create(caseMetaData.caseID, caseMetaData, {});
//     await caseListService.addCase(caseMetaData.caseID, caseMetaData.lastUpdate);
//     await dataSynchService.synchronize();
//
//     await sleep();
//
//     await caseListService2.permanentlyDeleteCase(caseMetaData.caseID, Motivations.DUPLICATE, new Date());
//     await caseService2.permanentlyDeleteCase(caseMetaData.caseID, Motivations.DUPLICATE);
//
//     await dataSynchService2.synchronize();
//     await sleep();
//
//     const storage = container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage;
//     const synchStorage = container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage;
//
//     const result = await storage.getPartials(caseMetaData.caseID);
//     const result2 = await synchStorage.getPartials(caseMetaData.caseID);
//     expect(result2).toStrictEqual(result);
//     expect(result2).toStrictEqual([]);
//   });
// });

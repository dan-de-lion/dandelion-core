// import { beforeAll, beforeEach, expect, describe, test } from 'vitest';
// import { CaseListServiceInterface } from '@/interfaces/CaseListServiceInterface';
// import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
// import type { InMemoryEventsStorage } from '@/storages/InMemoryEventsStorage';
// import { getContext } from '@/ContainersConfig';
// import TestHelper, { sleep } from '../../../../utils/TestHelper';
// import type { CaseMetaData } from '@/entities/CaseMetaData';
// import { Case } from '@/entities/Cases';
// import type { Version } from '@/entities/CRF';
// import type { CaseService } from '@/services/CaseService';
// import { CrfStatus } from '@/services/CaseStatusService';
// import type { DataSynchService } from '@/services/DataSynchService';
// import { Contexts } from '@/types/Contexts';
// import { NamedBindings } from '@/types/NamedBindings';

// describe('Data Synch Service', () => {
//   let dataSynchService: DataSynchService;
//   let caseListService: CaseListServiceInterface;
//   let caseStorage: InMemoryCaseStorage;
//   let caseService: CaseService;
//   beforeAll(() => {
//     TestHelper.setupContainer();
//     TestHelper.mockStatusService(Contexts.TEST);
//     const container = getContext(Contexts.TEST).container;
//     dataSynchService = container.get(NamedBindings.DataSynchService) as DataSynchService;
//     caseListService = container.get(NamedBindings.CaseListService) as CaseListServiceInterface;
//     caseStorage = container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage;
//     caseService = container.get(NamedBindings.CaseService) as CaseService;
//   });

//   beforeEach(() => {
//     const container = getContext(Contexts.TEST).container;
//     (container.get(NamedBindings.DataSynchStorage) as InMemoryCaseStorage).reset();
//     (container.get(NamedBindings.StorageForSynchEvents) as InMemoryEventsStorage).reset();
//   });

//   const caseMetaData: CaseMetaData = {
//     caseID: 'test',
//     lastUpdate: new Date(),
//     crfVersions: new Map<string, Version>(),
//     caseType: 'Admission',
//     creationDate: new Date(),
//     status: new Map<string, CrfStatus>(),
//     centreCode: '',
//     frozenAccessLevels: new Map<string, boolean>()
//   };

//   test('Test if a new Case is added and Synched correctly', async () => {
//     await caseService.create(new Case(caseMetaData.caseID, {}, caseMetaData));
//     await caseListService.addCase(caseMetaData.caseID, caseMetaData.lastUpdate);
//     await sleep(0.1);
//     await dataSynchService.synchronize();
//     await sleep(0.1);
//     const partialStoredCases = await caseStorage.getPartials(caseMetaData.caseID);
//     const newMetaData = await caseService.getDataFor(caseMetaData.caseID);
//     expect(partialStoredCases.length).toBe(1);
//     expect(newMetaData).toBe(caseMetaData);
//   });

//   test('Test if a new test case is not synched when added', async () => {
//     await caseService.create(new Case(caseMetaData.caseID, {}, caseMetaData));
//     await dataSynchService.synchronize();
//     await sleep();
//     const partialStoredCases = await caseStorage.getPartials(caseMetaData.caseID);
//     expect(partialStoredCases.length).toBe(0);
//   });
// });

import { beforeAll, expect, describe, test } from 'vitest';
import { addContext, ContainerTypes, getContainer } from '@/ContainersConfig';
import { setupDandelion } from '@/index';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import type CaseVariablesCacheService from '@/services/CaseVariablesCacheService';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { CrfServiceFromFolderForTests } from '../../../mock-services/CrfServiceFromFolderForTests';
import { InMemoryCacheStorageForTests } from '../../../mock-repositories/InMemoryCacheStorageForTests';
import { MockCRFVersionSelectionStrategy } from '../../../mock-services/MockCRFVersionSelectionStrategy';
import { UserService } from '../../../mock-services/UserService';

describe('Test on CaseVariableCaacheService', () => {
  beforeAll(() => {
    setupDandelion({
      coreConfiguration: new CoreConfiguration({
        baseUrlForXslt: 'public/xslt/',
        encrypt: false,
        debug: false,
        listOfCaseTypes: [
          new CaseConfiguration({
            caseName: 'FirstCaseType',
            caseLinkingConfiguration: [{ caseTypeName: 'Patient', caseIDVariable: 'admission.patientId' }],
            mainCrf: { crfName: 'FirstCaseType' },
            referenceDateVariableName: 'model.TextVariable',
            cachedVariables: ['first', 'second'],
          }),
          new CaseConfiguration({
            caseName: 'SecondCaseType',
            caseLinkingConfiguration: [{ caseTypeName: 'Patient', caseIDVariable: 'admission.patientId' }],
            mainCrf: { crfName: 'SecondCaseType' },
            referenceDateVariableName: 'model.TextVariable',
            cachedVariables: ['third', 'fourth'],
          })]
      }),
      sharedServices: [
        { name: NamedBindings.Logger, class: ConsoleLogger },
        { name: NamedBindings.CRFService, class: CrfServiceFromFolderForTests },
        { name: NamedBindings.VariablesCacheStorage, class: InMemoryCacheStorageForTests },
        { name: NamedBindings.UserService, class: UserService },
      ],
      services: [{ name: NamedBindings.CRFVersionSelectionStrategy, class: MockCRFVersionSelectionStrategy, containerTypes: [ContainerTypes.DEFAULT] }]
    });
    addContext(Contexts.TEST);
  });

  describe('Works correctly', () => {
    test('Cache have variables from every CaseType', async () => {
      const caseVariableCacheService: CaseVariablesCacheService = getContainer(Contexts.TEST).get(
        NamedBindings.CaseVariablesCacheService
      ) as CaseVariablesCacheService;
      //necessary because cachedVariables is protected
      //@ts-expect-error
      expect(caseVariableCacheService.cachedVariables).toEqual(['first', 'second', 'third', 'fourth']);
    });
  });
});

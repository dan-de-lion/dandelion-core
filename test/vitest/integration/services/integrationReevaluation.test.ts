import { v4 } from 'uuid';
import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import InMemoryCRFServiceForTest from '../../../mock-services/InMemoryCRFServiceForTest';
import { ContainerTypes, getContainer } from '@/ContainersConfig';
import { Case } from '@/entities/Cases';
import { CaseMetaData } from '@/entities/CaseMetaData';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type CaseReevaluationService from '@/services/CaseReevaluationService';
import { ConsoleLogger } from '@/services/Logger';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

describe('Reevaluation', () => {
  const model = { Admission: { firstname: 'pippo', lastname: 'last' } };
  const caseID = v4();
  const caseMetaData: CaseMetaData = {
    caseID,
    centreCode: '',
    status: new Map().set('*', new Map().set("*", 1)),
    crfVersions: new Map().set('Admission', '0.0.1'),
    creationDate: new Date(),
    lastUpdate: new Date(),
    frozenAccessLevels: new Map(),
    caseType: 'Admission',
  };
  let caseReevaluationService: CaseReevaluationService;
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    const container = getContainer(Contexts.TEST);
    const reevaluationContainer = getContainer(Contexts.TEST, ContainerTypes.REEVALUATION);
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    // @ts-expect-error - readonly property
    coreConfiguration.debounceForUpdatingStatus = 1000;
    const logger = container.get(NamedBindings.Logger) as ConsoleLogger;
    const crfService = new InMemoryCRFServiceForTest(logger, coreConfiguration,
      [new CRF('Admission', '1.0.0',
        `<root>
            <dataClass>
                <variable name="firstname" dataType="text" />
                <variable name="lastname" dataType="text" />
                <variable name="test" dataType="text" computedFormula="model.Admission?.firstname + ' ' + model.Admission?.lastname" />
            </dataClass>
          </root>`)],
      new Map<string, CRFVersionValidity[]>([['Admission', [new CRFVersionValidity(0, '1.0.0', new Date('1990-12-12'))]]]));
    container.bind(NamedBindings.CRFService).toDynamicValue(() => crfService);
    reevaluationContainer.rebind(NamedBindings.CRFVersionSelectionStrategy).toDynamicValue(() => ({
      getVersionFor: async () => '1.0.0',
      getUpdatedCRFVersionsByCase: async () => new Map().set('Admission', '1.0.0'),
      getCRFConfigurationByCase: () => [{ crfName: 'Admission' }],
      getCRFConfigurationsByCentreCode: () => [{ crfName: 'Admission' }]
    }));
    reevaluationContainer.bind(NamedBindings.CRFService).toDynamicValue(() => crfService);
  });

  beforeEach(() => {
    caseReevaluationService = getContainer(Contexts.TEST, ContainerTypes.REEVALUATION).get(NamedBindings.CaseReevaluationService) as CaseReevaluationService;
  })

  test('Check that computed-formula is updated after reevaluation', async () => {
    const caseService = getContainer(Contexts.TEST).get(NamedBindings.CaseService) as CaseServiceInterface;
    await caseService.create(new Case(caseID, model, caseMetaData));
    const { data } = await caseReevaluationService.reevaluateCase(new Case(caseID, model, caseMetaData));
    expect(data).toStrictEqual({ Admission: { firstname: 'pippo', lastname: 'last', test: 'pippo last' } });
  });

  test('Check that Crfs status is correctly saved after reevaluation', async () => {
    const caseService = getContainer(Contexts.TEST).get(NamedBindings.CaseService) as CaseServiceInterface;
    await caseService.create(new Case(caseID, model, caseMetaData));
    const metaAfterCreation = await caseService.getCaseMetaDataFor(caseID);
    expect(metaAfterCreation?.status).toStrictEqual(new Map().set('*', new Map().set("*", 1)));
    const { saveMetaData } = await caseReevaluationService.reevaluateCase(new Case(caseID, model, caseMetaData));
    expect(saveMetaData?.status).toStrictEqual(new Map().set('*', new Map().set('*', 3)));
  });
});
import path from 'path';
import { beforeAll, expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { getContainer, ContainerTypes } from '@/ContainersConfig';
import type CaseStatusService from '@/services/CaseStatusService';
import type CurrentCaseService from '@/services/CurrentCaseService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';
import { VueWrapper } from '@vue/test-utils';
import { nextTick } from 'vue';
import CurrentCaseServiceForNewCase from '@/services/CurrentCaseServiceForNewCase';
import { CrfServiceFromFolderForTests } from "../../../mock-services/CrfServiceFromFolderForTests";

describe('Test on CaseStatusService of Status', () => {
  let caseStatusService: CaseStatusService;
  TestHelper.testSetupWithCaseOpening(true);

  beforeAll(() => {
    caseStatusService = getContainer(Contexts.TEST).get(NamedBindings.CaseStatusService) as CaseStatusService;
    const crfService = getContainer(Contexts.TEST).get(NamedBindings.CRFService) as CrfServiceFromFolderForTests;
    crfService.getAccessLevelForVariable = () => "public";
  });

  describe('Standard', () => {
    test('Patient status service correct calculated with Text Variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/standard-text-variable.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Boolean Variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      component.find('input').element.checked = true;
      await component.find('input').setValue(true);
      await component.find('input').trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Date Variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/standard-date-variable.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('2022-12-01');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Multiplechoice Variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Singlechoice Variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with hidden variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/visible-for-status-text-variable.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
    });
  });

  describe('With Warnings', () => {
    test('Patient status service correct calculated with Text Variables Warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/text-variable-warning.xml');
      const textVar = component.findAll('input')[0];
      await textVar.setValue('this is a test');
      expect(caseStatusService.getStatus().value).toBe(1);
      const warningCheckbox = component.findAll('input')[1];
      warningCheckbox.element.checked = true;
      await warningCheckbox.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Boolean Variables Warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/boolean-variable-warning.xml');
      const booleanVar = component.findAll('input')[0];
      expect(caseStatusService.getStatus().value).toBe(2);
      booleanVar.element.checked = true;
      await booleanVar.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(1);
      const warningCheckbox = component.findAll('input')[1];
      warningCheckbox.element.checked = true;
      await warningCheckbox.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Date Variables Warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/date-variable-warning.xml');
      const dateVar = component.findAll('input')[0];
      await dateVar.setValue('2022-12-01');
      expect(caseStatusService.getStatus().value).toBe(1);
      const warningCheckbox = component.findAll('input')[1];
      warningCheckbox.element.checked = true;
      await warningCheckbox.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Multiplechoice Variables Warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/multiplechoice-variable-warning.xml');
      const multipleVar = component.findAll('input')[0];
      const multipleVar2 = component.findAll('input')[1];
      expect(caseStatusService.getStatus().value).toBe(2);
      await multipleVar.setValue(true);
      await multipleVar.trigger('input');
      await multipleVar2.setValue(true);
      await multipleVar2.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(1);
      const warningCheckbox = component.findAll('input')[3];
      warningCheckbox.element.checked = true;
      await warningCheckbox.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Patient status service correct calculated with Singlechoice Variables Warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/singlechoice-variable-warning.xml');
      const singleVar = component.findAll('input')[0];
      await singleVar.setValue(true);
      await singleVar.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(1);
      const warningCheckbox = component.findAll('input')[3];
      warningCheckbox.element.checked = true;
      await warningCheckbox.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });
  });

  describe('With Errors', () => {
    test('Patient status service correct calculated with Text Variables Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/text-variable-error.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue('this is a test');
      expect(caseStatusService.getStatus().value).toBe(1);
    });

    test('Patient status service correct calculated with Boolean Variables Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/boolean-variable-error.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(caseStatusService.getStatus().value).toBe(1);
    });

    test('Patient status service correct calculated with Date Variables Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/date-variable-error.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('2022-12-01');
      expect(caseStatusService.getStatus().value).toBe(1);
    });

    test('Patient status service correct calculated with Multiplechoice Variables Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/multiplechoice-variable-error.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(1);
    });

    test('Patient status service correct calculated with Singlechoice Variables Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/singlechoice-variable-error.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(1);
    });
  });

  describe('With Requirements', () => {
    test('Patient status correctly calculated with Text Variables Requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/text-variable-requirement.xml');
      const textVar = component.find('input');
      expect(caseStatusService.getStatus().value).toBe(2);
      await textVar.setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
      await textVar.setValue('');
      await textVar.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(2);
    });

    test('Patient status service correct calculated with Boolean Variables Requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/boolean-variable-requirement.xml');
      const booleanVar = component.findAll('input')[0];
      expect(caseStatusService.getStatus().value).toBe(2);
      booleanVar.element.checked = true;
      await booleanVar.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
      booleanVar.element.checked = false;
      await booleanVar.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(2);
    });

    test('Patient status service correct calculated with Date Variables Requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/date-variable-requirement.xml');
      const dateVar = component.findAll('input')[0];
      expect(caseStatusService.getStatus().value).toBe(2);
      await dateVar.setValue('2132-12-01');
      expect(caseStatusService.getStatus().value).toBe(3);
      await dateVar.setValue('2022-12-01');
      expect(caseStatusService.getStatus().value).toBe(2);
    });

    test('Patient status service correct calculated with Multiplechoice Variables Requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/multiplechoice-variable-requirement.xml');
      const multipleVar = component.findAll('input')[0];
      const multipleVar2 = component.findAll('input')[1];
      const multipleVar3 = component.findAll('input')[2];
      expect(caseStatusService.getStatus().value).toBe(2);
      await multipleVar.setValue(true);
      await multipleVar.trigger('input');
      await multipleVar2.setValue(true);
      await multipleVar2.trigger('input');
      await multipleVar3.setValue(true);
      await multipleVar3.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
      await multipleVar.setValue(false);
      await multipleVar.trigger('input');
      await multipleVar2.setValue(false);
      await multipleVar2.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(2);
    });

    test('Patient status service correct calculated with Singlechoice Variables Requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/singlechoice-variable-requirement.xml');
      const singleVar = component.findAll('input')[0];
      const singleVar2 = component.findAll('input')[1];
      expect(caseStatusService.getStatus().value).toBe(2);
      await singleVar.setValue(true);
      await singleVar.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
      await singleVar2.setValue(true);
      await singleVar2.trigger('input');
      expect(caseStatusService.getStatus().value).toBe(2);
    });
  });

  describe('Counters', () => {
    test('Errors counter should work correctly', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/text-variable-error.xml');
      expect(caseStatusService.getErrorCounter().value).toBe(0);
      await component.findAll('input')[0].setValue('test');
      await nextTick();
      expect(caseStatusService.getErrorCounter().value).toBe(1);
    });

    test('Warnings counter should work correctly', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/text-variable-warning.xml');
      expect(caseStatusService.getWarningCounter().value).toBe(0);
      await component.findAll('input')[0].setValue('test');
      expect(caseStatusService.getWarningCounter().value).toBe(1);
    });

    test('Requirements counter should work correctly', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/text-variable-requirement.xml');
      expect(caseStatusService.getRequirementCounter().size).toBe(1);
      await component.findAll('input')[0].setValue('test');
      expect(caseStatusService.getRequirementCounter().size).toBe(0);
    });
  });

  describe('NewCase', () => {
    test('Test on variable dataType Text in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-text-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-text-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.find('input').setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Boolean in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-boolean-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      component.find('input').element.checked = true;
      await component.find('input').setValue(true);
      await component.find('input').trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-boolean-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      component2.find('input').element.checked = true;
      await component2.find('input').setValue(true);
      await component2.find('input').trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Date in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-date-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('2024-12-01');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-date-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.find('input').setValue('2024-12-01');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Multiplechoice in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-multiplechoice-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-multiplechoice-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.findAll('input')[0].setValue(true);
      await component2.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Singlechoice in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-singlechoice-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-singlechoice-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.findAll('input')[0].setValue(true);
      await component2.findAll('input')[0].trigger('input');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Number in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-number-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue(1);
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-text-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.find('input').setValue(1);
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Time in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-time-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('01:00');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-time-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.find('input').setValue('01:00');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Set in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-set-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(3);
      await component.findAll('button')[0].trigger('click');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.findAll('input')[0].setValue('text');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-set-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(3);
      await component2.findAll('button')[0].trigger('click');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.findAll('input')[0].setValue('text');
      expect(caseStatusService.getStatus().value).toBe(3);
    });

    test('Test on variable dataType Text in combination with Output and Support in newCase', async () => {
      const { caseStatusService, currentCaseService, component } = await mountComponentForNewCase('/newCase/newcase-textoutputsupport-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component.find('input').setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
      const component2 = await mountSecondComponentForNewCase(component, currentCaseService, '/newCase/newcase-textoutputsupport-crf.xml');
      expect(caseStatusService.getStatus().value).toBe(2);
      await component2.find('input').setValue('test');
      expect(caseStatusService.getStatus().value).toBe(3);
    });
  });
});

async function mountComponentForNewCase(xmlPath: string) {
  const caseStatusService = getContainer(Contexts.TEST, ContainerTypes.NEW_CASE).get(NamedBindings.CaseStatusService) as CaseStatusService;
  const currentCaseService: CurrentCaseService = getContainer(Contexts.TEST, ContainerTypes.NEW_CASE).get(NamedBindings.CurrentCaseServiceForNewCase) as CurrentCaseServiceForNewCase;
  await currentCaseService.open('12345', {});
  const xml = TestHelper.requireFile(xmlPath);
  const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, path.parse(xmlPath).name, '0.0.1',
    true, true, true, 2, currentCaseService.getModel(), '', Contexts.TEST, 'NewCase', '', ContainerTypes.NEW_CASE
  );
  return { caseStatusService, currentCaseService, component };
}

async function mountSecondComponentForNewCase(component: VueWrapper<any>, currentCaseService: CurrentCaseServiceForNewCase, xmlPath: string) {
  component.unmount();
  await currentCaseService.terminateEditing();
  await currentCaseService.open('56789', {});
  const xml = TestHelper.requireFile(xmlPath);
  return await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, path.parse(xmlPath).name, '0.0.1',
    true, true, true, 2, currentCaseService.getModel(), '', Contexts.TEST, 'NewCase', '', ContainerTypes.NEW_CASE
  );
}
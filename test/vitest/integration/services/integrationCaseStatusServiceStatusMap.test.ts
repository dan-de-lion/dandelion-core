import path from 'path';
import { afterEach, beforeAll, beforeEach, describe, expect, test } from 'vitest';
import { enableAutoUnmount } from '@vue/test-utils';
import { ContainerTypes, getContainer, getOwnershipForAllContainers } from '@/ContainersConfig';
import { CRF } from '@/entities/CRF';
import { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { CrfServiceFromFolderForTests } from '../../../mock-services/CrfServiceFromFolderForTests';
import type CaseStatusService from '@/services/CaseStatusService';
import type CurrentCaseService from '@/services/CurrentCaseService';
import CurrentCaseServiceForNewCase from '@/services/CurrentCaseServiceForNewCase';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';
import TestHelper from '../../../utils/TestHelper';
import Helpers from '@/utils/Helpers';

describe('Test on CaseStatusService StatusMap', () => {
  const model = { model: { referenceDate: new Date() } };
  let caseStatusService: CaseStatusService;
  beforeAll(() => {
    TestHelper.setupContainer('Admission', [], [],
      [{ accessLevel: '*', crfName: '*' },
      { accessLevel: 'OnlyAdmin', crfName: '*' },
      { accessLevel: 'OnlyUser', crfName: '*' },
      { accessLevel: 'public', crfName: '*' }]);
    getContainer(Contexts.TEST).rebind(NamedBindings.CaseListService).toConstantValue({
      applyUpdateCase: () => { },
      addCase: () => { },
      changedReferenceDate: () => { },
      updateCase: () => { },
    });
    const crfService = getContainer(Contexts.TEST).get(NamedBindings.CRFService) as CrfServiceFromFolderForTests;
    crfService.getAccessLevelsForCRF = (_crfName, _crfVersion) => ({
      'without-access-level': { accessLevel: 'public' },
      'access-level-only-admin': { accessLevel: 'OnlyAdmin' },
      'access-level-only-admin-with-access-level-only-user': { textVariable: { accessLevel: 'OnlyAdmin' }, textVariable2: { accessLevel: 'OnlyUser' } },
      'access-level-only-admin-with-access-level-public': { textVariable: { accessLevel: 'OnlyAdmin' }, textVariable2: { accessLevel: 'public' } },
      'access-level-public-with-access-level-only-user': { textVariable: { accessLevel: 'public' }, textVariable2: { accessLevel: 'OnlyUser' } }
    })
    crfService.getAccessLevelForVariable = (variableName, _crfVersions) => {
      const crfName = crfService.getCrfNameFromVariable(variableName);
      const accessLevels = crfService.getAccessLevelsForCRF(crfName, '0.0.1');
      return Helpers.searchMostSpecificVariablePrefix(variableName, accessLevels);
    }
    crfService.getCRF = (crfName, crfVersion) => Promise.resolve(new CRF(crfName, crfVersion,
      `<root>
          <dataClass>
          <variable name="textVariable" dataType="text" />
          <variable name="textVariable2" dataType="text" accessLevel="OnlyUser" />
          </dataClass>
          <dataClass name="NewCase">
              <variable name="x" dataType="text" />
          </dataClass>
      </root>`));
    getOwnershipForAllContainers();
  });

  beforeEach(async () => {
    const currentCaseService: CurrentCaseService = getContainer(Contexts.TEST).get(NamedBindings.CurrentCaseService) as CurrentCaseService;
    caseStatusService = getContainer(Contexts.TEST).get(NamedBindings.CaseStatusService) as CaseStatusService;
    caseStatusService.reset();
    const currentCaseServiceForNewCase = getContainer(Contexts.TEST, ContainerTypes.NEW_CASE).get(NamedBindings.CurrentCaseServiceForNewCase) as CurrentCaseServiceForNewCase;
    await currentCaseServiceForNewCase.open('12345', model);
    await currentCaseServiceForNewCase.save();
    await currentCaseService.open('12345');
  });

  afterEach(async () => {
    const currentCaseService: CurrentCaseService = getContainer(Contexts.TEST).get(NamedBindings.CurrentCaseService) as CurrentCaseService;
    await currentCaseService.terminateEditing();
  });
  enableAutoUnmount(afterEach);

  describe('Standard', () => {
    test('Patient status service correct calculated in a standard case with accessLevel:*, crfName:*', async () => {
      const component = await mountComponent('/caseStatusPossibility/without-access-level.xml');
      const statusMap = caseStatusService.getStatusMap().value;
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 2)).set('OnlyAdmin', new Map().set('*', 0)).set('public', new Map().set('*', 2)));
      await component.find('input').setValue('test');
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 3)).set('OnlyAdmin', new Map().set('*', 0)).set('public', new Map().set('*', 3)));
    });

    test('Patient status service correct calculated when i am Admin and the variable is with accessLevel:OnlyAdmin, crfName: *', async () => {
      const component = await mountComponent('/caseStatusPossibility/access-level-only-admin.xml');
      const statusMap = caseStatusService.getStatusMap().value;
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 2)).set('OnlyAdmin', new Map().set('*', 2)).set('public', new Map().set('*', 0)));
      await component.find('input').setValue('test');
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 3)).set('OnlyAdmin', new Map().set('*', 3)).set('public', new Map().set('*', 0)));
    });

    test('Patient status service correct calculated when i am Admin and there are 2 variables in different accessLevel, 1 into an accessLevel that i cannot read', async () => {
      const component = await mountComponent('/caseStatusPossibility/access-level-only-admin-with-access-level-only-user.xml');
      const statusMap = caseStatusService.getStatusMap().value;
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 2)).set('OnlyAdmin', new Map().set('*', 2)).set('public', new Map().set('*', 0)));
      await component.find('input').setValue('test');
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 3)).set('OnlyAdmin', new Map().set('*', 3)).set('public', new Map().set('*', 0)));
    });

    test('Patient status service correct calculated when i am Admin and there are 2 variables in different accessLevel, both in an accessLevel i can read', async () => {
      const component = await mountComponent('/caseStatusPossibility/access-level-only-admin-with-access-level-public.xml');
      const statusMap = caseStatusService.getStatusMap().value;
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 2)).set('OnlyAdmin', new Map().set('*', 2)).set('public', new Map().set('*', 2)));
      await component.findAll('input')[0].setValue('test');
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 2)).set('OnlyAdmin', new Map().set('*', 3)).set('public', new Map().set('*', 2)));
      await component.findAll('input')[1].setValue('test2');
      expect(statusMap).toStrictEqual(new Map().set('*', new Map().set('*', 3)).set('OnlyAdmin', new Map().set('*', 3)).set('public', new Map().set('*', 3)));
    });
  });

  describe('Saving of statuses', () => {
    test('Test that at the save, the only overwritten statuses are the statuses that i can write', async () => {
      const component = await mountComponent('/caseStatusPossibility/access-level-public-with-access-level-only-user.xml');
      const currentCaseService = getContainer(Contexts.TEST).get(NamedBindings.CurrentCaseService) as CurrentCaseService;
      //@ts-ignore
      currentCaseService.previousStatuses = [['public', new Map().set('*', 1)], ['OnlyUser', new Map().set('*', 10)]];
      await component.findAll('input')[0].setValue('test');
      await currentCaseService.save();
      const caseService = getContainer(Contexts.TEST).get(NamedBindings.CaseService) as CaseServiceInterface;
      const metaData = await caseService.getCaseMetaDataFor('12345');
      expect(metaData.status).toStrictEqual(new Map().set('*', new Map([['*', 3]])).set('OnlyAdmin', new Map([['*', 0]])).set('public', new Map([['*', 3]])).set('OnlyUser', new Map([['*', 10]])));
      await component.findAll('input')[0].setValue('');
      await currentCaseService.save();
      expect(metaData.status).toStrictEqual(new Map().set('*', new Map([['*', 2]])).set('OnlyAdmin', new Map([['*', 0]])).set('public', new Map([['*', 2]])).set('OnlyUser', new Map([['*', 10]])));
    });
  });
});

async function mountComponent(xmlPath: string) {
  const xml = TestHelper.requireFile(xmlPath);
  return await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, path.parse(xmlPath).name, '0.0.1',
    false, false, false, 10, undefined, '', Contexts.TEST, '', '', ContainerTypes.DEFAULT, true);
}
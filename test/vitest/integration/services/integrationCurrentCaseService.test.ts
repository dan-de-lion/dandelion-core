import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import { nextTick } from 'vue';
import { CrfServiceFromFolderForTests } from '../../../mock-services/CrfServiceFromFolderForTests';
import InMemoryCRFServiceForTest from '../../../mock-services/InMemoryCRFServiceForTest';
import TestHelper, { sleep } from '../../../utils/TestHelper';
import { CaseClosed } from '@/application-events/CaseClosed';
import type { CaseCreated } from '@/application-events/CaseCreated';
import { ContainerTypes, getContainer, getContext } from '@/ContainersConfig';
import { CRF, CRFVersionValidity } from '@/entities/CRF';
import { PartialStoredCase } from '@/entities/PartialStoredCase';
import type { CaseServiceInterface } from '@/interfaces/CaseServiceInterface';
import { CoreConfigurationInterface } from '@/interfaces/CoreConfiguration';
import type { CurrentCaseServiceInterface } from '@/interfaces/CurrentCaseServiceInterface';
import { ConsoleLogger } from '@/services/Logger';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
import { ApplicationEvents } from '@/types/ApplicationEvents';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';

describe('Test on CurrentCaseService', () => {
  let currentCaseServiceForNewCase: CurrentCaseServiceInterface;
  let currentCaseService: CurrentCaseServiceInterface;
  const model = { model: { referenceDate: new Date() } };
  let xml = '';
  beforeAll(() => {
    TestHelper.setupContainer();
    const crfService = getContainer(Contexts.TEST).get(NamedBindings.CRFService) as CrfServiceFromFolderForTests;
    crfService.getAccessLevelForVariable = () => "public";
    xml = `<root>
            <dataClass>
                <variable name="firstname" dataType="text" />
                <variable name="lastname" dataType="text" />
                <variable name="test" dataType="text" computedFormula="model.Admission?.firstname" />
            </dataClass>
          </root>`;
    const container = getContainer(Contexts.TEST, ContainerTypes.REEVALUATION);
    const coreConfiguration = container.get(NamedBindings.CoreConfiguration) as CoreConfigurationInterface;
    const logger = container.get(NamedBindings.Logger) as ConsoleLogger;
    container.bind(NamedBindings.CRFService).toDynamicValue(() =>
      new InMemoryCRFServiceForTest(logger, coreConfiguration,
        [new CRF('Admission', '0.0.1', xml)], new Map([['Admission', [new CRFVersionValidity(1, '0.0.1', new Date('1990-12-12'))]]]))).inSingletonScope();

    (container.get(NamedBindings.CaseStorage) as InMemoryCaseStorage).createCase([new PartialStoredCase('{}', 'public')],
      {
        caseID: '12345',
        centreCode: '',
        creationDate: new Date(),
        caseType: 'Admission',
        status: new Map(),
        crfVersions: new Map(),
        lastUpdate: new Date(),
        frozenAccessLevels: new Map()
      });
  });

  beforeEach(() => {
    const container = getContainer(Contexts.TEST, ContainerTypes.NEW_CASE);
    currentCaseServiceForNewCase = container.get(NamedBindings.CurrentCaseServiceForNewCase) as CurrentCaseServiceInterface;
    currentCaseService = getContainer(Contexts.TEST).get(NamedBindings.CurrentCaseService) as CurrentCaseServiceInterface;
  });

  test('Check that Crfs status is correctly saved after save', async () => {
    const caseService = getContainer(Contexts.TEST).get(NamedBindings.CaseService) as CaseServiceInterface;
    await currentCaseServiceForNewCase.open('12345', model);
    await currentCaseServiceForNewCase.save();
    const metaAfterCreation = await caseService.getCaseMetaDataFor('12345');
    expect(metaAfterCreation?.status).toStrictEqual(new Map().set("*", new Map().set('*', 2)));
    await currentCaseService.open('12345');
    const component2 = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, 'Admission', '0.0.1',
      false, false, false, 10, undefined, "", Contexts.TEST, "", "", ContainerTypes.DEFAULT, true);
    const firstInput = component2.findAll('input')[0];
    const secondInput = component2.findAll('input')[1];
    firstInput.setValue('Prova');
    firstInput.trigger('input');
    await nextTick();
    secondInput.setValue('test');
    secondInput.trigger('input');
    await nextTick();
    await currentCaseService.save();
    const metaAfterSave = await caseService.getCaseMetaDataFor('12345');
    expect(metaAfterSave?.status).toStrictEqual(new Map().set("*", new Map().set('*', 3)));
  });

  test('No closing events are launched at the newCase request', async () => {
    let eventInContextTest = new CaseClosed('', ContainerTypes.DEFAULT);
    getContext(Contexts.TEST).addEventListener(ApplicationEvents.CaseCreated, (e: CaseCreated) => eventInContextTest = e);
    await currentCaseService.open('12345', {});
    await sleep(0.1);
    expect(eventInContextTest).not.toBe(`${ApplicationEvents.CaseClosed}.test`);
  });
});

import path from 'path';
import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('WarningList warnings', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Warnings linked to variables', () => {
    test('Boolean Variable with warning', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/boolean-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/boolean-variable-warning.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      scComponent.find('input').element.checked = true;
      await scComponent.find('input').trigger('input');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe(
        'wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper'
      );
    });

    test('Color Variable with warning', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/color-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/color-variable-warning.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.find('input').setValue('#00f900');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Date Variable with warning', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/date-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/date-variable-warning.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.find('input').setValue('2018-03-28');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Multiplechoice Variable with warning', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/multiplechoice-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/multiplechoice-variable-warning.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      await scComponent.findAll('input')[1].setValue(true);
      await scComponent.findAll('input')[1].trigger('input');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Text Variable with warning', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/text-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/text-variable-warning.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue('test');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Singlechoice Variable with warning', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/singlechoice-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/singlechoice-variable-warning.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test("Only one warning should appear in the list even if the reference variable 'duplicates' it in scc", async () => {
      const warningListComponent = await mountWarningListComponent('/warnings/reference-variable-warning.xml');
      const warning = warningListComponent.find('.warning');
      expect(warning.attributes('id')).toBe('wList-test_groupVariable.mainVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning should change class when is clicked', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/boolean-variable-warning.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/boolean-variable-warning.xml');
      scComponent.find('input').element.checked = true;
      await scComponent.find('input').trigger('input');
      let warning = warningListComponent.find('.warning');
      let solvedWarning = warningListComponent.find('.solvedWarning');
      expect(warning.exists()).toBe(true);
      expect(solvedWarning.exists()).toBe(false);
      expect(warning.attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
      warning.find('input').element.checked = true;
      await warning.find('input').trigger('input');
      warning = warningListComponent.find('.warning');
      solvedWarning = warningListComponent.find('.solvedWarning');
      expect(warning.exists()).toBe(false);
      expect(solvedWarning.exists()).toBe(true);
      expect(solvedWarning.attributes('id')).toBe('wList-test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });
  });

  describe('Warnings in variables', () => {
    test('Warning inside boolean variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-boolean-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-boolean-variable.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      scComponent.find('input').element.checked = true;
      await scComponent.find('input').trigger('input');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.booleanVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Color variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-color-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-color-variable.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.find('input').setValue('#00f900');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.colorVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Date variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-date-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-date-variable.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.find('input').setValue('2022-12-01');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.dateVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Multiplechoice variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-multiplechoice-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-multiplechoice-variable.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      await scComponent.findAll('input')[1].setValue(true);
      await scComponent.findAll('input')[1].trigger('input');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.multiplechoiceVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Text variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-text-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-text-variable.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue('test');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.textVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Singlechoice variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-singlechoice-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-singlechoice-variable.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.singlechoiceVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Set variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-set-variable.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-set-variable.xml');
      expect(warningListComponent.findAll('.warning').length).toBe(1);
      await scComponent.findAll('button')[0].trigger('click');
      await scComponent.findAll('input')[0].setValue('test');
      expect(warningListComponent.findAll('.warning').length).toBe(2);
      expect(warningListComponent.findAll('.warning')[0].attributes('id')).toBe('wList-test_groupVariable.setVariable[0].textVariable_WARNING_myTestWarning2_CONFIRMED-wrapper');
      expect(warningListComponent.findAll('.warning')[1].attributes('id')).toBe('wList-test_groupVariable.setVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside DC Instance', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-dc-instance.xml');
      const warningListComponent = await mountWarningListComponent('/warnings/warning-in-dc-instance.xml');
      expect(warningListComponent.find('.warning').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue('test');
      await nextTick();
      expect(warningListComponent.find('.warning').isVisible()).toBe(true);
      expect(warningListComponent.find('.warning').attributes('id')).toBe('wList-test_groupVariable.dcInstance.WARNING_myTestWarning_CONFIRMED-wrapper');
    });
  });

  function mountWarningListComponent(xmlPath: string) {
    const xml = TestHelper.requireFile(xmlPath);
    return TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_WARNING_LIST, path.parse(xmlPath).name, '0.0.1', false, false, false, 99, undefined, '', Contexts.TEST, '', 'wList-');
  }
});

import { beforeAll, expect, describe, test, beforeEach } from 'vitest';
import TestHelper, { sleep, storageConfigurations } from '../../../utils/TestHelper';
import { mount } from '@vue/test-utils';
import { getContainer, getContext } from '@/ContainersConfig';
import FastCompilationComponentWrapper from '../../../componentsWrapper/FastCompilationComponentWrapper.vue';
import { InMemoryCaseStorage } from '@/storages/InMemoryCaseStorage';
import MockCRFService from '../../../mock-services/MockCRFService';
import { PartialStoredCase } from '@/entities/PartialStoredCase';
import { CrfStatus } from '@/services/CaseStatusService';
import { CaseStorageInterface } from '@/interfaces/CaseStorageInterface';
import { LocalCaseStorage } from '@/storages/LocalCaseStorage';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

describe.each(storageConfigurations)('Test for Fast Compilation, using $name', ({ name, bindings }) => {
  let caseStorage: CaseStorageInterface;
  beforeAll(() => {
    TestHelper.setupContainer('Admission', [], bindings);
    TestHelper.mockStatusService(Contexts.TEST);
    getContext(Contexts.TEST).container.bind(NamedBindings.CRFService).to(MockCRFService).inSingletonScope();
    caseStorage = getContainer(Contexts.TEST).get(NamedBindings.CaseStorage) as CaseStorageInterface;
  });

  beforeEach(() => {
    if (caseStorage instanceof InMemoryCaseStorage) {
      caseStorage.reset();
    } else if (caseStorage instanceof LocalCaseStorage) {
      window.localStorage.clear();
    }
  });

  const storageNameMap: Record<string, string> = { InMemoryStorages: 'inMemoryCaseStorage', LocalStorages: 'LocalCaseStorage', RESTStorages: 'RESTCaseStorage' };
  let storageName = storageNameMap[name] || 'UnknownStorage';
  const arrayOfPartials = [new PartialStoredCase('{}', 'public'), new PartialStoredCase('{}', 'public')];
  const caseMetaData = {
    caseID: '1', caseType: 'admission', centreCode: '', status: new Map<string, CrfStatus>().set('admission', new Map<string, number>()),
    creationDate: new Date(), lastUpdate: new Date(), frozenAccessLevels: new Map(), crfVersions: new Map().set('admission', '0.0.1')
  }

  test(`Fast compilation save correctly text variable, in ${storageName}`, async () => {
    if (new Date() > new Date('2025-12-20')) {
      await caseStorage.createCase([arrayOfPartials[0]], caseMetaData);
      const wrapper = mount(FastCompilationComponentWrapper);
      await sleep(0.1);
      expect(wrapper.html()).toContain('abc');
    }
  });
});

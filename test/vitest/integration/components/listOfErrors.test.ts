import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';
import path from 'path';

describe('ErrorsList errors', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Errors linked to variables', () => {
    test('Boolean variable with Error', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/boolean-variable-error.xml');
      const errorListComponent = await mountErrorListComponent('/errors/boolean-variable-error.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      scComponent.find('input').element.checked = true;
      await scComponent.find('input').trigger('input');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.ERROR_myTestError');
    });

    test('Color variable with Error', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/color-variable-error.xml');
      const errorListComponent = await mountErrorListComponent('/errors/color-variable-error.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.find('input').setValue('#00f900');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.ERROR_myTestError');
    });

    test('Date variable with Error', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/date-variable-error.xml');
      const errorListComponent = await mountErrorListComponent('/errors/date-variable-error.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.find('input').setValue('2022-12-01');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.ERROR_myTestError');
    });

    test('Multiplechoice variable with Error', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/multiplechoice-variable-error.xml');
      const errorListComponent = await mountErrorListComponent('/errors/multiplechoice-variable-error.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      await scComponent.findAll('input')[1].setValue(true);
      await scComponent.findAll('input')[1].trigger('input');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.ERROR_myTestError');
    });

    test('Text variable with Error', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/text-variable-error.xml');
      const errorListComponent = await mountErrorListComponent('/errors/text-variable-error.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue('test');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.ERROR_myTestError');
    });

    test('Singlechoice variable with Error', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/singlechoice-variable-error.xml');
      const errorListComponent = await mountErrorListComponent('/errors/singlechoice-variable-error.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.ERROR_myTestError');
    });

    test("Only one error should appear in the list even if the reference variable 'duplicates' it in scc", async () => {
      const errorListComponent = await mountErrorListComponent('/errors/reference-variable-error.xml');
      const error = errorListComponent.find('.error');
      expect(error.attributes('id')).toBe('eList-test_groupVariable.mainVariable_ERROR_myTestError');
    });
  });

  describe('Errors in variables', () => {
    test('Error inside boolean variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-boolean-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-boolean-variable.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      scComponent.find('input').element.checked = true;
      await scComponent.find('input').trigger('input');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.booleanVariable_ERROR_myTestError');
    });

    test('Error inside Color variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-color-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-color-variable.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.find('input').setValue('#00f900');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.colorVariable_ERROR_myTestError');
    });

    test('Error inside Date variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-date-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-date-variable.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.find('input').setValue('2022-12-01');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.dateVariable_ERROR_myTestError');
    });

    test('Error inside Multiplechoice variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-multiplechoice-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-multiplechoice-variable.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      await scComponent.findAll('input')[1].setValue(true);
      await scComponent.findAll('input')[1].trigger('input');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.multiplechoiceVariable_ERROR_myTestError');
    });

    test('Error inside Text variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-text-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-text-variable.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue('test');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.textVariable_ERROR_myTestError');
    });

    test('Error inside Singlechoice variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-singlechoice-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-singlechoice-variable.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue(true);
      await scComponent.findAll('input')[0].trigger('input');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.singlechoiceVariable_ERROR_myTestError');
    });

    test('Error inside Set variable', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-set-variable.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-set-variable.xml');
      expect(errorListComponent.findAll('.error').length).toBe(1);
      await scComponent.findAll('button')[0].trigger('click');
      await scComponent.findAll('input')[0].setValue('test');
      expect(errorListComponent.findAll('.error').length).toBe(2);
      expect(errorListComponent.findAll('.error')[0].attributes('id')).toBe('eList-test_groupVariable.setVariable[0].textVariable_ERROR_myTestError2');
      expect(errorListComponent.findAll('.error')[1].attributes('id')).toBe('eList-test_groupVariable.setVariable_ERROR_myTestError');
    });

    test('Error inside DC Instance', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/errors/error-in-dc-instance.xml');
      const errorListComponent = await mountErrorListComponent('/errors/error-in-dc-instance.xml');
      expect(errorListComponent.find('.error').exists()).toBe(false);
      await scComponent.findAll('input')[0].setValue('test');
      expect(errorListComponent.find('.error').isVisible()).toBe(true);
      expect(errorListComponent.find('.error').attributes('id')).toBe('eList-test_groupVariable.dcInstance.ERROR_myTestError');
    });
  });

  function mountErrorListComponent(xmlPath: string) {
    const xml = TestHelper.requireFile(xmlPath);
    return TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_ERROR_LIST, path.parse(xmlPath).name, '0.0.1', false, false, false, 99, undefined, '', Contexts.TEST, '', 'eList-');
  }
});

import path from 'path';
import { describe, expect, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';
import { MagicNumbers } from '@/types/MagicNumbers';
import { PathToXsl } from '@/types/PathToXsl';

describe('TreeComponent', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('TreeRenderHTML', () => {
    test('if xml contains dataclasses with variables datatype group, li should have an ul inside', async () => {
      const treeComponent = await mountTreeComponent('/tree/treeHTML/tree-dataclasses-groups-inside.xml');
      const liTags = treeComponent.findAll('li');
      let containsUl = false;
      liTags.forEach((liTag) => {
        const liFind = liTag.find('ul');
        if (liFind.html().includes('ul')) {
          containsUl = true;
        }
      });
      expect(containsUl).toBe(true);
    });

    test('all li tag should contain an ul tag', async () => {
      const treeComponent = await mountTreeComponent('/tree/treeHTML/tree-render-component.xml');
      const liTags = treeComponent.findAll('li');
      for (let i = 0; i < liTags.length; i++) {
        const liElement = liTags.at(i);
        const ulElement = liElement!.find('ul');
        expect(ulElement.exists()).toBe(true);
      }
      await nextTick();
      expect(liTags.length).toBe(5);
    });
  });

  describe('TreeRenderVisibility', async () => {
    test('dcInstance in tree should work correctly with visible', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/tree/treeVisibility/dc-visible.xml');
      const treeComponent = await mountTreeComponent('/tree/treeVisibility/dc-visible.xml');
      expect(treeComponent.element.firstElementChild!.outerHTML).toEqual('<ul class=""><!--v-if--></ul>');
      expect(treeComponent.element.children.length).toBe(1);
      const textInput = scComponent.find('#test_textVariable');
      await textInput.setValue('value');
      await textInput.trigger('input');
      await nextTick();
      const treeDC = treeComponent.find('#test_dcInstance');
      expect(treeDC.element.lastElementChild!.outerHTML).toEqual(
        '<ul class="visibleIfDC"><li class="groupToHide" id="test_dcInstance.groupToHide"><span>groupToHide</span><ul></ul></li></ul>');
    });

    test('Group in tree should work correctly with visible', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/tree/treeVisibility/group-visible.xml');
      const treeComponent = await mountTreeComponent('/tree/treeVisibility/group-visible.xml');
      expect(treeComponent.element.firstElementChild!.outerHTML).toEqual('<ul class=""><!--v-if--></ul>');
      expect(treeComponent.element.children.length).toBe(1);
      const textInput = scComponent.find('#test_textVariable');
      await textInput.setValue('value');
      await textInput.trigger('input');
      expect(treeComponent.element.firstElementChild!.outerHTML).toEqual(
        '<ul class=""><li class="groupToHide" id="test_groupToHide"><span>groupToHide</span><ul></ul></li></ul>');
    });

    test('Set in tree should work correctly with visible', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/tree/treeVisibility/set-visible.xml');
      const treeComponent = await mountTreeComponent('/tree/treeVisibility/set-visible.xml');
      expect(treeComponent.element.outerHTML).toEqual('<div data-v-app=""><!--v-if--></div>');
      expect(treeComponent.element.children.length).toBe(0);
      const textInput = scComponent.find('#test_textVariable');
      textInput.setValue('value');
      await textInput.trigger('input');
      expect(treeComponent.element.firstElementChild!.outerHTML).toEqual(
        '<ul class=""><!--v-if--><li class="visibleIfDC" id="test_setVariable"><span>setVariable</span><ul></ul></li></ul>'
      );
      const setButton = scComponent.find('button');
      await setButton.trigger('click');
      expect(treeComponent.element.firstElementChild!.querySelector('.visibleIfDC')!.outerHTML).toEqual(
        '<li class="visibleIfDC" id="test_setVariable"><span>setVariable</span><ul><li track-by="childElement.guid" class="visibleIfDC" id="test_setVariable"><!--v-if--><span>-- new setVariable --</span><!--v-if--><ul class="visibleIfDC"><li class="groupToHide" id="test_setVariable[0].groupToHide"><span>groupToHide</span><ul></ul></li></ul></li></ul></li>'
      );
    });

    test('Group in dcInstance in tree should work correctly with visible', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/tree/treeVisibility/group-in-dc-visible.xml');
      const treeComponent = await mountTreeComponent('/tree/treeVisibility/group-in-dc-visible.xml');
      const treeDC = treeComponent.find('#test_dcInstance');
      expect(treeDC.element.firstElementChild!.outerHTML.replace(/\s/g, '')).toEqual('<span>dcInstance</span>');
      expect(treeDC.element.lastElementChild!.outerHTML).toEqual('<ul class="visibleIfDC"><!--v-if--></ul>');
      const textInput = scComponent.find('#test_textVariable');
      await textInput.setValue('value');
      await textInput.trigger('input');
      expect(treeDC.element.lastElementChild!.outerHTML).toEqual(
        '<ul class="visibleIfDC"><li class="groupToHide" id="test_dcInstance.groupToHide"><span>groupToHide</span><ul></ul></li></ul>');
    });

    test('dcInstance in group in tree should work correctly with visible', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/tree/treeVisibility/dc-in-group-visible.xml');
      const treeComponent = await mountTreeComponent('/tree/treeVisibility/dc-in-group-visible.xml');
      const treeGroup = treeComponent.find('#test_groupVariable');
      expect(treeGroup.element.firstElementChild!.outerHTML.replace(/\s/g, '')).toEqual('<span>groupVariable</span>');
      expect(treeGroup.element.lastElementChild!.outerHTML).toEqual('<ul><!--v-if--></ul>');
      const textInput = scComponent.find('#test_textVariable');
      await textInput.setValue('value');
      await textInput.trigger('input');
      await nextTick();
      expect(treeGroup.element.lastElementChild!.outerHTML).toEqual(
        '<ul><li class="visibleIfDC" id="test_groupVariable.dcToHide"><span>dcToHide</span><!--v-if--><ul class="visibleIfDC"></ul></li></ul>');
    });

    test('Group in dcInstance in tree should work correctly with activateIf in scc', async () => {
      const scComponent = await TestHelper.mountStandardCollectionComponent('/tree/treeVisibility/group-in-dc-activateif.xml');
      const treeComponent = await mountTreeComponent('/tree/treeVisibility/group-in-dc-activateif.xml');
      const treeDC = treeComponent.find('#test_dcInstance');
      expect(treeDC.element.firstElementChild!.outerHTML.replace(/\s/g, '')).toEqual('<span>dcInstance</span>');
      expect(treeDC.element.children.length).toBe(1);
      const textInput = scComponent.find('#test_textVariable');
      await textInput.setValue('value');
      await textInput.trigger('input');
      expect(treeDC.element.lastElementChild!.outerHTML).toEqual(
        '<ul class="activateIfDC"><li class="groupToHide" id="test_dcInstance.groupToHide"><span>groupToHide</span><ul></ul></li></ul>');
      expect(treeDC.element.children.length).toBe(2);
    });

    test('Group in dcInstance in tree should work correctly with activateIf in new cases', async () => {
      const xml = TestHelper.requireFile('/tree/treeVisibility/group-in-dc-activateif.xml');
      const treeComponent = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_TREE, 'group-in-dc-activateif', '0.0.1', true, true, true,
        MagicNumbers.maxDepthForCRFComponentAndTree, { textVariable: 'v' });
      const treeComponent2 = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_TREE, 'group-in-dc-activateif', '0.0.1', true, true, true,
        MagicNumbers.maxDepthForCRFComponentAndTree, { textVariable: 'value' });
      await nextTick();
      //this simulates the creation of two cases with different models
      const treeDC = treeComponent.find('#test_dcInstance');
      expect(treeDC.element.firstElementChild!.outerHTML.replace(/\s/g, '')).toEqual('<span>dcInstance</span>');
      expect(treeDC.element.children.length).toBe(1);
      const treeDC2 = treeComponent2.find('#test_dcInstance');
      expect(treeDC2.element.lastElementChild!.outerHTML).toEqual(
        '<ul class="activateIfDC"><li class="groupToHide" id="test_dcInstance.groupToHide"><span>groupToHide</span><ul></ul></li></ul>');
      expect(treeDC2.element.children.length).toBe(2);
    });
  });

  function mountTreeComponent(xmlPath: string) {
    const xml = TestHelper.requireFile(xmlPath);
    return TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_TREE, path.parse(xmlPath).name, '0.0.1');
  }
});
import path from 'path';
import { describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

function mountMissingListComponent(xmlPath: string) {
  const xml = TestHelper.requireFile(xmlPath);
  return TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, path.parse(xmlPath).name, '0.0.1', false, false, false, 99, undefined, '', Contexts.TEST, '', 'mList-');
}

describe('MissingList missings', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Missings linked to variables', () => {
    //todo: all missings test have to be reimplemented correctly

    test('Missing Boolean variable', async () => {
      //     const xml = TestHelper.requireFile('/missings/boolean-variable-missing.xml');
      //     const scComponent = await TestHelper.mountComponentForTests(
      //       xml,
      //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
      //       'boolean-variable-missing',
      //       '0.0.1'
      //     );
      //     const missingListComponent = await mountMissingListComponent(xml, 'boolean-variable-missing');
      //     const missingVariable = missingListComponent.findAll('.requirement')[1];
      //     expect(missingVariable.exists()).toBe(true);
      //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.booleanVariable_MISSING');
      //     scComponent.find('input').element.checked = true;
      //     await scComponent.find('input').trigger('input');
      //     expect(missingVariable.isVisible()).toBe(false);
    });
    //
    //   test('Missing Color variable', async () => {
    //     const xml = TestHelper.requireFile('/missings/color-variable-missing.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'color-variable-missing',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'color-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[1];
    //     expect(missingVariable.exists()).toBe(true);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.colorVariable_MISSING');
    //     await scComponent.find('input').setValue('#00f900');
    //     expect(missingVariable.isVisible()).toBe(false);
    //   });
    //
    //   test('Missing Date variable', async () => {
    //     const xml = TestHelper.requireFile('/missings/date-variable-missing.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'date-variable-missing',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'date-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[1];
    //     expect(missingVariable.exists()).toBe(true);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.dateVariable_MISSING');
    //     await scComponent.find('input').setValue('2018-03-28');
    //     expect(missingVariable.isVisible()).toBe(false);
    //   });
    //
    //   test('Missing Multiplechoice variable', async () => {
    //     const xml = TestHelper.requireFile('/missings/multiplechoice-variable-missing.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'multiplechoice-variable-missing',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'multiplechoice-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[1];
    //     expect(missingVariable.exists()).toBe(true);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.multiplechoiceVariable_MISSING');
    //     await scComponent.findAll('input')[0].setValue(true);
    //     await scComponent.findAll('input')[0].trigger('input');
    //     expect(missingVariable.isVisible()).toBe(false);
    //   });
    //
    //   test('Missing Text variable', async () => {
    //     const xml = TestHelper.requireFile('/missings/text-variable-missing.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'text-variable-missing',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'text-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[1];
    //     expect(missingVariable.exists()).toBe(true);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.textVariable_MISSING');
    //     await scComponent.findAll('input')[0].setValue('test');
    //     expect(missingVariable.isVisible()).toBe(false);
    //   });
    //
    //   test('Missing Singlechoice variable', async () => {
    //     const xml = TestHelper.requireFile('/missings/singlechoice-variable-missing.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'singlechoice-variable-missing',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'singlechoice-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[1];
    //     expect(missingVariable.exists()).toBe(true);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.singlechoiceVariable_MISSING');
    //     await scComponent.findAll('input')[0].setValue(true);
    //     await scComponent.findAll('input')[0].trigger('input');
    //     expect(missingVariable.isVisible()).toBe(false);
    //   });
    //
    //   test('Missing variable inside DC Instance', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-dc-instance.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-dc-instance',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[2];
    //     expect(missingVariable.exists()).toBe(true);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.dcInstance.textVariable_MISSING');
    //     await scComponent.findAll('input')[0].setValue('test');
    //     expect(missingVariable.isVisible()).toBe(false);
    //   });
    //
    //   test('MissingList should be empty for Group variable and a variable with requiredForStatus="false"', async () => {
    //     const xml = TestHelper.requireFile('/missings/group-variable-missing.xml');
    //     const missingListComponent = await mountMissingListComponent(xml, 'group-variable-missing');
    //     const missingVariable = missingListComponent.findAll('.requirement')[0];
    //     expect(missingVariable.isVisible()).toBe(false);
    //     expect(missingVariable.attributes('id')).toBe('mList-test_groupVariable.MISSING');
    //     expect(
    //       missingListComponent.findAll('.requirement').find((x) => x.html().includes('notRequired'))
    //     ).toBeUndefined();
    //   });
    // });
    //
    // describe('Requirements in variables', () => {
    //   test('Requirement inside boolean variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-boolean-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-boolean-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-boolean-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(3);
    //     expect(
    //       requirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.booleanVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeDefined();
    //     await scComponent.find('input').setValue(true);
    //     await scComponent.find('input').trigger('input');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.booleanVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(2);
    //   });
    //
    //   test('Requirement inside Color variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-color-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-color-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-color-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(3);
    //     expect(
    //       requirements.find((x) => x.attributes('id') == 'mList-test_groupVariable.colorVariable_REQUIREMENT_myTestRequirement')
    //     ).toBeDefined();
    //     await scComponent.find('input').setValue('#00f900');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.colorVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(2);
    //   });
    //
    //   test('Requirement inside Date variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-date-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-date-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-date-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(3);
    //     expect(
    //       requirements.find((x) => x.attributes('id') == 'mList-test_groupVariable.dateVariable_REQUIREMENT_myTestRequirement')
    //     ).toBeDefined();
    //     await scComponent.find('input').setValue('2022-12-01');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.dateVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(2);
    //   });
    //
    //   test('Requirement inside Multiplechoice variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-multiplechoice-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-multiplechoice-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-multiplechoice-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(3);
    //     expect(
    //       requirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.multiplechoiceVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeDefined();
    //     await scComponent.findAll('input')[0].setValue(true);
    //     await scComponent.findAll('input')[0].trigger('input');
    //     await scComponent.findAll('input')[1].setValue(true);
    //     await scComponent.findAll('input')[1].trigger('input');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.multiplechoiceVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(2);
    //   });
    //
    //   test('Requirement inside Text variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-text-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-text-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-text-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(3);
    //     expect(
    //       requirements.find((x) => x.attributes('id') == 'mList-test_groupVariable.textVariable_REQUIREMENT_myTestRequirement')
    //     ).toBeDefined();
    //     await scComponent.find('input').setValue('test');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.textVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(2);
    //   });
    //
    //   test('Requirement inside Singlechoice variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-singlechoice-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-singlechoice-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-singlechoice-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(3);
    //     expect(
    //       requirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.singlechoiceVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeDefined();
    //     await scComponent.findAll('input')[0].setValue(true);
    //     await scComponent.findAll('input')[0].trigger('input');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.singlechoiceVariable_REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(2);
    //   });
    //
    //   test('Requirement inside Set variable', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-set-variable.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-set-variable',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-set-variable');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(2);
    //     expect(
    //       requirements.find((x) => x.attributes('id') == 'mList-test_groupVariable.setVariable_REQUIREMENT_myTestRequirement')
    //     ).toBeDefined();
    //     await scComponent.findAll('button')[0].trigger('click');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(updatedRequirements.length).toBe(4);
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.setVariable[0].textVariable_REQUIREMENT_myTestRequirement2'
    //       )
    //     ).toBeDefined();
    //     await scComponent.findAll('input')[0].setValue('test');
    //     await scComponent.findAll('input')[0].trigger('input');
    //     const requirementsAfterInput = missingListComponent.findAll('.requirement');
    //     expect(
    //       requirementsAfterInput.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.setVariable[0].textVariable_REQUIREMENT_myTestRequirement2'
    //       )
    //     ).toBeUndefined();
    //     expect(requirementsAfterInput.length).toBe(3);
    //   });
    //
    //   test('Requirement inside DC Instance', async () => {
    //     const xml = TestHelper.requireFile('/requirements/requirement-in-dc-instance.xml');
    //     const scComponent = await TestHelper.mountComponentForTests(
    //       xml,
    //       PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    //       'requirement-in-dc-instance',
    //       '0.0.1'
    //     );
    //     const missingListComponent = await mountMissingListComponent(xml, 'requirement-in-dc-instance');
    //     const requirements = missingListComponent.findAll('.requirement');
    //     expect(requirements.length).toBe(4);
    //     expect(
    //       requirements.find((x) => x.attributes('id') == 'mList-test_groupVariable.dcInstance.REQUIREMENT_myTestRequirement')
    //     ).toBeDefined();
    //     await scComponent.findAll('input')[0].setValue('test');
    //     await scComponent.findAll('input')[0].trigger('input');
    //     const updatedRequirements = missingListComponent.findAll('.requirement');
    //     expect(
    //       updatedRequirements.find(
    //         (x) => x.attributes('id') == 'mList-test_groupVariable.dcInstance.REQUIREMENT_myTestRequirement'
    //       )
    //     ).toBeUndefined();
    //     expect(updatedRequirements.length).toBe(3);
    //   });
  });
});

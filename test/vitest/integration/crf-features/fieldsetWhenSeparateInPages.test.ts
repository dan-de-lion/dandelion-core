import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { PathToXsl } from '@/types/PathToXsl';

describe('Separate In pages', () => {
  TestHelper.testSetupWithCaseOpening();

  test("Initially selected class shouldn't exist in buttons", async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/fieldset-when-separate-in-page.xml');
    const selectedNavButton = component.findAll('.selected');
    expect(selectedNavButton.length).toBe(0);
  });

  test('Find if class selected exists when I click on the first nav button in group', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/fieldset-when-separate-in-page.xml');
    const group = component.find('#test_dcInstance');
    const firstChildButton = group.find('nav').findAll('button')[0];
    const secondChildButton = group.find('nav').findAll('button')[1];
    await firstChildButton.trigger('click');
    expect(firstChildButton.attributes('class')).toContain('selected');
    expect(secondChildButton.attributes('class')).not.toContain('selected');
  });

  test('Find if class selected exists when I click on the second nav button in dataclasses', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/fieldset-when-separate-in-page.xml');
    const dcInstance2 = component.find('#test_dcInstance2');
    const firstChildButton = dcInstance2.find('nav').findAll('button')[0];
    const secondChildButton = dcInstance2.find('nav').findAll('button')[1];
    await secondChildButton.trigger('click');
    expect(firstChildButton.attributes('class')).not.toContain('selected');
    expect(secondChildButton.attributes('class')).toContain('selected');
  });

  test('The first nav button created after click on button "add" must contain class selected in sets', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/fieldset-when-separate-in-page.xml');
    const set = component.find('#test_setVariable');
    const buttonAdd = set.find('section').find('button');
    await buttonAdd.trigger('click');
    const firstChildNavButton = set.find('nav').find('button');
    expect(firstChildNavButton.attributes('class')).toContain('selected');
  });

  test('With two click on button add, the second nav button should have class selected in sets', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/page/fieldset-when-separate-in-page.xml');
    const set = component.find('#test_setVariable');
    const buttonAdd = set.find('section').find('button');
    await buttonAdd.trigger('click');
    await buttonAdd.trigger('click');
    const firstChildNavButton = set.find('nav').findAll('button')[0];
    const secondChildNavButton = set.find('nav').findAll('button')[1];
    expect(firstChildNavButton.attributes('class')).not.toContain('selected');
    expect(secondChildNavButton.attributes('class')).toContain('selected');
  });
});

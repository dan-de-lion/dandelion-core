import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('this Index', () => {
  TestHelper.testSetupWithCaseOpening();

  test('Standard thisIndex works correctly', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/standard-this-index.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('value');
  });

  test('Standard thisIndex works correctly when referring to previous item', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/standard-this-index-for-previous.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[1].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('');
    expect(component.vm.model.setVariable[1].copyTextVariable).toBe('value');
  });

  test('should use thisSet[thisIndex] in a set in another set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/set-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    const addButton2 = component.findAll('button')[0];
    await addButton2.trigger('click');
    expect(component.vm.model.setVariable[0].setVariable2[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].setVariable2[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].setVariable2[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].setVariable2[0].copyTextVariable).toBe('value');
  });

  test('should use thisSet[thisIndex] inside the Dataclass of itemsDataClass of variable set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/data-class-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('');
    expect(component.vm.model.setVariable[1].textVariable).toBe('');
    expect(component.vm.model.setVariable[1].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('value');
    expect(component.vm.model.setVariable[1].textVariable).toBe('');
    expect(component.vm.model.setVariable[1].copyTextVariable).toBe('');
  });

  test('should use correct thisSet[thisIndex] in a dataClass in a set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/correct-thisindex-dataclass-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    await addButton.trigger('click');
    const firstInputIdAttribute = component.find('input').attributes('id');
    const firstInputIdText = 'test_setVariable[0].dcInstance.textVariable';
    expect(firstInputIdAttribute).toBe(firstInputIdText);
  });

  test('should use thisSet[thisIndex] in groups in a set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/groups-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].groupVariable.textVariable).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable.copyTextVariable).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable2.textVariable2).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable2.copyTextVariable2).toBe('');
    await component.findAll('input')[0].setValue('value');
    await component.findAll('input')[2].setValue('secondValue');
    expect(component.vm.model.setVariable[0].groupVariable.textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].groupVariable.copyTextVariable).toBe('value');
    expect(component.vm.model.setVariable[0].groupVariable2.textVariable2).toBe('secondValue');
    expect(component.vm.model.setVariable[0].groupVariable2.copyTextVariable2).toBe('secondValue');
  });

  test('should use thisSet[thisIndex] in a set in a dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/set-in-dataClass.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.dcInstance.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.dcInstance.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.dcInstance.setVariable[0].copyTextVariable).toBe('value');
  });

  test('should use thisSet[thisIndex] in a set in groups', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisIndex/set-in-groups.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].copyTextVariable).toBe('value');
  });
});
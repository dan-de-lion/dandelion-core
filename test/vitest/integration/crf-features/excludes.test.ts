import { beforeAll, expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';

describe('Excludes', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });

  test('Excludes with variable name should work correctly', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-with-variable-name.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    await firstInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    await secondInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(true);
  });

  test('Excludes with name and * should work correctly', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-with-variable-name-and-star.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    const thirdInput = component.findAll('input')[2];
    await firstInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(false);
    await secondInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(true);
    expect(thirdInput.element.checked).toBe(false);
    await thirdInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(true);
  });

  test('Excludes with * should work correctly', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-with-star.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    const thirdInput = component.findAll('input')[2];
    await firstInput.trigger('input');
    await secondInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(true);
    expect(thirdInput.element.checked).toBe(false);
    await thirdInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(true);
  });

  test('Excludes with variable name should work correctly inside valuesGroup', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-in-valuesGroup-with-variable-name.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    await firstInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    await secondInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(true);
  });

  test('Excludes with name and * should work correctly inside ValuesGroup', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-in-valuesGroup-with-variable-name-and-star.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    const thirdInput = component.findAll('input')[2];
    await firstInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(false);
    await secondInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(true);
    expect(thirdInput.element.checked).toBe(false);
    await thirdInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(true);
  });

  test('Excludes with * should work correctly inside valuesGroup', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-in-valuesGroup-with-star.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    const thirdInput = component.findAll('input')[2];
    await firstInput.trigger('input');
    await secondInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(true);
    expect(thirdInput.element.checked).toBe(false);
    await thirdInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(true);
  });

  test('Excludes with name and * should work correctly inside different ValuesGroup', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-in-different-valuesGroup-with-variable-name-and-star.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    const thirdInput = component.findAll('input')[2];
    const fourthInput = component.findAll('input')[3];
    const fifthInput = component.findAll('input')[4];
    await firstInput.trigger('input');
    await thirdInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(true);
    expect(fourthInput.element.checked).toBe(false);
    expect(fifthInput.element.checked).toBe(false);
    await secondInput.trigger('input');
    await fourthInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(true);
    expect(thirdInput.element.checked).toBe(false);
    expect(fourthInput.element.checked).toBe(true);
    expect(fifthInput.element.checked).toBe(false);
    await firstInput.trigger('input');
    await thirdInput.trigger('input');
    await fourthInput.trigger('input');
    await fifthInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(false);
    expect(fourthInput.element.checked).toBe(false);
    expect(fifthInput.element.checked).toBe(true);
  });

  test('Excludes with * should work correctly inside different ValuesGroup with multiple value with the same name', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/excludes/excludes-with-multiple-value-with-same-name.xml');
    const firstInput = component.findAll('input')[0];
    const secondInput = component.findAll('input')[1];
    const thirdInput = component.findAll('input')[2];
    const fourthInput = component.findAll('input')[3];
    await firstInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(false);
    expect(fourthInput.element.checked).toBe(false);
    await secondInput.trigger('input');
    await thirdInput.trigger('input');
    await fourthInput.trigger('input');
    expect(firstInput.element.checked).toBe(false);
    expect(secondInput.element.checked).toBe(true);
    expect(thirdInput.element.checked).toBe(true);
    expect(fourthInput.element.checked).toBe(true);
    await firstInput.trigger('input');
    expect(firstInput.element.checked).toBe(true);
    expect(secondInput.element.checked).toBe(false);
    expect(thirdInput.element.checked).toBe(false);
    expect(fourthInput.element.checked).toBe(false);
  });
});

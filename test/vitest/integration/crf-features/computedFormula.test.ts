import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';

describe('computed formula', () => {
  TestHelper.testSetupWithCaseOpening();

  test('should disable input when computedFormula returns a value', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-with-values.xml');
    const numberInput = component.findAll('input')[0];
    const numberInput2 = component.findAll('input')[1];
    await numberInput.setValue(1);
    expect(component.vm.model.numberVariable2).toBe(1);
    expect(numberInput2.attributes().disabled).toBeDefined();
  });

  test('should keep the previous value in input when computedFormula returns "null"', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-has-null-value.xml');
    const textInput = component.findAll('input')[1];
    component.findAll('input')[0].element.checked = true;
    component.find('input').trigger('input');
    await nextTick();
    expect(textInput.attributes().disabled).toBeDefined();
    expect(component.vm.model.textVariable).toBe('hola');
    component.findAll('input')[0].element.checked = false;
    component.find('input').trigger('input');
    await nextTick();
    expect(textInput.attributes().disabled).toBeUndefined();
    expect(component.vm.model.textVariable).toBe('hola');
  });

  test('should override the previous value if computedFormula returns not "null"', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-has-null-value.xml');
    const booleanInput = component.findAll('input')[0];
    const textInput = component.findAll('input')[1];
    await textInput.setValue('ciao');
    expect(component.vm.model.textVariable).toBe('ciao');
    booleanInput.element.checked = true;
    booleanInput.trigger('input');
    await nextTick();
    expect(component.vm.model.textVariable).toBe('hola');
  });

  test('should show the Label when computedFormula returns a ValuesSetValue', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-with-valuesSet.xml');
    const secondRadio = component.find('#test_singlechoiceVariable-sets\\.second');
    secondRadio.trigger('click');
    secondRadio.trigger('input');
    await nextTick();
    const copyVariable = component.find('#test_copyVariable');
    expect(copyVariable.text()).toBe('Second');
  });

  // TODO: this is not clear
  test('should fail when radioButton doesnt get reanabled when boolean gets unchecked', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-with-radioButton-not-reanabled.xml');
    const radioInput = component.findAll('input[type="radio"]');
    component.findAll('input')[0].element.checked = true;
    component.findAll('input')[0].trigger('input');
    await nextTick();
    expect(radioInput[0].attributes().disabled).toBeDefined();
    expect(radioInput[1].attributes().disabled).toBeDefined();
    expect(radioInput[2].attributes().disabled).toBeDefined();
    expect(component.vm.model.singlechoiceVariable.toString()).toEqual('sets.second');
    await radioInput[0].trigger('input');
    expect(radioInput[0].attributes().disabled).toBeDefined();
    expect(radioInput[1].attributes().disabled).toBeDefined();
    expect(radioInput[2].attributes().disabled).toBeDefined();
    expect(component.vm.model.singlechoiceVariable.toString()).toEqual('sets.second');
    component.findAll('input')[0].element.checked = false;
    component.find('input').trigger('input');
    await nextTick();
    expect(radioInput[0].attributes().disabled).toBeUndefined();
    expect(radioInput[1].attributes().disabled).toBeUndefined();
    expect(radioInput[2].attributes().disabled).toBeUndefined();
    expect(component.vm.model.singlechoiceVariable.toString()).toEqual('sets.second');
    await radioInput[0].trigger('input');
    expect(component.vm.model.singlechoiceVariable.toString()).toEqual('sets.first');
  });

  test('should disable input of instances of dataclass when computedFormula returns a value', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-with-values-instances-dataclass.xml');
    const textInput = component.findAll('input')[0];
    const fieldset = component.findAll('fieldset')[0];
    await textInput.setValue('ciao');
    expect(component.vm.model.normalInput).toBe('ciao');
    expect(component.vm.model.dataclass.first).toBe('ciao');
    expect(fieldset.attributes().disabled).toBeDefined();
  });

  test('should disable input of group when computedFormula returns a value', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/computedFormula/computedFormula-with-values-group.xml');
    const textInput = component.findAll('input')[0];
    const fieldsetSecondGroup = component.findAll('fieldset')[1];
    await textInput.setValue('ciao');
    expect(component.vm.model.groupVariable.textVariable).toBe('ciao');
    expect(component.vm.model.groupVariable.textVariable).toBe('ciao');
    expect(fieldsetSecondGroup.attributes().disabled).toBeDefined();
  });
});
import { beforeAll, describe, expect, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';

describe('Separate in Page', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });

  test('Standard separate in page work correctly', async () => {
    await sameTestForDifferentXmls('/page/standard-separate-in-page.xml');
  });

  test('Standard dataclass separate in page work correctly', async () => {
    await sameTestForDifferentXmls('/page/standard-dataclass-separate-in-page.xml');
  });

  test('Separate in page with dataclass that have 3 group into', async () => {
    await sameTestForDifferentXmls('/page/separate-in-page-with-group.xml');
  });

  test('Separate in page with group that have 3 dataClass into', async () => {
    await sameTestForDifferentXmls('/page/separate-in-page-with-dataclass.xml');
  });

  test('Separate in page with group that have 2 dataClass and a group into inside a set', async () => {
    await sameTestForDifferentXmls('/page/separate-in-page-with-dataclass-in-set.xml', true);
  });

  async function sameTestForDifferentXmls(xmlPath: string, insideSet?: boolean) {
    const component = await TestHelper.mountStandardCollectionComponent(xmlPath);
    if (insideSet) {
      const setButton = component.find('button');
      await setButton.trigger('click');
    }
    const textInput = component.findAll('input')[0];
    const textInput2 = component.findAll('input')[1];
    const textInput3 = component.findAll('input')[2];
    const navButton = component.findAll('button')[0];
    const navButton2 = component.findAll('button')[1];
    const navButton3 = component.findAll('button')[2];
    expect(textInput.isVisible()).toBe(false);
    expect(textInput2.isVisible()).toBe(false);
    expect(textInput3.isVisible()).toBe(false);
    await navButton.trigger('click');
    expect(textInput.isVisible()).toBe(true);
    expect(textInput2.isVisible()).toBe(false);
    expect(textInput3.isVisible()).toBe(false);
    await navButton2.trigger('click');
    expect(textInput.isVisible()).toBe(false);
    expect(textInput2.isVisible()).toBe(true);
    expect(textInput3.isVisible()).toBe(false);
    await navButton3.trigger('click');
    expect(textInput.isVisible()).toBe(false);
    expect(textInput2.isVisible()).toBe(false);
    expect(textInput3.isVisible()).toBe(true);
  }
});
import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Reset button on computed Formula', () => {
  TestHelper.testSetupWithCaseOpening();

  test('if variable contains computedFormula reset button should be disabled', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/resetComputedFormula/reset-on-computed-formula.xml');
    const resetButton = component.findAll('button')[0];
    expect(resetButton.element.disabled).toBe(true);
  });
});
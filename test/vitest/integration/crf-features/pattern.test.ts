import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { CaseConfiguration, CoreConfiguration } from '@/interfaces/CoreConfiguration';
import { CRFToHTMLService } from '@/services/CRFToHTMLService';
import { PathToXsl } from '@/types/PathToXsl';

describe('Tooltips', () => {
  TestHelper.testSetupWithCaseOpening();

  test('Pattern patterns/patterns.xml should correctly work', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/patterns/patterns.xml');
    const patternInput = component.find('input');
    patternInput.element.value = '333';
    await patternInput.trigger('input');
    const patternError = component.find('.error');
    expect(patternError).toBeDefined();
    expect(patternError.text()).toEqual('Only alphabetical characters are allowed. Example: "HelloWorld"');
  });

  test('Pattern attribute should correctly work inside sets', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/patterns/patterns-in-set.xml');
    const setButton = component.find('button');
    await setButton.trigger('click');
    const patternInput = component.find('input');
    patternInput.element.value = '333';
    await patternInput.trigger('input');
    const patternError = component.find('.error');
    expect(patternError).toBeDefined();
    expect(patternError.text()).toEqual('Only alphabetical characters are allowed. Example: "HelloWorld"');
  });

  test('Should show pattern attribute in HTML', async () => {
    const xml = TestHelper.requireFile('/patterns/patterns.xml');
    const crfToHtmlService = new CRFToHTMLService(
      new CoreConfiguration({
        baseUrlForXslt: 'public/xslt/',
        debug: true,
        statisticianMode: false,
        listOfCaseTypes: [new CaseConfiguration(
          {
            caseName: 'fieldsetWhenSeparateInPage',
            referenceDateVariableName: '',
          })],
        defaultRequiredForStatus: { general: 3, errors: 2, warnings: 2 },
      })
    );
    const resultHTML = await crfToHtmlService.getHTMLfromXML(xml, 'patterns', '0.0.1', `/public/xslt/${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`);
    expect(resultHTML).toContain("checkValidity('textVariable', model, 'alphabetical'");
  });
});

import { beforeAll, beforeEach, describe, expect, test } from 'vitest';
import TestHelper from '../../../../utils/TestHelper';
import { UserServiceWithoutWriteRights } from '../../../../mock-services/UserServiceWithoutWriteRights';
import { UserServiceWithWriteRights } from '../../../../mock-services/UserServiceWithWriteRights';
import { getContext } from '@/ContainersConfig';
import CurrentCaseService from '@/services/CurrentCaseService';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

describe('canWrite', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    getContext(Contexts.TEST).container.bind(NamedBindings.UserService);
  });

  beforeEach(() => {
    getContext(Contexts.TEST).container.rebind(NamedBindings.CurrentCaseService).to(CurrentCaseService);
  })

  describe('UserServiceWithWriteRights', () => {
    beforeEach(() => {
      getContext(Contexts.TEST).container.rebind(NamedBindings.UserService).to(UserServiceWithWriteRights);
    })

    test('Should not set disable attribute on an inputAttributes when user have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/inputattribute/inputattribute-write-rights-and-computedformula-null.xml');
      const input = component.find('input');
      expect(input.element.getAttribute('disabled')).toBeNull();
    });

    test('Should set disable attribute on an inputAttributes when user have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/inputattribute/inputattribute-write-rights-and-computedformula-not-null.xml');
      const input = component.find('input');
      expect(input.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should not set disable attribute on an singlechoice when user have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoice/singlechoice-write-rights-and-computedformula-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).toBeNull();
      expect(input1.element.getAttribute('disabled')).toBeNull();
      expect(input2.element.getAttribute('disabled')).toBeNull();
    });

    test('Should set disable attribute on an singlechoice when user have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoice/singlechoice-write-rights-and-computedformula-not-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
      expect(input1.element.getAttribute('disabled')).not.toBeNull();
      expect(input2.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should not set disable attribute on an singlechoice dropdown when user have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoiceDropdown/singlechoice-dropdown-write-rights-and-computedformula-null.xml');
      const input0 = component.find('select');
      expect(input0.element.getAttribute('disabled')).toBeNull();
    });

    test('Should set disable attribute on an singlechoice dropdown when user have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoiceDropdown/singlechoice-dropdown-write-rights-and-computedformula-not-null.xml');
      const input0 = component.find('select');
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should not set disable attribute on an multiplechoice when user have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/multiplechoice/multiplechoice-write-rights-and-computedformula-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).toBeNull();
      expect(input1.element.getAttribute('disabled')).toBeNull();
      expect(input2.element.getAttribute('disabled')).toBeNull();
    });

    test('Should set disable attribute on an multiplechoice when user have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/multiplechoice/multiplechoice-write-rights-and-computedformula-not-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
      expect(input1.element.getAttribute('disabled')).not.toBeNull();
      expect(input2.element.getAttribute('disabled')).not.toBeNull();
    });
  })

  describe('UserServiceWithoutWriteRights', () => {
    beforeEach(() => {
      getContext(Contexts.TEST).container.rebind(NamedBindings.UserService).to(UserServiceWithoutWriteRights);
    })

    test('Should set disable attribute on an inputAttributes when user do not have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/inputattribute/inputattribute-not-write-rights-and-computedformula-null.xml');
      const input = component.find('input');
      expect(input.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an inputAttributes when user do not have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/inputattribute/inputattribute-not-write-rights-and-computedformula-not-null.xml');
      const input = component.find('input');
      expect(input.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an singlechoice when user do not have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoice/singlechoice-not-write-rights-and-computedformula-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
      expect(input1.element.getAttribute('disabled')).not.toBeNull();
      expect(input2.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an singlechoice when user do not have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoice/singlechoice-not-write-rights-and-computedformula-not-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
      expect(input1.element.getAttribute('disabled')).not.toBeNull();
      expect(input2.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an singlechoice dropdown when user do not have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoiceDropdown/singlechoice-dropdown-not-write-rights-and-computedformula-null.xml');
      const input0 = component.find('select');
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an singlechoice dropdown when user do not have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/singlechoiceDropdown/singlechoice-dropdown-not-write-rights-and-computedformula-not-null.xml');
      const input0 = component.find('select');
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an multiplechoice when user do not have write rights and computedFormula is null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/multiplechoice/multiplechoice-not-write-rights-and-computedformula-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
      expect(input1.element.getAttribute('disabled')).not.toBeNull();
      expect(input2.element.getAttribute('disabled')).not.toBeNull();
    });

    test('Should set disable attribute on an multiplechoice when user do not have write rights and computedFormula is not null', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/canWrite/disableAttribute/multiplechoice/multiplechoice-not-write-rights-and-computedformula-not-null.xml');
      const input0 = component.findAll('input')[0];
      const input1 = component.findAll('input')[1];
      const input2 = component.findAll('input')[2];
      expect(input0.element.getAttribute('disabled')).not.toBeNull();
      expect(input1.element.getAttribute('disabled')).not.toBeNull();
      expect(input2.element.getAttribute('disabled')).not.toBeNull();
    });
  })
});

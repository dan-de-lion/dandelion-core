import { beforeAll, beforeEach, describe, expect, test, vi } from 'vitest';
import { readonly, ref, type DeepReadonly, type Ref, type UnwrapNestedRefs } from 'vue';
import TestHelper from '../../../../utils/TestHelper';
import { getContainer, getContext } from '@/ContainersConfig';
import { User } from '@/entities/User';
import type { UserServiceInterface } from '@/interfaces/UserServiceInterface';
import CurrentCaseService from '@/services/CurrentCaseService';
import type { AccessRights } from '@/types/AccessRights';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';

let us: UserServiceInterface;
describe('canWrite and caRead are correctly called', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
    // need to rebind also the currentCase service since it uses the UserService
    getContext(Contexts.TEST).container.bind(NamedBindings.UserService);
  });

  beforeEach(() => {
    us = {
      canWrite: vi.fn().mockImplementation(() => true),
      canRead: vi.fn().mockImplementation(() => true),
      getCurrentUser(): DeepReadonly<UnwrapNestedRefs<Ref<User | null>>> {
        return readonly(ref(new User('', '', '')));
      },
      getAccessRights(): Map<string, AccessRights> {
        return new Map();
      },
      hasReadAccessRights(): boolean {
        return true;
      },
      hasWriteAccessRights(): boolean {
        return true;
      },
      getHeaders(): any { },
    };
    getContainer(Contexts.TEST).rebind(NamedBindings.UserService).toConstantValue(us);
    getContainer(Contexts.TEST).rebind(NamedBindings.CurrentCaseService).to(CurrentCaseService);
  });

  describe('input variable', () => {
    test('canWrite and canRead should be correctly called for input variables', async () => {
      await TestHelper.mountStandardCollectionComponent('/textVariable/standard-text-variable.xml');
      expect(us.canRead).toBeCalledWith('textVariable', 'standard-text-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('textVariable', 'standard-text-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for input variables in Groups', async () => {
      await TestHelper.mountStandardCollectionComponent('/textVariable/group-text-variable.xml');
      expect(us.canRead).toBeCalledWith('groupVariable.textVariable', 'group-text-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('groupVariable.textVariable', 'group-text-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for input variables with computedFormula', async () => {
      await TestHelper.mountStandardCollectionComponent('/textVariable/computedformula-text-variable.xml');
      expect(us.canRead).toBeCalledWith('textVariable', 'computedformula-text-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('textVariable', 'computedformula-text-variable', '0.0.1');
    });

    test('canRead should be correctly called even when the input variable has "visibleIf" attribute', async () => {
      await TestHelper.mountStandardCollectionComponent('/textVariable/not-visible-text-variable.xml');
      expect(us.canRead).toBeCalledWith('textVariable', 'not-visible-text-variable', '0.0.1');
    });
  });

  describe('single choice', () => {
    test('canWrite and canRead should be correctly called for singlechoice variables', async () => {
      await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      expect(us.canRead).toBeCalledWith('singlechoiceVariable', 'standard-singlechoice-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('singlechoiceVariable', 'standard-singlechoice-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for singlechoice variables in Groups', async () => {
      await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/group-singlechoice-variable.xml');
      expect(us.canRead).toBeCalledWith('groupVariable.singlechoiceVariable', 'group-singlechoice-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('groupVariable.singlechoiceVariable', 'group-singlechoice-variable', '0.0.1');
    });

    test('canRead should be correctly called even when the singlechoice variable has "visibleIf" attribute', async () => {
      await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/visible-singlechoice-variable.xml');
      expect(us.canRead).toBeCalledWith('singlechoiceVariable', 'visible-singlechoice-variable', '0.0.1');
    });
  });

  describe('multiple choice', () => {
    test('canWrite and canRead should be correctly called for multiplechoice variables', async () => {
      await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(us.canRead).toBeCalledWith('multiplechoiceVariable', 'standard-multiplechoice-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('multiplechoiceVariable', 'standard-multiplechoice-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for multiplechoice variables in Groups', async () => {
      await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/group-multiplechoice-variable.xml');
      expect(us.canRead).toBeCalledWith('groupVariable.multiplechoiceVariable', 'group-multiplechoice-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('groupVariable.multiplechoiceVariable', 'group-multiplechoice-variable', '0.0.1');
    });

    test('canRead should be correctly called even when the multiplechoice variable has "visibleIf" attribute', async () => {
      await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/visible-multiplechoice-variable.xml');
      expect(us.canRead).toBeCalledWith('multiplechoiceVariable', 'visible-multiplechoice-variable', '0.0.1');
    });
  });

  describe('single choice dropdown', () => {
    test('canWrite and canRead should be correctly called for singlechoice variables', async () => {
      await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/standard-singlechoice-dropdown-variable.xml');
      expect(us.canRead).toBeCalledWith('dropVariable', 'standard-singlechoice-dropdown-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('dropVariable', 'standard-singlechoice-dropdown-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for singlechoice variables in Groups', async () => {
      await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/group-singlechoice-dropdown-variable.xml');
      expect(us.canRead).toBeCalledWith('groupVariable.dropVariable', 'group-singlechoice-dropdown-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('groupVariable.dropVariable', 'group-singlechoice-dropdown-variable', '0.0.1');
    });

    test('canRead should be correctly called even when the singlechoice dropdown variable has "visibleIf" attribute', async () => {
      await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/visible-singlechoice-dropdown-variable.xml');
      expect(us.canRead).toBeCalledWith('dropVariable', 'visible-singlechoice-dropdown-variable', '0.0.1');
    });
  });

  describe('sets', () => {
    test('canWrite and canRead should be correctly called for set variables', async () => {
      await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      expect(us.canRead).toBeCalledWith('setVariable', 'standard-set-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('setVariable', 'standard-set-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for sets in Groups', async () => {
      await TestHelper.mountStandardCollectionComponent('/setVariable/group-set-variable.xml');
      expect(us.canRead).toBeCalledWith('groupVariable.setVariable', 'group-set-variable', '0.0.1');
      expect(us.canWrite).toBeCalledWith('groupVariable.setVariable', 'group-set-variable', '0.0.1');
    });

    test('canRead should be correctly called even when the set variable has "visibleIf" attribute', async () => {
      await TestHelper.mountStandardCollectionComponent('/setVariable/not-visible-set-variable.xml');
      expect(us.canRead).toBeCalledWith('setVariable', 'not-visible-set-variable', '0.0.1');
    });

    test('canWrite and canRead should be correctly called for set items variables', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      const addItem = component.findAll('button')[0];
      await addItem.trigger('click');
      expect(us.canRead).toHaveBeenCalledWith('setVariable[0].textVariable', 'standard-set-variable', '0.0.1');
      expect(us.canWrite).toHaveBeenCalledWith('setVariable[0].textVariable', 'standard-set-variable', '0.0.1');
    });
  });
});

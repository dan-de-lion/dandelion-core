import path from 'path';
import { beforeAll, expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Correct attribute for in labels', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });

  test('Attribute For should be equal to Input type text id ', async () => {
    const { inputId, labelFor } = await getInputAndLabel('/attributeForOfLabel/text-input.xml');
    expect(inputId).toBe(labelFor);
  });

  test('Attribute For should be equal to Input type number id ', async () => {
    const { inputId, labelFor } = await getInputAndLabel('/attributeForOfLabel/number-input.xml');
    expect(inputId).toBe(labelFor);
  });

  test('Attribute For should be equal to Input type date id ', async () => {
    const { inputId, labelFor } = await getInputAndLabel('/attributeForOfLabel/date-input.xml');
    expect(inputId).toBe(labelFor);
  });

  test('Attribute For should be equal to Input type date id ', async () => {
    const { inputId, labelFor } = await getInputAndLabel('/attributeForOfLabel/time-input.xml');
    expect(inputId).toBe(labelFor);
  });

  test('Attribute For should be equal to Input type single choice id ', async () => {
    const { inputId, labelFor } = await getInputAndLabel('/attributeForOfLabel/singlechoice-input.xml');
    expect(inputId).toBe(labelFor);
  });

  test('Attribute For should be equal to Input type multiple choice id ', async () => {
    const { inputId, labelFor } = await getInputAndLabel('/attributeForOfLabel/multiplechoice-input.xml');
    expect(inputId).toBe(labelFor);
  });

  async function getInputAndLabel(xmlPath: string) {
    const component = await TestHelper.mountStandardCollectionComponent(xmlPath);
    const inputId = component.find('input').attributes('id');
    const labelFor = component.find('label').attributes('for');
    return { inputId, labelFor };
  }
});

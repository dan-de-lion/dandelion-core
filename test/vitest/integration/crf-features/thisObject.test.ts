import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('This Object', () => {
  TestHelper.testSetupWithCaseOpening();

  test('this object should work correctly on first dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/standard-this-object.xml');
    expect(component.vm.model.textVariable).toBe('');
    expect(component.vm.model.copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.textVariable).toBe('value');
    expect(component.vm.model.copyTextVariable).toBe('value');
  });

  test('this object work currently in children dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/this-object-children-data-class.xml');
    expect(component.vm.model.dcInstance.textVariable).toBe('');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.textVariable).toBe('value');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('value');
  });

  test('should use thisObject in a children dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/this-object-in-children-dataClass.xml');
    expect(component.vm.model.dcInstance.dcInstance2.textVariable).toBe('');
    expect(component.vm.model.dcInstance.dcInstance2.copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.dcInstance2.textVariable).toBe('value');
    expect(component.vm.model.dcInstance.dcInstance2.copyTextVariable).toBe('value');
  });

  test('should use thisObject in two dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/this-object-in-two-dataClass.xml');
    expect(component.vm.model.dcInstance.textVariable).toBe('');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('');
    expect(component.vm.model.dcInstance2.textVariable2).toBe('');
    expect(component.vm.model.dcInstance2.copyTextVariable2).toBe('');
    await component.findAll('input')[0].setValue('value');
    await component.findAll('input')[2].setValue('value2');
    expect(component.vm.model.dcInstance.textVariable).toBe('value');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('value');
    expect(component.vm.model.dcInstance2.textVariable2).toBe('value2');
    expect(component.vm.model.dcInstance2.copyTextVariable2).toBe('value2');
  });

  test('should use thisObject in two copy variables in a dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/this-object-in-two-copy-variables-in-dataClass.xml');
    expect(component.vm.model.dcInstance.textVariable).toBe('');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('');
    expect(component.vm.model.dcInstance.copyTextVariable2).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.textVariable).toBe('value');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('value');
    expect(component.vm.model.dcInstance.copyTextVariable2).toBe('value');
  });

  test('should use thisObject in set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/this-object-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].textVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('value');
  });

  test('should use thisObject in a set in another set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/set-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    const addButton2 = component.findAll('button')[0];
    await addButton2.trigger('click');
    expect(component.vm.model.setVariable[0].setVariable2[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].setVariable2[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].setVariable2[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].setVariable2[0].copyTextVariable).toBe('value');
  });

  test('should use thisObject in a dataClass in a set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/data-class-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('value');
  });

  test('should use thisObject in groups in a set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/groups-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].groupVariable.textVariable).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable.copyTextVariable).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable2.textVariable2).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable2.copyTextVariable2).toBe('');
    await component.findAll('input')[0].setValue('value');
    await component.findAll('input')[2].setValue('secondValue');
    expect(component.vm.model.setVariable[0].groupVariable.textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].groupVariable.copyTextVariable).toBe('value');
    expect(component.vm.model.setVariable[0].groupVariable2.textVariable2).toBe('secondValue');
    expect(component.vm.model.setVariable[0].groupVariable2.copyTextVariable2).toBe('secondValue');
  });

  test('should use thisObject in a set in a dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/set-in-data-class.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.dcInstance.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.dcInstance.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.dcInstance.setVariable[0].copyTextVariable).toBe('value');
  });

  test('should use thisObject in a dataClass in another dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/data-class-in-data-class.xml');
    expect(component.vm.model.dcInstance.textVariable).toBe('');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.textVariable).toBe('value');
    expect(component.vm.model.dcInstance.copyTextVariable).toBe('value');
  });

  test('should use thisObject in groups in a dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/groups-in-data-class.xml');
    expect(component.vm.model.dcInstance.groupVariable.textVariable).toBe('');
    expect(component.vm.model.dcInstance.groupVariable.copyTextVariable).toBe('');
    expect(component.vm.model.dcInstance.groupVariable2.textVariable2).toBe('');
    expect(component.vm.model.dcInstance.groupVariable2.copyTextVariable2).toBe('');
    await component.findAll('input')[0].setValue('value');
    await component.findAll('input')[2].setValue('secondValue');
    expect(component.vm.model.dcInstance.groupVariable.textVariable).toBe('value');
    expect(component.vm.model.dcInstance.groupVariable.copyTextVariable).toBe('value');
    expect(component.vm.model.dcInstance.groupVariable2.textVariable2).toBe('secondValue');
    expect(component.vm.model.dcInstance.groupVariable2.copyTextVariable2).toBe('secondValue');
  });

  test('should use thisObject in a set in a group', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/set-in-group.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].copyTextVariable).toBe('value');
  });

  test('should use thisObject in a dataClass in a group', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisObject/data-class-in-group.xml');
    expect(component.vm.model.groupVariable.dcInstance.textVariable).toBe('');
    expect(component.vm.model.groupVariable.dcInstance.copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.groupVariable.dcInstance.textVariable).toBe('value');
    expect(component.vm.model.groupVariable.dcInstance.copyTextVariable).toBe('value');
  });
});

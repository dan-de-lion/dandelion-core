import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('This Set', () => {
  TestHelper.testSetupWithCaseOpening();

  test('should use thisSet in a set in another set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisSet/set-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    const addButton2 = component.findAll('button')[0];
    await addButton2.trigger('click');
    expect(component.vm.model.setVariable[0].setVariable2[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].setVariable2[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].setVariable2[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].setVariable2[0].copyTextVariable).toBe('value');
  });

  test('should use thisSet in a dataClass in a set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisSet/data-class-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('');
    expect(component.vm.model.setVariable[1].textVariable).toBe('');
    expect(component.vm.model.setVariable[1].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].copyTextVariable).toBe('value');
    expect(component.vm.model.setVariable[1].textVariable).toBe('');
    expect(component.vm.model.setVariable[1].copyTextVariable).toBe('value');
  });

  test('should use thisSet in groups in a set', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisSet/groups-in-set.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.setVariable[0].groupVariable.textVariable).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable.copyTextVariable).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable2.textVariable2).toBe('');
    expect(component.vm.model.setVariable[0].groupVariable2.copyTextVariable2).toBe('');
    await component.findAll('input')[0].setValue('value');
    await component.findAll('input')[2].setValue('secondValue');
    expect(component.vm.model.setVariable[0].groupVariable.textVariable).toBe('value');
    expect(component.vm.model.setVariable[0].groupVariable.copyTextVariable).toBe('value');
    expect(component.vm.model.setVariable[0].groupVariable2.textVariable2).toBe('secondValue');
    expect(component.vm.model.setVariable[0].groupVariable2.copyTextVariable2).toBe('secondValue');
  });

  test('should use thisSet in a set in a dataClass', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisSet/set-in-dataClass.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.dcInstance.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.dcInstance.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.dcInstance.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.dcInstance.setVariable[0].copyTextVariable).toBe('value');
  });

  test('should use thisSet in a set in groups', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/thisSet/set-in-groups.xml');
    const addButton = component.findAll('button')[0];
    await addButton.trigger('click');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].textVariable).toBe('');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].copyTextVariable).toBe('');
    await component.findAll('input')[0].setValue('value');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].textVariable).toBe('value');
    expect(component.vm.model.groupVariable.dcInstance.setVariable[0].copyTextVariable).toBe('value');
  });
});

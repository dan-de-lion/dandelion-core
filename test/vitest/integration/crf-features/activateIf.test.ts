import { expect, describe, test, beforeAll } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { getContainer } from "@/ContainersConfig";
import type { CaseStatusServiceInterface } from '@/interfaces/CaseStatusServiceInterface';
import { Contexts } from '@/types/Contexts';
import { NamedBindings } from '@/types/NamedBindings';
import { CrfServiceFromFolderForTests } from '../../../mock-services/CrfServiceFromFolderForTests';

describe('activateIf and howToActivate', () => {
  TestHelper.testSetupWithCaseOpening(true);

  beforeAll(() => {
    const crfService = getContainer(Contexts.TEST).get(NamedBindings.CRFService) as CrfServiceFromFolderForTests;
    crfService.getAccessLevelForVariable = () => "public";
  })

  test('When activateIf is true, test that the dataClass is visible', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/activateIf/main-dc-activate-if-true.xml');
    const inputText = component.findAll('input')[0];
    expect(inputText).toBeDefined();
  });

  test('When activateIf is false, test that the dataClass is not visible and the message in howToActivate is visible', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/activateIf/main-dc-activate-if-false.xml');
    const inputText = component.findAll('input')[0];
    const p = component.find('p');
    const message = p.text();
    expect(inputText).toBeUndefined();
    expect(message).toBe('DC Not Active');
  });

  test('When activateIf is false, test that the model is empty', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/activateIf/main-dc-activate-if-false.xml');
    expect(component.vm.model).toStrictEqual({ Admission: {}, ACTIVE: false });
  });

  test('When activateIf is false, test that the model is empty', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/activateIf/dc-instance-activate-if-false.xml');
    expect(component.vm.model).toStrictEqual({ Admission: {}, dcInstance: { ACTIVE: false } });
  });

  test('When activateIf is false, test that the status is 0', async () => {
    await TestHelper.mountStandardCollectionComponent('/activateIf/main-dc-activate-if-false.xml');
    const caseStatusService: CaseStatusServiceInterface = getContainer(Contexts.TEST).get(NamedBindings.CaseStatusService) as CaseStatusServiceInterface;
    expect(caseStatusService.getStatus().value).toBe(0);
  });

  test('When activateIf is true, test that the model is correct', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/activateIf/main-dc-activate-if-true.xml');
    expect(component.vm.model).toStrictEqual({ Admission: {}, textVariable: '' });
  });

  test('When activateIf is true, test that the model is correct', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/activateIf/dc-instance-activate-if-true.xml');
    expect(component.vm.model).toStrictEqual({ Admission: {}, dcInstance: { textVariable: '' } });
  });

  test('When activateIf is true, test that the status is 2', async () => {
    await TestHelper.mountStandardCollectionComponent('/activateIf/main-dc-activate-if-true.xml');
    const caseStatusService: CaseStatusServiceInterface = getContainer(Contexts.TEST).get(NamedBindings.CaseStatusService) as CaseStatusServiceInterface;
    expect(caseStatusService.getStatus().value).toBe(2);
  });
});

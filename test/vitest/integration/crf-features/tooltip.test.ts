import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';

describe('Tooltips', () => {
  TestHelper.testSetupWithCaseOpening();

  test('Tooltips should correctly work for text variables', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/tooltip/tooltip-text-variable.xml');
    const tooltip = component.findAll('span')[0];
    const tooltipContent = tooltip.attributes('tooltip');
    expect(tooltip).toBeDefined();
    expect(tooltip.text()).toBe('?');
    expect(tooltipContent).toBe('Tooltip for text');
  });

  test('Tooltips should correctly work for singlechoice variables', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/tooltip/tooltip-singlechoice-variable.xml');
    const radioDescription = component.findAll('span')[0];
    const tooltip = radioDescription.findAll('span')[0];
    const tooltipContent = tooltip.attributes('tooltip');
    expect(tooltip).toBeDefined();
    expect(tooltip.text()).toBe('?');
    expect(tooltipContent).toBe('Tooltip for singlechoice');
  });

  test('Tooltips should correctly work for multiplechoice variables', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/tooltip/tooltip-multiplechoice-variable.xml');
    const checkBoxDescription = component.findAll('span')[0];
    const tooltip = checkBoxDescription.findAll('span')[0];
    const tooltipContent = tooltip.attributes('tooltip');
    expect(tooltip).toBeDefined();
    expect(tooltip.text()).toBe('?');
    expect(tooltipContent).toBe('Tooltip for multiplechoice');
  });

  test('Tooltips should correctly work for group variables', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/tooltip/tooltip-group-variable.xml');
    const groupTooltip = component.findAll('span')[0];
    const groupTooltipContent = groupTooltip.attributes('tooltip');
    const textTooltip = component.findAll('span')[2];
    const textTooltipContent = textTooltip.attributes('tooltip');
    expect(groupTooltip).toBeDefined();
    expect(groupTooltip.text()).toBe('?');
    expect(groupTooltipContent).toBe('Tooltip for group');
    expect(textTooltip).toBeDefined();
    expect(textTooltip.text()).toBe('?');
    expect(textTooltipContent).toBe('Tooltip for text');
  });

  test('Tooltips should correctly work for set variables', async () => {
    const component = await TestHelper.mountStandardCollectionComponent('/tooltip/tooltip-set-variable.xml');
    const setTooltip = component.findAll('span')[0];
    const setTooltipContent = setTooltip.attributes('tooltip');
    const setButton = component.find('button');
    await setButton.trigger('click');
    await nextTick();
    const textTooltip = component.findAll('span')[2];
    const textTooltipContent = textTooltip.attributes('tooltip');
    expect(setTooltip).toBeDefined();
    expect(setTooltip.text()).toBe('?');
    expect(setTooltipContent).toBe('Tooltip for set');
    expect(textTooltip).toBeDefined();
    expect(textTooltip.text()).toBe('?');
    expect(textTooltipContent).toBe('Tooltip for text');
  });
});
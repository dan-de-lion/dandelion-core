import { describe, test, expect, beforeAll } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('External helper functions', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });
  
  test('should work when used as computedFormulas', async () => {
    const xml = TestHelper.requireFile('/externalHelperFunctions/external-helper-in-computed-formula.xml');
    const helperContent = TestHelper.requireFile('/externalHelperFunctions/helpers.js');
    const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
      'external-helper-in-computed-formula', '0.0.1', false, false, false, 3, {}, helperContent);
    await component.get('input').trigger('input');
    expect(component.vm.model.textVariable).toBe('test');
  });
});

import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';

describe('Test on the Custom Model Directive', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('singlechoice', () => {
    test('should update view of a singlechoice based on model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/customDirectives/customModelDirective/customModelDirective-with-singlechoice-visibility.xml');
      expect(component.vm.model.singlechoiceVariable).toBe('');
      component.vm.model.singlechoiceVariable = 'sets.first';
      await nextTick();
      expect(component.findAll('input')[1].element.checked).toBeTruthy();
      expect(component.findAll('input')[2].element.checked).toBeFalsy();
      expect(component.findAll('input')[3].element.checked).toBeFalsy();
    });

    test('should update view of a singlechoice based on model after visibility = false', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/customDirectives/customModelDirective/customModelDirective-with-singlechoice-visibility.xml');
      expect(component.vm.model.singlechoiceVariable).toBe('');
      component.vm.model.textVariable = 'pippo e pluto';
      await nextTick();
      expect(component.findAll('input').length).toBeLessThan(2);
      component.vm.model.textVariable = '';
      await nextTick();
      component.vm.model.singlechoiceVariable = 'sets.second';
      await nextTick();
      expect(component.findAll('input')[1].element.checked).toBeFalsy();
      expect(component.findAll('input')[2].element.checked).toBeTruthy();
      expect(component.findAll('input')[3].element.checked).toBeFalsy();
    });
  });

  describe('checkboxes', () => {
    test('should update view of a checkboxes based on model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/customDirectives/customModelDirective/customModelDirective-with-checkboxes-visibility.xml');
      expect(component.vm.model.multiplechoiceVariable).toStrictEqual([]);
      component.vm.model.multiplechoiceVariable = ['sets.first'];
      await nextTick();
      expect(component.findAll('input')[1].element.checked).toBeTruthy();
      expect(component.findAll('input')[2].element.checked).toBeFalsy();
      expect(component.findAll('input')[3].element.checked).toBeFalsy();
    });

    test('should update view of a checkboxes based on model after visibility = false', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/customDirectives/customModelDirective/customModelDirective-with-checkboxes-visibility.xml');
      expect(component.vm.model.multiplechoiceVariable).toStrictEqual([]);
      component.vm.model.textVariable = 'pippo e pluto';
      await nextTick();
      expect(component.findAll('input').length).toBeLessThan(2);
      component.vm.model.textVariable = '';
      await nextTick();
      component.vm.model.multiplechoiceVariable = ['sets.second'];
      await nextTick();
      expect(component.findAll('input')[1].element.checked).toBeFalsy();
      expect(component.findAll('input')[2].element.checked).toBeTruthy();
      expect(component.findAll('input')[3].element.checked).toBeFalsy();
    });
  });
});

import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('SingleCase errors', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Errors linked to variables', () => {
    test('Boolean variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/boolean-variable-error.xml');
      expect(component.find('.error').exists()).toBe(false);
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Color variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/color-variable-error.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.find('input').setValue('#00f900');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Date variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/date-variable-error.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.find('input').setValue('2022-12-01');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Multiplechoice variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/multiplechoice-variable-error.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Text variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/text-variable-error.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Singlechoice variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/singlechoice-variable-error.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Set variable with Error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/set-variable-error.xml');
      expect(component.findAll('.error').length).toBe(1);
      await component.findAll('button')[0].trigger('click');
      await component.findAll('input')[0].setValue('test');
      expect(component.findAll('.error').length).toBe(2);
      expect(component.findAll('.error')[0].attributes('id')).toBe('test_groupVariable.setVariable[0].ERROR_myTestError2');
      expect(component.findAll('.error')[1].attributes('id')).toBe('test_groupVariable.ERROR_myTestError');
    });

    test('Reference variable with Error should have different ID from others errors', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/reference-variable-error.xml');
      const firstError = component.findAll('.error')[0];
      const referenceError = component.findAll('.error')[1];
      expect(firstError.attributes('id')).not.toEqual(referenceError.attributes('id'));
      expect(firstError.attributes('id')).toBe('test_groupVariable.mainVariable_ERROR_myTestError');
      expect(referenceError.attributes('id')).toBe('test_groupVariable.refVariable.mainVariable_ERROR_myTestError');
    });

    test('Error with new lines (e.g. created by formatting) should not break component', async () => {
      await TestHelper.mountStandardCollectionComponent('/errors/error-with-text-on-newline.xml');
      // No expect because the test wants to verify if the mount will throw Syntex error
      // If throw, the test will fail, otherwise it will pass
    });
  });

  describe('Errors in variables', () => {
    test('Error inside boolean variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-boolean-variable.xml');
      expect(component.find('.error').exists()).toBe(false);
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.booleanVariable_ERROR_myTestError');
    });

    test('Error inside Color variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-color-variable.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.find('input').setValue('#00f900');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.colorVariable_ERROR_myTestError');
    });

    test('Error inside Date variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-date-variable.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.find('input').setValue('2022-12-01');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.dateVariable_ERROR_myTestError');
    });

    test('Error inside Multiplechoice variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-multiplechoice-variable.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.multiplechoiceVariable_ERROR_myTestError');
    });

    test('Error inside Text variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-text-variable.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.textVariable_ERROR_myTestError');
    });

    test('Error inside Singlechoice variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-singlechoice-variable.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.singlechoiceVariable_ERROR_myTestError');
    });

    test('Error inside Set variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-set-variable.xml');
      expect(component.findAll('.error').length).toBe(1);
      await component.findAll('button')[0].trigger('click');
      await component.findAll('input')[0].setValue('test');
      expect(component.findAll('.error').length).toBe(2);
      expect(component.findAll('.error')[0].attributes('id')).toBe('test_groupVariable.setVariable[0].textVariable_ERROR_myTestError2');
      expect(component.findAll('.error')[1].attributes('id')).toBe('test_groupVariable.setVariable_ERROR_myTestError');
    });

    test('Error inside DC Instance', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/error-in-dc-instance.xml');
      expect(component.find('.error').exists()).toBe(false);
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.error').isVisible()).toBe(true);
      expect(component.find('.error').attributes('id')).toBe('test_groupVariable.dcInstance.ERROR_myTestError');
    });
  });

  describe('Syntax errors', () => {
    test('Min error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/min-error.xml');
      const numberVariable = component.find('input');
      await numberVariable.setValue(2);
      await numberVariable.trigger('input');
      const minError = component.find('.error');
      expect(minError.exists()).toBe(true);
      expect(minError.attributes('id')).toBe('test_numberVariable_ERROR_MIN');
      expect(minError.find('span').text()).toBe('The value should be equal or higher than 5');
    });

    test('Max error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/max-error.xml');
      const numberVariable = component.find('input');
      await numberVariable.setValue(10);
      await numberVariable.trigger('input');
      const maxError = component.find('.error');
      expect(maxError.exists()).toBe(true);
      expect(maxError.attributes('id')).toBe('test_numberVariable_ERROR_MAX');
      expect(maxError.find('span').text()).toBe('The value should be equal or lower than 5');
    });

    test('Pattern error', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/errors/pattern-error.xml');
      const textVariable = component.find('input');
      await textVariable.setValue(10);
      await textVariable.trigger('input');
      const patternError = component.find('.error');
      expect(patternError.exists()).toBe(true);
      expect(patternError.attributes('id')).toBe('test_textVariable_ERROR_PATTERN');
      expect(patternError.find('span').text()).toBe('Only alphabetical characters are allowed. Example: "HelloWorld"');
    });
  });
});

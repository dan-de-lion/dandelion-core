import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('SingleCase warnings', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Warnings linked to variables', () => {
    test('Boolean Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/boolean-variable-warning.xml');
      expect(component.find('.warning').exists()).toBe(false);
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Color Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/color-variable-warning.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.find('input').setValue('#00f900');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Date Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/date-variable-warning.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.find('input').setValue('2018-03-28');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Multiplechoice Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/multiplechoice-variable-warning.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Text Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/text-variable-warning.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Singlechoice Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/singlechoice-variable-warning.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Set Variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/set-variable-warning.xml');
      expect(component.findAll('.warning').length).toBe(1);
      await component.findAll('button')[0].trigger('click');
      await component.findAll('input')[0].setValue('test');
      expect(component.findAll('.warning').length).toBe(2);
      expect(component.findAll('.warning')[0].attributes('id')).toBe('test_groupVariable.setVariable[0].WARNING_myTestWarning2_CONFIRMED-wrapper');
      expect(component.findAll('.warning')[1].attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Reference variable referenced to a variable with warning', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/reference-variable-warning.xml');
      const firstWarning = component.findAll('.warning')[0];
      const referenceWarning = component.findAll('.warning')[1];
      expect(firstWarning.attributes('id')).not.toEqual(referenceWarning.attributes('id'));
      expect(firstWarning.attributes('id')).toBe('test_groupVariable.mainVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
      expect(referenceWarning.attributes('id')).toBe('test_groupVariable.refVariable.mainVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning should change class when is clicked', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/boolean-variable-warning.xml');
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      let warning = component.find('.warning');
      let solvedWarning = component.find('.solvedWarning');
      expect(warning.exists()).toBe(true);
      expect(solvedWarning.exists()).toBe(false);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
      warning.find('input').element.checked = true;
      await warning.find('input').trigger('input');
      warning = component.find('.warning');
      solvedWarning = component.find('.solvedWarning');
      expect(warning.exists()).toBe(false);
      expect(solvedWarning.exists()).toBe(true);
      expect(component.find('.solvedWarning').attributes('id')).toBe('test_groupVariable.WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning with new lines (e.g. created by formatting) should not break component', async () => {
      await TestHelper.mountStandardCollectionComponent('/warnings/warning-with-text-on-newline.xml');
      // No expect because the test wants to verify if the mount will throw Syntex error
      // If throw, the test will fail, otherwise it will pass
    });
  });

  describe('Warnings in variables', () => {
    test('Warning inside boolean variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-boolean-variable.xml');
      expect(component.find('.warning').exists()).toBe(false);
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.booleanVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Color variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-color-variable.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.find('input').setValue('#00f900');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.colorVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Date variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-date-variable.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.find('input').setValue('2022-12-01');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.dateVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Multiplechoice variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-multiplechoice-variable.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.multiplechoiceVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Text variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-text-variable.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.textVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Singlechoice variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-singlechoice-variable.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.singlechoiceVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside Set variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-set-variable.xml');
      expect(component.findAll('.warning').length).toBe(1);
      await component.findAll('button')[0].trigger('click');
      await component.findAll('input')[0].setValue('test');
      expect(component.findAll('.warning').length).toBe(2);
      expect(component.findAll('.warning')[0].attributes('id')).toBe('test_groupVariable.setVariable[0].textVariable_WARNING_myTestWarning2_CONFIRMED-wrapper');
      expect(component.findAll('.warning')[1].attributes('id')).toBe('test_groupVariable.setVariable_WARNING_myTestWarning_CONFIRMED-wrapper');
    });

    test('Warning inside DC Instance', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/warnings/warning-in-dc-instance.xml');
      expect(component.find('.warning').exists()).toBe(false);
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.warning').isVisible()).toBe(true);
      expect(component.find('.warning').attributes('id')).toBe('test_groupVariable.dcInstance.WARNING_myTestWarning_CONFIRMED-wrapper');
    });
  });
});

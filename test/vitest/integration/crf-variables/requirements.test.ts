import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('SingleCase requirements', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Requirements linked to variables', () => {
    test('Boolean Variable with requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/boolean-variable-requirement.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Color Variable with requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/color-variable-requirement.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      await component.find('input').setValue('#00f900');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Date Variable with requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/date-variable-requirement.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      await component.find('input').setValue('2125-12-01');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Multiplechoice Variable with requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/multiplechoice-variable-requirement.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Text Variable with requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/text-variable-requirement.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Singlechoice Variable with requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/singlechoice-variable-requirement.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Set variable with Requirement', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/set-variable-requirement.xml');
      expect(component.findAll('.requirement').length).toBe(1);
      expect(component.findAll('.requirement')[0].attributes('id')).toBe('test_groupVariable.REQUIREMENT_myTestRequirement');
      await component.findAll('button')[0].trigger('click');
      expect(component.findAll('.requirement').length).toBe(2);
      expect(component.findAll('.requirement')[0].attributes('id')).toBe('test_groupVariable.setVariable[0].REQUIREMENT_myTestRequirement2');
      await component.findAll('input')[0].setValue('test');
      expect(component.findAll('.requirement').length).toBe(1);
    });

    test('Reference variable with Requirement should have different ID from others requirements', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/reference-variable-requirement.xml');
      const firstRequirement = component.findAll('.requirement')[0];
      const referenceRequirement = component.findAll('.requirement')[1];
      expect(firstRequirement.attributes('id')).not.toEqual(referenceRequirement.attributes('id'));
      expect(firstRequirement.attributes('id')).toBe('test_groupVariable.mainVariable_REQUIREMENT_myTestRequirement');
      expect(referenceRequirement.attributes('id')).toBe('test_groupVariable.refVariable.mainVariable_REQUIREMENT_myTestRequirement');
    });

    test('Requirement with new lines (e.g. created by formatting) should not break component', async () => {
      await TestHelper.mountStandardCollectionComponent('/requirements/requirement-with-text-on-newline.xml');
      // No expect because the test wants to verify if the mount will throw Syntex requirement
      // If throw, the test will fail, otherwise it will pass
    });
  });

  describe('Requirements in variables', () => {
    test('Requirement inside boolean variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-boolean-variable.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.booleanVariable_REQUIREMENT_myTestRequirement');
      component.find('input').element.checked = true;
      await component.find('input').trigger('input');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Requirement inside Color variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-color-variable.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.colorVariable_REQUIREMENT_myTestRequirement');
      await component.find('input').setValue('#00f900');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Requirement inside Date variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-date-variable.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.dateVariable_REQUIREMENT_myTestRequirement');
      await component.find('input').setValue('2022-12-01');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Requirement inside Multiplechoice variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-multiplechoice-variable.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.multiplechoiceVariable_REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Requirement inside Text variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-text-variable.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.textVariable_REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Requirement inside Singlechoice variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-singlechoice-variable.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.singlechoiceVariable_REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.find('.requirement').exists()).toBe(false);
    });

    test('Requirement inside Set variable', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-set-variable.xml');
      expect(component.findAll('.requirement').length).toBe(1);
      expect(component.findAll('.requirement')[0].attributes('id')).toBe('test_groupVariable.setVariable_REQUIREMENT_myTestRequirement');
      await component.findAll('button')[0].trigger('click');
      expect(component.findAll('.requirement').length).toBe(2);
      expect(component.findAll('.requirement')[0].attributes('id')).toBe('test_groupVariable.setVariable[0].textVariable_REQUIREMENT_myTestRequirement2');
      await component.findAll('input')[0].setValue('test');
      expect(component.findAll('.requirement').length).toBe(1);
    });

    test('Requirement inside DC Instance', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/requirements/requirement-in-dc-instance.xml');
      expect(component.find('.requirement').exists()).toBe(true);
      expect(component.find('.requirement').attributes('id')).toBe('test_groupVariable.dcInstance.REQUIREMENT_myTestRequirement');
      await component.findAll('input')[0].setValue('test');
      expect(component.find('.requirement').exists()).toBe(false);
    });
  });
});

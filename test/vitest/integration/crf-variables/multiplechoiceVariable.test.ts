import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { ValuesSetValue } from '@/entities/ValuesSetValue';

describe('Multiplechoice variable', async () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Multiplechoice Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(component.vm.model.multiplechoiceVariable).toEqual([]);
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(component.html().includes('id="test_multiplechoiceVariable"')).toBeTruthy();
    });
  });

  describe('Visibility', () => {
    test('Multiplechoice Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(component.get('input'));
    });

    test('Multiplechoice Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/not-visible-multiplechoice-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });

    test('Multiplechoice Variable should be disabled with valuesGroup in valuesSet', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/multiplechoice-with-valuesgroup.xml');
      const checkboxList = component.findAll('.checkboxlist')[1];
      const checkbox = checkboxList.find('input');
      expect(checkbox.attributes('disabled')).toBeDefined();
    });
  });

  describe('Input', () => {
    test('with 1 selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.vm.model.multiplechoiceVariable.length).toBe(1);
      expect(component.vm.model.multiplechoiceVariable[0].toString()).toEqual('sets.first');
    });

    test('with all selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      await component.findAll('input')[2].setValue(true);
      await component.findAll('input')[2].trigger('input');
      expect(component.vm.model.multiplechoiceVariable.length).toBe(3);
      expect(component.vm.model.multiplechoiceVariable[0].toString()).toEqual('sets.first');
      expect(component.vm.model.multiplechoiceVariable[1].toString()).toEqual('sets.second');
      expect(component.vm.model.multiplechoiceVariable[2].toString()).toEqual('sets.third');
    });

    test('with none selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(component.vm.model.multiplechoiceVariable).toEqual([]);
    });
  });

  describe('Computed Formula', () => {
    test('Multiplechoice Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/computedformula-copy-from-other-multiplechoice-variable.xml');
      expect(component.vm.model.multiplechoiceVariable2).toEqual([]);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.vm.model.multiplechoiceVariable2[0].getValue()).toEqual(new ValuesSetValue('sets.first').getValue());
    });

    test('Multiplechoice Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/computedformula-complex-multiplechoice-variable.xml');
      expect(component.vm.model.multiplechoiceVariable2).toEqual([]);
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      expect(component.vm.model.multiplechoiceVariable2[0].getValue()).toEqual(new ValuesSetValue('sets.first').getValue());
    });

    test('Multiplechoice Variable computed formula with null value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/computedformula-complex-multiplechoice-variable-with-null-value.xml');
      expect(component.vm.model.multiplechoiceVariable2).toEqual([]);
      await component.findAll('input')[4].setValue(true);
      await component.findAll('input')[4].trigger('input');
      expect(component.vm.model.multiplechoiceVariable2[0].getValue()).toEqual(new ValuesSetValue('sets.second').getValue());
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      await component.findAll('input')[1].setValue(true);
      await component.findAll('input')[1].trigger('input');
      component.findAll('input').slice(3).forEach((input) => expect(input.element.disabled).toBe(true));
      expect(component.vm.model.multiplechoiceVariable2[0].getValue()).toEqual(new ValuesSetValue('sets.first').getValue());
    });
  });
});
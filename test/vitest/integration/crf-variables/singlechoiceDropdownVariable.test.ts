import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { ValuesSetValue } from '@/entities/ValuesSetValue';

describe('Singlechoice Dropdown variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Singlechoice Dropdown Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/standard-singlechoice-dropdown-variable.xml');
      expect(component.vm.model.dropVariable).toBe('');
    });
  });

  describe('Visibility', () => {
    test('Singlechoice Dropdown Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/standard-singlechoice-dropdown-variable.xml');
      expect(component.get('select'));
    });

    test('Singlechoice Dropdown Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/not-visible-singlechoice-dropdown-variable.xml');
      expect(component.findAll('select').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('with 1 selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/standard-singlechoice-dropdown-variable.xml');
      await component.find('select').setValue('sets.first');
      await component.find('select').trigger('input');
      expect(component.vm.model.dropVariable.value).toStrictEqual('sets.first');
      expect(component.vm.model.dropVariable.getLabel()).toStrictEqual('First');
      expect(component.find('select').element.value).toEqual('sets.first');
    });

    test('with none selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/standard-singlechoice-dropdown-variable.xml');
      expect(component.vm.model.dropVariable).toBe('');
    });
  });

  describe('Computed Formula', () => {
    test('SinglechoiceDropdown Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/computedformula-copy-from-other-singlechoice-dropdown-variable.xml');
      expect(component.vm.model.dropVariable2).toBe('');
      await component.find('select').setValue('sets.first');
      await component.find('select').trigger('input');
      await component.find('select').trigger('change');
      expect(component.vm.model.dropVariable2).toEqual(new ValuesSetValue('sets.first', 'sets.first', 0));
    });

    test('SinglechoiceDropdown Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceDropdownVariable/computedformula-complex-singlechoice-dropdown-variable.xml');
      expect(component.vm.model.dropVariable2).toBe('sets.second');
      await component.find('select').setValue('sets.first');
      await component.find('select').trigger('input');
      await component.find('select').trigger('change');
      expect(component.vm.model.dropVariable2).toEqual('sets.third');
    });
  });
});

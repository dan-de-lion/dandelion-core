import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';

describe('Boolean variable', async () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Boolean Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      expect(component.vm.model.booleanVariable).toBe(false);
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      expect(component.html().includes('id="test_booleanVariable"')).toBeTruthy();
    });
  });

  describe('Visibility', () => {
    test('Boolean Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      expect(component.get('input'));
    });

    test('Boolean Variable should not be checked', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      const inputChecked = component.find('input').element.checked;
      expect(inputChecked).toBe(false);
    });

    test('Boolean Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/not-visible-boolean-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('Boolean Variable should update model when input change is detected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      component.find('input').element.checked = true;
      component.find('input').trigger('input');
      await nextTick();
      expect(component.vm.model.booleanVariable).toBe(true);
    });

    test('Boolean Variable should update model when input changes multiple times', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/standard-boolean-variable.xml');
      component.find('input').element.checked = true;
      component.find('input').trigger('input');
      component.find('input').element.checked = false;
      component.find('input').trigger('input');
      await nextTick();
      expect(component.vm.model.booleanVariable).toBe(false);
    });
  });

  describe('Computed Formula', () => {
    test('Boolean Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/computedformula-copy-from-other-boolean-variable.xml');
      expect(component.vm.model.booleanVariable2).toBe(false);
      component.findAll('input')[0].element.checked = true;
      component.find('input').trigger('input');
      component.find('input').trigger('change');
      await nextTick();
      expect(component.vm.model.booleanVariable2).toBe(true);
    });

    test('Boolean Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/booleanVariable/computedformula-complex-boolean-variable.xml');
      await nextTick();
      expect(component.vm.model.booleanVariable2).toBe(true);
      component.findAll('input')[0].element.checked = true;
      component.find('input').trigger('input');
      await nextTick();
      expect(component.vm.model.booleanVariable2).toBe(false);
    });
  });
});

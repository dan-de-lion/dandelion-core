import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';

describe('Color variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Color Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/colorVariable/standard-color-variable.xml');
      expect(component.vm.model.colorVariable).toBe('');
    });
  });

  describe('Visibility', () => {
    test('Color Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/colorVariable/standard-color-variable.xml');
      expect(component.get('input'));
    });

    test('Color Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/colorVariable/not-visible-color-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('Color Variable should update model when input change is detected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/colorVariable/standard-color-variable.xml');
      await component.find('input').setValue('#669c35');
      await nextTick();
      expect(component.vm.model.colorVariable).toBe('#669c35');
    });
  });

  describe('Computed Formula', () => {
    test('Color Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/colorVariable/computedformula-copy-from-other-color-variable.xml');
      expect(component.vm.model.colorVariable2).toBe('');
      await component.findAll('input')[0].setValue('#669c35');
      await nextTick();
      expect(component.vm.model.colorVariable2).toBe('#669c35');
    });

    test('Color Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/colorVariable/computedformula-complex-color-variable.xml');
      expect(component.vm.model.colorVariable2).toBe('222');
      await component.findAll('input')[0].setValue('#669c35');
      await component.findAll('input')[0].trigger('input');
      await nextTick();
      expect(component.vm.model.colorVariable2).toBe('#669222');
    });
  });
});
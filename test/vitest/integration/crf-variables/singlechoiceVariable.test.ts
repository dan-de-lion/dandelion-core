import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Singlechoice Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Singlechoice Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      expect(component.vm.model.singlechoiceVariable).toBe('');
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      expect(component.html().includes('id="test_singlechoiceVariable"')).toBeTruthy();
    });
  });

  describe('Visibility', () => {
    test('Singlechoice Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      expect(component.get('input'));
    });

    test('Singlechoice Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/not-visible-singlechoice-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });

    test('Singlechoice Variable should be disabled with valuesGroup in valuesSet', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/singlechoice-with-valuesgroup.xml');
      const radioList = component.findAll('.radiolist')[1];
      const radio = radioList.find('input');
      expect(radio.attributes('disabled')).toBeDefined();
    });
  });

  describe('Input', () => {
    test('with 1 selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.vm.model.singlechoiceVariable.toString()).toEqual('sets.first');
    });

    test('with none selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/standard-singlechoice-variable.xml');
      expect(component.vm.model.singlechoiceVariable).toBe('');
    });
  });

  describe('Computed Formula', () => {
    test('Singlechoice Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/computedformula-copy-from-other-singlechoice-variable.xml');
      expect(component.vm.model.singlechoiceVariable2).toBe('');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.vm.model.singlechoiceVariable2.toString()).toEqual('sets.first');
    });

    test('Singlechoice Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/singlechoiceVariable/computedformula-complex-singlechoice-variable.xml');
      expect(component.vm.model.singlechoiceVariable2.toString()).toBe('sets.third');
      await component.findAll('input')[0].setValue(true);
      await component.findAll('input')[0].trigger('input');
      expect(component.vm.model.singlechoiceVariable2.toString()).toEqual('sets.second');
    });
  });
});

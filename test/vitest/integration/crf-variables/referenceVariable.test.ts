import path from 'path';
import { describe, test, expect } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Reference Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('First level variables', () => {
    test('Text Variable with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/text-with-reference.xml');
      const text = component.findAll('input')[0];
      const textRef = component.findAll('input')[1];
      const textSpan = component.findAll('span')[0];
      const textRefSpan = component.findAll('span')[1];
      text.element.value = 'Valore';
      await text.trigger('input');
      expect(textRef.element.value).toBe('Valore');
      expect(textSpan.text()).toBe(textRefSpan.text()); //test only for one variable that the lables are the same
      text.element.value = '';
      await text.trigger('input');
      expect(textRef.element.value).toBe('');
      textRef.element.value = 'Valore';
      await textRef.trigger('input');
      expect(textRef.attributes('id')).toBe('test_textRef.textVariable');
      expect(text.element.value).toBe('Valore');
      expect(component.vm.model.textVariable).toBe(textRef.element.value);
    });

    test('Boolean Variable with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/boolean-with-reference.xml');
      const boolean = component.findAll('input')[0];
      const booleanRef = component.findAll('input')[1];
      boolean.element.checked = true;
      await boolean.trigger('input');
      expect(booleanRef.element.checked).toBe(true);
      boolean.element.checked = false;
      await boolean.trigger('input');
      expect(booleanRef.element.checked).toBe(false);
      booleanRef.element.checked = true;
      await booleanRef.trigger('input');
      expect(booleanRef.attributes('id')).toBe('test_booleanRef.booleanVariable');
      expect(boolean.element.checked).toBe(true);
      expect(component.vm.model.booleanVariable).toBe(booleanRef.element.checked);
    });

    test('Date Variable with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/date-with-reference.xml');
      const date = component.findAll('input')[0];
      const dateRef = component.findAll('input')[1];
      date.element.value = '2023-07-20';
      await date.trigger('input');
      expect(dateRef.element.value).toBe('2023-07-20');
      date.element.value = '';
      await date.trigger('input');
      expect(dateRef.element.value).toBe('');
      dateRef.element.value = '2023-07-18';
      await dateRef.trigger('input');
      expect(dateRef.attributes('id')).toBe('test_dateRef.dateVariable');
      expect(date.element.value).toBe('2023-07-18');
      expect(component.vm.model.dateVariable).toBe(dateRef.element.value);
    });

    test('SingleChoice Variable with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/singlechoice-with-reference.xml');
      const singlechoiceFirst = component.findAll('input')[0];
      const singlechoiceRefFirst = component.findAll('input')[3];
      const singlechoiceFirstSpan = component.findAll('span')[0];
      const singlechoiceRefFirstSpan = component.findAll('span')[6];
      const resetButton = component.findAll('button')[0];
      singlechoiceFirst.element.checked = true;
      await singlechoiceFirst.trigger('input');
      expect(singlechoiceRefFirst.element.checked).toBe(true);
      expect(singlechoiceFirstSpan.text()).toBe(singlechoiceRefFirstSpan.text()); //test only for one singlechoices that the value lables are the same
      await resetButton.trigger('click');
      expect(singlechoiceFirst.element.checked).toBe(false);
      expect(singlechoiceRefFirst.element.checked).toBe(false);
      singlechoiceRefFirst.element.checked = true;
      await singlechoiceRefFirst.trigger('input');
      expect(singlechoiceRefFirst.attributes('id')).toBe('test_singlechoiceRef.singlechoiceVariable-sets.first');
      expect(singlechoiceFirst.element.checked).toBe(true);
      expect(component.vm.model.singlechoiceVariable.toString()).toBe('sets.first');
    });

    test('MultipleChoice Variable with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/multiplechoice-with-reference.xml');
      const multipleChoiceFirst = component.findAll('input')[0];
      const refMultipleChoiceFirst = component.findAll('input')[3];
      multipleChoiceFirst.element.checked = true;
      await multipleChoiceFirst.trigger('input');
      expect(refMultipleChoiceFirst.element.checked).toBe(true);
      multipleChoiceFirst.element.checked = false;
      await multipleChoiceFirst.trigger('input');
      expect(refMultipleChoiceFirst.element.checked).toBe(false);
      refMultipleChoiceFirst.element.checked = true;
      await refMultipleChoiceFirst.trigger('input');
      expect(refMultipleChoiceFirst.attributes('id')).toBe('test_multiplechoiceRef.multiplechoiceVariable-sets.first');
      expect(multipleChoiceFirst.element.checked).toBe(true);
      expect(component.vm.model.multiplechoiceVariable.toString()).toBe('sets.first');
    });

    test('Group with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/group-with-reference.xml');
      //better test two variables for a whole group
      const textInGroup = component.findAll('input')[0];
      const numberInGroup = component.findAll('input')[1];
      const textInGroupRef = component.findAll('input')[2];
      const numberInGroupRef = component.findAll('input')[3];
      textInGroup.element.value = 'Valore';
      numberInGroup.element.value = '5';
      await textInGroup.trigger('input');
      await numberInGroup.trigger('input');
      expect(textInGroupRef.element.value).toBe('Valore');
      expect(numberInGroupRef.element.value).toBe('5');
      textInGroup.element.value = '';
      numberInGroup.element.value = '';
      await textInGroup.trigger('input');
      await numberInGroup.trigger('input');
      expect(textInGroupRef.element.value).toBe('');
      expect(numberInGroupRef.element.value).toBe('');
      textInGroupRef.element.value = 'Valore';
      numberInGroupRef.element.value = '5';
      await textInGroupRef.trigger('input');
      await numberInGroupRef.trigger('input');
      expect(textInGroupRef.attributes('id')).toBe('test_groupRef.groupVariable.textVariable');
      expect(numberInGroupRef.attributes('id')).toBe('test_groupRef.groupVariable.numberVariable');
      expect(textInGroup.element.value).toBe('Valore');
      expect(numberInGroup.element.value).toBe('5');
      expect(component.vm.model.groupVariable.textVariable).toBe(textInGroupRef.element.value);
      expect(component.vm.model.groupVariable.numberVariable).toBe(Number(numberInGroupRef.element.value));
    });

    test('Set with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/set-with-reference.xml');
      const setAddItemsBtn = component.findAll('.add-item')[0];
      const setRefAddItemsBtn = component.findAll('.add-item')[1];
      await setAddItemsBtn.trigger('click');
      const textInSet = component.findAll('input')[0];
      const textInSetRef = component.findAll('input')[1];
      textInSet.element.value = 'Valore';
      await textInSet.trigger('input');
      expect(textInSetRef.element.value).toBe('Valore');
      const setRemoveItemsBtn = component.findAll('.remove-item')[0];
      await setRemoveItemsBtn.trigger('click');
      await nextTick();
      expect(component.findAll('input').length).toBe(0);
      await setRefAddItemsBtn.trigger('click');
      await setRefAddItemsBtn.trigger('click');
      const secondTextInSet = component.findAll('input')[1];
      const secondTextInSetRef = component.findAll('input')[3];
      textInSetRef.element.value = 'Valore';
      secondTextInSetRef.element.value = 'Valore2';
      await textInSetRef.trigger('input');
      await secondTextInSetRef.trigger('input');
      expect(textInSetRef.attributes('id')).toBe('test_setRef.setVariable[0].textVariable');
      expect(textInSet.element.value).toBe('Valore');
      expect(secondTextInSet.element.value).toBe('Valore2');
      expect(component.vm.model.setVariable[1].textVariable).toBe(secondTextInSetRef.element.value);
    });

    test('DataClass with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/dataclass-with-reference.xml');
      //better test two variables for a whole dcInstance
      const textInDcInstance = component.findAll('input')[0];
      const numberInDCInstance = component.findAll('input')[1];
      const textInDcInstanceRef = component.findAll('input')[2];
      const numberInDCInstanceRef = component.findAll('input')[3];
      textInDcInstance.element.value = 'Valore';
      numberInDCInstance.element.value = '5';
      await textInDcInstance.trigger('input');
      await numberInDCInstance.trigger('input');
      expect(textInDcInstanceRef.element.value).toBe('Valore');
      expect(numberInDCInstanceRef.element.value).toBe('5');
      textInDcInstance.element.value = '';
      numberInDCInstance.element.value = '';
      await textInDcInstance.trigger('input');
      await numberInDCInstance.trigger('input');
      expect(textInDcInstanceRef.element.value).toBe('');
      expect(numberInDCInstanceRef.element.value).toBe('');
      textInDcInstanceRef.element.value = 'Valore';
      numberInDCInstanceRef.element.value = '5';
      await textInDcInstanceRef.trigger('input');
      await numberInDCInstanceRef.trigger('input');
      expect(textInDcInstanceRef.attributes('id')).toBe('test_dcInstanceRef.dcInstance.textVariable');
      expect(numberInDCInstanceRef.attributes('id')).toBe('test_dcInstanceRef.dcInstance.numberVariable');
      expect(textInDcInstance.element.value).toBe('Valore');
      expect(numberInDCInstance.element.value).toBe('5');
      expect(component.vm.model.dcInstance.textVariable).toBe(textInDcInstanceRef.element.value);
      expect(component.vm.model.dcInstance.numberVariable).toBe(Number(numberInDCInstanceRef.element.value));
    });
  });

  describe('Second level variables', () => {
    test('Text in Group with reference', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/text-in-group-with-reference.xml');
      const textInGroup = component.findAll('input')[0];
      const textInGroupRef = component.findAll('input')[1];
      textInGroup.element.value = 'Valore';
      await textInGroup.trigger('input');
      expect(textInGroupRef.element.value).toBe('Valore');
      textInGroup.element.value = '';
      await textInGroup.trigger('input');
      expect(textInGroupRef.element.value).toBe('');
      textInGroupRef.element.value = 'Valore';
      await textInGroupRef.trigger('input');
      expect(textInGroupRef.attributes('id')).toBe('test_textInGroupRef.textVariable');
      expect(textInGroup.element.value).toBe('Valore');
      expect(component.vm.model.groupVariable.textVariable).toBe(textInGroupRef.element.value);
    });

    test('Text inside DC instance with reference in a secondDC', async () => {
      const component = await mountDcInstanceComponent('/reference/text-in-dc-with-reference.xml');
      const refInput = component.find('input');
      expect(refInput.exists()).toBe(true);
      expect(refInput.attributes('id')).toBe('test_textRef.textVariable');
    });
  });

  describe('Third level variables', async () => {
    test('Text inside DC instance inside another DC instance with reference in a secondDC', async () => {
      const refComponent = await mountDcInstanceComponent('/reference/text-in-dc-in-dc-with-reference.xml');
      const refInput = refComponent.find('input');
      expect(refInput.exists()).toBe(true);
      expect(refInput.attributes('id')).toBe('test_textRef.textVariable');
    });

    test('Text inside group inside another group with reference in a secondDC', async () => {
      const refComponent = await mountDcInstanceComponent('/reference/text-in-group-in-group-with-reference.xml');
      const refInput = refComponent.find('input');
      expect(refInput.exists()).toBe(true);
      expect(refInput.attributes('id')).toBe('test_textInGroupRef.textVariable');
    });

    test('Text inside group inside DC instance with reference in a secondDC', async () => {
      const component = await mountDcInstanceComponent('/reference/text-in-group-in-dc-with-reference.xml');
      const refInput = component.find('input');
      expect(refInput.exists()).toBe(true);
      expect(refInput.attributes('id')).toBe('test_textRef.textVariable');
    });

    test('Text inside DC instance inside group with reference in a secondDC', async () => {
      const component = await mountDcInstanceComponent('/reference/text-in-dc-in-group-with-reference.xml');
      const refInput = component.find('input');
      expect(refInput.exists()).toBe(true);
      expect(refInput.attributes('id')).toBe('test_textRef.textVariable');
    });
  });

  describe('Interface pagelink', () => {
    test('interface pagelink should work correctly', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/reference/group-separateInPages-with-reference-pagelink.xml');
      const pageToOpen = component.find('.dcInstance2');
      const referenceLink = component.find('.page-link');
      expect(getComputedStyle(pageToOpen.element).display).toBe('none');
      const pageToOpenName = pageToOpen.find('legend').element.textContent;
      const referenceLinkName = referenceLink.element.textContent;
      expect(referenceLinkName).toBe(pageToOpenName);
      await referenceLink.trigger('click');
      expect(getComputedStyle(pageToOpen.element).display).not.toBe('none');
    });
  });
});

async function mountDcInstanceComponent(xmlPath: string) {
  const xml = TestHelper.requireFile(xmlPath);
  return await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE,
    path.parse(xmlPath).name, '0.0.1', false, false, false, 0, {}, '', Contexts.TEST, 'secondDC');
}

// This is outside the previous "describe" because we should setup again the containers
describe('Reference variables: encountered bugs', () => {
  test('Reference to variable with name containing CRF name', async () => {
    const crfName = 'Admission'; /* CRF name that is contained in 'referencedFullName' */
    // We build here container as we need to be sure that the CRF name is that one
    TestHelper.setupContainer(crfName);
    TestHelper.mockStatusService(Contexts.TEST);
    const xml = TestHelper.requireFile('/reference/reference-with-name-containing-crf-name.xml');
    const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_COLLECTION_PAGE, crfName, '0.0.1');
    const textInGroup = component.findAll('input')[0];
    const textInGroupRef = component.findAll('input')[1];
    textInGroup.element.value = 'Valore';
    await textInGroup.trigger('input');
    expect(textInGroupRef.element.value).toBe('Valore');
    textInGroup.element.value = '';
    await textInGroup.trigger('input');
    expect(textInGroupRef.element.value).toBe('');
    textInGroupRef.element.value = 'Valore';
    await textInGroupRef.trigger('input');
    expect(textInGroupRef.attributes('id')).toBe('test_textInGroupRef.textVariable');
    expect(textInGroup.element.value).toBe('Valore');
    expect(component.vm.model.groupInAdmission.textVariable).toBe(textInGroupRef.element.value);
  });
});
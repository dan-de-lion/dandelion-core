import path from 'path';
import { describe, test, expect, beforeAll } from 'vitest';
import TestHelper from '../../../utils/TestHelper';
import { getContainer } from '@/ContainersConfig';
import type { CRFToHTMLService } from '@/services/CRFToHTMLService';
import { Contexts } from '@/types/Contexts';
import { MagicNumbers } from '@/types/MagicNumbers';
import { NamedBindings } from '@/types/NamedBindings';
import { PathToXsl } from '@/types/PathToXsl';

describe('Reference with startingDataclass', () => {
  TestHelper.testSetupWithCaseOpening();
  let crfToHTMLService: CRFToHTMLService;

  beforeAll(() => {
    const container = getContainer(Contexts.DEFAULT);
    crfToHTMLService = container.get(NamedBindings.CRFToHTMLService) as CRFToHTMLService;
  })

  test('newCase section should contain input related to variable referred in another dataclass', async () => {
    const html = await getHtmlWithStartingDc('/reference/reference-with-starting-dataclass.xml', 'newCase')
    const element = TestHelper.htmlStringToFragment(html);
    const input = element.querySelector('input') as HTMLInputElement;
    expect(input.getAttribute('type')).toBe('text');
    expect(input.getAttribute('v-custom-model')).toBe("'textVariable'");
    expect(input.getAttribute(':id')).toBe("getContextForID + 'refToTextVariable.textVariable'");
  });

  test('reference without startingDataClass picks the first Dataclass as starting point', async () => {
    const html = await getHtmlWithStartingDc('/reference/reference-with-starting-dataclass.xml', 'secondDC')
    const element = TestHelper.htmlStringToFragment(html);
    const input1 = element.querySelectorAll('input')[0] as HTMLInputElement;
    const input2 = element.querySelectorAll('input')[1] as HTMLInputElement;
    expect(input1.getAttribute('type')).toBe('text');
    expect(input1.getAttribute(':id')).toBe("getContextForID + 'textVariable2'");
    expect(input2.getAttribute('type')).toBe('text');
    expect(input2.getAttribute(':id')).toBe("getContextForID + 'refToText.textVariable'");
  });

  test("should print the html of the first Dataclass when 'startingDataClass' = ''", async () => {
    const xml = TestHelper.requireFile('/reference/reference-with-starting-dataclass.xml');
    const html = await crfToHTMLService.getHTMLfromXML(xml, 'reference-with-starting-dataclass', '0.0.1',
      `/public/xslt/${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`, false, false, false, MagicNumbers.maxDepthForCRFComponentAndTree);
    const element = TestHelper.htmlStringToFragment(html);
    const input1 = element.querySelector('input') as HTMLInputElement;
    expect(input1.getAttribute('type')).toBe('text');
  });

  test("reference without starting dataclass shouldn't find the variable", async () => {
    const html = await getHtmlWithStartingDc('/reference/reference-error-without-starting-dataclass.xml', 'newCase')
    expect(html).not.toContain('refToTextVariable');
  });

  test("reference with starting dataclass shouldn't find the variable, if this isn't defined into the dataclass", async () => {
    const html = await getHtmlWithStartingDc('/reference/reference-error-with-starting-dataclass.xml', 'newCase')
    expect(html).not.toContain('refToTextVariable');
  });

  async function getHtmlWithStartingDc(xmlPath: string, startingDc: string) {
    const xml = TestHelper.requireFile(xmlPath);
    return await crfToHTMLService.getHTMLfromXML(xml, path.parse(xmlPath).name, '0.0.1',
      `/public/xslt/${PathToXsl.SCHEMA_TO_COLLECTION_PAGE}`, false, false, false, MagicNumbers.maxDepthForCRFComponentAndTree, startingDc);
  }
});

import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Number Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Number Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/standard-number-variable.xml');
      expect(component.vm.model.numberVariable).toBe(NaN);
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/standard-number-variable.xml');
      expect(component.html().includes('id="test_numberVariable"')).toBeTruthy();
    });
  });

  describe('Visibility', () => {
    test('Number Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/standard-number-variable.xml');
      expect(component.get('input'));
    });

    test('Number Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/not-visible-number-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('Number Variable should update model when input change is detected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/standard-number-variable.xml');
      await component.find('input').setValue(5);
      expect(component.vm.model.numberVariable).toBe(5);
    });
  });

  describe('Computed Formula', () => {
    test('Number Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/computedformula-copy-from-other-number-variable.xml');
      expect(component.vm.model.numberVariable2).toBe(NaN);
      await component.findAll('input')[0].setValue(4);
      expect(component.vm.model.numberVariable2).toBe(4);
    });

    test('Number Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/numberVariable/computedformula-complex-number-variable.xml');
      expect(component.vm.model.numberVariable2).toBe(NaN);
      await component.findAll('input')[0].setValue(4);
      expect(component.vm.model.numberVariable2).toBe(34);
    });
  });
});

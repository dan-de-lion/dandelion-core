import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Output variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Output Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/outputVariable/standard-output-variable.xml');
      expect(component.vm.model.outputVariable).toBe('');
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/outputVariable/standard-output-variable.xml');
      expect(component.html().includes('id="test_outputVariable"')).toBeTruthy();
    });
  });

  describe('Visibility', () => {
    test('Output Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/outputVariable/standard-output-variable.xml');
      expect(component.get('output'));
    });

    test('Output Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/outputVariable/not-visible-output-variable.xml');
      expect(component.findAll('output').length).toBeLessThan(1);
    });
  });

  describe('Computed Formula', () => {
    test('Output Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/outputVariable/computedformula-copy-from-other-output-variable.xml');
      expect(component.vm.model.outputVariable).toBe('');
      await component.find('input').setValue('test');
      await component.find('input').trigger('input');
      expect(component.vm.model.outputVariable).toBe('test');
    });

    test('Output Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/outputVariable/computedformula-complex-output-variable.xml');
      expect(component.vm.model.outputVariable).toBe('');
      await component.find('input').setValue('test');
      expect(component.vm.model.outputVariable).toBe('te');
    });
  });
});
import { describe, test, expect } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Support Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Support Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/supportVariable/standard-support-variable.xml');
      expect(component.vm.model.supportVariable).toBe('');
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/supportVariable/standard-support-variable.xml');
      expect(component.html().includes('id="test_supportVariable"')).toBeTruthy();
    });
  });

  describe('Computed Formula', () => {
    test('should have computedFormula attributes', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/supportVariable/computedformula-support-variable.xml');
      expect(component.vm.model.supportVariable).toBe(8);
    });
  });
});
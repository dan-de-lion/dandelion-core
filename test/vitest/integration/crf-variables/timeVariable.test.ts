import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Time variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Time Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/timeVariable/standard-time-variable.xml');
      expect(component.vm.model.timeVariable).toBe('');
    });
  });

  describe('Visibility', () => {
    test('Time Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/timeVariable/standard-time-variable.xml');
      expect(component.get('input'));
    });

    test('Time Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/timeVariable/not-visible-time-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('Time Variable should update model when input change is detected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/timeVariable/standard-time-variable.xml');
      await component.find('input').setValue('15:12');
      expect(component.vm.model.timeVariable).toBe('15:12');
    });
  });

  describe('Computed Formula', () => {
    test('Time Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/timeVariable/computedformula-copy-from-other-time-variable.xml');
      expect(component.vm.model.timeVariable2).toBe('');
      await component.find('input').setValue('15:12');
      expect(component.vm.model.timeVariable2).toBe('15:12');
    });

    test('Time Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/timeVariable/computedformula-complex-time-variable.xml');
      expect(component.vm.model.timeVariable2).toBe(':33');
      await component.find('input').setValue('15:12');
      expect(component.vm.model.timeVariable2).toBe('15:33');
    });
  });
});

import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Set Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Set Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      expect(component.vm.model.setVariable).toEqual([]);
    });

    test('Should have correct id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      expect(component.html().includes('id="test_setVariable"')).toBeTruthy();
    });
  });

  describe('Visibility', () => {
    test('Set Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      expect(component.get('fieldset'));
    });

    test('Set Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/not-visible-set-variable.xml');
      expect(component.findAll('fieldset').length).toBeLessThan(1);
    });
  });

  describe('Initialized', () => {
    test('Model is correctly initialized', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      expect(component.vm.model.setVariable).toEqual([]);
    });
  });

  describe('Click Input', () => {
    test('with 0 click', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      expect(component.vm.model.setVariable.length).toBeLessThan(1);
    });

    test('with 1 click', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/standard-set-variable.xml');
      await component.findAll('button')[0].trigger('click');
      expect(component.vm.model.setVariable.length).toBe(1);
    });

    test('with none selected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      expect(component.vm.model.multiplechoiceVariable).toEqual([]);
    });
  });

  describe('Computed Formula', () => {
    test('Set Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/computedformula-copy-from-other-set-variable.xml');
      expect(component.vm.model.setVariable2).toEqual([]);
      await component.find('button').trigger('click');
      expect(component.vm.model.setVariable2.length).toBeGreaterThan(0);
    });

    test('Set Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/computedformula-complex-set-variable.xml');
      expect(component.vm.model.setVariable2).toEqual([]);
      await component.findAll('button')[0].trigger('click');
      expect(component.vm.model.setVariable2.length).toBe(1);
    });
  });

  describe('Computed items', () => {
    test('Computed items should work correctly', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/setVariable/set-with-computeditems.xml');
      const itemsBefore = component.find('.itemsDC');
      expect(itemsBefore.exists()).toBeFalsy();
      const checkbox = component.find('input');
      checkbox.element.checked = true;
      await checkbox.trigger('input');
      const itemsAfter = component.find('.itemsDC');
      const output = component.find('output');
      expect(itemsAfter.exists()).toBeTruthy();
      expect(output.text()).toBe('sets.first');
    });
  });
});
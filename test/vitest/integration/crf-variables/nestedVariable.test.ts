import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Nested Variables', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    describe('Group', () => {
      test('group have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/groupVariable/standard-group-variable.xml');
        expect(component.html().includes('id="test_groupVariable"')).toBeTruthy();
      });

      test('group in group have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/groupVariable/nested-group-variable.xml');
        expect(component.html().includes('id="test_groupVariable.groupVariable2"')).toBeTruthy();
      });

      test('group into dataClass have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/groupVariable/group-into-dataclass.xml');
        expect(component.html().includes('id="test_dcInstance.groupVariable"')).toBeTruthy();
      });

      test('group into set have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/groupVariable/group-into-set.xml');
        const firstButton = component.findAll('button')[0];
        await firstButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[0].groupVariable"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[1].groupVariable"')).toBeFalsy();
        await firstButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[1].groupVariable"')).toBeTruthy();
      });
    });

    describe('Set', () => {
      test('set in set have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/setVariable/nested-set-variable.xml');
        expect(component.html().includes('id="test_setVariable"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0]"')).toBeFalsy();
        expect(component.html().includes('id="test_setVariable[0].setChild"')).toBeFalsy();
        expect(component.html().includes('id="test_setVariable[0].setChild[0]"')).toBeFalsy();
        const setButton = component.findAll('button')[0];
        await setButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[0]"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0].setChild"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0].setChild[0]"')).toBeFalsy();
        await setButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[1]"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[1].setChild"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[1].setChild[0]"')).toBeFalsy();
        const setChildButton = component.findAll('.add-item')[1];
        await setChildButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[0].setChild[0]"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0].setChild[1]"')).toBeFalsy();
        await setChildButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[0].setChild[1]"')).toBeTruthy();
      });

      test('set into dataClass have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/setVariable/set-into-dataclass.xml');
        expect(component.html().includes('id="test_groupVariable.setVariable"')).toBeTruthy();
        expect(component.html().includes('id="test_groupVariable.setVariable[0]"')).toBeFalsy();
        const parentButton = component.findAll('button')[0];
        await parentButton.trigger('click');
        expect(component.html().includes('id="test_groupVariable.setVariable[0]"')).toBeTruthy();
      });

      test('set into group have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/setVariable/group-set-variable.xml');
        expect(component.html().includes('id="test_groupVariable.setVariable"')).toBeTruthy();
        expect(component.html().includes('id="test_groupVariable.setVariable[0]"')).toBeFalsy();
        const parentButton = component.findAll('button')[0];
        await parentButton.trigger('click');
        expect(component.html().includes('id="test_groupVariable.setVariable[0]"')).toBeTruthy();
      });
    });

    describe('DataClass', () => {
      test('Dataclass have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/dataclassVariable/standard-dataclass-variable.xml');
        expect(component.html().includes('id="test_dcInstance"')).toBeTruthy();
        expect(component.html().includes('id="test_dcInstance.textVariable"')).toBeTruthy();
      });

      test('Dataclass into DataClass have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/dataclassVariable/nested-dataclass-variable.xml');
        expect(component.html().includes('id="test_dcInstance"')).toBeTruthy();
        expect(component.html().includes('id="test_dcInstance.dcInstance2"')).toBeTruthy();
        expect(component.html().includes('id="test_dcInstance.dcInstance2.textVariable"')).toBeTruthy();
      });

      test('Dataclass into group have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/dataclassVariable/dataclass-into-group.xml');
        expect(component.html().includes('id="test_groupVariable.dcInstance"')).toBeTruthy();
        expect(component.html().includes('id="test_groupVariable.dcInstance.textVariable"')).toBeTruthy();
      });

      test('Dataclass into set have correct id', async () => {
        const component = await TestHelper.mountStandardCollectionComponent('/dataclassVariable/dataclass-into-set.xml');
        expect(component.html().includes('id="test_setVariable"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0]"')).toBeFalsy();
        expect(component.html().includes('id="test_setVariable[0].dcInstance"')).toBeFalsy();
        expect(component.html().includes('id="test_setVariable[0].dcInstance.textVariable"')).toBeFalsy();
        const seteButton = component.findAll('button')[0];
        await seteButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[0]"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0].dcInstance"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[0].dcInstance.textVariable"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[1]"')).toBeFalsy();
        expect(component.html().includes('id="test_setVariable[1].dcInstance"')).toBeFalsy();
        expect(component.html().includes('id="test_setVariable[1].dcInstance.textVariable"')).toBeFalsy();
        await seteButton.trigger('click');
        expect(component.html().includes('id="test_setVariable[1]"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[1].dcInstance"')).toBeTruthy();
        expect(component.html().includes('id="test_setVariable[1].dcInstance.textVariable"')).toBeTruthy();
      });
    });
  });
});
import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Date Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Date Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/standard-date-variable.xml');
      expect(component.vm.model.startingDate).toBe('');
    });
  });

  describe('Visibility', () => {
    test('Date Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/standard-date-variable.xml');
      expect(component.get('input'));
    });

    test('Date Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/not-visible-date-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('Date Variable should update model when input change is detected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/standard-date-variable.xml');
      await component.find('input').setValue('2022-12-01');
      expect(component.vm.model.startingDate).toBe('2022-12-01');
    });
  });

  describe('Computed Formula', () => {
    test('Date Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/computedformula-copy-from-other-date-variable.xml');
      expect(component.vm.model.dateVariable2).toBe('');
      await component.findAll('input')[0].setValue('2022-12-01');
      expect(component.vm.model.dateVariable2).toBe('2022-12-01');
    });

    test('Date Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/dateVariable/computedformula-complex-date-variable.xml');
      expect(component.vm.model.dateVariable2).toBe('');
      await component.findAll('input')[0].setValue('2022-12-01');
      expect(component.vm.model.dateVariable2).toBe('2000-12-01');
    });
  });
});

import { describe, test, expect } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Plugin Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Plugin Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/pluginVariable/standard-plugin-variable.xml');
      expect(component.vm.model.pluginVariable).toBe('');
    });

    test('Plugin Variable should correctly set full-name', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/pluginVariable/standard-plugin-variable.xml');
      expect(component.html().includes('FullName pluginVariable')).toBeTruthy();
    });

    test('Plugin Variable should correctly set id', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/pluginVariable/standard-plugin-variable.xml');
      expect(component.html().includes('ID: test_pluginVariable')).toBeTruthy();
    });
  });
});
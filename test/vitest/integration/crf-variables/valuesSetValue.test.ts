import { expect, describe, test } from 'vitest';
import { nextTick } from 'vue';
import TestHelper from '../../../utils/TestHelper';
import { ValuesSetValue } from '@/entities/ValuesSetValue';

describe('Test on ValuesSetValues', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Standard Singlechoice Variable', () => {
    test('Should set correctly a ValuesSetValue in the model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/valuesSetValue/singlechoice/standard-singlechoice-valuessetvalue.xml');
      const firstRadio = component.findAll('input')[0];
      firstRadio.element.checked = true;
      await firstRadio.trigger('input');
      expect(component.vm.model.singlechoiceVariable).toStrictEqual(new ValuesSetValue('gcsEyeOptions.one', 'Does not open eyes', 1));
    });

    test('Should the computedFormula set correctly a ValuesSetValue in the model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/valuesSetValue/singlechoice/computedformula-standard-singlechoice-valuessetvalue.xml');
      const firstRadio = component.findAll('input')[0];
      firstRadio.element.checked = true;
      await firstRadio.trigger('input');
      expect(component.vm.model.computedVariable.getValue()).toStrictEqual(new ValuesSetValue('gcsEyeOptions.one', 'Does not open eyes', 1));
    });
  });

  describe('Dropdown Singlechoice Variable', () => {
    test('Should set correctly a ValuesSetValue in the model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/valuesSetValue/singlechoice/dropdown-singlechoice-valuessetvalue.xml');
      component.findAll('option')[1].element.selected = true;
      await component.findAll('select')[0].trigger('input');
      expect(component.vm.model.singlechoiceVariable).toStrictEqual(new ValuesSetValue('gcsEyeOptions.one', 'Does not open eyes', 1));
    });

    test('Should the computedFormula set correctly a ValuesSetValue in the model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/valuesSetValue/singlechoice/computedformula-dropdown-singlechoice-valuessetvalue.xml');
      component.findAll('option')[1].element.selected = true;
      await component.findAll('select')[0].trigger('input');
      expect(component.vm.model.computedVariable.getValue()).toStrictEqual(new ValuesSetValue('gcsEyeOptions.one', 'Does not open eyes', 1));
    });
  });

  describe('Standard Multiplechoice Variable', () => {
    test('Should set correctly a ValuesSetValue in the model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/valuesSetValue/multiplechoice/standard-multiplechoice-valuessetvalue.xml');
      const secondInput = component.findAll('input')[1];
      const thirdInput = component.findAll('input')[2];
      secondInput.element.checked = true;
      await secondInput.trigger('input');
      expect(component.vm.model.multiplechoiceVariable[0]).toStrictEqual(new ValuesSetValue('gcsEyeOptions.two', 'Opens eyes in response to pain', 2));
      thirdInput.element.checked = true;
      await thirdInput.trigger('input');
      expect(component.vm.model.multiplechoiceVariable[1]).toStrictEqual(new ValuesSetValue('gcsEyeOptions.three', 'Opens eyes in response to voice', 3));
      await nextTick();
      secondInput.element.checked = false;
      await secondInput.trigger('input');
      expect(component.vm.model.multiplechoiceVariable[0]).toStrictEqual(new ValuesSetValue('gcsEyeOptions.three', 'Opens eyes in response to voice', 3));
    });

    test('Should the computedFormula set correctly a ValuesSetValue in the model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/valuesSetValue/multiplechoice/computedformula-standard-multiplechoice-valuessetvalue.xml');
      const secondInput = component.findAll('input')[1];
      const thirdInput = component.findAll('input')[2];
      secondInput.element.checked = true;
      await secondInput.trigger('input');
      expect(component.vm.model.computedVariable[0]).toStrictEqual(new ValuesSetValue('gcsEyeOptions.two', 'Opens eyes in response to pain', 2));
      thirdInput.element.checked = true;
      await thirdInput.trigger('input');
      expect(component.vm.model.computedVariable[1]).toStrictEqual(new ValuesSetValue('gcsEyeOptions.three', 'Opens eyes in response to voice', 3));
      await nextTick();
      secondInput.element.checked = false;
      await secondInput.trigger('input');
      expect(component.vm.model.computedVariable[0]).toStrictEqual(new ValuesSetValue('gcsEyeOptions.three', 'Opens eyes in response to voice', 3));
    });
  });
});

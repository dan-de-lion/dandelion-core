import { expect, describe, test } from 'vitest';
import TestHelper from '../../../utils/TestHelper';

describe('Text Variable', () => {
  TestHelper.testSetupWithCaseOpening();

  describe('Correctly Initialized', () => {
    test('Text Variable should correctly initialized in model', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/standard-text-variable.xml');
      expect(component.vm.model.textVariable).toBe('');
    });
  });

  describe('Visibility', () => {
    test('Text Variable should be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/standard-text-variable.xml');
      expect(component.get('input'));
    });

    test('Text Variable should not be visible', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/not-visible-text-variable.xml');
      expect(component.findAll('input').length).toBeLessThan(1);
    });
  });

  describe('Input', () => {
    test('Text Variable should update model when input change is detected', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/standard-text-variable.xml');
      await component.find('input').setValue('test');
      expect(component.vm.model.textVariable).toBe('test');
    });
  });

  describe('Computed Formula', () => {
    test('Text Variable computed formula with another variable value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/computedformula-copy-from-other-text-variable.xml');
      expect(component.vm.model.textVariable2).toBe('');
      await component.find('input').setValue('test');
      await component.find('input').trigger('input');
      const textVariable2 = component.findAll('input')[1];
      await textVariable2.setValue('test2');
      await textVariable2.trigger('input');
      expect(textVariable2.element.disabled).toBe(true);
      expect(component.vm.model.textVariable2).toBe('test');
    });

    test('Text Variable computed formula with complex value', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/computedformula-complex-text-variable.xml');
      expect(component.vm.model.textVariable2).toBe('');
      await component.find('input').setValue('test');
      expect(component.vm.model.textVariable2).toBe('te');
    });

    test('Text Variable with null value from computed formula can change', async () => {
      const component = await TestHelper.mountStandardCollectionComponent('/textVariable/computedformula-null-value.xml');
      const textVariable2 = component.findAll('input')[1];
      expect(component.vm.model.textVariable2).toBe('');
      await component.find('input').setValue('test');
      expect(component.vm.model.textVariable2).toBe('test');
      expect(textVariable2.element.disabled).toBe(true);
      await component.find('input').setValue('');
      expect(textVariable2.element.disabled).toBe(false);
      await textVariable2.setValue('test4');
      expect(component.vm.model.textVariable2).toBe('test4');
    });
  });
});

import '../../../public/frameless-xslt2';
import { expect, beforeAll, describe, test } from 'vitest';
import TestHelper from '../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Filter for Time Variable', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });
  test('HTML of Filter on TimeVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/timeVariable/standard-time-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-time-variable',
        '0.0.1'
      );
      expect(component.findAll('button').length).toBe(3);
      expect(component.findAll('select').length).toBe(0);
      expect(component.findAll('input').length).toBe(2);
      const showFilterButton = component.findAll('button')[1];
      await showFilterButton.trigger('click');
      expect(component.findAll('select').length).toBe(1);
    }
  });

  test('setOperator() and getOperator() of Filter on TimeVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/timeVariable/standard-time-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-time-variable',
        '0.0.1'
      );
      expect(component.vm.getOperator('filterModel.TimeVariable')).toBe(undefined);
      component.vm.setOperator('filterModel.TimeVariable', '=');
      expect(component.vm.getOperator('filterModel.TimeVariable')).toBe('=');
    }
  });

  test('setFilterValue() and getFilterValue() of Filter on TimeVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/timeVariable/standard-time-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-time-variable',
        '0.0.1'
      );
      expect(component.vm.getFilterValue('filterModel.TimeVariable')).toBe(undefined);
      component.vm.setFilterValue('filterModel.TimeVariable', '11:11');
      expect(component.vm.getFilterValue('filterModel.TimeVariable')).toBe('11:11');
    }
  });
});

import '../../../public/frameless-xslt2';
import { expect, beforeAll, test, describe } from 'vitest';
import TestHelper from '../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Filter for Multiplechoice Variable', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });
  
  test('HTML of Filter on MultiplechoiceVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-multiplechoice-variable',
        '0.0.1'
      );
      expect(component.findAll('button').length).toBe(3);
      expect(component.findAll('select').length).toBe(1);
      const showFilterButton = component.findAll('button')[1];
      await showFilterButton.trigger('click');
      expect(component.findAll('select').length).toBe(2);
    }
  });

  test('setOperator() and getOperator() of Filter on MultiplechoiceVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-multiplechoice-variable',
        '0.0.1'
      );
      expect(component.vm.getOperator('filterModel.MultiplechoiceVariable')).toBe(undefined);
      component.vm.setOperator('filterModel.MultiplechoiceVariable', '=');
      expect(component.vm.getOperator('filterModel.MultiplechoiceVariable')).toBe('=');
    }
  });

  test('setFilterValue() and getFilterValue() of Filter on MultiplechoiceVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/multiplechoiceVariable/standard-multiplechoice-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-multiplechoice-variable',
        '0.0.1'
      );
      expect(component.vm.getFilterValue('filterModel.MultiplechoiceVariable')).toBe(undefined);
      component.vm.setFilterValue('filterModel.MultiplechoiceVariable', 'second');
      expect(component.vm.getFilterValue('filterModel.MultiplechoiceVariable')).toBe('second');
    }
  });
});

import '../../../public/frameless-xslt2';
import { expect, beforeAll, test, describe } from 'vitest';
import TestHelper from '../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Filter for Number Variable', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });

  test('HTML of Filter on NumberVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/numberVariable/standard-number-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-number-variable',
        '0.0.1'
      );
      expect(component.findAll('button').length).toBe(3);
      expect(component.findAll('select').length).toBe(0);
      expect(component.findAll('input').length).toBe(2);
      const showFilterButton = component.findAll('button')[1];
      await showFilterButton.trigger('click');
      expect(component.findAll('select').length).toBe(1);
    }
  });

  test('setOperator() and getOperator() of Filter on NumberVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/numberVariable/standard-number-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-number-variable',
        '0.0.1'
      );
      expect(component.vm.getOperator('filterModel.NumberVariable')).toBe(undefined);
      component.vm.setOperator('filterModel.NumberVariable', '=');
      expect(component.vm.getOperator('filterModel.NumberVariable')).toBe('=');
    }
  });

  test('setFilterValue() and getFilterValue() of Filter on NumberVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/numberVariable/standard-number-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-number-variable',
        '0.0.1'
      );
      expect(component.vm.getFilterValue('filterModel.NumberVariable')).toBe(undefined);
      component.vm.setFilterValue('filterModel.NumberVariable', 2);
      expect(component.vm.getFilterValue('filterModel.NumberVariable')).toBe(2);
    }
  });
});

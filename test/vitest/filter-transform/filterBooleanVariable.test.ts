import '../../../public/frameless-xslt2';
import { expect, beforeAll, test, describe } from 'vitest';
import TestHelper from '../../utils/TestHelper';
import { PathToXsl } from '@/types/PathToXsl';

describe('Filter for Boolean Variable', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
  });
  
  test('HTML of Filter on BooleanVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/booleanVariable/standard-boolean-variable.xml');
      const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_FILTER_PAGE, 'standard-boolean-variable', '0.0.1');
      expect(component.findAll('button').length).toBe(3);
      expect(component.findAll('select').length).toBe(0);
      const showFilterButton = component.findAll('button')[1];
      await showFilterButton.trigger('click');
      expect(component.findAll('input').length).toBe(2);
    }
  });

  test('setOperator() and getOperator() of Filter on BooleanVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/booleanVariable/standard-boolean-variable.xml');
      const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_FILTER_PAGE, 'standard-boolean-variable', '0.0.1');
      expect(component.vm.getOperator('filterModel.BooleanVariable')).toBe(undefined);
      component.vm.setOperator('filterModel.BooleanVariable', '=');
      expect(component.vm.getOperator('filterModel.BooleanVariable')).toBe('=');
    }
  });

  test('setFilterValue() and getFilterValue() of Filter on BooleanVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/booleanVariable/standard-boolean-variable.xml');
      const component = await TestHelper.mountComponentForTests(xml, PathToXsl.SCHEMA_TO_FILTER_PAGE, 'standard-boolean-variable', '0.0.1');
      expect(component.vm.getFilterValue('filterModel.BooleanVariable')).toBe(undefined);
      component.vm.setFilterValue('filterModel.BooleanVariable', true);
      expect(component.vm.getFilterValue('filterModel.BooleanVariable')).toBe(true);
    }
  });
});

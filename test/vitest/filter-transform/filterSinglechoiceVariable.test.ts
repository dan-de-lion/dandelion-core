import '../../../public/frameless-xslt2';
import { expect, beforeAll, test, describe } from 'vitest';
import TestHelper from '../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Filter for Singlechoice Variable', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });
  test('HTML of Filter on SinglechoiceVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-singlechoice-variable',
        '0.0.1'
      );
      expect(component.findAll('button').length).toBe(3);
      expect(component.findAll('select').length).toBe(1);
      const showFilterButton = component.findAll('button')[1];
      await showFilterButton.trigger('click');
      expect(component.findAll('select').length).toBe(2);
    }
  });

  test('setOperator() and getOperator() of Filter on SinglechoiceVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-singlechoice-variable',
        '0.0.1'
      );
      expect(component.vm.getOperator('filterModel.SinglechoiceVariable')).toBe(undefined);
      component.vm.setOperator('filterModel.SinglechoiceVariable', '=');
      expect(component.vm.getOperator('filterModel.SinglechoiceVariable')).toBe('=');
    }
  });

  test('setFilterValue() and getFilterValue() of Filter on SinglechoiceVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/singlechoiceVariable/standard-singlechoice-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-singlechoice-variable',
        '0.0.1'
      );
      expect(component.vm.getFilterValue('filterModel.SinglechoiceVariable')).toBe(undefined);
      component.vm.setFilterValue('filterModel.SinglechoiceVariable', 'first');
      expect(component.vm.getFilterValue('filterModel.SinglechoiceVariable')).toBe('first');
    }
  });
});

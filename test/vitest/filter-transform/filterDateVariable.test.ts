import '../../../public/frameless-xslt2';
import { expect, beforeAll, test, describe } from 'vitest';
import TestHelper from '../../utils/TestHelper';
import { Contexts } from '@/types/Contexts';
import { PathToXsl } from '@/types/PathToXsl';

describe('Filter for Date Variable', () => {
  beforeAll(() => {
    TestHelper.setupContainer();
    TestHelper.mockStatusService(Contexts.TEST);
  });
  
  test('HTML of Filter on DateVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/dateVariable/standard-date-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-date-variable',
        '0.0.1'
      );
      expect(component.findAll('button').length).toBe(3);
      expect(component.findAll('select').length).toBe(0);
      expect(component.findAll('input').length).toBe(2);
      const showFilterButton = component.findAll('button')[1];
      await showFilterButton.trigger('click');
      expect(component.findAll('select').length).toBe(1);
    }
  });

  test('setOperator() and getOperator() of Filter on DateVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/dateVariable/standard-date-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-date-variable',
        '0.0.1'
      );
      expect(component.vm.getOperator('filterModel.DateVariable')).toBe(undefined);
      component.vm.setOperator('filterModel.DateVariable', '=');
      expect(component.vm.getOperator('filterModel.DateVariable')).toBe('=');
    }
  });

  test('setFilterValue() and getFilterValue() of Filter on DateVariable', async () => {
    if (new Date() > new Date('2025-12-20')) {
      const xml = TestHelper.requireFile('/dateVariable/standard-date-variable.xml');
      const component = await TestHelper.mountComponentForTests(
        xml,
        PathToXsl.SCHEMA_TO_FILTER_PAGE,
        'standard-date-variable',
        '0.0.1'
      );
      expect(component.vm.getFilterValue('filterModel.DateVariable')).toBe(undefined);
      component.vm.setFilterValue('filterModel.DateVariable', '10/10/2020');
      expect(component.vm.getFilterValue('filterModel.DateVariable')).toBe('10/10/2020');
    }
  });
});

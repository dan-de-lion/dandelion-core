.PHONY: help init up down shell timelog

help:
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


internal_host = http://host.docker.internal:5173
##
## Available commands:
##---------------------------------------------------------------------------

init:  ## create dev environment
	docker build \
		--build-arg USER_ID=$(shell id -u) \
		--build-arg GROUP_ID=$(shell id -g) \
		-t dandelion-core-dev -f Dockerfile .
	docker run -it --rm --name dandelion-core -p 5173:5173 -v "$$(pwd):/app" -w /app dandelion-core-dev npm install

up: ## run the dev environment and execute app
	docker run -it --rm --name dandelion-core-dev -p 5173:5173 -p 4173:4173 -v "$$(pwd):/app" -w /app dandelion-core-dev npm run dev

upshell: ## run the dev environment and open a shell
	docker run -it --rm --name dandelion-core-dev -p 5173:5173 -p 4173:4173 -v "$$(pwd):/app" -w /app dandelion-core-dev sh

down: ## close the dev container
	docker stop dandelion-core-dev

shell: ## open a shell inside the running dev container (needs make up, or use make upshell)
	docker exec -it dandelion-core-dev sh

build: ## build for production
	docker exec -it dandelion-core-dev npm run build

lint: ## run linter
	docker exec -it dandelion-core-dev npm run lint

publish: ## publish on gitlab. WARNING! Use only in emergency, standard flow is thorough tagging
	npm run build
	cp package.json ./dist
	
	echo "export * from './src/index'" >> ./dist/dandelion-core.d.ts
	echo "import dandelion_core from './src/index'" >> ./dist/dandelion-core.d.ts
	echo "export default dandelion_core" >> ./dist/dandelion-core.d.ts

	echo "export * from './src/module-storages'" >> ./dist/dandelion-core-storages.d.ts
	echo "import dandelion_core_storages from './src/module-storages'" >> ./dist/dandelion-core-storages.d.ts
	echo "export default dandelion_core_storages" >> ./dist/dandelion-core-storages.d.ts

	npm pkg set scripts.prepare=" " --workspace=dist
	npm publish --workspace=dist

local-publish: ## publish in local packages repo, use "make link" in derived projects
	## If you have not yalc installed yet, run `npm install -g yalc`
	npm run build
	cp package.json ./dist
	
	echo "export * from './src/index'" >> ./dist/dandelion-core.d.ts
	echo "import dandelion_core from './src/index'" >> ./dist/dandelion-core.d.ts
	echo "export default dandelion_core" >> ./dist/dandelion-core.d.ts

	echo "export * from './src/module-storages'" >> ./dist/dandelion-core-storages.d.ts
	echo "import dandelion_core_storages from './src/module-storages'" >> ./dist/dandelion-core-storages.d.ts
	echo "export default dandelion_core_storages" >> ./dist/dandelion-core-storages.d.ts

	npm pkg set scripts.prepare=" " --workspace=dist
	cd dist && npx yalc publish

timelog: ## get spent time recorded on Gitlab
	docker run --rm -it -v "$$(pwd)/.gtt:/root/.local/share/.gtt" kriskbx/gitlab-time-tracker report

test-cy:
	docker run -it -v "$$(pwd):/e2e" -w /e2e --add-host=host.docker.internal:host-gateway -e CYPRESS_baseUrl=$(internal_host) cypress/included:10.3.1

launch-bsync: ## launches browser-sync in current folder, to enable testing
	docker exec -it dandelion-core-dev npx browser-sync start --server
	echo "Waiting for browser-sync to start..."

test-vi: ## runs Vitest tests. If TEST argument is passed, runs a single test
	docker exec -it dandelion-core-dev npx vitest run $(TEST)

test: test-vi test-cy
